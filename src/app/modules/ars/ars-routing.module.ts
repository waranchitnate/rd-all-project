import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ArsUserListComponent} from './ars-user/pages/ars-user-list/ars-user-list.component';
import { ArsURL } from './ars.url';

const routes: Routes = [
  { path: ArsURL.user.root, loadChildren: './ars-user/ars-user.module#ArsUserModule' },
  { path: ArsURL.approval.root, loadChildren: './ars-approval/ars-approval.module#ArsApprovalModule' },
  { path: ArsURL.approvalHighestLevel.root, loadChildren: './ars-approval-highest-level/ars-approval-highest-level.module#ArsApprovalHighestLevelModule' },
  { path: ArsURL.analyst.root, loadChildren: './ars-analyst/ars-analyst.module#ArsAnalystModule'},
  { path: ArsURL.config.root, loadChildren: './ars-config/ars-config.module#ArsConfigModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArsRoutingModule { }
