export const ArsURL = {
    analyst: {
      root: 'analyst',
      search: 'adhoc/search',
      manage: 'adhoc/manage/:requestFormId'
    },
    approval: {
      root: 'approval',
      search: 'adhoc/search',
      searchHighest: 'adhoc/search',
      manage: 'adhoc/manage/:requestFormId'
    },
    approvalHighestLevel: {
      root: 'approval-highest',
      search: 'search',
      manage: 'manage'
    },
    config: {
      root: 'config',
      data: 'data-template',
      group: 'data-template-detail/:searchTemplateId',
      system: 'system'
    },
    user: {
      root: 'user',
      search: 'adhoc/search',
      form: 'adhoc/form',
      add: 'adhoc/add',
      edit: 'adhoc/edit/:requestFormId'
    }
  };
