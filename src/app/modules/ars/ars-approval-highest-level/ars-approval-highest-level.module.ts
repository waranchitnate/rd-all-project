import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ApprovalHighestLevelSearchComponent} from './pages/approval-highest-level-search/approval-highest-level-search.component';
import {SharedModule} from '../../../shared/shared.module';
import {ArsApprovalHighestLevelRoutingModule} from './ars-approval-highest-level-routing.module';
import { ApprovalHighestLevelManageComponent } from './pages/approval-highest-level-manage/approval-highest-level-manage.component';

@NgModule({
  declarations: [ApprovalHighestLevelSearchComponent, ApprovalHighestLevelManageComponent],
  imports: [
    CommonModule,
    SharedModule,
    ArsApprovalHighestLevelRoutingModule
  ]
})
export class ArsApprovalHighestLevelModule {
}
