import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArsURL } from '../ars.url';
import {ApprovalHighestLevelSearchComponent} from './pages/approval-highest-level-search/approval-highest-level-search.component';
import {ApprovalHighestLevelManageComponent} from './pages/approval-highest-level-manage/approval-highest-level-manage.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: ArsURL.approvalHighestLevel.search},
  {path: ArsURL.approvalHighestLevel.search, component: ApprovalHighestLevelSearchComponent},
  {path: ArsURL.approvalHighestLevel.manage, component: ApprovalHighestLevelManageComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArsApprovalHighestLevelRoutingModule { }
