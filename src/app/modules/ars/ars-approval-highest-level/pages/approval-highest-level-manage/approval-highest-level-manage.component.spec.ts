import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalHighestLevelManageComponent } from './approval-highest-level-manage.component';

describe('ApprovalHighestLevelManageComponent', () => {
  let component: ApprovalHighestLevelManageComponent;
  let fixture: ComponentFixture<ApprovalHighestLevelManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalHighestLevelManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalHighestLevelManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
