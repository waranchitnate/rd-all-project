import {Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import {SearchTemplateDetailModel} from '../../../models/search-template-detail.model';

@Component({
  selector: 'app-moveitem',
  templateUrl: './moveitem.component.html',
  styleUrls: ['./moveitem.component.css']
})
export class MoveitemComponent implements OnInit,  OnChanges {

  @Input() groupsArray: Array<SearchTemplateDetailModel> = [];
  @Input() selectedItems: Array<SearchTemplateDetailModel> = [];
  @Input() leftTitle: string;
  @Input() rightTItle: string;
  @Input() contentHeight: number;
  @Output() onSelected = new EventEmitter<any>();

  selectedToAdd: SearchTemplateDetailModel;
  selectedToRemove: SearchTemplateDetailModel;

  constructor() {}

  ngOnInit() {
    if (!this.contentHeight) {
      this.contentHeight = 400;
    }
    if (!this.leftTitle) {
        this.leftTitle = 'Field ข้อมูลในตาราง';
    } 

    if (!this.rightTItle) {
      this.rightTItle = 'Field ข้อมูลที่ต้องการนำออก';
    }
  }

  ngOnChanges() {
    console.log('on changed', this.selectedItems);
    console.log('on changed2', this.selectedItems);
  }


  moveToRight() {
    this.selectedItems = this.selectedItems.concat(this.selectedToAdd);
    this.groupsArray = this.groupsArray.filter(selectedData => {
      return this.selectedItems.indexOf(selectedData) < 0;
    });
    this.selectedToAdd = [];
   this.onSelected.emit(this.selectedItems);
  }

  moveToLeft() {
    this.groupsArray = this.groupsArray.concat(this.selectedToRemove);
    this.selectedItems = this.selectedItems.filter(selectedData => {
      return this.groupsArray.indexOf(selectedData) < 0;
    });
    this.selectedToRemove = [];
   this.onSelected.emit(this.selectedItems);
  }
}
