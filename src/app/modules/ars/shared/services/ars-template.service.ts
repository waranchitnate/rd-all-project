import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { ArsApiExtensionsService } from './ars-apiextension.service';
import { Observable } from 'rxjs';
import { ResponseT } from 'src/app/shared/models/response.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { RequestFormTableModel } from '../../models/request-form-table.model';
import {RequestFormModel} from '../../models/request-form.model';

@Injectable({
  providedIn: 'root'
})

export class ArsTemplateService {
  private templateUrl = environment.arsApiUrl + 'template/';
  private httpOptions = {};

  constructor(private http: HttpClient, private arsApiExtensionsService: ArsApiExtensionsService) {
    this.httpOptions = this.arsApiExtensionsService.getHttpOptionsJson();
  }

  public getSearchTemplateDetailBySearchTemplateId(criteria: any): Observable<ResponseT<any>>  {
    return <Observable<ResponseT<any>>>this.http.post(this.templateUrl + 'getSearchTemplateDetailBySearchTemplateId', criteria, this.httpOptions);
  }

}
