import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import * as XLSX from 'xlsx';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class ArsExportService {
    fileExcelExtension = '.xlsx';
    exportExcel(element: any, prefixFileName: string) {
        if (element) {
            const dateNow = moment().locale('th').format('DDMMYYYYHHmmss');
            const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
            const wb: XLSX.WorkBook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
            /* save to file */
            XLSX.writeFile(wb, `${prefixFileName}${dateNow}${this.fileExcelExtension}`);
        }

    }
}