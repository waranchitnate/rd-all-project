import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
  })
export class ArsApiExtensionsService {

    getHttpOptionsJson() {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'SkipAuth': '1'
              })
        };
        return httpOptions;
    }
}