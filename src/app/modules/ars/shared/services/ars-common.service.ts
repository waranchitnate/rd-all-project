import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { ArsApiExtensionsService } from './ars-apiextension.service';

@Injectable({
  providedIn: 'root'
})

export class ArsCommonService {
  private commonUrl = environment.arsApiUrl + 'common/';
  private httpOptions = {};

  constructor(private http: HttpClient, private arsApiExtensionsService: ArsApiExtensionsService) {
    this.httpOptions = this.arsApiExtensionsService.getHttpOptionsJson();
  }

  public getAllActiveSearchTemplateDropdown() {
    return this.http.get<Array<any>>(this.commonUrl + 'getAllActiveSearchTemplateDropdown', this.httpOptions);
  }

  public getAllStatusDropdown() {
    return this.http.get<Array<any>>(this.commonUrl + 'getAllStatusDropdown', this.httpOptions);
  }

  public convertSortingToTH(txt: string) {
    return (!txt) ? '-' : ((txt === 'ASC') ? 'เรียงจากน้อยไปมาก' : 'เรียงจากมากไปน้อย');
  }
}
