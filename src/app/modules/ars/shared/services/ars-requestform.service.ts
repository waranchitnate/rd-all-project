import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { ArsApiExtensionsService } from './ars-apiextension.service';
import { Observable } from 'rxjs';
import { ResponseT } from 'src/app/shared/models/response.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { RequestFormTableModel } from '../../models/request-form-table.model';
import {RequestFormModel} from '../../models/request-form.model';

@Injectable({
  providedIn: 'root'
})

export class ArsRequestFormService {
  private requestFormUrl = environment.arsApiUrl + 'requestForm/';
  private httpOptions = {};

  constructor(private http: HttpClient, private arsApiExtensionsService: ArsApiExtensionsService) {
    this.httpOptions = this.arsApiExtensionsService.getHttpOptionsJson();
  }

  public getRequestFormForAnalystByCondition(criteria: any) {
    return this.http.post<Array<any>>(this.requestFormUrl + 'getRequestFormForAnalystByCondition', criteria, this.httpOptions);
  }
  public getRequestFormByRequestId(criteria: any): Observable<ResponseT<RequestFormModel>> {
    return <Observable<ResponseT<RequestFormModel>>>this.http.post(this.requestFormUrl + 'getRequestFormByRequestId', criteria, this.httpOptions);
  }
  public getRequestFormForApproverByCondition(criteria: any): Observable<ResponseT<PageResponse<RequestFormTableModel>>> {
    return <Observable<ResponseT<PageResponse<RequestFormTableModel>>>>this.http.post(this.requestFormUrl + 'getRequestFormForApproverByCondition', criteria, this.httpOptions);
  }

  public getRequestFormForFirstApproverByCondition(criteria: any): Observable<ResponseT<PageResponse<RequestFormTableModel>>> {
    return <Observable<ResponseT<PageResponse<RequestFormTableModel>>>>this.http.post(this.requestFormUrl + 'getRequestFormForFirstApproverByCondition', criteria, this.httpOptions);
  }

  public getRequestFormForSecondApproverByCondition(criteria: any): Observable<ResponseT<PageResponse<RequestFormTableModel>>> {
    return <Observable<ResponseT<PageResponse<RequestFormTableModel>>>>this.http.post(this.requestFormUrl + 'getRequestFormForSecondApproverByCondition', criteria, this.httpOptions);
  }
  public getSearchTemplateDetailByMasterId(id: number) {
    return this.http.post(this.requestFormUrl + 'getSearchTemplateDetailByMasterId', id, this.httpOptions);
  }
  public getRequestFormByCriteria(criteria: any): Observable<ResponseT<PageResponse<RequestFormTableModel>>> {
    return <Observable<ResponseT<PageResponse<RequestFormTableModel>>>>this.http.post(this.requestFormUrl + 'getRequestFormByCondition', criteria);
  }

  public deleteRequestFormByIds(ids: Array<number>) {
    return this.http.post(this.requestFormUrl + 'deleteById', ids, this.httpOptions);
  }

}
