import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MoveitemComponent} from './components/moveitem/moveitem.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [MoveitemComponent],
  imports: [
    CommonModule, RouterModule, FormsModule
  ], exports: [MoveitemComponent]
})
export class ArsCommonModule {
}
