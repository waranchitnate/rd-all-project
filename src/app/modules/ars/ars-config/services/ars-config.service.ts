import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Mparam } from '../models/m-param.model';
import { ResponseT } from 'src/app/shared/models/response.model';
import { ArsApiExtensionsService } from '../../shared/services/ars-apiextension.service';

@Injectable({
  providedIn: 'root'
})
export class ArsConfigService {

  private url = environment.arsApiUrl + 'config';
  private templateUrl = environment.arsApiUrl + 'template';
  
  masterTable = [
    { dataGroupId: 1, dataGroupName: 'กลุ่มข้อมูลการบริจาค' ,openStartDate: new Date(),openStartDateEnd: new Date(),lastDate: new Date()},
    { dataGroupId: 2, dataGroupName: 'กลุ่มข้อมูลการยื่นเพิ่มเติม'  ,openStartDate: new Date(),openStartDateEnd: new Date(),lastDate: new Date()},
    { dataGroupId: 3, dataGroupName: 'กลุ่มข้อมูลการขอคืน'  ,openStartDate: new Date(),openStartDateEnd: new Date(),lastDate: new Date()},
    { dataGroupId: 4, dataGroupName: 'กลุ่มเฉพาะกิจ'  ,openStartDate: new Date(),openStartDateEnd: new Date(),lastDate: new Date()}
  ];
 
  private httpOptions = {};

  constructor(private http: HttpClient, private arsApiExtensionsService: ArsApiExtensionsService) {
    this.httpOptions = this.arsApiExtensionsService.getHttpOptionsJson();
  }

  getAll(): Observable<ResponseT<Mparam>> {
    return <Observable<ResponseT<Mparam>>>this.http.get(this.url + '/getAllSetting');
  }

  upadetSetting(mparam: Mparam): Observable<ResponseT<Mparam[]>> {
    return <Observable<ResponseT<Mparam[]>>>this.http.post(this.url + '/update', mparam);
  }

  public getTemplateData(param: any): Observable<ResponseT<any>> {
    return <Observable<ResponseT<any>>>this.http.post(this.templateUrl + '/getData', JSON.stringify(param), this.httpOptions);
  }

  public findAllMockData(): Observable<Array<any>> {
    return of(this.masterTable);
  }
 
}
