import { TestBed } from '@angular/core/testing';

import { ArsConfigService } from './ars-config.service';

describe('ArsConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArsConfigService = TestBed.get(ArsConfigService);
    expect(service).toBeTruthy();
  });
});
