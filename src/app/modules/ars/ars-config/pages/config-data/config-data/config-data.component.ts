import { Component, OnInit, HostListener, TemplateRef, ɵConsole } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ArsConfigService } from '../../../services/ars-config.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ResponseT } from 'src/app/shared/models/response.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { ArsExportService } from 'src/app/modules/ars/shared/services/ars-export.service';
import { ModalService } from 'src/app/shared/services/modal.service';

declare var $;

@Component({
  selector: 'app-config-data',
  templateUrl: './config-data.component.html',
  styleUrls: ['./config-data.component.css']
})
export class ConfigDataComponent implements OnInit {
  isCollapsed = false;
  modalRef: BsModalRef;
  ids: number[] = [];
  searchConditionFormGroup: FormGroup;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  masters: Array<any>;
  limit: Array<number> = this.ids;

  tableExcelList: Array<any> = [];


  constructor(private toastr: ToastrService,
    private actRoute: ActivatedRoute,
    private authenService: AuthenticationService,
    private arsConfigService: ArsConfigService,
    private modalService: BsModalService,
    private modalAlertService: ModalService,
    private arsExportService: ArsExportService,
    private router: Router,
    private fb: FormBuilder) { }


  initSearchCondition() {
    this.searchConditionFormGroup = new FormGroup({
      searchTemplateId: new FormControl(''),
      searchTemplateName: new FormControl(''),
      activeDateFrom: new FormControl(''),
      activeDateTo: new FormControl('')
    });
  }

  ngOnInit() {
    this.initSearchCondition();
    this.ids = [];
    this.pageRequest.sortFieldName = 'SEARCH_TEMPLATE_NAME';
    this.pageRequest.sortDirection = 'ASC';
    this.getData();
  }

  getData() {
    const criteria = this.searchConditionFormGroup.getRawValue();
          criteria.pageRequestDto = this.pageRequest;
      this.arsConfigService.getTemplateData(criteria).subscribe((res: ResponseT<any>) => {
      this.masters = res.data.content;
    });
  }

  addData() {
    this.router.navigate(['ars/config/data-template-detail']);
  }

  goToEdit(searchTemplateId) {
    this.router.navigate(['ars/config/data-template-detail', searchTemplateId]);
  }

  exportExcel() {
    if (this.tableExcelList.length === 0) {
      this.modalAlertService.openConfirmModal('แจ้งเตือน', 'กรุณาเลือกข้อมูลที่ต้องการ Export ');
      return;
    }
    const element = document.getElementById('excel-table');
    this.arsExportService.exportExcel(element, 'ARSP0110');
  }

  checkAll(ele) {
    this.ids = [];
    const checked = ele.target.checked;
    if (checked && this.masters) {
      this.ids = this.masters.map(column => column.searchTemplateId);
    } else {
      this.ids = [];
    }
    const elementList = $('input[type="checkbox"].requestFormSelected');
    for (let i = 0; i < elementList.length; i++) {
      elementList[i].checked = checked;
    }

    this.OnGenerateTableExcel();

  }

  toggleId(id) {
    if (this.ids.includes(id)) {
      this.ids = this.ids.filter(ids => ids !== id);
    } else {
      this.ids.push(id);
    }

    const elementList = $('#checkRowsAll');
    elementList[0].checked = this.ids.length === this.masters.length;

    this.OnGenerateTableExcel();
  }

  OnGenerateTableExcel() {
    debugger;
    this.tableExcelList = [];
    this.ids.forEach(id => {
      const tableRow = this.masters.find(r => r.searchTemplateId === id);
      if (tableRow) {
        this.tableExcelList.push(tableRow);
      }
    });
  }

  clear() {
    this.searchConditionFormGroup.reset();
    this.searchConditionFormGroup.markAsUntouched();
  }

  saveConfigData() {

  }

}
