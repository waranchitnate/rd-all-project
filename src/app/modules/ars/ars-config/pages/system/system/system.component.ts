import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder } from '@angular/forms';
import { Mparam } from '../../../models/m-param.model';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { ArsConfigService } from '../../../services/ars-config.service';
import {ModalService} from '../../../../../../shared/services/modal.service';

@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.css']
})
export class SystemComponent implements OnInit {
  isValid: boolean;
  allowMouseWheel = false;
  mparam: Mparam;
  isMeridian = false;
  showSpinners = false;
  minTime: Date = new Date();
  maxTime: Date = new Date();
  defaultTime: Date = new Date();
  formCondition: FormGroup;
  TimeEnd = false;
  getDate: Date;
  systemSubmit = false;
  getCurrentUserInfo: SsoUserModel;
  invalidHours = false;

  constructor(private router: Router,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute,
    private authenService: AuthenticationService,
    private fb: FormBuilder,
    private templateModalService: ModalService,
    private arsConfigService: ArsConfigService) {
    this.minTime.setHours(8);
    this.minTime.setMinutes(0);
    this.maxTime.setHours(17);
    this.maxTime.setMinutes(0);
    this.defaultTime.setHours(0);
    this.defaultTime.setMinutes(0);
  }


  initialFormCondition(mparam: Mparam, getCurrentUserInfo: SsoUserModel): FormGroup {
    return new FormGroup({
      defaultDownloadTimeStart: new FormControl(mparam.defaultDownloadTimeStart != null ? new Date(mparam.defaultDownloadTimeStart) : this.defaultTime, Validators.required),
      defaultDownloadTimeEnd: new FormControl(mparam.defaultDownloadTimeEnd != null ? new Date(mparam.defaultDownloadTimeEnd) : this.defaultTime),
      defaultCompressFileSize: new FormControl(mparam.defaultCompressFileSize != null ? mparam.defaultCompressFileSize : 0, Validators.required),
      defaultStartProcessTime: new FormControl(mparam.defaultStartProcessTime != null ? new Date(mparam.defaultStartProcessTime) : this.defaultTime, Validators.required),
      defaultFileExpiryDate: new FormControl(mparam.defaultFileExpiryDate != null ? mparam.defaultFileExpiryDate : 0, Validators.required),
      username: new FormControl(getCurrentUserInfo.username != null ? getCurrentUserInfo.username : '')
    });
  }

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.formCondition = this.initialFormCondition(new Mparam(), this.getCurrentUserInfo);
    this.arsConfigService.getAll().subscribe(res => {
      this.mparam = res.data;
      this.formCondition = this.initialFormCondition(this.mparam, this.getCurrentUserInfo);
    })
    this.TimeEnd = false;
  }

  get field() {
    return this.formCondition.controls;
  }

  minMaxTime(event) {

    var startDate = this.formCondition.get('defaultDownloadTimeStart').value != null ? this.formCondition.get('defaultDownloadTimeStart').value : new Date(this.defaultTime);
    var endDate = this.formCondition.get('defaultDownloadTimeEnd').value != null ? this.formCondition.get('defaultDownloadTimeEnd').value : new Date(this.defaultTime);

    if (event == null || event == "") {
      this.TimeEnd = false;
      return;
    } else {

      this.getDate = endDate;
      this.getDate.setDate(endDate.getDate());
      this.getDate.setHours(endDate.getHours());
      this.getDate.setMinutes(endDate.getMinutes());

      if ((startDate != null && startDate !== '') && event != null) {
        var starthours = startDate.getHours();
        var startMinutes = startDate.getMinutes();
        var endHours = this.getDate.getHours();
        var endMinutes = this.getDate.getMinutes();

        if ((starthours > 0) && starthours != endHours) {
          if (((endHours != 0) && (endHours < starthours)) || (endHours == 0) && (endMinutes > 0)) {
            this.TimeEnd = true;
            return;
          } else {
            this.TimeEnd = false;
            return;
          }
        } else if ((starthours == endHours) && (startMinutes > endMinutes)) {
          this.TimeEnd = true;
          return;
        } else {
          this.TimeEnd = false;
          return;
        }

      }

    }
  }

  saveSetting() {
    this.systemSubmit = true;
    if (this.formCondition.invalid || this.TimeEnd === true) {
      return;
    }

    this.arsConfigService.upadetSetting(this.formCondition.value).subscribe((result: any) => {
      this.showSuccess();
    },
      err => { this.showError(); });
  }

  showSuccess() {
    this.templateModalService.openModal('สำเร็จ', 'บันทึกข้อมูลสำเร็จ');
  }

  showError() {
    this.templateModalService.openModal('ไม่สำเร็จ', 'พบข้อผิดพลาดระหว่างบันทึกข้อมูล');
  }

  get defaultDownloadTimeStartControl(): AbstractControl { return this.formCondition.get('defaultDownloadTimeStart'); }
  get defaultDownloadTimeEndControl(): AbstractControl { return this.formCondition.get('defaultDownloadTimeEnd'); }
}
