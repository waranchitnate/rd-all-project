import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigDataGroupComponent } from './config-data-group.component';

describe('ConfigDataGroupComponent', () => {
  let component: ConfigDataGroupComponent;
  let fixture: ComponentFixture<ConfigDataGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigDataGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigDataGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
