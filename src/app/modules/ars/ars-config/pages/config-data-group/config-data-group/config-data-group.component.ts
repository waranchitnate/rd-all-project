import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { ArsConfigService } from '../../../services/ars-config.service';
import { ArsTemplateService } from 'src/app/modules/ars/shared/services/ars-template.service';
import { RequestFormModel, RequestFormColumnModel } from 'src/app/modules/ars/models/request-form.model';
import { ArsRequestFormService } from 'src/app/modules/ars/shared/services/ars-requestform.service';
import { SearchTemplateDetailModel } from 'src/app/modules/ars/models/search-template.model';

@Component({
  selector: 'app-config-data-group',
  templateUrl: './config-data-group.component.html',
  styleUrls: ['./config-data-group.component.css']
})
export class ConfigDataGroupComponent implements OnInit {
  loginUserInfo: any;
  searchTemplateId = this.actRoute.snapshot.paramMap.get('searchTemplateId');
  searchTemplateForm: FormGroup;
  requestFormDB: any;
  source: any[] = [];
  target: any[] = [];
  currentItems: Array<SearchTemplateDetailModel> = [];
  currentColumnList: Array<SearchTemplateDetailModel> = [];
  master: Array<any> = [];
  masterRowsCount: number;

  constructor(private toastr: ToastrService,
    private actRoute: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private arsConfigService: ArsConfigService,
    private arsRequestFormService: ArsRequestFormService,
    private arsTemplateService: ArsTemplateService,
    private _formBuilder: FormBuilder,
    private router: Router) { }


  ngOnInit() {
    this.initTemplateGroup();
    this.getData(this.searchTemplateId);
  }

  initTemplateGroup() {
    this.loginUserInfo = this.authenticationService.ssoUserDetailSnapshot;
    this.searchTemplateForm = this._formBuilder.group({
      searchTemplateName: new FormControl(''),
      queryString: new FormControl(''),
      startDate: new FormControl(''),
      expiredDate: new FormControl(''),
      searchTemplateDetails: this._formBuilder.array([])
    });
  }

  initColumnForm(item: RequestFormColumnModel) {
    const form = this._formBuilder.group({
      schemaName: new FormControl(item.schemaName),
      tableName: new FormControl(item.tableName),
      colName: new FormControl(item.colName),
      colDescTh: new FormControl(item.colDescTh),
      datatype: new FormControl(item.datatype),
      length: new FormControl(item.length),
      isPeriodSearch: new FormControl(item.isPeriodSearch),
      isSorting: new FormControl(item.isSorting),
      isSearchByList: new FormControl(item.isSearchByList),
      createdBy: new FormControl(this.loginUserInfo.username),
    });
    this.columnFormArray.push(form);
  }

  initSource(searchTemplateId: any) {
    this.arsRequestFormService.getSearchTemplateDetailByMasterId(searchTemplateId).subscribe((res: any) => {
      this.source = res.data;
      for (const item of this.target) {
        const index = this.source.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName);
        if (index >= 0) {
          this.source.splice(index, 1);
        }
      }
      this.onSelectedFields(this.target);
    });
  }

  initTable(searchTemplateDetails: Array<any>) {
    this.master = searchTemplateDetails.slice();
  }

  getData(searchTemplateId: any) {
    this.arsTemplateService.getSearchTemplateDetailBySearchTemplateId(searchTemplateId).subscribe(
      res => {
        this.requestFormDB = res.data;
        this.requestFormDB.queryString = `select * from ${this.requestFormDB.queryString}`;
        this.searchTemplateForm.patchValue(this.requestFormDB);
        if (this.requestFormDB !== undefined) {
          const columnList: Array<any> = this.requestFormDB.searchTemplateDetails;
          this.target = columnList.slice();
          this.initSource(this.searchTemplateId);
          for (const item of columnList) {
            this.initColumnForm(item);
          }
          this.initTable(columnList);
        }
      }
      , error => { }
    );
  }

  save() {

  }

  onSelectedFields(selectedItems: Array<any>) {
    if (this.currentItems < selectedItems) {
      this.addFormArray(selectedItems);
    } else {
      this.removeFormArray(selectedItems);
    }
  }

  addFormArray(selectedItems: Array<SearchTemplateDetailModel>) {
    this.currentItems = selectedItems;
    for (const item of selectedItems) {
      const foundItem = this.currentColumnList.find(obj => obj.colName === item.colName  && obj.tableName === item.tableName);
      if (foundItem) {
        continue;
      }
      this.currentColumnList.push(item);
      this.addColumnForm(item);
    }
  }

  addColumnForm(item: SearchTemplateDetailModel) {
    const form = this._formBuilder.group({
      schemaName: new FormControl(item.schemaName),
      tableName: new FormControl(item.tableName),
      colName: new FormControl(item.colName),
      colDescTh: new FormControl(item.colDescTh),
      datatype: new FormControl(item.datatype),
      length: new FormControl(item.length),
      isPeriodSearch: new FormControl(item.isPeriodSearch),
      isSorting: new FormControl(item.isSorting),
      isSearchByList: new FormControl(item.isSearchByList),
      createdBy: new FormControl(this.loginUserInfo.username),
    });
    this.columnFormArray.push(form);
  }

  removeFormArray(selectedItems: Array<SearchTemplateDetailModel>) {
    const removeList = this.currentItems.filter(current => selectedItems.indexOf(current) < 0);
    for (const item of removeList) {
      this.removeColumnForm(item);
      if (item.isSorting) {

      }
      if (item.isPeriodSearch || item.isSearchByList) {

      }
    }
    this.currentItems = this.currentItems.filter(currentItem => !removeList.includes(currentItem));
  }


  removeColumnForm(item: SearchTemplateDetailModel) {
    this.currentColumnList = this.removeFromList(this.currentColumnList, item);
    const index = this.columnFormArray.value.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName);
    if (index < 0) {
      return;
    }
    console.log('removeColumnForm', index, this.columnFormArray.value);
    this.columnFormArray.removeAt(index);
    console.log('removeColumnForm success', index, this.columnFormArray.value);
  }

  removeFromList(list: Array<any>, removeItem: SearchTemplateDetailModel) {
    const index = list.findIndex(obj => obj.colName === removeItem.colName && obj.tableName === removeItem.tableName);
    if (index >= 0) {
      list.splice(index, 1);
    }
    return list;
    // return list.filter(item => item.colName !== item.colName && item.tableName !== item.tableName);
  }

  toggleSearchByList(ele, colName, tableName) {
    if (ele.checked) {
      this.master.forEach(r => r.isSearchByList = 0);
      console.log('test', this.master);
      this.master.find(r => r.colName === colName && r.tableName === tableName).isSearchByList = 1;
    }
  }

  backPage(){
    console.log('this.searchTemplateForm', this.searchTemplateForm.value);
    this.router.navigate(['ars/config/data-template']);
  }

  get columnFormArray(): FormArray {
    return this.searchTemplateForm.get('searchTemplateDetails') as FormArray;
  }
}
