import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArsURL } from '../ars.url';
import { SystemComponent } from './pages/system/system/system.component';
import { ConfigDataComponent } from './pages/config-data/config-data/config-data.component';
import { ConfigDataGroupComponent } from './pages/config-data-group/config-data-group/config-data-group.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: ArsURL.config.system},
  {path: ArsURL.config.system, component: SystemComponent},
  {path: ArsURL.config.data, component: ConfigDataComponent},
  {path: ArsURL.config.group, component: ConfigDataGroupComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArsConfigRoutingModule { }
