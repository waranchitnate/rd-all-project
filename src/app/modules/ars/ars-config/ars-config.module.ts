import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArsConfigRoutingModule } from './ars-config-routing.module';
import { SystemComponent } from './pages/system/system/system.component';
import { ConfigDataComponent } from './pages/config-data/config-data/config-data.component';
import { ConfigDataGroupComponent } from './pages/config-data-group/config-data-group/config-data-group.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ArsCommonModule } from '../shared/ars-common.modules';

@NgModule({
  declarations: [SystemComponent, ConfigDataComponent, ConfigDataGroupComponent],
  imports: [
    CommonModule,
    ArsConfigRoutingModule,
    SharedModule,
    ArsCommonModule
  ]
})
export class ArsConfigModule { }
