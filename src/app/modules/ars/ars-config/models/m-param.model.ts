export class Mparam {
    defaultDownloadTimeStart: Date;
    defaultDownloadTimeEnd: Date;
    defaultCompressFileSize: number;
    defaultStartProcessTime: Date;
    defaultFileExpiryDate: number;
}