import {SearchTemplateModel} from './search-template.model';
import {SearchTemplateDetailModel} from './search-template-detail.model';

export interface RequestFormModel {
  requestId?: number;
  userRequestUsername?: string;
  userRequestName?: string;
  formTitle?: string;
  formDesc?: string;
  searchTemplate?: SearchTemplateModel;
  statusCode?: string;
  rejectReason?: string;
  currentApproved?: number;
  totalApproved?: number;
  limitRow?: number;
  fileType?: string;
  sizePerFile?: number;
  totalFile?: number;
  filePrefix?: number;
  expiredDate?: Date;
  isProcessSuccess?: number;
  reqHistoryRefNo?: string;
  arsRunningNo?: string;
  requestedDate?: Date;
  userOfficeCode?: string;
  userRdOfficeCode?: string;
  userRank?: string;
  analystProceedDate?: Date;
  processDateStart?: Date;
  processDateFinished?: Date;
  errorLog?: string;
  createdBy?: string;
  createdDate?: Date;
  upDatedBy?: string;
  upDatedDate?: Date;
  requestApprovalLevels?: any;
  requestFormColumns?: Array<RequestFormColumnModel>;
  requestFormCriterias?: Array<RequestFormCriteriaModel>;
  requestFormSortings?: Array<RequestFormSortingModel>;
}

export interface RequestFormColumnModel extends SearchTemplateDetailModel {
  requestFormColumnId?: number;
  createdBy?: string;
  createdDate?: Date;
  upDatedBy?: string;
  upDatedDate?: Date;
}

export interface RequestFormCriteriaModel extends SearchTemplateDetailModel {
  requestFormCriteriaId?: number;
  colName?: string;
  createdBy?: string;
  createdDate?: Date;
  datatype?: string;
  filePath?: string;
  upDatedBy?: string;
  upDatedDate?: Date;
  criteriaValue?: string;
  periodDateFrom?: Date;
  periodDateTo?: Date;
  colDescTh?: string;
  isSearchByList?: boolean;
  isPeriodSearch?: boolean;
}

export interface RequestFormSortingModel extends SearchTemplateDetailModel {
  requestFormSortingId?: number;
  isSorting?: boolean;
  sortingSeq?: number;
  sortingType?: string;
  colDescTh?: string;
  createdBy?: string;
  createdDate?: Date;
  updatedBy?: string;
  updatedDate?: Date;
}


