export interface ArsDropdownModel {
  id?: number;
  code?: string;
  name?: string;
  value?: string;
}
