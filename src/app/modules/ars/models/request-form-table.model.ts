export interface RequestFormTableModel {
  requestFormId: number;
  arsRunningNo: string;
  formTitle: string;
  formDesc: string;
  userOfficeCode: string;
  searchTemplateId: number;
  searchTemplateName: string;
  currentApproved: number;
  totalApproved: number;
  createdDate: Date;
  approvedDate: Date;
  statusCode: string;
  statusName: string;
  analystProceedDate: Date;
  processDateStart: Date;
  processDateFinished: Date;
  requestedDate: Date;
  firstApprovedDate: Date;
  secondApprovedDate: Date;
  firstApprovedResult: string;
  secondApprovedResult: string;
  errorLog: string;
}
