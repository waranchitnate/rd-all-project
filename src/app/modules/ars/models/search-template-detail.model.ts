export interface SearchTemplateDetailModel {
  searchTemplateDetailId?: number;
  colName?: string;
  colDescTh?: string;
  createdBy?: string;
  createdDate?: Date;
  datatype?: string;
  length?: number;
  isPeriodSearch?: boolean;
  isSearchByList?: boolean;
  isSorting?: boolean;
  schemaName?: string;
  tableName?: string;
}
