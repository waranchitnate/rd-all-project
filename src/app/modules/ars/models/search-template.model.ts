export interface SearchTemplateModel {
  searchTemplateId?: number;
  createdBy?: string;
  createdDate?: Date;
  querystring?;
  string;
  searchTemplateName?: string;
  status?: number;
  updatedBy?: string;
  updatedDate?: Date;
  startDate?: Date;
  expiredDate?: Date;
  schemaName?: string;
  tableName?: string;
  searchTemplateDetails?: Array<SearchTemplateDetailModel>;
}

export interface SearchTemplateDetailModel {
  searchTemplateDetailId?: number;
  colName?: string;
  colDescTh?: string;
  createdBy?: string;
  createdDate?: Date;
  datatype?: string;
  length?: number;
  isPeriodSearch?: number;
  isSearchByList?: number;
  isSorting?: number;
  schemaName?: string;
  tableName?: string;
  upDatedBy?: string;
  upDatedDate?: Date;
}
