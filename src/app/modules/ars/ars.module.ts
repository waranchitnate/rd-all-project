import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArsRoutingModule } from './ars-routing.module';
import {ArsAnalystModule} from './ars-analyst/ars-analyst.module';
import {ArsUserModule} from './ars-user/ars-user.module';
import {ArsApprovalModule} from './ars-approval/ars-approval.module';
import {ArsConfigModule} from './ars-config/ars-config.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ArsRoutingModule,
    ArsAnalystModule,
    ArsUserModule,
    ArsApprovalModule,
    ArsConfigModule,
    RouterModule,
    SharedModule,
    ReactiveFormsModule,
    UiSwitchModule,
    FormsModule
  ]
})
export class ArsModule { }
