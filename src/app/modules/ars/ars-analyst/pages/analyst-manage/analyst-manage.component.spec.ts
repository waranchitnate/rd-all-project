import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalystManageComponent } from './analyst-manage.component';

describe('AnalystManageComponent', () => {
  let component: AnalystManageComponent;
  let fixture: ComponentFixture<AnalystManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalystManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalystManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
