import {RequestFormCriteriaModel, RequestFormModel} from '../../../models/request-form.model';
import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {ArsAnalystService} from '../../services/ars-analyst.service';
import {ArsRequestFormService} from '../../../shared/services/ars-requestform.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../shared/modals/answer-modal/answer-modal.component';
import {ModalService} from '../../../../../shared/services/modal.service';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../../../../core/authentication/authentication.service';
import {SsoUserModel} from '../../../../../core/models/sso-user.model';
import { ArsCommonService } from '../../../shared/services/ars-common.service';

@Component({
  selector: 'app-analyst-manage',
  templateUrl: './analyst-manage.component.html',
  styleUrls: ['./analyst-manage.component.css']
})
export class AnalystManageComponent implements OnInit {
  configExecuteBatchFormGroup: FormGroup;

  requestFormId = this.actRoute.snapshot.paramMap.get('requestFormId');
  requestForm: RequestFormModel;
  isCollapsed = false;
  formTitle: string;
  masters: Array<any>;
  requestFormCriterias: Array<RequestFormCriteriaModel>;
  mastersSorting: Array<any>;

  loginUserInfo: SsoUserModel;

  modalRef: BsModalRef;
  radio = true;

  proceedTimeHH: Date;
  proceedTimeMM: Date;
  defaultProceedTime: Date;

  constructor(private arsAnalystService: ArsAnalystService
    , private arsRequestFormService: ArsRequestFormService
    , private authenticationService: AuthenticationService
    , private analyzeService: ArsAnalystService
    , private modalService: BsModalService
    , private templateModalService: ModalService
    , public arsCommonService: ArsCommonService
    , private actRoute: ActivatedRoute
    , private _formBuilder: FormBuilder
    , private router: Router) {
  }

  ngOnInit() {
    this.loginUserInfo = this.authenticationService.ssoUserDetailSnapshot;
    this.getData(this.requestFormId);
    this.initFormGroup();
    this.getDefaultProceedTime();
  }

  initFormGroup() {
    this.configExecuteBatchFormGroup = this._formBuilder.group( {
      proceedType: new FormControl('DEFAULT'),
      proceedTimeHH: new FormControl('', [Validators.min(0), Validators.max(23)]),
      proceedTimeMM: new FormControl('', [Validators.min(0), Validators.max(59)]),
      proceedTime: new FormControl(''),
      defaultProceedTime: new FormControl('')
      });
  }

  isInvalidAndTouched(myControl: AbstractControl) {
    return myControl.invalid && (myControl.dirty || myControl.touched);
  }

  get proceedTimeHHControl(): AbstractControl { return this.configExecuteBatchFormGroup.get('proceedTimeHH'); }
  get proceedTimeMMControl(): AbstractControl { return this.configExecuteBatchFormGroup.get('proceedTimeMM'); }

  getDefaultProceedTime() {
    this.analyzeService.getDefaultProceedTime().subscribe((res: any) => {
      this.configExecuteBatchFormGroup.get('defaultProceedTime').setValue(res.data);
    });
  }

  getData(requestFormId) {
    this.arsRequestFormService.getRequestFormByRequestId(requestFormId).subscribe((res: any) => {
      this.requestForm = res.data;
      this.formTitle = this.requestForm.formTitle;
      this.masters = this.requestForm.requestFormColumns;
      this.requestFormCriterias = this.requestForm.requestFormCriterias;
      this.mastersSorting = this.requestForm
        .requestFormSortings
        .filter(r => r.hasOwnProperty('sortingType')).filter(r => r.sortingType);
    });
  }

  process(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-dialog-centered'
    });
  }

  rejectRequest() {
    const modalRef = this.templateModalService.openConfirmModal('ยืนยันการดำเนินการ', 'ท่านต้องการยืนยันยุติการประมวลผลใช่หรือไม่');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        this.templateModalService.openModal('สำเร็จ', 'บันทึกผลการพิจารณายุติประมวลผลสำเร็จ');
      }
    });
  }

  confirmConfig() {
    if (this.configExecuteBatchFormGroup.valid) {
      const criteria = this.configExecuteBatchFormGroup.getRawValue();
      if (criteria.proceedType === 'CUSTOM') {
        const hoursVal = this.proceedTimeHHControl.value;
        const minuteVal = this.proceedTimeMMControl.value;
        if (hoursVal === '' || hoursVal === undefined || hoursVal === null) {
          this.proceedTimeHHControl.setErrors({'required': true});
          return ;
        }
        if (minuteVal === '' || minuteVal === undefined || minuteVal === null) {
          this.proceedTimeMMControl.setErrors({'required': true});
          return ;
        }
        criteria.proceedTime = this.proceedTimeHHControl.value + ':' + this.proceedTimeMMControl.value;
      } else if (criteria.proceedType === 'DEFAULT'){
          criteria.proceedTime = this.defaultProceedTime;
      }
      criteria.analystUsername = this.loginUserInfo.username;
      criteria.analystName = this.loginUserInfo.nameTH + ' ' + this.loginUserInfo.surNameTH;
      criteria.requestFormId = this.requestForm.requestId;

      this.analyzeService.saveConfigExecuteBatch(criteria).subscribe(result => {
        if (result) {
          this.templateModalService.openModal('สำเร็จ', 'บันทึกการตั้งค่าประมวลผลสำเร็จ');
          this.goHome();
          this.modalRef.hide();
        }
      });
    }
  }

  goHome() {
    this.router.navigate(['/ars/analyst/adhoc/search']);
  }

  dismissModal(){
    this.modalRef.hide();
  }
}
