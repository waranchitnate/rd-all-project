import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalystSearchComponent } from './analyst-search.component';

describe('AnalystSearchComponent', () => {
  let component: AnalystSearchComponent;
  let fixture: ComponentFixture<AnalystSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalystSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalystSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
