import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArsURL } from '../ars.url';
import { AnalystSearchComponent } from './pages/analyst-search/analyst-search.component';
import { AnalystManageComponent } from './pages/analyst-manage/analyst-manage.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: ArsURL.analyst.search},
  {path: ArsURL.analyst.search, component: AnalystSearchComponent},
  {path: ArsURL.analyst.manage, component: AnalystManageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArsAnalystRoutingModule { }
