import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ArsApiExtensionsService } from '../../shared/services/ars-apiextension.service';
import { PageResponse } from 'src/app/shared/models/page-response';
import { ResponseT } from 'src/app/shared/models/response.model';

@Injectable({
  providedIn: 'root'
})

export class ArsAnalystService {

  private analyzeRestUrl = environment.arsApiUrl + 'manageAnalyze/';
  private httpOptions = {};

  constructor(private http: HttpClient, private arsApiExtensionsService: ArsApiExtensionsService) {
    this.httpOptions = this.arsApiExtensionsService.getHttpOptionsJson();
  }

  public getDefaultProceedTime(): Observable<ResponseT<any>> {
    return <Observable<ResponseT<any>>>this.http.get(this.analyzeRestUrl + 'getDefaultProceedTime', this.httpOptions);
  }

  public saveConfigExecuteBatch(saveObj: any): Observable<ResponseT<any>> {
    return <Observable<ResponseT<any>>>this.http.post(this.analyzeRestUrl + 'configExecuteBatch', saveObj, this.httpOptions);
  }




}
