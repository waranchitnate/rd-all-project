import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArsAnalystRoutingModule } from './ars-analyst-routing.module';
import { AnalystSearchComponent } from './pages/analyst-search/analyst-search.component';
import { AnalystManageComponent } from './pages/analyst-manage/analyst-manage.component';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  declarations: [AnalystSearchComponent, AnalystManageComponent],
  imports: [
    CommonModule,
    ArsAnalystRoutingModule,
    SharedModule
  ]
})
export class ArsAnalystModule { }
