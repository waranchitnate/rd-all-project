import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import { ResponseT } from 'src/app/shared/models/response.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import {VUserCert} from '../../../certificate/models/vusercert';
import {RequestFormTableModel} from '../../models/request-form-table.model';
import { ArsApiExtensionsService } from '../../shared/services/ars-apiextension.service';

@Injectable({
  providedIn: 'root'
})

export class ArsUserService {
  private requestFormUrl = environment.arsApiUrl + 'requestForm/';
  private commonUrl = environment.arsApiUrl + 'common/';
  private httpOptions = {};

  constructor(private http: HttpClient, private arsApiExtensionsService: ArsApiExtensionsService) {
    this.httpOptions = this.arsApiExtensionsService.getHttpOptionsJson();
  }


  public getAllActiveSearchTemplateDropdown() {
    return this.http.get<Array<any>>(this.commonUrl + 'getAllActiveSearchTemplateDropdown', this.httpOptions);
  }

  public getAllStatusDropdown() {
    return this.http.get<Array<any>>(this.commonUrl + 'getAllStatusDropdown', this.httpOptions);
  }

  public getSearchTemplateDetailByMasterId(id: number) {
    return this.http.post(this.requestFormUrl + 'getSearchTemplateDetailByMasterId', id, this.httpOptions);
  }

  public saveDraft(requestForm: any) {
    return this.http.post(this.requestFormUrl + 'saveDraft', requestForm, this.httpOptions);
  }

  public sendApprove(requestForm: any) {
    return this.http.post(this.requestFormUrl + 'sendApprove', requestForm, this.httpOptions);
  }

  public getRequestFormByCriteria(criteria: any): Observable<ResponseT<PageResponse<RequestFormTableModel>>> {
    return <Observable<ResponseT<PageResponse<RequestFormTableModel>>>>this.http.post(this.requestFormUrl + 'getRequestFormByCondition', criteria);
  }

}

