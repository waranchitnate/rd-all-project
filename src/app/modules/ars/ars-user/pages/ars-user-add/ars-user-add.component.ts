import {Component, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SearchTemplateDetailModel} from '../../../models/search-template-detail.model';
import {
  RequestFormColumnModel,
  RequestFormCriteriaModel,
  RequestFormModel,
  RequestFormSortingModel
} from '../../../models/request-form.model';
import {SsoUserModel} from '../../../../../core/models/sso-user.model';
import {ArsDropdownModel} from '../../../models/ars-dropdown.model';
import {MoveitemComponent} from '../../../shared/components/moveitem/moveitem.component';
import {ActivatedRoute, Router} from '@angular/router';
import {ArsRequestFormService} from '../../../shared/services/ars-requestform.service';
import {ArsUserService} from '../../services/ars-user.service';
import {ModalService} from '../../../../../shared/services/modal.service';
import {AuthenticationService} from '../../../../../core/authentication/authentication.service';

@Component({
  selector: 'app-ars-user-add',
  templateUrl: './ars-user-add.component.html',
  styleUrls: ['./ars-user-add.component.css']
})
export class ArsUserAddComponent implements OnInit {

  requestFormGroup: FormGroup;
  currentItems: Array<SearchTemplateDetailModel> = [];
  currentCriteriaList: Array<SearchTemplateDetailModel> = [];
  currentSortingList: Array<SearchTemplateDetailModel> = [];
  currentColumnList: Array<SearchTemplateDetailModel> = [];

  requestFormDB: RequestFormModel;

  loginUserInfo: SsoUserModel;
  fileUpload: File;

  tab = 1;
  key: string;
  display: any;
  filter = false;
  source: Array<any>;
  target: Array<any>;
  disabled = false;

  searchTemplateDropdown: Array<ArsDropdownModel>;

  @ViewChild(MoveitemComponent) moveItemComponent: MoveitemComponent;

  constructor(private router: Router,
              private _formBuilder: FormBuilder,
              private actRoute: ActivatedRoute,
              private requestFormService: ArsRequestFormService,
              private arsUserService: ArsUserService,
              private templateModalService: ModalService,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.initDropDown();
    this.initRequestFormGroup();
    this.OnSearchTemplateIdChanged();
  }

  initDropDown() {
    this.arsUserService.getAllActiveSearchTemplateDropdown().subscribe((res: any) => {
      this.searchTemplateDropdown = res.data;
      this.searchTemplateIdControl.setValue(this.searchTemplateDropdown[0].id);
    });
  }

  initRequestFormGroup() {
    this.loginUserInfo = this.authenticationService.ssoUserDetailSnapshot;
    this.requestFormGroup = this._formBuilder.group({
      requestId: new FormControl(''),
      searchTemplate: this._formBuilder.group({ searchTemplateId: new FormControl('', Validators.required)}),
      formTitle: new FormControl('', Validators.required),
      formDesc:  new FormControl(''),
      fileType: new FormControl('TXT', Validators.required),
      cancelReason: new FormControl(''),
      userOfficeCode: new FormControl(this.loginUserInfo.userOfficeCode),
      userRequestUsername: new FormControl(this.loginUserInfo.username),
      userRequestName: new FormControl(this.loginUserInfo.nameTH.concat(' ').concat(this.loginUserInfo.surNameTH)),
      createdBy: new FormControl(this.loginUserInfo.username),
      requestFormCriterias: this._formBuilder.array([]),
      requestFormSortings:  this._formBuilder.array([]),
      requestFormColumns:   this._formBuilder.array([])
    });
  }

  OnSearchTemplateIdChanged() {
    this.searchTemplateIdControl.valueChanges.subscribe(val => {
      this.arsUserService.getSearchTemplateDetailByMasterId(val).subscribe((res: any) => {
        this.source = res.data;
        this.moveItemComponent.selectedItems = [];
        this.onSelectedFields(this.moveItemComponent.selectedItems);
      });
    });
  }

  isInvalidAndTouched(myControl: AbstractControl) {
    return myControl.invalid && (myControl.dirty || myControl.touched);
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  onSelectedFields(selectedItems: Array<SearchTemplateDetailModel>) {
    if (this.currentItems < selectedItems) {
      this.addFormArray(selectedItems);
    } else {
      this.removeFormArray(selectedItems);
    }
    console.log(this.currentItems);
    console.log(this.currentColumnList);
    console.log(this.currentSortingList);
    console.log(this.currentCriteriaList);
    console.log('----------------------');
  }

  addFormArray(selectedItems: Array<SearchTemplateDetailModel>) {
    this.currentItems = selectedItems;
    for (const item of selectedItems) {
      const foundItem = this.currentColumnList.find(obj => obj.colName === item.colName  && obj.tableName === item.tableName);
      if (foundItem) {
        continue;
      }
      this.currentColumnList.push(item);
      this.addColumnForm(item);
      if (item.isSorting) {
          this.addSortingForm(item);
      }
      if (item.isPeriodSearch || item.isSearchByList) {
          this.addCriteriaForm(item);
      }
    }

    //   if (this.currentColumnList.filter(obj => obj.colName === item.colName).length === 0) {
    //     this.addColumnForm(item);
    //   }
    //   if (item.isSorting) {
    //     if (this.currentSortingList.filter(obj => obj.colName === item.colName).length === 0) {
    //       this.currentSortingList.push(item);
    //       this.addSortingForm(item);
    //     }
    //   }
    //   if (item.isPeriodSearch || item.isSearchByList) {
    //     if (this.currentCriteriaList.filter(obj => obj.colName === item.colName).length === 0) {
    //       this.currentCriteriaList.push(item);
    //       this.addCriteriaForm(item);
    //     }
    //   }
    // }
  }

  addColumnForm(item: SearchTemplateDetailModel) {
    const form = this._formBuilder.group({
      schemaName: new FormControl(item.schemaName),
      tableName: new FormControl(item.tableName),
      colName: new FormControl(item.colName),
      colDescTh: new FormControl(item.colDescTh),
      datatype: new FormControl(item.datatype),
      length: new FormControl(item.length),
      isPeriodSearch: new FormControl(item.isPeriodSearch),
      isSorting: new FormControl(item.isSorting),
      isSearchByList: new FormControl(item.isSearchByList),
      createdBy: new FormControl(this.loginUserInfo.username)
    });
    this.columnFormArray.push(form);
  }

  addCriteriaForm(item: SearchTemplateDetailModel) {
    const form = this._formBuilder.group({
      schemaName: new FormControl(item.schemaName),
      tableName: new FormControl(item.tableName),
      colName: new FormControl(item.colName),
      colDescTh: new FormControl({value: item.colDescTh, disabled: true}),
      datatype: new FormControl(item.datatype),
      criteriaValue: new FormControl(''),
      filePath: new FormControl(''),
      fileName: new FormControl(''),
      periodDateFrom: new FormControl(''),
      periodDateTo: new FormControl(''),
      isPeriodSearch: new FormControl(item.isPeriodSearch),
      isSearchByList: new FormControl(item.isSearchByList),
      createdBy: new FormControl(this.loginUserInfo.username),
    });
    this.criteriaFormArray.push(form);
  }

  addSortingForm(item: SearchTemplateDetailModel) {
    const form = this._formBuilder.group({
      schemaName: new FormControl(item.schemaName),
      tableName: new FormControl(item.tableName),
      colName: new FormControl(item.colName),
      colDescTh: new FormControl({value: item.colDescTh, disabled: true}),
      datatype: new FormControl(item.datatype),
      sortingType: new FormControl(''),
      isShowSorting: new FormControl(item.isSorting),
      createdBy: new FormControl(this.loginUserInfo.username),
      userRequestEmail: new FormControl(this.loginUserInfo.email)
    });
    this.sortingFormArray.push(form);
  }

  removeFormArray(selectedItems: Array<SearchTemplateDetailModel>) {
    const removeList = this.currentItems.filter(current => selectedItems.indexOf(current) < 0);
    for (const item of removeList) {
      this.removeColumnForm(item);
      if (item.isSorting) {
        this.removeSortingForm(item);
      }
      if (item.isPeriodSearch || item.isSearchByList) {
        this.removeCriteriaForm(item);
      }
    }
    this.currentItems = this.currentItems.filter(currentItem => !removeList.includes(currentItem));
  }

  removeSortingForm(item: SearchTemplateDetailModel) {
    this.currentSortingList  = this.removeFromList(this.currentSortingList, item);
    this.sortingFormArray.removeAt(this.sortingFormArray.value.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName));
  }

  removeColumnForm(item: SearchTemplateDetailModel) {
    this.currentColumnList = this.removeFromList(this.currentColumnList, item);
    this.columnFormArray.removeAt(this.columnFormArray.value.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName));
  }

  removeCriteriaForm(item: any) {
    this.currentCriteriaList = this.removeFromList(this.currentCriteriaList, item);
    this.criteriaFormArray.removeAt(this.criteriaFormArray.value.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName));
  }

  removeFromList(list: Array<any>, removeItem: SearchTemplateDetailModel) {
    return list.filter(item => item.colName !== item.colName && item.tableName !== item.tableName);
  }

  setFileName(e, i) {
    // @ts-ignore
    this.fileUpload = <File> event.target.files[0];
    this.fileControl(i).setValue(this.fileUpload);
    this.fileNameControl(i).setValue(this.fileUpload.name);
  }

  removeAttachmentForm(i) {
    this.fileControl(i).setValue(null);
    this.fileNameControl(i).setValue(null);
  }

  get criteriaFormArray(): FormArray {
    return this.requestFormGroup.get('requestFormCriterias') as FormArray;
  }

  get sortingFormArray(): FormArray {
    return this.requestFormGroup.get('requestFormSortings') as FormArray;
  }

  get columnFormArray(): FormArray {
    return this.requestFormGroup.get('requestFormColumns') as FormArray;
  }

  get formTitleControl(): AbstractControl {
    return this.requestFormGroup.get('formTitle');
  }

  get searchTemplateIdControl(): AbstractControl {
    return this.requestFormGroup.get('searchTemplate.searchTemplateId');
  }

  get fileTypeControl(): AbstractControl {
    return this.requestFormGroup.get('fileType');
  }

  isPeriodSearch(i):  boolean { return this.criteriaFormArray.at(i).get('isPeriodSearch').value; }
  isSearchByList(i):  boolean { return this.criteriaFormArray.at(i).get('isSearchByList').value; }
  fileNameControl(i): AbstractControl { return this.criteriaFormArray.at(i).get('fileName'); }
  fileControl(i):     AbstractControl { return this.criteriaFormArray.at(i).get('filePath'); }

  isFormValid(): boolean {
    this.markFormGroupTouched(this.requestFormGroup);
    return this.requestFormGroup.valid;
  }

  saveDraft() {
    if (this.isFormValid()) {
      this.arsUserService.saveDraft(this.requestFormGroup.getRawValue()).subscribe(
        res => {
          this.templateModalService.openModal('สำเร็จ', 'บันทึกร่างสำเร็จ');
          this.goHome();
        }, error => {
          this.templateModalService.openModal('ไม่สำเร็จ', 'พบข้อผิดพลาดระหว่างบันทึกข้อมูล');
        });
    }
  }

  sendApprove() {
    if (this.isFormValid()) {
      this.arsUserService.sendApprove(this.requestFormGroup.getRawValue()).subscribe(
        res => {
          this.templateModalService.openModal('สำเร็จ', 'บันทึกข้อมูลและส่งพิจารณาอนุมัติสำเร็จ');
          this.goHome();
        }, error => {
          this.templateModalService.openModal('ไม่สำเร็จ', 'พบข้อผิดพลาดระหว่างบันทึกข้อมูล');
        });
    }
  }

  clear() {
    // this.requestFormGroup.reset();
  }

  goHome() {
    this.router.navigate(['/ars/user/adhoc/search']);
  }

}
