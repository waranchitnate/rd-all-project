import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SearchTemplateDetailModel } from '../../../models/search-template-detail.model';
import {
  RequestFormColumnModel,
  RequestFormCriteriaModel,
  RequestFormModel,
  RequestFormSortingModel
} from '../../../models/request-form.model';
import { SsoUserModel } from '../../../../../core/models/sso-user.model';
import { ArsDropdownModel } from '../../../models/ars-dropdown.model';
import { MoveitemComponent } from '../../../shared/components/moveitem/moveitem.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ArsRequestFormService } from '../../../shared/services/ars-requestform.service';
import { ArsUserService } from '../../services/ars-user.service';
import { ModalService } from '../../../../../shared/services/modal.service';
import { AuthenticationService } from '../../../../../core/authentication/authentication.service';
import { pairwise, startWith } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-ars-user-edit',
  templateUrl: './ars-user-edit.component.html',
  styleUrls: ['./ars-user-edit.component.css']
})
export class ArsUserEditComponent implements OnInit {
  requestFormGroup: FormGroup;
  currentItems: Array<SearchTemplateDetailModel> = [];
  currentCriteriaList: Array<SearchTemplateDetailModel> = [];
  currentSortingList: Array<SearchTemplateDetailModel> = [];
  currentColumnList: Array<SearchTemplateDetailModel> = [];

  requestFormDB: RequestFormModel;

  loginUserInfo: SsoUserModel;

  fileUpload: File;
  requestFormId = this.actRoute.snapshot.paramMap.get('requestFormId'); //this.actRoute.snapshot.queryParamMap.get('requestFormId');

  tab = 1;
  key: string;
  display: any;
  filter = false;
  source: Array<any>;
  target: Array<any>;
  disabled = false;

  searchTemplateDropdown: Array<ArsDropdownModel>;
  // searchTemplateId: number;

  @ViewChild(MoveitemComponent) moveItemComponent: MoveitemComponent;

  constructor(private router: Router,
    private _formBuilder: FormBuilder,
    private actRoute: ActivatedRoute,
    private requestFormService: ArsRequestFormService,
    private arsUserService: ArsUserService,
    private templateModalService: ModalService,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.initDropDown();
    this.initRequestFormGroup();
    // this.OnSearchTemplateIdChanged(null);
    if (this.requestFormId != null) {
      this.getData(this.requestFormId);
    }
  }

  initDropDown() {
    this.arsUserService.getAllActiveSearchTemplateDropdown().subscribe((res: any) => {
      this.searchTemplateDropdown = res.data;
      if (this.requestFormId === null) {
        this.searchTemplateIdControl.setValue(this.searchTemplateDropdown[0].id);
      }
    });
  }

  initRequestFormGroup() {
    this.loginUserInfo = this.authenticationService.ssoUserDetailSnapshot;
    this.requestFormGroup = this._formBuilder.group({
      requestId: new FormControl(''),
      searchTemplate: this._formBuilder.group({ searchTemplateId: new FormControl('', Validators.required) }),
      formTitle: new FormControl('', Validators.required),
      formDesc: new FormControl(''),
      fileType: new FormControl('TXT', Validators.required),
      cancelReason: new FormControl(''),
      userOfficeCode: new FormControl(this.loginUserInfo.userOfficeCode),
      userRequestUsername: new FormControl(this.loginUserInfo.username),
      userRequestName: new FormControl(this.loginUserInfo.nameTH.concat(' ').concat(this.loginUserInfo.surNameTH)),
      createdBy: new FormControl(this.loginUserInfo.username),
      userRequestEmail: new FormControl(this.loginUserInfo.email),
      reqHistoryRefNo: new FormControl(''),
      arsRunningNo: new FormControl(''),
      requestFormCriterias: this._formBuilder.array([]),
      requestFormSortings: this._formBuilder.array([]),
      requestFormColumns: this._formBuilder.array([])
    });
  }

  OnSearchTemplateIdChanged(searchTemplateId: number) {
    this.searchTemplateIdControl.valueChanges.pipe(startWith(searchTemplateId), pairwise()).subscribe(([prev, next]: [any, any]) => {
      this.arsUserService.getSearchTemplateDetailByMasterId(next).subscribe((res: any) => {
        this.source = res.data;
        if (prev !== next) {
          this.target = [];
        }
        this.onSelectedFields(this.target);
        // this.moveItemComponent.selectedItems = [];
        // this.onSelectedFields(this.moveItemComponent.selectedItems);
      });

    });
  }

  initSource(searchTemplateId: number) {
    this.arsUserService.getSearchTemplateDetailByMasterId(searchTemplateId).subscribe((res: any) => {
      this.source = res.data;
      for (const item of this.target) {
        const index = this.source.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName);
        if (index >= 0) {
          this.source.splice(index, 1);
        }
      }
      this.onSelectedFields(this.target);
      // this.moveItemComponent.selectedItems = [];
      // this.onSelectedFields(this.moveItemComponent.selectedItems);
    });
  }

  getData(requestFormId: any) {
    this.requestFormService.getRequestFormByRequestId(requestFormId).subscribe(
      res => {
        this.requestFormDB = res.data;
        this.requestFormGroup.patchValue(this.requestFormDB);
        if (this.requestFormDB !== undefined) {
          const columnList: Array<SearchTemplateDetailModel> = this.requestFormDB.requestFormColumns;
          const searchTemplateId = this.requestFormDB.searchTemplate.searchTemplateId;
          this.target = columnList.slice();
          this.OnSearchTemplateIdChanged(searchTemplateId);
          this.initSource(searchTemplateId);
          // SOS
          // this.target = columnList.slice();
          // this.moveItemComponent.selectedItems.push(columnList);
          // SOS
          this.currentItems = this.target;
          this.currentColumnList = this.target;
          for (const item of columnList) {
            this.initColumnForm(item);
          }

          const sortingList: Array<RequestFormSortingModel> = this.requestFormDB.requestFormSortings;
          this.currentSortingList = sortingList;
          for (const item of sortingList) {
            this.initSortingForm(item);
          }
          const criteriaList: Array<RequestFormCriteriaModel> = this.requestFormDB.requestFormCriterias;
          this.currentCriteriaList = criteriaList;
          for (const item of criteriaList) {
            this.initCriteriaForm(item);
          }



        }
      }
      , error => { }
    );
  }

  initColumnForm(item: RequestFormColumnModel) {
    const form = this._formBuilder.group({
      schemaName: new FormControl(item.schemaName),
      tableName: new FormControl(item.tableName),
      colName: new FormControl(item.colName),
      colDescTh: new FormControl(item.colDescTh),
      datatype: new FormControl(item.datatype),
      length: new FormControl(item.length),
      isPeriodSearch: new FormControl(item.isPeriodSearch),
      isSorting: new FormControl(item.isSorting),
      isSearchByList: new FormControl(item.isSearchByList),
      createdBy: new FormControl(this.loginUserInfo.username),
    });
    this.columnFormArray.push(form);
  }

  initSortingForm(item: RequestFormSortingModel) {
    const form = this._formBuilder.group({
      schemaName: new FormControl(item.schemaName),
      tableName: new FormControl(item.tableName),
      colName: new FormControl(item.colName),
      colDescTh: new FormControl({ value: item.colDescTh, disabled: true }),
      datatype: new FormControl(item.datatype),
      sortingType: new FormControl(item.sortingType === '' ? null : item.sortingType),
      isShowSorting: new FormControl(item.isSorting),
      createdBy: new FormControl(this.loginUserInfo.username),
    });
    this.sortingFormArray.push(form);
  }

  initCriteriaForm(item: RequestFormCriteriaModel) {
    const form = this._formBuilder.group({
      schemaName: new FormControl(item.schemaName),
      colName: new FormControl(item.colName),
      tableName: new FormControl(item.tableName),
      colDescTh: new FormControl({ value: item.colDescTh, disabled: true }),
      datatype: new FormControl(item.datatype),
      criteriaValue: new FormControl(item.criteriaValue),
      filePath: new FormControl(''),
      fileName: new FormControl(''),
      periodDateFrom: new FormControl(item.periodDateFrom),
      periodDateTo: new FormControl(item.periodDateTo),
      isPeriodSearch: new FormControl(item.isPeriodSearch),
      isSearchByList: new FormControl(item.isSearchByList),
      createdBy: new FormControl(this.loginUserInfo.username),
    });
    this.criteriaFormArray.push(form);
  }

  isInvalidAndTouched(myControl: AbstractControl) {
    return myControl.invalid && (myControl.dirty || myControl.touched);
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  onSelectedFields(selectedItems: Array<SearchTemplateDetailModel>) {
    if (this.currentItems < selectedItems) {
      this.addFormArray(selectedItems);
    } else {
      this.removeFormArray(selectedItems);
    }
    console.log(this.currentItems);
    console.log(this.currentColumnList);
    console.log(this.currentSortingList);
    console.log(this.currentCriteriaList);
    console.log('----------------------');
  }

  addFormArray(selectedItems: Array<SearchTemplateDetailModel>) {
    this.currentItems = selectedItems;
    for (const item of selectedItems) {
      const foundItem = this.currentColumnList.find(obj => obj.colName === item.colName  && obj.tableName === item.tableName);
      if (foundItem) {
        continue;
      }
      this.currentColumnList.push(item);
      this.addColumnForm(item);
      if (item.isSorting) {
          this.addSortingForm(item);
      }
      if (item.isPeriodSearch || item.isSearchByList) {
          this.addCriteriaForm(item);
      }
    }
  }

  addColumnForm(item: SearchTemplateDetailModel) {
    const form = this._formBuilder.group({
      schemaName: new FormControl(item.schemaName),
      tableName: new FormControl(item.tableName),
      colName: new FormControl(item.colName),
      colDescTh: new FormControl(item.colDescTh),
      datatype: new FormControl(item.datatype),
      length: new FormControl(item.length),
      isPeriodSearch: new FormControl(item.isPeriodSearch),
      isSorting: new FormControl(item.isSorting),
      isSearchByList: new FormControl(item.isSearchByList),
      createdBy: new FormControl(this.loginUserInfo.username),
    });
    this.columnFormArray.push(form);
  }

  addCriteriaForm(item: SearchTemplateDetailModel) {
    const form = this._formBuilder.group({
      tableName: new FormControl(item.tableName),
      colName: new FormControl(item.colName),
      colDescTh: new FormControl({ value: item.colDescTh, disabled: true }),
      datatype: new FormControl(item.datatype),
      criteriaValue: new FormControl(''),
      filePath: new FormControl(''),
      fileName: new FormControl(''),
      periodDateFrom: new FormControl(''),
      periodDateTo: new FormControl(''),
      isPeriodSearch: new FormControl(item.isPeriodSearch),
      isSearchByList: new FormControl(item.isSearchByList),
      createdBy: new FormControl(this.loginUserInfo.username),
    });
    this.criteriaFormArray.push(form);
  }

  addSortingForm(item: SearchTemplateDetailModel) {
    const form = this._formBuilder.group({
      colName: new FormControl(item.colName),
      colDescTh: new FormControl({ value: item.colDescTh, disabled: true }),
      datatype: new FormControl(item.datatype),
      sortingType: new FormControl(''),
      isShowSorting: new FormControl(item.isSorting),
      createdBy: new FormControl(this.loginUserInfo.username),
    });
    this.sortingFormArray.push(form);
  }

  removeFormArray(selectedItems: Array<SearchTemplateDetailModel>) {
    const removeList = this.currentItems.filter(current => selectedItems.indexOf(current) < 0);
    for (const item of removeList) {
      this.removeColumnForm(item);
      this.removeSortingForm(item);
      this.removeCriteriaForm(item);
      if (item.isSorting) {

      }
      if (item.isPeriodSearch || item.isSearchByList) {

      }
    }
    this.currentItems = this.currentItems.filter(currentItem => !removeList.includes(currentItem));
  }

  removeSortingForm(item: SearchTemplateDetailModel) {
    this.currentSortingList = this.removeFromList(this.currentSortingList, item);
    this.sortingFormArray.removeAt(this.sortingFormArray.value.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName));
  }

  removeColumnForm(item: SearchTemplateDetailModel) {
    this.currentColumnList = this.removeFromList(this.currentColumnList, item);
    const index = this.columnFormArray.value.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName);
    if (index < 0) {
      return;
    }
    console.log('removeColumnForm', index, this.columnFormArray.value);
    this.columnFormArray.removeAt(index);
    console.log('removeColumnForm success', index, this.columnFormArray.value);
  }

  removeCriteriaForm(item: any) {
    this.currentCriteriaList = this.removeFromList(this.currentCriteriaList, item);
    const index = this.criteriaFormArray.value.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName);
    if (index < 0) {
      return;
    }
    console.log('removeCriteriaForm item', item);
    console.log('removeCriteriaForm', index, this.criteriaFormArray.value);
    this.criteriaFormArray.removeAt(index);
    console.log('removeCriteriaForm success', index, this.criteriaFormArray.value);

    // this.criteriaFormArray.removeAt(this.criteriaFormArray.value.findIndex(obj => obj.colName === item.colName && obj.tableName === item.tableName));
  }

  removeFromList(list: Array<any>, removeItem: SearchTemplateDetailModel) {
    const index = list.findIndex(obj => obj.colName === removeItem.colName && obj.tableName === removeItem.tableName);
    if (index >= 0) {
      list.splice(index, 1);
    }
    return list;
    // return list.filter(item => item.colName !== item.colName && item.tableName !== item.tableName);
  }

  setFileName(e, i) {
    // @ts-ignore
    this.fileUpload = <File>event.target.files[0];
    this.fileControl(i).setValue(this.fileUpload);
    this.fileNameControl(i).setValue(this.fileUpload.name);
  }

  removeAttachmentForm(i) {
    this.fileControl(i).setValue(null);
    this.fileNameControl(i).setValue(null);
  }

  get criteriaFormArray(): FormArray {
    return this.requestFormGroup.get('requestFormCriterias') as FormArray;
  }

  get sortingFormArray(): FormArray {
    return this.requestFormGroup.get('requestFormSortings') as FormArray;
  }

  get columnFormArray(): FormArray {
    return this.requestFormGroup.get('requestFormColumns') as FormArray;
  }

  get formTitleControl(): AbstractControl {
    return this.requestFormGroup.get('formTitle');
  }

  get searchTemplateIdControl(): AbstractControl {
    return this.requestFormGroup.get('searchTemplate.searchTemplateId');
  }

  get fileTypeControl(): AbstractControl {
    return this.requestFormGroup.get('fileType');
  }

  get cancelReason(): AbstractControl {
    return this.requestFormGroup.get('cancelReason');
  }

  isPeriodSearch(i): boolean { return this.criteriaFormArray.at(i).get('isPeriodSearch').value; }
  isSearchByList(i): boolean { return this.criteriaFormArray.at(i).get('isSearchByList').value; }
  fileNameControl(i): AbstractControl { return this.criteriaFormArray.at(i).get('fileName'); }
  fileControl(i): AbstractControl { return this.criteriaFormArray.at(i).get('filePath'); }

  isFormValid(): boolean {
    this.markFormGroupTouched(this.requestFormGroup);
    return this.requestFormGroup.valid;
  }

  saveDraft() {
    if (this.isFormValid()) {
      this.arsUserService.saveDraft(this.requestFormGroup.getRawValue()).subscribe(
        res => {
          this.templateModalService.openModal('สำเร็จ', 'บันทึกร่างสำเร็จ');
          this.goHome();
        }, error => {
          this.templateModalService.openModal('ไม่สำเร็จ', 'พบข้อผิดพลาดระหว่างบันทึกข้อมูล');
        });
    }
  }

  sendApprove() {
    if (this.isFormValid()) {
      this.arsUserService.sendApprove(this.requestFormGroup.getRawValue()).subscribe(
        res => {
          this.templateModalService.openModal('สำเร็จ', 'บันทึกข้อมูลและส่งพิจารณาอนุมัติสำเร็จ');
          this.goHome();
        }, error => {
          this.templateModalService.openModal('ไม่สำเร็จ', 'พบข้อผิดพลาดระหว่างบันทึกข้อมูล');
        });
    }
  }

  cancelDraft() {
    if (this.cancelReason.value.length === 0) {
      this.templateModalService.openModal('ไม่สำเร็จ', 'กรุณาระบุเหตุผลสำหรับการยกเลิกใบคำขอ');
    }
  }

  clear() {
    // this.requestFormGroup.reset();
  }

  goHome() {
    this.router.navigate(['/ars/user/adhoc/search']);
  }

}
