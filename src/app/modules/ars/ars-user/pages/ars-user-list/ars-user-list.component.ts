import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, AbstractControl } from '@angular/forms';
import { ArsUserService } from '../../services/ars-user.service';
import { Router } from '@angular/router';
import { DropDownList } from '../../../../certificate/models/drop-down-list.model';
import { ArsDropdownModel } from '../../../models/ars-dropdown.model';
import { PageResponse } from '../../../../../shared/models/page-response';
import { PageRequest } from '../../../../../shared/models/page-request';
import { RequestFormTableModel } from '../../../models/request-form-table.model';
import { ArsCommonService } from '../../../shared/services/ars-common.service';
import { ArsRequestFormService } from '../../../shared/services/ars-requestform.service';
import { AnswerModalComponent } from '../../../../../shared/modals/answer-modal/answer-modal.component';
import { ModalService } from '../../../../../shared/services/modal.service';
import { SsoUserModel } from '../../../../../core/models/sso-user.model';
import { AuthenticationService } from '../../../../../core/authentication/authentication.service';
import { ArsExportService } from '../../../shared/services/ars-export.service';

declare var $;

@Component({
  selector: 'app-ars-user-list',
  templateUrl: './ars-user-list.component.html',
  styleUrls: ['./ars-user-list.component.css']
})
export class ArsUserListComponent implements OnInit {
  isCollapsed = false;
  searchFormGroup: FormGroup;
  ids: number[] = [];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();

  searchTemplateDropdown: Array<ArsDropdownModel>;
  statusDropdown: Array<ArsDropdownModel>;

  tableList: Array<RequestFormTableModel> = [];
  tableExcelList: Array<RequestFormTableModel> = [];

  loginUserInfo: SsoUserModel;


  constructor(private arsUserService: ArsUserService,
    private arsRequestFormService: ArsRequestFormService,
    private authenticationService: AuthenticationService,
    private arsCommonService: ArsCommonService,
    private modalAlertService: ModalService,
    private arsExportService: ArsExportService,
    private fb: FormBuilder,
    private router: Router) {
  }

  ngOnInit() {
    this.ids = [];
    this.initDropDown();
    this.initSearchFormGroup();
    this.pageRequest.sortFieldName = 'ARS_RUNNING_NO';
    this.pageRequest.sortDirection = 'DESC';
    this.getData();
  }

  initSearchFormGroup() {
    this.loginUserInfo = this.authenticationService.ssoUserDetailSnapshot;
    this.searchFormGroup = new FormGroup({
      arsRunningNo: new FormControl(null),
      createdDateFrom: new FormControl(null),
      createdDateTo: new FormControl(null),
      approvedDateFrom: new FormControl(null),
      approvedDateTo: new FormControl(null),
      searchTemplateId: new FormControl(''),
      statusCode: new FormControl(''),
      username: new FormControl(this.loginUserInfo.username),
      userOfficeCode: new FormControl(this.loginUserInfo.userOfficeCode)
    });
  }

  initDropDown() {
    this.arsCommonService.getAllActiveSearchTemplateDropdown().subscribe((res: any) => {
      this.searchTemplateDropdown = res.data;
    }, error => {
      console.log('getSearchTemplateDropdown', error);
    });

    this.arsCommonService.getAllStatusDropdown().subscribe((res: any) => {
      this.statusDropdown = res.data;
    }, error => {
      console.log('getStatusDropdown', error);
    });
  }

  getData() {
    const criteria = this.searchFormGroup.getRawValue();
    criteria.username = this.loginUserInfo.username;
    criteria.userOfficeCode = this.loginUserInfo.userOfficeCode;
    criteria.pageRequestDto = this.pageRequest;
    this.arsRequestFormService.getRequestFormByCriteria(criteria).subscribe(res => {
      this.tableList = res.data.content;
    });

  }

  exportExcel() {
    if (this.tableExcelList.length === 0) {
      this.modalAlertService.openConfirmModal('แจ้งเตือน', 'กรุณาเลือกข้อมูลที่ต้องการ Export ');
      return;
    }
    const element = document.getElementById('excel-table');
    this.arsExportService.exportExcel(element, 'ARSP0101');
  }

  checkAll(ele) {
    this.ids = [];
    const checked = ele.target.checked;
    if (checked && this.tableList) {
      this.ids = this.tableList.map(column => column.requestFormId);
    } else {
      this.ids = [];
    }
    this.checkElement(checked);

    this.OnGenerateTableExcel();

  }

  checkElement(checked: boolean) {
    const elementList = $('input[type="checkbox"].requestFormSelected');
    for (let i = 0; i < elementList.length; i++) {
      elementList[i].checked = checked;
    }
  }

  toggleId(id) {
    if (this.ids.includes(id)) {
      this.ids = this.ids.filter(ids => ids !== id);
    } else {
      this.ids.push(id);
    }

    const elementList = $('#checkRowsAll');
    elementList[0].checked = this.ids.length === this.tableList.length;

    this.OnGenerateTableExcel();
  }

  OnGenerateTableExcel() {
    this.tableExcelList = [];
    this.ids.forEach(id => {
      const tableRow = this.tableList.find(r => r.requestFormId === id);
      if (tableRow) {
        this.tableExcelList.push(tableRow);
      }
    });
  }

  addData() {
    this.router.navigate(['ars/adhoc/add']);
  }

  editData(requestFormId) {
    this.router.navigate(['ars/adhoc/edit', requestFormId]);
    // this.router.navigate(['ars/adhoc/edit'], {queryParams: {requestFormId}});
  }

  chkDeleteRowStatus(): string {
    if (this.ids.length === 0) {
      return 'กรุณาเลือกรายการที่ต้องการลบ';
    }

    for (const id of this.ids) {
      const found = this.tableList.find(t => t.requestFormId === id && t.statusCode !== 'SAVE_DRAFT');
      if (found) {
        return 'ไม่สามารถลบรายการที่ไม่ใช่สถานะ บันทึกร่างได้';
      }
    }

    return null;
  }

  confirmDelete() {
    // console.log('ids', this.ids);
    console.table('tablelist', this.tableList);

    const statusText = this.chkDeleteRowStatus();
    if (statusText) {
      this.modalAlertService.openModal('แจ้งเตือน', statusText);
      return;
    }

    if (this.ids.length > 0) {
      const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ', 'ยืนยันการลบข้อมูลจำนวน ' + this.ids.length + ' รายการ ?');
      (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
        if (result) {
          this.arsRequestFormService.deleteRequestFormByIds(this.ids).subscribe(
            res => {
              this.modalAlertService.openModal('สำเร็จ', 'ลบข้อมูลสำเร็จ');
            },
            error => this.modalAlertService.openModal('ไม่สำเร็จ', 'พบข้อผิดพลาดระหว่างบันทึกข้อมูล'),
            () => {
              this.getData();
            }
          );
        }
      });
    } else {
      this.modalAlertService.openModal('แจ้งเตือน', 'กรุณาเลือกข้อมูลเพื่อลบอย่างน้อย 1 รายการ');
    }
  }

  cancel() {
    const elementList = $('#checkRowsAll');
    elementList[0].checked = false;
    this.checkElement(false);
    this.modalAlertService.openModal('แจ้งเตือน', 'ยกเลิก');
  }

  clear() {
    this.searchFormGroup.reset();
    this.statusCodeControl.setValue('');
    this.searchTemplateIdControl.setValue('');
    this.approvedToFromControl.setValue('');
    this.approvedDateFromControl.setValue('');
    this.createdDateFromControl.setValue('');
    this.createdDateToFromControl.setValue('');
    this.searchFormGroup.markAsUntouched();
  }

  get createdDateFromControl(): AbstractControl { return this.searchFormGroup.get('createdDateFrom'); }
  get createdDateToFromControl(): AbstractControl { return this.searchFormGroup.get('createdDateTo'); }
  get approvedDateFromControl(): AbstractControl { return this.searchFormGroup.get('approvedDateFrom'); }
  get approvedToFromControl(): AbstractControl { return this.searchFormGroup.get('approvedDateTo'); }
  get searchTemplateIdControl(): AbstractControl { return this.searchFormGroup.get('searchTemplateId'); }
  get statusCodeControl(): AbstractControl { return this.searchFormGroup.get('statusCode'); }

}
