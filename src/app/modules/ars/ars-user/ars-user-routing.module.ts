import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ArsUserListComponent} from './pages/ars-user-list/ars-user-list.component';
import {ArsUserFormComponent} from './pages/ars-user-form/ars-user-form.component';
import { ArsURL } from '../ars.url';
import {ArsUserAddComponent} from './pages/ars-user-add/ars-user-add.component';
import {ArsUserEditComponent} from './pages/ars-user-edit/ars-user-edit.component';

const routes: Routes = [
  { path: '', redirectTo: ArsURL.user.search, pathMatch: 'full'},
  { path: ArsURL.user.search, component: ArsUserListComponent},
  { path: ArsURL.user.form, component: ArsUserFormComponent},
  { path: ArsURL.user.add, component: ArsUserAddComponent},
  { path: ArsURL.user.edit, component: ArsUserEditComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArsUserRoutingModule { }
