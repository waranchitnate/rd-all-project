import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArsUserRoutingModule } from './ars-user-routing.module';
import { ArsUserListComponent } from './pages/ars-user-list/ars-user-list.component';
import { ArsUserFormComponent } from './pages/ars-user-form/ars-user-form.component';
import {SharedModule} from '../../../shared/shared.module';
import {ArsCommonModule} from '../shared/ars-common.modules';
import { ArsUserAddComponent } from './pages/ars-user-add/ars-user-add.component';
import { ArsUserEditComponent } from './pages/ars-user-edit/ars-user-edit.component';

@NgModule({
  declarations: [ArsUserListComponent, ArsUserFormComponent, ArsUserAddComponent, ArsUserEditComponent],
  imports: [
    SharedModule,
    CommonModule,
    ArsUserRoutingModule,
    SharedModule,
    ArsCommonModule
  ]
})
export class ArsUserModule { }
