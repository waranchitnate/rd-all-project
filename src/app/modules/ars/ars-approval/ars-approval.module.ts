import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArsApprovalRoutingModule } from './ars-approval-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ApprovalSearchComponent } from './pages/approval-search/approval-search.component';
import { ApprovalManageComponent } from './pages/approval-manage/approval-manage.component';

@NgModule({
  declarations: [ApprovalSearchComponent, ApprovalManageComponent],
  imports: [
    CommonModule,
    ArsApprovalRoutingModule,
    SharedModule
  ]
})
export class ArsApprovalModule { }
