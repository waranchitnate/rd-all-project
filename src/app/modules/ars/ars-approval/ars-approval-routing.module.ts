import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArsURL } from '../ars.url';
import { ApprovalSearchComponent } from './pages/approval-search/approval-search.component';
import { ApprovalManageComponent } from './pages/approval-manage/approval-manage.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: ArsURL.approval.search},
  {path: ArsURL.approval.search, component: ApprovalSearchComponent},
  {path: ArsURL.approval.manage, component: ApprovalManageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArsApprovalRoutingModule { }
