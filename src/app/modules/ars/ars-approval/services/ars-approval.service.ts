import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ArsApiExtensionsService } from '../../shared/services/ars-apiextension.service';
import { ResponseT } from 'src/app/shared/models/response.model';

@Injectable({
  providedIn: 'root'
})

export class ArsApprovalService {
  private approveRestUrl = environment.arsApiUrl + 'manageRequest/';
  private httpOptions = {};

  constructor(private http: HttpClient, private arsApiExtensionsService: ArsApiExtensionsService) {
    this.httpOptions = this.arsApiExtensionsService.getHttpOptionsJson();
  }

  public saveApprove(approveObj: any): Observable<ResponseT<any>> {
    return <Observable<ResponseT<any>>>this.http.post(this.approveRestUrl + 'saveApproval', approveObj, this.httpOptions);
  }

  public getApprovalHistoryByRequestFormId(requestFormId: number): Observable<ResponseT<any>> {
    return <Observable<ResponseT<any>>>this.http.post(this.approveRestUrl + 'getApprovalHistoryByRequestFormId', requestFormId, this.httpOptions);
  }

}
