import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalManageComponent } from './approval-manage.component';

describe('ApprovalManageComponent', () => {
  let component: ApprovalManageComponent;
  let fixture: ComponentFixture<ApprovalManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
