import { Component, OnInit, TemplateRef } from '@angular/core';
import { ArsApprovalService } from '../../services/ars-approval.service';
import { ModalService } from '../../../../../shared/services/modal.service';
import { AnswerModalComponent } from '../../../../../shared/modals/answer-modal/answer-modal.component';
import {ActivatedRoute, Router} from '@angular/router';
import { ArsRequestFormService } from '../../../shared/services/ars-requestform.service';
import { RequestFormCriteriaModel, RequestFormModel } from '../../../models/request-form.model';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { AuthenticationService } from '../../../../../core/authentication/authentication.service';
import { SsoUserModel } from '../../../../../core/models/sso-user.model';
import { ArsCommonService } from '../../../shared/services/ars-common.service';


@Component({
  selector: 'app-approval-manage',
  templateUrl: './approval-manage.component.html',
  styleUrls: ['./approval-manage.component.css']
})
export class ApprovalManageComponent implements OnInit {
  approvalFormGroup: FormGroup;
  requestFormId = this.actRoute.snapshot.paramMap.get('requestFormId');
  requestForm: RequestFormModel;
  isCollapsed = false;
  formTitle: string;
  masters: Array<any>;
  requestFormCriterias: Array<RequestFormCriteriaModel>;
  mastersSorting: Array<any> = [];
  loginUserInfo: SsoUserModel;
  approveHistory: Array<any> = [];


  constructor(private arsApprovalService: ArsApprovalService,
              private router: Router,
              private arsRequestFormService: ArsRequestFormService,
              private authenticationService: AuthenticationService,
              private templateModalService: ModalService,
              public arsCommonService: ArsCommonService,
              private _formBuilder: FormBuilder,
              private actRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.getData(this.requestFormId);
  }

  getData(requestFormId) {
    this.arsRequestFormService.getRequestFormByRequestId(requestFormId).subscribe(res => {
      this.requestForm = res.data;
      this.formTitle = this.requestForm.formTitle;
      this.masters = this.requestForm.requestFormColumns;
      this.requestFormCriterias = this.requestForm.requestFormCriterias;
      this.mastersSorting = this.requestForm
        .requestFormSortings
        .filter(r => r.hasOwnProperty('sortingType')).filter(r => r.sortingType);
      this.initApprovalForm();

      if (this.requestForm.currentApproved > 0) {
        this.arsApprovalService.getApprovalHistoryByRequestFormId(this.requestForm.requestId).subscribe( history => {
           this.approveHistory = history.data;
        });
      }
    });
  }

  initApprovalForm() {
    this.loginUserInfo = this.authenticationService.ssoUserDetailSnapshot;
    this.approvalFormGroup = this._formBuilder.group({
      requestFormId: new FormControl(this.requestForm.requestId),
      approverSeq: new FormControl(1),
      approverName: new FormControl(this.loginUserInfo.nameTH + ' ' + this.loginUserInfo.surNameTH),
      approverUsername: new FormControl(this.loginUserInfo.username),
      statusCode: new FormControl(''),
      rejectReason: new FormControl(''),
      userOfficeCode: new FormControl(this.loginUserInfo.userOfficeCode)
    });
  }


  rejectRequest() {
    const modalRef = this.templateModalService.openConfirmModal('ยืนยันการดำเนินการ', 'ท่านยืนยันการอนุมัติ/ยกเลิกใบคำขอนี้ใช่หรือไม่');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        this.approveRequest();
      }
    });
  }

  goHome() {
    this.router.navigate(['/ars/approval/adhoc/search']);
  }

  approveRequest() {
    const modalRef = this.templateModalService.openConfirmModal('ยืนยันการอนุมัติ',  'กรุณายืนยันการอนุมัติใบคำขอข้อมูลฯ หากยืนยันการอนุมัติ ให้กด ตกลง');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        this.approvalFormGroup.get('statusCode').setValue('APPROVED');
        this.arsApprovalService.saveApprove(this.approvalFormGroup.getRawValue()).subscribe(res => {
          this.goHome();
          this.templateModalService.openModal('สำเร็จ', 'บันทึกผลการพิจารณาสำเร็จ');
        }, error1 => {
          this.templateModalService.openModal('ไม่สำเร็จ', 'พบข้อผิดพลาดระหว่างบันทึกข้อมูล');
        });
      }
    });
  }
}

