import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {ArsURL} from '../../../ars.url';
import {ArsApprovalService} from '../../services/ars-approval.service';
import { ArsDropdownModel } from '../../../models/ars-dropdown.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { ArsCommonService } from '../../../shared/services/ars-common.service';
import {ArsRequestFormService} from '../../../shared/services/ars-requestform.service';
import {AuthenticationService} from '../../../../../core/authentication/authentication.service';
import {SsoUserModel} from '../../../../../core/models/sso-user.model';
import {RequestFormTableModel} from '../../../models/request-form-table.model';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { ArsExportService } from '../../../shared/services/ars-export.service';
import { ModalService } from '../../../../../shared/services/modal.service';

declare var $;

@Component({
  selector: 'app-approval-search',
  templateUrl: './approval-search.component.html',
  styleUrls: ['./approval-search.component.css']
})
export class ApprovalSearchComponent implements OnInit {
  isCollapsed = false;
  searchCondition: FormGroup;
  masters: Array<RequestFormTableModel> = [];
  tableExcelList: Array<RequestFormTableModel> = [];
  masterRowsCount = 0;
  rowCount = 0;
  ids: number[] = [];
  limit: Array<number> = this.ids;
  searchTemplateDropdown: Array<ArsDropdownModel>;
  statusDropdown: Array<ArsDropdownModel>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  loginUserInfo: SsoUserModel;
  fileExcelExtension = '.xlsx';

  constructor(private arsApprovalService: ArsApprovalService,
              private authenticationService: AuthenticationService,
              private arsRequestFormService: ArsRequestFormService,
              private arsCommonService: ArsCommonService,
              private modalAlertService: ModalService,
              private arsExportService: ArsExportService,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.initDropDown();
    this.initSearchFormGroup();
    this.pageRequest.sortFieldName = 'ARS_RUNNING_NO';
    this.pageRequest.sortDirection = 'DESC';
    this.getData();
  }


  initSearchFormGroup() {
    this.loginUserInfo = this.authenticationService.ssoUserDetailSnapshot;
    this.searchCondition = new FormGroup({
      arsRunningNo: new FormControl(''),
      createdDateFrom: new FormControl(''),
      createdDateTo: new FormControl(''),
      approvedDateFrom: new FormControl(''),
      approvedDateTo: new FormControl(''),
      searchTemplateId: new FormControl(''),
      statusCode: new FormControl(''),
      isSearchUnApproveOnly: new FormControl(false)
    });
  }

  initDropDown() {
    this.arsCommonService.getAllActiveSearchTemplateDropdown().subscribe((res: any) => {
      this.searchTemplateDropdown = res.data;
    }, error => {
      console.log('getSearchTemplateDropdown', error);
    });

    this.arsCommonService.getAllStatusDropdown().subscribe((res: any) => {
      this.statusDropdown = res.data;
      this.statusDropdown = this.statusDropdown.filter(obj => obj.code !== 'WAIT_APPROVE');
    }, error => {
    console.log('getStatusDropdown', error);
    });
  }

  getData() {
    const criteria = this.searchCondition.getRawValue();
    criteria.userOfficeCode = this.loginUserInfo.userOfficeCode;
    criteria.pageRequestDto = this.pageRequest;
    this.arsRequestFormService.getRequestFormForFirstApproverByCondition(criteria).subscribe(res => {
      this.masters = res.data.content;
      this.rowCount = this.masters.length;
      this.masterRowsCount = this.masters.filter((obj) => obj.firstApprovedDate === null).length;
    });
  }
  exportExcel() {
    if (this.tableExcelList.length === 0) {
      this.modalAlertService.openConfirmModal('แจ้งเตือน', 'กรุณาเลือกข้อมูลที่ต้องการ Export ');
      return;
    }
    const element = document.getElementById('excel-table');
    this.arsExportService.exportExcel(element, 'ARSP0104');
  }
  checkAll(ele) {
    this.ids = [];
    const checked = ele.target.checked;
    if (checked && this.masters) {
      this.ids = this.masters.map(column => column.requestFormId);
    } else {
      this.ids = [];
    }
    const elementList = $('input[type="checkbox"].requestFormSelected');
    for (let i = 0; i < elementList.length; i++) {
      elementList[i].checked = checked;
    }

    this.OnGenerateTableExcel();

  }

  toggleId(id) {
    if (this.ids.includes(id)) {
      this.ids = this.ids.filter(ids => ids !== id);
    } else {
      this.ids.push(id);
    }

    const elementList = $('#checkRowsAll');
    elementList[0].checked = this.ids.length === this.masters.length;

    this.OnGenerateTableExcel();
  }

  OnGenerateTableExcel() {
    this.tableExcelList = [];
    this.ids.forEach(id => {
      const tableRow = this.masters.find(r => r.requestFormId === id);
      if (tableRow) {
        this.tableExcelList.push(tableRow);
      }
    });
  }

  editData(requestFormId) {
    this.router.navigate(['ars/approval/adhoc/manage/', requestFormId]);
  }

  clear() {
    this.searchCondition.reset();
    this.statusCodeControl.setValue('');
    this.searchTemplateIdControl.setValue('');
    this.approvedToFromControl.setValue('');
    this.approvedDateFromControl.setValue('');
    this.createdDateFromControl.setValue('');
    this.createdDateToFromControl.setValue('');
    this.searchCondition.markAsUntouched();
  }


  get createdDateFromControl(): AbstractControl { return this.searchCondition.get('createdDateFrom'); }
  get createdDateToFromControl(): AbstractControl { return this.searchCondition.get('createdDateTo'); }
  get approvedDateFromControl(): AbstractControl { return this.searchCondition.get('approvedDateFrom'); }
  get approvedToFromControl(): AbstractControl { return this.searchCondition.get('approvedDateTo'); }
  get searchTemplateIdControl(): AbstractControl { return this.searchCondition.get('searchTemplateId'); }
  get statusCodeControl(): AbstractControl { return this.searchCondition.get('statusCode'); }
}
