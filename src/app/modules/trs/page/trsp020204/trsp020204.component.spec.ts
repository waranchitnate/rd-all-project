import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020204Component } from './trsp020204.component';

describe('trsp020204Component', () => {
  let component: trsp020204Component;
  let fixture: ComponentFixture<trsp020204Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020204Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020204Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
