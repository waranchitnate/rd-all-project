import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010100Component } from './trsp010100.component';

describe('trsp010100Component', () => {
  let component: trsp010100Component;
  let fixture: ComponentFixture<trsp010100Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010100Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010100Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
