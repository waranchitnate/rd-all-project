import { Component, OnInit, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, ValidatorFn, AbstractControl, ValidationErrors, Validators } from '@angular/forms';
import { Group } from 'src/app/modules/admin/group-management/models/group.model';
import { ProgramModel } from 'src/app/core/models/program.model';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-search',
  templateUrl: './trsp010100.component.html',
  styleUrls: ['./trsp010100.component.css']
})
export class trsp010100Component implements OnInit {

  private _gettrsp010100Url: string = environment.trsApiUrl + 'trsp0101/trsp010100';

  searchForm: FormGroup;
  fromMonth: FormControl;
  fromYear: FormControl;
  toMonth: FormControl;
  toYear: FormControl;
  nid: FormControl;
  branchType: FormControl;
  branchNo: FormControl;
  operatorName: FormControl;
  receiveData: any[] = [];

  programs: ProgramModel[];
  currentPage = 1;
  totalItem = 0;
  pageSize = 10;
  @ViewChild(FloatButtonComponent) floatComp;


  constructor(private fb: FormBuilder, private http: HttpClient) { 
    this.initForm();
  }

  ngOnInit() {
    //this.floatComp.urlAdd = '/thumb-drive/form';
    //this.floatComp.tooltipMsg = 'เพิ่มข้อมูล อุปกรณ์ Token';
    //this.search();
  }

  initForm() {
    
    let fromDate = new Date();
    this.searchForm = new FormGroup({
      fromMonth: new FormControl(this.fb.control('01')),
      fromYear: new FormControl(this.fb.control('2555')),
      toMonth: new FormControl(this.fb.control('12')),
      toYear: new FormControl(this.fb.control('2565')),
      nid: new FormControl(this.fb.control('0575526009099')),
      branchType: new FormControl(),
      branchNo: new FormControl(),
      operatorName: new FormControl()
    });

    this.fromMonth = this.fb.control(fromDate.getMonth);
    this.fromYear = this.fb.control(fromDate.getFullYear);

    this.toMonth = this.fb.control(fromDate.getMonth);
    this.toYear = this.fb.control(fromDate.getFullYear);

    this.nid = this.fb.control('');
    this.branchType = this.fb.control('');
    this.branchNo = this.fb.control('');
    this.operatorName = this.fb.control('');

    this.searchForm = this.fb.group({
      fromMonth: '01',
      fromYear: 2555,
      toMonth: 12,
      toYear: 2565,
      nid: '0575526009099',
      branchType: '00',
      branchNo: '00000',
      operatorName: this.operatorName
    }, {
        validators: [
        ]
    });

  }

  search() {
    this.getData(this.searchForm.value).subscribe((res) => {
      console.log(res.data);
      this.receiveData = res.data;
    });
  }

  getData(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._gettrsp010100Url+'/searchOperator', data);
  }

}
