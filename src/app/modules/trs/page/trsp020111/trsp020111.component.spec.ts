import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020111Component } from './trsp020111.component';

describe('trsp020111Component', () => {
  let component: trsp020111Component;
  let fixture: ComponentFixture<trsp020111Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020111Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020111Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
