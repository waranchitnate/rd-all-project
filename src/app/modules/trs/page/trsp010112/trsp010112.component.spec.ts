import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010112Component } from './trsp010112.component';

describe('trsp010112Component', () => {
  let component: trsp010112Component;
  let fixture: ComponentFixture<trsp010112Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010112Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010112Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
