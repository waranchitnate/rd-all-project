import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020202Component } from './trsp020202.component';

describe('trsp020202Component', () => {
  let component: trsp020202Component;
  let fixture: ComponentFixture<trsp020202Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020202Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020202Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
