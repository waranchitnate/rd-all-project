import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020401Component } from './trsp020401.component';

describe('trsp020401Component', () => {
  let component: trsp020401Component;
  let fixture: ComponentFixture<trsp020401Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020401Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020401Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
