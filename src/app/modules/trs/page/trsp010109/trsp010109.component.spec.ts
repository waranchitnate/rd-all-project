import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010109Component } from './trsp010109.component';

describe('trsp010109Component', () => {
  let component: trsp010109Component;
  let fixture: ComponentFixture<trsp010109Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010109Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010109Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
