import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020206Component } from './trsp020206.component';

describe('trsp020206Component', () => {
  let component: trsp020206Component;
  let fixture: ComponentFixture<trsp020206Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020206Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020206Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
