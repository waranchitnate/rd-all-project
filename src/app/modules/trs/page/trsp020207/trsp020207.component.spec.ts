import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020207Component } from './trsp020207.component';

describe('trsp020207Component', () => {
  let component: trsp020207Component;
  let fixture: ComponentFixture<trsp020207Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020207Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020207Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
