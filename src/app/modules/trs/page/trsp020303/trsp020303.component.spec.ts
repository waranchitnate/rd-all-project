import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020303Component } from './trsp020303.component';

describe('trsp020303Component', () => {
  let component: trsp020303Component;
  let fixture: ComponentFixture<trsp020303Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020303Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020303Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
