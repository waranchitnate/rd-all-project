import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020309Component } from './trsp020309.component';

describe('trsp020309Component', () => {
  let component: trsp020309Component;
  let fixture: ComponentFixture<trsp020309Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020309Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020309Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
