import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020105Component } from './trsp020105.component';

describe('trsp020105Component', () => {
  let component: trsp020105Component;
  let fixture: ComponentFixture<trsp020105Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020105Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020105Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
