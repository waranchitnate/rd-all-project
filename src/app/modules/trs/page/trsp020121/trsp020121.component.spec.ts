import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020121Component } from './trsp020121.component';

describe('trsp020121Component', () => {
  let component: trsp020121Component;
  let fixture: ComponentFixture<trsp020121Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020121Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020121Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
