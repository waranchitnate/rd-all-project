import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020208Component } from './trsp020208.component';

describe('trsp020208Component', () => {
  let component: trsp020208Component;
  let fixture: ComponentFixture<trsp020208Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020208Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020208Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
