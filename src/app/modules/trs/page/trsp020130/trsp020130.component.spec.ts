import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020130Component } from './trsp020130.component';

describe('trsp020130Component', () => {
  let component: trsp020130Component;
  let fixture: ComponentFixture<trsp020130Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020130Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020130Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
