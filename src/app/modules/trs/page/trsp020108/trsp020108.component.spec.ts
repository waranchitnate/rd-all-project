import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020108Component } from './trsp020108.component';

describe('trsp020108Component', () => {
  let component: trsp020108Component;
  let fixture: ComponentFixture<trsp020108Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020108Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020108Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
