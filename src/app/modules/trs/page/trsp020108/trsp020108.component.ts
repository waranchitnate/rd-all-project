import { Component, OnInit, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { Group } from 'src/app/modules/admin/group-management/models/group.model';
import { ProgramModel } from 'src/app/core/models/program.model';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
@Component({
  selector: 'app-search',
  templateUrl: './trsp020108.component.html',
  styleUrls: ['./trsp020108.component.css']
})
export class trsp020108Component implements OnInit {

  programs: ProgramModel[];
  currentPage = 1;
  totalItem = 0;
  pageSize = 10;
  @ViewChild(FloatButtonComponent) floatComp;

  constructor() {}

  ngOnInit() {
    //this.floatComp.urlAdd = '/thumb-drive/form';
    //this.floatComp.tooltipMsg = 'เพิ่มข้อมูล อุปกรณ์ Token';
    //this.search();
  }

  search() {
  }

}
