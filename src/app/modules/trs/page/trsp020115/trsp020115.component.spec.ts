import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020115Component } from './trsp020115.component';

describe('trsp020115Component', () => {
  let component: trsp020115Component;
  let fixture: ComponentFixture<trsp020115Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020115Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020115Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
