import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010108Component } from './trsp010108.component';

describe('trsp010108Component', () => {
  let component: trsp010108Component;
  let fixture: ComponentFixture<trsp010108Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010108Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010108Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
