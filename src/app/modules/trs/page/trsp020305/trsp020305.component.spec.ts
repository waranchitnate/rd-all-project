import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020305Component } from './trsp020305.component';

describe('trsp020305Component', () => {
  let component: trsp020305Component;
  let fixture: ComponentFixture<trsp020305Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020305Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020305Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
