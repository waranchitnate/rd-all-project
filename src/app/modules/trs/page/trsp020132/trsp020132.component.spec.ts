import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020132Component } from './trsp020132.component';

describe('trsp020132Component', () => {
  let component: trsp020132Component;
  let fixture: ComponentFixture<trsp020132Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020132Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020132Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
