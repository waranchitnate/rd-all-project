import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020109Component } from './trsp020109.component';

describe('trsp020109Component', () => {
  let component: trsp020109Component;
  let fixture: ComponentFixture<trsp020109Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020109Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020109Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
