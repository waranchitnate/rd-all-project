import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020128Component } from './trsp020128.component';

describe('trsp020128Component', () => {
  let component: trsp020128Component;
  let fixture: ComponentFixture<trsp020128Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020128Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020128Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
