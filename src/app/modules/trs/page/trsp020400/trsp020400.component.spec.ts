import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020400Component } from './trsp020400.component';

describe('trsp020400Component', () => {
  let component: trsp020400Component;
  let fixture: ComponentFixture<trsp020400Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020400Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020400Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
