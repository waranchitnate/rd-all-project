import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020112Component } from './trsp020112.component';

describe('trsp020112Component', () => {
  let component: trsp020112Component;
  let fixture: ComponentFixture<trsp020112Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020112Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020112Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
