import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010107Component } from './trsp010107.component';

describe('trsp010107Component', () => {
  let component: trsp010107Component;
  let fixture: ComponentFixture<trsp010107Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010107Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010107Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
