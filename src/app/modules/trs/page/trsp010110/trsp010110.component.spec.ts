import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010110Component } from './trsp010110.component';

describe('trsp010110Component', () => {
  let component: trsp010110Component;
  let fixture: ComponentFixture<trsp010110Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010110Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010110Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
