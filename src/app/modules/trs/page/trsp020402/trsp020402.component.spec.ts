import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020402Component } from './trsp020402.component';

describe('trsp020402Component', () => {
  let component: trsp020402Component;
  let fixture: ComponentFixture<trsp020402Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020402Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020402Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
