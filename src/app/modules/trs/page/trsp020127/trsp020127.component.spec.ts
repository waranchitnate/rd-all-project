import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020127Component } from './trsp020127.component';

describe('trsp020127Component', () => {
  let component: trsp020127Component;
  let fixture: ComponentFixture<trsp020127Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020127Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020127Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
