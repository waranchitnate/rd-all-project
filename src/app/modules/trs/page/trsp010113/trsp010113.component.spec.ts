import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010113Component } from './trsp010113.component';

describe('trsp010113Component', () => {
  let component: trsp010113Component;
  let fixture: ComponentFixture<trsp010113Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010113Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010113Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
