import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020308Component } from './trsp020308.component';

describe('trsp020308Component', () => {
  let component: trsp020308Component;
  let fixture: ComponentFixture<trsp020308Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020308Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020308Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
