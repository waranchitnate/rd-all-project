import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020306Component } from './trsp020306.component';

describe('trsp020306Component', () => {
  let component: trsp020306Component;
  let fixture: ComponentFixture<trsp020306Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020306Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020306Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
