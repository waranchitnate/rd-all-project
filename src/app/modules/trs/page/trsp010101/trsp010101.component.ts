import { Component, OnInit, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, ValidatorFn, AbstractControl, ValidationErrors, Validators } from '@angular/forms';
import { Group } from 'src/app/modules/admin/group-management/models/group.model';
import { ProgramModel } from 'src/app/core/models/program.model';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-search',
  templateUrl: './trsp010101.component.html',
  styleUrls: ['./trsp010101.component.css']
})
export class trsp010101Component implements OnInit {
  private _gettrsp010101Url: string = environment.trsApiUrl + 'trsp0101/trsp010101';

  searchForm: FormGroup;
  nid: FormControl;
  branchNo: FormControl;
  fromMonth: FormControl;
  fromYear: FormControl;
  toMonth: FormControl;
  toYear: FormControl;
  from: FormControl;

  programs: ProgramModel[];
  currentPage = 1;
  totalItem = 0;
  pageSize = 10;
  @ViewChild(FloatButtonComponent) floatComp;

  receiveData: any[] = [];
  receiveData2: any[] = [];
  receiveDataSum: any[] = [];

  constructor(private activatedRoute: ActivatedRoute, private fb: FormBuilder, private http: HttpClient) {
  }

  ngOnInit() {
    //this.floatComp.urlAdd = '/thumb-drive/form';
    //this.floatComp.tooltipMsg = 'เพิ่มข้อมูล อุปกรณ์ Token';
    //this.search();
    this.searchForm = new FormGroup({
      nid: this.fb.control(this.activatedRoute.snapshot.paramMap.get('nid')),
      branchNo: this.fb.control(this.activatedRoute.snapshot.paramMap.get('branchNo')),
      fromMonth: this.fb.control(this.activatedRoute.snapshot.paramMap.get('fromMonth')),
      fromYear: this.fb.control(this.activatedRoute.snapshot.paramMap.get('fromYear')),
      toYear: this.fb.control(this.activatedRoute.snapshot.paramMap.get('toYear')),
      toMonth: this.fb.control(this.activatedRoute.snapshot.paramMap.get('toMonth')),
      from: this.fb.control(this.activatedRoute.snapshot.paramMap.get('from'))
    });
    this.search();
  }

  search() {
    console.log(this.searchForm.value);
    this.getData(this.searchForm.value).subscribe((res) => {
      console.log(res.data);
      this.receiveData = res.data;
    });
    this.getData2(this.searchForm.value).subscribe((res) => {
      console.log(res.data);
      this.receiveData2 = res.data;
    });
    this.getDataSum(this.searchForm.value).subscribe((res) => {
      console.log(res.data);
      this.receiveDataSum = res.data;
    });
  }

  getData(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._gettrsp010101Url+'/searchOperator', data);
  }

  getData2(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._gettrsp010101Url+'/getData010101', data);
  }

  getDataSum(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._gettrsp010101Url+'/getSumData010101', data);
  }
}
