import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010101Component } from './trsp010101.component';

describe('trsp010101Component', () => {
  let component: trsp010101Component;
  let fixture: ComponentFixture<trsp010101Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
