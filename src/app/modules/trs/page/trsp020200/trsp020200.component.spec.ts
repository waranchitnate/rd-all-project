import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020200Component } from './trsp020200.component';

describe('trsp020200Component', () => {
  let component: trsp020200Component;
  let fixture: ComponentFixture<trsp020200Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020200Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020200Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
