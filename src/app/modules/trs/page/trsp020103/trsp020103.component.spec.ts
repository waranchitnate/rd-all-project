import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020103Component } from './trsp020103.component';

describe('trsp020103Component', () => {
  let component: trsp020103Component;
  let fixture: ComponentFixture<trsp020103Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020103Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020103Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
