import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020122Component } from './trsp020122.component';

describe('trsp020122Component', () => {
  let component: trsp020122Component;
  let fixture: ComponentFixture<trsp020122Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020122Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020122Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
