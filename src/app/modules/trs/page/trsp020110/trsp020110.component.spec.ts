import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020110Component } from './trsp020110.component';

describe('trsp020110Component', () => {
  let component: trsp020110Component;
  let fixture: ComponentFixture<trsp020110Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020110Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020110Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
