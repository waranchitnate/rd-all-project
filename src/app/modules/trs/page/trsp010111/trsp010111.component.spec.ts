import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010111Component } from './trsp010111.component';

describe('trsp010111Component', () => {
  let component: trsp010111Component;
  let fixture: ComponentFixture<trsp010111Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010111Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010111Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
