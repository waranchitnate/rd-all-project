import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020124Component } from './trsp020124.component';

describe('trsp020124Component', () => {
  let component: trsp020124Component;
  let fixture: ComponentFixture<trsp020124Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020124Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020124Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
