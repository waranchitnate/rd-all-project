import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010103Component } from './trsp010103.component';

describe('trsp010103Component', () => {
  let component: trsp010103Component;
  let fixture: ComponentFixture<trsp010103Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010103Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010103Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
