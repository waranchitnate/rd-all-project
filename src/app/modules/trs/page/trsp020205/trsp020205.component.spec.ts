import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020205Component } from './trsp020205.component';

describe('trsp020205Component', () => {
  let component: trsp020205Component;
  let fixture: ComponentFixture<trsp020205Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020205Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020205Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
