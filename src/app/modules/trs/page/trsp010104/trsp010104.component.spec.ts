import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010104Component } from './trsp010104.component';

describe('trsp010104Component', () => {
  let component: trsp010104Component;
  let fixture: ComponentFixture<trsp010104Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010104Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010104Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
