import { Component, OnInit, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, ValidatorFn, AbstractControl, ValidationErrors, Validators } from '@angular/forms';
import { Group } from 'src/app/modules/admin/group-management/models/group.model';
import { ProgramModel } from 'src/app/core/models/program.model';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-search',
  templateUrl: './trsp010104.component.html',
  styleUrls: ['./trsp010104.component.css']
})
export class trsp010104Component implements OnInit {
  private _gettrsp010104Url: string = environment.trsApiUrl + 'trsp0101/trsp010104';

  searchForm: FormGroup;
  nid: FormControl;
  branchNo: FormControl;
  fromMonth: FormControl;
  fromYear: FormControl;
  toMonth: FormControl;
  toYear: FormControl;
  from: FormControl;
  month: FormControl;
  year: FormControl;
  recordType: FormControl;
  typeSB: FormControl;
  status: FormControl;
  docId: FormControl;

  programs: ProgramModel[];
  currentPage = 1;
  totalItem = 0;
  pageSize = 10;
  @ViewChild(FloatButtonComponent) floatComp;

  receiveData: any[] = [];
  receiveData2: any[] = [];
  receiveDataSum: any[] = [];
  
  constructor(private activatedRoute: ActivatedRoute, private fb: FormBuilder, private http: HttpClient) {
  }

  ngOnInit() {
    //this.floatComp.urlAdd = '/thumb-drive/form';
    //this.floatComp.tooltipMsg = 'เพิ่มข้อมูล อุปกรณ์ Token';
    //this.search();
    this.searchForm = new FormGroup({
      nid: this.fb.control(this.activatedRoute.snapshot.paramMap.get('nid')),
      branchNo: this.fb.control(this.activatedRoute.snapshot.paramMap.get('branchNo')),
      fromMonth: this.fb.control(this.activatedRoute.snapshot.paramMap.get('fromMonth')),
      fromYear: this.fb.control(this.activatedRoute.snapshot.paramMap.get('fromYear')),
      toYear: this.fb.control(this.activatedRoute.snapshot.paramMap.get('toYear')),
      toMonth: this.fb.control(this.activatedRoute.snapshot.paramMap.get('toMonth')),
      from: this.fb.control(this.activatedRoute.snapshot.paramMap.get('from')),
      month: this.fb.control(this.activatedRoute.snapshot.paramMap.get('month')),
      year: this.fb.control(this.activatedRoute.snapshot.paramMap.get('year')),
      recordType: this.fb.control(this.activatedRoute.snapshot.paramMap.get('recordType')),
      typeSB: this.fb.control(this.activatedRoute.snapshot.paramMap.get('typeSB')),
      status: this.fb.control(this.activatedRoute.snapshot.paramMap.get('status')),
      docId: this.fb.control(this.activatedRoute.snapshot.paramMap.get('docId'))
    });
    this.search();
  }

  search() {
    console.log(this.searchForm.value);
    this.getData(this.searchForm.value).subscribe((res) => {
      //console.log(res.data);
      this.receiveData = res.data;
    });
    this.getData2(this.searchForm.value).subscribe((res) => {
      console.log(res.data);
      this.receiveData2 = res.data;
    });
    this.getDataSum(this.searchForm.value).subscribe((res) => {
      console.log(res.data);
      this.receiveDataSum = res.data;
    });
  }

  getData(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._gettrsp010104Url+'/searchOperator', data);
  }

  getData2(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._gettrsp010104Url+'/getData010104', data);
  }

  getDataSum(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._gettrsp010104Url+'/getSumData010104', data);
  }

}
