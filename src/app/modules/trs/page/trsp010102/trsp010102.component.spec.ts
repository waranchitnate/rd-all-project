import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010102Component } from './trsp010102.component';

describe('trsp010102Component', () => {
  let component: trsp010102Component;
  let fixture: ComponentFixture<trsp010102Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010102Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010102Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
