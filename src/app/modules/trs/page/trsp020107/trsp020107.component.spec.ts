import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020107Component } from './trsp020107.component';

describe('trsp020107Component', () => {
  let component: trsp020107Component;
  let fixture: ComponentFixture<trsp020107Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020107Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020107Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
