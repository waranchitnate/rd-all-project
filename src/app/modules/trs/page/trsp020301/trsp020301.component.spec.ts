import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020301Component } from './trsp020301.component';

describe('trsp020301Component', () => {
  let component: trsp020301Component;
  let fixture: ComponentFixture<trsp020301Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020301Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020301Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
