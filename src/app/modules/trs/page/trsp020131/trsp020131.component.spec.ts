import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020131Component } from './trsp020131.component';

describe('trsp020131Component', () => {
  let component: trsp020131Component;
  let fixture: ComponentFixture<trsp020131Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020131Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020131Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
