import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020117Component } from './trsp020117.component';

describe('trsp020117Component', () => {
  let component: trsp020117Component;
  let fixture: ComponentFixture<trsp020117Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020117Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020117Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
