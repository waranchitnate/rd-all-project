import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020125Component } from './trsp020125.component';

describe('trsp020125Component', () => {
  let component: trsp020125Component;
  let fixture: ComponentFixture<trsp020125Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020125Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020125Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
