import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010105Component } from './trsp010105.component';

describe('trsp010105Component', () => {
  let component: trsp010105Component;
  let fixture: ComponentFixture<trsp010105Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010105Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010105Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
