import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020302Component } from './trsp020302.component';

describe('trsp020302Component', () => {
  let component: trsp020302Component;
  let fixture: ComponentFixture<trsp020302Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020302Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020302Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
