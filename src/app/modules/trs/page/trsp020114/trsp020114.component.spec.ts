import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020114Component } from './trsp020114.component';

describe('trsp020114Component', () => {
  let component: trsp020114Component;
  let fixture: ComponentFixture<trsp020114Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020114Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020114Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
