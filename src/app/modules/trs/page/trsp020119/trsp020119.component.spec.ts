import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020119Component } from './trsp020119.component';

describe('trsp020119Component', () => {
  let component: trsp020119Component;
  let fixture: ComponentFixture<trsp020119Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020119Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020119Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
