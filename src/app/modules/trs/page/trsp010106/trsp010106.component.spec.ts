import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp010106Component } from './trsp010106.component';

describe('trsp010106Component', () => {
  let component: trsp010106Component;
  let fixture: ComponentFixture<trsp010106Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp010106Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp010106Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
