import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020104Component } from './trsp020104.component';

describe('trsp020104Component', () => {
  let component: trsp020104Component;
  let fixture: ComponentFixture<trsp020104Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020104Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020104Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
