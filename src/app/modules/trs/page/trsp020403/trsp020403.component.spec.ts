import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020403Component } from './trsp020403.component';

describe('trsp020403Component', () => {
  let component: trsp020403Component;
  let fixture: ComponentFixture<trsp020403Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020403Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020403Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
