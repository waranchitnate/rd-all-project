import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020120Component } from './trsp020120.component';

describe('trsp020120Component', () => {
  let component: trsp020120Component;
  let fixture: ComponentFixture<trsp020120Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020120Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020120Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
