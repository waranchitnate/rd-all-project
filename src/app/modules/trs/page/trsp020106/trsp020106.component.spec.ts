import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020106Component } from './trsp020106.component';

describe('trsp020106Component', () => {
  let component: trsp020106Component;
  let fixture: ComponentFixture<trsp020106Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020106Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020106Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
