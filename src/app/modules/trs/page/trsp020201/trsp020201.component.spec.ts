import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020201Component } from './trsp020201.component';

describe('trsp020201Component', () => {
  let component: trsp020201Component;
  let fixture: ComponentFixture<trsp020201Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020201Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020201Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
