import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020116Component } from './trsp020116.component';

describe('trsp020116Component', () => {
  let component: trsp020116Component;
  let fixture: ComponentFixture<trsp020116Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020116Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020116Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
