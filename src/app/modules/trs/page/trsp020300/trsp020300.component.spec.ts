import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020300Component } from './trsp020300.component';

describe('trsp020300Component', () => {
  let component: trsp020300Component;
  let fixture: ComponentFixture<trsp020300Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020300Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020300Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
