import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020129Component } from './trsp020129.component';

describe('trsp020129Component', () => {
  let component: trsp020129Component;
  let fixture: ComponentFixture<trsp020129Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020129Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020129Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
