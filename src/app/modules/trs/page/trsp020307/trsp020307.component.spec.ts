import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020307Component } from './trsp020307.component';

describe('trsp020307Component', () => {
  let component: trsp020307Component;
  let fixture: ComponentFixture<trsp020307Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020307Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020307Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
