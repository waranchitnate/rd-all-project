import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020203Component } from './trsp020203.component';

describe('trsp020203Component', () => {
  let component: trsp020203Component;
  let fixture: ComponentFixture<trsp020203Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020203Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020203Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
