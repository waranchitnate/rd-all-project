import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020123Component } from './trsp020123.component';

describe('trsp020123Component', () => {
  let component: trsp020123Component;
  let fixture: ComponentFixture<trsp020123Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020123Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020123Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
