import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020304Component } from './trsp020304.component';

describe('trsp020304Component', () => {
  let component: trsp020304Component;
  let fixture: ComponentFixture<trsp020304Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020304Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020304Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
