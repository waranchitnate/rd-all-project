import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trsp020126Component } from './trsp020126.component';

describe('trsp020126Component', () => {
  let component: trsp020126Component;
  let fixture: ComponentFixture<trsp020126Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trsp020126Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trsp020126Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
