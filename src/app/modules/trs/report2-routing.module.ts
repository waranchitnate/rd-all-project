import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { trsp010100Component } from './page/trsp010100/trsp010100.component';
import { trsp010101Component } from './page/trsp010101/trsp010101.component';
import { trsp010102Component } from './page/trsp010102/trsp010102.component';

import { trsp010104Component } from './page/trsp010104/trsp010104.component';
import { trsp010105Component } from './page/trsp010105/trsp010105.component';
import { trsp010106Component } from './page/trsp010106/trsp010106.component';
import { trsp010107Component } from './page/trsp010107/trsp010107.component';
import { trsp010108Component } from './page/trsp010108/trsp010108.component';


import { trsp010103Component } from './page/trsp010103/trsp010103.component';

import { trsp010109Component } from './page/trsp010109/trsp010109.component';
import { trsp010110Component } from './page/trsp010110/trsp010110.component';
import { trsp010111Component } from './page/trsp010111/trsp010111.component';
import { trsp010112Component } from './page/trsp010112/trsp010112.component';
import { trsp010113Component } from './page/trsp010113/trsp010113.component';






import { trsp020103Component } from './page/trsp020103/trsp020103.component';
import { trsp020104Component } from './page/trsp020104/trsp020104.component';
import { trsp020105Component } from './page/trsp020105/trsp020105.component';
import { trsp020106Component } from './page/trsp020106/trsp020106.component';
import { trsp020107Component } from './page/trsp020107/trsp020107.component';
import { trsp020108Component } from './page/trsp020108/trsp020108.component';
import { trsp020109Component } from './page/trsp020109/trsp020109.component';
import { trsp020110Component } from './page/trsp020110/trsp020110.component';
import { trsp020111Component } from './page/trsp020111/trsp020111.component';
import { trsp020112Component } from './page/trsp020112/trsp020112.component';
import { trsp020114Component } from './page/trsp020114/trsp020114.component';
import { trsp020115Component } from './page/trsp020115/trsp020115.component';
import { trsp020116Component } from './page/trsp020116/trsp020116.component';
import { trsp020117Component } from './page/trsp020117/trsp020117.component';
import { trsp020119Component } from './page/trsp020119/trsp020119.component';
import { trsp020120Component } from './page/trsp020120/trsp020120.component';
import { trsp020121Component } from './page/trsp020121/trsp020121.component';
import { trsp020122Component } from './page/trsp020122/trsp020122.component';
import { trsp020123Component } from './page/trsp020123/trsp020123.component';
import { trsp020124Component } from './page/trsp020124/trsp020124.component';
import { trsp020125Component } from './page/trsp020125/trsp020125.component';
import { trsp020126Component } from './page/trsp020126/trsp020126.component';
import { trsp020127Component } from './page/trsp020127/trsp020127.component';
import { trsp020128Component } from './page/trsp020128/trsp020128.component';
import { trsp020129Component } from './page/trsp020129/trsp020129.component';
import { trsp020130Component } from './page/trsp020130/trsp020130.component';
import { trsp020131Component } from './page/trsp020131/trsp020131.component';
import { trsp020132Component } from './page/trsp020132/trsp020132.component';

import { trsp020200Component } from './page/trsp020200/trsp020200.component';
import { trsp020201Component } from './page/trsp020201/trsp020201.component';
import { trsp020202Component } from './page/trsp020202/trsp020202.component';
import { trsp020203Component } from './page/trsp020203/trsp020203.component';
import { trsp020204Component } from './page/trsp020204/trsp020204.component';
import { trsp020205Component } from './page/trsp020205/trsp020205.component';
import { trsp020206Component } from './page/trsp020206/trsp020206.component';
import { trsp020207Component } from './page/trsp020207/trsp020207.component';
import { trsp020208Component } from './page/trsp020208/trsp020208.component';

import { trsp020300Component } from './page/trsp020300/trsp020300.component';
import { trsp020301Component } from './page/trsp020301/trsp020301.component';
import { trsp020302Component } from './page/trsp020302/trsp020302.component';
import { trsp020303Component } from './page/trsp020303/trsp020303.component';
import { trsp020304Component } from './page/trsp020304/trsp020304.component';
import { trsp020305Component } from './page/trsp020305/trsp020305.component';
import { trsp020306Component } from './page/trsp020306/trsp020306.component';
import { trsp020307Component } from './page/trsp020307/trsp020307.component';
import { trsp020308Component } from './page/trsp020308/trsp020308.component';
import { trsp020309Component } from './page/trsp020309/trsp020309.component';

import { trsp020400Component } from './page/trsp020400/trsp020400.component';
import { trsp020401Component } from './page/trsp020401/trsp020401.component';
import { trsp020402Component } from './page/trsp020402/trsp020402.component';
import { trsp020403Component } from './page/trsp020403/trsp020403.component';

const mnsp010000routes: Routes = [{ path: 'trsp010100', component: trsp010100Component}, 
                                    { path: 'trsp010101/:from/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear', component: trsp010101Component}, 
                                    { path: 'trsp010102/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB', component: trsp010102Component}, 
                                      { path: 'trsp010104/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010104Component}, 
                                      { path: 'trsp010105/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010105Component}, 
                                      { path: 'trsp010106/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010106Component}, 
                                      { path: 'trsp010107/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010107Component}, 
                                      { path: 'trsp010108/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010108Component}, 
                                    { path: 'trsp010103/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB', component: trsp010103Component}, 
                                      { path: 'trsp010109/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010109Component}, 
                                      { path: 'trsp010110/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010110Component}, 
                                      { path: 'trsp010111/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010111Component}, 
                                      { path: 'trsp010112/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010112Component}, 
                                      { path: 'trsp010113/:nid/:branchNo/:fromMonth/:fromYear/:toMonth/:toYear/:month/:year/:recordType/:typeSB/:docId', component: trsp010113Component}, 
                                    






                                    { path: 'trsp020103', component: trsp020103Component},

                                  { path: 'trsp020104', component: trsp020104Component}, { path: 'trsp020105', component: trsp020105Component}, { path: 'trsp020106', component: trsp020106Component}, { path: 'trsp020107', component: trsp020107Component},
                                  { path: 'trsp020108', component: trsp020108Component}, { path: 'trsp020109', component: trsp020109Component}, { path: 'trsp020110', component: trsp020110Component}, { path: 'trsp020111', component: trsp020111Component},
                                  { path: 'trsp020112', component: trsp020112Component}, { path: 'trsp020114', component: trsp020114Component}, { path: 'trsp020115', component: trsp020115Component},
                                  { path: 'trsp020116', component: trsp020116Component}, { path: 'trsp020117', component: trsp020117Component}, { path: 'trsp020119', component: trsp020119Component},
                                  { path: 'trsp020120', component: trsp020120Component}, { path: 'trsp020121', component: trsp020121Component}, { path: 'trsp020122', component: trsp020122Component}, { path: 'trsp020123', component: trsp020123Component},
                                  { path: 'trsp020124', component: trsp020124Component}, { path: 'trsp020125', component: trsp020125Component}, { path: 'trsp020126', component: trsp020126Component}, { path: 'trsp020127', component: trsp020127Component},
                                  { path: 'trsp020128', component: trsp020128Component}, { path: 'trsp020129', component: trsp020129Component}, { path: 'trsp020130', component: trsp020130Component}, { path: 'trsp020131', component: trsp020131Component},
                                  { path: 'trsp020132', component: trsp020132Component},
                                
                                  { path: 'trsp020200', component: trsp020200Component}, { path: 'trsp020201', component: trsp020201Component}, { path: 'trsp020202', component: trsp020202Component}, { path: 'trsp020203', component: trsp020203Component},
                                  { path: 'trsp020204', component: trsp020204Component}, { path: 'trsp020205', component: trsp020205Component}, { path: 'trsp020206', component: trsp020206Component}, { path: 'trsp020207', component: trsp020207Component},
                                  { path: 'trsp020208', component: trsp020208Component},

                                  { path: 'trsp020300', component: trsp020300Component}, { path: 'trsp020301', component: trsp020301Component}, { path: 'trsp020302', component: trsp020302Component}, { path: 'trsp020303', component: trsp020303Component},
                                  { path: 'trsp020304', component: trsp020304Component}, { path: 'trsp020305', component: trsp020305Component}, { path: 'trsp020306', component: trsp020306Component}, { path: 'trsp020307', component: trsp020307Component},
                                  { path: 'trsp020308', component: trsp020308Component}, { path: 'trsp020309', component: trsp020309Component},

                                  { path: 'trsp020400', component: trsp020400Component}, { path: 'trsp020401', component: trsp020401Component}, { path: 'trsp020402', component: trsp020402Component}, { path: 'trsp020403', component: trsp020403Component}
                                ];

@NgModule({
  imports: [RouterModule.forChild(mnsp010000routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
