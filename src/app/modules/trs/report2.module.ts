import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportRoutingModule } from './report2-routing.module';

import { trsp010100Component } from './page/trsp010100/trsp010100.component';
import { trsp010101Component } from './page/trsp010101/trsp010101.component';
import { trsp010102Component } from './page/trsp010102/trsp010102.component';

import { trsp010104Component } from './page/trsp010104/trsp010104.component';
import { trsp010105Component } from './page/trsp010105/trsp010105.component';
import { trsp010106Component } from './page/trsp010106/trsp010106.component';
import { trsp010107Component } from './page/trsp010107/trsp010107.component';
import { trsp010108Component } from './page/trsp010108/trsp010108.component';
import { trsp010113Component } from './page/trsp010113/trsp010113.component';


import { trsp010103Component } from './page/trsp010103/trsp010103.component';

import { trsp010109Component } from './page/trsp010109/trsp010109.component';
import { trsp010110Component } from './page/trsp010110/trsp010110.component';
import { trsp010111Component } from './page/trsp010111/trsp010111.component';
import { trsp010112Component } from './page/trsp010112/trsp010112.component';



import { trsp020103Component } from './page/trsp020103/trsp020103.component';
import { trsp020104Component } from './page/trsp020104/trsp020104.component';
import { trsp020105Component } from './page/trsp020105/trsp020105.component';
import { trsp020106Component } from './page/trsp020106/trsp020106.component';
import { trsp020107Component } from './page/trsp020107/trsp020107.component';
import { trsp020108Component } from './page/trsp020108/trsp020108.component';
import { trsp020109Component } from './page/trsp020109/trsp020109.component';
import { trsp020110Component } from './page/trsp020110/trsp020110.component';
import { trsp020111Component } from './page/trsp020111/trsp020111.component';
import { trsp020112Component } from './page/trsp020112/trsp020112.component';
import { trsp020114Component } from './page/trsp020114/trsp020114.component';
import { trsp020115Component } from './page/trsp020115/trsp020115.component';
import { trsp020116Component } from './page/trsp020116/trsp020116.component';
import { trsp020117Component } from './page/trsp020117/trsp020117.component';
import { trsp020119Component } from './page/trsp020119/trsp020119.component';
import { trsp020120Component } from './page/trsp020120/trsp020120.component';
import { trsp020121Component } from './page/trsp020121/trsp020121.component';
import { trsp020122Component } from './page/trsp020122/trsp020122.component';
import { trsp020123Component } from './page/trsp020123/trsp020123.component';
import { trsp020124Component } from './page/trsp020124/trsp020124.component';
import { trsp020125Component } from './page/trsp020125/trsp020125.component';
import { trsp020126Component } from './page/trsp020126/trsp020126.component';
import { trsp020127Component } from './page/trsp020127/trsp020127.component';
import { trsp020128Component } from './page/trsp020128/trsp020128.component';
import { trsp020129Component } from './page/trsp020129/trsp020129.component';
import { trsp020130Component } from './page/trsp020130/trsp020130.component';
import { trsp020131Component } from './page/trsp020131/trsp020131.component';
import { trsp020132Component } from './page/trsp020132/trsp020132.component';

import { trsp020200Component } from './page/trsp020200/trsp020200.component';
import { trsp020201Component } from './page/trsp020201/trsp020201.component';
import { trsp020202Component } from './page/trsp020202/trsp020202.component';
import { trsp020203Component } from './page/trsp020203/trsp020203.component';
import { trsp020204Component } from './page/trsp020204/trsp020204.component';
import { trsp020205Component } from './page/trsp020205/trsp020205.component';
import { trsp020206Component } from './page/trsp020206/trsp020206.component';
import { trsp020207Component } from './page/trsp020207/trsp020207.component';
import { trsp020208Component } from './page/trsp020208/trsp020208.component';

import { trsp020300Component } from './page/trsp020300/trsp020300.component';
import { trsp020301Component } from './page/trsp020301/trsp020301.component';
import { trsp020302Component } from './page/trsp020302/trsp020302.component';
import { trsp020303Component } from './page/trsp020303/trsp020303.component';
import { trsp020304Component } from './page/trsp020304/trsp020304.component';
import { trsp020305Component } from './page/trsp020305/trsp020305.component';
import { trsp020306Component } from './page/trsp020306/trsp020306.component';
import { trsp020307Component } from './page/trsp020307/trsp020307.component';
import { trsp020308Component } from './page/trsp020308/trsp020308.component';
import { trsp020309Component } from './page/trsp020309/trsp020309.component';

import { trsp020400Component } from './page/trsp020400/trsp020400.component';
import { trsp020401Component } from './page/trsp020401/trsp020401.component';
import { trsp020402Component } from './page/trsp020402/trsp020402.component';
import { trsp020403Component } from './page/trsp020403/trsp020403.component';

import { ProgramService } from './service/program-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { TooltipModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [trsp010100Component,trsp010101Component,
                  trsp010102Component,
                    trsp010104Component,
                    trsp010105Component,
                    trsp010106Component,
                    trsp010107Component,
                    trsp010108Component,
                  trsp010103Component,
                    trsp010109Component,
                    trsp010110Component,
                    trsp010111Component,
                    trsp010112Component,
                    trsp010113Component,

                    
                  trsp020103Component,
    
    
    
    
                 
                 trsp020104Component,trsp020105Component,trsp020106Component,trsp020107Component,
                 trsp020108Component,trsp020109Component,trsp020110Component,trsp020111Component,
                 trsp020112Component,trsp020114Component,trsp020115Component,
                 trsp020116Component,trsp020117Component,trsp020119Component,
                 trsp020120Component,trsp020121Component,trsp020122Component,trsp020123Component,
                 trsp020124Component,trsp020125Component,trsp020126Component,trsp020127Component,
                 trsp020128Component,trsp020129Component,trsp020130Component,trsp020131Component,
                 trsp020132Component,
                 
                 trsp020200Component,trsp020201Component,trsp020202Component,trsp020203Component,
                 trsp020204Component,trsp020205Component,trsp020206Component,trsp020207Component,trsp020208Component,

                 trsp020300Component,trsp020301Component,trsp020302Component,trsp020303Component,
                 trsp020304Component,trsp020305Component,trsp020306Component,trsp020307Component,
                 trsp020308Component,trsp020309Component,

                 trsp020400Component,trsp020401Component,trsp020402Component,trsp020403Component
                ],
  imports: [
    CommonModule,
    ReportRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TooltipModule,
  ],
  providers: [ProgramService]
})
export class ReportModule { }
