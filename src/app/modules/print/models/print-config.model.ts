export interface PrintConfig {
    paper: string;
    direction: string;
    url?: string;
}