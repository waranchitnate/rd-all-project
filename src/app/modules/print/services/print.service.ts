import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PrintConfig } from '../models/print-config.model';

@Injectable({
    providedIn: 'root'
})
export class PrintService {

    private config: BehaviorSubject<PrintConfig> = new BehaviorSubject<PrintConfig>(null);

    constructor() { }

    getConfig() {
        return this.config.asObservable();
    }

    setConfig(config: PrintConfig) {
        this.config.next(config);
    }


}
