import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintPreviewComponent } from './pages/print-preview/print-preview.component';
import { PrintRoutingModule } from './print-routing.module';
import { Pnd1WithholdingTaxComponent } from './pages/pnd1-withholding-tax/pnd1-withholding-tax.component';
import { PrintSummaryPerTaxComponent } from './pages/print-summary-per-tax/print-summary-per-tax.component';
import { Pnd1FilingTaxComponent } from './pages/pnd1-filing-tax/pnd1-filing-tax.component';
import { Pnd1TransactionTaxComponent } from './pages/pnd1-transaction-tax/pnd1-transaction-tax.component';
import { Pnd1TransactionDetailTaxComponent } from './pages/pnd1-transaction-detail-tax/pnd1-transaction-detail-tax.component';
import { Pnd1FilingDetailTaxComponent } from './pages/pnd1-filing-detail-tax/pnd1-filing-detail-tax.component';
import { PrintPnd3654ByMonthComponent } from './pages/print-pnd3654-by-month/print-pnd3654-by-month.component';
import { SpecialDetailPerBankComponent } from './pages/special-detail-per-bank/special-detail-per-bank.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [PrintPreviewComponent, Pnd1WithholdingTaxComponent, PrintSummaryPerTaxComponent, Pnd1FilingTaxComponent, Pnd1TransactionTaxComponent, Pnd1TransactionDetailTaxComponent, Pnd1FilingDetailTaxComponent, PrintPnd3654ByMonthComponent, SpecialDetailPerBankComponent],
  imports: [
    CommonModule,
    PdfViewerModule,
    PrintRoutingModule
  ]
})
export class PrintModule { }
