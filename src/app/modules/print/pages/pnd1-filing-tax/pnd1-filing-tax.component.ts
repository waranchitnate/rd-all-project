import { Component, OnInit } from '@angular/core';
import { PrintService } from '../../services/print.service';

@Component({
    selector: 'app-pnd1-filing-tax',
    templateUrl: './pnd1-filing-tax.component.html',
    styleUrls: ['./pnd1-filing-tax.component.css']
})
export class Pnd1FilingTaxComponent implements OnInit {

    constructor(private ps: PrintService) { }

    ngOnInit() {
        this.ps.setConfig({
            paper: 'A4',
            direction: 'แนวนอน'
        });
    }

}
