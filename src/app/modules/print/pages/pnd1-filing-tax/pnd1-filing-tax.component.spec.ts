import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pnd1FilingTaxComponent } from './pnd1-filing-tax.component';

describe('Pnd1FilingTaxComponent', () => {
  let component: Pnd1FilingTaxComponent;
  let fixture: ComponentFixture<Pnd1FilingTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pnd1FilingTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pnd1FilingTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
