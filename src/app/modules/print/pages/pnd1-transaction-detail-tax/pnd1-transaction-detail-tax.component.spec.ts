import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pnd1TransactionDetailTaxComponent } from './pnd1-transaction-detail-tax.component';

describe('Pnd1TransactionDetailTaxComponent', () => {
  let component: Pnd1TransactionDetailTaxComponent;
  let fixture: ComponentFixture<Pnd1TransactionDetailTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pnd1TransactionDetailTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pnd1TransactionDetailTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
