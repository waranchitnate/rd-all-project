import { Component, OnInit } from '@angular/core';
import { PrintService } from '../../services/print.service';

@Component({
    selector: 'app-pnd1-transaction-detail-tax',
    templateUrl: './pnd1-transaction-detail-tax.component.html',
    styleUrls: ['./pnd1-transaction-detail-tax.component.css']
})
export class Pnd1TransactionDetailTaxComponent implements OnInit {

    constructor(private ps: PrintService) { }

    ngOnInit() {
        this.ps.setConfig({
            paper: 'A4',
            direction: 'แนวตั้ง'
        });
    }

}
