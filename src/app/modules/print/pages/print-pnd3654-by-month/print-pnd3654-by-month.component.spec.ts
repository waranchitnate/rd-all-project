import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintPnd3654ByMonthComponent } from './print-pnd3654-by-month.component';

describe('PrintPnd3654ByMonthComponent', () => {
  let component: PrintPnd3654ByMonthComponent;
  let fixture: ComponentFixture<PrintPnd3654ByMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintPnd3654ByMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintPnd3654ByMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
