import { Component, OnInit } from '@angular/core';
import { PrintService } from '../../services/print.service';

@Component({
  selector: 'app-print-pnd3654-by-month',
  templateUrl: './print-pnd3654-by-month.component.html',
  styleUrls: ['./print-pnd3654-by-month.component.css']
})
export class PrintPnd3654ByMonthComponent implements OnInit {

  constructor(private ps: PrintService) { }

    ngOnInit() {
        this.ps.setConfig({
            paper: 'A4',
            direction: 'แนวตั้ง'
        });
    }

}
