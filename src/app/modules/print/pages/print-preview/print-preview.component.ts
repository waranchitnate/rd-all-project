import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PrintService } from '../../services/print.service';
import { PrintConfig } from '../../models/print-config.model';
import * as printJS from 'print-js';

@Component({
    selector: 'app-print-preview',
    templateUrl: './print-preview.component.html',
    styleUrls: ['./print-preview.component.css']
})
export class PrintPreviewComponent implements OnInit {

    config: PrintConfig;
    
    constructor(private ps: PrintService) { }

    ngOnInit() {
        this.ps.getConfig().subscribe(data => {
            if (data) this.config = data;
        });
    }

    print() {
        if (this.config.url) printJS({printable: this.config.url});
        // window.print();
    }

}
