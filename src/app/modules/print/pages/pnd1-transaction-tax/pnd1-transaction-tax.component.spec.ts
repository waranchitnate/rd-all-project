import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pnd1TransactionTaxComponent } from './pnd1-transaction-tax.component';

describe('Pnd1TransactionTaxComponent', () => {
  let component: Pnd1TransactionTaxComponent;
  let fixture: ComponentFixture<Pnd1TransactionTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pnd1TransactionTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pnd1TransactionTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
