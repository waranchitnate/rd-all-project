import { Component, OnInit } from '@angular/core';
import { PrintService } from '../../services/print.service';

@Component({
    selector: 'app-pnd1-transaction-tax',
    templateUrl: './pnd1-transaction-tax.component.html',
    styleUrls: ['./pnd1-transaction-tax.component.css']
})
export class Pnd1TransactionTaxComponent implements OnInit {

    constructor(private ps: PrintService) { }

    ngOnInit() {
        this.ps.setConfig({
            paper: 'A4',
            direction: 'แนวนอน'
        });
    }

}
