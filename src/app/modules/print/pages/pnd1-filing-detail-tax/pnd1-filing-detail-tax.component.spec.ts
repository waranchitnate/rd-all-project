import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pnd1FilingDetailTaxComponent } from './pnd1-filing-detail-tax.component';

describe('Pnd1FilingDetailTaxComponent', () => {
  let component: Pnd1FilingDetailTaxComponent;
  let fixture: ComponentFixture<Pnd1FilingDetailTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pnd1FilingDetailTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pnd1FilingDetailTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
