import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialDetailPerBankComponent } from './special-detail-per-bank.component';

describe('SpecialDetailPerBankComponent', () => {
  let component: SpecialDetailPerBankComponent;
  let fixture: ComponentFixture<SpecialDetailPerBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialDetailPerBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialDetailPerBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
