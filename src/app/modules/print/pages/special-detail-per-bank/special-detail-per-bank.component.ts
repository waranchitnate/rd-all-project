import { Component, OnInit } from '@angular/core';
import { PrintService } from '../../services/print.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-special-detail-per-bank',
    templateUrl: './special-detail-per-bank.component.html',
    styleUrls: ['./special-detail-per-bank.component.css']
})
export class SpecialDetailPerBankComponent implements OnInit {

    urlPdf: string = 'http://localhost:8083/rd-internal-dss/specific/form/download?receiverProxy=506955348&year=2019&receiverFilingCode=006';

    constructor(private ps: PrintService, private ar: ActivatedRoute) {
        console.log(this.ar.snapshot.params.pin);
    }

    ngOnInit() {
        this.ps.setConfig({
            paper: 'A4',
            direction: 'แนวตั้ง',
            url: this.urlPdf
        });
    }

}
