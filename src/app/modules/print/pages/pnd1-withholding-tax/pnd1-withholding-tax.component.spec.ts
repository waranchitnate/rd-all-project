import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pnd1WithholdingTaxComponent } from './pnd1-withholding-tax.component';

describe('Pnd1WithholdingTaxComponent', () => {
  let component: Pnd1WithholdingTaxComponent;
  let fixture: ComponentFixture<Pnd1WithholdingTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pnd1WithholdingTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pnd1WithholdingTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
