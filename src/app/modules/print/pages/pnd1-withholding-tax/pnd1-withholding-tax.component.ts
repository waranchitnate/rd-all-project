import { Component, OnInit } from '@angular/core';
import { PrintService } from '../../services/print.service';

@Component({
    selector: 'app-pnd1-withholding-tax',
    templateUrl: './pnd1-withholding-tax.component.html',
    styleUrls: ['./pnd1-withholding-tax.component.css']
})
export class Pnd1WithholdingTaxComponent implements OnInit {

    constructor(private ps: PrintService) { }

    ngOnInit() {
        this.ps.setConfig({
            paper: 'A4',
            direction: 'แนวตั้ง'
        });
    }

}
