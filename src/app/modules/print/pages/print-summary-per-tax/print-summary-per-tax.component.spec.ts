import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintSummaryPerTaxComponent } from './print-summary-per-tax.component';

describe('PrintSummaryPerTaxComponent', () => {
  let component: PrintSummaryPerTaxComponent;
  let fixture: ComponentFixture<PrintSummaryPerTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintSummaryPerTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintSummaryPerTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
