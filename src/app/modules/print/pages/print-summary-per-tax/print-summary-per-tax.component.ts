import { Component, OnInit } from '@angular/core';
import { PrintService } from '../../services/print.service';

@Component({
    selector: 'app-print-summary-per-tax',
    templateUrl: './print-summary-per-tax.component.html',
    styleUrls: ['./print-summary-per-tax.component.css']
})
export class PrintSummaryPerTaxComponent implements OnInit {

    constructor(private ps: PrintService) { }

    ngOnInit() {
        this.ps.setConfig({
            paper: 'A4',
            direction: 'แนวตั้ง'
        });
    }

}
