import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrintPreviewComponent } from './pages/print-preview/print-preview.component';
import { Pnd1WithholdingTaxComponent } from './pages/pnd1-withholding-tax/pnd1-withholding-tax.component';
import { PrintSummaryPerTaxComponent } from './pages/print-summary-per-tax/print-summary-per-tax.component';
import { Pnd1FilingTaxComponent } from './pages/pnd1-filing-tax/pnd1-filing-tax.component';
import { Pnd1TransactionTaxComponent } from './pages/pnd1-transaction-tax/pnd1-transaction-tax.component';
import { Pnd1FilingDetailTaxComponent } from './pages/pnd1-filing-detail-tax/pnd1-filing-detail-tax.component';
import { Pnd1TransactionDetailTaxComponent } from './pages/pnd1-transaction-detail-tax/pnd1-transaction-detail-tax.component';
import { PrintPnd3654ByMonthComponent } from './pages/print-pnd3654-by-month/print-pnd3654-by-month.component';
import { SpecialDetailPerBankComponent } from './pages/special-detail-per-bank/special-detail-per-bank.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'preview' },
    { 
        path: 'preview', 
        component: PrintPreviewComponent,
        children: [
            { path: 'summary-per-tax', component: PrintSummaryPerTaxComponent },
            { path: 'pnd1-wht', component: Pnd1WithholdingTaxComponent },
            { path: 'pnd1-filing-tax', component: Pnd1FilingTaxComponent },
            { path: 'pnd1-filing-detail-tax', component: Pnd1FilingDetailTaxComponent },
            { path: 'pnd1-transaction-tax', component: Pnd1TransactionTaxComponent },
            { path: 'pnd1-transaction-detail-tax', component: Pnd1TransactionDetailTaxComponent },
            { path: 'pnd3654-by-month', component: PrintPnd3654ByMonthComponent },
            { path: 'special-detail-per-bank', component: SpecialDetailPerBankComponent },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PrintRoutingModule { }
