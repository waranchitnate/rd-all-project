import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuSearchComponent} from './pages/menu-search/menu-search.component';
import {AuthGuard} from '../../../core/guard/auth.guard';
import {MenuFormComponent} from './pages/menu-form/menu-form.component';

const routes: Routes = [
  { path: 'menu/search', component: MenuSearchComponent, canActivate: [AuthGuard] },
  { path: 'menu/form/add', component: MenuFormComponent, canActivate: [AuthGuard] },
  { path: 'menu/form/edit/:menuId', component: MenuFormComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRoutingModule { }
