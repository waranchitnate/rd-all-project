import { Menu } from '../models/menu.model';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private url = environment.webApiUrl + 'menu';
  constructor(private http: HttpClient) { }

  searchMenuByCondition(condition: Menu): Observable<any> {
    return this.http.post(this.url + '/searchMenuByCondition', condition);
  }

  saveMenu(menu: Menu): Observable<any> {
    return this.http.post(this.url + '/saveMenu', menu);
  }

  getMenuById(menuId: number): Observable<any> {
    return this.http.get(this.url + '/getMenuById?menuId=' + menuId);
  }

  deleteMenu(menu: Menu): Observable<any> {
    return this.http.post(this.url + '/deleteMenu', menu);
  }

  deleteMenuList(menuList: Array<Menu>): Observable<any> {
    return this.http.post(this.url + '/deleteMenuList', menuList);
  }
}
