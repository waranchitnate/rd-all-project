import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuSearchComponent} from './pages/menu-search/menu-search.component';
import {MenuFormComponent} from './pages/menu-form/menu-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../shared/shared.module';
import {MenuService} from './services/menu.service';

@NgModule({
  declarations: [MenuSearchComponent, MenuFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [MenuService]
})
export class MenuModule { }
