import { PageRequest } from '../../../../shared/models/page-request';

export class Menu {

  menuId: number;
  menuLevel: number;
  menuCode: string;
  menuName: string;
  menuUrl: string;
  actionCode: string;
  actionDesc: string;
  permissionName: string;
  pageRequestDto: PageRequest;
}
