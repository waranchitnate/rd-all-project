import { MenuService } from '../../services/menu.service';
import { Menu } from '../../models/menu.model';
import { Component, OnInit } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { ModalService } from 'src/app/shared/services/modal.service';

@Component({
  selector: 'app-menu-search',
  templateUrl: './menu-search.component.html',
  styleUrls: ['./menu-search.component.css']
})
export class MenuSearchComponent implements OnInit {
  menuList = <Menu[]>[];
  pageResponse = new PageResponse<any>();
  condition: Menu;
  totalRec: number;
  page: number;
  constructor(private menuService: MenuService, private modalService: ModalService) { }

  ngOnInit() {
    this.setDefaultPaging();
    this.getMenus();
  }

  pageChanged(evt) {
    this.page = evt;
    this.condition.pageRequestDto.page = --evt;
    this.getMenus();
  }

  getMenus() {
    console.log(this.condition);
    this.menuService.searchMenuByCondition(this.condition).subscribe(response => {
      this.condition.pageRequestDto.page = this.page;
      this.menuList = response.data.content;
      this.totalRec = response.data.totalElements;
    }, err => {
      this.modalService.openModal('ไม่สำเร็จ', 'ไม่สามารถค้นหาข้อมูลได้');
      const subscribe = this.modalService.modalService.onHidden.subscribe(reason => {
        subscribe.unsubscribe();
      });
    });
  }

  clearSearchCondition() {
    this.condition = undefined;
    this.setDefaultPaging();
    this.getMenus();
  }

  setDefaultPaging() {
    this.page = 1;
    this.condition = new Menu();
    this.condition.pageRequestDto = new PageRequest();
    this.condition.pageRequestDto.sortFieldName = 'menu_code';
    this.condition.pageRequestDto.page = this.page;
    this.condition.pageRequestDto.pageSize = 5;
  }

  deleteMenu(menu: Menu){
    this.menuService.deleteMenu(menu).subscribe(response => {
      this.getMenus();
    }, err => {
      this.modalService.openModal('ไม่สำเร็จ', 'ไม่สามารถลบข้อมูลได้');
      const subscribe = this.modalService.modalService.onHidden.subscribe(reason => {
        subscribe.unsubscribe();
      });
    });
  }

}
