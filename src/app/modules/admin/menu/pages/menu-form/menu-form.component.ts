import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormControl } from '@angular/forms';
import { MenuService } from '../../services/menu.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalService } from 'src/app/shared/services/modal.service';

@Component({
  selector: 'app-menu-form',
  templateUrl: './menu-form.component.html',
  styleUrls: ['./menu-form.component.css']
})
export class MenuFormComponent implements OnInit {
  isLoading = false;
  menuForm = this.fb.group({
    menuCode:  ['', Validators.required],
    menuName: ['', Validators.required],
    menuUrl: ['', Validators.required],
    menuId: ['']
  });

  constructor(private fb: FormBuilder, private menuService: MenuService, private router: Router
    , private modalService: ModalService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params['menuId'];
    console.log('form init : ' + id);
    if (id !== undefined && id !== null) {
      this.getMenuById(id);
    }
  }

  getMenuById(menuId: number) {
    this.menuService.getMenuById(menuId).subscribe(response => {
      console.log('getMenuById : ' + response);
      this.menuForm.patchValue(response.data);
    }, err => {
      this.modalService.openModal('ไม่สำเร็จ', 'ไม่สามารถค้นหาข้อมูลได้');
      const subscribe = this.modalService.modalService.onHidden.subscribe(reason => {
        subscribe.unsubscribe();
      });
    });
  }

  onSubmit() {
    this.isLoading = true;
    console.warn(this.menuForm.value);
    this.menuService.saveMenu(this.menuForm.value).subscribe(res => {
      this.modalService.openModal('สำเร็จ', 'บันทึกสำเร็จ');
      const subscribe = this.modalService.modalService.onHidden.subscribe(reason => {
        subscribe.unsubscribe();
        this.router.navigateByUrl('/admin/menu/search');
        console.log('success');
      });
    }, err => {
      this.modalService.openModal('ไม่สำเร็จ', 'บันทึกไม่สำเร็จ');
      const subscribe = this.modalService.modalService.onHidden.subscribe(reason => {
        subscribe.unsubscribe();
      });
    });
  }

  get menuCode() { return this.menuForm.get('menuCode'); }
  get menuName() { return this.menuForm.get('menuName'); }
  get menuUrl() { return this.menuForm.get('menuUrl'); }
}
