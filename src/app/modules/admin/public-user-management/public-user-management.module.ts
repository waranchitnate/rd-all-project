import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PublicUserManagementRoutingModule} from './public-user-management-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {ManageEstateUserStatusSearchComponent} from './pages/manage-estate-user-status-search/manage-estate-user-status-search.component';
import {ManagePartnershipUserStatusSearchComponent} from './pages/manage-partnership-user-status-search/manage-partnership-user-status-search.component';
import {ManageThaiJuristicPersonUserStatusSearchComponent} from './pages/manage-thai-juristic-person-user-status-search/manage-thai-juristic-person-user-status-search.component';
import {ManageSenderUserStatusSearchComponent} from './pages/manage-sender-user-status-search/manage-sender-user-status-search.component';
import {ManageForeignIndividualUserStatusSearchComponent} from './pages/manage-foreign-individual-user-status-search/manage-foreign-individual-user-status-search.component';
import { RegisteredAttachmentManagementComponent } from './pages/registered-attachment-management/registered-attachment-management.component';
import {MRegisteredAttachmentService} from './services/m-registered-attachment.service';
import { MRegisteredAttachmentFormComponent } from './modals/m-registered-attachment-form/m-registered-attachment-form.component';
import { AttachmentNameValidatorDirective } from './directives/attachment-name-validator.directive';
import { AttachmentNameEnValidatorDirective } from './directives/attachment-name-en-validator.directive';
import { ManageThaiIndividualStatusSearchComponent } from './pages/manage-thai-individual-status-search/manage-thai-individual-status-search.component';
import { ApproveUserAttachmentTableComponent } from './components/approve-user-attachment-table/approve-user-attachment-table.component';
import {PublicUserService} from './services/public-user.service';
import {ManagePublicUserService} from './services/manage-public-user.service';
import { ApproveUserDocumentComponent } from './pages/approve-user-document/approve-user-document.component';
import { ApproveUserComponent } from './pages/approve-user/approve-user.component';
import { ViewUserInformationComponent } from './pages/view-user-information/view-user-information.component';
import { ManageOrdinaryPartnershipUserComponent } from './pages/manage-ordinary-partnership-user/manage-ordinary-partnership-user.component';
import { UserRegistrationLogTableComponent } from './components/user-registration-log-table/user-registration-log-table.component';
import { ManageAffiliationUserStatusSearchComponent } from './pages/manage-affiliation-user-status-search/manage-affiliation-user-status-search.component';
import { ManageUserStatusMoiComponent } from './pages/manage-user-status-moi/manage-user-status-moi.component';


@NgModule({
  declarations: [
    ManageEstateUserStatusSearchComponent
    , ManagePartnershipUserStatusSearchComponent
    , ManageThaiJuristicPersonUserStatusSearchComponent
    , ManageSenderUserStatusSearchComponent
    , ManageForeignIndividualUserStatusSearchComponent
    , RegisteredAttachmentManagementComponent
    , MRegisteredAttachmentFormComponent
    , AttachmentNameValidatorDirective
    , AttachmentNameEnValidatorDirective
    , ManageThaiIndividualStatusSearchComponent
    , ApproveUserAttachmentTableComponent
    , ApproveUserDocumentComponent
    , ApproveUserComponent
    , ViewUserInformationComponent
    , ManageOrdinaryPartnershipUserComponent
    , UserRegistrationLogTableComponent
    , ManageAffiliationUserStatusSearchComponent
    , ManageUserStatusMoiComponent
  ],
  imports: [
    CommonModule,
    PublicUserManagementRoutingModule,
    SharedModule
  ],
  providers: [MRegisteredAttachmentService
    , PublicUserService
    , ManagePublicUserService
  ],
  entryComponents : [
    MRegisteredAttachmentFormComponent
  ]
})
export class PublicUserManagementModule {
}
