import {Component, Input, OnInit} from '@angular/core';
import {UserRegistrationLogModel} from '../../../../../shared/models/user-registration-log.model';

@Component({
  selector: 'app-user-registration-log-table',
  templateUrl: './user-registration-log-table.component.html'
})
export class UserRegistrationLogTableComponent implements OnInit {

  @Input() userRegistrationLogList: Array<UserRegistrationLogModel> = [];
  constructor() { }

  ngOnInit() {
  }

}
