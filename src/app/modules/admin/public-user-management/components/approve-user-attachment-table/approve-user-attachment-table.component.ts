import {Component, Input, OnInit} from '@angular/core';
import {UserAttachmentModel} from '../../models/user-attachment.model';
import {MRegisteredAttachmentModel} from '../../models/m-registered-attachment.model';
import {MRegisteredAttachmentService} from '../../services/m-registered-attachment.service';
import {FileService} from '../../../../../shared/services/file.service';
import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {forkJoin} from 'rxjs';
import {UserAttachmentService} from '../../../../../core/services/user-attachment.service';
import {environment} from '../../../../../../environments/environment';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {ScannedUserAttachmentModel} from '../../models/scanned-user-attachment.model';

@Component({
  selector: 'app-approve-user-attachment-table',
  templateUrl: './approve-user-attachment-table.component.html',
  styleUrls: ['./approve-user-attachment-table.component.css']
})
export class ApproveUserAttachmentTableComponent implements OnInit {

  @Input() hashUserId: string;
  @Input() userTypeKey: string;
  @Input() username: string;
  @Input() approvedDocumentForm: FormGroup;
  @Input() reUploadDoc = false;
  fileApi = environment.internalUserManagementApiUrl + 'file/download-user-attachment/';
  fileScanApi = environment.internalUserManagementApiUrl + 'file/download-scan-attachment/';
  userAttachmentDtoList: Array<UserAttachmentModel>;
  registeredAttachmentList: Array<MRegisteredAttachmentModel> = [];
  scanFileList: any[] = [];
  refScanModal: NgbModalRef;
  registeredAttachment: MRegisteredAttachmentModel;
  userAttachmentModel: UserAttachmentModel;
  scannedUserAttachmentList: Array<ScannedUserAttachmentModel> = [];

  constructor(
    private mRegisteredAttachmentService: MRegisteredAttachmentService,
    private fileService: FileService,
    private userAttachmentService: UserAttachmentService,
    private modalService: NgbModal,
    private toastrService: ToastrService
  ) {
  }

  ngOnInit() {
    forkJoin({
      userAttachmentList: this.userAttachmentService.findAllByUserId(this.hashUserId)
      ,
      registeredAttachmentLust: this.mRegisteredAttachmentService.getRequiredAttachmentsByUserTypeKey(this.userTypeKey)
    })
      .subscribe(res => {
        this.userAttachmentDtoList = res.userAttachmentList;
        this.setUserAttachmentDtoListFormArray(res.registeredAttachmentLust);
        this.setUserAttachmentIdToMRegisteredAttachment(res.registeredAttachmentLust);
        this.registeredAttachmentList = res.registeredAttachmentLust;
      });

    this.getScanAttachFile();

  }

  getScanAttachFile() {
    this.userAttachmentService.findScanAttachAllByUserId(this.hashUserId).subscribe(res => {
      this.scannedUserAttachmentList = res;
    });
  }

  setUserAttachmentIdToMRegisteredAttachment(registeredAttachmentList: Array<MRegisteredAttachmentModel>) {
    registeredAttachmentList.forEach(registeredAttachment => {
      const userAttachment = this.userAttachmentDtoList.find(item => item.mRegisteredAttachmentId === registeredAttachment.id);
      if (userAttachment !== undefined) {
        registeredAttachment.userAttachmentDto = userAttachment;
      } else {
        registeredAttachment.userAttachmentDto = new UserAttachmentModel();
      }
    });
  }

  addApproveStatus(model: MRegisteredAttachmentModel, index: number) {
    model.userAttachmentDto.addApprovedStatus = true;
    this.getApprovedControl(index).setValue(null);
  }

  setUserAttachmentDtoListFormArray(registeredAttachmentList: Array<MRegisteredAttachmentModel>) {
    registeredAttachmentList.forEach(registeredAttachment => {
      const userAttachmentDto = this.userAttachmentDtoList.find(userAttachment => userAttachment.mRegisteredAttachmentId === registeredAttachment.id);
      this.userAttachmentDtoListFormArray.push(new FormGroup({
        userAttachmentId: new FormControl(userAttachmentDto ? userAttachmentDto.userAttachmentId : null),
        mRegisteredAttachmentId: new FormControl(registeredAttachment.id),
        approved: new FormControl(this.attachmentAttachedAndConsidered(userAttachmentDto) ? userAttachmentDto.approved : true, Validators.required)
      }));
    });
  }

  attachmentAttachedAndConsidered(userAttachmentDto: UserAttachmentModel): boolean {
    return userAttachmentDto && userAttachmentDto.attach && userAttachmentDto.approved;
  }

  getApprovedControl(index: number): AbstractControl {
    return this.userAttachmentDtoListFormArray.at(index).get('approved');
  }

  get userAttachmentDtoListFormArray(): FormArray {
    return this.approvedDocumentForm.get('userAttachmentDtoList') as FormArray;
  }

  openScanModal(parentRegisteredAttachment, scanModal, parentUserAttachmentModel) {
    this.refScanModal = this.modalService.open(scanModal, {size: 'lg', backdrop: 'static'});
    this.registeredAttachment = parentRegisteredAttachment;
    this.userAttachmentModel = parentUserAttachmentModel;
  }

  closeScanModal() {
    this.getScanAttachFile();
    this.refScanModal.close();
  }

  upload(base64Pdf) {
    // const filename =
    // const fileExtension =
    // const dataBase64 =
    // this.fileService.uploadScannedAttachment(this.username, fileName, fileExtension, dataBase64).subscribe(() => {
    //   const fileModel: FileModel = <FileModel>{
    //     fileBaseName = fileName,
    //     fileExtension = fileExtension
    //   };
    //   const scannedUserAttachment: ScannedUserAttachmentModel = new ScannedUserAttachmentModel();
    //   scannedUserAttachment.userId = this.userId;
    //   scannedUserAttachment.mRegisteredAttachmentId =
    //     scannedUserAttachment.fileDto = fileModel;
    //   this.userAttachmentService.addScannedAttachment(scannedUserAttachment, this.username).subscribe(() => {
    //     this.toastrService.success('เพิ่มเอกสารสำเร็จ');
    //   }, err => {
    //     this.toastrService.error('เพิ่มเอกสารไม้สำรเร็จเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
    //   });
    // }, err => {
    //   this.toastrService.error('อัพโหลดเอกสารไม่สำเร็จเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
    // });

    this.refScanModal.close();
  }
}
