import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ManageEstateUserStatusSearchComponent} from './pages/manage-estate-user-status-search/manage-estate-user-status-search.component';
import {ManagePartnershipUserStatusSearchComponent} from './pages/manage-partnership-user-status-search/manage-partnership-user-status-search.component';
import {ManageThaiJuristicPersonUserStatusSearchComponent} from './pages/manage-thai-juristic-person-user-status-search/manage-thai-juristic-person-user-status-search.component';
import {ManageSenderUserStatusSearchComponent} from './pages/manage-sender-user-status-search/manage-sender-user-status-search.component';
import {ManageForeignIndividualUserStatusSearchComponent} from './pages/manage-foreign-individual-user-status-search/manage-foreign-individual-user-status-search.component';
import {RegisteredAttachmentManagementComponent} from './pages/registered-attachment-management/registered-attachment-management.component';
import {ManageThaiIndividualStatusSearchComponent} from './pages/manage-thai-individual-status-search/manage-thai-individual-status-search.component';
import {ApproveUserDocumentComponent} from './pages/approve-user-document/approve-user-document.component';
import {ApproveUserComponent} from './pages/approve-user/approve-user.component';
import {ViewUserInformationComponent} from './pages/view-user-information/view-user-information.component';
import {ManageOrdinaryPartnershipUserComponent} from './pages/manage-ordinary-partnership-user/manage-ordinary-partnership-user.component';
import {ManageAffiliationUserStatusSearchComponent} from './pages/manage-affiliation-user-status-search/manage-affiliation-user-status-search.component';
import { ManageUserStatusMoiComponent } from './pages/manage-user-status-moi/manage-user-status-moi.component';

const routes: Routes = [
  {path: 'manage-user-status/estate/search', component: ManageEstateUserStatusSearchComponent, data: {programCode: 'UMSP040103'}},
  {path: 'manage-user-status/estate/approve-document/:hashUserId', component: ApproveUserDocumentComponent, data: {userTypeKey: 'H', programCode: 'UMSP040103'}},
  {path: 'manage-user-status/estate/approve/:hashUserId', component: ApproveUserComponent, data: {userTypeKey: 'H', programCode: 'UMSP040103'}},
  {path: 'manage-user-status/estate/view/:hashUserId', component: ViewUserInformationComponent, data: {userTypeKey: 'H', programCode: 'UMSP040103'}},
  {path: 'manage-user-status/partnership/search', component: ManagePartnershipUserStatusSearchComponent, data: {programCode: 'UMSP040104'}},
  {path: 'manage-user-status/partnership/approve-document/:hashUserId', component: ApproveUserDocumentComponent, data: {userTypeKey: 'G', programCode: 'UMSP040104'}},
  {path: 'manage-user-status/partnership/approve/:hashUserId', component: ApproveUserComponent, data: {userTypeKey: 'G', programCode: 'UMSP040104'}},
  {path: 'manage-user-status/partnership/view/:hashUserId', component: ViewUserInformationComponent, data: {userTypeKey: 'G', programCode: 'UMSP040104'}},
  {path: 'manage-user-status/ordinary-partnership/search', component: ManageOrdinaryPartnershipUserComponent, data: {programCode: 'UMSP040110'}},
  {path: 'manage-user-status/ordinary-partnership/approve-document/:hashUserId', component: ApproveUserDocumentComponent, data: {userTypeKey: 'O', programCode: 'UMSP040110'}},
  {path: 'manage-user-status/ordinary-partnership/approve/:hashUserId', component: ApproveUserComponent, data: {userTypeKey: 'O', programCode: 'UMSP040110'}},
  {path: 'manage-user-status/ordinary-partnership/view/:hashUserId', component: ViewUserInformationComponent, data: {userTypeKey: 'O', programCode: 'UMSP040110'}},
  {path: 'manage-user-status/thai-juristic-person/search', component: ManageThaiJuristicPersonUserStatusSearchComponent, data: {programCode: 'UMSP040105'}},
  {path: 'manage-user-status/thai-juristic-person/approve-document/:hashUserId', component: ApproveUserDocumentComponent, data: {userTypeKey: 'C', programCode: 'UMSP040105'}},
  {path: 'manage-user-status/thai-juristic-person/approve/:hashUserId', component: ApproveUserComponent, data: {userTypeKey: 'C', programCode: 'UMSP040105'}},
  {path: 'manage-user-status/thai-juristic-person/view/:hashUserId', component: ViewUserInformationComponent, data: {userTypeKey: 'C', programCode: 'UMSP040105'}},
  {path: 'manage-user-status/affiliation/search', component: ManageAffiliationUserStatusSearchComponent, data: {userTypeKey: 'A', programCode: 'UMSP040111'}},
  {path: 'manage-user-status/affiliation/approve-document/:hashUserId', component: ApproveUserDocumentComponent, data: {userTypeKey: 'A', programCode: 'UMSP040111'}},
  {path: 'manage-user-status/affiliation/approve/:hashUserId', component: ApproveUserComponent, data: {userTypeKey: 'A', programCode: 'UMSP040111'}},
  {path: 'manage-user-status/affiliation/view/:hashUserId', component: ViewUserInformationComponent, data: {userTypeKey: 'A', programCode: 'UMSP040111'}},
  {path: 'manage-user-status/sender/search', component: ManageSenderUserStatusSearchComponent, data: {programCode: 'UMSP040108'}},
  {path: 'manage-user-status/sender/approve-document/:hashUserId', component: ApproveUserDocumentComponent, data: {userTypeKey: 'S', programCode: 'UMSP040108'}},
  {path: 'manage-user-status/sender/approve/:hashUserId', component: ApproveUserComponent, data: {userTypeKey: 'S', programCode: 'UMSP040108'}},
  {path: 'manage-user-status/sender/view/:hashUserId', component: ViewUserInformationComponent, data: {userTypeKey: 'S', programCode: 'UMSP040108'}},
  {path: 'manage-user-status/foreign-individual/search', component: ManageForeignIndividualUserStatusSearchComponent, data: {programCode: 'UMSP040102'}},
  {path: 'manage-user-status/foreign-individual/approve-document/:hashUserId', component: ApproveUserDocumentComponent, data: {userTypeKey: 'F', programCode: 'UMSP040102'}},
  {path: 'manage-user-status/foreign-individual/approve/:hashUserId', component: ApproveUserComponent, data: {userTypeKey: 'F', programCode: 'UMSP040102'}},
  {path: 'manage-user-status/foreign-individual/view/:hashUserId', component: ViewUserInformationComponent, data: {userTypeKey: 'F', programCode: 'UMSP040102'}},
  {path: 'manage-user-status/thai-individual/search', component: ManageThaiIndividualStatusSearchComponent, data: {programCode: 'UMSP040109'}},
  {path: 'manage-user-status/thai-individual/view/:hashUserId', component: ViewUserInformationComponent, data: {userTypeKey: 'T', programCode: 'UMSP040109'}},
  {path: 'manage-user-status-moi', component: ManageUserStatusMoiComponent, data: {programCode: 'UMSP070100'}},
  {path: 'registered-attachment-management', component: RegisteredAttachmentManagementComponent, data: {programCode: 'UMSP040101'}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicUserManagementRoutingModule {
}
