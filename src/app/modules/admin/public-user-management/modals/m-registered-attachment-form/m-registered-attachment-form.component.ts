import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {MRegisteredAttachmentService} from '../../services/m-registered-attachment.service';
import {NameValuePairModel} from '../../../../../shared/models/NameValuePair.model';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertModalComponent} from '../../../../../shared/modals/alert-modal/alert-modal.component';
import {HttpErrorResponse} from '@angular/common/http';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-registered-attachment-form',
  templateUrl: './m-registered-attachment-form.component.html',
  styleUrls: ['./m-registered-attachment-form.component.css']
})
export class MRegisteredAttachmentFormComponent implements OnInit {

  id: number;
  requiredAttachmentUserType: Array<NameValuePairModel>;
  requiredAttachmentForm: FormGroup;
  @Output() answerEvent = new EventEmitter();

  constructor(public bsModelRef: BsModalRef, private mRegisteredAttachmentService: MRegisteredAttachmentService,
              private bsModalService: BsModalService) {
  }

  ngOnInit() {
    this.requiredAttachmentForm = this.initialNewForm();
    if (this.id !== null) {
      this.mRegisteredAttachmentService.getRequiredAttachment(this.id).subscribe(model => {
        this.requiredAttachmentForm.patchValue(model);
      }, (err: HttpErrorResponse) => {
        this.bsModalService.show(AlertModalComponent, {
          initialState: {
            title: 'พบปัญหาในการดึงข้อมูลเอกสาร',
            content: err.error.message
          }
        });
      });
    }
  }

  createOrEdit() {
    if (this.requiredAttachmentForm.invalid) {
      this.markFormGroupTouched(this.requiredAttachmentForm);
      this.bsModalService.show(AlertModalComponent, {
        initialState: {
          title: 'กรุณาระบุข้อมูลให้ถูกต้อง'
        }
      });
    } else if (this.id !== null) {
      this.mRegisteredAttachmentService.updateRequiredAttachment(this.id, this.requiredAttachmentForm.value)
        .subscribe(() => {
          this.bsModalService.show(AlertModalComponent, {
            initialState: {
              title: 'แก้ไขเอกสารสำเร็จ'
            }
          });
          this.bsModalService.onHide
            .pipe(take(1))
            .subscribe(() => {
              this.closeModal(true);
            });
        }, (err: HttpErrorResponse) => {
          this.bsModalService.show(AlertModalComponent, {
            initialState: {
              title: 'พบปัญหาในการแก้ไขเอกสาร',
              content: err.error.message
            }
          });
        });
    } else {
      this.mRegisteredAttachmentService.createRequiredAttachment(this.requiredAttachmentForm.value)
        .subscribe(() => {
          this.bsModalService.show(AlertModalComponent, {
            initialState: {
              title: 'สร้างเอกสารสำเร็จ'
            }
          });
          this.bsModalService.onHide
            .pipe(take(1))
            .subscribe(() => {
              this.closeModal(true);
            });
        }, (err: HttpErrorResponse) => {
          this.bsModalService.show(AlertModalComponent, {
            initialState: {
              title: 'พบปัญหาในการแก้ไขเอกสาร',
              content: err.error.message
            }
          });
        });
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    Object.values(formGroup.controls).forEach(control => {
      control.markAsTouched();
    });
  }

  initialNewForm(): FormGroup {
    return new FormGroup({
      id: new FormControl(null),
      userTypeKey: new FormControl(null, {updateOn: 'blur', validators: Validators.required}),
      attachmentName: new FormControl(null, {updateOn: 'blur', validators: [Validators.required, Validators.maxLength(255)]}),
      attachmentNameEn: new FormControl(null, {updateOn: 'blur', validators: [Validators.required, Validators.maxLength(255)]}),
      attachmentDesc: new FormControl(null, {updateOn: 'blur', validators: Validators.maxLength(512)}),
      required: new FormControl(false, {updateOn: 'submit'})
    });
  }

  closeModal(result: boolean) {
    this.answerEvent.next(result);
    this.bsModelRef.hide();
  }

  get userTypeKey(): AbstractControl {
    return this.requiredAttachmentForm.get('userTypeKey');
  }

  get attachmentName(): AbstractControl {
    return this.requiredAttachmentForm.get('attachmentName');
  }

  get attachmentNameEn(): AbstractControl {
    return this.requiredAttachmentForm.get('attachmentNameEn');
  }

  get attachmentDesc(): AbstractControl {
    return this.requiredAttachmentForm.get('attachmentDesc');
  }

}
