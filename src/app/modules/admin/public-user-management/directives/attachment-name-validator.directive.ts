import {Directive} from '@angular/core';
import {AsyncValidator, FormGroup, NG_ASYNC_VALIDATORS, ValidationErrors} from '@angular/forms';
import {catchError, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {MRegisteredAttachmentService} from '../services/m-registered-attachment.service';

@Directive({
  selector: '[appAttachmentNameValidator]',
  providers: [{provide: NG_ASYNC_VALIDATORS, useExisting: AttachmentNameValidatorDirective, multi: true}]
})
export class AttachmentNameValidatorDirective implements AsyncValidator {

  constructor(private mRegisteredAttachmentService: MRegisteredAttachmentService) {
  }

  validate(control: FormGroup): Observable<ValidationErrors | null> {
    return this.mRegisteredAttachmentService
      .isAttachmentNameAvailable(control.controls['userTypeKey'].value, control.controls['attachmentName'].value, control.controls['id'].value)
      .pipe(
        map(
          s => {
            return s ? null : {'attachmentNameIsNotAvailable': true};
          },
          catchError(
            () => null
          )
        )
      );
  }

}
