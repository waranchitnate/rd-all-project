import { Directive } from '@angular/core';
import {FormGroup, NG_ASYNC_VALIDATORS, ValidationErrors} from '@angular/forms';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {MRegisteredAttachmentService} from '../services/m-registered-attachment.service';

@Directive({
  selector: '[appAttachmentNameEnValidator]',
  providers: [{provide: NG_ASYNC_VALIDATORS, useExisting: AttachmentNameEnValidatorDirective, multi: true}]
})
export class AttachmentNameEnValidatorDirective {

  constructor(private mRegisteredAttachmentService: MRegisteredAttachmentService) { }

  validate(control: FormGroup): Observable<ValidationErrors | null> {
    return this.mRegisteredAttachmentService
      .isAttachmentNameEnAvailable(control.controls['userTypeKey'].value, control.controls['attachmentNameEn'].value, control.controls['id'].value)
      .pipe(
        map(
          s => {
            return s ? null : {'attachmentNameEnIsNotAvailable': true};
          },
          catchError(
            () => null
          )
        )
      );
  }

}
