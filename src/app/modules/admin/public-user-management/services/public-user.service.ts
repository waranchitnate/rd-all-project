import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PublicUserModel} from '../../../../shared/models/public-user.model';

@Injectable()
export class PublicUserService {

  private publicUserApiUrl = environment.internalUserManagementApiUrl + 'public-user/';

  constructor(private httpClient: HttpClient) {
  }

  public getPublicUserByUserType(userId: string, userType: string): Observable<PublicUserModel> {
    if (userType === 'T') {
      return this.getThaiIndividualDto(userId);
    } else if (userType === 'F') {
      return this.getForeignIndividual(userId);
    } else if (userType === 'H') {
      return this.getEstateIndividual(userId);
    } else if (userType === 'G') {
      return this.getPartnershipIndividual(userId);
    } else if (userType === 'O') {
      return this.getOrdinaryPartnershipIndividual(userId);
    } else if (userType === 'C') {
      return this.getThaiJuristicPerson(userId);
    } else if (userType === 'S') {
      return this.getSender(userId);
    } else if (userType === 'A') {
      return this.getAffiliation(userId);
    }
  }

  public getThaiIndividualDto(userId: string): Observable<PublicUserModel> {
    return this.httpClient.get<PublicUserModel>(this.publicUserApiUrl + 'thai-individual-dto/' + userId);
  }

  public getForeignIndividual(userId: string): Observable<PublicUserModel> {
    return this.httpClient.get<PublicUserModel>(this.publicUserApiUrl + 'foreign-individual/' + userId);
  }

  public getEstateIndividual(userId: string): Observable<PublicUserModel> {
    return this.httpClient.get<PublicUserModel>(this.publicUserApiUrl + 'estate-individual/' + userId);
  }

  public getPartnershipIndividual(userId: string): Observable<PublicUserModel> {
    return this.httpClient.get<PublicUserModel>(this.publicUserApiUrl + 'partnership-individual/' + userId);
  }

  public getOrdinaryPartnershipIndividual(userId: string): Observable<PublicUserModel> {
    return this.httpClient.get<PublicUserModel>(this.publicUserApiUrl + 'ordinary-partnership-individual/' + userId);
  }

  public getThaiJuristicPerson(userId: string): Observable<PublicUserModel> {
    return this.httpClient.get<PublicUserModel>(this.publicUserApiUrl + 'thai-juristic-person-dto/' + userId);
  }

  public getSender(userId: string): Observable<PublicUserModel> {
    return this.httpClient.get<PublicUserModel>(this.publicUserApiUrl + 'sender-dto/' + userId);
  }

  public getAffiliation(userId: string): Observable<PublicUserModel> {
    return this.httpClient.get<PublicUserModel>(this.publicUserApiUrl + 'affiliation-dto/' + userId);
  }

  public getUserTypeName(userType: string): string {
    let userTypeName;
    if (userType === 'T') {
      userTypeName = 'บุคคลธรรมดาสัญชาติไทย';
    } else if (userType === 'F') {
      userTypeName = 'บุคคลธรรมดาต่างด้าว';
    } else if (userType === 'H') {
      userTypeName = 'กองมรดกที่ยังมิได้แบ่ง';
    } else if (userType === 'G') {
      userTypeName = 'คณะบุคคล';
    } else if (userType === 'O') {
      userTypeName = 'ห้างหุ้นส่วนสามัญ';
    } else if (userType === 'C') {
      userTypeName = 'นิติบุคคลไทย';
    } else if (userType === 'I') {
      userTypeName = 'นิติบุคคลต่างด้าว';
    } else if (userType === 'M') {
      userTypeName = 'ผู้ประกอบการในเขตพื้นที่พัฒนาร่วมไทย - มาเลเซีย (นิติบุคคล)';
    } else if (userType === 'S') {
      userTypeName = 'ผู้ส่งข้อมูล';
    } else if (userType === 'A') {
      userTypeName = 'หน่วยงานต้นสังกัด';
    }
    return userTypeName;
  }
}
