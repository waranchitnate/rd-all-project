import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {MRegisteredAttachmentPageRequest} from '../models/m-registered-attachment.page-request';
import {Observable} from 'rxjs';
import {PageResponse} from '../../../../shared/models/page-response';
import {PageRequest} from '../../../../shared/models/page-request';
import {NameValuePairModel} from '../../../../shared/models/NameValuePair.model';
import {MRegisteredAttachmentModel} from '../models/m-registered-attachment.model';

@Injectable({
  providedIn: 'root'
})
export class MRegisteredAttachmentService {

  private mRegisterAttachmentApi = environment.internalUserManagementApiUrl + 'm-registered-attachment/';

  constructor(private httpClient: HttpClient) {
  }

  public pagination(pageRequest: MRegisteredAttachmentPageRequest): Observable<PageResponse<any>> {
    const copyPageRequest: PageRequest = new PageRequest();
    Object.assign(copyPageRequest, pageRequest);
    copyPageRequest.page -= 1;
    return this.httpClient.post<PageResponse<any>>(this.mRegisterAttachmentApi + 'pagination', copyPageRequest);
  }

  public getRequiredAttachmentUserType(): Observable<Array<NameValuePairModel>> {
    return this.httpClient.get<Array<NameValuePairModel>>(this.mRegisterAttachmentApi + 'required-attachment-user-type');
  }

  public getRequiredAttachment(id: number): Observable<MRegisteredAttachmentModel> {
    return this.httpClient.get<MRegisteredAttachmentModel>(this.mRegisterAttachmentApi + id);
  }

  public getRequiredAttachmentsByUserTypeKey(userTypeKey: string): Observable<Array<MRegisteredAttachmentModel>> {
    return this.httpClient.get<Array<MRegisteredAttachmentModel>>(this.mRegisterAttachmentApi + 'user-type-key/' + userTypeKey);
  }

  public createRequiredAttachment(model: MRegisteredAttachmentModel) {
    return this.httpClient.post(this.mRegisterAttachmentApi, model);
  }

  public updateRequiredAttachment(id: number, model: MRegisteredAttachmentModel) {
    return this.httpClient.put(this.mRegisterAttachmentApi + id, model);
  }

  public deleteRegisteredAttachment(id: number) {
    return this.httpClient.delete(this.mRegisterAttachmentApi + id);
  }

  public isAttachmentNameAvailable(userTypeKey: string, attachmentName: string, id: number): Observable<boolean> {
    let params = new HttpParams();
    params = params.append('user-type', userTypeKey);
    params = params.append('attachment-name', attachmentName);
    if (id !== null) {
      params = params.append('registered-attachment-id', id + '');
    }
    return this.httpClient.get<boolean>(this.mRegisterAttachmentApi + 'attachment-name-available', {params: params});
  }

  public isAttachmentNameEnAvailable(userTypeKey: string, attachmentName: string, id: number): Observable<boolean> {
    let params = new HttpParams();
    params = params.append('user-type', userTypeKey);
    params = params.append('attachment-name-en', attachmentName);
    if (id !== null) {
      params = params.append('registered-attachment-id', id + '');
    }
    return this.httpClient.get<boolean>(this.mRegisterAttachmentApi + 'attachment-name-en-available', {params: params});
  }
}
