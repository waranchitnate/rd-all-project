import {Injectable} from '@angular/core';
import {Observable, throwError as observableThrowError} from 'rxjs';
import {UserRegistrationDetailModel} from '../models/user-registration-detail.model';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ApproveUserDocumentModel} from '../models/approve-user-document.model';
import {catchError} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {PageRequest} from '../../../../shared/models/page-request';
import {PageResponse} from '../../../../shared/models/page-response';
import {ManagePublicUserStatusModel} from '../models/ManagePublicUserStatus.model';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {AnswerModalComponent} from '../../../../shared/modals/answer-modal/answer-modal.component';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap';

@Injectable()
export class ManagePublicUserService {

  private managePublicUserApi = environment.internalUserManagementApiUrl + 'manage-public-user/';

  constructor(private httpClient: HttpClient, private toastrService: ToastrService, private router: Router, private bsModalService: BsModalService) {
  }

  public pagination(pageRequest: PageRequest): Observable<PageResponse<ManagePublicUserStatusModel>> {
    const copyPageRequest: PageRequest = new PageRequest();
    Object.assign(copyPageRequest, pageRequest);
    copyPageRequest.page -= 1;
    return this.httpClient.post<PageResponse<ManagePublicUserStatusModel>>(this.managePublicUserApi + 'pagination', copyPageRequest);
  }

  public approveDocument(approveUserDocument: ApproveUserDocumentModel): Observable<any> {
    return this.httpClient.put<any>(this.managePublicUserApi + 'approve-document', approveUserDocument)
      .pipe(
        catchError(err => {
          this.toastrService.error('อนุมัติเอกสารผู้ใช้งานล้มเหลว', err.error.message);
          return observableThrowError(err);
        })
      );
  }

  public rejectDocument(approveUserDocument: ApproveUserDocumentModel): Observable<any> {
    return this.httpClient.put(this.managePublicUserApi + 'reject-document', approveUserDocument)
      .pipe(
        catchError(err => {
          this.toastrService.error('ปฏิเสธเอกสารผู้ใช้งานล้มเหลว', err.error.message);
          return observableThrowError(err);
        })
      );
  }

  public approveUser(userRegistrationDetail: UserRegistrationDetailModel): Observable<boolean> {
    return this.httpClient.put<boolean>(this.managePublicUserApi + 'approve-user', userRegistrationDetail)
      .pipe(
        catchError(err => {
          this.toastrService.error(err.error.message, 'พบปัญหาขณะอนุมัติผู้ใช้งาน');
          return observableThrowError(err);
        })
      );
  }

  public approveAndSendActivationEmail(userRegistrationDetail: UserRegistrationDetailModel): Observable<boolean> {
    return this.httpClient.put<boolean>(this.managePublicUserApi + 'approve-and-send-email', userRegistrationDetail)
      .pipe(
        catchError(err => {
          this.toastrService.error(err.error.message, 'พบปัญหาขณะอนุมัติผู้ใช้งาน');
          return observableThrowError(err);
        })
      );
  }

  public disableUser(userRegistrationDetail: UserRegistrationDetailModel) {
    return this.httpClient.put(this.managePublicUserApi + 'disable-user', userRegistrationDetail)
      .pipe(
        catchError(err => {
          console.log(err);
          this.toastrService.error('ยกเลิกผู้ใช้งานไม่สำเร็จเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
  }

  public resendActivationEmail(hashUserId: string) {
    return this.httpClient.post(this.managePublicUserApi + 're-send-activation-email/' + hashUserId, null)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.toastrService.error('ส่งอีเมลล์ยืนยันตัวตนไม่สำเร็จเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
  }

  public resendActivationEmailForSender(hashUserId: string) {
    return this.httpClient.post(this.managePublicUserApi + 're-send-activation-email-for-sender/' + hashUserId, null)
      .pipe(
        catchError(err => {
          this.toastrService.error('ส่งอีเมลยืนยันตัวตนไม่สำเร็จ ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
  }

  public resendApprovedDocumentEmail(hashUserId: string) {
    return this.httpClient.post(this.managePublicUserApi + 'resend-approve-document-email/' + hashUserId, null)
      .pipe(
        catchError(err => {
          this.toastrService.error('ส่งอีเมลแจ้งเตือนอนุมัติเอกสารล้มเหลวเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
  }

  public resendRejectedDocumentEmail(hashUserId: string) {
    return this.httpClient.post(this.managePublicUserApi + 'resend-rejected-document-email/' + hashUserId, null)
      .pipe(
        catchError(err => {
          this.toastrService.error('ส่งอีเมลแจ้งเตือนปฏิเสธอเอกสารล้มเหลวเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
  }

  public resendApproveUserEmail(hashUserId: string) {
    return this.httpClient.post(this.managePublicUserApi + 'resend-approve-user-email/' + hashUserId, null)
      .pipe(
        catchError(err => {
          this.toastrService.error('ส่งอีเมลอนุมัติผู้ใช้งานล้มเหลวเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
  }

  public resendSenderActivateSuccessEmail(hashUserId: string) {
    return this.httpClient.post(this.managePublicUserApi + 'resend-sender-activate-success-email/' + hashUserId, null)
      .pipe(
        catchError(err => {
          this.toastrService.error('ส่งอีเมลยืนยันตัวตนสำเร็จล้มเหลวเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
  }

  public resendDisapproveUserEmail(hashUserId: string) {
    return this.httpClient.post(this.managePublicUserApi + 'resend-disapprove-user-email/' + hashUserId, null)
      .pipe(
        catchError(err => {
          this.toastrService.error('ส่งอีเมลอนุมัติผู้ใช้งานล้มเหลวเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
  }

  public resendDisapproveFromDocStepEmail(hashUserId: string) {
    return this.httpClient.post(this.managePublicUserApi + 'resend-disapprove-from-doc-step-email/' + hashUserId, null)
      .pipe(
        catchError(err => {
          this.toastrService.error('ส่งอีเมลยุติคำร้องขอลงทะเบียนล้มเหลวเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
  }

  public initialApproveUserForm(hashUserId: string, programCode: string): FormGroup {
    return new FormGroup({
      approveUser: new FormControl(true),
      hashUserId: new FormControl(hashUserId),
      programCode: new FormControl(programCode),
      remark: new FormControl(null, Validators.maxLength(512))
    }, this.approveUserValidator);
  }

  public disapproveUserFromApproveUserDocumentPage(userRegistrationDetail: UserRegistrationDetailModel, activatedRoute: ActivatedRoute) {
    const modalRef = this.bsModalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
    (modalRef.content as AnswerModalComponent).content = 'คุณต้องไม่อนุมัติผู้ใช้งานใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        return this.httpClient.put<boolean>(this.managePublicUserApi + 'disapprove-user-from-doc-step', userRegistrationDetail)
          .subscribe(res => {
            this.toastrService.success('ไม่อนุมัติผู้ใช้งานสำเร็จ');
            this.router.navigate(['../../search'], {relativeTo: activatedRoute});
          }, error => {
            this.toastrService.error('ยกเลิกผู้ใช้งานไม่สำเร็จเนื่องจาก ' + error.error.message, 'ข้อผิดพลาด');
        });
      }
      sub.unsubscribe();
    });
  }

  public disApproveUser(userRegistrationDetail: UserRegistrationDetailModel, activatedRoute: ActivatedRoute) {
    const modalRef = this.bsModalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
    (modalRef.content as AnswerModalComponent).content = 'คุณต้องไม่อนุมัติผู้ใช้งานใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        return this.httpClient.put<boolean>(this.managePublicUserApi + 'disapprove-user', userRegistrationDetail)
          .subscribe(res => {
            this.toastrService.success('ไม่อนุมัติผู้ใช้งานสำเร็จ');
            this.router.navigate(['../../search'], {relativeTo: activatedRoute});
          }, error => {
            this.toastrService.error('ยกเลิกผู้ใช้งานไม่สำเร็จเนื่องจาก ' + error.error.message, 'ข้อผิดพลาด');
          });
      }
      sub.unsubscribe();
    });
  }


  approveUserValidator(control: AbstractControl): null | ValidationErrors {
    let validationResult = null;
    if (control !== undefined) {
      const approveUserForm = control as FormGroup;
      const remarkControl = approveUserForm.get('remark');
      if (!approveUserForm.get('approveUser').value) {
        if (remarkControl.value === null || remarkControl.value === '') {
          validationResult = {remarkRequired: 'กรุณาระบุเหตุผลไม่อนุมัติผู้ใช้งาน'};
        }
      } else {
        if (remarkControl.value) {
          remarkControl.setValue(null);
        }
      }
    }
    return validationResult;
  }
}
