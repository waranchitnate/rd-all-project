export class UserRegistrationDetailModel {
  hashUserId: string;
  registeredCode: string;
  registeredStatus: string;
  documentRejectedNote: string;
  remark: string;
  programCode: string;
}
