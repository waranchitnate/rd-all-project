import {FileModel} from './file.model';

export class ScannedUserAttachmentModel {
  scannedUserAttachmentId: number;
  mRegisteredAttachmentId: number;
  attach: string;
  userId: number;
  fileDto: FileModel;
}
