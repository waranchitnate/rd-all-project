export interface ManagePublicUserStatusModel {

  hashUserId: string;
  registeredCode: string;
  nId: string;
  affiliationCode: string;
  submitterPersonalNumber: string;
  firstName: string;
  lastName: string;
  fullName: string;
  companyName: string;
  registeredStatusName: string;
  registeredStatus: string;
  createdDate: Date;
  verifyType: string;
  registeredFrom: string;
  registeredFromName: string;
  reUploadDocument: boolean;
  dataTypeIdList: string;
  innerHTMLDataTypeName: string;

}
