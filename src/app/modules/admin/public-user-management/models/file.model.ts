import {SafeResourceUrl} from '@angular/platform-browser';

export interface FileModel {

  fileBaseName: string;
  fileExtension: string;
  dataBase64: string;
  contentType: string;
  safeResourceUrl?: SafeResourceUrl;
}
