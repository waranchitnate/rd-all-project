import {PageRequest} from '../../../../shared/models/page-request';

export class ManagePublicUserPageRequest extends PageRequest{
  registeredCode: string;
  nId: string;
  affiliationCode: string;
  name: string;
  registeredStatus: string;
  userType: string;
}
