import {UserAttachmentModel} from './user-attachment.model';

export interface ApproveUserDocumentModel {
  approveDocument: boolean;
  remark: string;
  userId: number;
  userAttachmentDtoList: Array<UserAttachmentModel>;
  programCode: string;
}
