export class UserAttachmentModel {
  userAttachmentId: number;
  attach: string;
  mRegisteredAttachmentId: number;
  approved: boolean;
  userId: number;
  addApprovedStatus: boolean;
}
