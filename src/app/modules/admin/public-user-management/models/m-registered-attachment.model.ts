import {FileModel} from './file.model';
import {UserAttachmentModel} from './user-attachment.model';

export interface MRegisteredAttachmentModel {
  id: number;
  attachmentName: string;
  attachmentNameEn: string;
  attachmentDesc: string;
  required: boolean;
  userTypeName: string;
  fileModel: FileModel;
  userAttachmentDto: UserAttachmentModel;
  userAttachmentId: number;
}
