import {PageRequest} from '../../../../shared/models/page-request';

export interface MRegisteredAttachmentPageRequest extends PageRequest {
  userTypeKey?: string;
  attachmentName?: string;
}
