import {Component, OnInit} from '@angular/core';
import {MRegisteredAttachmentService} from '../../services/m-registered-attachment.service';
import {MRegisteredAttachmentPageRequest} from '../../models/m-registered-attachment.page-request';
import {PageResponse} from '../../../../../shared/models/page-response';
import {PageRequest} from '../../../../../shared/models/page-request';
import {MRegisteredAttachmentModel} from '../../models/m-registered-attachment.model';
import {NameValuePairModel} from '../../../../../shared/models/NameValuePair.model';
import {BsModalService, ModalOptions} from 'ngx-bootstrap';
import {MRegisteredAttachmentFormComponent} from '../../modals/m-registered-attachment-form/m-registered-attachment-form.component';
import {HttpErrorResponse} from '@angular/common/http';
import {AlertModalComponent} from '../../../../../shared/modals/alert-modal/alert-modal.component';
import {take} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
  selector: 'app-registered-attachment-management',
  templateUrl: './registered-attachment-management.component.html',
  styleUrls: ['./registered-attachment-management.component.css']
})
export class RegisteredAttachmentManagementComponent implements OnInit {

  pageRequest: MRegisteredAttachmentPageRequest;
  pageResponse: PageResponse<MRegisteredAttachmentModel> = new PageResponse<MRegisteredAttachmentModel>();
  requiredAttachmentUserTypes: Array<NameValuePairModel>;
  modalConfig: ModalOptions = new ModalOptions();

  constructor(private mRegisteredAttachmentService: MRegisteredAttachmentService, private bsModalService: BsModalService) {
  }

  ngOnInit() {
    this.pageRequest = this.initialPageRequest();
    this.pagination();
    this.mRegisteredAttachmentService.getRequiredAttachmentUserType().subscribe(requiredAttachmentUserTypes => {
      this.requiredAttachmentUserTypes = requiredAttachmentUserTypes;
    });

    this.modalConfig.keyboard = false;
    this.modalConfig.ignoreBackdropClick = true;
    this.modalConfig.backdrop = true;
  }

  createOrUpdateRequiredAttachment(id) {
    this.modalConfig.initialState = {requiredAttachmentUserType: this.requiredAttachmentUserTypes, id: id};
    const modalRef = this.bsModalService.show(MRegisteredAttachmentFormComponent, this.modalConfig);
    (modalRef.content as MRegisteredAttachmentFormComponent).answerEvent
      .pipe(take(1))
      .subscribe(result => {
        if (result) {
          this.pagination();
        }
      });
  }

  deleteAttachment(id) {
    const modalRef = this.bsModalService.show(AnswerModalComponent, {
      initialState: {
        title: 'แจ้งเตือน',
        content: 'คุณต้องการลบเอกสาร ?'
      }
    });
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        this.mRegisteredAttachmentService.deleteRegisteredAttachment(id).subscribe(() => {
          this.bsModalService.show(AlertModalComponent, {
            initialState: {
              title: 'ลบเอกสารสำเร็จ'
            }
          });
          this.bsModalService.onHide.pipe(take(1)).subscribe(() => {
            this.pagination();
          });
        }, (err: HttpErrorResponse) => {
          this.bsModalService.show(AlertModalComponent, {
            initialState: {
              title: 'พบปัญหาในการแก้ไขเอกสาร',
              content: err.error.message
            }
          });
        });
      }
    });
  }

  initialPageRequest(): MRegisteredAttachmentPageRequest {
    return <PageRequest>{
      page: 1,
      pageSize: 5,
      sortDirection: 'ASC',
      sortFieldName: 'userTypeKey'
    };
  }

  pagination() {
    this.mRegisteredAttachmentService.pagination(this.pageRequest).subscribe(pageResponse => {
      this.pageResponse = pageResponse;
    });
  }

  sort(fieldName: string) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC';
    } else {
      this.pageRequest.sortDirection = 'ASC';
      this.pageRequest.sortFieldName = fieldName;
    }
    this.pagination();
  }

}
