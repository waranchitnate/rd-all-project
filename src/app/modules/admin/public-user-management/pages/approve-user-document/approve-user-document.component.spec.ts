import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveUserDocumentComponent } from './approve-user-document.component';

describe('ApproveUserDocumentComponent', () => {
  let component: ApproveUserDocumentComponent;
  let fixture: ComponentFixture<ApproveUserDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveUserDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveUserDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
