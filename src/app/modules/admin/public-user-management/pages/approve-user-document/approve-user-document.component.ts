import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PublicUserService} from '../../services/public-user.service';
import {ManagePublicUserService} from '../../services/manage-public-user.service';
import {PublicUserModel} from '../../../../../shared/models/public-user.model';
import {ToastrService} from 'ngx-toastr';
import {AbstractControl, FormArray, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {AnswerModalComponent} from '../../../../../shared/modals/answer-modal/answer-modal.component';
import {BsModalService} from 'ngx-bootstrap';
import {ApproveUserAttachmentTableComponent} from '../../components/approve-user-attachment-table/approve-user-attachment-table.component';
import {ApproveUserDocumentModel} from '../../models/approve-user-document.model';
import {UserAttachmentModel} from '../../models/user-attachment.model';

@Component({
  selector: 'app-approve-user-document',
  templateUrl: './approve-user-document.component.html',
  styleUrls: ['./approve-user-document.component.css']
})
export class ApproveUserDocumentComponent implements OnInit {

  userTypeName: string;
  hashUserId: string;
  publicUser: PublicUserModel;
  userTypeKey: string;
  approvedDocumentForm: FormGroup;
  programCode: string;
  @ViewChild('approveUserAttachmentTable') approveUserAttachmentTable: ApproveUserAttachmentTableComponent;

  constructor(private activatedRoute: ActivatedRoute, private publicUserService: PublicUserService, private managePublicUserService: ManagePublicUserService,
              private toastrService: ToastrService, private router: Router, private bsModalService: BsModalService) {
  }

  ngOnInit() {
    this.userTypeKey = this.activatedRoute.snapshot.data['userTypeKey'];
    this.programCode = this.activatedRoute.snapshot.data['programCode'];
    this.userTypeName = this.publicUserService.getUserTypeName(this.userTypeKey);
    this.hashUserId = this.activatedRoute.snapshot.params['hashUserId'];
    this.publicUserService.getPublicUserByUserType(this.hashUserId, this.userTypeKey).subscribe(publicUserRes => {
      this.publicUser = publicUserRes;
    }, err => {
      this.toastrService.error('พบปัญหาในการดึงข้อมูลผู้ใช้งานเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
    });

    this.approvedDocumentForm = new FormGroup({
      approveResult: new FormControl('approveUserDocument'),
      hashUserId: new FormControl(this.hashUserId),
      programCode: new FormControl(this.programCode),
      remark: new FormControl(null, [Validators.maxLength(512)]),
      userAttachmentDtoList: new FormArray([])
    }, [this.remarkRequired, this.approveResultValidator]);
  }

  updateRegisterStatus() {
    if (this.approvedDocumentForm.invalid) {
      this.markFormGroupTouched(this.approvedDocumentForm);
      this.toastrService.error('กรุณาระบุข้อมูลการอนุมัติให้ถูกต้อง', 'ข้อผิดพลาด');
    } else {
      if (this.approveResultControl.value === 'approveUserDocument') {
        this.approveDocument(this.approvedDocumentForm.getRawValue());
      } else if (this.approveResultControl.value === 'disApproveUserDocument') {
        const approveUserDocument: ApproveUserDocumentModel = this.approvedDocumentForm.getRawValue();
        if (approveUserDocument.userAttachmentDtoList.find(s => !s.approved) === undefined) {
          approveUserDocument.userAttachmentDtoList.forEach(s => s.approved = false);
        }
        this.disApproveDocument(approveUserDocument);
      } else if (this.approveResultControl.value === 'disApproveUser') {
        this.managePublicUserService.disapproveUserFromApproveUserDocumentPage(this.approvedDocumentForm.getRawValue(), this.activatedRoute);
      }
    }
  }

  approveDocument(approveUserDocument: ApproveUserDocumentModel) {
    const modalRef = this.bsModalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
    (modalRef.content as AnswerModalComponent).content = 'คุณต้องการอนุมัติเอกสารผู้ใช้งานใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.managePublicUserService.approveDocument(approveUserDocument).subscribe(() => {
          this.toastrService.success('อนุมัติเอกสารผู้ใช้งานสำเร็จ');
          this.router.navigate(['../../search'], {relativeTo: this.activatedRoute});
        });
      }
      sub.unsubscribe();
    });
  }

  disApproveDocument(approveUserDocument: ApproveUserDocumentModel) {
    const modalRef = this.bsModalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
    (modalRef.content as AnswerModalComponent).content = 'คุณต้องปฏิเสธเอกสารผู้ใช้งาน เพื่อให้ผู้ใช้งานส่งเอกสารใหม่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.managePublicUserService.rejectDocument(approveUserDocument).subscribe(() => {
          this.toastrService.success('ไม่อนุมัติเอกสารผู้ใช้งานสำเร็จ');
          this.router.navigate(['../../search'], {relativeTo: this.activatedRoute});
        });
      }
      sub.unsubscribe();
    });
  }

  approveResultValidator(control: AbstractControl): ValidationErrors | null {
    let validationResult = null;
    const frmGrp = control as FormGroup;
    if (frmGrp && frmGrp.controls.userAttachmentDtoList && frmGrp.controls.approveResult) {
      const userAttachmentDtoList = frmGrp.controls.userAttachmentDtoList.value as Array<UserAttachmentModel>;
      if (userAttachmentDtoList.length !== 0) {
        const isAnyDisapproved = userAttachmentDtoList.find(s => !s.approved) !== undefined;
        const isAllApproved = userAttachmentDtoList.filter(s => s.approved).length === userAttachmentDtoList.length;
        if (isAllApproved && frmGrp.controls.approveResult.value === 'disApproveUserDocument') {
          validationResult = {documentDisApprovedRequired: 'กรุณาระบุเอกสารที่ไม่ถูกต้อง'};
        } else if (isAnyDisapproved && frmGrp.controls.approveResult.value === 'approveUserDocument') {
          validationResult = {documentDisApprovedRequired: 'กรุณาระบุเอกสารถูกต้องทุกรายการ'};
        }
      }
    }
    return validationResult;
  }

  remarkRequired(control: AbstractControl): ValidationErrors | null {
    let validationResult = null;
    if (control !== undefined) {
      const approvedDocumentForm = control as FormGroup;
      const remarkControl = approvedDocumentForm.controls['remark'];
      if (approvedDocumentForm.controls['approveResult'].value === 'disApproveUserDocument') {
        if (remarkControl.value === null || remarkControl.value === '') {
          validationResult = {remarkRequired: 'กรุณาระบุเหตุผลการไม่อนุมัติเอกสาร'};
        }
      } else if (approvedDocumentForm.controls['approveResult'].value === 'disApproveUser') {
        if (remarkControl.value === null || remarkControl.value === '') {
          validationResult = {remarkRequired: 'กรุณาระบุเหตุผลไม่อนุมัติผู้ใช้งาน'};
        }
      } else {
        if (remarkControl.value) {
          remarkControl.setValue(null);
        }
      }
    }
    return validationResult;
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  get remarkControl(): AbstractControl {
    return this.approvedDocumentForm.get('remark');
  }

  get approveResultControl(): AbstractControl {
    return this.approvedDocumentForm.get('approveResult');
  }

}
