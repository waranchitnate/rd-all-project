import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageUserStatusMoiComponent } from './manage-user-status-moi.component';

describe('ManageUserStatusMoiComponent', () => {
  let component: ManageUserStatusMoiComponent;
  let fixture: ComponentFixture<ManageUserStatusMoiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageUserStatusMoiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageUserStatusMoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
