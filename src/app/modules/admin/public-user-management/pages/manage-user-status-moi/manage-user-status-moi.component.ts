import { Component, OnInit } from '@angular/core';
import { FormBuilder, AbstractControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-manage-user-status-moi',
  templateUrl: './manage-user-status-moi.component.html',
  styleUrls: ['./manage-user-status-moi.component.css']
})
export class ManageUserStatusMoiComponent implements OnInit {

  fileUpload: File;
  confirmSubmit = false;
  uploadFileForm = this.formBuilder.group({
    fileName: [null]
  });
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.uploadFileForm;
    this.confirmSubmit = false;
  }

  fileProgress(e) {
    this.fileUpload = <File>e.target.files[0];
    this.uploadFileForm.get('fileName').setValue(e.target.files[0].name);

  }

  removeAttachmentForm() {
    this.uploadFileForm.get('fileName').setValue(null);
  }

  confirmUpload() {
    this.confirmSubmit = true;
  }

}
