import {Component, OnInit} from '@angular/core';
import {ManagePublicUserPageRequest} from '../../models/ManagePublicUserPageRequest';
import {PageResponse} from '../../../../../shared/models/page-response';
import {ManagePublicUserStatusModel} from '../../models/ManagePublicUserStatus.model';
import {AnswerModalComponent} from '../../../../../shared/modals/answer-modal/answer-modal.component';
import {BsModalService} from 'ngx-bootstrap';
import {take} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {ManagePublicUserService} from '../../services/manage-public-user.service';
import {UserRegistrationDetailModel} from '../../models/user-registration-detail.model';

@Component({
  selector: 'app-manage-thai-individual-status-search',
  templateUrl: './manage-thai-individual-status-search.component.html',
  styleUrls: ['./manage-thai-individual-status-search.component.css']
})
export class ManageThaiIndividualStatusSearchComponent implements OnInit {

  page: PageResponse<ManagePublicUserStatusModel> = new PageResponse<ManagePublicUserStatusModel>();
  pageRequest: ManagePublicUserPageRequest;

  constructor(private managePublicUserService: ManagePublicUserService, private bsModalService: BsModalService, private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.page.content = [];
    this.createNewPageRequest();
    this.pagination();
  }

  resendActivationEmail(hashUserId: string) {
    const modalRef = this.bsModalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยันการส่งอีเมลล์ยืนยันตัวตน';
    (modalRef.content as AnswerModalComponent).content = 'คุณต้องการส่งอีเมลล์ยืนยันตัวตนหรือไม่';
    (modalRef.content as AnswerModalComponent).answerEvent
      .pipe(take(1))
      .subscribe(a => {
      if (a) {
        this.managePublicUserService.resendActivationEmail(hashUserId).subscribe(() => {
          this.toastrService.success('ส่งอีเมลล์ยืนยันตัวตนสำเร็จ', 'แจ้งเตือน');
          this.pagination();
        });
      }
    });
  }

  confirmDisableUser(hashUserId: string, fullname: string) {
    const modalRef = this.bsModalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยันการยกเลิกผู้ใช้งาน';
    (modalRef.content as AnswerModalComponent).content = 'คุณต้องการยกเลิกผู้ใช้งาน' + fullname + 'หรือไม่';
    (modalRef.content as AnswerModalComponent).answerEvent
      .pipe(take(1))
      .subscribe(a => {
      if (a) {
        const disabledUserDto = new UserRegistrationDetailModel();
        disabledUserDto.hashUserId = hashUserId;
        disabledUserDto.programCode = 'UMSP040109';
        this.managePublicUserService.disableUser(disabledUserDto).subscribe(() => {
          this.toastrService.success('ยกเลิกผู้ใช้งานสำเร็จ', 'แจ้งเตือน');
          this.pagination();
        });
      }
    });
  }

  pagination() {
    this.pageRequest.userType = 'T';
    this.managePublicUserService.pagination(this.pageRequest).subscribe(pageResponse => {
      this.page = pageResponse;
    });
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.pagination();
  }

  createNewPageRequest() {
    this.pageRequest = new ManagePublicUserPageRequest();
    this.pageRequest.sortFieldName = 'createdDate';
    this.pageRequest.sortDirection = 'DESC';
  }

  paginationWhenPressEnter(evt: KeyboardEvent) {
    if (evt.key === 'Enter') {
      this.pagination();
    }
  }

}
