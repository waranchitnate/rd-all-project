import { Component, OnInit } from '@angular/core';
import {PageResponse} from '../../../../../shared/models/page-response';
import {ManagePublicUserPageRequest} from '../../models/ManagePublicUserPageRequest';
import {ManagePublicUserStatusModel} from '../../models/ManagePublicUserStatus.model';
import {BsModalService} from 'ngx-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {AnswerModalComponent} from '../../../../../shared/modals/answer-modal/answer-modal.component';
import {take} from 'rxjs/operators';
import {ManagePublicUserService} from '../../services/manage-public-user.service';
import {UserRegistrationDetailModel} from '../../models/user-registration-detail.model';

@Component({
  selector: 'app-manage-thai-juristic-person-user-status-search',
  templateUrl: './manage-thai-juristic-person-user-status-search.component.html',
  styleUrls: ['./manage-thai-juristic-person-user-status-search.component.css']
})
export class ManageThaiJuristicPersonUserStatusSearchComponent implements OnInit {

  page: PageResponse<ManagePublicUserStatusModel> = new PageResponse<ManagePublicUserStatusModel>();
  pageRequest: ManagePublicUserPageRequest;

  constructor(private managePublicUserService: ManagePublicUserService, private bsModalService: BsModalService, private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.page.content = [];
    this.createNewPageRequest();
    this.pagination();
  }

  confirmDisableUser(hashUserId: string, fullname: string) {
    const modalRef = this.bsModalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยันการยกเลิกผู้ใช้งาน';
    (modalRef.content as AnswerModalComponent).content = 'คุณต้องการยกเลิกผู้ใช้งาน ' + fullname + ' หรือไม่';
    (modalRef.content as AnswerModalComponent).answerEvent
      .pipe(take(1))
      .subscribe(a => {
        if (a) {
          const disabledUserDto = new UserRegistrationDetailModel();
          disabledUserDto.hashUserId = hashUserId;
          disabledUserDto.programCode = 'UMSP040105';
          this.managePublicUserService.disableUser(disabledUserDto).subscribe(() => {
            this.toastrService.success('ยกเลิกผู้ใช้งานสำเร็จ', 'แจ้งเตือน');
            this.pagination();
          });
        }
      });
  }

  paginationWhenPressEnter(evt: KeyboardEvent) {
    if (evt.key === 'Enter') {
      this.pagination();
    }
  }

  pagination() {
    this.pageRequest.userType = 'C';
    this.managePublicUserService.pagination(this.pageRequest).subscribe(pageResponse => {
      this.page = pageResponse;
    });
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.pagination();
  }

  createNewPageRequest() {
    this.pageRequest = new ManagePublicUserPageRequest();
    this.pageRequest.sortFieldName = 'createdDate';
    this.pageRequest.sortDirection = 'DESC';
  }

  resendApprovedDocument(hashUserId: string) {
    this.managePublicUserService.resendApprovedDocumentEmail(hashUserId).subscribe(() => {
      this.toastrService.success('ส่งอีเมลอนุมัติเอกสารสำเร็จ');
    });
  }

  resendRejectedDocument(hashUserId: string) {
    this.managePublicUserService.resendRejectedDocumentEmail(hashUserId).subscribe(() => {
      this.toastrService.success('ส่งอีเมลปฎิเสธเอกสารสำเร็จ');
    });
  }

  resendApproveUserEmail(hashUserId: string) {
    this.managePublicUserService.resendApproveUserEmail(hashUserId).subscribe(() => {
      this.toastrService.success('ส่งอีเมลแจ้งเตือนอนุมัติผู้ใช้งานสำเร็จ');
    });
  }

  resendDisapproveUserEmail(hashUserId: string) {
    this.managePublicUserService.resendDisapproveUserEmail(hashUserId).subscribe(() => {
      this.toastrService.success('ส่งอีเมลแจ้งเตือนไม่อนุมัติผู้ใช้งานสำเร็จ');
    });
  }

  resendDisapproveFromDocStepEmail(hashUserId: string) {
    this.managePublicUserService.resendDisapproveFromDocStepEmail(hashUserId).subscribe(() => {
      this.toastrService.success('ส่งอีเมลแจ้งเตือนไม่อนุมัติผู้ใช้งานสำเร็จ');
    });
  }
}
