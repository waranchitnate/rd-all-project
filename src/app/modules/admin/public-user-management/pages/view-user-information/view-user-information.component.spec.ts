import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUserInformationComponent } from './view-user-information.component';

describe('ViewUserInformationComponent', () => {
  let component: ViewUserInformationComponent;
  let fixture: ComponentFixture<ViewUserInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUserInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUserInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
