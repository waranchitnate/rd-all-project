import { Component, OnInit } from '@angular/core';
import {PublicUserModel} from '../../../../../shared/models/public-user.model';
import {ActivatedRoute, Router} from '@angular/router';
import {PublicUserService} from '../../services/public-user.service';
import {ManagePublicUserService} from '../../services/manage-public-user.service';
import {ToastrService} from 'ngx-toastr';
import {BsModalService} from 'ngx-bootstrap';
import {OtpService} from '../../../../../shared/services/otp.service';

@Component({
  selector: 'app-view-user-information',
  templateUrl: './view-user-information.component.html',
  styleUrls: ['./view-user-information.component.css']
})
export class ViewUserInformationComponent implements OnInit {

  userTypeName: string;
  hashUserId: string;
  publicUser: PublicUserModel;
  userTypeKey: string;
  constructor(private activatedRoute: ActivatedRoute, private publicUserService: PublicUserService, private managePublicUserService: ManagePublicUserService,
              private toastrService: ToastrService, private router: Router, private bsModalService: BsModalService, private otpService: OtpService) {
  }

  ngOnInit() {
    this.userTypeKey = this.activatedRoute.snapshot.data['userTypeKey'];
    this.userTypeName = this.publicUserService.getUserTypeName(this.userTypeKey);
    this.hashUserId = this.activatedRoute.snapshot.params['hashUserId'];
    this.publicUserService.getPublicUserByUserType(this.hashUserId, this.userTypeKey).subscribe(publicUserRes => {
      this.publicUser = publicUserRes;
    }, err => {
      this.toastrService.error('พบปัญหาในการดึงข้อมูลผู้ใช้งาน', 'ข้อผิดพลาด');
    });
  }
}
