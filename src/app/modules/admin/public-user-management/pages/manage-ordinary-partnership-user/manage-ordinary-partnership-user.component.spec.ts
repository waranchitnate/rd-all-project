import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageOrdinaryPartnershipUserComponent } from './manage-ordinary-partnership-user.component';

describe('ManageOrdinaryPartnershipUserComponent', () => {
  let component: ManageOrdinaryPartnershipUserComponent;
  let fixture: ComponentFixture<ManageOrdinaryPartnershipUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageOrdinaryPartnershipUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageOrdinaryPartnershipUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
