import {Component, isDevMode, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PublicUserService} from '../../services/public-user.service';
import {ManagePublicUserService} from '../../services/manage-public-user.service';
import {ToastrService} from 'ngx-toastr';
import {BsModalService} from 'ngx-bootstrap';
import {AbstractControl, FormGroup} from '@angular/forms';
import {PublicUserModel} from '../../../../../shared/models/public-user.model';
import {take, tap} from 'rxjs/operators';
import {VerifyOtpModalComponent} from '../../../../../shared/modals/verify-otp-modal/verify-otp-modal.component';
import {Observable} from 'rxjs';
import {OtpResponse} from '../../../../../shared/models/OtpResponse.model';
import {OtpService} from '../../../../../shared/services/otp.service';
import {UserRegistrationDetailModel} from '../../models/user-registration-detail.model';

@Component({
  selector: 'app-approve-user',
  templateUrl: './approve-user.component.html',
  styleUrls: ['./approve-user.component.css']
})
export class ApproveUserComponent implements OnInit {

  userTypeName: string;
  hashUserId: string;
  publicUser: PublicUserModel;
  userTypeKey: string;
  approveUserForm: FormGroup;

  constructor(private activatedRoute: ActivatedRoute, private publicUserService: PublicUserService, private managePublicUserService: ManagePublicUserService,
              private toastrService: ToastrService, private router: Router, private bsModalService: BsModalService, private otpService: OtpService) {
  }

  ngOnInit() {
    this.userTypeKey = this.activatedRoute.snapshot.data['userTypeKey'];
    const programCode = this.activatedRoute.snapshot.data['programCode'];
    this.userTypeName = this.publicUserService.getUserTypeName(this.userTypeKey);
    this.hashUserId = this.activatedRoute.snapshot.params['hashUserId'];
    this.publicUserService.getPublicUserByUserType(this.hashUserId, this.userTypeKey).subscribe(publicUserRes => {
      this.publicUser = publicUserRes;
    }, err => {
      this.toastrService.error('พบปัญหาในการดึงข้อมูลผู้ใช้งาน', 'ข้อผิดพลาด');
    });

    this.approveUserForm = this.managePublicUserService.initialApproveUserForm(this.hashUserId, programCode);
  }

  updateRegisteredStatus() {
    if (this.approveUserForm.invalid) {
      this.markFormGroupTouched(this.approveUserForm);
      this.toastrService.error('กรุณาระบุข้อมูลให้ถูกต้อง', 'ข้อผิดพลาด');
    } else {
      const userRegistrationDetail: UserRegistrationDetailModel = this.approveUserForm.value;
      if (this.approveUserControl.value) {
        this.verifyOtpAndApprove(userRegistrationDetail);
      } else {
        this.managePublicUserService.disApproveUser(userRegistrationDetail, this.activatedRoute);
      }
    }
  }

  verifyOtpAndApprove(userRegistrationDetail: UserRegistrationDetailModel) {
    if (this.userTypeKey === 'S') {
      this.managePublicUserService.approveAndSendActivationEmail(userRegistrationDetail)
        .subscribe(() => {
            this.toastrService.success('อนุมัติผู้ใช้งานสำเร็จ');
            this.router.navigate(['../../search'], {relativeTo: this.activatedRoute});
        });
    } else {
      this.requestOtp()
        .pipe(take(1))
        .subscribe(res => {
          if (res.code === '200') {
            const modalRef = this.bsModalService.show(VerifyOtpModalComponent, {
              ignoreBackdropClick: false,
              keyboard: false,
              backdrop: true,
              initialState: {
                username: this.publicUser.username,
                phoneNumber: this.publicUser.phoneNumber,
                otpResponse: res
              }
            });
            const requestNewOtpSub = (modalRef.content as VerifyOtpModalComponent).requestNewOtpEvent.subscribe(() => {
              this.requestOtp().subscribe(newOtpResponse => {
                if (!isDevMode()) {
                  newOtpResponse.data.otpCode = null;
                }
                (modalRef.content as VerifyOtpModalComponent).otpForm.patchValue(newOtpResponse.data);
              });
            });
            const sub = (modalRef.content as VerifyOtpModalComponent).closeModalEvent.subscribe(closeResult => {
              sub.unsubscribe();
              requestNewOtpSub.unsubscribe();
              if (closeResult) {
                this.approveUser(userRegistrationDetail);
              }
            });
          }
        });
    }
  }

  approveUser(userRegistrationDetail: UserRegistrationDetailModel) {
    this.managePublicUserService.approveUser(userRegistrationDetail).subscribe(activeRes => {
      this.toastrService.success('อนุมัติผู้ใช้งานสำเร็จ');
      this.router.navigate(['../../search'], {relativeTo: this.activatedRoute});
    });
  }

  requestOtp(): Observable<OtpResponse> {
    return this.otpService.requestOtpForActiveUser(this.publicUser.phoneNumber, this.publicUser.username)
      .pipe(
        tap(res => {
          if (res.code !== '200') {
            this.toastrService.error(res.message, 'ขอรหัสยืนยันตัวตนไม่สำเร็จ');
          }
        }, err => {
          this.toastrService.error(err.message, 'ระบบไม่สามารถขอรหัสยืนยันตัวตนได้');
        })
      );
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  get remarkControl(): AbstractControl {
    return this.approveUserForm.get('remark');
  }

  get approveUserControl(): AbstractControl {
    return this.approveUserForm.get('approveUser');
  }
}
