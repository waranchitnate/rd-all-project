
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PageResponse } from '../../../../shared/models/page-response';
import { PermissionExternal } from '../models/permission.model';
import { UserType } from '../models/user-type.model';
import { ProgramExternal } from '../models/program.model';



@Injectable()
export class UserTypePermissionService {

  private url = environment.internalUserManagementApiUrl + 'user-type';

  constructor(private http: HttpClient) { }

  getUserTypes(name, page, pageSize): Observable<PageResponse<any>> {
    return <Observable<PageResponse<any>>>this.http.get(this.url + '/search', {params: {page: page, pageSize: pageSize, name: name}});
  }

  getUserTypeByKey(key): Observable<UserType> {
    return <Observable<UserType>>this.http.get(this.url + '/' + key);
  }

  getUserTypeByName(name): Observable<UserType> {
    return <Observable<UserType>>this.http.get(this.url + '/' + name);
  }

  getPermissionAll(): Observable<PermissionExternal[]> {
    return <Observable<PermissionExternal[]>>this.http.get(this.url + '/permission');
  }

  getProgramAll(): Observable<ProgramExternal[]> {
    return <Observable<ProgramExternal[]>>this.http.get(this.url + '/program');
  }

  save(request: UserType): Observable<any> {
    // console.log('UserType ==> ' + JSON.stringify(request));
    return this.http.post(this.url, request);
  }

}

