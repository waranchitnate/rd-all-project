
import { PageResponse } from 'src/app/shared/models/page-response';
import { UserTypePermissionService } from '../../service/user-type-permission.service';
import { UserType } from '../../models/user-type.model';
import { Component, OnInit } from '@angular/core';
import { UserTypeRequest } from '../../models/user-type-request.model';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  form: FormGroup;
  page: PageResponse<any> = new PageResponse<any>();
  pageRequest: UserTypeRequest;
  userTypes: Array<UserType> = [];

  currentPage = 1;
  totalItem = 0;
  pageSize = 10;
  isValue = false;

  constructor(
    private permissionService: UserTypePermissionService,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.initForm();
    this.initPage();
    this.getUserTypes();
  }

  initForm() {
    this.form = this.fb.group({
      name: this.fb.control('')
    });
  }

  initPage() {
    this.pageRequest = new UserTypeRequest();
    this.pageRequest.sortFieldName = 'firstName';
  }

  getUserTypes() {
    console.log('this.currentPage ==> ' + this.currentPage);
    const input = this.form.get('name').value;
    this.permissionService.getUserTypes( input, this.currentPage, this.pageSize).subscribe(userTypes => {
      this.totalItem = userTypes.totalElements;
      this.userTypes = userTypes.content;
    });
  }

  search() {
    this.getUserTypes();
  }

  clear() {
    this.form.get('name').setValue('');
    this.getUserTypes();
  }

}
