import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalService } from 'src/app/core/services/modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserType } from '../../models/user-type.model';
import { UserTypePermissionService } from '../../service/user-type-permission.service';
import { PermissionExternal} from '../../models/permission.model';
import { containsElement } from '@angular/animations/browser/src/render/shared';
import { GroupService } from 'src/app/modules/admin/group-management/services/group.service';
import { Program } from 'src/app/modules/admin/group-management/models/program.model';
import { JsonPipe } from '@angular/common';
import { ProgramExternal } from '../../models/program.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  form: FormGroup;
  permissionList: PermissionExternal[];
  programList: ProgramExternal[];
  userType: UserType;
  title = '';
  disabled = false;
  isValue = false;

  constructor(
    private fb: FormBuilder,
    private userTypeService: UserTypePermissionService,
    private modal: ModalService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit() {
    this.activateRoute();
    this.getUserTypeByKey();
    this.formInit();
    // this.getProgramList();
  }

  private formInit() {
    this.form = this.fb.group({
      key: this.fb.control(''),
      name: this.fb.control(''),
      permissions: this.fb.group([])
    });
  }

  private activateRoute() {
    this.activatedRoute.data.subscribe(data => {
      this.title = data.title;
      this.disabled = data.disabled;
    });
  }

  private getUserTypeByKey() {
    const userTypeKey = this.activatedRoute.snapshot.paramMap.get('key');
    this.userTypeService.getUserTypeByKey(userTypeKey).subscribe(result => {
      this.userType = result;
      this.getProgramList();
    });
  }

  getProgramList() {
    this.userTypeService.getProgramAll().subscribe(p => {
      this.programList = p;
      this.initFormGroup();
      this.getUserTypePermission();
    });
  }

  // private getPermissionAll() {
  //   this.userTypeService.getPermissionAll().subscribe(u => {
  //     this.permissionList = u;
  //     this.initFormGroup();
  //     this.getUserTypePermission();
  //   });
  // }

  private initFormGroup() {
    this.form.get('key').setValue(this.userType.key);
    this.form.get('name').setValue(this.userType.name);
    this.programList.forEach((p, i) => {
      console.log(' p => ' + JSON.stringify(p));
      p.permissions.forEach((pm) => {
        (this.form.controls.permissions as FormGroup)
        .registerControl(String(pm.permissionId),
        this.fb.control({value: false, disabled: this.disabled }));
      });
    });
   }

  private getUserTypePermission() {
    this.userType.permissions.forEach(p => {
      this.form.get('permissions').get(String(p.permissionId)).setValue(true);
    });
  }

  public submit() {
    if (this.form.valid) {
      this.userTypeService.save(this.form.getRawValue()).subscribe(
        response => {
          this.modal.openModal('บันทึกสำเร็จ', 'บันทึกสำเร็จ');
          const modalHide = this.modal.modalService.onHide.subscribe(e => {
            modalHide.unsubscribe();
            this.router.navigate(['/external-user-management/permission/search']);
          });
        },
        err => {
          this.modal.openModal('เกิดข้อผิดพลาด', 'ไม่สามารถบันทึกได้กรุณาทำรายการใหม่ภายหลัง');
        }
      );
    }
  }

}
