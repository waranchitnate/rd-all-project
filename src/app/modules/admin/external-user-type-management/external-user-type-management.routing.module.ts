import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './pages/search/search.component';
import { EditComponent } from './pages/edit/edit.component';

const routes: Routes = [
  { path: 'permission/search', component: SearchComponent},
  { path: 'permission/edit/:key', component: EditComponent , data: {title: 'แก้ไขกลุ่มสิทธิ์', disabled:  false}},
  { path: 'permission/view/:key', component: EditComponent , data: {title: 'เรียกดูกลุ่มสิทธิ์', disabled:  true}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExternalUserTypeManagementRoutingModule { }
