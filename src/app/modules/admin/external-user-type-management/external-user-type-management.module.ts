import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchComponent } from './pages/search/search.component';
import { EditComponent} from './pages/edit/edit.component';
import { SharedModule } from '../../../shared/shared.module';
import { ExternalUserTypeManagementRoutingModule } from './external-user-type-management.routing.module';
import { UserTypePermissionService } from './service/user-type-permission.service';
import { ViewComponent } from './pages/view/view.component';

@NgModule({
  declarations: [SearchComponent
  , EditComponent, ViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    ExternalUserTypeManagementRoutingModule
  ],
  providers: [UserTypePermissionService]
})
export class ExternalUserTypeManagementModule { }
