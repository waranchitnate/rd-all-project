
export class PermissionExternal {
  permissionId: number;
  name: string;
  key: string;
  programCode: string;
}
