import {PageRequest} from '../../../../shared/models/page-request';

export class UserTypeRequest extends PageRequest {
  key: string;
  name: string;
}
