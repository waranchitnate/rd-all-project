import { PermissionExternal } from './permission.model';

export class UserType {
  key: string;
  name: string;
  permissions: PermissionExternal[];
}
