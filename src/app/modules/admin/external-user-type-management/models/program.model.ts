import { PermissionExternal } from './permission.model';

export interface ProgramExternal {
    code: string;
    name: string;
    permissions: PermissionExternal[];
  }
