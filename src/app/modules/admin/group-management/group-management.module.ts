import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupManagementRoutingModule } from './group-management-routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { SearchComponent } from './pages/search/search.component';
import { FormsModule } from '@angular/forms';
import { GroupService } from './services/group.service';
import { AddComponent } from './pages/add/add.component';
import { EditComponent } from './pages/edit/edit.component';


@NgModule({
  declarations: [SearchComponent, AddComponent, EditComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    GroupManagementRoutingModule,
  ],
  providers: [GroupService]
})
export class GroupManagementModule { }
