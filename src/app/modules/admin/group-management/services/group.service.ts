import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Group } from '../models/group.model';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PageResponse } from '../../../../shared/models/page-response';
import { Program } from '../models/program.model';
import { SaveRole } from '../models/save-role.model';
import { MSTApplication } from '../models/mst-application.model';
import { FormGroup, FormControl } from '@angular/forms';


@Injectable()
export class GroupService {

  url = environment.internalUserManagementApiUrl + 'group';
  constructor(private http: HttpClient) {
  }

  getGroupList(page, pageSize): Observable<PageResponse<any>> {
    return <Observable<PageResponse<any>>>this.http.get(this.url, {params: {page: page, pageSize: pageSize}});
  }

  getGroupByCriteria(criteria: Group, page, pageSize): Observable<PageResponse<any>>  {
    const newCriteria: any = {page: page, pageSize: pageSize};
    for (const key in criteria) {
      if (criteria[key]) {
        newCriteria[key] = criteria[key];
      }
    }
    return <Observable<PageResponse<any>>>this.http.get(this.url + '/findByCriteria', {params: newCriteria});
  }

  getProgramList(): Observable<Program[]> {
    return <Observable<Program[]>>this.http.get(this.url + '/getProgramList');
  }

  getProgramByAppId(appId: number): Observable<Program[]> {
    return <Observable<Program[]>>this.http.get(this.url + '/program/' + appId);
  }

  getProgramByProgramCode(programCode: String): Observable<Program[]> {
    return <Observable<Program[]>>this.http.get(this.url + '/get-program-list-by-program-code/' + programCode);
  }

  getProgramByRoleId(roleId: number): Observable<Program[]> {
    return <Observable<Program[]>>this.http.get(this.url + '/program/role/' + roleId);
  }

  getMstApplications(): Observable<MSTApplication[]> {
    return <Observable<MSTApplication[]>>this.http.get(this.url + '/mst-applications');
  }

  getMstApplication(id: number): Observable<MSTApplication> {
    return <Observable<MSTApplication>>this.http.get(this.url + '/mst-applications/' + id);
  }

  getMstApplicationByAppCode(code: string): Observable<MSTApplication> {
    return <Observable<MSTApplication>>this.http.get(this.url + '/mst-applications/appCode/' + code);
  }

  save(request: SaveRole): Observable<any> {
    return this.http.post(this.url, request);
  }

  update(request: SaveRole): Observable<any> {
    return this.http.put(this.url, request);
  }

  getById(id: number): Observable<Group> {
    return <Observable<Group>>this.http.get(this.url + '/' + id);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.url + '/' + id);
  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {

      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }

    });
  }

}
