import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Program } from '../../models/program.model';
import { MSTApplication } from '../../models/mst-application.model';
import { GroupService } from '../../services/group.service';
import { ModalService } from 'src/app/core/services/modal.service';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';



@Component({
  selector: 'app-group-management-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  form: FormGroup = this.fb.group({});
  programList: Program[];
  mstApps: MSTApplication[];
  mstApp: MSTApplication;
  title = '';
  disabled = false;
  groupCode = '';
  appId: number;
  roleId: number;
  programCode: string;

  constructor(
    private fb: FormBuilder,
    private group: GroupService,
    private modal: ModalService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit() {

    this.activatedRoute.data.subscribe(data => {
      this.title = data.title;
      this.disabled = data.disabled;
    });
    // this.groupCode = this.activatedRoute.snapshot.paramMap.get('code').substring(0, 3);
    // this.getMstApplication(this.groupCode);
    debugger;
    this.roleId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.programCode = this.activatedRoute.snapshot.paramMap.get('code').substring(0, 3);
    this.getProgramByProgramCode(this.programCode);
    this.form = this.fb.group({
      id: this.fb.control(''),
      name: this.fb.control({value: '', disabled: this.disabled }, Validators.required),
      code: this.fb.control({value: '', disabled: this.disabled }, Validators.required),
      desc: this.fb.control({value: '', disabled: this.disabled }),
      status: this.fb.control({value: '', disabled: this.disabled }),
      mode: 'edit',
      permissions: this.fb.group([])
    });
  }

  // getMstApplication(code) {
  //   this.group.getMstApplicationByAppCode(code).subscribe(m => {
  //     this.mstApp = m;
  //     this.appId = this.mstApp.appId;
  //     this.getProgramByAppId(this.appId);
  //   });
  // }

  getProgramByRoleId(id) {
    this.group.getProgramByRoleId(id).subscribe(p => {
      this.programList = p;
      this.initFormGroup();
      this.getGroup();
    });

    // this.group.getProgramList().subscribe(p => {
    //   this.programList = p;
    //   this.initFormGroup();
    //   this.getGroup();
    // });
  }

  getProgramByProgramCode(programCode) {
    this.group.getProgramByProgramCode(programCode).subscribe(p => {
      this.programList = p;
      this.initFormGroup();
      this.getGroup();
    });
  }

  initFormGroup() {
    this.programList.forEach((p, i) => {
      p.permissions.forEach(ps => {
        (this.form.controls.permissions as FormGroup)
        .registerControl(String(ps.permissionId),
        this.fb.control({value: false, disabled: this.disabled }));
      });
    });

  }

  getGroup() {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.group.getById(id).subscribe(g => {
      this.form.get('id').setValue(g.id);
      this.form.get('name').setValue(g.name);
      this.form.get('code').setValue(g.code);
      this.form.get('desc').setValue(g.desc);
      this.form.get('status').setValue(g.status);
      g.permissions.forEach(p => {
        const permission = this.form.get('permissions').get(String(p.permissionId));
        console.log( ' permission ==> ' + permission );
        if ( permission != null ) {
          this.form.get('permissions').get(String(p.permissionId)).setValue(true);
        }
      });
    });
  }

  submit() {
    if (this.form.valid) {
      this.group.update(this.form.getRawValue()).subscribe(
        response => {
          this.modal.openModal('บันทึกสำเร็จ', 'บันทึกสำเร็จ');
          const modalHide = this.modal.modalService.onHide.subscribe(e => {
            modalHide.unsubscribe();
            this.router.navigate(['/group-management']);
          });
        },
        err => {
          this.modal.openModal('เกิดข้อผิดพลาด', 'ไม่สามารถบันทึกได้กรุณาทำรายการใหม่ภายหลัง');
        }
      );
    }
  }

}
