import { Component, OnInit, ViewChild } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { Group } from '../../models/group.model';
import { ModalService } from 'src/app/core/services/modal.service';
import { BsModalService } from 'ngx-bootstrap';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { FormBuilder } from '@angular/forms';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';

@Component({
  selector: 'app-group-management-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @ViewChild(FloatButtonComponent) floatComp;

  searchForm = this.fb.group({
    'code': this.fb.control(null),
    'name': this.fb.control(null),
    'status': this.fb.control('')
  });

  groupList: Group[];
  currentPage = 1;
  totalItem = 0;
  pageSize = 10;
  sortDirection = 'ASC';
  sortFieldName = '';
  constructor(
    private groupService: GroupService,
    public modalService: BsModalService,
    private alertModalService: ModalService,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.search();
    this.floatComp.urlAdd = '/group-management/add';
    this.floatComp.tooltipMsg = 'เพิ่มข้อมูล กลุ่มสิทธิ์';
  }

  pageChanged(e) {
    this.pageSize = e.itemsPerPage;
    this.currentPage = e.page;
    this.search();
  }

  sort(fieldName) {
    if (this.sortFieldName === fieldName) {
      this.sortDirection = (this.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.sortFieldName = fieldName;
      this.sortDirection = 'ASC';
    }
    this.search();
  }

  delete(id: number) {
    const modalRef = this.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
    (modalRef.content as AnswerModalComponent).content = 'คุณต้องการลบกลุ่มผู้ใช้นี้ใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.groupService.delete(id).subscribe(
          res => {
            this.search();
          },
          err => {
            this.alertModalService.openModal('เกิดข้อผิดพลาด', 'ไม่สามารถลบรายการได้ขณะนี้');
          }
        );
      }
      sub.unsubscribe();
    });
  }

  search() {
    this.groupService.getGroupByCriteria(this.searchForm.getRawValue(), this.currentPage, this.pageSize).subscribe(groupList => {
      debugger;
      this.totalItem = groupList.totalElements;
      this.groupList = groupList.content;
    });
  }

  clear() {
    this.searchForm.reset();
  }

}
