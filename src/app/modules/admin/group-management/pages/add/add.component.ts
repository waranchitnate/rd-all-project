import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Program } from '../../models/program.model';
import { GroupService } from '../../services/group.service';
import { ModalService } from 'src/app/core/services/modal.service';
import { Router } from '@angular/router';
import { MSTApplication } from '../../models/mst-application.model';

@Component({
  selector: 'app-group-management-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  form: FormGroup;
  programList: Program[];
  mstApps: MSTApplication[];
  mstApp: MSTApplication;
  appCode: String;
  constructor(private fb: FormBuilder, private group: GroupService, private modal: ModalService, private router: Router) { }

  ngOnInit() {
    this.appCode = 'ADM';
    this.getProgramList();
    this.getMstApplication();
    this.form = this.fb.group({
      appId: this.fb.control(14),
      name: this.fb.control(''),
      code: this.fb.control('', Validators.required),
      desc: this.fb.control(''),
      status: this.fb.control('ACTIVE'),
      permissions: this.fb.group([])
    });
  }

  getAppCodeByAppId(appId) {
    Object.keys(this.mstApps).some(key => {
      let i: Number;
       i = this.mstApps[key].appId;
      if ( i === Number(appId) ) {
        this.appCode = this.mstApps[key].appCode;
        return true;
      }
      return false;
    });
  }

  // setName() {
  //   this.form.value.name = this.appCode + '_' + this.form.value.code;
  //   this.form.controls['name'].setValue(this.appCode + '_' + this.form.value.code);
  // }

  getMstApplication() {
    this.group.getMstApplications().subscribe(m => {
      this.mstApps = m;
    });
  }

  getProgramList() {
    this.group.getProgramList().subscribe(p => {
      this.programList = p;
      this.initFormGroup();
    });
  }

  initFormGroup() {
    this.programList.forEach((p, i) => {
      p.permissions.forEach(ps => {
        (this.form.controls.permissions as FormGroup).registerControl(String(ps.permissionId), this.fb.control(false));
      });
    });

  }

  setApp(id) {
    this.getAppCodeByAppId(id);
    this.group.getProgramByAppId(id).subscribe(p => {
      this.programList = p;
      this.initFormGroup();
    });
  }

  save() {
    if (this.form.valid) {
      this.form.controls['code'].setValue(this.appCode.concat(this.form.value.code));
      console.log(this.form.getRawValue());
      this.group.save(this.form.getRawValue()).subscribe(
        response => {
          this.modal.openModal('บันทึกสำเร็จ', 'บันทึกสำเร็จ');
          const modalHide = this.modal.modalService.onHide.subscribe(e => {
            modalHide.unsubscribe();
            this.router.navigate(['/group-management']);
          });
        },
        err => {
          this.modal.openModal('เกิดข้อผิดพลาด', 'ไม่สามารถบันทึกได้กรุณาทำรายการใหม่ภายหลัง');
        }
      );
    } else {
      this.group.validateAllFormFields( this.form );
    }
  }

  get code() {
    return this.form.get('code');
  }



}
