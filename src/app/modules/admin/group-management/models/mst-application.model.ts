
export interface MSTApplication {
  appId: number;
  appCode: string;
  appNameEn: string;
  appNameTh: string;
}
