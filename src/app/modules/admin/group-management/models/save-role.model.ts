export interface SaveRole {
  id?: number| null;
  name: string;
  permissions: Object;
}
