import { Permission } from './permission.model';

export interface Group {
  id?: number;
  appId?: number;
  name?: string;
  code?: string;
  desc?: string;
  status?: string;
  permissions?: Permission[] | null;
}
