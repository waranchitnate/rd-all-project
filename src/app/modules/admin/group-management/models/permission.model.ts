export interface Permission {
  permissionId: number;
  permissionCode: string;
  permissionName: string;
}
