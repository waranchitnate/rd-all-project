import {Permission} from './permission.model';

export interface Program {
  programCode: string;
  programName: string;
  permissions: Permission[];
}
