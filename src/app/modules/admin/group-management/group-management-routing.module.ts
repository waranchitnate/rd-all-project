import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './pages/search/search.component';
import { AddComponent } from './pages/add/add.component';
import { EditComponent } from './pages/edit/edit.component';

const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full'},
  { path: 'search', component: SearchComponent},
  { path: 'add', component: AddComponent},
  { path: 'edit/:id/:code', component: EditComponent, data: {title: 'แก้ไขกลุ่มสิทธิ์', disabled:  false}},
  { path: 'view/:id/:code', component: EditComponent, data: {title: 'เรียกดูกลุ่มสิทธิ์', disabled: true}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupManagementRoutingModule { }
