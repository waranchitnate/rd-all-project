import { OrganizationSearchComponent } from './pages/organization-search/organization-search.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../../core/guard/auth.guard';
import { OrganizationFormComponent } from './pages/organization-form/organization-form.component';

const routes: Routes = [
  { path: 'search', component: OrganizationSearchComponent, canActivate: [AuthGuard] },
  { path: 'form', component: OrganizationFormComponent, canActivate: [AuthGuard] },
  { path: ':orgId/edit', component: OrganizationFormComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationRoutingModule { }
