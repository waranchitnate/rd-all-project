import { OrganizationService } from './services/organization.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OrganizationRoutingModule } from './organization-routing.module';
import { OrganizationSearchComponent } from './pages/organization-search/organization-search.component';
import { OrganizationFormComponent } from './pages/organization-form/organization-form.component';
import { SharedModule } from '../../../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [OrganizationSearchComponent, OrganizationFormComponent],
  imports: [
    CommonModule,
    OrganizationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [OrganizationService]
})
export class OrganizationModule { }
