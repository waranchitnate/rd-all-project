import { OrganizationService } from '../../services/organization.service';
import { Component, OnInit } from '@angular/core';
import { Organization } from 'src/app/modules/admin/setting-organization/models/organization.model';
import { PageResponse } from '../../../../../shared/models/page-response';
import { PageRequest } from '../../../../../shared/models/page-request';

@Component({
  selector: 'app-organization-search',
  templateUrl: './organization-search.component.html',
  styleUrls: ['./organization-search.component.css']
})
export class OrganizationSearchComponent implements OnInit {
  organizationList = <Organization[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  orgName = '';

  constructor(private orgService: OrganizationService) { }

  ngOnInit() {
    this.pageRequest.sortFieldName = 'orgName';
    this.pagination();

  }

  pageChanged() {
    this.pageRequest.page = this.pageResponse.number;
    this.pagination();
  }

  sort(fieldName) {
    if (fieldName == this.pageRequest.sortFieldName) {
      if (this.pageRequest.sortDirection == 'DESC') {
        this.pageRequest.sortDirection = 'ASC';
      } else {
        this.pageRequest.sortDirection = 'DESC';
      }
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.pageRequest.page = this.pageResponse.number;
    this.pagination();
  }

  pagination() {
    this.pageRequest.page--;
    this.orgService.getOrganizationPaging(this.orgName, this.pageRequest).subscribe(res => {
      // spring page start at 0 but Ngb Page start at 1
      res.number++;
      this.pageResponse = res;

    });
  }

}
