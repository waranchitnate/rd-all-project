import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { OrganizationService } from '../../services/organization.service';
import { Organization, OrganizationType } from 'src/app/modules/admin/setting-organization/models/organization.model';
import { Status } from 'src/app/core/models/status.model';
import { ActivatedRoute, Router } from '@angular/router';
import { OrganizationModule } from '../../organization.module';
import { Location } from '@angular/common';

@Component({
  selector: 'app-organization-form',
  templateUrl: './organization-form.component.html',
  styleUrls: ['./organization-form.component.css']
})
export class OrganizationFormComponent implements OnInit {
  organizationTypeList = <OrganizationType[]>[];
  statusTypeList = <Status[]>[];
  organizationForm: FormGroup;

  constructor(
    private orgService: OrganizationService,
    private router: Router,
    private actRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private location: Location,
  ) { }

  ngOnInit() {
    this.orgService.getAllOrganizationType().subscribe((result: any) => {
      this.organizationTypeList = result.data;
    });
    this.orgService.getAllStatusType().subscribe((result: any) => {
      this.statusTypeList = result.data;
    });

    this.initOrganizationData();
    this.getOrganization(this.actRoute.snapshot.paramMap.get('orgId'));

    // console.log(this.organizationForm.get('organizationType'));


  }

  // Accessing form control using getters
  get orgCode() {
    return this.organizationForm.get('orgCode');
  }

  get orgName() {
    return this.organizationForm.get('orgName');
  }


  // Contains Reactive Form logic
  initOrganizationData() {
    this.organizationForm = this.formBuilder.group({
      orgId: [''],
      orgCode: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      orgName: [''],
      organizationType: this.formBuilder.group({
        createdBy: [''],
        createdDate: [''],
        updatedBy: [''],
        updatedDate: [''],
        organizationTypeId: [''],
        organizationTypeName: [''],
        status: this.formBuilder.group({
          statusCode: [''],
          statusEngDesc: [''],
          statusThDesc: ['']
        })
      }),
      status: this.formBuilder.group({
        statusCode: [''],
        statusEngDesc: [''],
        statusThDesc: ['']
      }),
      organizations: [''],
      createdBy: [''],
      createdDate: [''],
      updatedBy: [''],
      updatedDate: [''],
      parentOrg: [''],
      childrenOrg: ['']
    });
  }

  // Go back to previous component
  goBack() {
    this.location.back();
  }

  // Below methods fire when somebody click on submit button
  updateOrg() {
    this.orgService.saveOrganization(this.organizationForm.value).subscribe((result: any) => {
    });
    this.router.navigate(['organization/search']);
  }

  getOrganization(orgId) {
    this.orgService.getOrganizationById(parseInt(orgId)).subscribe((result: any) => {
      this.organizationForm.setValue(result.data);
    });
  }

}
