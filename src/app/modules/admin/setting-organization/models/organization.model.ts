export interface Organization {
  orgId?: number;
  orgCode: string;
  orgName: string;
  organizationType: OrganizationType;
  organization?: Organization;
  createdDate: any;
  createdBy: any;
  updatedDate: any;
  updatedBy: any;
}

export interface OrganizationType {
  organizationTypeId?: string;
  organizationTypeName?: string;
  createdDate: any;
  createdBy: any;
  updatedDate: any;
  updatedBy: any;
}
