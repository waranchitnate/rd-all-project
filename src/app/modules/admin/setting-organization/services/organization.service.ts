import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ResponseT } from '../../../../shared/models/response.model';
import { Organization } from '../models/organization.model';
import { PageResponse } from '../../../../shared/models/page-response';
import { tap } from 'rxjs/operators';

@Injectable()
export class OrganizationService {
  url = environment.webApiUrl + 'organization';

  constructor(private http: HttpClient) { }

  getAllOrganizations(): Observable<ResponseT<Organization[]>> {
    return <Observable<ResponseT<Organization[]>>>this.http.get(this.url + '/getAllOrganizations');
  }

  getOrganizationPaging(name, pageRequest) {
    return <Observable<PageResponse<any>>>this.http.post(this.url + '/getOrganizationPaging?organizationName=' + name, pageRequest);
  }

  getOrganizationById(orgId: number): Observable<ResponseT<Organization>> {
    return <Observable<ResponseT<Organization>>>this.http.get(this.url + '/getOrganizationById?orgId=' + orgId);
  }

  saveOrganization(organization: Organization): Observable<ResponseT<Organization>> {
    return <Observable<ResponseT<Organization>>>this.http.post(this.url + '/saveOrganization', organization);
  }

  getAllOrganizationType(): Observable<ResponseT<OrientationType[]>> {
    return <Observable<ResponseT<OrientationType[]>>>this.http.get(this.url + '/getAllOrganizationType');
  }

  getAllStatusType(): Observable<ResponseT<OrientationType[]>> {
    return <Observable<ResponseT<OrientationType[]>>>this.http.get(this.url + '/getAllStatusType');
  }
}
