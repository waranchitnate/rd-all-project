import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigatortabComponent } from './navigatortab.component';

describe('NavigatortabComponent', () => {
  let component: NavigatortabComponent;
  let fixture: ComponentFixture<NavigatortabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigatortabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigatortabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
