import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigatorWhtTabComponent } from './navigator-wht-tab.component';

describe('NavigatorWhtTabComponent', () => {
  let component: NavigatorWhtTabComponent;
  let fixture: ComponentFixture<NavigatorWhtTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigatorWhtTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigatorWhtTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
