import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../models/menu-item.model';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter, finalize } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { Subject, Subscription } from 'rxjs';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {

    home: MenuItem = {
        key: 'whtLanding',
        label: 'หน้าหลัก',
        url: '/dss/landing',
    };
    menuItems: MenuItem[];
    private _labelSubscription$: Subscription;

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private bs: BreadcrumbService) { }

    ngOnInit() {
        this.menuItems = this.createBreadcrumbs(this.activatedRoute.root);
        this.updateDynamicLabel();

        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd)
        ).subscribe(() => {
            this.menuItems = this.createBreadcrumbs(this.activatedRoute.root);
            this.updateDynamicLabel();
        });
    }

    updateDynamicLabel() {
        if (this._labelSubscription$) this._labelSubscription$.unsubscribe();
        this._labelSubscription$ = this.bs.getBreadcrumbLabel().subscribe(menuItems => {
            if (menuItems && menuItems.length > 0) {
                this.menuItems = this.createBreadcrumbs(this.activatedRoute.root);
                for (let menuItem of menuItems) {
                    let target = this.menuItems.find((obj) => obj.key == menuItem.key);
                    if (target && target.label) {
                        const keys = target.label.match(/[^{{]+(?=\}})/g);
                        if (keys) {
                            for (const key of keys) {
                                const replace = menuItem.params.find((obj) => obj.key == key).value;
                                this.menuItems.find((obj) => obj.key == menuItem.key).label = this.menuItems.find((obj) => obj.key == menuItem.key).label.replace('{{' + key + '}}', replace);
                            }
                        }
                    }
                }
            }
        });
    }

    private createBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: MenuItem[] = []): MenuItem[] {
        const children: ActivatedRoute[] = route.children;

        if (children.length === 0) {
            return breadcrumbs;
        }

        for (const child of children) {
            const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');
            if (routeURL !== '') {
                url += `/${routeURL}`;
            }

            const breadcrumb = child.snapshot.data['breadcrumb'];
            if (!isNullOrUndefined(breadcrumb)) {
                breadcrumbs.push({
                    key: breadcrumb.key,
                    label: breadcrumb.label,
                    url: url
                });
            }

            return this.createBreadcrumbs(child, url, breadcrumbs);
        }
    }

}
