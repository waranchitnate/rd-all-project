import { Component, OnInit } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {EdcTaxInputTaxModel} from '../../models/edc-tax-input-tax.model';
import {EPaymentService} from '../../services/e-payment.service';

@Component({
  selector: 'app-vat-pp36-transaction-detail',
  templateUrl: './vat-pp36-transaction-detail.component.html',
  styleUrls: ['./vat-pp36-transaction-detail.component.css']
})
export class VatPp36TransactionDetailComponent implements OnInit {

  pin: string;
  model: EdcTaxInputTaxModel;
  constructor(private modalRef: BsModalRef, private ePaymentService: EPaymentService) { }

  ngOnInit() {
    console.log(this.pin);
    this.ePaymentService.getBySenderTaxId(this.pin).subscribe(res => {
      this.model = res.data[0];
    });
  }

}
