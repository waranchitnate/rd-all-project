import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilingSearhModalComponent } from './filing-searh-modal.component';

describe('FilingSearhModalComponent', () => {
  let component: FilingSearhModalComponent;
  let fixture: ComponentFixture<FilingSearhModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilingSearhModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilingSearhModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
