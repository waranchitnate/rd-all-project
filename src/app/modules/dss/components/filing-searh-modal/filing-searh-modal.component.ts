import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Component, EventEmitter, isDevMode, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-filing-searh-modal',
  templateUrl: './filing-searh-modal.component.html',
  styleUrls: ['./filing-searh-modal.component.css']
})
export class FilingSearhModalComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
   
  }

  constructor(public bsModalRef: BsModalRef,
    private bsModalService: BsModalService,
    private modalService: NgbModal) { }

  ngOnInit() {
     
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static',windowClass: 'modal-xxl' }).result.then((result) => {
      //this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
     // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  

}
