import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalSystemAuthenticationComponent } from './external-system-authentication.component';

describe('ExternalSystemAuthenticationComponent', () => {
  let component: ExternalSystemAuthenticationComponent;
  let fixture: ComponentFixture<ExternalSystemAuthenticationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalSystemAuthenticationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalSystemAuthenticationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
