import { Component, OnInit } from '@angular/core';
import {DssAuthenticationService} from '../../services/dss-authentication.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-external-system-authentication',
  templateUrl: './external-system-authentication.component.html',
  styleUrls: ['./external-system-authentication.component.css']
})
export class ExternalSystemAuthenticationComponent implements OnInit {

  private systemRequestId: string;
  constructor(private dssAuthenticationService: DssAuthenticationService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    const queryParam = this.activatedRoute.snapshot.queryParams;
    this.systemRequestId = queryParam.systemRequestId;
    this.dssAuthenticationService.login(queryParam.access_token).subscribe(() => {
      this.router.navigateByUrl('dss/certverify');
    });
  }

}
