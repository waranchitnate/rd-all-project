import {Component, Input, OnInit, Output} from '@angular/core';
import {UserAttachmentModel} from '../../../admin/public-user-management/models/user-attachment.model';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-tabbody',
  templateUrl: './tabbody.component.html',
  styleUrls: ['./tabbody.component.css']
})
export class TabbodyComponent implements OnInit {

  nId: string;
  taxPayerName: string;
  @Input() page: string;
  constructor() { }

  ngOnInit() {
    console.log(this.page);
  }

}
