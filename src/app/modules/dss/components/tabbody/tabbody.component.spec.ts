import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabbodyComponent } from './tabbody.component';

describe('TabbodyComponent', () => {
  let component: TabbodyComponent;
  let fixture: ComponentFixture<TabbodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabbodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabbodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
