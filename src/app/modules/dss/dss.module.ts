import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DssRoutingModule } from './dss-routing.module';
import { LandingComponent } from './pages/landing/landing.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EPaymentSearchComponent } from './pages/e-payment-search/e-payment-search.component';
import { NavigatortabComponent } from './components/navigatortab/navigatortab.component';
import { TabbodyComponent } from './components/tabbody/tabbody.component';
import { EPaymentResultComponent } from './pages/e-payment-result/e-payment-result.component';
import { EPaymentSummaryComponent } from './pages/e-payment-summary/e-payment-summary.component';
import { PrintEPaymentSummaryComponent } from './pages/print-e-payment-summary/print-e-payment-summary.component';
import { SummaryPerTaxidComponent } from './pages/summary-per-taxid/summary-per-taxid.component';
import { VatPp36ByMonthComponent } from './pages/vat-pp36-by-month/vat-pp36-by-month.component';
import { FilingSearhModalComponent } from './components/filing-searh-modal/filing-searh-modal.component';
import { VatPp36TransactionDetailComponent } from './components/vat-pp36-transaction-detail/vat-pp36-transaction-detail.component';
import { WhtPnd53ByMonthComponent } from './pages/wht-pnd53-by-month/wht-pnd53-by-month.component';
import { NavigatorWhtTabComponent } from './components/navigator-wht-tab/navigator-wht-tab.component';
import { WhtPnd53PerMonthComponent } from './pages/wht-pnd53-per-month/wht-pnd53-per-month.component';
import { VatPp36MonthsComponent } from './pages/vat-pp36-months/vat-pp36-months.component';
import { Pnd3654ByMonthComponent } from './pages/pnd3654-by-month/pnd3654-by-month.component';
import { Pnd3654PerMonthComponent } from './pages/pnd3654-per-month/pnd3654-per-month.component';
import { WhtPnd1ByMonthComponent } from './pages/wht-pnd1-by-month/wht-pnd1-by-month.component';
import { SpecialSearchComponent } from './pages/special-search/special-search.component';
import { EtaxSummaryPerTaxidComponent } from './pages/etax-summary-per-taxid/etax-summary-per-taxid.component';
import { OutputTaxByMonthComponent } from './pages/output-tax-by-month/output-tax-by-month.component';
import { SpecialByMonthComponent } from './pages/special-by-month/special-by-month.component';
import { OutputTaxPerMonthComponent } from './pages/output-tax-per-month/output-tax-per-month.component';
import { TaxInvoiceComponent } from './pages/tax-invoice/tax-invoice.component';
import { WhtSpendingComponent } from './pages/wht-spending/wht-spending.component';
import { WhtTransferComponent } from './pages/wht-transfer/wht-transfer.component';
import { WhtTransfereeComponent } from './pages/wht-transferee/wht-transferee.component';
import { WhtTransferPerMonthComponent } from './pages/wht-transfer-per-month/wht-transfer-per-month.component';
import { WhtTransfereePerMonthComponent } from './pages/wht-transferee-per-month/wht-transferee-per-month.component';
import { WhtSpendingSearchComponent } from './pages/wht-spending-search/wht-spending-search.component';
import { EtaxSummarySearchComponent } from './pages/etax-summary-search/etax-summary-search.component';
import { HistoryComponent } from './pages/history/history.component';
import { ExternalSystemComponent } from './pages/external-system/external-system.component';
import { BatchHistoryComponent } from './pages/batch-history/batch-history.component';
import { ExternalHistoryComponent } from './pages/external-history/external-history.component';
import { NgxMaskModule } from 'ngx-mask';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CertverifyComponent } from './pages/certverify/certverify.component';
import { ExternalSystemAuthenticationComponent } from './components/external-system-authentication/external-system-authentication.component';
import { EPaymentMonthlyComponent } from './pages/e-payment-monthly/e-payment-monthly.component';
import { EPaymentPerMonthComponent } from './pages/e-payment-per-month/e-payment-per-month.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { WhtBaseComponent } from './pages/wht-base/wht-base.component';
import { WhtSpendingResultComponent } from './pages/wht-spending-result/wht-spending-result.component';
import { BackButtonComponent } from './components/back-button/back-button.component';
import { ModalShowPdfComponent } from './pages/modal-show-pdf/modal-show-pdf.component';
import { PreviewPdfComponent } from './pages/preview-pdf/preview-pdf.component';

@NgModule({
    declarations: [
        LandingComponent,
        EPaymentSearchComponent,
        NavigatortabComponent,
        TabbodyComponent,
        EPaymentResultComponent,
        EPaymentSummaryComponent,
        PrintEPaymentSummaryComponent,
        SummaryPerTaxidComponent,
        VatPp36ByMonthComponent,
        FilingSearhModalComponent,
        VatPp36TransactionDetailComponent,
        WhtPnd53ByMonthComponent,
        NavigatorWhtTabComponent,
        WhtPnd53PerMonthComponent,
        VatPp36MonthsComponent,
        Pnd3654ByMonthComponent,
        WhtPnd1ByMonthComponent,
        Pnd3654PerMonthComponent,
        EtaxSummaryPerTaxidComponent,
        OutputTaxByMonthComponent,
        OutputTaxPerMonthComponent,
        SpecialSearchComponent,
        SpecialByMonthComponent,
        TaxInvoiceComponent,
        WhtSpendingComponent,
        WhtTransferComponent,
        WhtTransfereeComponent,
        WhtTransferPerMonthComponent,
        WhtTransfereePerMonthComponent,
        WhtSpendingSearchComponent,
        EtaxSummarySearchComponent,
        HistoryComponent,
        ExternalSystemComponent,
        BatchHistoryComponent,
        ExternalHistoryComponent,
        ExternalSystemAuthenticationComponent,
        EPaymentMonthlyComponent,
        EPaymentPerMonthComponent,
        CertverifyComponent,
        BreadcrumbComponent,
        WhtBaseComponent, WhtSpendingResultComponent, BackButtonComponent, ModalShowPdfComponent,PreviewPdfComponent
        
    ],
    imports: [
        CommonModule,
        DssRoutingModule,
        SharedModule,
        PdfViewerModule,
        NgxMaskModule.forRoot(),
    ],
    entryComponents: [
        VatPp36TransactionDetailComponent
    ]
})
export class DssModule { }
