export const DssConstant = {
    monthList: [
        { value: 0, name: 'มกราคม' },
        { value: 1, name: 'กุมภาพันธ์' },
        { value: 2, name: 'มีนาคม' },
        { value: 3, name: 'เมษายน' },
        { value: 4, name: 'พฤษภาคม' },
        { value: 5, name: 'มิถุนายน' },
        { value: 6, name: 'กรกฏาคม' },
        { value: 7, name: 'สิงหาคม' },
        { value: 8, name: 'กันยายน' },
        { value: 9, name: 'ตุลาคม' },
        { value: 10, name: 'พฤศจิกายน' },
        { value: 11, name: 'ธันวาคม' },
    ],
    pndTypeList: [
        { value: 'PND01', name: 'ภ.ง.ด.1' },
        { value: 'PND02', name: 'ภ.ง.ด.2' },
        { value: 'PND03', name: 'ภ.ง.ด.3' },
        { value: 'PND53', name: 'ภ.ง.ด.53' },
        { value: 'PND54', name: 'ภ.ง.ด.54' },
        { value: 'PP36', name: 'ภ.พ.36' },
        { value: 'PND54PP36', name: 'ภ.ง.ด.54 และ ภ.พ.36' },
    ],
    grouppndList: [
        { value: 'WHT', name: 'รายการที่หักภาษี ณ ที่จ่าย' },
        { value: 'DEDUCTION', name: 'รายการที่ถูกหักภาษี ณ ที่จ่าย' },
        { value: 'VAT_PP36', name: 'ยอดซื้อภาษีมูลค่าเพิ่ม' },
        { value: 'OTHER', name: 'ข้อมูล' },
    ],
    spendingList: [
        { value: 'SENDER', name: 'รายการที่โอน' },
        { value: 'RECEIVE', name: 'รายการที่รับโอน' },
    ]    
}