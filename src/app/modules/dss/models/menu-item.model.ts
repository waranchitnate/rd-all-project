export interface MenuItem {
    key: string;
    label?: string;
    url?: string;
    params?: MenuItemParam[];
}

export interface MenuItemParam {
    key: string;
    value: any;
}