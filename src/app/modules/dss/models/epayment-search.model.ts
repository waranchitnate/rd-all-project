export interface EpaymentSearchModel {
    taxPayerNo: string;
    taxPayerName: string;
    branch: string;
    branchNo: string;
    taxType: string;
    taxCode: string;
    taxMonthStart: string;
    taxYearStart: string;
    taxMonthEnd: string;
    taxYearEnd: string;
}