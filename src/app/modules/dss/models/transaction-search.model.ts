export interface TransactionSearchModel {
    taxMonth: string;
    taxYear: string;
    pin: string;
    pndType: string;
}
