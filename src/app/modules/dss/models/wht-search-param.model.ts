export interface WHTSearchParams {
    pin?: string;
    pndGroup?: string;
    pndType?: string;
    taxMonth?: string;
    taxYear?: string;
    typeSpending?: string;
    branchNo?:string;
}