export class WhtPnd53Model {
  filingId: number;
  ppType: string;
  taxFilingNo: string;
  taxpayerPin: string;
  taxpayerTitleCode: string;
  taxpayerTitle: string;
  taxpayerFirstname: string;
  taxpayerLastname: string;
  taxpayerType: string;
  branchNo: string;
  nameBuilding: string;
  roomNo: string;
  floorNo: string;
  village: string;
  addressNo: string;
  moo: string;
  soi: string;
  lane: string;
  road: string;
  subDistrictCode: string;
  subDistrict: string;
  districtCode: string;
  district: string;
  provinceCode: string;
  province: string;
  postcode: string;
  recipientPin: string;
  recipientTitleCode: string;
  recipientTitle: string;
  recipientFirstname: string;
  recipientMiddlename: string;
  recipientLastname: string;
  recipientType: string;
  recipientBuilding: string;
  recipientAddressNo: string;
  recipientRoad: string;
  city: string;
  state: string;
  countryCode: string;
  countryName: string;
  recipientPostcode: string;
  recTambol: string;
  recAmphur: string;
  recProvince: string;
  homeOfficeCode: string;
  officeName: string;
  receiptNo: string;
  uid: string;
  dln: string;
  ltoFlag: string;
  taxYear: number;
  taxMonth: number;
  taxYearmonth: Date;
  filingType: string;
  additionalSeq: number;
  vatTypeCode: number;
  vatTypeName: string;
  subVatTypeCode: number;
  subVatTypeName: string;
  filingStatus: string;
  amount: number;
  vatAmount: number;
  surcharge: number;
  pernalty: number;
  totalVatAmount: number;
  paidAmount: number;
  exchangeDocNo: string;
  payFor: string;
  payForOther: string;
  paymentDate: Date;
  paymentTime: string;
  tclPayDate: Date;
  transferNo: string;
  transferDate: Date;
  refTaxFilingNo: string;
  sbtStatus: string;
  recipientSbtStatus: string;
  vatCal: number;
  vatDiff: number;
  vatBalanceDiff: number;
  vatDiscrepancyPos: number;
  vatDiscrepancyMinus: number;
  vatStatus: string;
  refNo: string;
  recalCode: string;
  adjustBy: string;
  adjustFlag: number;
  loginOfficeCode: string;
  processingId: number;
  processingDetailId: number;
  createdDate: Date;
  createdBy: string;
  updatedDate: Date;
  updatedBy: string;
  payerPin: string;
  reciptPin: string;
}
