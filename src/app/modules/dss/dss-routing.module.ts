import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './pages/landing/landing.component';
import { EPaymentSearchComponent } from './pages/e-payment-search/e-payment-search.component';
import { EPaymentResultComponent } from './pages/e-payment-result/e-payment-result.component';
import { EPaymentSummaryComponent } from './pages/e-payment-summary/e-payment-summary.component';
import { EPaymentMonthlyComponent } from './pages/e-payment-monthly/e-payment-monthly.component';
import { PrintEPaymentSummaryComponent } from './pages/print-e-payment-summary/print-e-payment-summary.component';
import { SummaryPerTaxidComponent } from './pages/summary-per-taxid/summary-per-taxid.component';
import { VatPp36ByMonthComponent } from './pages/vat-pp36-by-month/vat-pp36-by-month.component';
import { WhtPnd53ByMonthComponent } from './pages/wht-pnd53-by-month/wht-pnd53-by-month.component';
import { WhtPnd53PerMonthComponent } from './pages/wht-pnd53-per-month/wht-pnd53-per-month.component';
import { VatPp36MonthsComponent } from './pages/vat-pp36-months/vat-pp36-months.component';
import { Pnd3654ByMonthComponent } from './pages/pnd3654-by-month/pnd3654-by-month.component';
import { Pnd3654PerMonthComponent } from './pages/pnd3654-per-month/pnd3654-per-month.component';
import { WhtPnd1ByMonthComponent } from './pages/wht-pnd1-by-month/wht-pnd1-by-month.component';
import { SpecialSearchComponent } from './pages/special-search/special-search.component';
import { EtaxSummarySearchComponent } from './pages/etax-summary-search/etax-summary-search.component';
import { EtaxSummaryPerTaxidComponent } from './pages/etax-summary-per-taxid/etax-summary-per-taxid.component';
import { OutputTaxByMonthComponent } from './pages/output-tax-by-month/output-tax-by-month.component';
import { OutputTaxPerMonthComponent } from './pages/output-tax-per-month/output-tax-per-month.component';
import { TaxInvoiceComponent } from './pages/tax-invoice/tax-invoice.component';
import { SpecialByMonthComponent } from './pages/special-by-month/special-by-month.component';
import { WhtTransferComponent } from './pages/wht-transfer/wht-transfer.component';
import { WhtTransferPerMonthComponent } from './pages/wht-transfer-per-month/wht-transfer-per-month.component';
import { WhtSpendingComponent } from './pages/wht-spending/wht-spending.component';
import { WhtTransfereeComponent } from './pages/wht-transferee/wht-transferee.component';
import { WhtTransfereePerMonthComponent } from './pages/wht-transferee-per-month/wht-transferee-per-month.component';
import { WhtSpendingSearchComponent } from './pages/wht-spending-search/wht-spending-search.component';
import { BatchHistoryComponent } from './pages/batch-history/batch-history.component';
import { HistoryComponent } from './pages/history/history.component';
import { ExternalSystemComponent } from './pages/external-system/external-system.component';
import { ExternalHistoryComponent } from './pages/external-history/external-history.component';
import { EPaymentPerMonthComponent } from './pages/e-payment-per-month/e-payment-per-month.component';
import { CertverifyComponent } from './pages/certverify/certverify.component';
import { ExternalSystemAuthenticationComponent } from './components/external-system-authentication/external-system-authentication.component';
import { DssAuthGuard } from './guard/dss-auth.guard';
import { AuthGuard } from '../../core/guard/auth.guard';
import { WhtBaseComponent } from './pages/wht-base/wht-base.component';
import { WhtSpendingResultComponent } from './pages/wht-spending-result/wht-spending-result.component';
import { ModalShowPdfComponent} from './pages/modal-show-pdf/modal-show-pdf.component';
import { PreviewPdfComponent } from './pages/preview-pdf/preview-pdf.component';
const routes: Routes = [
    { path: 'landing', component: LandingComponent },
    { path: 'certverify', component: CertverifyComponent },
    {
        path: 'wht',
        component: WhtBaseComponent,
        children: [
            {
                path: 'search',
                data: {
                    breadcrumb: {
                        key: 'whtSearch',
                        label: 'คัดค้นภาษีเงินได้หัก ณ ที่จ่ายและภาษีมูลค่าเพิ่ม'
                    }
                },
                children: [
                    {
                        path: '',
                        component: EPaymentSearchComponent,
                        data: {
                            breadcrumb: null
                        },
                    },
                    {
                        path: 'result',
                        component: EPaymentResultComponent,
                        data: {
                            breadcrumb: {
                                key: 'whtResult',
                                label: 'ผลการคัดค้นข้อมูลผู้เสียภาษีอากร'
                            }
                        },
                    },
                    {
                        path: 'summary/:pin',
                        component: EPaymentSummaryComponent,
                        data: {
                            breadcrumb: {
                                key: 'whtSummary',
                                label: 'ผลการคัดค้นแบบสรุป'
                            }
                        },
                        children: [
                            {
                                path: 'monthly/:groupPnd/:pndType',
                                component: EPaymentMonthlyComponent,
                                data: {
                                    breadcrumb: {
                                        key: 'whtMonthly',
                                        label: '{{titleName}} {{pndType}} รายเดือน'
                                    }
                                },
                                children: [
                                    {
                                        path: 'per-month/:taxMonth/:taxYear',
                                        component: EPaymentPerMonthComponent,
                                        data: {
                                            breadcrumb: {
                                                key: 'whtPerMonth',
                                                label: 'เดือน{{monthName}} {{year}}'
                                            }
                                        }
                                    }
                                ]
                            }
                        ]
                    },
                ]
            }
        ]
    },
    { path: 'epayment-search', component: EPaymentSearchComponent },
    { path: 'epayment-result', component: EPaymentResultComponent },
    { path: 'epayment-summary/:pin', component: EPaymentSummaryComponent },
    { path: 'epayment-monthly/:pin/:groupPnd/:pndType', component: EPaymentMonthlyComponent },
    { path: 'epayment-per-month/:pin/:taxMonth/:taxYear/:groupPnd/:pndType', component: EPaymentPerMonthComponent },
    { path: 'print-epayment-summary', component: PrintEPaymentSummaryComponent },
    { path: 'summary-per-taxid', component: SummaryPerTaxidComponent },
    { path: 'vat-pp36-by-month', component: VatPp36ByMonthComponent },
    { path: 'wht-pnd53-by-month', component: WhtPnd53ByMonthComponent },
    { path: 'wht-pnd53-per-month', component: WhtPnd53PerMonthComponent },
    { path: 'vat-pp36-months', component: VatPp36MonthsComponent },
    { path: 'pnd3654-by-month', component: Pnd3654ByMonthComponent },
    { path: 'pnd3654-per-month', component: Pnd3654PerMonthComponent },
    { path: 'wht-pnd1-by-month', component: WhtPnd1ByMonthComponent },
    { path: 'modal-show-pdf', component: ModalShowPdfComponent },
    { path: 'preview-pdf' ,component:PreviewPdfComponent },
    { path: 'wht',
      component: WhtBaseComponent, 
      children: [
        {
            path: 'special-search',
            data: {
                breadcrumb: {
                    key: 'whtSpecialSearch',
                    label: 'ข้อมูลธุรกรรมลักษณะเฉพาะ (Special Transaction)'
                }
            },
            children: [
                {
                    path: '',
                    component: SpecialSearchComponent,
                    data: {
                        breadcrumb: null
                    },
                },
                {
                    path: ':pin/special-by-month/:year',
                    component: SpecialByMonthComponent,
                    data: {
                        breadcrumb: {
                            key: 'whtSpecialByMonth',
                            label: 'ผลการคัดค้นธุรกรรมลักษณะเฉพาะแบบสรุป'
                        }
                    },
                },
            ]
        }
    ]
    },
    { path: 'special-by-month', component: SpecialByMonthComponent },
    { path: 'etax-summary-search', component: EtaxSummarySearchComponent },
    { path: 'etax-summary-per-taxid', component: EtaxSummaryPerTaxidComponent },
    { path: 'output-tax-by-month', component: OutputTaxByMonthComponent },
    { path: 'output-tax-per-month', component: OutputTaxPerMonthComponent },
    { path: 'tax-invoice', component: TaxInvoiceComponent },
    { path: 'wht-transfer', component: WhtTransferComponent },
    { path: 'wht-transfer-per-month', component: WhtTransferPerMonthComponent },
    { path: 'wht-spending-result', component: WhtSpendingComponent },
    { path: 'wht-transferee-per-month', component: WhtTransfereePerMonthComponent },
    { path: 'wht',
      component: WhtBaseComponent, 
      children: [
        {
            path: 'spending-search',
            data: {
                breadcrumb: {
                    key: 'whtSpendingSearch',
                    label: 'คัดค้นการใช้จ่าย'
                }
            },
            children: [
                {
                    path: '',
                    component: WhtSpendingSearchComponent,
                    data: {
                        breadcrumb: null
                    },
                },
                {
                    path: 'spending-result',
                    component: WhtSpendingResultComponent,
                    data: {
                        breadcrumb: {
                            key: 'whtSpendingResult',
                            label: 'ผลการคัดค้นข้อมูลผู้เสียภาษีอากร'
                        }
                    },
                },
                {
                    path: 'spending-summary/:pin',
                    component: WhtSpendingComponent,
                    data: {
                        breadcrumb: {
                            key: 'whtSpendingSummary',
                            label: 'ผลการคัดค้นแบบสรุป'
                        }
                    },
                    children: [
                        {
                            path: 'monthly/:typeSpending',
                            component: WhtTransfereeComponent,
                            data: {
                                breadcrumb: {
                                    key: 'whtSpendingMonthly',
                                    label: '{{typeSpending}} รายเดือน'
                                }
                            },
                            children: [
                                {
                                    path: 'per-month/:taxMonth/:taxYear',
                                    component: WhtTransfereePerMonthComponent,
                                    data: {
                                        breadcrumb: {
                                            key: 'whtSpendingPerMonth',
                                            label: 'เดือน{{monthName}} {{year}}'
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
            ]
        }
    ]
    },
      
    { path: 'history', component: HistoryComponent },
    { path: 'batch-history', component: BatchHistoryComponent },
    { path: 'external-system', component: ExternalSystemComponent, canActivate: [AuthGuard] },
    { path: 'external-history', component: ExternalHistoryComponent, canActivate: [DssAuthGuard] },
    { path: 'authentication', component: ExternalSystemAuthenticationComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DssRoutingModule { }
