import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, ValidatorFn, AbstractControl, ValidationErrors, Validators } from '@angular/forms';
import * as moment from 'moment';
import { DssService } from '../../services/dss.service';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EpaymentSearchModel } from '../../models/epayment-search.model';
import { DssConstant } from '../../class/dss-constants';
import { BreadcrumbService } from '../../services/breadcrumb.service';

@Component({
    selector: 'app-e-payment-search',
    templateUrl: './e-payment-search.component.html',
    styleUrls: ['./e-payment-search.component.css']
})
export class EPaymentSearchComponent implements OnInit {

    searchForm: FormGroup;
    taxPayerNo: FormControl;
    taxPayerName: FormControl;
    branch: FormControl;
    branchNo: FormControl;
    taxType: FormControl;
    taxCode: FormControl;
    taxMonthStart: FormControl;
    taxYearStart: FormControl;
    taxMonthEnd: FormControl;
    taxYearEnd: FormControl;
    isSubmit: boolean = false;

    @ViewChild('content') modalContent: ElementRef;

    monthList: any[] = DssConstant.monthList;

    yearList: any[] = [];
    taxList: any[] = [];
    taxListFilter: any[] = [];

    constructor(private fb: FormBuilder, private dssService: DssService, private router: Router, private ms: NgbModal, private bs: BreadcrumbService, private ar: ActivatedRoute, private cd: ChangeDetectorRef) {
        this.initForm();
        this.generateYearList();
    }


    ngOnInit() {
        this.branchChange();
        
        this.dssService.getPndType().subscribe(data => {
            this.taxList = [...data.data];
            this.filterTaxList();
        });
    }

    initForm() {
        let date = new Date();
        this.taxPayerNo = this.fb.control(null);
        this.taxPayerName = this.fb.control(null);
        this.branch = this.fb.control('');
        this.branchNo = this.fb.control(null);
        this.taxType = this.fb.control('');
        this.taxCode = this.fb.control('');
        this.taxMonthStart = this.fb.control(date.getMonth());
        this.taxYearStart = this.fb.control(date.getFullYear());
        this.taxMonthEnd = this.fb.control(date.getMonth());
        this.taxYearEnd = this.fb.control(date.getFullYear());

        this.searchForm = this.fb.group({
            taxPayerNo: this.taxPayerNo,
            taxPayerName: this.taxPayerName,
            branch: this.branch,
            branchNo: this.branchNo,
            taxType: this.taxType,
            taxCode: this.taxCode,
            taxMonthStart: this.taxMonthStart,
            taxYearStart: this.taxYearStart,
            taxMonthEnd: this.taxMonthEnd,
            taxYearEnd: this.taxYearEnd,
        }, {
            validators: [
                validateTaxPayerNameOrNoRequire(),
                validateSearchTimeNotMoreThan12Month(),
            ]
        });
    }

    search() {
        this.isSubmit = true;
        if (this.searchForm.valid) {
            let taxMonthStart = (Number(this.taxMonthStart.value) + 1).toString();
            let taxYearStart = (Number(this.taxYearStart.value) + 543).toString();
            let taxMonthEnd = (Number(this.taxMonthEnd.value) + 1).toString();
            let taxYearEnd = (Number(this.taxYearEnd.value) + 543).toString();
            let searchModel: EpaymentSearchModel = {
                taxPayerNo: this.taxPayerNo.value,
                taxPayerName: this.taxPayerName.value,
                branch: this.branch.value,
                branchNo: this.branchNo.value,
                taxType: this.taxType.value,
                taxCode: this.taxCode.value,
                taxMonthStart: taxMonthStart,
                taxYearStart: taxYearStart,
                taxMonthEnd: taxMonthEnd,
                taxYearEnd: taxYearEnd,
            };
            this.dssService.whtSearch(searchModel, DataTableUtils.getPageable()).subscribe((data) => {
                if (data.data.recordsTotal) {
                    this.dssService.setEpaymentSearchState(searchModel);
                    if (data.data.recordsTotal == 1) {
                        this.dssService.whtSearchParams = {
                            pin: data.data.data[0].pin,
                            branchNo: data.data.data[0].branch == undefined ? '' :data.data.data[0].branch 
                        };
                        this.router.navigate(['/dss/wht/search/summary', data.data.data[0].pin]);
                    } else {
                        this.router.navigate(['/dss/wht/search/result']);
                    }
                } else {
                    this.ms.open(this.modalContent);
                }
            });
        }
    }

    generateYearList() {
        let date = new Date();
        for (let year = date.getFullYear(); year > date.getFullYear() - 5; year--) {
            this.yearList = [...this.yearList, year];
        }
    }

    branchChange() {
        this.cd.detectChanges();
        if (this.branch.value == 2) {
            this.branchNo.setValidators(Validators.required);
            this.branchNo.enable();
        } else {
            this.branchNo.clearValidators();
            this.branchNo.disable();
            this.branchNo.setValue('');
        }
        this.branchNo.updateValueAndValidity();
    }

    filterTaxList() {
        if (this.taxType.value) {
            this.taxListFilter = this.taxList.filter((obj) => obj.taxType == this.taxType.value);
        } else {
            this.taxListFilter = this.taxList;
        }
    }

    reset() {
        this.isSubmit = false;
        this.initForm();
        this.branchChange();
    }

}

export function validateTaxPayerNameOrNoRequire(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        let taxPayerName = control.get('taxPayerName');
        let taxPayerNo = control.get('taxPayerNo');
        if (Validators.required(taxPayerName) && Validators.required(taxPayerNo)) {
            return {
                requireNameOrNo: 'ต้องระบุเลขประจำตัวผู้เสียภาษีอากร หรือชื่อผู้เสียภาษีอากร'
            };
        }
        return null;
    }
}

export function validateSearchTimeNotMoreThan12Month(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
        let startDate = new Date(control.get('taxYearStart').value, control.get('taxMonthStart').value, 1);
        let endDate = new Date(control.get('taxYearEnd').value, control.get('taxMonthEnd').value, 1);
        if (startDate > endDate) {
            return {
                searchTime: 'เดือนปีภาษีเริ่มต้นต้องน้อยกว่าเดือนปีภาษีสิ้นสุด'
            };
        } else if (endDate > moment(startDate).add(12, 'months').add(-1, 'days').toDate()) {
            return {
                searchTime: 'กำหนดช่วงเวลาที่คัดค้นต้องน้อยกว่าหรือเท่ากับ 12 เดือน'
            };
        }
        return null;
    }
}
