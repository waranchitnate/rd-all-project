import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPaymentSearchComponent } from './e-payment-search.component';

describe('EPaymentSearchComponent', () => {
  let component: EPaymentSearchComponent;
  let fixture: ComponentFixture<EPaymentSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPaymentSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPaymentSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
