import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtBaseComponent } from './wht-base.component';

describe('WhtBaseComponent', () => {
  let component: WhtBaseComponent;
  let fixture: ComponentFixture<WhtBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
