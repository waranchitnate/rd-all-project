import {Component, OnInit, OnDestroy} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {PageResponse} from 'src/app/shared/models/page-response';
import {EPaymentService} from 'src/app/modules/dss/services/e-payment.service';
import {FilingSearhModalComponent} from '../../components/filing-searh-modal/filing-searh-modal.component';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {VatFilingPp36Model} from '../../models/vat-filing-pp36.model';
import {VatPp36TransactionDetailComponent} from '../../components/vat-pp36-transaction-detail/vat-pp36-transaction-detail.component';

@Component({
  selector: 'app-vat-pp36-by-month',
  templateUrl: './vat-pp36-by-month.component.html',
  styleUrls: ['./vat-pp36-by-month.component.css']
})
export class VatPp36ByMonthComponent implements OnInit, OnDestroy {

  taxYearmonth: Date;
  pin = this.actRoute.snapshot.queryParamMap.get('pin');
  pageResponse: PageResponse<VatFilingPp36Model> = new PageResponse();

  constructor(private bsModalService: BsModalService,
              private actRoute: ActivatedRoute,
              private ePaymentService: EPaymentService,
              private modalService: NgbModal) {
  }

  ngOnDestroy(): void {

  }

  ngOnInit() {
    this.ePaymentService.getVatDetailByPin(this.pin).subscribe(
      (res: any) => {
        console.log(res.data);
        this.pageResponse.content = res.data;
        if(res.data.length !== 0) {
          this.taxYearmonth = res.data[0].taxYearmonth;
        }
      });


    //const modalRef = this.bsModalService.show(FilingSearhModalComponent);
    // (modalRef.content as FilingSearhModalComponent).title = 'ยืนยัน';
    // (modalRef.content as FilingSearhModalComponent).content = 'คุณต้องไม่อนุมัติผู้ใช้งานใช่หรือไม่';
    // const sub = (modalRef.content as FilingSearhModalComponent).answerEvent.subscribe(a => {
    //   sub.unsubscribe();
    // });
  }

  openVatPP36Detail(pin: string) {
    this.bsModalService.show(VatPp36TransactionDetailComponent, {initialState: {pin: pin}, class: 'modal-lg'});
  }


  open(content) {
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static',
      windowClass: 'modal-xxl'
    }).result.then((result) => {
      //this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


}
