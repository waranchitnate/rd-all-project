import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VatPp36ByMonthComponent } from './vat-pp36-by-month.component';

describe('VatPp36ByMonthComponent', () => {
  let component: VatPp36ByMonthComponent;
  let fixture: ComponentFixture<VatPp36ByMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VatPp36ByMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VatPp36ByMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
