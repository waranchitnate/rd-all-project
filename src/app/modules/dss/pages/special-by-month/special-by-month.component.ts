import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from "@angular/router";
import { SpecialTransService } from 'src/app/modules/dss/services/special-trans.service';
import { environment } from 'src/environments/environment';

import { ModalShowPdfComponent } from '../modal-show-pdf/modal-show-pdf.component';

@Component({
    selector: 'app-special-by-month',
    templateUrl: './special-by-month.component.html',
    styleUrls: ['./special-by-month.component.css']
})
export class SpecialByMonthComponent implements OnInit {

    pin :any;
    year :any;
    newYear :any;
    specialTransDetails: any[] = [];
    specialTransAddress: any = {};
    numberOfAccount: any;
    transferTimes: any;
    totalAmountReceived: any;
    urlPdfOpen :any;
    urlPdfTotal: string ;
    urlPdfSummary:string;
    

    constructor(private bsModalService: BsModalService,
        private modalService: NgbModal,
        private specialTransService: SpecialTransService,
        private actRoute: ActivatedRoute) { }

    ngOnInit() {
        this.pin = this.actRoute.snapshot.params.pin;
        this.year = this.actRoute.snapshot.params.year;
        this.newYear = String(Number(this.year) - 543);
        this.urlPdfOpen = environment.dssApiUrl + `specific/form/download?receiverProxy=${this.pin}&year=${this.newYear}&receiverFilingCode=`;
        this.urlPdfTotal = environment.dssApiUrl + `specific/form/download-sum?receiverProxy=${this.pin}&year=${this.newYear}`;
        this.urlPdfSummary = environment.dssApiUrl + `specific/form/download-summary?receiverProxy=${this.pin}&year=${this.newYear}`;
        this.specialTransService.getSpecialTransDetail(this.pin, this.newYear).subscribe((res) => {
            this.specialTransDetails = res.data;
            let numberOfAccount = 0;
            let transferTimes = 0;
            let totalAmountReceived = 0;
            for (let data of res.data) {
                numberOfAccount += Number(data.numberOfAccount);
                transferTimes += Number(data.transferTimes);
                totalAmountReceived += Number(data.totalAmountReceived);
            }
            this.numberOfAccount = numberOfAccount;
            this.transferTimes = transferTimes;
            this.totalAmountReceived = totalAmountReceived;
        });

        this.specialTransService.getSpecialTransAddress(this.pin, this.newYear).subscribe((res) => {
          this.specialTransAddress = res.data;
        });
    }

    // open(content, receiverFilingCode) {
    //     this.urlPdf = environment.dssApiUrl + `specific/form/download?receiverProxy=${this.pin}&year=${this.newYear}&receiverFilingCode=${receiverFilingCode}`;
    //     this.modalService.open(content, {
    //         ariaLabelledBy: 'modal-basic-title',
    //         size: 'xl',
    //         backdrop: 'static',
    //         windowClass: 'modal-xxl'
    //     });
    // }

//     openPrintSum(content) {
//       this.urlPdf = environment.dssApiUrl + `specific/form/download-sum?receiverProxy=${this.pin}&year=${this.newYear}`;
//       this.modalService.open(content, {
//           ariaLabelledBy: 'modal-basic-title',
//           size: 'lg',
//           backdrop: 'static',
//           windowClass: 'modal-xxl'
//       });
//   }

    // openPrintSummary(content) {
    //     this.urlPdf = environment.dssApiUrl + `specific/form/download-summary?receiverProxy=${this.pin}&year=${this.newYear}`;
    //     this.modalService.open(content, {
    //         ariaLabelledBy: 'modal-basic-title',
    //         size: 'lg',
    //         backdrop: 'static',
    //         windowClass: 'modal-xxl'
    //     });
    // }

    // print() {
    //     printJS({printable: this.urlPdf});
    // }

    
}
