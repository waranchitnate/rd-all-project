import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialByMonthComponent } from './special-by-month.component';

describe('SpecialByMonthComponent', () => {
  let component: SpecialByMonthComponent;
  let fixture: ComponentFixture<SpecialByMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialByMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialByMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
