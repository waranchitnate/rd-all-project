import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { DataTablePageable } from 'src/app/shared/modals/data-table/data-table-pageable';
import { DssService } from '../../services/dss.service';
import { finalize } from 'rxjs/operators';
import { SpecialSearchRequest } from "../../models/special-search-request.model";

@Component({
    selector: 'app-special-search',
    templateUrl: './special-search.component.html',
    styleUrls: ['./special-search.component.css']
})

export class SpecialSearchComponent implements OnInit {
    
    hideTable = true;
    pageable: DataTablePageable = DataTableUtils.getPageable();
    dataSet: any[] = [];
    yearList: any[] = [];
    year: string = ((new Date()).getFullYear() - 1).toString();
    name: string = null;
    dataTableStateKey = 'specialSearchDataTableState';
    specialSearchRequest: SpecialSearchRequest = {
        year: 2019,
        name: ''
      };

    constructor(private router: Router, private dssService: DssService) {
        this.generateYearList();
    }

    ngOnInit() {
        const state = DataTableUtils.getState(this.dataTableStateKey);
        this.pageable.total = this.dataSet.length;
        if (state) {
            this.specialSearchRequest = state.params;
            this.searchData();
        }else{
            this.searchData();
        }
    }

    searchData(reset?: boolean) {
        if (reset) this.pageable.pageIndex = 1;
        this.pageable.loading = true;
        this.dssService.specialSearch(this.specialSearchRequest, this.pageable).pipe(
            finalize(() => this.pageable.loading = false)
        ).subscribe(data => {
            this.dataSet = data.data.data;
            if (this.dataSet.length) {
                this.dataSet.forEach(obj =>{
                    if(obj.receiverProxyType == "NID"){
                        obj.receiverProxyType = "เลขประจำตัวประชาชน/เลขนิติบุคคล";
                    }else if(obj.receiverProxyType == "PAS"){
                        obj.receiverProxyType = "เลขพาสปอร์ต";
                    }else if(obj.receiverProxyType == "CID"){
                        obj.receiverProxyType = "เลขนิติบุคคลต่างชาติ";
                    }else if(obj.receiverProxyType == "OTH"){
                        obj.receiverProxyType = "อื่นๆ";
                    }
                });
            }    
            this.pageable.total = data.data.recordsTotal;
            DataTableUtils.saveState(this.dataTableStateKey, this.specialSearchRequest, this.pageable);
        });
    }

    generateYearList() {
        let date = new Date();
        for (let year = date.getFullYear(); year > date.getFullYear() - 3; year--) {
            this.yearList = [...this.yearList, year];
        }
    }

    routDetail(pin, year) {
        this.router.navigate(['/dss/wht/special-search/', pin, 'special-by-month',year]);
    }
}
