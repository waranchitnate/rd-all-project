import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialSearchComponent } from './special-search.component';

describe('SpecialSearchComponent', () => {
  let component: SpecialSearchComponent;
  let fixture: ComponentFixture<SpecialSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
