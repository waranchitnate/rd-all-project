import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintEPaymentSummaryComponent } from './print-e-payment-summary.component';

describe('PrintEPaymentSummaryComponent', () => {
  let component: PrintEPaymentSummaryComponent;
  let fixture: ComponentFixture<PrintEPaymentSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintEPaymentSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintEPaymentSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
