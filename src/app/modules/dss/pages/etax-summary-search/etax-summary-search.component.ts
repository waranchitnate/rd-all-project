import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-etax-summary-search',
  templateUrl: './etax-summary-search.component.html',
  styleUrls: ['./etax-summary-search.component.css']
})
export class EtaxSummarySearchComponent implements OnInit {

  branchNoSelect: string;

  constructor() { }

  ngOnInit() {
    this.branchNoSelect = '-1';
  }

}
