import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtaxSummarySearchComponent } from './etax-summary-search.component';

describe('EtaxSummarySearchComponent', () => {
  let component: EtaxSummarySearchComponent;
  let fixture: ComponentFixture<EtaxSummarySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtaxSummarySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtaxSummarySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
