import { Component, OnInit , ViewChild , TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import { DssPKIService } from '../../services/dss-pki.service';
import {ToastrService} from 'ngx-toastr';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-certverify',
  templateUrl: './certverify.component.html',
  styleUrls: ['./certverify.component.css']
})
export class CertverifyComponent implements OnInit {
  modalRef: BsModalRef;
  @ViewChild('verifyModal')
  msgTempRef: TemplateRef<any>;
  constructor(private modalService: BsModalService, private dssPKIService: DssPKIService, private toastrService: ToastrService, private router: Router ) {
  }

  ngOnInit() {
    this.showPopup(this.msgTempRef);
  }

  showPopup(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  onVerify(usbResponse: any) {
    console.log(usbResponse);
    this.dssPKIService.checkPin(usbResponse.data, usbResponse.request.time).subscribe(searchState => {
          if (searchState) {
            this.modalRef.hide();
            environment.pkiFlag = true;
            this.router.navigate(['/dss/landing']);
          } else {
            this.toastrService.error('รหัสยืนยันตัวบุคคลไม่ถูกต้องกรุณาลองใหม่อีกครั้ง', 'ข้อผิดพลาด');
          }
    }, error => {
      console.log(error.name);
  }
  );
  }
}
