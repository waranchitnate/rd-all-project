import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertverifyComponent } from './certverify.component';

describe('CertverifyComponent', () => {
  let component: CertverifyComponent;
  let fixture: ComponentFixture<CertverifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertverifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertverifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
