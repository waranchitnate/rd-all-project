import { Component, OnInit } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { VatFilingPp36Model } from '../../models/vat-filing-pp36.model';
import { EPaymentService } from '../../services/e-payment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-tax-invoice',
  templateUrl: './tax-invoice.component.html',
  styleUrls: ['./tax-invoice.component.css']
})
export class TaxInvoiceComponent implements OnInit {

  pin = this.actRoute.snapshot.queryParamMap.get('pin');
  pageResponse: PageResponse<VatFilingPp36Model> = new PageResponse();

  constructor(private actRoute: ActivatedRoute,
    private ePaymentService: EPaymentService,
    private router: Router,
    public modalService: NgbModal) { }

  ngOnInit() {
    this.ePaymentService.getVatDetailByPin(this.pin).subscribe(
      (res: any) => {
        this.pageResponse.content = res.data;
      });
  }

  viewTaxInvoiceDetail(content) {
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-tax-invoice',
      size: 'lg',
      backdrop: 'static',
      windowClass: 'modal-xxl'
    });
  }

}
