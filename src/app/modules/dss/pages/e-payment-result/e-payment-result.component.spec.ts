import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPaymentResultComponent } from './e-payment-result.component';

describe('EPaymentResultComponent', () => {
  let component: EPaymentResultComponent;
  let fixture: ComponentFixture<EPaymentResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPaymentResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPaymentResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
