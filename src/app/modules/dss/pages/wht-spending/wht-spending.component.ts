import { Component, OnInit } from '@angular/core';
import { DssService } from '../../services/dss.service';
import { Router,ActivatedRoute } from '@angular/router';
import { EpaymentSearchModel } from '../../models/epayment-search.model';
import { DssConstant } from '../../class/dss-constants';

@Component({
  selector: 'app-wht-spending',
  templateUrl: './wht-spending.component.html',
  styleUrls: ['./wht-spending.component.css']
})
export class WhtSpendingComponent implements OnInit {

  dataSet: any[] = [];
  searchState: EpaymentSearchModel;
  monthList: any[] = DssConstant.monthList;
  address: any;
  pin :any;
  branchNo :any;
  yearStart :any;
  yearEnd :any;
  constructor(private dssService: DssService, private ar: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    this.pin = this.dssService.whtSearchParams.pin;
    this.branchNo = this.dssService.whtSearchParams.branchNo;
    this.getDetail();
  }

  async getDetail() {
    this.dssService.getEpaymentSearchState().subscribe(searchState => {
        if (searchState) {
            this.searchState = searchState;
            this.yearStart = Number(searchState.taxYearStart) + 543;
            this.yearEnd = Number(searchState.taxYearEnd) + 543;
            this.dssService.getExpenseSummary(this.ar.snapshot.params.pin, searchState.taxMonthStart, searchState.taxYearStart, searchState.taxMonthEnd, searchState.taxYearEnd,this.branchNo).subscribe((data) => {
                this.dataSet = data.data
            });

            this.dssService.getExpenseAddress(this.ar.snapshot.params.pin,this.branchNo).subscribe(data => {
                this.address = data.data;
            });
        }
    });
  }

  routDetail(typeSpending: string,pin : string){
      this.dssService.whtSearchParams = {
        pin: pin,
        branchNo:this.branchNo,
        typeSpending: typeSpending
      };
      this.router.navigate(["/dss/wht/spending-search/spending-summary",pin,"monthly",typeSpending]);
  }

}
