import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtSpendingComponent } from './wht-spending.component';

describe('WhtSpendingComponent', () => {
  let component: WhtSpendingComponent;
  let fixture: ComponentFixture<WhtSpendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtSpendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtSpendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
