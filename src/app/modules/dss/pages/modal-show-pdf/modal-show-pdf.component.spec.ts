import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalShowPdfComponent } from './modal-show-pdf.component';

describe('ModalShowPdfComponent', () => {
  let component: ModalShowPdfComponent;
  let fixture: ComponentFixture<ModalShowPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalShowPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalShowPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
