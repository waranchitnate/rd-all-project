import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as printJS from 'print-js';
import { PreviewPdfComponent } from '../preview-pdf/preview-pdf.component';
@Component({
  selector: 'app-modal-show-pdf',
  templateUrl: './modal-show-pdf.component.html',
  styleUrls: ['./modal-show-pdf.component.css']
})
export class ModalShowPdfComponent implements OnInit {


  @ViewChild('content') content: ElementRef;
  //@Input('option') option: string;

  //@ViewChild('previewPdf') previewPdf: PreviewPdfComponent;

  constructor(private modalService: NgbModal ) { }
  url: any;
  headername: any;
  ngOnInit() {
  }

  openFilingFormSmall(url : any,header :any ){
    this.url = url;
    this.headername = header ;
    this.modalService.open(this.content, {
        ariaLabelledBy: 'modal-basic-title',
        size: 'lg',
        backdrop: 'static',
        windowClass: 'modal-xxl'
    });
  }
  openFilingFormLarge(url : any,header :any ){
    this.url = url;
    this.headername = header ;
    this.modalService.open(this.content, {
        ariaLabelledBy: 'modal-basic-title',
        size: <any>'xl',
        backdrop: 'static',
        windowClass: 'modal-xxl'
    });
  }
  print() {
   printJS({printable: this.url});
  }
}

