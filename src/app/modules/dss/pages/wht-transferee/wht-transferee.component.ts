import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DssService } from '../../services/dss.service';
import { EpaymentSearchModel } from '../../models/epayment-search.model';
import { DssConstant } from '../../class/dss-constants';
import { TransactionSearchService } from '../../services/transaction-search.service';
import { TransactionSearchModel } from '../../models/transaction-search.model';
import { SpendingService } from '../../services/spending.service';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { BreadcrumbService } from '../../services/breadcrumb.service';

@Component({
  selector: 'app-wht-transferee',
  templateUrl: './wht-transferee.component.html',
  styleUrls: ['./wht-transferee.component.css']
})
export class WhtTransfereeComponent implements OnInit {
  
  searchState: EpaymentSearchModel;
  pin: any;
  branchNo: any;
  monthList: any[] = DssConstant.monthList;
  typeSpending: any;
  monthsSpending: any[] = [];
  address: any;
  spendingList: any[] = DssConstant.spendingList;
  constructor(private router: Router,private dssService: DssService,private spendingService: SpendingService
    ,private transactionService: TransactionSearchService, private bs: BreadcrumbService) { }

  ngOnInit() {
    this.pin = this.dssService.whtSearchParams.pin;
    this.branchNo = this.dssService.whtSearchParams.branchNo;
    this.typeSpending = this.dssService.whtSearchParams.typeSpending;
    this.getDetail();
    this.bs.addBreadcrumbLabel({
      key: 'whtSpendingMonthly',
      params: [{ key: 'typeSpending',value: this.getSpendingName(this.typeSpending) }]
    });
  }
  async getDetail() {
    this.dssService.getEpaymentSearchState().subscribe(searchState => {
        if (searchState) {
            this.searchState = searchState;
            this.dssService.getExpenseMonthly(this.pin, searchState.taxMonthStart, searchState.taxYearStart, searchState.taxMonthEnd,
                searchState.taxYearEnd,this.typeSpending,this.branchNo).subscribe((data) => {
                    this.monthsSpending = data.data;
                })
            this.dssService.getExpenseAddress(this.pin,this.branchNo).subscribe(data => {
                this.address = data.data;
            })
        }
    });
  }
  getMonthName(key: any) {
    return this.monthList.find((obj) => obj.value == Number(key) - 1).name;
  }
  getSpendingName(key: any) {
    return this.spendingList.find((obj) => obj.value == key).name;
  }
  sum(group: any[], key: string) {
    return group.reduce((a, b) => a + (b[key] || 0), 0);
  }
  
  routDetail(taxMonth: string, taxYear: string){
    let searchModel: TransactionSearchModel = {
      taxMonth: taxMonth,
      taxYear: taxYear,
      pin: this.pin,
      pndType: this.typeSpending
    }
    this.spendingService.getExpenseTransaction(searchModel, DataTableUtils.getPageable(),this.branchNo).subscribe((data) => {
      if (data.data.recordsTotal) {
          this.transactionService.setTransactionSearchState(searchModel);
      }
    });
    this.router.navigate(["/dss/wht/spending-search/spending-summary",this.pin,"monthly",this.typeSpending, 'per-month', taxMonth, taxYear]);
  }

}
