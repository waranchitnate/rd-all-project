import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtTransfereeComponent } from './wht-transferee.component';

describe('WhtTransfereeComponent', () => {
  let component: WhtTransfereeComponent;
  let fixture: ComponentFixture<WhtTransfereeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtTransfereeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtTransfereeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
