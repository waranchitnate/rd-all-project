import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtPnd53PerMonthComponent } from './wht-pnd53-per-month.component';

describe('WhtPnd53PerMonthComponent', () => {
  let component: WhtPnd53PerMonthComponent;
  let fixture: ComponentFixture<WhtPnd53PerMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtPnd53PerMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtPnd53PerMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
