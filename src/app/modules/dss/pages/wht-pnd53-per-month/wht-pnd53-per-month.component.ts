import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-wht-pnd53-per-month',
  templateUrl: './wht-pnd53-per-month.component.html',
  styleUrls: ['./wht-pnd53-per-month.component.css']
})
export class WhtPnd53PerMonthComponent implements OnInit {

  radioSelected: string;

  constructor(private bsModalService: BsModalService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.radioSelected = '1';
  }

  open(content) {
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static',
      windowClass: 'modal-xxl'
    }).result.then((result) => {
      //this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModal(contents) {
    this.modalService.open(contents, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static',
      windowClass: 'modal-xxl'
    }).result.then((result) => {
      //this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

}
