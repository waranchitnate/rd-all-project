import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pnd3654PerMonthComponent } from './pnd3654-per-month.component';

describe('Pnd3654PerMonthComponent', () => {
  let component: Pnd3654PerMonthComponent;
  let fixture: ComponentFixture<Pnd3654PerMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pnd3654PerMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pnd3654PerMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
