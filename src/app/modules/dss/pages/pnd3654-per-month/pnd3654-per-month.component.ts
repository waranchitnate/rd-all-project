import { Component, OnInit } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { ActivatedRoute } from '@angular/router';
import { EPaymentService } from '../../services/e-payment.service';
import { VatFilingPp36Model } from '../../models/vat-filing-pp36.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pnd3654-per-month',
  templateUrl: './pnd3654-per-month.component.html',
  styleUrls: ['./pnd3654-per-month.component.css']
})
export class Pnd3654PerMonthComponent implements OnInit {

  pin = this.actRoute.snapshot.queryParamMap.get('pin');
  pageResponse: PageResponse<VatFilingPp36Model> = new PageResponse();

  constructor(private actRoute: ActivatedRoute,
    private ePaymentService: EPaymentService,
    public modalService: NgbModal) { }

  ngOnInit() {
    this.ePaymentService.getVatDetailByPin(this.pin).subscribe(
      (res: any) => {
        this.pageResponse.content = res.data;
    });
  }

  viewPnd54(content) {
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-pnd54-1',
      size: 'lg',
      backdrop: 'static',
      windowClass: 'modal-xxl'
    });
  }

  viewVat36(vat36content) {
    this.modalService.open(vat36content, {
      ariaLabelledBy: 'modal-vat36-1',
      size: 'lg',
      backdrop: 'static',
      windowClass: 'modal-xxl'
    });
  }

  viewTransaction(transactionContent) {
    this.modalService.open(transactionContent, {
      ariaLabelledBy: 'modal-vat36-1',
      size: 'lg',
      backdrop: 'static',
      windowClass: 'modal-xxl'
    });
  }
}
