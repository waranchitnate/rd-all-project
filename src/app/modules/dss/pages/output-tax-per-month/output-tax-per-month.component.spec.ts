import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputTaxPerMonthComponent } from './output-tax-per-month.component';

describe('OutputTaxPerMonthComponent', () => {
  let component: OutputTaxPerMonthComponent;
  let fixture: ComponentFixture<OutputTaxPerMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputTaxPerMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputTaxPerMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
