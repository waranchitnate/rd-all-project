import { Component, OnInit } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { VatFilingPp36Model } from '../../models/vat-filing-pp36.model';
import { ActivatedRoute, Router } from '@angular/router';
import { EPaymentService } from '../../services/e-payment.service';

@Component({
  selector: 'app-output-tax-per-month',
  templateUrl: './output-tax-per-month.component.html',
  styleUrls: ['./output-tax-per-month.component.css']
})
export class OutputTaxPerMonthComponent implements OnInit {

  pin = this.actRoute.snapshot.queryParamMap.get('pin');
  pageResponse: PageResponse<VatFilingPp36Model> = new PageResponse();

  constructor(private actRoute: ActivatedRoute,
    private ePaymentService: EPaymentService,
    private router: Router) { }

  ngOnInit() {
    this.ePaymentService.getVatDetailByPin(this.pin).subscribe(
      (res: any) => {
        this.pageResponse.content = res.data;
      });
  }

  showTaxInvoice(pin) {
    this.router.navigate(['/dss/tax-invoice'], {
      queryParams: { pin }
    });
  }
}
