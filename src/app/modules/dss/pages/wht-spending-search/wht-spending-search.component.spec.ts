import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtSpendingSearchComponent } from './wht-spending-search.component';

describe('WhtSpendingSearchComponent', () => {
  let component: WhtSpendingSearchComponent;
  let fixture: ComponentFixture<WhtSpendingSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtSpendingSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtSpendingSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
