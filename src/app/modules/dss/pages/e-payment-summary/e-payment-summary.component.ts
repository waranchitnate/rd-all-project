import { Component, OnInit } from '@angular/core';
import { DssService } from '../../services/dss.service';
import { Router,ActivatedRoute } from '@angular/router';
import { EpaymentSearchModel } from '../../models/epayment-search.model';
import { DssConstant } from '../../class/dss-constants';

@Component({
    selector: 'app-e-payment-summary',
    templateUrl: './e-payment-summary.component.html',
    styleUrls: ['./e-payment-summary.component.css']
})
export class EPaymentSummaryComponent implements OnInit {

    groupWHT: any[] = [];
    groupDeduction: any[] = [];
    groupVat: any[] = [];
    groupOther: any[] = [];
    searchState: EpaymentSearchModel;
    monthList: any[] = DssConstant.monthList;
    address: any;
    pin :any;
    branchNo :any;
    constructor(private dssService: DssService, private ar: ActivatedRoute,private router: Router) { 
        
    }

    ngOnInit() {
        this.pin = this.dssService.whtSearchParams.pin;
        this.branchNo = this.dssService.whtSearchParams.branchNo;
        this.getDetail();
    }
    
    async getDetail() {
        this.dssService.getEpaymentSearchState().subscribe(searchState => {
            if (searchState) {
                this.searchState = searchState;
                this.dssService.getWhtDetailByPinAndDate(this.ar.snapshot.params.pin, searchState.taxMonthStart, searchState.taxYearStart, searchState.taxMonthEnd, searchState.taxYearEnd,searchState.taxCode,this.branchNo).subscribe((data) => {
                    let group: any[] = data.data;
                    if (group.length) {
                        this.groupWHT = [...group.filter((target) => target.groupPnd == 'WHT')];
                        this.groupDeduction = [...group.filter((target) => target.groupPnd == 'DEDUCTION')];
                        this.groupVat = [...group.filter((target) => target.groupPnd == 'VAT_PP36')];
                        this.groupOther = [...group.filter((target) => target.groupPnd == 'OTHER')];
                    }
                });

                this.dssService.getWhtAddressDetail(this.pin,this.branchNo).subscribe(data => {
                    this.address = data.data;
                })
            }
        });
    }

    sum(group: any[], key: string) {
        return group.reduce((a, b) => a + (b[key] || 0), 0);
    }

    getMonthName(key: any) {
        return this.monthList.find((obj) => obj.value == Number(key) - 1).name;
    }
    
    toDetail(groupPnd: string,pndType:string) {
        this.dssService.whtSearchParams = Object.assign(this.dssService.whtSearchParams, {
            pndGroup: groupPnd,
            pndType: pndType
        });
        this.router.navigate(['/dss/wht/search/summary', this.pin, 'monthly', groupPnd, pndType]);
    }
}
