import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPaymentSummaryComponent } from './e-payment-summary.component';

describe('EPaymentSummaryComponent', () => {
  let component: EPaymentSummaryComponent;
  let fixture: ComponentFixture<EPaymentSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPaymentSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPaymentSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
