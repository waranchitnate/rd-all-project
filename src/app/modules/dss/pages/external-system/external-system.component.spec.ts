import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalSystemComponent } from './external-system.component';

describe('ExternalSystemComponent', () => {
  let component: ExternalSystemComponent;
  let fixture: ComponentFixture<ExternalSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
