import { Component, OnInit } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { PageRequest } from '../../../../shared/models/page-request';
import { PageResponse } from '../../../../shared/models/page-response';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-external-system',
  templateUrl: './external-system.component.html',
  styleUrls: ['./external-system.component.css']
})
export class ExternalSystemComponent implements OnInit {
  modalRef: BsModalRef;
  minDate = new Date();
  maxDate = new Date();
  bsConfig: Partial<BsDatepickerConfig>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  LIMITS = [
    {key: '10', value: 10},
    {key: '25', value: 25},
    {key: '50', value: 50},
    {key: '100', value: 100}
  ];

  limit: number = this.LIMITS[0].value;

  constructor(private modalService: BsModalService) { 
    this.minDate = new Date();
    this.maxDate = new Date();
    this.minDate.setDate(this.minDate.getDate());
    this.maxDate.setDate(this.maxDate.getDate());
    this.bsConfig = Object.assign({}, { containerClass: 'theme-orange' });
  }
  ngOnInit() {
  }
  sort(fieldName) {
  }
  pageChange(event: any, limit) {
  }

  showPopup(reqId: number, template: any) {
    this.modalRef = this.modalService.show(template);

  }
  clear() {}

}
