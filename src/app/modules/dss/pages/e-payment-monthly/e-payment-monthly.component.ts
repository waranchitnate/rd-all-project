import { Component, OnInit } from '@angular/core';
import { EpaymentSearchModel } from '../../models/epayment-search.model';
import { DssConstant } from '../../class/dss-constants';
import { DssService } from '../../services/dss.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TransactionSearchService } from '../../services/transaction-search.service';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { TransactionSearchModel } from '../../models/transaction-search.model';
import { DssDeducationService } from '../../services/dss-deducation.service';
import { DssVatService } from '../../services/dss-vat.service';
import { DssOtherService } from '../../services/dss-other.service';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { any } from 'codelyzer/util/function';

@Component({
    selector: 'app-e-payment-monthly',
    templateUrl: './e-payment-monthly.component.html',
    styleUrls: ['./e-payment-monthly.component.css']
})
export class EPaymentMonthlyComponent implements OnInit {

    searchState: EpaymentSearchModel;
    monthList: any[] = DssConstant.monthList;
    pndTypeList: any[] = DssConstant.pndTypeList;
    groupPndList: any[] = DssConstant.grouppndList;
    address: any;
    pin: any;
    branchNo:any;
    pndGroup: any;
    pndType: any;
    monthsPnd: any[] = [];

    constructor(private dssService: DssService, private router: Router
        , private transactionService: TransactionSearchService, private dssDeducationService: DssDeducationService, private dssVatService: DssVatService, private bs: BreadcrumbService
        , private dssOtherService: DssOtherService) {

    }

    ngOnInit() {
        this.pin = this.dssService.whtSearchParams.pin;
        this.branchNo = this.dssService.whtSearchParams.branchNo;
        this.pndGroup = this.dssService.whtSearchParams.pndGroup;
        this.pndType = this.dssService.whtSearchParams.pndType;
        this.getDetail();

        this.bs.addBreadcrumbLabel({
            key: 'whtMonthly',
            params: [{ key: 'pndType',value: this.getPndTypeName(this.pndType) },{key: 'titleName',value: this.getGroupPndName(this.pndGroup)}]
        });
    }

    async getDetail() {
        this.dssService.getEpaymentSearchState().subscribe(searchState => {
            if (searchState) {
                this.searchState = searchState;
                this.dssService.getWhtMonthly(this.pin, this.pndGroup, searchState.taxMonthStart, searchState.taxYearStart, searchState.taxMonthEnd,
                    searchState.taxYearEnd, this.pndType,this.branchNo).subscribe((data) => {
                        this.monthsPnd = data.data;
                    })
                this.dssService.getWhtAddressDetail(this.pin, this.branchNo).subscribe(data => {
                    this.address = data.data;
                })
            }
        });
    }
    sum(group: any[], key: string) {
        return group.reduce((a, b) => a + (b[key] || 0), 0);
    }
    
    getMonthName(key: any) {
        return (this.monthList.find((obj) => obj.value == Number(key) - 1) || {}).name;
    }

    getGroupPndName(key: any) {
        return (this.groupPndList.find((obj) => obj.value == key) || {}).name;
    }

    getPndTypeName(key: any) {
        return (this.pndTypeList.find((obj) => obj.value == key) || {}).name;
    }

    routDetail(taxMonth: string, taxYear: string) {
        let searchModel: TransactionSearchModel = {
            taxMonth: taxMonth,
            taxYear: taxYear,
            pin: this.pin,
            pndType: this.pndType
        }
        if (this.pndGroup == 'WHT') {
            this.transactionService.filingSearch(searchModel, DataTableUtils.getPageable(),this.branchNo).subscribe((data) => {
                if (data.data.recordsTotal) {
                    this.transactionService.setTransactionSearchState(searchModel);
                }
            });

            this.transactionService.transactionSearch(searchModel, DataTableUtils.getPageable(),this.branchNo).subscribe((data) => {
                if (data.data.recordsTotal) {
                    this.transactionService.setTransactionSearchState(searchModel);
                }
            });

        } else if (this.pndGroup == 'DEDUCTION') {
            this.dssDeducationService.filingSearch(searchModel, DataTableUtils.getPageable(),this.branchNo).subscribe((data) => {
                if (data.data.recordsTotal) {
                    this.dssDeducationService.setTransactionSearchState(searchModel);
                }
            });

            this.dssDeducationService.transactionSearch(searchModel, DataTableUtils.getPageable(),this.branchNo).subscribe((data) => {
                if (data.data.recordsTotal) {
                    this.dssDeducationService.setTransactionSearchState(searchModel);
                }
            });
        } else if (this.pndGroup == 'VAT_PP36') {
            this.dssVatService.transactionSearch(searchModel, DataTableUtils.getPageable(),this.branchNo).subscribe((data) => {
                if (data.data.recordsTotal) {
                    this.dssVatService.setTransactionSearchState(searchModel);
                }
            });
        }else{
          this.dssOtherService.transactionSearch(searchModel, DataTableUtils.getPageable(),this.branchNo).subscribe((data) => {
           if (data.data.recordsTotal) {
             this.dssOtherService.setTransactionSearchState(searchModel);
           }
         });
       }
        this.router.navigate(['/dss/wht/search/summary', this.pin, 'monthly', this.pndGroup, this.pndType, 'per-month', taxMonth, taxYear]);
    }


}
