import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPaymentMonthlyComponent } from './e-payment-monthly.component';

describe('EPaymentMonthlyComponent', () => {
  let component: EPaymentMonthlyComponent;
  let fixture: ComponentFixture<EPaymentMonthlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPaymentMonthlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPaymentMonthlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
