import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { PageResponse } from 'src/app/shared/models/page-response';
import { EPaymentService } from "src/app/modules/dss/services/e-payment.service";
import { VatFilingPp36Model } from '../../models/vat-filing-pp36.model';

@Component({
    selector: 'app-summary-per-taxid',
    templateUrl: './summary-per-taxid.component.html',
    styleUrls: ['./summary-per-taxid.component.css']
})
export class SummaryPerTaxidComponent implements OnInit {
    pin = this.actRoute.snapshot.queryParamMap.get("pin");
    pageResponse: PageResponse<VatFilingPp36Model> = new PageResponse();
    constructor(private actRoute: ActivatedRoute,
        private ePaymentService: EPaymentService,
        private router: Router) { }

    ngOnInit() {
        this.ePaymentService.getVatDetailByPin(this.pin).subscribe(
            (res: any) => {
                console.log(res.data);
                this.pageResponse.content = res.data;
            });
    }

    showDetail(pin) {
        this.router.navigate(["/dss/vat-pp36-months"], {
            queryParams: { pin }
        });
    }
    
    gotoPnd01() {
        this.router.navigate(["/dss/wht-pnd1-by-month"]);
    }

    gotoWhtPnd53() {
        this.router.navigate(["/dss/wht-pnd53-by-month"], {
            queryParams: {}
        });
    }
    
    gotoOhterDetail53(pin) {
        this.router.navigate(["/dss/pnd3654-by-month"], {
            queryParams: { pin }
        });
    }
}
