import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryPerTaxidComponent } from './summary-per-taxid.component';

describe('SummaryPerTaxidComponent', () => {
  let component: SummaryPerTaxidComponent;
  let fixture: ComponentFixture<SummaryPerTaxidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryPerTaxidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryPerTaxidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
