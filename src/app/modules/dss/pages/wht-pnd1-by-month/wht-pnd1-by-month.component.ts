import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wht-pnd1-by-month',
  templateUrl: './wht-pnd1-by-month.component.html',
  styleUrls: ['./wht-pnd1-by-month.component.css']
})
export class WhtPnd1ByMonthComponent implements OnInit {

    pageResponse: any;

    constructor(private router: Router) { }

    ngOnInit() {
    }

    showDetail() {
        this.router.navigate(['/dss/wht-pnd1-per-month']);
    }

}
