import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtPnd1ByMonthComponent } from './wht-pnd1-by-month.component';

describe('WhtPnd1ByMonthComponent', () => {
  let component: WhtPnd1ByMonthComponent;
  let fixture: ComponentFixture<WhtPnd1ByMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtPnd1ByMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtPnd1ByMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
