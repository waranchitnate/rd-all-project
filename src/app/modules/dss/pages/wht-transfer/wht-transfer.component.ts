import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DssService } from '../../services/dss.service';
@Component({
  selector: 'app-wht-transfer',
  templateUrl: './wht-transfer.component.html',
  styleUrls: ['./wht-transfer.component.css']
})
export class WhtTransferComponent implements OnInit {

  constructor(private router: Router,private dssService: DssService) { }

  ngOnInit() {
  }

  routDetail(){
    this.router.navigate(["dss/wht-transfer-per-month"]);
  }

}
