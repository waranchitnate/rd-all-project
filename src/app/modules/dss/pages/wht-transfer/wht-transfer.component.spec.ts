import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtTransferComponent } from './wht-transfer.component';

describe('WhtTransferComponent', () => {
  let component: WhtTransferComponent;
  let fixture: ComponentFixture<WhtTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
