import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pnd3654ByMonthComponent } from './pnd3654-by-month.component';

describe('Pnd3654ByMonthComponent', () => {
  let component: Pnd3654ByMonthComponent;
  let fixture: ComponentFixture<Pnd3654ByMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pnd3654ByMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pnd3654ByMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
