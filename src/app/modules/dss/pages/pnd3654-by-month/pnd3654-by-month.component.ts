import { Component, OnInit } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { ActivatedRoute, Router } from '@angular/router';
import { EPaymentService } from '../../services/e-payment.service';
import { VatFilingPp36Model } from '../../models/vat-filing-pp36.model';

@Component({
  selector: 'app-pnd3654-by-month',
  templateUrl: './pnd3654-by-month.component.html',
  styleUrls: ['./pnd3654-by-month.component.css']
})
export class Pnd3654ByMonthComponent implements OnInit {

  pin = this.actRoute.snapshot.queryParamMap.get('pin');
  pageResponse: PageResponse<VatFilingPp36Model> = new PageResponse();

  constructor(private actRoute: ActivatedRoute,
    private ePaymentService: EPaymentService,
    private router: Router) { }

  ngOnInit() {
    this.ePaymentService.getVatDetailByPin(this.pin).subscribe(
      (res: any) => {
        this.pageResponse.content = res.data;
      });
  }

  showMonthDetail(pin) {
    this.router.navigate(['/dss/pnd3654-per-month'], {
      queryParams: { pin }
    });
  }
}
