import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtTransferPerMonthComponent } from './wht-transfer-per-month.component';

describe('WhtTransferPerMonthComponent', () => {
  let component: WhtTransferPerMonthComponent;
  let fixture: ComponentFixture<WhtTransferPerMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtTransferPerMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtTransferPerMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
