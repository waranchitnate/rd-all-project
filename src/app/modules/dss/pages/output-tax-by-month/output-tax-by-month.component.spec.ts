import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputTaxByMonthComponent } from './output-tax-by-month.component';

describe('OutputTaxByMonthComponent', () => {
  let component: OutputTaxByMonthComponent;
  let fixture: ComponentFixture<OutputTaxByMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputTaxByMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputTaxByMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
