import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VatPp36MonthsComponent } from './vat-pp36-months.component';

describe('VatPp36MonthsComponent', () => {
  let component: VatPp36MonthsComponent;
  let fixture: ComponentFixture<VatPp36MonthsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VatPp36MonthsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VatPp36MonthsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
