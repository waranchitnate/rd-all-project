import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import {PageResponse} from 'src/app/shared/models/page-response';
import {EPaymentService} from 'src/app/modules/dss/services/e-payment.service';
import {VatFilingPp36Model} from '../../models/vat-filing-pp36.model';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-vat-pp36-months',
  templateUrl: './vat-pp36-months.component.html',
  styleUrls: ['./vat-pp36-months.component.css']
})
export class VatPp36MonthsComponent implements OnInit {
  taxYearmonth: Date;
  pin = this.actRoute.snapshot.queryParamMap.get('pin');
  pageResponse: PageResponse<VatFilingPp36Model> = new PageResponse();
  
  constructor(private bsModalService: BsModalService,
    private actRoute: ActivatedRoute,
    private ePaymentService: EPaymentService,
    private modalService: NgbModal,
    private router: Router) { }

  ngOnInit() {
    
  }

  showDetail(pin){ 
    this.router.navigate(["/dss/vat-pp36-by-month"], {
      queryParams: { pin }
    });
  }
}
