import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EPaymentPerMonthComponent } from './e-payment-per-month.component';

describe('EPaymentPerMonthComponent', () => {
  let component: EPaymentPerMonthComponent;
  let fixture: ComponentFixture<EPaymentPerMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EPaymentPerMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EPaymentPerMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
