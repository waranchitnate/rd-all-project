import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TransactionSearchService } from '../../services/transaction-search.service';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { DataTablePageable } from 'src/app/shared/modals/data-table/data-table-pageable';
import { TransactionSearchModel } from '../../models/transaction-search.model';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { DssDeducationService } from '../../services/dss-deducation.service';
import { DssVatService } from '../../services/dss-vat.service';
import { ThaiMonthPipe } from 'src/app/shared/pipe/thai-month.pipe';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { DssService } from '../../services/dss.service';
import { DssConstant } from '../../class/dss-constants';
import { DssOtherService } from '../../services/dss-other.service';
import * as printJS from 'print-js';

@Component({
    selector: 'app-e-payment-per-month',
    templateUrl: './e-payment-per-month.component.html',
    styleUrls: ['./e-payment-per-month.component.css']
})
export class EPaymentPerMonthComponent implements OnInit {

    radio: string = '1';
    pageResponse: any;
    pageable: DataTablePageable = DataTableUtils.getPageable();
    pageableF: DataTablePageable = DataTableUtils.getPageable();
    dataFilingSet: any[] = [];
    dataSet: any[] = [];
    searchState: TransactionSearchModel;
    pin: any;
    branchNo: any;
    pndGroup: any;
    pndType: any;
    taxMonth: any;
    taxYear: any;
    urlPdf: string = environment.dssApiUrl + 'pnd/form/download?filingId=254';
    monthList: any[] = DssConstant.monthList;
    pndTypeList: any[] = DssConstant.spendingList;
    address: any;
    countTransactionByPndType: any;
    countTransaction: any;

  constructor(private modalService: NgbModal, private ar: ActivatedRoute, private transactionService: TransactionSearchService,
    private dssDeducationService: DssDeducationService, private dssVatService: DssVatService
    , private dssOtherService: DssOtherService, private bs: BreadcrumbService, private dssService: DssService) { }

    ngOnInit() {
        this.pin = this.dssService.whtSearchParams.pin;
        this.branchNo = this.dssService.whtSearchParams.branchNo;
        this.pndGroup = this.dssService.whtSearchParams.pndGroup;
        this.pndType = this.dssService.whtSearchParams.pndType;
        this.taxMonth = this.ar.snapshot.params.taxMonth;
        this.taxYear = this.ar.snapshot.params.taxYear;
        this.dssService.getWhtAddressDetail(this.pin,this.branchNo).subscribe(data => {
            this.address = data.data;
        });

        if (this.pndGroup == 'WHT') {
            this.transactionService.getTransactionSearchState().subscribe(data => {
                if (data) {
                    this.searchState = data;
                    this.searchFilingData();
                    this.searchData();
                    this.transactionService.getCountWhtTransactionByPndType(data,this.branchNo).subscribe(data => {
                        this.countTransactionByPndType = data.data;
                    });

                    this.transactionService.getCountWhtTransaction(data,this.branchNo).subscribe(data => {
                        this.countTransaction = data.data;
                    });
                }
            });
        } else if (this.pndGroup == 'DEDUCTION') {
            this.dssDeducationService.getTransactionSearchState().subscribe(data => {
                if (data) {
                    this.searchState = data;
                    this.searchFilingData();
                    this.searchData();

                    this.transactionService.getCountDeductionByPndType(data,this.branchNo).subscribe(data => {
                        this.countTransactionByPndType = data.data;
                    });

                    this.transactionService.getCountDeductionTransaction(data,this.branchNo).subscribe(data => {
                        this.countTransaction = data.data;
                    });

                   
                }
            });
        } else if (this.pndGroup == 'VAT_PP36') {
            this.dssVatService.getTransactionSearchState().subscribe(data => {
                if (data) {
                    this.searchState = data;
                    this.searchData();
                    this.transactionService.getCountVatTransaction(data,this.branchNo).subscribe(data => {
                        this.countTransaction = data.data;
                    });                  
                }
            });
        }else{
          this.dssOtherService.getTransactionSearchState().subscribe(data => {
            if (data) {
                this.searchState = data;
                this.searchData();

                this.transactionService.getCountOtherTransaction(data,this.branchNo).subscribe(data => {
                    this.countTransaction = data.data;
                });
            }
          });
        }

        this.bs.addBreadcrumbLabel({
            key: 'whtPerMonth',
            params: [{ key: 'monthName', value: this.monthList.find((obj) => obj.value == Number(this.taxMonth) - 1).name, },{ key: 'year', value: this.taxYear}]
        })

    }

    searchData(reset?: boolean) {
        if (reset) this.pageable.pageIndex = 1;
        this.pageable.loading = true;
        if (this.pndGroup == 'WHT') {
            this.transactionService.transactionSearch(this.searchState, this.pageable,this.branchNo).pipe(
                finalize(() => this.pageable.loading = false)
            ).subscribe(data => {
                this.dataSet = data.data.data;
                this.pageable.total = data.data.recordsTotal;
            });
        } else if (this.pndGroup == 'DEDUCTION') {
            this.dssDeducationService.transactionSearch(this.searchState, this.pageable,this.branchNo).pipe(
                finalize(() => this.pageable.loading = false)
            ).subscribe(data => {
                this.dataSet = data.data.data;
                this.pageable.total = data.data.recordsTotal;
            });
        } else if (this.pndGroup == 'VAT_PP36') {
            this.dssVatService.transactionSearch(this.searchState, this.pageable,this.branchNo).pipe(
                finalize(() => this.pageable.loading = false)
            ).subscribe(data => {
                this.dataSet = data.data.data;
                this.pageable.total = data.data.recordsTotal;
            });
        }else{
          this.dssOtherService.transactionSearch(this.searchState, this.pageable,this.branchNo).pipe(
            finalize(() => this.pageable.loading = false)
          ).subscribe(data => {
              this.dataSet = data.data.data;
              this.pageable.total = data.data.recordsTotal;
          });
    
        }

    }

    searchFilingData(reset?: boolean) {
        if (reset) this.pageableF.pageIndex = 1;
        this.pageableF.loading = true;
        if (this.pndGroup == 'WHT') {
            this.transactionService.filingSearch(this.searchState, this.pageableF,this.branchNo).pipe(
                finalize(() => this.pageableF.loading = false)
            ).subscribe(data => {
                this.dataFilingSet = data.data.data;
                this.pageableF.total = data.data.recordsTotal;
            });
        } else if (this.pndGroup == 'DEDUCTION') {
            this.dssDeducationService.filingSearch(this.searchState, this.pageableF,this.branchNo).pipe(
                finalize(() => this.pageableF.loading = false)
            ).subscribe(data => {
                this.dataFilingSet = data.data.data;
                this.pageableF.total = data.data.recordsTotal;
            });
        }

    }

    sum(group: any[], key: string) {
        return group.reduce((a, b) => a + (b[key] || 0), 0);
    }
    getMonthName(key: any) {
        return this.monthList.find((obj) => obj.value == Number(key) - 1).name;
    }
    getPndTypeName(key: any) {
        return this.pndTypeList.find((obj) => obj.value == key).name;
    }
    

    openFilingForm(content, filingId: any) {
        if (this.pndGroup == 'WHT') {
            this.urlPdf = environment.dssApiUrl + `pnd/form/download?filingId=` + filingId+'&pndType='+ this.pndType;
        } else if (this.pndGroup == 'DEDUCTION') {
            this.urlPdf = environment.dssApiUrl + `pnd/form/download-deduction?filingDetailId=` + filingId+'&pndType='+ this.pndType;
        } else if(this.pndGroup == 'VAT_PP36'){
            this.urlPdf = environment.dssApiUrl + `dssVatTransactionController/download-pnd?filingId=` + filingId;
        } else if(this.pndGroup == 'OTHER'){
            this.urlPdf = environment.dssApiUrl + `dssVatTransactionController/download-pnd?filingId=` + filingId;
        }
        this.modalService.open(content, {
            ariaLabelledBy: 'modal-basic-title',
            size: <any>'xl',
            backdrop: 'static',
            windowClass: 'modal-xxl'
        });
    }

    openTransactionForm(content, filingDetailId:any) {
      this.urlPdf = environment.dssApiUrl + `transaction/form/download?filingDetailId=` + filingDetailId+'&pndType='+ this.pndType;
        this.modalService.open(content, {
            ariaLabelledBy: 'modal-basic-title',
            backdrop: 'static',
            windowClass: 'modal-xxl'
        });
    }

    openVatTransactionForm(content, filingId:any) {
        this.urlPdf = environment.dssApiUrl + `dssVatTransactionController/download-transaction?filingId=` + filingId;
          this.modalService.open(content, {
              ariaLabelledBy: 'modal-basic-title',
              backdrop: 'static',
              windowClass: 'modal-xxl'
          });
    }
    openPnd54InOtherType(content, filingId: any){
        this.urlPdf = environment.dssApiUrl + `pnd/form/download-deduction?filingDetailId=` + filingId+'&pndType=PND54';
          this.modalService.open(content, {
              ariaLabelledBy: 'modal-basic-title',
              size: 'lg',
              backdrop: 'static',
              windowClass: 'modal-xxl'
          });
    }
    openPnd54InOtherTransactionType(content, filingDetailId: any){
        this.urlPdf = environment.dssApiUrl + `dssOtherTransactionController/download-transaction?filingDetailId=` + filingDetailId;
          this.modalService.open(content, {
              ariaLabelledBy: 'modal-basic-title',
              size: 'lg',
              backdrop: 'static',
              windowClass: 'modal-xxl'
          });
    }


    print() {
      printJS({printable: this.urlPdf});
    }

}
