import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtPnd53ByMonthComponent } from './wht-pnd53-by-month.component';

describe('WhtPnd53ByMonthComponent', () => {
  let component: WhtPnd53ByMonthComponent;
  let fixture: ComponentFixture<WhtPnd53ByMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtPnd53ByMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtPnd53ByMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
