import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-wht-pnd53-by-month',
  templateUrl: './wht-pnd53-by-month.component.html',
  styleUrls: ['./wht-pnd53-by-month.component.css']
})
export class WhtPnd53ByMonthComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  routDetail(){
    this.router.navigate(["dss/wht-pnd53-per-month"]);
  }

}
