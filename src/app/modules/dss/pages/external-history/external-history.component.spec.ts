import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalHistoryComponent } from './external-history.component';

describe('ExternalHistoryComponent', () => {
  let component: ExternalHistoryComponent;
  let fixture: ComponentFixture<ExternalHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
