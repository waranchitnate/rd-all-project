import { Component, OnInit } from '@angular/core';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { BsModalService } from 'ngx-bootstrap';
import { environment } from 'src/environments/environment';
import {Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(private bsModalService: BsModalService, private router: Router ) {
    console.log(environment.pkiFlag);
    if ( environment.pkiFlag === false ) {
      this.router.navigate(['/dss/certverify']);
    }
   }

  ngOnInit() {
    // const modalRef = this.bsModalService.show(AnswerModalComponent);
    // (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
    // (modalRef.content as AnswerModalComponent).content = 'คุณต้องไม่อนุมัติผู้ใช้งานใช่หรือไม่';
    // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
    //   sub.unsubscribe();
    // });
  }

}
