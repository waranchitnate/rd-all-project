import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as printJS from 'print-js';

@Component({
  selector: 'app-preview-pdf',
  templateUrl: './preview-pdf.component.html',
  styleUrls: ['./preview-pdf.component.css']
})
export class PreviewPdfComponent implements OnInit {
  @ViewChild('content') content: ElementRef;
  
  constructor(private modalService: NgbModal) { }
  url: any;
  ngOnInit() {
  }
  preview(url : any){
    this.url = url;
    console.log(this.url);
    this.modalService.open(this.content, {
        ariaLabelledBy: 'modal-basic-title',
        size: <any>'xl',
        windowClass: 'dark-modal'
    });
  }

  print() {
    //this.modalService.open();
    printJS({printable: this.url});
  }
}
