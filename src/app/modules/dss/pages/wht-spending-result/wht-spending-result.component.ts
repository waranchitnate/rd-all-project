import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DssService } from '../../services/dss.service';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { DataTablePageable } from 'src/app/shared/modals/data-table/data-table-pageable';
import { EpaymentSearchModel } from '../../models/epayment-search.model';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-wht-spending-result',
  templateUrl: './wht-spending-result.component.html',
  styleUrls: ['./wht-spending-result.component.css']
})
export class WhtSpendingResultComponent implements OnInit {

  pageable: DataTablePageable = DataTableUtils.getPageable();
  dataSet: any[] = [];
  searchState: EpaymentSearchModel;

  constructor(private dssService: DssService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.dssService.getEpaymentSearchState().subscribe(data => {
      if (data) {
          this.searchState = data;
          this.searchData();
      }
    });
  }

  searchData(reset?: boolean) {
    if (reset) this.pageable.pageIndex = 1;
    this.pageable.loading = true;
    this.dssService.whtSpendingSearch(this.searchState, this.pageable).pipe(
        finalize(() => this.pageable.loading = false)
    ).subscribe(data => {
        this.dataSet = data.data.data;
        this.pageable.total = data.data.recordsTotal;
    });
}

toDetail(pin: string,branchNo: string) {
    this.dssService.whtSearchParams = {
        pin: pin,
        branchNo:branchNo
    };
    this.router.navigate(['/dss/wht/spending-search/spending-summary', pin]);
}

}
