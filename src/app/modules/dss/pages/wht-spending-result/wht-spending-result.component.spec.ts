import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtSpendingResultComponent } from './wht-spending-result.component';

describe('WhtSpendingResultComponent', () => {
  let component: WhtSpendingResultComponent;
  let fixture: ComponentFixture<WhtSpendingResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtSpendingResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtSpendingResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
