import { Component, OnInit } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { VatFilingPp36Model } from '../../models/vat-filing-pp36.model';
import { EPaymentService } from '../../services/e-payment.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-etax-summary-per-taxid',
  templateUrl: './etax-summary-per-taxid.component.html',
  styleUrls: ['./etax-summary-per-taxid.component.css']
})
export class EtaxSummaryPerTaxidComponent implements OnInit {

  pin = this.actRoute.snapshot.queryParamMap.get('pin');
  pageResponse: PageResponse<VatFilingPp36Model> = new PageResponse();

  constructor(private actRoute: ActivatedRoute,
    private ePaymentService: EPaymentService,
    private router: Router) { }

  ngOnInit() {
    this.ePaymentService.getVatDetailByPin(this.pin).subscribe(
      (res: any) => {
        this.pageResponse.content = res.data;
      });
  }

  viewOutputTax(pin) {
    this.router.navigate(['/dss/output-tax-by-month'], {
      queryParams: { pin }
    });
  }
}
