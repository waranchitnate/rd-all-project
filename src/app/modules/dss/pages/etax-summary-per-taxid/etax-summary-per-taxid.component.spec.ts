import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtaxSummaryPerTaxidComponent } from './etax-summary-per-taxid.component';

describe('EtaxSummaryPerTaxidComponent', () => {
  let component: EtaxSummaryPerTaxidComponent;
  let fixture: ComponentFixture<EtaxSummaryPerTaxidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtaxSummaryPerTaxidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtaxSummaryPerTaxidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
