import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { DataTablePageable } from 'src/app/shared/modals/data-table/data-table-pageable';
import { TransactionSearchService } from '../../services/transaction-search.service';
import { TransactionSearchModel } from '../../models/transaction-search.model';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ThaiMonthPipe } from 'src/app/shared/pipe/thai-month.pipe';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { DssService } from '../../services/dss.service';
import { DssConstant } from '../../class/dss-constants';
import { SpendingService } from '../../services/spending.service';
import * as printJS from 'print-js';

@Component({
  selector: 'app-wht-transferee-per-month',
  templateUrl: './wht-transferee-per-month.component.html',
  styleUrls: ['./wht-transferee-per-month.component.css']
})
export class WhtTransfereePerMonthComponent implements OnInit {

  pageResponse: any;
  pageable: DataTablePageable = DataTableUtils.getPageable();
  pageableF: DataTablePageable = DataTableUtils.getPageable();
  dataSet: any[] = [];
  searchState: TransactionSearchModel;
  pin: any;
  branchNo: any;
  pndGroup: any;
  typeSpending: any;
  taxMonth: any;
  taxYear: any;
  urlPdf: string = environment.dssApiUrl + 'pnd/form/download?filingId=254';
  monthList: any[] = DssConstant.monthList;
  address: any;
  countExpenseTransaction: any;

  constructor(private modalService: NgbModal, private ar: ActivatedRoute, private spendingService: SpendingService,
     private bs: BreadcrumbService, private dssService: DssService, private transactionService: TransactionSearchService) { }

  ngOnInit() {
    this.pin = this.dssService.whtSearchParams.pin;
    this.branchNo = this.dssService.whtSearchParams.branchNo;
    this.typeSpending = this.dssService.whtSearchParams.typeSpending;
    this.taxMonth = this.ar.snapshot.params.taxMonth;
    this.taxYear = Number(this.ar.snapshot.params.taxYear) + 543;
    this.getDetail();
    this.bs.addBreadcrumbLabel({
      key: 'whtSpendingPerMonth',
      params: [{ key: 'monthName', value: this.monthList.find((obj) => obj.value == Number(this.taxMonth) - 1).name, },{ key: 'year', value: this.taxYear}]
    })
    
  }

  async getDetail() {
    this.transactionService.getTransactionSearchState().subscribe(data => {
      if (data) {
          this.searchState = data;
          this.searchData();
          this.spendingService.getCountExpenseTransaction(data,this.branchNo).subscribe(data => {
            this.countExpenseTransaction = data.data;
          });
          this.dssService.getExpenseAddress(this.pin,this.branchNo).subscribe(data => {
            this.address = data.data;
          });
      }
    });
  }

  searchData(reset?: boolean) {
    if (reset) this.pageable.pageIndex = 1;
    this.pageable.loading = true;
    this.spendingService.getExpenseTransaction(this.searchState, this.pageable,this.branchNo).pipe(
      finalize(() => this.pageable.loading = false)
    ).subscribe(data => {
        this.dataSet = data.data.data;
        this.pageable.total = data.data.recordsTotal;
    });

  }

  open(content, id:any, seq:any, processingDetailId:any) {
    this.urlPdf = environment.dssApiUrl + `expense-transaction/form/download?id=` + id +'&seq='+ seq +'&processingDetailId='+ processingDetailId + '&typeSpending='+ this.typeSpending;
        this.modalService.open(content, {
            ariaLabelledBy: 'modal-basic-title',
            size: 'lg',
            backdrop: 'static',
            windowClass: 'modal-xxl'
        });
  }

  print() {
    printJS({printable: this.urlPdf});
  }

  openPnd54(contentPnd54) {
    this.modalService.open(contentPnd54, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static',
      windowClass: 'modal-xxl'
    }).result.then((result) => {
      //this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openPp36(contentPp36) {
    this.modalService.open(contentPp36, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static',
      windowClass: 'modal-xxl'
    }).result.then((result) => {
      //this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

}
