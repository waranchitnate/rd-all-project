import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhtTransfereePerMonthComponent } from './wht-transferee-per-month.component';

describe('WhtTransfereePerMonthComponent', () => {
  let component: WhtTransfereePerMonthComponent;
  let fixture: ComponentFixture<WhtTransfereePerMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhtTransfereePerMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhtTransfereePerMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
