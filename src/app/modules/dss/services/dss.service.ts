import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DataTablePageable } from 'src/app/shared/modals/data-table/data-table-pageable';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { ResponseT } from 'src/app/shared/models/response.model';
import { DataTableResult } from 'src/app/shared/modals/data-table/data-table-result';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { EpaymentSearchModel } from '../models/epayment-search.model';
import { WHTSearchParams } from '../models/wht-search-param.model';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { SpecialSearchRequest } from "../models/special-search-request.model";

const EPAYMENT_SEARCH_KEY = 'epayment-search-state';
const WHT_SEARCH_KEY = 'wht-search-param';

@Injectable({
    providedIn: 'root'
})
export class DssService {

    private _searchUrl: string = environment.dssApiUrl + 'dssWhtMainController/search';
    private _pndListUrl: string = environment.dssApiUrl + 'dssWhtMainController/getPndtype';
    private _detailWhtUrl: string = environment.dssApiUrl + 'dssWhtMainController/getWhtMonthlyAllPndType';
    private _addressWhtUrl: string = environment.dssApiUrl + 'dssWhtMainController/getTaxpayerAddressDetail';
    private _getExpenseAddress: string = environment.dssApiUrl + 'dssExpenseController/getExpenseAddress'
    private _specialSearchUrl: string = environment.dssApiUrl + 'specialTransController/search';
    private _whtMonthlyUrl:string = environment.dssApiUrl + 'dssWhtMainController/getWhtMonthly';
    private _spendingSearchUrl: string = environment.dssApiUrl + 'dssExpenseController/search';
    private _getExpenseSummaryUrl: string = environment.dssApiUrl + 'dssExpenseController/getExpenseSummary';
    private _getExpenseMonthlyUrl: string = environment.dssApiUrl + 'dssExpenseController/getExpenseMonthly';
    private _epaymentSearchState: BehaviorSubject<EpaymentSearchModel> = new BehaviorSubject<EpaymentSearchModel>(null);
    private _whtSearchParams: WHTSearchParams;

    constructor(private http: HttpClient, @Inject(SESSION_STORAGE) private storage: StorageService) { }

    getEpaymentSearchState() {
        if (this.storage.has(EPAYMENT_SEARCH_KEY) && !this._epaymentSearchState.getValue()) this._epaymentSearchState.next(this.storage.get(EPAYMENT_SEARCH_KEY));
        return this._epaymentSearchState.asObservable();
    }

    setEpaymentSearchState(_state: EpaymentSearchModel) {
        this._epaymentSearchState.next(_state);
        this.storage.set(EPAYMENT_SEARCH_KEY, _state);
    }

    whtSearch(_searchModel: EpaymentSearchModel, _pageable: DataTablePageable) {

        let httpParams: HttpParams = DataTableUtils.generateParams(_pageable);

        if (_searchModel.taxPayerNo) httpParams = httpParams.append('taxPayerPin', _searchModel.taxPayerNo);
        if (_searchModel.taxPayerName) httpParams = httpParams.append('taxPayerName', _searchModel.taxPayerName);
        if (_searchModel.branch == '1') {
            httpParams = httpParams.append('branchNo', '00000');
        } else if (_searchModel.branch == '2') {
            httpParams = httpParams.append('branchNo', _searchModel.branchNo);
        }
        if (_searchModel.taxType) httpParams = httpParams.append('taxType', _searchModel.taxType);
        if (_searchModel.taxCode) httpParams = httpParams.append('taxNo', _searchModel.taxCode);
        if (_searchModel.taxMonthStart) httpParams = httpParams.append('taxMonthStart', _searchModel.taxMonthStart);
        if (_searchModel.taxYearStart) httpParams = httpParams.append('taxYearStart', _searchModel.taxYearStart);
        if (_searchModel.taxMonthEnd) httpParams = httpParams.append('taxMonthEnd', _searchModel.taxMonthEnd);
        if (_searchModel.taxYearEnd) httpParams = httpParams.append('taxYearEnd', _searchModel.taxYearEnd);

        return this.http.get<ResponseT<DataTableResult<any[]>>>(this._searchUrl, {
            params: httpParams
        });
    }

    whtSpendingSearch(_searchModel: EpaymentSearchModel, _pageable: DataTablePageable) {

        let httpParams: HttpParams = DataTableUtils.generateParams(_pageable);

        if (_searchModel.taxPayerNo) httpParams = httpParams.append('pin', _searchModel.taxPayerNo);
        if (_searchModel.taxPayerName) httpParams = httpParams.append('name', _searchModel.taxPayerName);
        if (_searchModel.branch == '1') {
            httpParams = httpParams.append('branchNo', '00000');
        } else if (_searchModel.branch == '2') {
            httpParams = httpParams.append('branchNo', _searchModel.branchNo);
        }
        if (_searchModel.taxType) httpParams = httpParams.append('typeSpending', _searchModel.taxType);
        if (_searchModel.taxMonthStart) httpParams = httpParams.append('taxMonthStart', _searchModel.taxMonthStart);
        if (_searchModel.taxYearStart) httpParams = httpParams.append('taxYearStart', _searchModel.taxYearStart);
        if (_searchModel.taxMonthEnd) httpParams = httpParams.append('taxMonthEnd', _searchModel.taxMonthEnd);
        if (_searchModel.taxYearEnd) httpParams = httpParams.append('taxYearEnd', _searchModel.taxYearEnd);

        return this.http.get<ResponseT<DataTableResult<any[]>>>(this._spendingSearchUrl, {
            params: httpParams
        });
    }

    getWhtDetailByPinAndDate(pin: string, taxMonthStart: string, taxYearStart: string, taxMonthEnd: string, taxYearEnd: string,taxCode:string,branchNo:string) {
        let httpParams: HttpParams = new HttpParams().set('pin', pin).set('taxMonthStart', taxMonthStart).set('taxYearStart', taxYearStart).set('taxMonthEnd', taxMonthEnd).set('taxYearEnd', taxYearEnd).set('branchNo',branchNo)
        .set('taxCode',taxCode);
        return this.http.get<any>(this._detailWhtUrl, {
            params: httpParams 
        });
    }

    getWhtAddressDetail(pin: string,branchNo: string) {
        let httpParams: HttpParams = new HttpParams().set('pin', pin).set('branchNo', branchNo);
        return this.http.get<any>(this._addressWhtUrl, {
            params: httpParams 
        });
    }

    getExpenseAddress(pin: string,branchNo : string) {
        let httpParams: HttpParams = new HttpParams().set('pin', pin).set('branchNo',branchNo);
        return this.http.get<any>(this._getExpenseAddress, {
            params: httpParams 
        });
    }

    getPndType() {
        return this.http.get<ResponseT<any>>(this._pndListUrl);
    }

    specialSearch(_searchModel: SpecialSearchRequest, _pageable: DataTablePageable) {
        let httpParams: HttpParams = DataTableUtils.generateParams(_pageable);

        httpParams = httpParams.append('year', _searchModel.year);
        if (_searchModel.name) httpParams = httpParams.append('name', _searchModel.name);

        return this.http.get<ResponseT<DataTableResult<any[]>>>(this._specialSearchUrl, {
            params: httpParams
        });
    }

    getWhtMonthly(pin: string ,groupPnd: string , taxMonthStart: string, taxYearStart: string, taxMonthEnd: string, taxYearEnd: string,pndType:string,branchNo:string) {
        let httpParams: HttpParams = new HttpParams().set('pin', pin).set('taxMonthStart', taxMonthStart).set('taxYearStart', taxYearStart).set('taxMonthEnd', taxMonthEnd).set('taxYearEnd', taxYearEnd)
        .set('groupPnd',groupPnd).set('pndType',pndType).set("branchNo",branchNo);
        return this.http.get<ResponseT<any>>(this._whtMonthlyUrl,{
            params : httpParams
        });
    }

    getExpenseSummary(pin: string, taxMonthStart: string, taxYearStart: string, taxMonthEnd: string, taxYearEnd: string,branchNo : string) {
        let httpParams: HttpParams = new HttpParams().set('pin', pin).set('taxMonthStart', taxMonthStart).set('taxYearStart', taxYearStart).set('taxMonthEnd', taxMonthEnd).set('taxYearEnd', taxYearEnd).set('branchNo', branchNo);
        return this.http.get<any>(this._getExpenseSummaryUrl, {
            params: httpParams 
        });
    }

    getExpenseMonthly(pin: string, taxMonthStart: string, taxYearStart: string, taxMonthEnd: string, taxYearEnd: string,typeSpending:string,branchNo : string) {
        let httpParams: HttpParams = new HttpParams().set('pin', pin).set('taxMonthStart', taxMonthStart).set('taxYearStart', taxYearStart).set('taxMonthEnd', taxMonthEnd)
        .set('taxYearEnd', taxYearEnd).set('typeSpending',typeSpending).set('branchNo', branchNo);
        return this.http.get<any>(this._getExpenseMonthlyUrl, {
            params: httpParams 
        });
    }

	public get whtSearchParams(): WHTSearchParams {
        if (this.storage.has(WHT_SEARCH_KEY) && !this._whtSearchParams) this._whtSearchParams = this.storage.get(WHT_SEARCH_KEY);
		return this._whtSearchParams;
	}

	public set whtSearchParams(value: WHTSearchParams) {
        this._whtSearchParams = value;
        this.storage.set(WHT_SEARCH_KEY, value);
	}
}
