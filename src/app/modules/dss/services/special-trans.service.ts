import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {HttpParams} from  "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SpecialTransService {

  private _getSpecialTransDetailUrl: string = environment.dssApiUrl + 'specialTransController/getPersonTrans';
  private _getSpecialTransAddressUrl: string = environment.dssApiUrl + 'specialTransController/getPersonAddress';

  constructor(private http: HttpClient) { }

  getSpecialTransDetail(pin?:string , year?:string): Observable<any> {
    const  params = new  HttpParams({fromString:  'pin='+pin +'&year='+year});
		return this.http.post<any>(this._getSpecialTransDetailUrl, params);
  }
  
  getSpecialTransAddress(pin?:string , year?:string): Observable<any> {
    const  params = new  HttpParams({fromString:  'pin='+pin +'&year='+year});
		return this.http.post<any>(this._getSpecialTransAddressUrl, params);
	}
}
