import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DataTablePageable } from 'src/app/shared/modals/data-table/data-table-pageable';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { DataTableResult } from 'src/app/shared/modals/data-table/data-table-result';
import { TransactionSearchModel } from '../models/transaction-search.model';
import { ResponseT } from 'src/app/shared/models/response.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DssOtherService {

  private _searchUrl: string = environment.dssApiUrl + 'dssOtherTransactionController/getOtherTransaction';
  private _transactionSearchState: BehaviorSubject<TransactionSearchModel> = new BehaviorSubject<TransactionSearchModel>(null);

  constructor(private http: HttpClient) { }

  getTransactionSearchState() {
    return this._transactionSearchState.asObservable();
  }

  setTransactionSearchState(_state: TransactionSearchModel) {
      this._transactionSearchState.next(_state);
  }

  transactionSearch(_searchModel: TransactionSearchModel, _pageable: DataTablePageable,branchNo :string) {

    let httpParams: HttpParams = DataTableUtils.generateParams(_pageable);

    if (_searchModel.taxMonth) httpParams = httpParams.append('taxMonth', _searchModel.taxMonth);
    if (_searchModel.taxYear) httpParams = httpParams.append('taxYear', _searchModel.taxYear);
    if (_searchModel.pin) httpParams = httpParams.append('pin', _searchModel.pin);
    if (_searchModel.pndType) httpParams = httpParams.append('pndType', _searchModel.pndType);
    httpParams = httpParams.append('branchNo', branchNo);
    return this.http.get<ResponseT<DataTableResult<any[]>>>(this._searchUrl, {
        params: httpParams
    });
  }
}
