import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MenuItem } from '../models/menu-item.model';

@Injectable({
    providedIn: 'root'
})
export class BreadcrumbService {

    private _breadCrumbLabel: BehaviorSubject<MenuItem[]> = new BehaviorSubject<MenuItem[]>(null);

    constructor() { }

    getBreadcrumbLabel() {
        return this._breadCrumbLabel.asObservable();
    }

    addBreadcrumbLabel(menuItem: MenuItem) {
        let menuItemList = this._breadCrumbLabel.getValue();
        if (menuItemList && menuItemList.length > 0) {
            menuItemList = menuItemList.filter((data) => data.key != menuItem.key);
            this._breadCrumbLabel.next([...menuItemList ,menuItem]);
        } else {
            this._breadCrumbLabel.next([menuItem]);
        }
    }

    resetBreadcrumbLabel() {
        this._breadCrumbLabel.next(null);
    }
}
