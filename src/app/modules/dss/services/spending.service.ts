import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DataTablePageable } from 'src/app/shared/modals/data-table/data-table-pageable';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { DataTableResult } from 'src/app/shared/modals/data-table/data-table-result';
import { TransactionSearchModel } from '../models/transaction-search.model';
import { ResponseT } from 'src/app/shared/models/response.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpendingService {

  private _searchUrl: string = environment.dssApiUrl + 'dssExpenseController/getExpenseTransaction';
  private _getCountExpenseTransactionUrl: string = environment.dssApiUrl + 'dssExpenseController/getCountExpenseTransaction';
  private _transactionSearchState: BehaviorSubject<TransactionSearchModel> = new BehaviorSubject<TransactionSearchModel>(null);

  constructor(private http: HttpClient) { }

  getExpenseTransaction(_searchModel: TransactionSearchModel, _pageable: DataTablePageable,branchNo :string) {

    let httpParams: HttpParams = DataTableUtils.generateParams(_pageable);

    if (_searchModel.taxMonth) httpParams = httpParams.append('taxMonth', _searchModel.taxMonth);
    if (_searchModel.taxYear) httpParams = httpParams.append('taxYear', _searchModel.taxYear);
    if (_searchModel.pin) httpParams = httpParams.append('pin', _searchModel.pin);
    if (_searchModel.pndType) httpParams = httpParams.append('typeSpending', _searchModel.pndType);
    httpParams = httpParams.append('branchNo', branchNo);
    return this.http.get<ResponseT<DataTableResult<any[]>>>(this._searchUrl, {
        params: httpParams
    });
  }

  getCountExpenseTransaction(_searchModel: TransactionSearchModel,branchNo :string) {

    let httpParams: HttpParams = new HttpParams();

    if (_searchModel.taxMonth) httpParams = httpParams.append('taxMonth', _searchModel.taxMonth);
    if (_searchModel.taxYear) httpParams = httpParams.append('taxYear', _searchModel.taxYear);
    if (_searchModel.pin) httpParams = httpParams.append('pin', _searchModel.pin);
    if (_searchModel.pndType) httpParams = httpParams.append('typeSpending', _searchModel.pndType);
    httpParams = httpParams.append('branchNo', branchNo);
    return this.http.get<ResponseT<DataTableResult<any[]>>>(this._getCountExpenseTransactionUrl, {
        params: httpParams
    });
  }
}
