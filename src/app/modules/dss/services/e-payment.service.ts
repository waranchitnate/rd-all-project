import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Vat} from '../models/dss-vat-summary.medel';
import {Observable} from 'rxjs';
import {ResponseT} from '../../../shared/models/response.model';
import {PageResponse} from '../../../shared/models/page-response';
import {VatFilingPp36Model} from '../models/vat-filing-pp36.model';
import {EdcTaxInputTaxModel} from '../models/edc-tax-input-tax.model';

@Injectable({
  providedIn: 'root'
})
export class EPaymentService {
  url = environment.dssApiUrl + 'dssVatController';

  constructor(private http: HttpClient) {
  }

  getSearchCriteria(
    vat: Vat
  ): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>(
      this.http.post(this.url + '/search', vat)
    );
  }

  getVatDetailByPin(
    pin: string
  ): Observable<ResponseT<PageResponse<VatFilingPp36Model>>> {
    return this.http.post<ResponseT<PageResponse<VatFilingPp36Model>>>(this.url + '/getVatDetailById/' + pin, null);
  }

  getBySenderTaxId(senderTaxId: string): Observable<ResponseT<Array<EdcTaxInputTaxModel>>> {
    return this.http.post<ResponseT<Array<EdcTaxInputTaxModel>>>(this.url + '/getBySenderTaxId/' + senderTaxId, null);
  }

}
