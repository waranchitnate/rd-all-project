import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {OauthCredential} from '../../../core/models/oauth-credential';

@Injectable({
  providedIn: 'root'
})
export class DssAuthenticationService {

  private checkTokenUrl = environment.oauthUrl + 'oauth/check_token';
  private accessToken: string;
  constructor(private httpClient: HttpClient, private toastrService: ToastrService) { }

  login(accessToken: string): Observable<any> {
    const body = new HttpParams({
      fromObject: {
        token: accessToken
      }
    });
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        'SkipAuth': '1'
      })
    };
    return this.httpClient.post(this.checkTokenUrl, body, httpOptions).pipe(
      tap(ret => {
        this.updateLocalStorage(accessToken);
        this.accessToken = accessToken;
      }),
      catchError(err => {
        this.accessToken = undefined;
        this.toastrService.error('ไม่พบข้อมูล Token', 'ข้อผิดพลาด');
        return of(err);
      })
    );
  }

  isLogin(): boolean {
    return this.accessToken === undefined;
  }

  updateLocalStorage(accessToken: string) {
    const oauth: OauthCredential = new OauthCredential();
    oauth.access_token = accessToken;
    localStorage.setItem('rd-internal-oauth', JSON.stringify(oauth));
  }
}
