import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DataTablePageable } from 'src/app/shared/modals/data-table/data-table-pageable';
import DataTableUtils from 'src/app/shared/class/data-table-utils';
import { DataTableResult } from 'src/app/shared/modals/data-table/data-table-result';
import { TransactionSearchModel } from '../models/transaction-search.model';
import { ResponseT } from 'src/app/shared/models/response.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DssPKIService {

  private _searchUrl: string = environment.dssApiUrl + 'dssPkiController/verifyUSBResponse';
  private _transactionSearchState: BehaviorSubject<TransactionSearchModel> = new BehaviorSubject<TransactionSearchModel>(null);

  constructor(private http: HttpClient) { }

  getTransactionSearchState() {
    return this._transactionSearchState.asObservable();
  }

  setTransactionSearchState(_state: TransactionSearchModel) {
      this._transactionSearchState.next(_state);
  }

  checkPin(key: string, time: string) {
    let httpParams: HttpParams =  new HttpParams();
    httpParams = httpParams.append('key', key);
    httpParams = httpParams.append('time', time);
    return this.http.get<ResponseT<DataTableResult<any[]>>>(this._searchUrl, {
        params: httpParams
    });
  }
}
