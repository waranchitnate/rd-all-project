import { Injectable } from '@angular/core';
import {AuthGuard} from '../../../core/guard/auth.guard';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {DssAuthenticationService} from '../services/dss-authentication.service';

@Injectable({
  providedIn: 'root'
})
export class DssAuthGuard implements CanActivate {

  constructor(private dssAuthenticationService: DssAuthenticationService, private authGuard: AuthGuard) {

  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authGuard.canActivate(next, state) || this.dssAuthenticationService.isLogin();
  }
}
