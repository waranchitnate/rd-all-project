import { Injectable } from '@angular/core';
import { WelfareConstant } from '../welfare-constant';
import { BsModalRef } from 'ngx-bootstrap';
import { ModalService } from 'src/app/shared/services/modal.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class WelfareModalService {

  constructor(public modalService: ModalService, private toastrService: ToastrService) { }

  openConfirmDeleteModal(fileName, extraMsg): BsModalRef {
    return this.modalService.openConfirmModal(WelfareConstant.title.deleteConfirmation, extraMsg + WelfareConstant.message.deleteConfirmation.replace('{0}', (fileName != null && fileName !== undefined ? fileName : '')));
  }

  openConfirmSaveModal(): BsModalRef {
    return this.modalService.openConfirmModal(WelfareConstant.title.saveConfirmation, WelfareConstant.message.saveConfirmation);
  }

  openUploadConfirmationModal(): BsModalRef {
    return this.modalService.openConfirmModal(WelfareConstant.title.uploadConfirmation, WelfareConstant.message.uploadConfirmation);
  }

  openfirmationModal(title, content): BsModalRef {
    return this.modalService.openConfirmModal(title, content);
  }

  openSaveSuccessModal() {
    this.toastrService.success(WelfareConstant.message.saveSuccess, WelfareConstant.title.saveSuccess);
  }

  openSaveErrorModal() {
    this.toastrService.error(WelfareConstant.message.saveError, WelfareConstant.title.saveError);
  }

  openWarningModal(msg){
    this.toastrService.warning(msg, WelfareConstant.title.saveWarning);
  }

  openErrorModal(msg){
    this.toastrService.error(msg, WelfareConstant.title.saveWarning);
  }

  openDownloadErrorModal() {
    this.toastrService.error(WelfareConstant.message.downloadError, WelfareConstant.title.saveWarning );
  }

  openInfoModal(title, msg) {
    this.toastrService.info(msg, title);
  }
}
