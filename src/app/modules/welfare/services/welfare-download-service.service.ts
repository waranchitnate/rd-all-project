import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class WelfareDownloadServiceService {

  constructor(private sanitizer: DomSanitizer) { }

  downloadFile(fileContent: string){
    const blob = new Blob([fileContent], { type: 'application/octet-stream' });
    const fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  dataURItoBlob(dataURI, contentType) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: contentType });
    return blob;
 }

 downloadFileByFileDto(file){
    const fileBlob = this.dataURItoBlob(file.dataBase64, file.contentType);
    const url = window.URL.createObjectURL(fileBlob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = file.fileBaseName;
    a.click();
 }

 downloadFileIe(file){
  if(window.navigator.msSaveOrOpenBlob) {
    console.log('ie' + file.fileBaseName);
    const byteCharacters = atob(file.dataBase64);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], file.contentType);
    window.navigator.msSaveOrOpenBlob(blob, file.fileBaseName +  '.' + file.fileExtension);
  } else {
    const fileBlob = this.dataURItoBlob(file.dataBase64, file.contentType);
    const url = window.URL.createObjectURL(fileBlob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = file.fileBaseName;
    a.click();
  }
 }

 downloadReportIe(file){
  if(window.navigator.msSaveOrOpenBlob) {
    console.log('ie' + file.fileBaseName);
    const blob = new Blob([file.dataBase64], file.contentType);
    window.navigator.msSaveOrOpenBlob(blob, file.fileBaseName +   '.' + file.fileExtension);
  } else {
    const fileBlob = this.dataURItoBlob(file.dataBase64, file.contentType);
    const url = window.URL.createObjectURL(fileBlob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = file.fileBaseName;
    a.click();
  }
 }

 downloadFileFromCurrentFile(file) {
  const url = URL.createObjectURL(file);
  window.open(url);
 }

 openReportAnotherTab(file){
    console.log('openFileAnotherTab: ');
    const imageBlob = this.dataURItoBlob(file.dataBase64, 'application/pdf');
    const url = URL.createObjectURL(imageBlob);
    // window.open(url, '_blank');

     let a = document.createElement('a');
     document.body.appendChild(a);
     a.href = url;
     a.download = file.fileBaseName + '.pdf';
     a.target = '_blank';
     a.click();
 }
}
