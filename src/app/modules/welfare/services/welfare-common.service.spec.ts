import { TestBed } from '@angular/core/testing';

import { WelfareCommonService } from './welfare-common.service';

describe('WelfareCommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WelfareCommonService = TestBed.get(WelfareCommonService);
    expect(service).toBeTruthy();
  });
});
