import { TestBed } from '@angular/core/testing';

import { WelfareDownloadServiceService } from './welfare-download-service.service';

describe('WelfareDownloadServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WelfareDownloadServiceService = TestBed.get(WelfareDownloadServiceService);
    expect(service).toBeTruthy();
  });
});
