import { TestBed } from '@angular/core/testing';

import { WelfareModalService } from './welfare-modal.service';

describe('WelfareModalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WelfareModalService = TestBed.get(WelfareModalService);
    expect(service).toBeTruthy();
  });
});
