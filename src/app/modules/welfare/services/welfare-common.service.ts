import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WelfareCommonService {

  constructor(private http: HttpClient) { }

  internalWelfareManagementApiUrl = environment.internalWelfareManagementApiUrl;
  welfareCommonEndpointUrl = 'gws/common';

  public getTaxYearList(): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.internalWelfareManagementApiUrl + this.welfareCommonEndpointUrl + '/getTaxYearList');
  }

  public getDataSourceList(): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.internalWelfareManagementApiUrl + this.welfareCommonEndpointUrl + '/getDataSourceList');
  }

  public getCurrentDate(): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.internalWelfareManagementApiUrl + this.welfareCommonEndpointUrl + '/getCurrentDate');
  }
}
