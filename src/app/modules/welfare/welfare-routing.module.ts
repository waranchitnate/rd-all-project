import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/home'},
  {path: 'config' , loadChildren: './modules/welfare-config/welfare-config.module#WelfareConfigModule'},
  {path: 'appeal', loadChildren: './modules/welfare-appeal/welfare-appeal.module#WelfareAppealModule'},
  {path: 'registration', loadChildren: './modules/welfare-registration/welfare-registration.module#WelfareRegistrationModule'},
  {path: 'website', loadChildren: './modules/welfare-website/welfare-website.module#WelfareWebsiteModule'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelfareRoutingModule { }
