import { TestBed } from '@angular/core/testing';

import { WelfareConfigService } from './welfare-config.service';

describe('WelfareConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WelfareConfigService = TestBed.get(WelfareConfigService);
    expect(service).toBeTruthy();
  });
});
