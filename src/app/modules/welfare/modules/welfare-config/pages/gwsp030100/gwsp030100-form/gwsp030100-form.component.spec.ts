import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp030100FormComponent } from './gwsp030100-form.component';

describe('Gwsp030100FormComponent', () => {
  let component: Gwsp030100FormComponent;
  let fixture: ComponentFixture<Gwsp030100FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp030100FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp030100FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
