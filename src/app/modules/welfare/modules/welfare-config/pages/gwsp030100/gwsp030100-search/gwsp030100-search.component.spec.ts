import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp030100SearchComponent } from './gwsp030100-search.component';

describe('Gwsp030100SearchComponent', () => {
  let component: Gwsp030100SearchComponent;
  let fixture: ComponentFixture<Gwsp030100SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp030100SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp030100SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
