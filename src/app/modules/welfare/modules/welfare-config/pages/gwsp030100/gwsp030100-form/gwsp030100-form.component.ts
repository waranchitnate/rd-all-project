import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { WelfareConfigService } from '../../../welfare-config.service';
import { WelfareAppealOrganization } from 'src/app/modules/welfare/models/welfare-appeal-organization.model';
import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp030100-form',
  templateUrl: './gwsp030100-form.component.html',
  styleUrls: ['./gwsp030100-form.component.css']
})
export class Gwsp030100FormComponent implements OnInit {

  welfareAppealOrganizationForm:  FormGroup;
  welfareAppealOrganization: WelfareAppealOrganization;
  orgId;
  organizationList: Array<any>;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: WelfareModalService
    , public bsModalService: BsModalService, private welfareConfigService: WelfareConfigService, public authService: AuthenticationService) {
  }


  ngOnInit() {
    this.welfareAppealOrganization = new WelfareAppealOrganization();
    this.welfareAppealOrganizationForm = this.initialWelfareAppealOrganizationForm(this.welfareAppealOrganization);
      this.activatedRoute.paramMap.subscribe(v => {
        this.orgId = v.get('id');
        if ( this.orgId != null) {
          this.getWelfareAppealOrganizationById(this.orgId);
        } else {
          this.setDefaultValue();
        }
      });
  }

  getWelfareAppealOrganizationById(id: string) {
    this.welfareConfigService.getAppealOrganizationById(id).subscribe((res: any) => {
      if (res.status === 200) {
        this.welfareAppealOrganization = res.data;
        this.welfareAppealOrganizationForm = this.initialWelfareAppealOrganizationForm(this.welfareAppealOrganization);
      } else {
        this.modalService.openErrorModal(res.message);
        this.back();
      }
    }, (error: any) => {
      this.modalService.openErrorModal(error.message);
    });
  }

  initialWelfareAppealOrganizationForm(welfareAppealOrganization:  WelfareAppealOrganization): FormGroup {
    return new FormGroup({
      orgId: new FormControl(welfareAppealOrganization.orgId, Validators.required),
      orgCode: new FormControl(welfareAppealOrganization.orgCode, Validators.required),
      orgName: new FormControl(welfareAppealOrganization.orgName, Validators.required),
      level: new FormControl(welfareAppealOrganization.level, Validators.required),
      parentId: new FormControl(welfareAppealOrganization.parentId),
      dataStatus: new FormControl(welfareAppealOrganization.dataStatus, Validators.required),
      dataFlag: new FormControl(welfareAppealOrganization.dataFlag, Validators.required)
    });
  }

  setDefaultValue() {
    this.welfareAppealOrganization.dataFlag = 'ADD';
    this.welfareAppealOrganization.dataStatus = 'ACTIVE';
    this.welfareAppealOrganizationForm = this.initialWelfareAppealOrganizationForm(this.welfareAppealOrganization);
  }

  saveAppealOrganization() {
    if (this.welfareAppealOrganizationForm.valid) {
      this.welfareConfigService.saveAppealOrganization(this.welfareAppealOrganizationForm.getRawValue()).subscribe((result: any) => {
        if (result.status === 200) {
          this.modalService.openSaveSuccessModal();
          this.router.navigateByUrl('/gws/config/gwsp030100/search');
        } else {
          this.modalService.openErrorModal(result.message);
        }
      });
    } else {
      this.modalService.openWarningModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.welfareAppealOrganizationForm);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  back() {
    this.router.navigateByUrl('/gws/config/gwsp030100/search');
  }

  setValue(val){
    if (val != null) {
      this.getValueByFormControlName('orgId', val.orgId);
      this.getValueByFormControlName('orgName', val.orgName);
      this.getValueByFormControlName('orgCode', val.orgCode);
      this.getValueByFormControlName('level', val.level);
      this.getValueByFormControlName('parentId', val.parentId);
    } else {
      this.getValueByFormControlName('orgId', null);
      this.getValueByFormControlName('orgName', null);
      this.getValueByFormControlName('orgCode', null);
      this.getValueByFormControlName('level', null);
      this.getValueByFormControlName('parentId', null);
    }
  }

  getValueByFormControlName(name: string, val: any) {
    return this.welfareAppealOrganizationForm.get(name).setValue(val);
  }


  get orgIdControl(): AbstractControl { return this.welfareAppealOrganizationForm.get('orgId'); }
  get orgNameControl(): AbstractControl { return this.welfareAppealOrganizationForm.get('orgName'); }
  get dataStatusControl(): AbstractControl { return this.welfareAppealOrganizationForm.get('dataStatus'); }
  get dataFlagControl(): AbstractControl { return this.welfareAppealOrganizationForm.get('dataFlag'); }
}
