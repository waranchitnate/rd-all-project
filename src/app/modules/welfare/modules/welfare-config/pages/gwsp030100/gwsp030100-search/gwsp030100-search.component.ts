import { Component, OnInit, ViewChild } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { FormBuilder, Validators } from '@angular/forms';
import { WelfareConfigService } from '../../../welfare-config.service';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { Router } from '@angular/router';
import { OrganizationTypeAheadComponent } from 'src/app/modules/welfare/shared/organization-type-ahead/organization-type-ahead.component';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp030100-search',
  templateUrl: './gwsp030100-search.component.html',
  styleUrls: ['./gwsp030100-search.component.css']
})
export class Gwsp030100SearchComponent implements OnInit {
  @ViewChild(FloatButtonComponent) floatComp;
  welfareAppealOrganizationList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  searchCondition = this.fb.group({
    orgId: ['', Validators.required],
    dataStatus: ['ACTIVE'],
    search: [true]
  });
  @ViewChild('org') org: OrganizationTypeAheadComponent;

  constructor(private router: Router, private fb: FormBuilder, private welfareConfigService: WelfareConfigService
    , private modalService: ModalService, public authService: AuthenticationService) { }

  ngOnInit() {
    this.setDefaultSort();
    if (this.floatComp !== undefined && this.floatComp != null){
      this.floatComp.urlAdd = '/gws/config/gwsp030100/form';
      this.floatComp.tooltipMsg = 'เพิ่ม';
    }

    this.getData();
  }

  clear() {
    this.searchCondition.reset();
    this.searchCondition.get('orgId').setValue(null);
    this.searchCondition.get('dataStatus').setValue('ACTIVE');
    this.org.clearCondition();
    this.getData();
  }

  goToDelete(row) {
    const modalRef = this.modalService.openConfirmModal('ยืนยัน', 'คุณต้องการปิดการใช้งานหน่วยงานอุทธรณ์ ' + row.orgName + ' ?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        this.modalService.openModal('บันทึกสำเร็จ', 'ปิดการใช้งานสำเร็จ');
      } else {
        console.log('false');
        this.modalService.openModal('แจ้งเตือน', 'ปิดการใช้งานไม่สำเร็จ');
      }
    });
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  setDefaultSort() {
    this.pageRequest.sortFieldName = 'T1.ORG_CODE';
    this.pageRequest.sortDirection = 'ASC';
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.setDefaultSort();
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  getData() {
    const condition = this.searchCondition.getRawValue();
    condition.pageRequestDto = this.pageRequest;
    this.welfareConfigService.getAppealOrganizationListByCondition(condition).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  setValue(val) {
    if (val != null) {
      this.searchCondition.get('orgId').setValue(val.orgId);
    } else {
      this.searchCondition.get('orgId').setValue(null);
    }
  }

  goToAdd() {
    this.router.navigateByUrl('/gws/config/gwsp030100/form');
  }

  goToEdit(row) {
    this.router.navigateByUrl('/gws/config/gwsp030100/edit/' + row.aesOrgId);
  }

}
