import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp030200FormComponent } from './gwsp030200-form.component';

describe('Gwsp030200FormComponent', () => {
  let component: Gwsp030200FormComponent;
  let fixture: ComponentFixture<Gwsp030200FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp030200FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp030200FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
