import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp030200SearchComponent } from './gwsp030200-search.component';

describe('Gwsp030200SearchComponent', () => {
  let component: Gwsp030200SearchComponent;
  let fixture: ComponentFixture<Gwsp030200SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp030200SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp030200SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
