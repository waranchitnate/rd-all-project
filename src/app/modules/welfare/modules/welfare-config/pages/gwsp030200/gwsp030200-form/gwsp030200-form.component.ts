import { Component, OnInit } from '@angular/core';
import { WelfareTaxType } from 'src/app/modules/welfare/models/welfare-tax-type.model';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalService } from 'src/app/shared/services/modal.service';
import { BsModalService } from 'ngx-bootstrap';
import { WelfareConfigService } from '../../../welfare-config.service';
import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp030200-form',
  templateUrl: './gwsp030200-form.component.html',
  styleUrls: ['./gwsp030200-form.component.css']
})
export class Gwsp030200FormComponent implements OnInit {
  welfareTaxTypeForm:  FormGroup;
  welfareTaxType: WelfareTaxType;
  taxTypeId;
  workSystemList: Array<any>;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: WelfareModalService
    , public bsModalService: BsModalService, private welfareConfigService: WelfareConfigService, public authService: AuthenticationService) { }

  ngOnInit() {
    this.welfareTaxType = new WelfareTaxType();
    this.welfareTaxTypeForm = this.initialWelfareTaxTypeForm(this.welfareTaxType);

    this.welfareConfigService.getWorkSystems().subscribe((result: any) => {
      this.workSystemList = result.data;

      this.activatedRoute.paramMap.subscribe(v => {
        this.taxTypeId = v.get('id');
        if ( this.taxTypeId != null) {
          this.getWelfareTaxTypeById(this.taxTypeId);
        } else {
          this.setDefaultValue();
        }
      });
    });
  }

  getWelfareTaxTypeById(id: string) {
    this.welfareConfigService.getTaxTypeById(id).subscribe((res: any) => {
      if (res.status === 200) {
        this.welfareTaxType = res.data;
        this.welfareTaxTypeForm = this.initialWelfareTaxTypeForm(this.welfareTaxType);
      } else {
        this.modalService.openErrorModal(res.message);
        this.back();
      }
    }, (error: any) => {
      this.modalService.openErrorModal(error.message);
    });
  }

  setDefaultValue() {
    this.welfareTaxType.dataFlag = 'ADD';
    this.welfareTaxType.workSystemId = -1;
    this.welfareTaxTypeForm = this.initialWelfareTaxTypeForm(this.welfareTaxType);
  }

  initialWelfareTaxTypeForm(welfareTaxType:  WelfareTaxType): FormGroup {
    return new FormGroup({
      taxTypeId: new FormControl(welfareTaxType.taxTypeId),
      taxTypeCode: new FormControl(welfareTaxType.taxTypeCode, Validators.required),
      workSystemId: new FormControl(welfareTaxType.workSystemId, Validators.required),
      taxTypeDesc: new FormControl(welfareTaxType.taxTypeDesc, Validators.required),
      fileCode: new FormControl(welfareTaxType.fileCode, Validators.required),
      dataFlag: new FormControl(welfareTaxType.dataFlag, Validators.required)
    });
  }

  saveTaxType() {
    if (this.welfareTaxTypeForm.valid) {
      this.welfareConfigService.saveTaxType(this.welfareTaxTypeForm.getRawValue()).subscribe((result: any) => {
        if (result.status === 200) {
          this.modalService.openSaveSuccessModal();
          this.router.navigateByUrl('/gws/config/gwsp030200/search');
        } else {
          this.modalService.openErrorModal(result.message);
        }
      });
    } else {
      this.modalService.openWarningModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.welfareTaxTypeForm);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  back() {
    this.router.navigateByUrl('/gws/config/gwsp030200/search');
  }

  onWorkSystemChange(value) {
    this.welfareTaxTypeForm.get('taxTypeCode').setValue(null);

    if (value > -1) {
      this.welfareTaxTypeForm.get('taxTypeCode').setValue(value);
    }
  }

  get taxTypeCodeControl(): AbstractControl { return this.welfareTaxTypeForm.get('taxTypeCode'); }
  get workSystemIdControl(): AbstractControl { return this.welfareTaxTypeForm.get('workSystemId'); }
  get taxTypeDescControl(): AbstractControl { return this.welfareTaxTypeForm.get('taxTypeDesc'); }
  get fileCodeControl(): AbstractControl { return this.welfareTaxTypeForm.get('fileCode'); }
  get dataFlagControl(): AbstractControl { return this.welfareTaxTypeForm.get('dataFlag'); }
}
