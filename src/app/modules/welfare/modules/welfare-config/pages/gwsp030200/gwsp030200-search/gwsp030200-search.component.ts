import { Component, OnInit, ViewChild } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { WelfareConfigService } from '../../../welfare-config.service';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp030200-search',
  templateUrl: './gwsp030200-search.component.html',
  styleUrls: ['./gwsp030200-search.component.css']
})
export class Gwsp030200SearchComponent implements OnInit {
  @ViewChild(FloatButtonComponent) floatComp;
  workSystemList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  searchCondition = this.fb.group({
    workSystemId: [''],
    taxTypeDesc: ['']
  });
  constructor(private router: Router , private activatedRoute: ActivatedRoute, private fb: FormBuilder
    , private welfareConfigService: WelfareConfigService, public authService: AuthenticationService) { }

  ngOnInit() {
    this.setDefaultSort();

    if (this.floatComp !== undefined && this.floatComp != null) {
      this.floatComp.urlAdd = '/gws/config/gwsp030200/form';
      this.floatComp.tooltipMsg = 'เพิ่ม';
    }
    this.getWorkSystemList();
    this.getData();
  }

  setDefaultSort(){
    this.pageRequest.sortFieldName = 'T2.WORK_SYSTEM_NAME';
    this.pageRequest.sortDirection = 'ASC';
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  getWorkSystemList() {
    this.welfareConfigService.getWorkSystems().subscribe((result: any) => {
      this.workSystemList = result.data;
      },
      err => console.log('timeout'),
    );
  }

  clear() {
    this.searchCondition.reset();
    this.searchCondition.get('workSystemId').setValue('');
    this.getData();
  }

  goToAdd() {
    this.router.navigateByUrl('/gws/config/gwsp030200/form');
  }

  goToEdit(row) {
    this.router.navigateByUrl('/gws/config/gwsp030200/edit/' + row.aesTaxTypeId);
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.setDefaultSort();
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  getData() {
    const condition = this.searchCondition.getRawValue();
    condition.pageRequestDto = this.pageRequest;
    this.welfareConfigService.getTaxTypeListByCondition(condition).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;

      });
  }
}
