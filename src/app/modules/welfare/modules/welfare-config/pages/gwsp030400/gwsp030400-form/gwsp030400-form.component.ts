import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder, FormArray } from '@angular/forms';
import { WelfareConfigService } from '../../../welfare-config.service';
import { WelfareWorkSystemEmail } from 'src/app/modules/welfare/models/welfare-work-system-email.model';
import { WelfareWorkSystem } from 'src/app/modules/welfare/models/welfare-work-system.model';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';

@Component({
  selector: 'app-gwsp030400-form',
  templateUrl: './gwsp030400-form.component.html',
  styleUrls: ['./gwsp030400-form.component.css']
})
export class Gwsp030400FormComponent implements OnInit {
  welfareWorkSystem: WelfareWorkSystem;
  welfareWorkSystemForm: FormGroup;
  welfareWorkSystemEmail: WelfareWorkSystemEmail;

  formWorkSystemEmailForm: FormGroup;
  submitted = false;
  id: any;
  // welfareWorkSystemEmailList: Array<WelfareWorkSystemEmail>;
  welfareWorkSystemEmailList: Array<any>;
  mock = [
    { workSystemEmailId: 0, workSystemId: 1 , email: 'nam.weerat@gmail.com' ,workSystemName: 'ภาษีเงินได้บุคคลธรรมดา', workSystemAbbr: 'ITPP'},
    { workSystemEmailId: 1, workSystemId: 1 , email: 'weeratl@wisesoft.go.th' ,workSystemName: 'ภาษีเงินได้บุคคลธรรมดา', workSystemAbbr: 'ITPP'},
    { workSystemEmailId: 4, workSystemId: 1 , email: 'nutn@wisesoft.go.th' ,workSystemName: 'ภาษีเงินได้บุคคลธรรมดา', workSystemAbbr: 'ITPP'},
    { workSystemEmailId: 5, workSystemId: 1 , email: 'kanoknartk@wisesoft.go.th' ,workSystemName: 'ภาษีเงินได้บุคคลธรรมดา', workSystemAbbr: 'ITPP'},
    { workSystemEmailId: 6, workSystemId: 1 , email: 'eswtest1111@gmail.com',workSystemName: 'ภาษีเงินได้บุคคลธรรมดา', workSystemAbbr: 'ITPP' }
  ];
  constructor(private router: Router,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute,
    private modalService: WelfareModalService,
    private welfareConfigService: WelfareConfigService, private fb: FormBuilder) { }

  initialFormWorkSystemEmail(): FormGroup {
    return new FormGroup({
      workSystemId: new FormControl(null),
      workSystemEmailId: new FormControl(null),
      workSystemName: new FormControl(null),
      workSystemAbbr: new FormControl(null),
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }
  ngOnInit() {
    this.welfareWorkSystem = new WelfareWorkSystem();
    this.welfareWorkSystemForm = this.initialwelfareWorkSystemForm(this.welfareWorkSystem);
    // debugger;
    this.formWorkSystemEmailForm = this.initialFormWorkSystemEmail();
    this.id = +this.actRoute.snapshot.queryParamMap.get('id');
    this.getData(this.id);
  }

  initialwelfareWorkSystemForm(welfareWorkSystem:  WelfareWorkSystem): FormGroup {
    return new FormGroup({
      workSystemId: new FormControl({value: welfareWorkSystem.workSystemId, disabled: true }),
      workSystemName: new FormControl({value: welfareWorkSystem.workSystemName, disabled: true }),
      workSystemAbbr: new FormControl({value: welfareWorkSystem.workSystemAbbr, disabled: true }),
      email1: new FormControl({value: welfareWorkSystem.email1}, [Validators.required, Validators.email]),
      email2: new FormControl({value: welfareWorkSystem.email2}, [Validators.email]),
      email3: new FormControl({value: welfareWorkSystem.email3}, [Validators.email]),
      email4: new FormControl({value: welfareWorkSystem.email4}, [Validators.email]),
      email5: new FormControl({value: welfareWorkSystem.email5}, [Validators.email]),
      welfareWorkSystemEmailList : this.fb.array([])
    });
  }

  initWelfareWorkSystemEmail(object): FormGroup {
    return this.fb.group({
      workSystemId: new FormControl(object.workSystemId),
      workSystemEmailId: new FormControl(object.workSystemEmailId),
      email: new FormControl(object.email)
    });
  }

  getData(id: number) {
    // this.welfareWorkSystemEmailList = this.mock;
    var temp: WelfareWorkSystemEmail;
    var workSystemId: number;
    this.welfareWorkSystemEmailList =   [];
    const formArray = this.welfareWorkSystemForm.get('welfareWorkSystemEmailList') as FormArray;

    debugger;
    this.welfareConfigService.getWorkSystemEmailByWorkSystemId(id).subscribe((result) => {
        this.welfareWorkSystemEmailList = result.data;

        if(this.welfareWorkSystemEmailList != null && this.welfareWorkSystemEmailList != undefined && this.welfareWorkSystemEmailList.length > 0){
          for (let index = 0; index < 5; index++) {
            const e = this.welfareWorkSystemEmailList[index];
            if(index == 0) {
              workSystemId = e.workSystemId;
            }
            temp = new WelfareWorkSystemEmail();
            if (e) {
              temp.workSystemEmailId = e.workSystemEmailId;
              temp.workSystemId = e.workSystemId;
              temp.email = e.email;
            } else {
              temp.workSystemEmailId = 0;
              temp.workSystemId = workSystemId;
              temp.email = '';
            }
            formArray.push(this.initWelfareWorkSystemEmail(temp));
          }
          this.welfareWorkSystemForm.get('email1').setValue(formArray.at(0) ? formArray.at(0).get('email').value : '');
          this.welfareWorkSystemForm.get('email2').setValue(formArray.at(1) ? formArray.at(1).get('email').value : '');
          this.welfareWorkSystemForm.get('email3').setValue(formArray.at(2) ? formArray.at(2).get('email').value : '');
          this.welfareWorkSystemForm.get('email4').setValue(formArray.at(3) ? formArray.at(3).get('email').value : '');
          this.welfareWorkSystemForm.get('email5').setValue(formArray.at(4) ? formArray.at(4).get('email').value : '');
          this.welfareWorkSystemForm.get('workSystemId').setValue(id);
          this.welfareWorkSystemForm.get('workSystemName').setValue(this.welfareWorkSystemEmailList[0].workSystemName);
          this.welfareWorkSystemForm.get('workSystemAbbr').setValue(this.welfareWorkSystemEmailList[0].workSystemAbbr);
        } else {
          this.welfareConfigService.getWelfareMWorkSystemById(this.id).subscribe( (res: any) => {
            this.welfareWorkSystemForm.get('email1').setValue(null);
            this.welfareWorkSystemForm.get('email2').setValue(null);
            this.welfareWorkSystemForm.get('email3').setValue(null);
            this.welfareWorkSystemForm.get('email4').setValue(null);
            this.welfareWorkSystemForm.get('email5').setValue(null);
            this.welfareWorkSystemForm.get('workSystemId').setValue(this.id);
            this.welfareWorkSystemForm.get('workSystemName').setValue(res.data.workSystemName);
            this.welfareWorkSystemForm.get('workSystemAbbr').setValue(res.data.workSystemAbbr);

            for (let i = 0; i < 5; i++) {
              temp = new WelfareWorkSystemEmail();
              formArray.push(this.initWelfareWorkSystemEmail(temp));
            }
          });
        }
      });
  }

  saveWorkSystemEmail() {
    this.submitted = true;
    if (this.welfareWorkSystemForm.valid) {
      debugger;
      this.welfareWorkSystemForm.get('welfareWorkSystemEmailList.0.email').setValue(this.welfareWorkSystemForm.get('email1').value);
      this.welfareWorkSystemForm.get('welfareWorkSystemEmailList.1.email').setValue(this.getValidEmail(this.welfareWorkSystemForm.get('email2')));
      this.welfareWorkSystemForm.get('welfareWorkSystemEmailList.2.email').setValue(this.getValidEmail(this.welfareWorkSystemForm.get('email3')));
      this.welfareWorkSystemForm.get('welfareWorkSystemEmailList.3.email').setValue(this.getValidEmail(this.welfareWorkSystemForm.get('email4')));
      this.welfareWorkSystemForm.get('welfareWorkSystemEmailList.4.email').setValue(this.getValidEmail(this.welfareWorkSystemForm.get('email5')));

      this.welfareConfigService.saveWelfareWorkSystemEmail(this.welfareWorkSystemForm.getRawValue()).subscribe((result: any) => {
        if (result.status === 200) {
          this.showSuccess();
          this.backPage();
        } else {
          this.showError();
        }
      });
    } else {
      this.modalService.openWarningModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.welfareWorkSystemForm);
    }
  }

  getValidEmail(formControl: AbstractControl) {
    if(formControl != null && formControl != undefined) {
      return formControl.value;
    }
    return null;
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  backPage() {
    this.router.navigateByUrl('/gws/config/gwsp030400/search');
  }

  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล', 'ไม่สำเร็จ');
  }

  isInvalidAndTouched(myControl: AbstractControl) {
    return myControl.invalid && (myControl.dirty || myControl.touched);
  }
  clear(){
    this.formWorkSystemEmailForm.reset();
  }
  get emailControl(): AbstractControl { return this.formWorkSystemEmailForm.get('email'); }

  get email1Control(): AbstractControl { return this.welfareWorkSystemForm.get('email1'); }
  get email2Control(): AbstractControl { return this.welfareWorkSystemForm.get('email2'); }
  get email3Control(): AbstractControl { return this.welfareWorkSystemForm.get('email3'); }
  get email4Control(): AbstractControl { return this.welfareWorkSystemForm.get('email4'); }
  get email5Control(): AbstractControl { return this.welfareWorkSystemForm.get('email5'); }
}
