import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { WelfareConfigService } from '../../../welfare-config.service';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-gwsp030400-search',
  templateUrl: './gwsp030400-search.component.html',
  styleUrls: ['./gwsp030400-search.component.css']
})
export class Gwsp030400SearchComponent implements OnInit {

  @ViewChild(FloatButtonComponent) floatComp;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  searchCondition = this.fb.group({
    workSystemName: ['']
  });
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private fb: FormBuilder
    , private welfareConfigService: WelfareConfigService, private modalAlertService: ModalService,
    public authService: AuthenticationService, private toastr: ToastrService) { }




  ngOnInit() {
    this.setDefaultSort();
    // this.floatComp.urlAdd = '/gws/config/gwsp030400/form';
    // this.floatComp.tooltipMsg = 'เพิ่ม';
    this.getData();
  }

  getData() {
    debugger;
    const condition = this.searchCondition.getRawValue();
    condition.pageRequestDto = this.pageRequest;
    this.welfareConfigService.getWelfareMWorkSystemByCondition(condition).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;

      });
  }

  goToAdd() {
    this.router.navigateByUrl('/gws/config/gwsp030400/form');
  }

  goToEdit(id) {
    // debugger;
    // this.router.navigate(['/gws/config/gwsp030400/edit',id]);
    this.router.navigate(['/gws/config/gwsp030400/form'], {queryParams: {id}});
    // this.router.navigateByUrl('/gws/config/gwsp030400/edit/' + id);
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.setDefaultSort();
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }
  setDefaultSort() {
    this.pageRequest.sortFieldName = 'WORK_SYSTEM_NAME';
    this.pageRequest.sortDirection = 'ASC';
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }
  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }
  clear() {
    this.searchCondition.reset();
  }

  deleteWorkSystemEmailByWorkSystemEmail(workSystemId) {
    const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ', 'คุณต้องการลบข้อมูล?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        console.log('true');
        this.welfareConfigService.deleteWorkSystemEmailByWorkSystemEmailId(workSystemId)
          .subscribe(
            res => {
              this.showDeleteSuccess();
            },
            error => this.showDeleteError(),
            () => {
              // this.modalService.dismissAll();
              this.ngOnInit();

            }
          );

      } else {
        console.log('false');
      }
    });
  }

  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }
  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ', {
      timeOut: 3000
    });
  }
}
