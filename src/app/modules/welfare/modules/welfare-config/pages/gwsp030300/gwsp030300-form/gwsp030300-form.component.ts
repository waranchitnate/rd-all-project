import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';
import {WelfareConfigService} from '../../../welfare-config.service';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {WelfareConfig} from 'src/app/modules/welfare/models/welfare-config.model';
import {WelfareCommonService} from 'src/app/modules/welfare/services/welfare-common.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import * as moment from 'moment';
@Component({
  selector: 'app-gwsp030300-form',
  templateUrl: './gwsp030300-form.component.html',
  styleUrls: ['./gwsp030300-form.component.css']
})
export class Gwsp030300FormComponent implements OnInit {

  welfareConfigForm:  FormGroup;
  welfareConfig: WelfareConfig;
  configId;
  minDate: Date;
  taxYearList: Array<any>;
  maxConfigDate;
  bsConfig: Partial<BsDatepickerConfig>;
  @ViewChild('startDate') startDateElement: ElementRef;
  @ViewChild('endDate') endDateElement: ElementRef;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private welfareCommonService: WelfareCommonService
    , private modalService: WelfareModalService, public bsModalService: BsModalService
    , private welfareConfigService: WelfareConfigService, public authService: AuthenticationService) { }

  ngOnInit() {
    this.welfareConfig = new WelfareConfig();
    this.welfareConfigForm = this.initialWelfareConfigForm(this.welfareConfig);

    this.bsConfig = Object.assign({}, { containerClass: 'theme-orange' });

    this.welfareCommonService.getTaxYearList().subscribe((res: any) => {
      this.taxYearList = res.data;
      this.activatedRoute.paramMap.subscribe(v => {
        this.configId = v.get('id');
        if ( this.configId != null) {
          this.getWelfareConfigById(this.configId);
        } else {
          this.welfareConfigService.getMaxConfigDate().subscribe((result: any) => {
            this.maxConfigDate = moment(result.data).toDate();
            this.minDate = this.maxConfigDate;
            this.welfareConfigForm.get('startDate').setValue(this.maxConfigDate);
            this.welfareConfigForm.get('endDate').setValue(this.maxConfigDate);
            this.setDefaultValue();
          });
        }
      });
    });
  }

  getWelfareConfigById(id: string) {
    this.welfareConfigService.getConfigById(id).subscribe((res: any) => {
      if (res.status === 200) {
        this.welfareConfig = res.data;
        this.minDate = moment(this.welfareConfig.startDate).toDate();
        this.welfareConfigForm = this.initialWelfareConfigForm(this.welfareConfig);
      } else {
        this.modalService.openErrorModal(res.message);
        this.back();
      }
    }, (error: any) => {
      this.modalService.openErrorModal(error.message);
    });
  }

  setDefaultValue() {
    console.log('setDefaultValue: ');
    this.welfareConfig.startDate = new Date(this.maxConfigDate);
    this.welfareConfig.endDate = this.welfareConfig.startDate;
    this.welfareConfig.process = false;
    this.welfareConfig.year = '';
    this.welfareConfigForm = this.initialWelfareConfigForm(this.welfareConfig);
  }

  initialWelfareConfigForm(welfareConfig:  WelfareConfig): FormGroup {
    return new FormGroup({
      configId: new FormControl(welfareConfig.configId),
      year: new FormControl(welfareConfig.year, Validators.required),
      income: new FormControl(welfareConfig.income, Validators.required),
      startDate: new FormControl((welfareConfig.startDate != null ? moment(welfareConfig.startDate).toDate() : new Date()), Validators.required),
      endDate: new FormControl((welfareConfig.endDate != null ? moment(welfareConfig.endDate).toDate() : new Date()), Validators.required),
      isProcess: new FormControl(welfareConfig.process)
    });
  }

  saveConfig() {
    if (this.welfareConfigForm.valid) {
      this.welfareConfigService.saveConfig(this.welfareConfigForm.getRawValue()).subscribe((result: any) => {
        if (result.status === 200) {
          this.modalService.openSaveSuccessModal();
          this.router.navigateByUrl('/gws/config/gwsp030300/search');
        } else {
          this.modalService.openErrorModal(result.message);
        }
      });
    } else {
      this.modalService.openWarningModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.welfareConfigForm);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  back() {
    this.router.navigateByUrl('/gws/config/gwsp030300/search');
  }

  dateChange() {
    console.log('dateChange : ' , this.welfareConfigForm.get('endDate').value);
    // this.welfareConfigForm.get('endDate').setValue(null);
  }

  yearChange(evt) {
    if (evt === 'null' || evt === null || evt === undefined) {
      this.welfareConfigForm.get('year').setValue(null);
    }
  }

  get yearControl(): AbstractControl { return this.welfareConfigForm.get('year'); }
  get incomeControl(): AbstractControl { return this.welfareConfigForm.get('income'); }
  get startDateControl(): AbstractControl { return this.welfareConfigForm.get('startDate'); }
  get endDateControl(): AbstractControl { return this.welfareConfigForm.get('endDate'); }
  get isProcessControl(): AbstractControl { return this.welfareConfigForm.get('isProcess'); }
}
