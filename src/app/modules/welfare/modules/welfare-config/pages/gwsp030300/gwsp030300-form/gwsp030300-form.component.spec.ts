import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp030300FormComponent } from './gwsp030300-form.component';

describe('Gwsp030300FormComponent', () => {
  let component: Gwsp030300FormComponent;
  let fixture: ComponentFixture<Gwsp030300FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp030300FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp030300FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
