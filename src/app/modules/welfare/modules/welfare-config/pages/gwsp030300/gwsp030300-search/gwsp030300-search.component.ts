import { Component, OnInit, ViewChild } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ModalService } from 'src/app/shared/services/modal.service';
import { WelfareConfigService } from '../../../welfare-config.service';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp030300-search',
  templateUrl: './gwsp030300-search.component.html',
  styleUrls: ['./gwsp030300-search.component.css']
})
export class Gwsp030300SearchComponent implements OnInit {
  @ViewChild(FloatButtonComponent) floatComp;
  taxYearList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  searchCondition = this.fb.group({
    year: ['']
  });

  constructor(private router: Router , private activatedRoute: ActivatedRoute
    , private fb: FormBuilder, private modalService: ModalService
    , private welfareConfigService: WelfareConfigService, public authService: AuthenticationService
    , private welfareCommonService: WelfareCommonService) { }

  ngOnInit() {
    this.setDefaultSort();
    if (this.floatComp !== undefined && this.floatComp != null) {
      this.floatComp.urlAdd = '/gws/config/gwsp030300/form';
      this.floatComp.tooltipMsg = 'เพิ่ม';
    }

    this.getTaxyearList();
    this.getData();
  }

  getTaxyearList() {
    this.welfareCommonService.getTaxYearList().subscribe((result: any) => {
      this.taxYearList = result.data;
      },
      err => console.log('timeout'),
    );
  }

  clear() {
    this.searchCondition.reset();
    this.getData();
  }

  setDefaultSort(){
    this.pageRequest.sortFieldName = 'YEAR';
    this.pageRequest.sortDirection = 'ASC';
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  goToAdd() {
    this.router.navigateByUrl('/gws/config/gwsp030300/form');
  }

  goToEdit(row) {
    this.router.navigateByUrl('/gws/config/gwsp030300/edit/' + row.aesConfigId);
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.setDefaultSort();
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  getData() {
    const condition = this.searchCondition.getRawValue();
    condition.pageRequestDto = this.pageRequest;
    this.welfareConfigService.getConfigListByCondition(condition).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;

      });
  }

}
