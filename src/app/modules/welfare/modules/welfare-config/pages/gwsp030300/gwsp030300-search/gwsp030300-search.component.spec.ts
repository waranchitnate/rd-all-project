import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp030300SearchComponent } from './gwsp030300-search.component';

describe('Gwsp030300SearchComponent', () => {
  let component: Gwsp030300SearchComponent;
  let fixture: ComponentFixture<Gwsp030300SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp030300SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp030300SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
