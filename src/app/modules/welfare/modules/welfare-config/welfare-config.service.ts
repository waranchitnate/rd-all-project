import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ResponseT } from 'src/app/shared/models/response.model';
import { WelfareConfig } from 'src/app/modules/welfare/models/welfare-config.model';
import { Observable } from 'rxjs';
import { WelfareTaxType } from 'src/app/modules/welfare/models/welfare-tax-type.model';
import { WelfareAppealOrganization } from 'src/app/modules/welfare/models/welfare-appeal-organization.model';
import { WelfareWorkSystem } from '../../models/welfare-work-system.model';
import { WelfareWorkSystemEmail } from '../../models/welfare-work-system-email.model';

@Injectable({
  providedIn: 'root'
})
export class WelfareConfigService {

  constructor(private http: HttpClient) { }

  internalWelfareManagementApiUrl = environment.internalWelfareManagementApiUrl;
  welfareConfigEndpointUrl = 'gws/master/config';

  //config
  public saveConfig(welfareConfig: WelfareConfig){
    return this.http.post<ResponseT<any>>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/saveConfig', welfareConfig);
  }

  public getConfigListByCondition(condition: WelfareConfig): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getConfigListByCondition', condition);
  }

  public getConfigById(id: string): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getConfigById', id);
  }

  public getMaxConfigDate(): Observable<any>{
    return this.http.get<any>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getMaxConfigDate');
  }

  public checkExistsConfig(condition: WelfareConfig): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/checkExistsConfig', condition);
  }

  //taxtype
  public saveTaxType(welfareTaxType: WelfareTaxType){
    return this.http.post<ResponseT<any>>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/saveTaxType', welfareTaxType);
  }

  public getTaxTypeListByCondition(condition: WelfareTaxType): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getTaxTypeListByCondition', condition);
  }

  public getTaxTypeById(id: string): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getTaxTypeById', id);
  }

  public getWorkSystems(): Observable<any>{
    return this.http.get<any>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getWorkSystem');
  }

  public getAppealOrganizationList(condition: WelfareAppealOrganization): Observable<any>{
    return this.http.post<any>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getAppealOrganizationList', condition);
  }

  public getAppealOrganizationListByCondition(condition: WelfareAppealOrganization): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getAppealOrganizationListByCondition', condition);
  }

  public saveAppealOrganization(welfareAppealOrganization: WelfareAppealOrganization){
    return this.http.post<ResponseT<any>>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/saveAppealOrganization', welfareAppealOrganization);
  }

  public getAppealOrganizationById(id: string): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getAppealOrganizationById', id);
  }

  public getAppealOrganizationByOrgCode(orgCode: string): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getAppealOrganizationByOrgCode', orgCode);
  }

  public getAppealTopic(): Observable<any>{
    return this.http.get<any>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getAppealTopic');
  }

  public getTransferAppealOrg(): Observable<any>{
    return this.http.get<any>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getTransferAppealOrg');
  }

  public getRegisterAppealOrg(): Observable<any>{
    return this.http.get<any>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getRegisterAppealOrg');
  }

  public getConfigByTaxYear(taxYear: string): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getConfigByTaxYear', taxYear);
  }

  public getWelfareMWorkSystemByCondition(condition: WelfareWorkSystem): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getWelfareMWorkSystemByCondition', condition);
  }


  public deleteWorkSystemEmailByWorkSystemEmailId(workSystemId: number): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/deleteWorkSystemEmailByWorkSystemEmailId', workSystemId);
  }

  public getWorkSystemEmailByWorkSystemId(workSystemId: number): Observable<any> {
    return this.http.get(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getWorkSystemEmailByWorkSystemId?workSystemId=' +workSystemId);
  }
  public saveWelfareWorkSystemEmail(welfareWorkSystem: WelfareWorkSystem){
    return this.http.post<ResponseT<any>>(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/saveWorkSystemEmail', welfareWorkSystem);
  }

  public updateWorkSystemEmailByWorkSystemId(workSystemEmailId: number): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/updateWorkSystemEmailByWorkSystemId', workSystemEmailId);
  }
  public insertWorkSystemEmailByWorkSystemId(welfareWorkSystemEmail: WelfareWorkSystemEmail): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/insertWorkSystemEmailByWorkSystemId', welfareWorkSystemEmail);
  }

  public getWelfareMWorkSystemById(id: number): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareConfigEndpointUrl + '/getWelfareMWorkSystemById', id);
  }
}
