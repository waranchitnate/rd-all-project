import { Gwsp030100FormComponent } from './pages/gwsp030100/gwsp030100-form/gwsp030100-form.component';
import { Gwsp030100SearchComponent } from './pages/gwsp030100/gwsp030100-search/gwsp030100-search.component';
import { Gwsp030200FormComponent } from './pages/gwsp030200/gwsp030200-form/gwsp030200-form.component';
import { Gwsp030200SearchComponent } from './pages/gwsp030200/gwsp030200-search/gwsp030200-search.component';
import { Gwsp030300FormComponent } from './pages/gwsp030300/gwsp030300-form/gwsp030300-form.component';
import { Gwsp030300SearchComponent } from './pages/gwsp030300/gwsp030300-search/gwsp030300-search.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Gwsp030400SearchComponent } from './pages/gwsp030400/gwsp030400-search/gwsp030400-search.component';
import { Gwsp030400FormComponent } from './pages/gwsp030400/gwsp030400-form/gwsp030400-form.component';

const routes: Routes = [
  {path: 'gwsp030100/search', component: Gwsp030100SearchComponent },
  {path: 'gwsp030100/form', component: Gwsp030100FormComponent },
  {path: 'gwsp030100/edit/:id', component: Gwsp030100FormComponent },

  {path: 'gwsp030200/search', component: Gwsp030200SearchComponent },
  {path: 'gwsp030200/form', component: Gwsp030200FormComponent },
  {path: 'gwsp030200/edit/:id', component: Gwsp030200FormComponent },

  {path: 'gwsp030300/search', component: Gwsp030300SearchComponent },
  {path: 'gwsp030300/form', component: Gwsp030300FormComponent },
  {path: 'gwsp030300/edit/:id', component: Gwsp030300FormComponent },

  {path: 'gwsp030400/search', component: Gwsp030400SearchComponent },
  {path: 'gwsp030400/form', component: Gwsp030400FormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelfareConfigRoutingModule { }
