import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelfareConfigRoutingModule } from './welfare-config-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { Gwsp030300SearchComponent } from './pages/gwsp030300/gwsp030300-search/gwsp030300-search.component';
import { Gwsp030300FormComponent } from './pages/gwsp030300/gwsp030300-form/gwsp030300-form.component';
import { Gwsp030100FormComponent } from './pages/gwsp030100/gwsp030100-form/gwsp030100-form.component';
import { Gwsp030100SearchComponent } from './pages/gwsp030100/gwsp030100-search/gwsp030100-search.component';
import { Gwsp030200SearchComponent } from './pages/gwsp030200/gwsp030200-search/gwsp030200-search.component';
import { Gwsp030200FormComponent } from './pages/gwsp030200/gwsp030200-form/gwsp030200-form.component';
import { WelfareSharedModule } from '../../shared/welfare-shared.module';
import { Gwsp030400SearchComponent } from './pages/gwsp030400/gwsp030400-search/gwsp030400-search.component';
import { Gwsp030400FormComponent } from './pages/gwsp030400/gwsp030400-form/gwsp030400-form.component';



@NgModule({
  declarations: [
    Gwsp030300SearchComponent
    , Gwsp030300FormComponent
    , Gwsp030100FormComponent
    , Gwsp030100SearchComponent
    , Gwsp030200SearchComponent
    , Gwsp030200FormComponent
    , Gwsp030400SearchComponent
    , Gwsp030400FormComponent
  ],
  imports: [
    CommonModule,
    WelfareConfigRoutingModule,
    SharedModule,
    WelfareSharedModule
  ]
})
export class WelfareConfigModule { }
