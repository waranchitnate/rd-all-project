import { WelfareRegistrant } from './../models/welfare-registrant.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ResponseT } from 'src/app/shared/models/response.model';
import { WelfareRegistrantLog } from '../models/welfare-registrant-log.model';
import { WelfareVerificationError } from '../models/welfare-verification-error.model';
import { WelfareAppealProgress } from '../../welfare-appeal/models/welfare-appeal-progress.model';

@Injectable({
  providedIn: 'root'
})
export class WelfareRegistrantService {

  constructor(private http: HttpClient) { }

  internalWelfareManagementApiUrl = environment.internalWelfareManagementApiUrl;
  welfareRegistratEndpointUrl = 'gws/registrant';

  public uploadRegistrantFile(fileForm) {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/uploadRegistrantFile', fileForm);
  }

  public checkDupFileName(fileName: string, taxYear: string, sourceData: number): Observable<any> {
    return this.http.get<any>(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/checkDupFileName?fileName=' + fileName + '&taxYear=' + taxYear + '&sourceData=' + sourceData);
  }

  public getRegistrantLogListByCondition(condition: WelfareRegistrant): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/getRegistrantLogListByCondition', condition);
  }

  public searchAppealToExportA2(condition: WelfareRegistrantLog): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/searchAppealToExportA2', condition);
  }

  public downloadErrorFile(downloadForm): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/downloadErrorFile', downloadForm);
  }

  public importRegistrantData(welfareRegistrantLog: WelfareRegistrantLog){
    return this.http.post<ResponseT<any>>(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/importRegistrantData', welfareRegistrantLog);
  }

  public getRegistrantLogById(registrantLogId: string): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/getRegistrantLogById', registrantLogId);
  }

  public downloadA2File(downloadForm): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/downloadA2File', downloadForm);
  }

  public uploadVerificationFile(fileForm) {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/uploadVerificationFile', fileForm);
  }

  public importVerificationData(welfareRegistrantLog: WelfareRegistrantLog){
    return this.http.post<ResponseT<any>>(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/importVerificationData', welfareRegistrantLog);
  }

  public downloadResultFile(downloadForm): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/downloadResultFile', downloadForm);
  }

  public processIncome(welfareRegistrantLog: WelfareRegistrantLog){
    return this.http.post<ResponseT<any>>(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/processIncome', welfareRegistrantLog);
  }

  public printGwsR010301Report(welfareRegistrantLog: WelfareRegistrantLog){
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/printGwsR010301Report', welfareRegistrantLog);
  }

  public importAppealData(welfareRegistrantLog: WelfareRegistrantLog){
    return this.http.post<ResponseT<any>>(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/importAppealData', welfareRegistrantLog);
  }

  public checkExistsAppealData(condition: WelfareRegistrantLog): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/checkExistsAppealData', condition);
  }

  public getMaxRegistrantEndDateByTaxYear(taxYear: string): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/getMaxRegistrantEndDateByTaxYear', taxYear);
  }

  public getRegistrantErrorLogListById(registrantLogId: string): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/getRegistrantLogListById', registrantLogId);
  }

  public getVerificationLogById(verificationLogId: number): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/getVerificationLogById', verificationLogId);
  }

  public getWelfareVerificationErrorByVerificationLogId(welfareVerificationError: WelfareVerificationError): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareRegistratEndpointUrl + '/getWelfareVerificationErrorByVerificationLogId', welfareVerificationError);
  }
}
