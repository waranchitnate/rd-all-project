import { TestBed } from '@angular/core/testing';

import { WelfareRegistrantService } from './welfare-registrant.service';

describe('WelfareRegistrantService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WelfareRegistrantService = TestBed.get(WelfareRegistrantService);
    expect(service).toBeTruthy();
  });
});
