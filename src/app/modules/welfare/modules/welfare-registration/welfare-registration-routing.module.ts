import { Gwsp010300ReportComponent } from './pages/gwsp010300/gwsp010300-report/gwsp010300-report.component';
import { Gwsp010100FormComponent } from './pages/gwsp010100/gwsp010100-form/gwsp010100-form.component';
import { Gwsp010100SearchComponent } from './pages/gwsp010100/gwsp010100-search/gwsp010100-search.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Gwsp010200SearchComponent } from './pages/gwsp010200/gwsp010200-search/gwsp010200-search.component';
import { Gwsp010100ErrorListComponent } from './pages/gwsp010100/gwsp010100-error-list/gwsp010100-error-list.component';
import { Gwsp010100VerificationLogComponent } from './pages/gwsp010100/gwsp010100-verification-log/gwsp010100-verification-log.component';

const routes: Routes = [
  {path: 'gwsp010100/search', component: Gwsp010100SearchComponent },
  {path: 'gwsp010100/form', component: Gwsp010100FormComponent },
  {path: 'gwsp010100/edit/:id', component: Gwsp010100FormComponent },
  {path: 'gwsp010100/verificationLog/:id', component: Gwsp010100VerificationLogComponent },
  {path: 'gwsp010200/search', component: Gwsp010200SearchComponent },
  {path: 'gwsp010300/report', component: Gwsp010300ReportComponent },
  {path: 'gwsp010100/error-list/:id', component: Gwsp010100ErrorListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelfareRegistrationRoutingModule { }
