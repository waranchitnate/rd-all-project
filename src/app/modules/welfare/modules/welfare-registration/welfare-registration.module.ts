import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelfareRegistrationRoutingModule } from './welfare-registration-routing.module';
import { Gwsp010100SearchComponent } from './pages/gwsp010100/gwsp010100-search/gwsp010100-search.component';
import { Gwsp010100FormComponent } from './pages/gwsp010100/gwsp010100-form/gwsp010100-form.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { WelfareSharedModule } from '../../shared/welfare-shared.module';
import { Gwsp010100UploadModalComponent } from './pages/gwsp010100/gwsp010100-upload-modal/gwsp010100-upload-modal.component';
import { Gwsp010200SearchComponent } from './pages/gwsp010200/gwsp010200-search/gwsp010200-search.component';
import { Gwsp010300ReportComponent } from './pages/gwsp010300/gwsp010300-report/gwsp010300-report.component';
import { Gwsp010100ErrorListComponent } from './pages/gwsp010100/gwsp010100-error-list/gwsp010100-error-list.component';
import { Gwsp010100VerificationLogComponent } from './pages/gwsp010100/gwsp010100-verification-log/gwsp010100-verification-log.component';

@NgModule({
  declarations: [Gwsp010100SearchComponent, Gwsp010100FormComponent, Gwsp010100UploadModalComponent, Gwsp010200SearchComponent, Gwsp010300ReportComponent, Gwsp010100ErrorListComponent, Gwsp010100VerificationLogComponent],
  imports: [
    CommonModule,
    WelfareRegistrationRoutingModule,
    SharedModule,
    WelfareSharedModule
  ],
  entryComponents: [Gwsp010100UploadModalComponent],
})
export class WelfareRegistrationModule { }
