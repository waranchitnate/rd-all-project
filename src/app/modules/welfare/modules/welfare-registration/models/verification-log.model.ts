export class VerificationLog {
  verificationLogId: number;
  registrantLogId: number;
  uploadFileName: string;
  uploadFileDate: any;
  insertRecord: number;
  totalPeople: number;
  process: boolean;
  fileCorrect: boolean;
  verificationWorkSystemId: number;
  workSystemAbbr: string;
  processStartDate: Date;
  processEndDate: Date;
  importStartDate: Date;
  importEndDate: Date;
  dataFlag: any;

  //screen
  file: File;

}
