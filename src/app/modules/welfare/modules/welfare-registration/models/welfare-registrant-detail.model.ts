export class WelfareRegistrantDetail {
  registrantDetailId: number;
  registrantId: number;
  workSystemId: number;
  workSystemName: string;
  taxTypeId: number;
  taxTypeDesc: string;
  income: number;
}
