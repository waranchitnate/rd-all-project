import { VerificationLog } from "./verification-log.model";

export class WelfareRegistrantLog {
  registrantLogId: number;
  taxYear: string;
  fileName: string;
  a2StartFileDate: Date;
  a2EndFileDate: Date;
  a2FileName: string;
  insertRecord: number;
  failedRecord: number;
  dupRecord: number;
  errRecord: number;
  successRecord: number;
  invalidFileName: string;
  sourceEnum: number;
  sourceEnumDesc: string;
  stepCode: string;
  stepDesc: string;
  verificationLogList: Array<VerificationLog>;
  expectedVerificationFileList: Array<string>;
  processAll: boolean;
  fileCorrectAll: boolean;
  aesRegistrantLogId: string;
}
