import { RegistrantErrorLogDetail } from "./registrant-error-log-detail.model";

export class WelfareRegistrantErrorLog {
  registrantLogId: number;
  taxYear: string;
  sourceEnum: number;
  sourceEnumDesc: string;
  fileName: string;
  a2FileName: string;
  registrantErrorLogDetailList: Array<RegistrantErrorLogDetail>;
}
