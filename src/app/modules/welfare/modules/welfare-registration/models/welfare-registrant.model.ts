import { WelfareRegistrantDetail } from "./welfare-registrant-detail.model";

export class WelfareRegistrant {
  registrantId: number;
  nid: string;
  taxYear: string;
  firstName: string;
  lastName: string;
  resultDesc: string;
  resultCode: string;
  birthDate: Date;
  registerDate: Date;
  bankName: string;
  sexDesc: string;
  maxIncome: number;
  registrantDetailDtoList: Array<WelfareRegistrantDetail>;
}

