import { PageRequest } from 'src/app/shared/models/page-request';

export class WelfareVerificationError {
  verificationLogId: number;
  fileRow: number;
  detail: string;
  pageRequestDto: PageRequest;
}
