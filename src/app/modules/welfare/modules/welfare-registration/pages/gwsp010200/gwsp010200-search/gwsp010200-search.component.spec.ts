import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp010200SearchComponent } from './gwsp010200-search.component';

describe('Gwsp010200SearchComponent', () => {
  let component: Gwsp010200SearchComponent;
  let fixture: ComponentFixture<Gwsp010200SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp010200SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp010200SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
