import { WelfareModalService } from './../../../../../services/welfare-modal.service';
import { WelfareRegistrantService } from './../../../services/welfare-registrant.service';
import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, AbstractControl, FormControl, Validators, FormGroup } from '@angular/forms';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { PageRequest } from 'src/app/shared/models/page-request';
import { PageResponse } from 'src/app/shared/models/page-response';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { BsModalRef } from 'ngx-bootstrap';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { WelfareConfigService } from '../../../../welfare-config/welfare-config.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import * as moment from 'moment';
import { ifError } from 'assert';

@Component({
  selector: 'app-gwsp010200-search',
  templateUrl: './gwsp010200-search.component.html',
  styleUrls: ['./gwsp010200-search.component.css']
})
export class Gwsp010200SearchComponent implements OnInit {

  maxConfigDate;
  minConfigDate;
  taxYearList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  searchCondition: FormGroup;
  allowA2Export: boolean;
  constructor(private router: Router, private fb: FormBuilder, private welfareRegistrantService: WelfareRegistrantService
    , private modalService: WelfareModalService, private welfareCommonService: WelfareCommonService, public bsModalRef: BsModalRef
    , private welareConfigService: WelfareConfigService, public authService: AuthenticationService) { }

  ngOnInit() {
    this.setDefaultSort();
    this.allowA2Export = true;
    this.searchCondition = this.initialSearchCondition();
    this.getTaxyearList();

    this.welfareCommonService.getCurrentDate().subscribe((res: any) => {
      this.maxConfigDate = moment(res.data).toDate();
      this.searchCondition.get('startDate').setValue(this.maxConfigDate);
      this.searchCondition.get('endDate').setValue(this.maxConfigDate);
    });
  }

  initialSearchCondition(): FormGroup {
    return new FormGroup({
      taxYear: new FormControl('', Validators.required),
      startDate: new FormControl(new Date(), Validators.required),
      endDate: new FormControl(new Date(), Validators.required),
      checkIncStatus: new FormControl(''),
    });
  }

  onCheckIncChange(event){
    const status = event.srcElement.value;
    if (status == '') {
      this.allowA2Export = false;
    } else {
      this.allowA2Export = true;
    }
  }

  setDefaultSort(){
    this.pageRequest.sortFieldName = 'T2.NID';
    this.pageRequest.sortDirection = 'ASC';
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData(true);
  }

  getTaxyearList() {
    this.welfareCommonService.getTaxYearList().subscribe((result: any) => {
      this.taxYearList = result.data;
      },
      err => console.log('timeout'),
    );
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.setDefaultSort();
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData(true);
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  setValue(val) {
    if (val != null) {
      this.searchCondition.get('orgId').setValue(val.orgId);
    } else {
      this.searchCondition.get('orgId').setValue(null);
    }
  }

  clear() {
    this.welfareCommonService.getCurrentDate().subscribe((res: any) => {
      this.maxConfigDate = res.data;
      this.searchCondition.reset();
      this.searchCondition = this.initialSearchCondition();
      this.pageResponse = null;
    });

  }

  getData(isSearchRefresh: boolean) {
    const condition = this.searchCondition.getRawValue();
    condition.pageRequestDto = this.pageRequest;
    if (this.searchCondition.valid) {
      condition.year = this.searchCondition.get('taxYear').value;
      if (isSearchRefresh) {
        this.searchAppealToExportA2(condition);
        this.allowA2Export = true;
      } else {
        // this.welareConfigService.checkExistsConfig(condition).subscribe((resultConfig: any) => {
          // if (!resultConfig.data) {
          //   this.allowA2Export = !resultConfig.data;
          //   this.pageResponse = null;
          //   this.modalService.openWarningModal(WelfareConstant.message.configAppealNotExists);
          // } else {
            // this.welfareRegistrantService.checkExistsAppealData(condition).subscribe((result: any) => {
              // this.allowA2Export = result.data;
              // if (this.allowA2Export) {
              //   this.modalService.openWarningModal(WelfareConstant.message.appealExists);
              // }
              this.searchAppealToExportA2(condition);
              // this.welfareRegistrantService.searchAppealToExportA2(condition).subscribe(
              //   (res: any) => {
              //     this.pageResponse = res.data;
              //     this.pageResponse.totalPages = res.data.totalElements;
              //   });
            // });
          // }
        // });
      }
    } else {
      this.modalService.openWarningModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.searchCondition);
    }
  }

  searchAppealToExportA2(condition) {
    this.welfareRegistrantService.searchAppealToExportA2(condition).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        if ( this.pageResponse.content != null && this.pageResponse.content.length > 0
          && this.searchCondition.get('checkIncStatus').value == '') {
          this.allowA2Export = false;
        }
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  startDateChange() {
    const startDate = this.searchCondition.get('startDate').value;
    if (startDate < this.minConfigDate || startDate > this.maxConfigDate) {
      this.searchCondition.get('startDate').setValue(this.minConfigDate);
      this.modalService.openWarningModal('วันที่รับเรื่องอุทธรณ์ตั้งแต่' + WelfareConstant.message.dateOverConfig);
    }
  }

  endDateChange() {
    const endDate = this.searchCondition.get('endDate').value;
    const startDate = this.searchCondition.get('startDate').value;
    if (endDate > this.maxConfigDate) {
      this.searchCondition.get('endDate').setValue(this.maxConfigDate);
      this.modalService.openWarningModal('ถึงวันที่รับเรื่องอุทธรณ์' + WelfareConstant.message.dateOverConfig);
    }

    if(endDate < startDate){
      this.searchCondition.get('endDate').setValue(this.maxConfigDate);
      this.modalService.openWarningModal(WelfareConstant.message.endDateOverStart);
    }
  }

  processAppeal() {
    const uploadConfirmModalRef = this.modalService.openfirmationModal(WelfareConstant.title.appealConfirmation, WelfareConstant.message.appealConfirmation);
    const gwsp010100FormConfirmModal = (uploadConfirmModalRef.content as AnswerModalComponent);
    gwsp010100FormConfirmModal.answerEvent.subscribe(cfAnwser => {
      if (cfAnwser) {
        const condition = this.searchCondition.getRawValue();
        this.welfareRegistrantService.importAppealData(condition).subscribe((importResult: any) => {
          if (importResult.status === 200) {
            this.modalService.openInfoModal(WelfareConstant.title.processInprogess, WelfareConstant.message.appealA2InProgress);
            this.bsModalRef.hide();
            this.getData(true);
          } else {
            this.modalService.openErrorModal(importResult.message);
          }
        }, importVerificationFileError => {
          this.modalService.openErrorModal(WelfareConstant.message.appealA2ImportFaile);
        });
      } else {
        this.bsModalRef.hide();
      }
    });
  }

  taxYearChange(taxYear) {
    this.welareConfigService.getConfigByTaxYear(taxYear).subscribe((res: any) => {
      const configDate = res.data;
      this.maxConfigDate = moment(configDate.endDate).toDate();
      this.minConfigDate = moment(configDate.startDate).toDate();
      this.searchCondition.get('startDate').setValue(this.minConfigDate);
      this.searchCondition.get('endDate').setValue(this.maxConfigDate);
    });
  }

  get startDateControl(): AbstractControl { return this.searchCondition.get('startDate'); }
  get endDateControl(): AbstractControl { return this.searchCondition.get('endDate'); }
  get taxYearControl(): AbstractControl { return this.searchCondition.get('taxYear'); }
}
