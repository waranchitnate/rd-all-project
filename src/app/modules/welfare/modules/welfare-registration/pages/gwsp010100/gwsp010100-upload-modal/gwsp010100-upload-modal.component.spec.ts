import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp010100UploadModalComponent } from './gwsp010100-upload-modal.component';

describe('Gwsp010100UploadModalComponent', () => {
  let component: Gwsp010100UploadModalComponent;
  let fixture: ComponentFixture<Gwsp010100UploadModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp010100UploadModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp010100UploadModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
