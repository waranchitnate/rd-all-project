import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp010100FormComponent } from './gwsp010100-form.component';

describe('Gwsp010100FormComponent', () => {
  let component: Gwsp010100FormComponent;
  let fixture: ComponentFixture<Gwsp010100FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp010100FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp010100FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
