import { Component, OnInit } from '@angular/core';
import { VerificationLog } from '../../../models/verification-log.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { Router,ActivatedRoute } from '@angular/router';
import { WelfareRegistrantService } from '../../../services/welfare-registrant.service';
import { getLocaleDateFormat } from '@angular/common';
import { WelfareVerificationError } from '../../../models/welfare-verification-error.model';

@Component({
  selector: 'app-gwsp010100-verification-log',
  templateUrl: './gwsp010100-verification-log.component.html',
  styleUrls: ['./gwsp010100-verification-log.component.css']
})
export class Gwsp010100VerificationLogComponent implements OnInit {
  verificationLog: VerificationLog;

  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  constructor(
    private activatedRoute: ActivatedRoute,
    private welfareRegistrantService: WelfareRegistrantService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(v => {
      this.welfareRegistrantService.getVerificationLogById(Number(v.get('id'))).subscribe((res: any) => {
        this.verificationLog = res.data;
        this.getData();
      });
    });
  }


  back() {
    this.router.navigateByUrl('/gws/registration/gwsp010100/search');
  }

  getData() {
    let welfareVerificationError = new WelfareVerificationError();
    welfareVerificationError.verificationLogId = this.verificationLog.verificationLogId;
    welfareVerificationError.pageRequestDto = this.pageRequest;
    this.welfareRegistrantService.getWelfareVerificationErrorByVerificationLogId(welfareVerificationError).subscribe((res: any) => {
      this.pageResponse = res.data;
      this.pageResponse.totalPages = res.data.totalElements;
    });
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }
}
