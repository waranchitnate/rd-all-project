import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp010100ErrorListComponent } from './gwsp010100-error-list.component';

describe('Gwsp010100ErrorListComponent', () => {
  let component: Gwsp010100ErrorListComponent;
  let fixture: ComponentFixture<Gwsp010100ErrorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp010100ErrorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp010100ErrorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
