import { Gwsp010100UploadModalComponent } from './../gwsp010100-upload-modal/gwsp010100-upload-modal.component';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { WelfareRegistrantService } from '../../../services/welfare-registrant.service';
import { FormBuilder } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap';
import { WelfareDownloadServiceService } from 'src/app/modules/welfare/services/welfare-download-service.service';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { ToastrService } from 'ngx-toastr';
import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp010100-search',
  templateUrl: './gwsp010100-search.component.html',
  styleUrls: ['./gwsp010100-search.component.css']
})
export class Gwsp010100SearchComponent implements OnInit {
  fileSize:  number;
  welfareRegistrantLogList: Array<any>;
  sourceDataList: Array<any>;
  taxYearList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  searchCondition = this.fb.group({
    taxYear: [''],
    sourceEnum: [''],
    a2FileName: [''],
    file: [''],
    search: [true]
  });

  constructor(private router: Router, private fb: FormBuilder, private welfareRegistrantService: WelfareRegistrantService
    , private modalService: WelfareModalService, private welfareCommonService:  WelfareCommonService, private uploadModal: BsModalService
    , private welfareDownloadFile: WelfareDownloadServiceService, private toastrService: ToastrService
    , public authService: AuthenticationService) { }

  ngOnInit() {
    this.setDefaultSort();
    this.fileSize = 1000000001;
    this.getTaxyearList();
    this.getSourceDataList();
    this.getData();
  }

  setDefaultSort() {
    this.pageRequest.sortFieldName = 'T1.REGISTRANT_LOG_ID';
    this.pageRequest.sortDirection = 'DESC';
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  getTaxyearList() {
    this.welfareCommonService.getTaxYearList().subscribe((result: any) => {
      this.taxYearList = result.data;
      },
      err => console.log('timeout'),
    );
  }

  getSourceDataList() {
    this.welfareCommonService.getDataSourceList().subscribe((result: any) => {
      this.sourceDataList = result.data;
      },
      err => console.log('timeout'),
    );
  }

  clear() {
    this.searchCondition.reset();
    this.searchCondition.get('taxYear').setValue('');
    this.searchCondition.get('sourceEnum').setValue('');
    this.getData();
  }

  goToEdit(row) {
    this.router.navigateByUrl('/gws/registration/gwsp010100/edit/' + row.aesRegistrantLogId);
  }

  goToErrorList(row) {
    this.router.navigateByUrl('/gws/registration/gwsp010100/error-list/' + row.aesRegistrantLogId);
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.setDefaultSort();
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  getData() {
    const condition = this.searchCondition.getRawValue();
    condition.pageRequestDto = this.pageRequest;
    this.welfareRegistrantService.getRegistrantLogListByCondition(condition).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
    });
  }

  downloadErrorFile(row){
    let formData = new FormData();
    formData.append('fileName', row.invalidFileName);
    formData.append('sourceData', row.sourceEnum);
    formData.append('a2StartFileDate', row.a2StartFileDate);
    this.welfareRegistrantService.downloadErrorFile(formData).subscribe((res: any) => {
      if (res.status === 200) {
        //download file
        this.welfareDownloadFile.downloadFileIe(res.data);
      } else {
        this.modalService.openDownloadErrorModal();
      }
    }, error => {
      this.modalService.openDownloadErrorModal();
    });
  }

  setFileUpload(element){
    let inputElement = element as HTMLInputElement;
    let file = inputElement.files[0];
    if (file !== undefined){
      console.log('file.size', file.size);
      if (file.size > this.fileSize) {
        this.modalService.openWarningModal(WelfareConstant.message.fileSizeOver.replace('{0}', ' 1GB '));
      } else {
        this.searchCondition.get('file').setValue(file);
      }
    }
  }

  isFailedToImportFile(v: any): boolean {
    return ( v.failedRecord > 0) || (v.errRecord > 0) || (v.dupRecord > 0);
  }

  openUploadRegistrantFileModal() {
    const modalRef = this.uploadModal.show(Gwsp010100UploadModalComponent, {backdrop: true, ignoreBackdropClick: true});
    const gwsp010100UploadModal = (modalRef.content as Gwsp010100UploadModalComponent);
    gwsp010100UploadModal.taxYearList = this.taxYearList;
    gwsp010100UploadModal.sourceDataList = this.sourceDataList;
    gwsp010100UploadModal.closeUploadModalEvent.subscribe(result => {
      if (result) {
        this.getData();
      }
    });
  }

  downloadA2File(row){
    let formData = new FormData();
    formData.append('fileName', row.a2FileName);
    formData.append('a2StartFileDate', row.a2StartFileDate);
    this.welfareRegistrantService.downloadA2File(formData).subscribe((res: any) => {
      if (res.status === 200) {
        //download file
        this.welfareDownloadFile.downloadFileIe(res.data);
      } else {
        this.modalService.openDownloadErrorModal();
      }
    }, error => {
      this.modalService.openDownloadErrorModal();
    });
  }

  downloadResultFile(row){
    let formData = new FormData();
    formData.append('fileName', row.resultFileName);
    formData.append('sourceData', row.sourceEnum);
    formData.append('a2StartFileDate', row.a2StartFileDate);
    formData.append('registrantLogId', row.registrantLogId);

    this.welfareRegistrantService.downloadResultFile(formData).subscribe((res: any) => {
      if (res.status === 200) {
        //download file
        this.welfareDownloadFile.downloadFileIe(res.data);
      } else {
        this.modalService.openDownloadErrorModal();
      }
    }, error => {
      this.modalService.openDownloadErrorModal();
    });
  }
}
