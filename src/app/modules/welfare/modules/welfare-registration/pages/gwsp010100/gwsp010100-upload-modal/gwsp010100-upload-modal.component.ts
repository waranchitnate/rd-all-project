import { WelfareModalService } from './../../../../../services/welfare-modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { WelfareRegistrantService } from './../../../services/welfare-registrant.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { WelfareRegistrantLog } from '../../../models/welfare-registrant-log.model';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp010100-upload-modal',
  templateUrl: './gwsp010100-upload-modal.component.html',
  styleUrls: ['./gwsp010100-upload-modal.component.css']
})
export class Gwsp010100UploadModalComponent implements OnInit {
  @Input() taxYearList;
  @Input() sourceDataList;
  @Output('closeUploadModalEvent') closeUploadModalEvent = new EventEmitter();
  gwsp01010UploadForm: FormGroup;
  fileSize:  number;

  constructor(public bsModalRef: BsModalRef, private welfareRegistrantService: WelfareRegistrantService
    , private modalService: WelfareModalService, private fb: FormBuilder, public authService: AuthenticationService) { }

  ngOnInit() {
    this.fileSize = 1000000000;
    // this.fileSize = 1;
    this.gwsp01010UploadForm = new FormGroup({
        taxYear: new FormControl(null, Validators.required),
        file: new FormControl(null, Validators.required),
        fileName: new FormControl(null, Validators.required),
        sourceEnum: new FormControl(null, Validators.required)
      }
    );
  }

  setFileUpload(element) {
    const inputElement = element as HTMLInputElement;
    const file = inputElement.files[0];
    if (file !== undefined) {
      console.log('file.type : ', file.type);
      if (file.size > this.fileSize) {
        this.modalService.openWarningModal(WelfareConstant.message.fileSizeOver.replace('{0}', ' 1GB '));

      } else {
        this.gwsp01010UploadForm.get('file').setValue(file);
        this.gwsp01010UploadForm.get('fileName').setValue(file.name);
      }
    }
  }

  submit() {
    if (this.gwsp01010UploadForm.valid) {
      const uploadConfirmModalRef = this.modalService.openUploadConfirmationModal();
      const gwsp010100ConfirmModal = (uploadConfirmModalRef.content as AnswerModalComponent);
      gwsp010100ConfirmModal.answerEvent.subscribe(cfAnwser => {
        if (cfAnwser) {
          const fileName = this.gwsp01010UploadForm.get('fileName').value;
          const taxYear = this.gwsp01010UploadForm.get('taxYear').value;
          const sourceData = this.gwsp01010UploadForm.get('sourceEnum').value;
          if (fileName !== undefined && fileName != null) {
            this.welfareRegistrantService.checkDupFileName(fileName, taxYear, sourceData).subscribe((res: any) => {
              const checkDupResult = res.data;
              if (checkDupResult == null) {
                this.uploadFile();
              } else if (checkDupResult == 1) {
                const modalRef = this.modalService.openfirmationModal(WelfareConstant.title.uploadConfirmation, WelfareConstant.message.duplicateFile);
                const gwsp010100ConfirmDupModal = (modalRef.content as AnswerModalComponent);
                gwsp010100ConfirmDupModal.answerEvent.subscribe(result => {
                  if (result) {
                    this.uploadFile();
                  } else {
                    this.closeModal();
                  }
                });
              } else {
                this.modalService.openErrorModal(WelfareConstant.message.alreadyProcess);
                this.closeModal();
              }
            });
          }
        } else {
          this.closeModal();
        }
      });
    } else {
      this.markFormGroupTouched(this.gwsp01010UploadForm);
    }
  }

  closeModal() {
    this.bsModalRef.hide();
    this.closeUploadModalEvent.emit(false);
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  uploadFile(){
    const formData = new FormData();
    formData.append('file', this.gwsp01010UploadForm.get('file').value);
    formData.append('sourceData', this.gwsp01010UploadForm.get('sourceEnum').value);
    this.welfareRegistrantService.uploadRegistrantFile(formData).subscribe(res => {
      let condition = new WelfareRegistrantLog();
      condition = this.gwsp01010UploadForm.getRawValue();
      this.welfareRegistrantService.importRegistrantData(condition).subscribe(result => {
        if (result.status === 200) {
          this.modalService.openInfoModal(WelfareConstant.title.uploadSuccess, WelfareConstant.message.uploadSuccess);
          this.bsModalRef.hide();
          this.closeUploadModalEvent.emit(true);
        } else {
          this.closeUploadModalEvent.emit(false);
          this.modalService.openErrorModal(WelfareConstant.message.uploadFailed);
        }
      });
    }, uploadFileError => {
      this.modalService.openErrorModal(WelfareConstant.message.uploadFailed);
    });
  }


  close() {
    this.bsModalRef.hide();
  }

  get taxYear() {
    return this.gwsp01010UploadForm.get('taxYear');
  }

  get file() {
    return this.gwsp01010UploadForm.get('file');
  }

  get sourceEnum() {
    return this.gwsp01010UploadForm.get('sourceEnum');
  }

}
