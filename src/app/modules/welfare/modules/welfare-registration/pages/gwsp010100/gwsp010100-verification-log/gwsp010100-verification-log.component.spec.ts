import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp010100VerificationLogComponent } from './gwsp010100-verification-log.component';

describe('Gwsp010100VerificationLogComponent', () => {
  let component: Gwsp010100VerificationLogComponent;
  let fixture: ComponentFixture<Gwsp010100VerificationLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp010100VerificationLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp010100VerificationLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
