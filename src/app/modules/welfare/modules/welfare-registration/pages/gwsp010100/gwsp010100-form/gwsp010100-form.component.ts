import { VerificationLog } from './../../../models/verification-log.model';
import { WelfareRegistrantLog } from './../../../models/welfare-registrant-log.model';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators, AbstractControl } from '@angular/forms';
import { WelfareRegistrantService } from '../../../services/welfare-registrant.service';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { BsModalService, TabsetComponent, BsModalRef } from 'ngx-bootstrap';
import { WelfareDownloadServiceService } from 'src/app/modules/welfare/services/welfare-download-service.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import * as moment from 'moment';
import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp010100-form',
  templateUrl: './gwsp010100-form.component.html',
  styleUrls: ['./gwsp010100-form.component.css']
})
export class Gwsp010100FormComponent implements OnInit {

  filePattern = ['ITPP', 'WHT', 'VAT'];
  fileSize:  number;

  welfareRegistrantLog: WelfareRegistrantLog;
  welfareRegistrantLogForm:  FormGroup;

  itppVerificationLogList: Array<VerificationLog>;
  itppVerificationLogListForm: FormArray;

  whtVerificationLogList: Array<VerificationLog>;
  whtVerificationLogListForm: FormArray;

  vatVerificationLogList: Array<VerificationLog>;
  vatVerificationLogListForm: FormArray;

  verificationLogList: Array<VerificationLog>;
  verificationListForm:  FormArray;

  uploadedFile: File;
  removeList: Array<VerificationLog>;
  currentId: number;
  todayDate = moment();

  @ViewChild('uploadFileInput') uploadFileInputval: ElementRef;
  @ViewChild('tabVerificationFile') tabVerificationFileVal: TabsetComponent;

  constructor(private router: Router, private fb: FormBuilder, private welfareRegistrantService: WelfareRegistrantService
    , private modalService: WelfareModalService, private welfareCommonService:  WelfareCommonService, private uploadModal: BsModalService
    , private welfareDownloadFile: WelfareDownloadServiceService, private activatedRoute: ActivatedRoute, public bsModalRef: BsModalRef
    , public authService: AuthenticationService) { }

  ngOnInit() {
    moment.locale('th');
    this.fileSize = 1000000000;
    this.currentId = 0;
    this.welfareRegistrantLog = new WelfareRegistrantLog();
    this.welfareRegistrantLogForm = this.initialWelfareRegistrantLogForm(this.welfareRegistrantLog);
    this.activatedRoute.paramMap.subscribe(v => {
      this.welfareRegistrantService.getRegistrantLogById(v.get('id')).subscribe((res: any) => {
        this.welfareRegistrantLog = res.data;
        this.welfareRegistrantLogForm = this.initialWelfareRegistrantLogForm(this.welfareRegistrantLog);

        if (this.welfareRegistrantLog.verificationLogList != null && this.welfareRegistrantLog.verificationLogList !== undefined) {
          this.verificationLogList = this.welfareRegistrantLog.verificationLogList;
          this.initialVerificationLogFormArray();
        }
      });
    });
  }

  initialWelfareRegistrantLogForm(welfareRegistrantLog:  WelfareRegistrantLog): FormGroup {
    return new FormGroup({
      taxYear: new FormControl(welfareRegistrantLog.taxYear),
      fileName: new FormControl(welfareRegistrantLog.fileName),
      a2FileName: new FormControl(welfareRegistrantLog.a2FileName),
      a2StartFileDate: new FormControl(welfareRegistrantLog.a2StartFileDate),
      sourceEnum: new FormControl(welfareRegistrantLog.sourceEnum),
      sourceEnumDesc: new FormControl(welfareRegistrantLog.sourceEnumDesc),
      registrantLogId: new FormControl(welfareRegistrantLog.registrantLogId),
      isProcessAll: new FormControl(welfareRegistrantLog.processAll),
      isFileCorrectAll: new FormControl(welfareRegistrantLog.fileCorrectAll),
      currentUploadFile: new FormControl(null),
      itppVerificationLogList :  this.fb.array([]),
      whtVerificationLogList :  this.fb.array([]),
      vatVerificationLogList :  this.fb.array([]),
    });
  }

  initialVerificationLogFormArray() {
    this.verificationLogList.forEach((item) => {
      item.dataFlag = 'EDIT';
      item.registrantLogId = this.welfareRegistrantLogForm.get('registrantLogId').value;
      const formArray = this.getFormArrayByFileName(item.uploadFileName);
      formArray.push(this.initVerificationLog((item)));

      const verificationLogList = this.getArrayListByFileName(item.uploadFileName);
      verificationLogList.push(item);
    });
  }

  initVerificationLog(object): FormGroup {
    return this.fb.group({
      registrantLogId: new FormControl(object.registrantLogId),
      verificationLogId: new FormControl(object.verificationLogId),
      uploadFileName: new FormControl(object.uploadFileName, Validators.required),
      uploadFileDate: new FormControl(object.uploadFileDate, Validators.required),
      insertRecord: new FormControl(object.insertRecord),
      totalPeople: new FormControl(object.totalPeople),
      isProcess: new FormControl(object.isProcess),
      isFileCorrect: new FormControl(object.isFileCorrect),
      processStartDate: new FormControl(object.processStartDate),
      processEndDate: new FormControl(object.processEndDate),
      importStartDate: new FormControl(object.importStartDate),
      importEndDate: new FormControl(object.importEndDate),
      file: new FormControl(object.file !== undefined ? object.file : null),
      workSystemAbbr: new FormControl(object.workSystemAbbr),
      dataFlag:  new FormControl(object.dataFlag)
    });
  }

  removeFile(row, i) {
     // send to remove in the backend
     if (row.verificationLogId > 0) {
      if (this.removeList == null) {
        this.removeList = new Array<VerificationLog>();
      }
      row.dataFlag = 'DELETE';
      this.removeList.push(row);
    }

    const verificationLogListForm = this.getFormArrayByFileName(row.uploadFileName);
    verificationLogListForm.removeAt(i);

    const verificationLogList = this.getArrayListByFileName(row.uploadFileName);
    verificationLogList.splice(i, 1);
  }

  confirmRemoveFile(row, i){
    let extraMsg = '';
    if (row.verificationLogId > 0){
      extraMsg = WelfareConstant.message.fileInSystem + '\n';
    }

    const removeFileConfirmModalRef = this.modalService.openConfirmDeleteModal(row.uploadFileName, extraMsg);
    const gwsp010100ConfirmModal = (removeFileConfirmModalRef.content as AnswerModalComponent);

    gwsp010100ConfirmModal.answerEvent.subscribe(cfAnwser => {
      if (cfAnwser) {
        this.removeFile(row, i);
        this.bsModalRef.hide();
      } else {
        this.bsModalRef.hide();
      }
    });
  }

  setUploadedFiles(event) {
    if (event.target.files && event.target.files[0]) {
      const filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const file = event.target.files[i];
        const fileName = file.name;
        console.log('fileName: ', fileName);
        if (file !== undefined) {
          if (file.size > this.fileSize) {
            this.modalService.openWarningModal(WelfareConstant.message.fileSizeOver.replace('{0}', '1GB'));
          } else if (!this.checkFileIsCorrect(fileName)) {
            // check file name must be in pattern
            this.modalService.openWarningModal(WelfareConstant.message.uploadFileNameIncorrect + ' ' + fileName);
          } else if (this.checkDuplicateUploadFile(fileName)) {
            // check file Duplicate in current list
            this.modalService.openWarningModal(WelfareConstant.message.duplicateUploadedFile + ' ' + fileName);
          } else {
            this.uploadToQueue(file);
            this.selectTabByFileName(fileName);
          }
        }
      }
      this.uploadFileInputval.nativeElement.value = null;
    }
  }

  setUploadedFile(element) {
    const inputElement = element as HTMLInputElement;
    const file = inputElement.files[0];
    const fileName = file.name;
    if (file !== undefined) {
      if (file.size > this.fileSize) {
        this.modalService.openWarningModal(WelfareConstant.message.fileSizeOver.replace('{0}', '1GB'));
      } else if (!this.checkFileIsCorrect(fileName)) {
        // check file name must be in pattern
        this.modalService.openWarningModal(WelfareConstant.message.uploadFileNameIncorrect + ' ' + fileName);
      } else if (this.checkDuplicateUploadFile(fileName)) {
        // check file Duplicate in current list
        this.modalService.openWarningModal(WelfareConstant.message.duplicateUploadedFile + ' ' + fileName);
      } else {
        this.uploadToQueue(file);
        this.selectTabByFileName(fileName);
      }
      this.uploadFileInputval.nativeElement.value = null;
    }
  }

  checkFileIsCorrect(fileName: string) {
    return this.welfareRegistrantLog.expectedVerificationFileList.some(e => fileName.indexOf(e) > -1);
  }

  checkDuplicateUploadFile(fileName: string) {
    if ((this.itppVerificationLogList == null || this.itppVerificationLogList === undefined)
        && (this.whtVerificationLogList == null || this.whtVerificationLogList === undefined)
        && (this.vatVerificationLogList == null || this.vatVerificationLogList === undefined)) {
      return false;
    } else if ( (this.itppVerificationLogList != null && this.itppVerificationLogList.some(e => fileName.indexOf(e.uploadFileName) > -1))
                || (this.whtVerificationLogList != null && this.whtVerificationLogList.some(e => fileName.indexOf(e.uploadFileName) > -1))
                || (this.vatVerificationLogList != null && this.vatVerificationLogList.some(e => fileName.indexOf(e.uploadFileName) > -1))) {
      return true;
    }
    return false;
  }

  uploadToQueue(file) {
    const fileName = file.name;
    const verificationLogListForm = this.getFormArrayByFileName(fileName);

    const verificationLog = new VerificationLog();
    verificationLog.dataFlag = 'ADD';
    verificationLog.uploadFileDate = moment();
    verificationLog.file = file;
    verificationLog.uploadFileName = fileName;
    verificationLog.registrantLogId = this.welfareRegistrantLogForm.get('registrantLogId').value;
    this.currentId = this.currentId + 1;
    verificationLog.verificationLogId =  ((this.currentId + 1) * -1);
    verificationLog.workSystemAbbr = this.getSystemAbbr(fileName);

    verificationLogListForm.push(this.initVerificationLog(verificationLog));
    const verificationLogList = this.getArrayListByFileName(fileName);
    verificationLogList.push(verificationLog);
    this.welfareRegistrantLogForm.get('isFileCorrectAll').setValue(false);
    this.welfareRegistrantLogForm.get('isProcessAll').setValue(false);
  }

  getSystemAbbr(fileName: string) {
    if (fileName.indexOf(this.filePattern[0]) > -1) {
      return this.filePattern[0];
    } else if (fileName.indexOf(this.filePattern[1]) > -1) {
      return this.filePattern[1];
    } else if (fileName.indexOf(this.filePattern[2]) > -1) {
      return this.filePattern[2];
    } else {
      return null;
    }
  }

  selectTabByFileName(fileName: string) {
    if (fileName.indexOf(this.filePattern[0]) > -1) {
      this.tabVerificationFileVal.tabs[0].active = true;
    } else if (fileName.indexOf(this.filePattern[1]) > -1) {
      this.tabVerificationFileVal.tabs[1].active = true;
    } else if (fileName.indexOf(this.filePattern[2]) > -1) {
      this.tabVerificationFileVal.tabs[2].active = true;
    } else {
      this.tabVerificationFileVal.tabs[0].active = true;
    }
  }

  getFormArrayByFileName(fileName: string): FormArray {
    if (fileName.indexOf(this.filePattern[0]) > -1) {
      return this.welfareRegistrantLogForm.get('itppVerificationLogList') as FormArray;
    } else if (fileName.indexOf(this.filePattern[1]) > -1) {
      return this.welfareRegistrantLogForm.get('whtVerificationLogList') as FormArray;
    } else if (fileName.indexOf(this.filePattern[2]) > -1) {
      return this.welfareRegistrantLogForm.get('vatVerificationLogList') as FormArray;
    } else {
      return null;
    }
  }

  getArrayListByFileName(fileName: string): Array<VerificationLog> {
    if (fileName.indexOf(this.filePattern[0]) > -1) {
      if (this.itppVerificationLogList == null || this.itppVerificationLogList === undefined) {
        this.itppVerificationLogList = new Array<VerificationLog>();
      }
      return this.itppVerificationLogList;
    } else if (fileName.indexOf(this.filePattern[1]) > -1) {
      if (this.whtVerificationLogList == null || this.whtVerificationLogList === undefined) {
        this.whtVerificationLogList = new Array<VerificationLog>();
      }
      return this.whtVerificationLogList;
    } else if (fileName.indexOf(this.filePattern[2]) > -1) {
      if (this.vatVerificationLogList == null || this.vatVerificationLogList === undefined) {
        this.vatVerificationLogList = new Array<VerificationLog>();
      }
      return this.vatVerificationLogList;
    } else {
      return null;
    }
  }

  processIncome() {
    const uploadConfirmModalRef = this.modalService.openfirmationModal(WelfareConstant.title.processConfirmation, WelfareConstant.message.processConfirmation);
    const gwsp010100FormConfirmModal = (uploadConfirmModalRef.content as AnswerModalComponent);
    gwsp010100FormConfirmModal.answerEvent.subscribe(cfAnwser => {
      if (cfAnwser) {
        this.welfareRegistrantService.processIncome(this.welfareRegistrantLog).subscribe((importResult: any) => {
          if (importResult.status === 200) {
            this.modalService.openInfoModal(WelfareConstant.title.processInprogess, WelfareConstant.message.processInprogess);
            this.bsModalRef.hide();
            this.back();
          } else {
            this.modalService.openErrorModal(WelfareConstant.message.processFailed);
          }
        }, importVerificationFileError => {
          this.modalService.openErrorModal(WelfareConstant.message.processFailed);
        });
      } else {
        this.bsModalRef.hide();
      }
    });
  }

  saveWelfareRegistrantLog() {
    if (this.welfareRegistrantLogForm.valid) {
       const itppVerificationLogList = this.welfareRegistrantLogForm.get('itppVerificationLogList') as FormArray;
       const whtVerificationLogList = this.welfareRegistrantLogForm.get('whtVerificationLogList') as FormArray;
       const vatVerificationLogList = this.welfareRegistrantLogForm.get('vatVerificationLogList') as FormArray;

      if ((itppVerificationLogList == null || itppVerificationLogList === undefined || itppVerificationLogList.length === 0)
            && (whtVerificationLogList == null || whtVerificationLogList === undefined  || whtVerificationLogList.length === 0)
            && (vatVerificationLogList == null || vatVerificationLogList === undefined  || vatVerificationLogList.length === 0)) {
          this.modalService.openWarningModal(WelfareConstant.message.pleaseUpload);
          this.markFormGroupTouched(this.welfareRegistrantLogForm);
      } else {
        const uploadConfirmModalRef = this.modalService.openUploadConfirmationModal();
        const gwsp010100FormConfirmModal = (uploadConfirmModalRef.content as AnswerModalComponent);
          gwsp010100FormConfirmModal.answerEvent.subscribe(cfAnwser => {
          if (cfAnwser) {
            let verificationLogList = itppVerificationLogList.getRawValue();
            verificationLogList = verificationLogList.concat(whtVerificationLogList.getRawValue(), vatVerificationLogList.getRawValue());
            const formData = new FormData();
            let deleteFiles = new Array<string>();
            formData.append('a2StartFileDate', this.welfareRegistrantLogForm.get('a2StartFileDate').value);
            for (const item of verificationLogList) {
              if (item.file != null) {
                formData.append('file', item.file);
              }
            }

            if (this.removeList != null && this.removeList !== undefined ) {
              for (const item of this.removeList) {
                deleteFiles.push(item.uploadFileName);
              }
              formData.append('deleteFiles', deleteFiles != null ? JSON.stringify(deleteFiles) : null);
            }

            this.welfareRegistrantService.uploadVerificationFile(formData).subscribe((res: any) => {
              this.welfareRegistrantLog = this.welfareRegistrantLogForm.getRawValue();
              if (this.removeList != null) {
                verificationLogList = this.removeList.concat(verificationLogList);
              }

              this.welfareRegistrantLog.verificationLogList = verificationLogList;
              this.welfareRegistrantService.importVerificationData(this.welfareRegistrantLog).subscribe((importResult: any) => {
                if (importResult.status === 200) {
                  this.modalService.openInfoModal(WelfareConstant.title.uploadSuccess, WelfareConstant.message.uploadSuccess);
                  this.bsModalRef.hide();
                  this.back();
                } else {
                  this.modalService.openErrorModal(importResult.message);
                }
              }, importVerificationFileError => {
                this.modalService.openErrorModal(WelfareConstant.message.uploadFailed);
              });
            }, uploadFileError => {
              this.modalService.openErrorModal(WelfareConstant.message.uploadFailed);
            });
          } else {
            this.bsModalRef.hide();
          }
        });
      }
    } else {
      this.modalService.openWarningModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.welfareRegistrantLogForm);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  back() {
    this.router.navigateByUrl('/gws/registration/gwsp010100/search');
  }

  goToVerificationLog(verificationLog) {
    this.router.navigateByUrl('/gws/registration/gwsp010100/verificationLog/' + verificationLog.verificationLogId);
  }

  get taxYear(): AbstractControl { return this.welfareRegistrantLogForm.get('taxYear'); }
  get fileName(): AbstractControl { return this.welfareRegistrantLogForm.get('fileName'); }
  get a2FileName(): AbstractControl { return this.welfareRegistrantLogForm.get('a2FileName'); }
  get currentUploadFile(): AbstractControl { return this.welfareRegistrantLogForm.get('currentUploadFile'); }
  get sourceEnumDesc(): AbstractControl { return this.welfareRegistrantLogForm.get('sourceEnumDesc'); }

}
