import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp010100SearchComponent } from './gwsp010100-search.component';

describe('Gwsp010100SearchComponent', () => {
  let component: Gwsp010100SearchComponent;
  let fixture: ComponentFixture<Gwsp010100SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp010100SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp010100SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
