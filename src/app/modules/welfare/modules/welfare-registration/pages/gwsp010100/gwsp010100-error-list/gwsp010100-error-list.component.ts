import { VerificationLog } from './../../../models/verification-log.model';
import { WelfareRegistrantLog } from './../../../models/welfare-registrant-log.model';
import { WelfareRegistrantErrorLog } from './../../../models/welfare-registrant-error-log.model';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators, AbstractControl } from '@angular/forms';
import { WelfareRegistrantService } from '../../../services/welfare-registrant.service';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { BsModalService, TabsetComponent, BsModalRef } from 'ngx-bootstrap';
import { WelfareDownloadServiceService } from 'src/app/modules/welfare/services/welfare-download-service.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import * as moment from 'moment';
import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { RegistrantErrorLogDetail } from '../../../models/registrant-error-log-detail.model';


@Component({
  selector: 'app-gwsp010100-error-list',
  templateUrl: './gwsp010100-error-list.component.html',
  styleUrls: ['./gwsp010100-error-list.component.css']
})
export class Gwsp010100ErrorListComponent implements OnInit {

  welfareRegistrantErrorLog: WelfareRegistrantErrorLog;
  welfareRegistrantErrorLogForm: FormGroup;

  welfareRegistrantErrorLogList: Array<RegistrantErrorLogDetail>;
  welfareRegistrantErrorLogListForm: FormArray;

  constructor(private router: Router, private fb: FormBuilder, private welfareRegistrantService: WelfareRegistrantService
    , private modalService: WelfareModalService, private welfareCommonService: WelfareCommonService, private uploadModal: BsModalService
    , private welfareDownloadFile: WelfareDownloadServiceService, private activatedRoute: ActivatedRoute, public bsModalRef: BsModalRef
    , public authService: AuthenticationService) { }

  ngOnInit() {
    moment.locale('th');
    this.welfareRegistrantErrorLog = new WelfareRegistrantErrorLog();
    this.welfareRegistrantErrorLogForm = this.initialWelfareRegistrantErrorLogForm(this.welfareRegistrantErrorLog);
    this.activatedRoute.paramMap.subscribe(v => {
      this.welfareRegistrantService.getRegistrantErrorLogListById(v.get('id')).subscribe((res: any) => {
        // console.log(res)
        this.welfareRegistrantErrorLog = res.data;
        this.welfareRegistrantErrorLogForm = this.initialWelfareRegistrantErrorLogForm(this.welfareRegistrantErrorLog);
        this.welfareRegistrantErrorLogList = res.data.registrantErrorlogListDto;
        // }
      });
    });
  }

  initialWelfareRegistrantErrorLogForm(welfareRegistrantErrorLog: WelfareRegistrantErrorLog): FormGroup {
    return new FormGroup({
      taxYear: new FormControl(welfareRegistrantErrorLog.taxYear),
      fileName: new FormControl(welfareRegistrantErrorLog.fileName),
      a2FileName: new FormControl(welfareRegistrantErrorLog.a2FileName),
      sourceEnum: new FormControl(welfareRegistrantErrorLog.sourceEnum),
      sourceEnumDesc: new FormControl(welfareRegistrantErrorLog.sourceEnumDesc),
      registrantLogId: new FormControl(welfareRegistrantErrorLog.registrantLogId),
      registrantErrorLogDetailList: this.fb.array([]),
    });
  }

  back() {
    this.router.navigateByUrl('/gws/registration/gwsp010100/search');
  }

  get taxYear(): AbstractControl { return this.welfareRegistrantErrorLogForm.get('taxYear'); }
  get fileName(): AbstractControl { return this.welfareRegistrantErrorLogForm.get('fileName'); }
  get a2FileName(): AbstractControl { return this.welfareRegistrantErrorLogForm.get('a2FileName'); }
  get currentUploadFile(): AbstractControl { return this.welfareRegistrantErrorLogForm.get('currentUploadFile'); }
  get sourceEnumDesc(): AbstractControl { return this.welfareRegistrantErrorLogForm.get('sourceEnumDesc'); }
}
