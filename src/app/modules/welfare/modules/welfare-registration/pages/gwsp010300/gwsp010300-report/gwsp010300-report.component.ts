import { WelfareConstant } from './../../../../../welfare-constant';
import { WelfareRegistrantService } from './../../../services/welfare-registrant.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { WelfareDownloadServiceService } from 'src/app/modules/welfare/services/welfare-download-service.service';
import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp010300-report',
  templateUrl: './gwsp010300-report.component.html',
  styleUrls: ['./gwsp010300-report.component.css']
})
export class Gwsp010300ReportComponent implements OnInit {

  taxYearList: Array<any>;
  searchCondition:  FormGroup;
  constructor(private fb: FormBuilder, private welfareCommonService: WelfareCommonService, private welfareDownloadFile: WelfareDownloadServiceService
    , private welfareRegistrantService: WelfareRegistrantService, private modalService: WelfareModalService
    , public authService: AuthenticationService) { }

  ngOnInit() {
    this.searchCondition = this.initialSearchCondition();
    this.welfareCommonService.getTaxYearList().subscribe((res: any) => {
      this.taxYearList = res.data;
      this.searchCondition = this.initialSearchCondition();
    });
  }

  initialSearchCondition(): FormGroup {
    return new FormGroup({
      taxYear: new FormControl('', Validators.required)
    });
  }

  clear() {
    this.searchCondition.reset();
    this.searchCondition.get('taxYear').setValue('');
  }

  print() {
    const condition = this.searchCondition.getRawValue();
    if (this.searchCondition.valid) {
      this.welfareRegistrantService.printGwsR010301Report(condition).subscribe((res: any) => {
        if (res.status === 200) {
          this.welfareDownloadFile.downloadFileIe(res.data);
        } else {
          this.modalService.openErrorModal(WelfareConstant.message.reportError);
        }
      }, () => {
        this.modalService.openErrorModal(WelfareConstant.message.reportError);
      });
    } else {
      this.modalService.openErrorModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.searchCondition);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  get taxYearControl(): AbstractControl { return this.searchCondition.get('taxYear'); }

}
