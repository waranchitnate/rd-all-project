import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp010300ReportComponent } from './gwsp010300-report.component';

describe('Gwsp010300ReportComponent', () => {
  let component: Gwsp010300ReportComponent;
  let fixture: ComponentFixture<Gwsp010300ReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp010300ReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp010300ReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
