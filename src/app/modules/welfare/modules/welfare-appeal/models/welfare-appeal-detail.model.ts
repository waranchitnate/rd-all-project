import { WelfareAppealAttachFile } from "./welfare-appeal-attach-file.model";

export class WelfareAppealDetail {
  nid: string;
  taxYear: string;
  appealDetailId: number;
  appealId: number;
  appealTopicCode: string;
  appealTopicDesc: string;
  approvedBy: string;
  otherTopic: string;
  appealNo: string;
  remark: string;
  approvedDate: Date;
  transfer: Boolean;
  registerOrgId: number;
  transferOrgId: number;
  registerOrgName: string;
  transferOrgName: string;
  stepCode: string;
  stepDesc: string;
  dataFlag: string;
  attachFileDtoList: Array<WelfareAppealAttachFile>;
  checkDisabled: boolean;
}
