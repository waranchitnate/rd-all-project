import { PageRequest } from 'src/app/shared/models/page-request';
export class WelfareAppealProgress {
  taxYear: string;
  orgId: number;
  orgName: string;
  inProgressCount: number;
  finishedCount: number;
  level: number;

  nid: string;
  fullName: string;
  stepDesc: string;
  stepCodeIncCheckDesc: string;
  createdDate: Date;
  pageRequestDto: PageRequest;

  //sector
  sectorOrgId: number;
  sectorLevel: number;
  sectorTaxYear: string;
  sectorOrgName: string;

  //area
  areaOrgId: number;
  areaLevel: number;
  areaOrgName: string;

  //branch
  branchOrgId: number;
  branchLevel: number;
  branchOrgName: string;

  //office
  personOrgId: number;
  personLevel: number;
  personOrgName: string;
  personTaxYear: string;
}
