import { WelfareRegistrant } from "../../welfare-registration/models/welfare-registrant.model";
import { WelfareAppealDetail } from "./welfare-appeal-detail.model";

export class WelfareAppeal {
  appealId: number;
  resultCode: string;
  resultDesc: string;
  nid: string;
  taxYear: string;
  stepCode: string;
  stepDesc: string;
  stepCodeIncCheck: string;
  stepCodeIncCheckDesc: string;
  registrantDto: WelfareRegistrant;
  appealOnlineDto: WelfareRegistrant;
  lastAppealDto: WelfareRegistrant;
  dataFlag: string;
  createdDate: Date;
  appealDetailDtoList: Array<WelfareAppealDetail>;
  appealResultDesc: string;
}
