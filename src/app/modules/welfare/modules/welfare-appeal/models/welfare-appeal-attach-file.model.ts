export class WelfareAppealAttachFile {
  appealAttachFileId: number;
  appealDetailId: number;
  attachFile: string;
  dataFlag: string;
  file: File;
  fileSize: number;
}
