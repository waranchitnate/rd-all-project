import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelfareAppealRoutingModule } from './welfare-appeal-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { WelfareSharedModule } from '../../shared/welfare-shared.module';
import { Gwsp020100AppealModalComponent } from './pages/gwsp020100/gwsp020100-appeal-modal/gwsp020100-appeal-modal.component';
import { Gwsp020100SearchComponent } from './pages/gwsp020100/gwsp020100-search/gwsp020100-search.component';
import { Gwsp020200SearchComponent } from './pages/gwsp020200/gwsp020200-search/gwsp020200-search.component';
import { Gwsp020300ReportComponent } from './pages/gwsp020300/gwsp020300-report/gwsp020300-report.component';
import { Gwsp020200SearchAreaComponent } from './pages/gwsp020200/gwsp020200-search-area/gwsp020200-search-area.component';
import { Gwsp020200SearchBranchComponent } from './pages/gwsp020200/gwsp020200-search-branch/gwsp020200-search-branch.component';
import { Gwsp020200SearchPersonComponent } from './pages/gwsp020200/gwsp020200-search-person/gwsp020200-search-person.component';

@NgModule({
  declarations: [Gwsp020100SearchComponent, Gwsp020100AppealModalComponent, Gwsp020200SearchComponent, Gwsp020300ReportComponent, Gwsp020200SearchAreaComponent, Gwsp020200SearchBranchComponent, Gwsp020200SearchPersonComponent],
  imports: [
    CommonModule,
    WelfareAppealRoutingModule,
    SharedModule,
    WelfareSharedModule
  ],
  entryComponents: [Gwsp020100AppealModalComponent],
})
export class WelfareAppealModule { }
