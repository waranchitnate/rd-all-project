import { Gwsp020200SearchPersonComponent } from './pages/gwsp020200/gwsp020200-search-person/gwsp020200-search-person.component';
import { Gwsp020200SearchBranchComponent } from './pages/gwsp020200/gwsp020200-search-branch/gwsp020200-search-branch.component';
import { Gwsp020200SearchAreaComponent } from './pages/gwsp020200/gwsp020200-search-area/gwsp020200-search-area.component';
import { Gwsp020300ReportComponent } from './pages/gwsp020300/gwsp020300-report/gwsp020300-report.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Gwsp020100SearchComponent } from './pages/gwsp020100/gwsp020100-search/gwsp020100-search.component';
import { Gwsp020200SearchComponent } from './pages/gwsp020200/gwsp020200-search/gwsp020200-search.component';
import { Gwsp020200GuardService } from '../../guards/gwsp020200-guard.service';

const routes: Routes = [
  {path: 'gwsp020100/search', component: Gwsp020100SearchComponent },
  {path: 'gwsp020300/report', component: Gwsp020300ReportComponent },
  {path: 'gwsp020200/search', component: Gwsp020200SearchComponent, data: {prevUrl: null, nextUrl: 'area'}, canActivate: [Gwsp020200GuardService]},
  {path: 'gwsp020200/area', component: Gwsp020200SearchAreaComponent, data: {prevUrl: '', nextUrl: 'branch'}, canActivate: [Gwsp020200GuardService] },
  {path: 'gwsp020200/branch', component: Gwsp020200SearchBranchComponent, data: {prevUrl: 'area', nextUrl: 'person'}, canActivate: [Gwsp020200GuardService] },
  {path: 'gwsp020200/person', component: Gwsp020200SearchPersonComponent, data: {prevUrl: 'branch', nextUrl: null}, canActivate: [Gwsp020200GuardService] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelfareAppealRoutingModule { }
