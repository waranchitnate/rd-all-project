import { WelfareAppealProgress } from './../models/welfare-appeal-progress.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class Gwsp020200SharedDataService {

  private appealProgress;
  currentAppealProgress;

  private prevPath: string;
  private nextPath: string;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  setAppealProgress(condition: WelfareAppealProgress) {
    if (this.appealProgress == null) {
      this.appealProgress =  new BehaviorSubject<WelfareAppealProgress>(null);
      this.currentAppealProgress = this.appealProgress.asObservable();
    }
    this.appealProgress.next(condition);
  }

  clearAppealProgress() {
    console.log('clearAppealProgress');
    this.appealProgress = null;
  }

  getCurrentAppealProgress() {
    return this.appealProgress != null ? this.appealProgress.value : null;
  }

  nextStep(nextPath: string) {
    this.router.navigateByUrl(nextPath);
  }

  setParentStepAndPath(prevUrl: string, nextUrl: string) {
    this.prevPath = prevUrl;
    this.nextPath = nextUrl;
  }

  goToAreaFromSector(row: WelfareAppealProgress) {
    const appealProgress = this.getCurrentAppealProgress();
    appealProgress.sectorOrgId = appealProgress.orgId;
    appealProgress.sectorTaxYear = appealProgress.taxYear;
    appealProgress.sectorOrgName = appealProgress.orgName;
    appealProgress.orgId = row.orgId;
    appealProgress.orgName = row.orgName;
    appealProgress.taxYear = row.taxYear;
    this.goToNextStep(appealProgress);
  }

  goToBranchFromArea(row: WelfareAppealProgress) {
     const appealProgress = this.getCurrentAppealProgress();
     appealProgress.areaLevel = 2;
     appealProgress.areaOrgId = appealProgress.orgId;
     appealProgress.areaOrgName = appealProgress.orgName;
     appealProgress.orgId = row.orgId;
     appealProgress.orgName = row.orgName;
    this.goToNextStep(appealProgress);
  }

  goToPersonFromBranch(row: WelfareAppealProgress) {
    const appealProgress = this.getCurrentAppealProgress();
    appealProgress.branchLevel = 3;
    appealProgress.branchOrgId = appealProgress.orgId;
    appealProgress.branchOrgName = appealProgress.orgName;
    appealProgress.orgId = row.orgId;
    appealProgress.orgName = row.orgName;
    this.goToNextStepPerson(appealProgress);
  }

  goToAppealFromPerson(row: WelfareAppealProgress) {
    const appealProgress = this.getCurrentAppealProgress();
    appealProgress.personOrgId = appealProgress.orgId;
    appealProgress.personOrgName = appealProgress.orgName;
    appealProgress.nid = row.nid;
    appealProgress.personTaxYear = row.taxYear;
    this.goToNextStepPerson(appealProgress);
  }

  goToNextStep(result: WelfareAppealProgress) {
    this.setAppealProgress(result);
    this.nextStep(this.nextPath);
  }

  goToNextStepPerson(result: WelfareAppealProgress) {
    this.setAppealProgress(result);
    const navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({'source': 2})
      }
    };
    this.router.navigate([this.nextPath], navigationExtras);
  }

  backToSectorFromArea() {
    const appealProgress = this.getCurrentAppealProgress();
    appealProgress.orgId = appealProgress.sectorOrgId;
    appealProgress.taxYear = appealProgress.sectorTaxYear;
    appealProgress.orgName = appealProgress.sectorOrgName;
    this.backToPrevious(appealProgress);
  }

  backToAreaFromBranch() {
    const appealProgress = this.getCurrentAppealProgress();
    appealProgress.orgId = appealProgress.areaOrgId;
    appealProgress.orgName = appealProgress.areaOrgName;
    appealProgress.level = appealProgress.areaLevel;
    this.backToPrevious(appealProgress);
  }

  backToBranchFromPerson() {
    const appealProgress = this.getCurrentAppealProgress();
    appealProgress.orgId = appealProgress.branchOrgId;
    appealProgress.orgName = appealProgress.branchOrgName;
    appealProgress.level = appealProgress.branchLevel;
    this.backToPrevious(appealProgress);
  }

  backToPersonFromAppeal() {
    const appealProgress = this.getCurrentAppealProgress();
    appealProgress.orgId = appealProgress.personOrgId;
    appealProgress.orgName = appealProgress.personOrgName;
    appealProgress.level = appealProgress.personLevel;
    this.backToPrevious(appealProgress);
  }

  backToPrevious(result: WelfareAppealProgress) {
    this.setAppealProgress(result);
    this.nextStep(this.prevPath);
  }
}
