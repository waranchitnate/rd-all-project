import { TestBed } from '@angular/core/testing';

import { WelfareAppealService } from './welfare-appeal.service';

describe('WelfareAppealService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WelfareAppealService = TestBed.get(WelfareAppealService);
    expect(service).toBeTruthy();
  });
});
