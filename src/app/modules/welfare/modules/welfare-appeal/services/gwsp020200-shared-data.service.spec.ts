import { TestBed } from '@angular/core/testing';

import { Gwsp020200SharedDataService } from './gwsp020200-shared-data.service';

describe('Gwsp020200SharedDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Gwsp020200SharedDataService = TestBed.get(Gwsp020200SharedDataService);
    expect(service).toBeTruthy();
  });
});
