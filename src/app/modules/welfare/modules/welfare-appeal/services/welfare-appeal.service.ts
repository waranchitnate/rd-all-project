import { WelfareAppealProgress } from './../models/welfare-appeal-progress.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { WelfareAppeal } from '../models/welfare-appeal.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WelfareAppealService {

  constructor(private http: HttpClient) { }

  internalWelfareManagementApiUrl = environment.internalWelfareManagementApiUrl;
  welfareAppealEndpointUrl = 'gws/appeal';

  public searchAppealByNidAndTaxYear(appeal: WelfareAppeal): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareAppealEndpointUrl + '/searchAppealByNidAndTaxYear', appeal);
  }

  public saveAppealDetailDto(appealDetailForm) {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareAppealEndpointUrl + '/saveAppealDetailDto', appealDetailForm);
  }

  public getAppealDetailById(appealDetailId: number): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareAppealEndpointUrl + '/getAppealDetailById', appealDetailId);
  }

  public searchProgressBySector(appealProgress: WelfareAppealProgress): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareAppealEndpointUrl + '/searchProgressBySector', appealProgress);
  }

  public searchProgressByArea(appealProgress: WelfareAppealProgress): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareAppealEndpointUrl + '/searchProgressByArea', appealProgress);
  }

  public searchProgressBranch(appealProgress: WelfareAppealProgress): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareAppealEndpointUrl + '/searchProgressBranch', appealProgress);
  }

  public searchAppealProgressForPerson(appealProgress: WelfareAppealProgress): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareAppealEndpointUrl + '/searchAppealProgressForPerson', appealProgress);
  }

  public downloadAttachFile(downloadForm): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareAppealEndpointUrl + '/downloadAttachFile', downloadForm);
  }

  public printGwsR020301Report(appealProgress: WelfareAppealProgress){
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareAppealEndpointUrl + '/printGwsR020301Report', appealProgress);
  }
}
