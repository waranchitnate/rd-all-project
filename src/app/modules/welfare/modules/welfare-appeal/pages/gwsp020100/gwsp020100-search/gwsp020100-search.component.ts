import { WelfareModalService } from './../../../../../services/welfare-modal.service';
import { Gwsp020100AppealModalComponent } from './../gwsp020100-appeal-modal/gwsp020100-appeal-modal.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { WelfareAppealService } from '../../../services/welfare-appeal.service';
import { WelfareAppeal } from '../../../models/welfare-appeal.model';
import { BsModalService } from 'ngx-bootstrap';
import { WelfareConfigService } from '../../../../welfare-config/welfare-config.service';
import { WelfareAppealDetail } from '../../../models/welfare-appeal-detail.model';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { WelfareAppealProgress } from '../../../models/welfare-appeal-progress.model';
import { Gwsp020200SharedDataService } from '../../../services/gwsp020200-shared-data.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { WelfareAppealOrganization } from 'src/app/modules/welfare/models/welfare-appeal-organization.model';

@Component({
  selector: 'app-gwsp020100-search',
  templateUrl: './gwsp020100-search.component.html',
  styleUrls: ['./gwsp020100-search.component.css']
})
export class Gwsp020100SearchComponent implements OnInit {
  taxYearList: Array<any>;
  appeal: WelfareAppeal;
  searchCondition:  FormGroup;
  isSearch: boolean;
  topicList: Array<any>;
  transferAppealOrgList: Array<any>;
  registerAppealOrgList: Array<any>;
  appealProgress: WelfareAppealProgress;
  currentUser: SsoUserModel;
  defaultTransferOrg: WelfareAppealOrganization;

  sumPit: number;
  sumWht: number;
  sumVat: number;

  sumOnlinePit: number;
  sumOnlineWht: number;
  sumOnlineVat: number;

  sumAppealPit: number;
  sumAppealWht: number;
  sumAppealVat: number;

  canShowAddBtn: boolean;

  constructor(private fb: FormBuilder, private welfareAppealService: WelfareAppealService, private appealModal: BsModalService
    , private welfareCommonService: WelfareCommonService , private modalService: WelfareModalService, private router: Router
    , private welfareConfigService: WelfareConfigService , private activatedRoute: ActivatedRoute
    , private dataService: Gwsp020200SharedDataService, public authService: AuthenticationService) { }

  ngOnInit() {
    this.searchCondition = this.initialSearchCondition();
    this.authService.getSSOUserDetail().subscribe(result => {
      this.currentUser = result;
      this.welfareConfigService.getAppealOrganizationByOrgCode(this.currentUser.userOfficeCode).subscribe((res: any) => {
        this.defaultTransferOrg = res.data;
      });
    });

    this.welfareCommonService.getTaxYearList().subscribe((res: any) => {
      this.taxYearList = res.data;

      this.activatedRoute.queryParams.subscribe(params => {
        if (params && params.data) {
          this.appealProgress = this.dataService.getCurrentAppealProgress();
          const condition = new WelfareAppeal();
          condition.nid = this.appealProgress.nid;
          condition.taxYear = this.appealProgress.personTaxYear;
          this.dataService.setParentStepAndPath('gws/appeal/gwsp020200/person', null);
          this.setCondition(condition);

          this.getData();
        } else {
          this.searchCondition = this.initialSearchCondition();
        }
      });
    });

    this.welfareConfigService.getAppealTopic().subscribe((res: any) => {
      this.topicList = res.data;
    });

    this.welfareConfigService.getTransferAppealOrg().subscribe((res: any) => {
      this.transferAppealOrgList = res.data;
    });

    this.welfareConfigService.getRegisterAppealOrg().subscribe((res: any) => {
      this.registerAppealOrgList = res.data;
    });
  }

  setCondition(condition: WelfareAppeal) {
    this.searchCondition.get('taxYear').setValue(condition.taxYear);
    this.searchCondition.get('nid').setValue(condition.nid);
  }

  initialSearchCondition(): FormGroup {
    return new FormGroup({
      nid: new FormControl(null, [Validators.required, Validators.minLength(13)]),
      taxYear: new FormControl('', Validators.required)
    });
  }

  clear() {
    this.isSearch = false;
    this.searchCondition.reset();
    this.searchCondition = this.initialSearchCondition();
    this.appeal = null;
  }

  back() {
    this.dataService.backToPersonFromAppeal();
  }

  getData() {
    const condition = this.searchCondition.getRawValue();
    console.log('condition : ', condition);
    if (this.searchCondition.valid) {
      this.welfareAppealService.searchAppealByNidAndTaxYear(condition).subscribe(
        (res: any) => {
          this.appeal = res.data;
          console.log('this.appeal ', this.appeal);
          this.isSearch = true;

          const lastAppealDto = this.appeal.lastAppealDto;
          const appealOnlineDto = this.appeal.appealOnlineDto;
          const registrantDto = this.appeal.registrantDto;

          if (lastAppealDto != null
              || (lastAppealDto == null && appealOnlineDto != null && appealOnlineDto.resultCode != 'N')
              || (lastAppealDto == null && appealOnlineDto == null && registrantDto != null && registrantDto.resultCode != 'N')) {
            this.canShowAddBtn = false;
          } else{
            this.canShowAddBtn = true;
          }


          this.calculateSumEach(this.appeal);
        });
    } else {
      this.isSearch = false;
      this.modalService.openWarningModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.searchCondition);
    }
  }

  calculateSumEach(appeal: WelfareAppeal) {
    this.sumPit = 0;
    this.sumWht = 0;
    this.sumVat = 0;

    this.sumOnlinePit = 0;
    this.sumOnlineWht = 0;
    this.sumOnlineVat = 0;

    this.sumAppealPit = 0;
    this.sumAppealWht = 0;
    this.sumAppealVat = 0;

    if(appeal.registrantDto != null &&
        appeal.registrantDto.registrantDetailDtoList != null && appeal.registrantDto.registrantDetailDtoList != undefined){

      appeal.registrantDto.registrantDetailDtoList.forEach(dto => {
        switch (dto.workSystemId) {
          case 1:
            this.sumPit = this.sumPit + dto.income
            break;
          case 2:
            this.sumWht = this.sumWht + dto.income
            break;
          case 3:
            this.sumVat = this.sumVat + dto.income
            break;
          default:
            break;
        }
      });
    }


     if(appeal.appealOnlineDto != null &&
        appeal.appealOnlineDto.registrantDetailDtoList != null && appeal.appealOnlineDto.registrantDetailDtoList != undefined){
          appeal.appealOnlineDto.registrantDetailDtoList.forEach(dto => {
            switch (dto.workSystemId) {
              case 1:
                this.sumOnlinePit = this.sumOnlinePit + dto.income
                break;
              case 2:
                this.sumOnlineWht = this.sumOnlineWht + dto.income
                break;
              case 3:
                this.sumOnlineVat = this.sumOnlineVat + dto.income
                break;
              default:
                break;
            }
      });
    }

  if(appeal.lastAppealDto != null &&
        appeal.lastAppealDto.registrantDetailDtoList != null && appeal.lastAppealDto.registrantDetailDtoList != undefined){
        appeal.lastAppealDto.registrantDetailDtoList.forEach(dto => {
        switch (dto.workSystemId) {
          case 1:
            this.sumAppealPit = this.sumAppealPit + dto.income
            break;
          case 2:
            this.sumAppealWht = this.sumAppealWht + dto.income
            break;
          case 3:
            this.sumAppealVat = this.sumAppealVat + dto.income
            break;
          default:
            break;
        }
      });
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  goToAdd() {
    if (this.appeal == null) {
      this.modalService.openWarningModal('กรุณาค้นหาข้อมูลผู้ลงทะเบียนก่อนเพิ่มเรื่องอุทธรณ์');
    } else {
      const appealDetail = new WelfareAppealDetail();
      appealDetail.appealTopicCode = '';
      appealDetail.stepCode = '1';
      appealDetail.checkDisabled = false;
      appealDetail.registerOrgId = this.defaultTransferOrg.orgId;
      appealDetail.registerOrgName = this.defaultTransferOrg.orgName;
      appealDetail.transferOrgId = this.defaultTransferOrg.orgId;
      appealDetail.transferOrgName = this.defaultTransferOrg.orgName;
      appealDetail.transfer = false;
      this.openAppealModal(null, appealDetail);
    }
  }

  goToEdit(row) {
    this.welfareAppealService.getAppealDetailById(row.appealDetailId).subscribe((res: any) => {
      const welfareAppealDetail = res.data
      const isEdit = welfareAppealDetail.appealDetailId != null && welfareAppealDetail.appealDetailId !== undefined;
      const isFinised = welfareAppealDetail.stepCode == '2';
      if (isEdit && isFinised) {
        welfareAppealDetail.checkDisabled = true;
      } else {
        welfareAppealDetail.checkDisabled = false;
      }
      this.openAppealModal(row.appealDetailId, welfareAppealDetail);
    }, (error: any) => {
      this.modalService.openWarningModal(error.message);
    });

  }

  openAppealModal(appealDetailId: number, appealDetail: WelfareAppealDetail) {

    // backdrop: true,
    const initData = {topicList : this.topicList
                      , nid : this.searchCondition.get('nid').value
                      , taxYear : this.searchCondition.get('taxYear').value
                      , appealDetailId : appealDetailId
                      , appealId: this.appeal.appealId
                      , appealDetail: appealDetail
                      , defaultTransferOrg: this.defaultTransferOrg
                      , ltoTransferOrg: {"orgId": 2875, "orgName": "กองมาตรฐานการจัดเก็บภาษี"}};

    const modalRef = this.appealModal.show(Gwsp020100AppealModalComponent, { ignoreBackdropClick: true, class: 'modal-lg', initialState: initData});
    const gwsp020100AppealModalComponent = (modalRef.content as Gwsp020100AppealModalComponent);
    gwsp020100AppealModalComponent.closeAppealModalEvent.subscribe(result => {
      if (result) {
        this.getData();
      }
    });
  }

  get nidControl(): AbstractControl { return this.searchCondition.get('nid'); }
  get taxYearControl(): AbstractControl { return this.searchCondition.get('taxYear'); }
}
