import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp020100SearchComponent } from './gwsp020100-search.component';

describe('Gwsp020100SearchComponent', () => {
  let component: Gwsp020100SearchComponent;
  let fixture: ComponentFixture<Gwsp020100SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp020100SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp020100SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
