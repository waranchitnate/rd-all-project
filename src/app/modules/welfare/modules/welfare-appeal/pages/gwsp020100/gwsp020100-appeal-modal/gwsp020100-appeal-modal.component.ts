import { AppealOrganizationTypeAheadComponent } from './../../../../../shared/appeal-organization-type-ahead/appeal-organization-type-ahead.component';
import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators, AbstractControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { WelfareAppealService } from '../../../services/welfare-appeal.service';
import { WelfareAppealDetail } from '../../../models/welfare-appeal-detail.model';
import { WelfareAppealAttachFile } from '../../../models/welfare-appeal-attach-file.model';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { WelfareDownloadServiceService } from 'src/app/modules/welfare/services/welfare-download-service.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp020100-appeal-modal',
  templateUrl: './gwsp020100-appeal-modal.component.html',
  styleUrls: ['./gwsp020100-appeal-modal.component.css']
})
export class Gwsp020100AppealModalComponent implements OnInit {
  topicList;
  nid;
  taxYear;
  appealDetailId;
  appealId;
  appealDetail;
  defaultTransferOrg;
  ltoTransferOrg;

  @Output('closeAppealModalEvent') closeAppealModalEvent = new EventEmitter();
  @ViewChild('appealOrg') appealOrg: AppealOrganizationTypeAheadComponent;

  // appealDetail: WelfareAppealDetail;
  appealDetailForm: FormGroup;
  fileSize:  number;
  attachFileDtoList: Array<WelfareAppealAttachFile>;
  currentId: number;
  removeList: Array<WelfareAppealAttachFile>;

  @ViewChild('attachFileInput') attachFileInput: ElementRef;
  constructor(public bsModalRef: BsModalRef, private welfareAppealService: WelfareAppealService, public authService: AuthenticationService
    , private modalService: WelfareModalService, private fb: FormBuilder, private welfareDownloadFile: WelfareDownloadServiceService) {

    }

  ngOnInit() {
    this.fileSize = 5242880; //5mb
    this.currentId = 0;

    this.appealDetailForm = this.initialAppealDetailForm(this.appealDetail);
    if (this.appealDetail.attachFileDtoList != null && this.appealDetail.attachFileDtoList !== undefined) {
      this.attachFileDtoList = this.appealDetail.attachFileDtoList;
      this.initialAppealAttachFileFormArray();
    }
    this.disableForm();

    this.appealDetailForm.get('transfer').valueChanges.subscribe(value => {
      this.appealOrg.clearCondition();
      if (value) {
        this.appealDetailForm.get('transferOrgName').setValue(null);
        this.appealDetailForm.get('transferOrgId').setValue(null);
      } else {
        this.appealDetailForm.get('transferOrgName').setValue(this.defaultTransferOrg.orgName);
        this.appealDetailForm.get('transferOrgId').setValue(this.defaultTransferOrg.orgId);
      }
    });
  }

  disableForm() {
    if (this.appealDetail.checkDisabled) {
      this.appealDetailForm.disable();
    }
  }

  initialAppealDetailForm(welfareAppealDetail:  WelfareAppealDetail): FormGroup {
    return new FormGroup({
      appealDetailId: new FormControl(welfareAppealDetail.appealDetailId),
      appealId: new FormControl(this.appealId),
      nid: new FormControl(this.nid),
      taxYear: new FormControl(this.taxYear),
      appealTopicCode: new FormControl( welfareAppealDetail.appealTopicCode , Validators.required),
      appealTopicDesc: new FormControl( welfareAppealDetail.appealTopicDesc ),
      otherTopic: new FormControl(welfareAppealDetail.otherTopic, welfareAppealDetail.appealTopicCode == '4' ? Validators.required : null),
      appealNo: new FormControl( welfareAppealDetail.appealNo , Validators.required),
      remark: new FormControl(welfareAppealDetail.remark, Validators.required),
      transfer: new FormControl(welfareAppealDetail.transfer),
      registerOrgId: new FormControl(welfareAppealDetail.registerOrgId),
      transferOrgId: new FormControl(welfareAppealDetail.transferOrgId, Validators.required),
      stepCode: new FormControl(welfareAppealDetail.stepCode, Validators.required),
      dataFlag: new FormControl(welfareAppealDetail.appealDetailId != null && welfareAppealDetail.appealDetailId !== undefined ? 'EDIT' : 'ADD'),
      transferOrgName: new FormControl(welfareAppealDetail.transferOrgName),
      registerOrgName: new FormControl(welfareAppealDetail.registerOrgName),
      checkDisabled: new FormControl(welfareAppealDetail.checkDisabled),
      attachFileDtoListForm :  this.fb.array([]),
    });
  }

  initialAppealAttachFileFormArray() {
    this.attachFileDtoList.forEach((item) => {
      item.dataFlag = 'EDIT';
      const formArray = this.appealDetailForm.get('attachFileDtoListForm') as FormArray;
      formArray.push(this.initAppealAttachFile((item)));
    });
  }

  initAppealAttachFile(object): FormGroup {
    return this.fb.group({
      appealAttachFileId: new FormControl(object.appealAttachFileId),
      appealDetailId: new FormControl(object.appealDetailId),
      attachFile: new FormControl(object.attachFile, Validators.required),
      dataFlag: new FormControl(object.dataFlag, Validators.required),
      file: new FormControl(object.file !== undefined ? object.file : null),
      fileSize: new FormControl(object.fileSize !== undefined ? object.fileSize : null),
    });
  }

  removeFile(row, i) {
    // send to remove in the backend
    if (row.appealAttachFileId > 0) {
     if (this.removeList == null) {
       this.removeList = new Array<WelfareAppealAttachFile>();
     }
     row.dataFlag = 'DELETE';
     this.removeList.push(row);
   }

   const attachFileListForm = this.getAttachFileFormArray();
   attachFileListForm.removeAt(i);

   const attachFileDtoList = this.getAttachFileList();
   attachFileDtoList.splice(i, 1);
 }

 setTransferValue(val) {
  if (val != null) {
    this.appealDetailForm.get('transferOrgId').setValue(val.orgId );
  } else {
    this.appealDetailForm.get('transferOrgId').setValue(null);
  }
}

 setUploadedFile(element) {
  const inputElement = element as HTMLInputElement;
  const file = inputElement.files[0];
  const fileName = file.name;

  if (this.getAttachFileList() != null && this.getAttachFileList().length === 10){
    this.modalService.openWarningModal( WelfareConstant.message.fileOverMaximum);
  } else {
    if (file !== undefined) {
      console.log('file.size',file.size);
      if (file.size > this.fileSize) {
        this.modalService.openWarningModal(WelfareConstant.message.fileSizeOver.replace('{0}', ' 5MB '));
      } else if (this.checkDuplicateUploadFile(fileName)) {
        // check file Duplicate in current list
        this.modalService.openWarningModal(WelfareConstant.message.duplicateUploadedFile);
      } else {
        this.uploadToQueue(file);
      }
      this.attachFileInput.nativeElement.value = null;
    }
  }
}

 uploadToQueue(file) {
  const fileName = file.name;
  const attachFileListForm = this.getAttachFileFormArray();

  const appealAttachFile = new WelfareAppealAttachFile();
  appealAttachFile.dataFlag = 'ADD';
  appealAttachFile.file = file;
  appealAttachFile.attachFile = fileName;
  appealAttachFile.appealDetailId = this.appealDetailForm.get('appealDetailId').value;
  appealAttachFile.fileSize = ((file.size / 1024) / 1024);
  this.currentId = this.currentId + 1;
  appealAttachFile.appealAttachFileId =  ((this.currentId + 1) * -1);

  attachFileListForm.push(this.initAppealAttachFile(appealAttachFile));
  const appealAttachFileList = this.getAttachFileList();
  appealAttachFileList.push(appealAttachFile);

}

  getAttachFileFormArray() {
    return this.appealDetailForm.get('attachFileDtoListForm') as FormArray;
  }

  getAttachFileList() {
    if (this.attachFileDtoList == null || this.attachFileDtoList === undefined) {
      this.attachFileDtoList = new Array<WelfareAppealAttachFile>();
    }
    return this.attachFileDtoList;
  }

  checkDuplicateUploadFile(fileName: string) {
    if ((this.attachFileDtoList == null || this.attachFileDtoList === undefined)) {
      return false;
    } else if ( (this.attachFileDtoList != null && this.attachFileDtoList.some(e => fileName.indexOf(e.attachFile) > -1))) {
      return true;
    }
    return false;
  }

  confirmRemoveFile(row, i) {
    const removeFileConfirmModalRef = this.modalService.openConfirmDeleteModal('', row.attachFile);
    const gwsp020100ConfirmModal = (removeFileConfirmModalRef.content as AnswerModalComponent);

    gwsp020100ConfirmModal.answerEvent.subscribe(cfAnwser => {
      if (cfAnwser) {
        this.removeFile(row, i);
      }
    });
  }

  saveWelfareAppealDetail() {
    if (this.appealDetailForm.valid) {
       const appealAttachFileListForm = this.appealDetailForm.get('attachFileDtoListForm') as FormArray;

      const appealConfirmModalRef = this.modalService.openConfirmSaveModal();
      const gwsp010100FormConfirmModal = (appealConfirmModalRef.content as AnswerModalComponent);
        gwsp010100FormConfirmModal.answerEvent.subscribe(cfAnwser => {
        if (cfAnwser) {
          let appealAttachFileList = appealAttachFileListForm.getRawValue();

          const formData = new FormData();
          for (const item of appealAttachFileList) {
            if (item.file != null) {
              formData.append('file', item.file);
            }
          }

          this.appealDetail = this.appealDetailForm.getRawValue();

          if (this.removeList != null) {
            appealAttachFileList = this.removeList.concat(appealAttachFileList);
          }
          this.appealDetail.attachFileDtoList = appealAttachFileList;

          const appealDetailDto = JSON.stringify(this.appealDetail);
          formData.append('appealDetailDto', new Blob([appealDetailDto], {type: 'application/json'}));

          this.welfareAppealService.saveAppealDetailDto(formData).subscribe((res: any) => {
            if (res.status === 200) {
              this.modalService.openSaveSuccessModal();
              this.closeModal(true);
            } else {
              this.modalService.openSaveErrorModal();
            }
          }, saveError => {
            this.modalService.openSaveErrorModal();
          });
        } else {
          this.bsModalRef.hide();
        }
      });
    } else {
      this.modalService.openWarningModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.appealDetailForm);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  back() {
    this.closeModal(false);
  }

  closeModal (isSearch) {
    this.bsModalRef.hide();
    this.closeAppealModalEvent.emit(isSearch);
  }

  topicChange(topic) {
    if (topic == '4') {
      this.appealDetailForm.get('otherTopic').setValidators(Validators.required);
    } else {
      console.log('topic change: ', this.appealDetailForm.get('stepCode').value);
      this.appealDetailForm.get('otherTopic').setValidators(null);
      if(this.authService.hasAnyPermission(['ROLE_GWS002'])){
        this.appealDetailForm.get('stepCode').setValue('1');
      }
    }

    if (topic == '3') {
      this.appealDetailForm.get('transfer').setValue(false);
      this.appealDetailForm.get('transferOrgName').setValue(this.ltoTransferOrg.orgName);
      this.appealDetailForm.get('transferOrgId').setValue(this.ltoTransferOrg.orgId);

      if(!this.authService.hasAnyPermission(['ROLE_GWS005'])){
        this.appealDetailForm.get('stepCode').setValue('1');
      }
    } else {
      this.appealDetailForm.get('transferOrgName').setValue(this.defaultTransferOrg.orgName);
      this.appealDetailForm.get('transferOrgId').setValue(this.defaultTransferOrg.orgId);
    }

    this.appealDetailForm.get('otherTopic').updateValueAndValidity();
    // this.appealDetailForm.get('otherTopic').enable();
  }

  downloadAttachFile(row, i) {
    let formData = new FormData();
    formData.append('fileName', row.attachFile);
    formData.append('appealDetailId', row.appealDetailId);

    const formArray = this.appealDetailForm.get('attachFileDtoListForm') as FormArray;
    const file = formArray.getRawValue()[i].file;
    if (file != null && file !== undefined) {
      this.welfareDownloadFile.downloadFileFromCurrentFile(file);
    } else {
      this.welfareAppealService.downloadAttachFile(formData).subscribe((res: any) => {
        if (res.status === 200) {
          //download file
          this.welfareDownloadFile.downloadFileIe(res.data);
        } else {
          this.modalService.openDownloadErrorModal();
        }
      }, error => {
        this.modalService.openDownloadErrorModal();
      });
    }

  }

  get appealTopicCode(): AbstractControl { return this.appealDetailForm.get('appealTopicCode'); }
  get appealTopicDesc(): AbstractControl { return this.appealDetailForm.get('appealTopicDesc'); }
  get otherTopic(): AbstractControl { return this.appealDetailForm.get('otherTopic'); }
  get appealNo(): AbstractControl { return this.appealDetailForm.get('appealNo'); }
  get remark(): AbstractControl { return this.appealDetailForm.get('remark'); }
  get registerOrgId(): AbstractControl { return this.appealDetailForm.get('registerOrgId'); }
  get transferOrgId(): AbstractControl { return this.appealDetailForm.get('transferOrgId'); }
  get stepCode(): AbstractControl { return this.appealDetailForm.get('stepCode'); }
  get transfer(): AbstractControl { return this.appealDetailForm.get('transfer'); }
  get transferOrgName(): AbstractControl { return this.appealDetailForm.get('transferOrgName'); }
  get registerOrgName(): AbstractControl { return this.appealDetailForm.get('registerOrgName'); }
  get dataFlag(): AbstractControl { return this.appealDetailForm.get('dataFlag'); }
  get checkDisabled(): AbstractControl { return this.appealDetailForm.get('checkDisabled'); }
}
