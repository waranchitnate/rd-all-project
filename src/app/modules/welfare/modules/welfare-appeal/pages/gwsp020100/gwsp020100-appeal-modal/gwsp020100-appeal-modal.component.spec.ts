import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp020100AppealModalComponent } from './gwsp020100-appeal-modal.component';

describe('Gwsp020100AppealModalComponent', () => {
  let component: Gwsp020100AppealModalComponent;
  let fixture: ComponentFixture<Gwsp020100AppealModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp020100AppealModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp020100AppealModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
