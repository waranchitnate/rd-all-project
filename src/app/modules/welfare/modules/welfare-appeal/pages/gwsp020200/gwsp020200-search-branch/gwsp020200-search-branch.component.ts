import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { WelfareAppealProgress } from '../../../models/welfare-appeal-progress.model';
import { ActivatedRoute} from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { WelfareAppealService } from '../../../services/welfare-appeal.service';
import { Gwsp020200SharedDataService } from '../../../services/gwsp020200-shared-data.service';

@Component({
  selector: 'app-gwsp020200-search-branch',
  templateUrl: './gwsp020200-search-branch.component.html',
  styleUrls: ['./gwsp020200-search-branch.component.css']
})
export class Gwsp020200SearchBranchComponent implements OnInit {

  private rootModuleUrl;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  appealProgress = new WelfareAppealProgress();
  constructor(private fb: FormBuilder, private welfareAppealService: WelfareAppealService
    , private dataService: Gwsp020200SharedDataService, private activatedRoute: ActivatedRoute
    , private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.rootModuleUrl = this.activatedRoute.snapshot.parent.routeConfig.path;
    this.setParentStepAndPath(this.activatedRoute);
    this.appealProgress = this.dataService.getCurrentAppealProgress();
    console.log('branch ', this.appealProgress);
    this.getData();
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  getData() {
    const condition = this.appealProgress;
    condition.pageRequestDto = this.pageRequest;
    this.welfareAppealService.searchProgressBranch(condition).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  back() {
    this.dataService.backToAreaFromBranch();
  }

  goToViewPerson(row) {
    this.dataService.goToPersonFromBranch(row);
  }

  setParentStepAndPath(activatedRoute: ActivatedRoute) {
    activatedRoute.data.subscribe(data => {
      this.dataService.setParentStepAndPath((data['prevUrl'] == null ? null : 'gws' + '/'  + this.rootModuleUrl + '/' + 'gwsp020200/'  + data['prevUrl']),
      (data['nextUrl'] == null ? null : 'gws' + '/'  + this.rootModuleUrl + '/' + 'gwsp020200/'  + data['nextUrl']));
    });

    this.changeDetectorRef.detectChanges();
  }

}
