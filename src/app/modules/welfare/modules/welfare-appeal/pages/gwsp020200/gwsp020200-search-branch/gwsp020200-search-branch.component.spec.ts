import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp020200SearchBranchComponent } from './gwsp020200-search-branch.component';

describe('Gwsp020200SearchBranchComponent', () => {
  let component: Gwsp020200SearchBranchComponent;
  let fixture: ComponentFixture<Gwsp020200SearchBranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp020200SearchBranchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp020200SearchBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
