import { Gwsp020200SearchComponent } from './../gwsp020200-search/gwsp020200-search.component';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { WelfareAppealService } from '../../../services/welfare-appeal.service';
import { Gwsp020200SharedDataService } from '../../../services/gwsp020200-shared-data.service';
import { WelfareAppealProgress } from '../../../models/welfare-appeal-progress.model';

@Component({
  selector: 'app-gwsp020200-search-area',
  templateUrl: './gwsp020200-search-area.component.html',
  styleUrls: ['./gwsp020200-search-area.component.css']
})
export class Gwsp020200SearchAreaComponent implements OnInit {

  private rootModuleUrl;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  appealProgress = new WelfareAppealProgress();

  constructor(private fb: FormBuilder, private welfareAppealService: WelfareAppealService, private activatedRoute: ActivatedRoute
    , private changeDetectorRef: ChangeDetectorRef, private dataService: Gwsp020200SharedDataService
   ) { }
  ngOnInit() {
    this.rootModuleUrl = this.activatedRoute.snapshot.parent.routeConfig.path;
    this.setParentStepAndPath(this.activatedRoute);
    this.appealProgress = this.dataService.getCurrentAppealProgress();
    this.getData();
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  getData() {
    const condition = this.appealProgress;
    condition.pageRequestDto = this.pageRequest;
    this.welfareAppealService.searchProgressByArea(condition).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  back() {
    this.dataService.backToSectorFromArea();
  }

  goToViewBranch(row) {
    this.dataService.goToBranchFromArea(row);
  }

  setParentStepAndPath(activatedRoute: ActivatedRoute) {
    activatedRoute.data.subscribe(data => {
      this.dataService.setParentStepAndPath((data['prevUrl'] == null ? null : 'gws' + '/'  + this.rootModuleUrl + '/' + 'gwsp020200/search'  + data['prevUrl']),
      (data['nextUrl'] == null ? null : 'gws' + '/'  + this.rootModuleUrl + '/' + 'gwsp020200/'  + data['nextUrl']));
    });

    this.changeDetectorRef.detectChanges();
  }
}
