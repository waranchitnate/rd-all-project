import { WelfareAppealService } from './../../../services/welfare-appeal.service';
import { AppealOrganizationTypeAheadComponent } from './../../../../../shared/appeal-organization-type-ahead/appeal-organization-type-ahead.component';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { Gwsp020200SharedDataService } from '../../../services/gwsp020200-shared-data.service';
import { WelfareAppealProgress } from '../../../models/welfare-appeal-progress.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp020200-search',
  templateUrl: './gwsp020200-search.component.html',
  styleUrls: ['./gwsp020200-search.component.css']
})
export class Gwsp020200SearchComponent implements OnInit {

  private rootModuleUrl;

  taxYearList: Array<any>;
  welfareAppealOrganizationList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];
  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  searchCondition = this.fb.group({
    orgId: [''],
    orgName: [''],
    taxYear: [''],
    search: [true]
  });

  appealProgress = new WelfareAppealProgress();

  @ViewChild(AppealOrganizationTypeAheadComponent) appealOrganizationTypeAheadComponent: AppealOrganizationTypeAheadComponent;
  constructor(private fb: FormBuilder, private welfareAppealService: WelfareAppealService
    , private welfareCommonService: WelfareCommonService, private activatedRoute: ActivatedRoute
    , private dataService: Gwsp020200SharedDataService, private changeDetectorRef: ChangeDetectorRef, public authService: AuthenticationService) { }

  ngOnInit() {
    this.setDefaultSort();
    this.rootModuleUrl = this.activatedRoute.snapshot.parent.routeConfig.path;
    if (this.dataService.getCurrentAppealProgress() != null && this.dataService.getCurrentAppealProgress() != undefined) {
      this.searchCondition.patchValue(this.dataService.getCurrentAppealProgress());
      this.dataService.clearAppealProgress();
    }
    this.setParentStepAndPath(this.activatedRoute);
    this.getTaxyearList();
    this.getData();
  }

  getTaxyearList() {
    this.welfareCommonService.getTaxYearList().subscribe((result: any) => {
      this.taxYearList = result.data;
      },
      err => console.log('timeout'),
    );
  }

  setDefaultSort() {
    this.pageRequest.sortFieldName = 'TAX_YEAR';
    this.pageRequest.sortDirection = 'ASC';
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.setDefaultSort();
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  getData() {

    const condition = this.searchCondition.getRawValue();
    condition.pageRequestDto = this.pageRequest;
    this.welfareAppealService.searchProgressBySector(condition).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  setValue(val) {
    if (val != null) {
      this.searchCondition.get('orgId').setValue(val.orgId);
      this.searchCondition.get('orgName').setValue(val.orgName);
    } else {
      this.searchCondition.get('orgId').setValue(null);
      this.searchCondition.get('orgName').setValue(null);
    }
  }

  clear() {
    this.searchCondition.reset();
    this.searchCondition.get('taxYear').setValue('');
    this.searchCondition.get('orgId').setValue(null);
    this.getData();
  }

  goToViewArea(row) {
    this.appealProgress = this.searchCondition.getRawValue();
    this.dataService.setAppealProgress(this.searchCondition.getRawValue());
    this.dataService.goToAreaFromSector(row);
    // , this.searchCondition.get('orgId').value , this.searchCondition.get('taxYear').value
  }

  setParentStepAndPath(activatedRoute: ActivatedRoute) {
    activatedRoute.data.subscribe(data => {
      this.dataService.setParentStepAndPath((data['prevUrl'] == null ? null : 'gws' + '/'  + this.rootModuleUrl + '/' + 'gwsp020200'  + data['prevUrl']),
      (data['nextUrl'] == null ? null : 'gws' + '/'  + this.rootModuleUrl + '/' + 'gwsp020200/'  + data['nextUrl']));
    });

    this.changeDetectorRef.detectChanges();
  }

}
