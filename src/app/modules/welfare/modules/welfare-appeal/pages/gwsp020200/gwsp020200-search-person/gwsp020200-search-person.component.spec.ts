import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp020200SearchPersonComponent } from './gwsp020200-search-person.component';

describe('Gwsp020200SearchPersonComponent', () => {
  let component: Gwsp020200SearchPersonComponent;
  let fixture: ComponentFixture<Gwsp020200SearchPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp020200SearchPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp020200SearchPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
