import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp020200SearchComponent } from './gwsp020200-search.component';

describe('Gwsp020200SearchComponent', () => {
  let component: Gwsp020200SearchComponent;
  let fixture: ComponentFixture<Gwsp020200SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp020200SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp020200SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
