import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp020200SearchAreaComponent } from './gwsp020200-search-area.component';

describe('Gwsp020200SearchAreaComponent', () => {
  let component: Gwsp020200SearchAreaComponent;
  let fixture: ComponentFixture<Gwsp020200SearchAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp020200SearchAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp020200SearchAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
