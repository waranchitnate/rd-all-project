import { WelfareModalService } from './../../../../../services/welfare-modal.service';
import { WelfareAppealService } from './../../../services/welfare-appeal.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';
import { WelfareDownloadServiceService } from 'src/app/modules/welfare/services/welfare-download-service.service';
import { AppealOrganizationLevelTypeAheadComponent } from 'src/app/modules/welfare/shared/appeal-organization-level-type-ahead/appeal-organization-level-type-ahead.component';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-gwsp020300-report',
  templateUrl: './gwsp020300-report.component.html',
  styleUrls: ['./gwsp020300-report.component.css']
})
export class Gwsp020300ReportComponent implements OnInit {
  taxYearList: Array<any>;
  searchCondition:  FormGroup;
  @ViewChild('sector') sector: AppealOrganizationLevelTypeAheadComponent;
  @ViewChild('area') area: AppealOrganizationLevelTypeAheadComponent;
  @ViewChild('branch') branch: AppealOrganizationLevelTypeAheadComponent;

  constructor(private fb: FormBuilder, private welfareCommonService: WelfareCommonService, private welfareDownloadFile: WelfareDownloadServiceService
    , private welfareAppealService: WelfareAppealService , private modalService: WelfareModalService
    , public authService: AuthenticationService) { }

  ngOnInit() {
    this.searchCondition = this.initialSearchCondition();
    this.welfareCommonService.getTaxYearList().subscribe((res: any) => {
      this.taxYearList = res.data;
      this.searchCondition = this.initialSearchCondition();
    });
  }



  initialSearchCondition(): FormGroup {
    return new FormGroup({
      taxYear: new FormControl('', Validators.required),
      sectorOrgId: new FormControl(null),
      areaOrgId: new FormControl(null),
      branchOrgId: new FormControl(null),
      sectorOrgName: new FormControl(null),
      areaOrgName: new FormControl(null),
      branchOrgName: new FormControl(null),
      orgId: new FormControl(null),
    });
  }

  clear() {
    this.searchCondition.reset();
    this.searchCondition.get('taxYear').setValue('');
    this.searchCondition.get('sectorOrgId').setValue(null);
    this.searchCondition.get('areaOrgId').setValue(null);
    this.searchCondition.get('branchOrgId').setValue(null);
    this.area.clearCondition();
    this.sector.clearCondition();
    this.branch.clearCondition();
  }

  print() {
    const condition = this.searchCondition.getRawValue();
    if (this.searchCondition.valid) {
      this.welfareAppealService.printGwsR020301Report(condition).subscribe((res: any) => {
        if (res.status === 200) {
          this.welfareDownloadFile.downloadFileIe(res.data);
        } else {
          this.modalService.openErrorModal(WelfareConstant.message.reportError);
        }
      }, () => {
        this.modalService.openErrorModal(WelfareConstant.message.reportError);
      });
    } else {
      this.modalService.openErrorModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.searchCondition);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });

    // (<any>Object).values(formGroup.controls).forEach(control => {
    //   if (control.controls) { // control is a FormGroup
    //     this.markFormGroupTouched(control);
    //   } else { // control is a FormControl
    //     control.markAsTouched();
    //   }
    // });
  }

  setSector(val){
    if (val != null) {
      this.searchCondition.get('orgId').setValue(val.orgId );
      this.searchCondition.get('sectorOrgId').setValue(val.orgId );
      this.searchCondition.get('sectorOrgName').setValue(val.orgName );
    } else {
      this.searchCondition.get('orgId').setValue(null);
      this.searchCondition.get('sectorOrgId').setValue(null);
      this.searchCondition.get('sectorOrgName').setValue(null);
    }

    this.searchCondition.get('areaOrgId').setValue(null);
    this.searchCondition.get('branchOrgId').setValue(null);
    this.searchCondition.get('areaOrgName').setValue(null);
    this.searchCondition.get('branchOrgName').setValue(null);
  }

  setArea(val) {
    if (val != null) {
      this.searchCondition.get('orgId').setValue(val.orgId );
      this.searchCondition.get('areaOrgId').setValue(val.orgId );
      this.searchCondition.get('areaOrgName').setValue(val.orgName );
    } else {
      this.searchCondition.get('orgId').setValue(null);
      this.searchCondition.get('areaOrgId').setValue(null);
      this.searchCondition.get('areaOrgName').setValue(null);
    }
    this.searchCondition.get('branchOrgId').setValue(null);
    this.searchCondition.get('branchOrgName').setValue(null);
  }

  setBranch(val) {
    if (val != null) {
      this.searchCondition.get('orgId').setValue(val.orgId );
      this.searchCondition.get('branchOrgId').setValue(val.orgId );
      this.searchCondition.get('branchOrgName').setValue(val.orgName );
    } else {
      this.searchCondition.get('orgId').setValue(null);
      this.searchCondition.get('branchOrgId').setValue(null);
      this.searchCondition.get('branchOrgName').setValue(null);
    }
  }
  get taxYearControl(): AbstractControl { return this.searchCondition.get('taxYear'); }
  get sectorOrgIdControl(): AbstractControl { return this.searchCondition.get('sectorOrgId'); }
  get areaOrgIdControl(): AbstractControl { return this.searchCondition.get('areaOrgId'); }
  get branchOrgIdControl(): AbstractControl { return this.searchCondition.get('branchOrgId'); }
}
