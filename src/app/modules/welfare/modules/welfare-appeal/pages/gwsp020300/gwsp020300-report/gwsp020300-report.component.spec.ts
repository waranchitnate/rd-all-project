import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp020300ReportComponent } from './gwsp020300-report.component';

describe('Gwsp020300ReportComponent', () => {
  let component: Gwsp020300ReportComponent;
  let fixture: ComponentFixture<Gwsp020300ReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp020300ReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp020300ReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
