import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { WelfareRegistrant } from '../../welfare-registration/models/welfare-registrant.model';

@Injectable({
  providedIn: 'root'
})
export class WelfareWebsiteService {

  constructor(private http: HttpClient) { }

  internalWelfareManagementApiUrl = environment.internalWelfareManagementApiUrl;
  welfareWebsiteEndpointUrl = 'gws/website';

  public getRegistrationResult(registrant: WelfareRegistrant): Observable<any> {
    return this.http.post(this.internalWelfareManagementApiUrl + this.welfareWebsiteEndpointUrl + '/getRegistrationResult', registrant);
  }
}
