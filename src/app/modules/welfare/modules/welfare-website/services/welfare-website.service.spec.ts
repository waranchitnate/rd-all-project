import { TestBed } from '@angular/core/testing';

import { WelfareWebsiteService } from './welfare-website.service';

describe('WelfareWebsiteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WelfareWebsiteService = TestBed.get(WelfareWebsiteService);
    expect(service).toBeTruthy();
  });
});
