import { WelfareRegistrant } from './../../../welfare-registration/models/welfare-registrant.model';
import { Component, OnInit } from '@angular/core';
import { WelfareWebsiteService } from '../../services/welfare-website.service';
import { FormBuilder, Validators, AbstractControl, FormGroup, FormControl } from '@angular/forms';
import { WelfareCommonService } from 'src/app/modules/welfare/services/welfare-common.service';
import { WelfareModalService } from 'src/app/modules/welfare/services/welfare-modal.service';
import { WelfareConstant } from 'src/app/modules/welfare/welfare-constant';

@Component({
  selector: 'app-gwsp040100-search',
  templateUrl: './gwsp040100-search.component.html',
  styleUrls: ['./gwsp040100-search.component.css']
})
export class Gwsp040100SearchComponent implements OnInit {
  taxYearList: Array<any>;
  registrant: WelfareRegistrant;
  searchCondition:  FormGroup;
  isSearch: boolean;

  constructor(private fb: FormBuilder, private welfareWebsiteService: WelfareWebsiteService
    , private welfareCommonService: WelfareCommonService , private modalService: WelfareModalService) { }

  ngOnInit() {
    this.searchCondition = this.initialSearchCondition();
    this.welfareCommonService.getTaxYearList().subscribe((res: any) => {
      this.taxYearList = res.data;
      this.searchCondition = this.initialSearchCondition();
    });
  }

  initialSearchCondition(): FormGroup {
    return new FormGroup({
      nid: new FormControl(null, Validators.required),
      taxYear: new FormControl('', Validators.required),
      birthDate: new FormControl(null, Validators.required),
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required)
    });
  }

  clear() {
    this.isSearch = false;
    this.searchCondition.reset();
    this.searchCondition.get('taxYear').setValue('');
  }

  getData() {
    const condition = this.searchCondition.getRawValue();
    if (this.searchCondition.valid) {
      this.welfareWebsiteService.getRegistrationResult(condition).subscribe(
        (res: any) => {
          this.registrant = res.data;
          this.isSearch = true;
        });
    } else {
      this.isSearch = false;
      this.modalService.openWarningModal(WelfareConstant.message.requirementFeild);
      this.markFormGroupTouched(this.searchCondition);
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  get nidControl(): AbstractControl { return this.searchCondition.get('nid'); }
  get taxYearControl(): AbstractControl { return this.searchCondition.get('taxYear'); }
  get birthDateControl(): AbstractControl { return this.searchCondition.get('birthDate'); }
  get firstNameControl(): AbstractControl { return this.searchCondition.get('firstName'); }
  get lastNameControl(): AbstractControl { return this.searchCondition.get('lastName'); }
}
