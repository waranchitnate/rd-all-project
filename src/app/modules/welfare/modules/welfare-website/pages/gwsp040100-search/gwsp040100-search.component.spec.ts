import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Gwsp040100SearchComponent } from './gwsp040100-search.component';

describe('Gwsp040100SearchComponent', () => {
  let component: Gwsp040100SearchComponent;
  let fixture: ComponentFixture<Gwsp040100SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gwsp040100SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gwsp040100SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
