import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelfareWebsiteRoutingModule } from './welfare-website-routing.module';
import { Gwsp040100SearchComponent } from './pages/gwsp040100-search/gwsp040100-search.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { WelfareSharedModule } from '../../shared/welfare-shared.module';

@NgModule({
  declarations: [Gwsp040100SearchComponent],
  imports: [
    CommonModule,
    WelfareWebsiteRoutingModule,
    SharedModule,
    WelfareSharedModule
  ]
})
export class WelfareWebsiteModule { }
