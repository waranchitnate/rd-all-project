import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Gwsp040100SearchComponent } from './pages/gwsp040100-search/gwsp040100-search.component';

const routes: Routes = [
  {path: 'gwsp040100/search', component: Gwsp040100SearchComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelfareWebsiteRoutingModule { }
