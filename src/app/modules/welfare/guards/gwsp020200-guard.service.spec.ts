import { TestBed } from '@angular/core/testing';

import { Gwsp020200GuardService } from './gwsp020200-guard.service';

describe('Gwsp020200GuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Gwsp020200GuardService = TestBed.get(Gwsp020200GuardService);
    expect(service).toBeTruthy();
  });
});
