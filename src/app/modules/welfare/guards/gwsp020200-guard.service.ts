import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree, CanActivate} from "@angular/router";
import {Observable} from "rxjs";
import { Gwsp020200SharedDataService } from '../modules/welfare-appeal/services/gwsp020200-shared-data.service';

@Injectable({
  providedIn: 'root'
})
export class Gwsp020200GuardService implements CanActivate  {

  constructor(private router: Router, private dataService: Gwsp020200SharedDataService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const refUrl = this.router.routerState.snapshot.url;
    const currentUrl = route.url.toString();

    if (currentUrl.indexOf('area') !== -1 && (refUrl.indexOf('search') !== -1  || refUrl.indexOf('branch') !== -1 )) {
      return true;
    } else if (currentUrl.indexOf('branch') !== -1 && (refUrl.indexOf('area') !== -1  || refUrl.indexOf('person') !== -1 )) {
      return true;
    } else if (currentUrl.indexOf('person') !== -1 && (refUrl.indexOf('branch') !== -1  || refUrl.indexOf('gwsp020100/search') !== -1 )) {
      return true;
    } else if (currentUrl.indexOf('search') !== -1 && (refUrl.indexOf('area') !== -1)) {
      return true;
    } else if ( currentUrl.indexOf('search') !== -1) {
      this.dataService.clearAppealProgress();
      return true;
    }
      this.dataService.clearAppealProgress();
      this.router.navigate(['/gws/appeal/gwsp020200/search']);
      return false;
  }
}
