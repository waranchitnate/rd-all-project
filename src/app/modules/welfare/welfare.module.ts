import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelfareRoutingModule } from './welfare-routing.module';
import { WelfareConfigModule } from './modules/welfare-config/welfare-config.module';
import { WelfareAppealModule } from './modules/welfare-appeal/welfare-appeal.module';
import { WelfareRegistrationModule } from './modules/welfare-registration/welfare-registration.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    WelfareRoutingModule,
    WelfareConfigModule,
    WelfareAppealModule,
    WelfareRegistrationModule,
    RouterModule,
    SharedModule
  ]
})
export class WelfareModule { }
