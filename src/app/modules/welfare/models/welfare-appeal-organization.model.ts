import { PageRequest } from './../../../shared/models/page-request';
export class WelfareAppealOrganization {
  orgId: number;
  parentId: number;
  orgName: string;
  orgCode: string;
  level: number;
  organizationTypeName: string;
  dataFlag: any;
  dataStatus: any;
  statusDesc: string;
  search: boolean;
  pageRequestDto: PageRequest;
  aesOrgId: string;
}
