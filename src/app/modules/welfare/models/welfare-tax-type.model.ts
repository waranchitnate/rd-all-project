export class WelfareTaxType {
  taxTypeCode: string;
  workSystemId: number;
  workSystemName: string;
  taxTypeDesc: string;
  fileCode: string;
  dataFlag: string;
  taxTypeId: number;
  aesTaxTypeId: string;
}
