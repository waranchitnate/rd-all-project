import { WelfareWorkSystemEmail } from './welfare-work-system-email.model';

export class WelfareWorkSystem {
    workSystemId: number;
    workSystemName: string;
    workSystemAbbr: string;
    welfareWorkSystemEmailList: Array<WelfareWorkSystemEmail>;
    email1: string;
    email2: string;
    email3: string;
    email4: string;
    email5: string;
}