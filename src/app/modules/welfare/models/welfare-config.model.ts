export class WelfareConfig {
  configId: number;
  year: string;
  income: number;
  startDate: any;
  endDate: any;
  process: Boolean;
  aesConfigId: string;
  canEditMoney: Boolean;
}
