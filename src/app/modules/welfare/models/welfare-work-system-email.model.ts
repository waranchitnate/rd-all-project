export class WelfareWorkSystemEmail {
    workSystemId: number;
    workSystemEmailId: number;
    email: string;
}