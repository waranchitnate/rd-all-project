import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrganizationTypeAheadComponent } from './organization-type-ahead/organization-type-ahead.component';
import { AppealOrganizationTypeAheadComponent } from './appeal-organization-type-ahead/appeal-organization-type-ahead.component';
import { AppealOrganizationLevelTypeAheadComponent } from './appeal-organization-level-type-ahead/appeal-organization-level-type-ahead.component';

@NgModule({
  declarations: [OrganizationTypeAheadComponent
    , AppealOrganizationTypeAheadComponent
    , AppealOrganizationLevelTypeAheadComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    OrganizationTypeAheadComponent
    , AppealOrganizationTypeAheadComponent
    , AppealOrganizationLevelTypeAheadComponent
  ]
})
export class WelfareSharedModule { }
