import { Component, OnInit, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { WelfareAppealOrganization } from '../../models/welfare-appeal-organization.model';
import { WelfareConfigService } from '../../modules/welfare-config/welfare-config.service';
import { ControlContainer, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { PageRequest } from 'src/app/shared/models/page-request';

@Component({
  selector: 'app-appeal-organization-type-ahead',
  templateUrl: './appeal-organization-type-ahead.component.html',
  styleUrls: ['./appeal-organization-type-ahead.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AppealOrganizationTypeAheadComponent),
      multi: true
    }
  ]
})
export class AppealOrganizationTypeAheadComponent implements OnInit, ControlValueAccessor {

  asyncSelected: string;
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;
  dataSource: Observable<any>;
  disabled: boolean = false;
  @Input() isRequired;
  @Output() selectedOrgEvent = new EventEmitter();
  @Input() borderDanger: boolean;
  orgName = new BehaviorSubject<any>(null);
  wordRequired = new BehaviorSubject<any>(null);
  isDisabled = new BehaviorSubject<boolean>(false);
  isSearch = new BehaviorSubject<boolean>(false);
  condition = new WelfareAppealOrganization();
  pageRequest = new PageRequest();
  onChange() {}
  onTouched() {}

  writeValue(obj: any): void {
    this.asyncSelected = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onTouch() {
    this.onTouched();
  }

  @Input('input-search')
  set inputSearch(value) {
    this.isSearch.next(value);
  }

  get inputSearch() {
    return this.isSearch.getValue();
  }

  @Input('input-org')
  set inputOrg(value) {
    this.orgName.next(value);
  }

  get inputOrg() {
    return this.orgName.getValue();
  }

  @Input('input-phd')
  set inputPhd(value) {
    this.wordRequired.next(value);
  }

  get inputPhd() {
    return this.wordRequired.getValue();
  }

  @Input('input-disabled')
  set inputDisabled(value) {
    this.isDisabled.next(value);
  }

  get inputDisabled() {
    return this.isDisabled.getValue();
  }


  constructor(public controlContainer: ControlContainer, private welfareConfigService: WelfareConfigService) {
  }

  createDataSource() {
    this.dataSource = Observable.create((observer: any) => {
      this.condition.orgName = this.asyncSelected;
      this.condition.search = this.isSearch.value;
      this.condition.level = 2;
      this.condition.pageRequestDto = this.pageRequest;
      if (this.asyncSelected == null || this.asyncSelected === undefined || this.asyncSelected === '') {
        this.selectedOrgEvent.next(null);
      }
        this.welfareConfigService.getAppealOrganizationListByCondition(this.condition).subscribe((result: any) => {
          observer.next(result.data.content);
        });
    });
  }

  ngOnInit() {
    this.orgName.subscribe(data => {
      this.asyncSelected = data;
      this.createDataSource();
    });
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.selectedOrgEvent.next(e.item);
  }

  clearCondition() {
    this.orgName.next(null);
    this.condition.orgName = null;
    this.condition.search = this.isSearch.value;
  }
}
