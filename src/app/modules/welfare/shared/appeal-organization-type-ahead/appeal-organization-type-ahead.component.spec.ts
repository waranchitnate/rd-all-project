import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppealOrganizationTypeAheadComponent } from './appeal-organization-type-ahead.component';

describe('AppealOrganizationTypeAheadComponent', () => {
  let component: AppealOrganizationTypeAheadComponent;
  let fixture: ComponentFixture<AppealOrganizationTypeAheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppealOrganizationTypeAheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppealOrganizationTypeAheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
