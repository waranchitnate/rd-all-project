import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationTypeAheadComponent } from './organization-type-ahead.component';

describe('OrganizationTypeAheadComponent', () => {
  let component: OrganizationTypeAheadComponent;
  let fixture: ComponentFixture<OrganizationTypeAheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationTypeAheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationTypeAheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
