import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { WelfareAppealOrganization } from '../../models/welfare-appeal-organization.model';
import { ControlContainer } from '@angular/forms';
import { WelfareConfigService } from '../../modules/welfare-config/welfare-config.service';
import { TypeaheadMatch } from 'ngx-bootstrap';

@Component({
  selector: 'app-appeal-organization-level-type-ahead',
  templateUrl: './appeal-organization-level-type-ahead.component.html',
  styleUrls: ['./appeal-organization-level-type-ahead.component.css']
})
export class AppealOrganizationLevelTypeAheadComponent implements OnInit {

  @Input() isRequired;
  @Input() borderDanger: boolean;
  @Output() selectedOrgEvent = new EventEmitter();
  orgName = new BehaviorSubject<any>(null);
  wordRequired = new BehaviorSubject<any>(null);
  isDisabled = new BehaviorSubject<boolean>(false);
  isSearch = new BehaviorSubject<boolean>(false);
  condition = new WelfareAppealOrganization();
  inputLevel = new BehaviorSubject<any>(null);
  parentOrgId = new BehaviorSubject<any>(null);
  orgId = new BehaviorSubject<any>(null);

  // @ViewChild(AppealOrganizationLevelTypeAheadComponent) appealOrganizationLevelTypeAheadComponent: AppealOrganizationLevelTypeAheadComponent;
  @Input('input-search')
  set inputSearch(value) {
    this.isSearch.next(value);
  }

  get inputSearch() {
    return this.isSearch.getValue();
  }

  @Input('input-org')
  set inputOrg(value) {
    this.orgName.next(value);
  }

  get inputOrg() {
    return this.orgName.getValue();
  }

  @Input('input-phd')
  set inputPhd(value) {
    this.wordRequired.next(value);
  }

  get inputPhd() {
    return this.wordRequired.getValue();
  }

  @Input('input-disabled')
  set inputDisabled(value) {
    this.isDisabled.next(value);
  }

  get inputDisabled() {
    return this.isDisabled.getValue();
  }

  @Input('input-level')
  set level(value) {
    this.inputLevel.next(value);
  }

  get level() {
    return this.inputLevel.getValue();
  }

  @Input('input-parent')
  set parent(value) {
    this.parentOrgId.next(value);
  }

  get parent() {
    return this.parentOrgId.getValue();
  }

  @Input('input-selforg')
  set selfOrg(value) {
    this.orgId.next(value);
  }

  get selfOrg() {
    return this.orgId.getValue();
  }

  asyncSelected: string;
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;
  dataSource: Observable<any>;

  constructor(public controlContainer: ControlContainer, private welfareConfigService: WelfareConfigService) {
  }

  createDataSource() {
    this.dataSource = Observable.create((observer: any) => {
      this.condition.orgName = this.asyncSelected;
      this.condition.search = this.isSearch.value;
      this.condition.parentId = this.parentOrgId.value;
      this.condition.level = this.inputLevel.value;
      this.condition.orgId = this.orgId.value;

      if (this.asyncSelected == null || this.asyncSelected === undefined || this.asyncSelected === '') {
        console.log('this.asyncSelected: ', this.asyncSelected );
        this.selectedOrgEvent.next(null);
      }
        this.welfareConfigService.getAppealOrganizationList(this.condition).subscribe((result: any) => {
          observer.next(result.data);
        });
    });
  }

  ngOnInit() {
    this.orgName.subscribe(data => {
      this.asyncSelected = data;
      this.createDataSource();
    });
  }

  changeTypeaheadLoading(e: boolean): void {
    console.log('changeTypeaheadLoading ', e );
    if (!e) {
      this.selectedOrgEvent.next(null);
    }
    this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    console.log('typeaheadOnSelect ', e );
    this.selectedOrgEvent.next(e.item);
  }

  clearCondition() {
    this.orgName.next(null);
    this.condition.orgName = null;
    this.condition.search = this.isSearch.value;
    // this.appealOrganizationLevelTypeAheadComponent.clearCondition();
  }

}
