import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppealOrganizationLevelTypeAheadComponent } from './appeal-organization-level-type-ahead.component';

describe('AppealOrganizationLevelTypeAheadComponent', () => {
  let component: AppealOrganizationLevelTypeAheadComponent;
  let fixture: ComponentFixture<AppealOrganizationLevelTypeAheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppealOrganizationLevelTypeAheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppealOrganizationLevelTypeAheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
