import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserHomeComponent } from './user-home/user-home.component';
import { AuthGuard } from '../../core/guard/auth.guard';

const routes: Routes = [
  { path: '', component: UserHomeComponent, canActivate: [AuthGuard], data: {requestPermission: ['PERMISSION_USER.VIEW']}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
