import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { ResponseT } from "../../../shared/models/response.model";
import { PageResponse } from "../../../shared/models/page-response";
import { FormGroup } from "@angular/forms";
import { VUserXmlTemplate } from "../models/vuser-xml-template.model";
import { XmlTemplateDetail } from "../models/xml-template-detail.model";
import { PdfTemplateDetail } from "../models/pdf-template-detail.model";
import { RdbOffice } from "../models/misc.model";
import { MPdfTemplatestatus } from "../models/m-pdf-template-status.model";
import { DomSanitizer } from "@angular/platform-browser";
import { PdfTemplateAttachment } from "../models/pdf-resource-attachment.model";

@Injectable({
  providedIn: "root"
})
export class AdminTemplateManagementService {
  url = environment.pdfUtilityUrl + "admin/template";
  commonUrl = environment.pdfUtilityUrl + "template";

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

  getSearchCriteria(
    vUserXmlTemplate: VUserXmlTemplate
  ): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>(
      this.http.post(this.url + "/search", vUserXmlTemplate)
    );
  }

  getXmlTemplateByXmlTemplateId(
    xmlTemplateId: number
  ): Observable<ResponseT<XmlTemplateDetail>> {
    return <Observable<ResponseT<XmlTemplateDetail>>>(
      this.http.get(
        this.url + "/getXmlTemplateById?xmlTemplateId=" + xmlTemplateId
      )
    );
  }

  getPdfTemplateByXmlTemplateId(
    pdfTemplate: PdfTemplateDetail
  ): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>(
      this.http.post(
        this.url + "/getPdfTemplateListByXmlTemplateId",
        pdfTemplate
      )
    );
  }

  saveXmlTemplate(xmlTemplateDetail: XmlTemplateDetail) {
    return this.http.post<ResponseT<any>>(
      this.url + "/saveXmlTemplate",
      xmlTemplateDetail
    );
  }

  uploadXmlTemplateAttachment(file) {
    return this.http.post(this.url + "/uploadXmlTemplateAttachment", file);
  }

  // uploadPdfTemplateAttachment(file) {
  //   return this.http.post(this.url + "/uploadPdfTemplateAttachment", file);
  // }

  uploadPdfTemplateDemoAttachment(file) {
    return this.http.post(this.url + "/uploadPdfTemplateDemoAttachment", file);
  }

  uploadPdfResourceAttachment(file) {
    return this.http.post(this.url + "/uploadPdfResourceAttachment", file);
  }

  savePdfTemplate(pdfTemplateDetail: PdfTemplateDetail) {
    return this.http.post<ResponseT<any>>(
      this.url + "/savePdfTemplate",
      pdfTemplateDetail
    );
  }

  approvePdfTemplate(pdfTemplateDetail: PdfTemplateDetail) {
    return this.http.post<ResponseT<any>>(
      this.url + "/approvePdfTemplate",
      pdfTemplateDetail
    );
  }

  denyPdfTemplate(pdfTemplateDetail: PdfTemplateDetail) {
    return this.http.post<ResponseT<any>>(
      this.url + "/denyPdfTemplate",
      pdfTemplateDetail
    );
  }

  getPdfTemplateByPdfTemplateId(
    pdfTemplateId: number
  ): Observable<ResponseT<PdfTemplateDetail>> {
    return <Observable<ResponseT<PdfTemplateDetail>>>(
      this.http.get(
        this.url +
        "/getPdfTemplateByPdfTemplateId?pdfTemplateId=" +
        pdfTemplateId
      )
    );
  }

  getOfficeCode(officeCode: string): Observable<RdbOffice> {
    return <Observable<RdbOffice>>this.http.get(
      environment.commonApiUrl + "moi/office/find-by-office-code",
      {
        headers: { skipAuth: '1' },
        params: new HttpParams({ fromObject: { code: officeCode } })
      }
    );
  }

  getAllMPdfTemplateStatus(): Observable<ResponseT<MPdfTemplatestatus[]>> {
    return <Observable<ResponseT<MPdfTemplatestatus[]>>>(
      this.http.get(this.commonUrl + "/findAllMPdfTemplateStatus")
    );
  }

  downloadXmlTemplateAttachment(downloadForm): Observable<any> {
    return this.http.post(
      this.url + "/downloadXmlTemplateAttachment",
      downloadForm
    );
  }

  downloadPdfTemplateAttachment(downloadForm): Observable<any> {
    return this.http.post(
      this.url + "/downloadPdfTemplateAttachment",
      downloadForm
    );
  }

  downloadPdfDemoAttachment(downloadForm): Observable<any> {
    return this.http.post(
      this.url + "/downloadPdfDemoAttachment",
      downloadForm
    );
  }

  convertBase64FileToSafeLink(file) {
    const blob = this.dataURItoBlob(
      file.dataBase64,
      file.contentType ? file.contentType : "application/octet-stream"
    );
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      // IE
      window.navigator.msSaveOrOpenBlob(blob, file.fileBaseName);
      window.navigator.msSaveOrOpenBlob(blob, file.fileBaseName);
    } else {
      //Chrome & Firefox
      const a = document.createElement("a");
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = file.contentType
        ? file.fileBaseName
        : file.fileBaseName + "." + file.fileExtension;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    }

    // const blob = this.dataURItoBlob(file.dataBase64, file.contentType);
    // return this.sanitizer.bypassSecurityTrustResourceUrl(
    //   window.URL.createObjectURL(blob)
    // );
    // let url = URL.createObjectURL(blob);
    // window.open(url);
  }

  openFileOnNewTab(file) {
    const blob = this.dataURItoBlob(
      file.dataBase64,
      file.contentType ? file.contentType : "application/octet-stream"
    );
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      window.URL.createObjectURL(blob)
    );
    let url = URL.createObjectURL(blob);
    window.open(url);
  }

  dataURItoBlob(dataURI, contentType) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: contentType });
    return blob;
  }

  getPdfAttachmentLists(pdfTemplateId): Observable<ResponseT<PdfTemplateAttachment[]>> {
    return <Observable<ResponseT<PdfTemplateAttachment[]>>>(
      this.http.get(this.url + "/getPdfAttachmentListByPdfTemplateId?pdfTemplateId=" +
        pdfTemplateId)
    );
  }

  downloadPdfResourceAttachment(downloadForm): Observable<any> {
    return this.http.post(
      this.url + "/downloadPdfResourceAttachment",
      downloadForm
    );
  }

  cancelPdfTemplate(pdfTemplateDetail: PdfTemplateDetail) {
    return this.http.post<ResponseT<any>>(
      this.url + "/cancelPdfTemplate",
      pdfTemplateDetail
    );
  }
}
