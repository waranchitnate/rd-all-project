import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ResponseT } from "../../../shared/models/response.model";
import { PageResponse } from "../../../shared/models/page-response";
import { FormGroup } from "@angular/forms";
import { VUserXmlTemplate } from "../models/vuser-xml-template.model";

@Injectable({
  providedIn: "root"
})
export class VUserPdfService {
  url = environment.pdfUtilityUrl + "template";

  constructor(private http: HttpClient) {}

  getSearchCriteria(
    vUserXmlTemplate: VUserXmlTemplate
  ): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>(
      this.http.post(this.url + "/search", vUserXmlTemplate)
    );
  }
}
