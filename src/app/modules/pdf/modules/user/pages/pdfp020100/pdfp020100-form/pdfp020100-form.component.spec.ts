import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pdfp020100FormComponent } from './pdfp020100-form.component';

describe('Pdfp020100FormComponent', () => {
  let component: Pdfp020100FormComponent;
  let fixture: ComponentFixture<Pdfp020100FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pdfp020100FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pdfp020100FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
