import { Component, OnInit, ViewChild, HostListener } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { PageResponse } from "src/app/shared/models/page-response";
import { PageRequest } from "src/app/shared/models/page-request";
import {
  NgbModal,
  NgbModalConfig,
  NgbModalRef
} from "@ng-bootstrap/ng-bootstrap";
import { TemplateManagementService } from "src/app/modules/pdf/services/template-management.service";
import { FloatButtonComponent } from "src/app/shared/components/float-button/float-button.component";
import { DropdownMPdfTemplateStatus } from "src/app/modules/pdf/models/vuser-xml-template.model";

@Component({
  selector: "app-pdfp020100-search",
  templateUrl: "./pdfp020100-search.component.html",
  styleUrls: ["./pdfp020100-search.component.css"],
  providers: [NgbModalConfig, NgbModal]
})
export class Pdfp020100SearchComponent implements OnInit {
  @ViewChild(FloatButtonComponent) floatComp;
  xmlTemplateList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  currentSortField = "XML_TEMPLATE_CODE DESC, PDF_TEMPLATE_CODE";
  currentSortDirection = "DESC";
  LIMITS = [
    { key: "10", value: 10 },
    { key: "25", value: 25 },
    { key: "50", value: 50 },
    { key: "100", value: 100 }
  ];
  windowScrolled: boolean;
  modalReference: NgbModalRef;

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  dropdownMPdfTemplateStatus = <DropdownMPdfTemplateStatus[]>[];

  pdfp020100SearchCondition = this.fb.group({
    xmlTemplateCode: [""],
    xmlTemplateName: [""],
    pdfTemplateCode: [""],
    pdfTemplateName: [""],
    pdfTemplateStatusId: [""],
    pdfTemplateStatusName: [""]
  });

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private vuserpdfService: TemplateManagementService
  ) { }

  ngOnInit() {
    this.floatComp.urlAdd = "/pdf/user/pdfp020100/add-xml";
    this.floatComp.tooltipMsg = "ลงทะเบียนขอใช้ PDF";

    this.pageRequest.sortFieldName = this.currentSortField;
    this.pageRequest.sortDirection = this.currentSortDirection;
    this.getData();
    this.getAllMPdfTemplateStatus();
  }

  clear() {
    this.pdfp020100SearchCondition.reset();
    this.pdfp020100SearchCondition.get("xmlTemplateCode").setValue("");
    this.pdfp020100SearchCondition.get("pdfTemplateStatusId").setValue("");
    this.getData();
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = this.currentSortField;
    this.pageRequest.sortDirection = this.currentSortDirection;
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  sort(fieldName) {
    this.currentSortField = fieldName;
    if (this.pageRequest.sortFieldName === this.currentSortField) {
      this.currentSortDirection =
        this.pageRequest.sortDirection === "ASC" ? "DESC" : "ASC";
      this.pageRequest.sortDirection = this.currentSortDirection;
    } else {
      this.pageRequest.sortFieldName = this.currentSortField;
      this.currentSortDirection = "ASC";
      this.pageRequest.sortDirection = "ASC";
    }
    this.getData();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  getData() {
    const criteria = this.pdfp020100SearchCondition.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.vuserpdfService.getSearchCriteria(criteria).subscribe((res: any) => {
      this.pageResponse = res.data;
      this.pageResponse.totalPages = res.data.totalElements;
    });
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop > 100
    ) {
      this.windowScrolled = true;
    } else if (
      (this.windowScrolled && window.pageYOffset) ||
      document.documentElement.scrollTop ||
      document.body.scrollTop < 10
    ) {
      this.windowScrolled = true;
    }
  }
  scrollToTop() {
    (function smoothscroll() {
      const currentScroll =
        document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - currentScroll / 8);
      }
    })();
  }

  open(modal) {
    this.modalReference = this.modalService.open(modal);
  }

  goToAdd() {
    this.modalReference.close();
    // TODO: add data validate
    const xmlTemplateId = 1;
    this.router.navigate(["/pdf/user/pdfp020100/add-xml"], {
      queryParams: { xmlTemplateId }
    });
  }

  viewXmlTemplateDetail(xmlTemplateId) {
    this.router.navigate(["/pdf/user/pdfp020100/edit-xml"], {
      queryParams: { xmlTemplateId }
    });
  }

  getAllMPdfTemplateStatus() {
    this.vuserpdfService.getAllMPdfTemplateStatus().subscribe(
      resultType => {
        this.dropdownMPdfTemplateStatus = resultType.data;
      },
      err => console.log("timeout")
    );
  }
}
