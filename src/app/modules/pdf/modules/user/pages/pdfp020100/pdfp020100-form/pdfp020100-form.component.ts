import {
  Component,
  OnInit,
  HostListener
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { PageResponse } from "src/app/shared/models/page-response";
import { PageRequest } from "src/app/shared/models/page-request";
import {
  NgbModal,
  NgbModalConfig,
  NgbModalRef
} from "@ng-bootstrap/ng-bootstrap";
import { AnswerModalComponent } from "src/app/shared/modals/answer-modal/answer-modal.component";
import { SsoUserModel } from "src/app/core/models/sso-user.model";
import { AuthenticationService } from "src/app/core/authentication/authentication.service";
import { XmlTemplateDetail } from "src/app/modules/pdf/models/xml-template-detail.model";
import { TemplateManagementService } from "src/app/modules/pdf/services/template-management.service";
import { ModalService } from "src/app/shared/services/modal.service";
import { ToastrService } from "ngx-toastr";
import { PdfTemplateDetail } from "src/app/modules/pdf/models/pdf-template-detail.model";
import { PdfResourceAttachment, PdfTemplateAttachment } from "src/app/modules/pdf/models/pdf-resource-attachment.model";

@Component({
  selector: "app-pdfp020100-form",
  templateUrl: "./pdfp020100-form.component.html",
  styleUrls: ["./pdfp020100-form.component.css"],
  providers: [NgbModalConfig, NgbModal]
})
export class Pdfp020100FormComponent implements OnInit {
  xmlTemplateList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: "10", value: 10 },
    { key: "25", value: 25 },
    { key: "50", value: 50 },
    { key: "100", value: 100 }
  ];
  windowScrolled: boolean;
  modalReference: NgbModalRef;
  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  GOBACK = "pdf/user/pdfp020100/search";
  toastrTimespan = 5000;
  // end of mesketeers

  currentUserInfo: SsoUserModel;
  isShow = false;
  isShowDownload = false;

  xmlReadOnly = false;

  // xmlTemplateList: Array<any>;
  xmlTemplateId = this.actRoute.snapshot.queryParamMap.get("xmlTemplateId");

  xmlTemplateDetail: XmlTemplateDetail;
  xmlTemplateDetailForm: FormGroup;
  xmlTemplateMaxFileSize: number;
  xmlTemplateFileToUpLoad: File = null;

  pdfTemplateDetail: PdfTemplateDetail;
  pdfTemplateDetailForm: FormGroup;
  pdfTemplateMaxFileSize: number;
  pdfTemplateFileToUpLoad: File = null;
  pdfResourceMaxFileSize: number;
  pdfResourceFileToUpLoad: Array<File>;

  pdfAttachmentList = <PdfTemplateAttachment[]>[];
  pdfAttachmentToUploadList: Array<PdfResourceAttachment>;
  pdfAttachmentToDeleteList: Array<PdfResourceAttachment>;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private blankModalService: NgbModal,
    private templateModalService: ModalService,
    private actRoute: ActivatedRoute,
    private authenService: AuthenticationService,
    private toastr: ToastrService,
    private templateManagementService: TemplateManagementService
  ) { }

  ngOnInit() {
    if (this.router.url.includes("add")) {
      this.xmlReadOnly = false;
    } else {
      this.xmlReadOnly = true;
    }
    this.pageRequest.sortFieldName = "PDF_TEMPLATE_CODE";
    this.pageRequest.sortDirection = "ASC";
    this.currentUserInfo = this.authenService.ssoUserDetailSnapshot;

    this.xmlTemplateDetail = new XmlTemplateDetail();
    this.xmlTemplateDetailForm = this.initXmlTemplateDetailForm(
      this.xmlTemplateDetail
    );

    // add office code

    this.pdfTemplateDetail = new PdfTemplateDetail();
    this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
      this.pdfTemplateDetail
    );

    this.getXmlTemplateDetail(
      this.actRoute.snapshot.queryParamMap.get("xmlTemplateId")
    );
    this.getPdfTemplateList(
      this.actRoute.snapshot.queryParamMap.get("xmlTemplateId")
    );

    // currently set maxsize
    this.xmlTemplateMaxFileSize = 100000001;
    this.pdfTemplateMaxFileSize = 100000001;
    this.pdfResourceMaxFileSize = 100000001;

    this.pdfResourceFileToUpLoad = [];
    this.pdfAttachmentToUploadList = [];
    this.pdfAttachmentToDeleteList = [];
  }


  initXmlTemplateDetailForm(xmlTemplate: XmlTemplateDetail): FormGroup {
    return new FormGroup({
      xmlTemplateId: new FormControl(xmlTemplate.xmlTemplateId),
      xmlTemplateCode: new FormControl(
        { value: xmlTemplate.xmlTemplateCode, disabled: true },
        Validators.required
      ),
      xmlTemplateName: new FormControl(xmlTemplate.xmlTemplateName, [
        Validators.required,
        Validators.maxLength(85)
      ]),
      xmlTemplateFileName: new FormControl(
        xmlTemplate.xmlTemplateFileName,
        Validators.required
      ),
      systemName: new FormControl(xmlTemplate.systemName, [
        Validators.required,
        Validators.maxLength(85)
      ]),
      departmentCode: new FormControl(xmlTemplate.departmentCode),
      departmentName: new FormControl(
        { value: xmlTemplate.departmentName, disabled: true },
        Validators.required
      ),
      email: new FormControl(xmlTemplate.email, [
        Validators.required,
        Validators.email
      ]),
      requestBy: new FormControl(xmlTemplate.requestBy),
      currentUser: new FormControl()
    });
  }

  initPdfTemplateDetailForm(pdfTemplate: PdfTemplateDetail): FormGroup {
    return new FormGroup({
      pdfTemplateId: new FormControl(pdfTemplate.pdfTemplateId),
      xmlTemplate: this.initXmlTemplateDetailForm(this.xmlTemplateDetail),
      pdfTemplateCode: new FormControl(
        { value: pdfTemplate.pdfTemplateCode, disabled: true }
      ),
      pdfTemplateName: new FormControl(pdfTemplate.pdfTemplateName, [
        Validators.required,
        Validators.maxLength(85)
      ]),
      pdfTemplateDesc: new FormControl(pdfTemplate.pdfTemplateDesc),
      xmlTemplateIdBatch: new FormControl(pdfTemplate.xmlTemplateIdBatch),
      pdfTemplateFileName: new FormControl(
        pdfTemplate.pdfTemplateFileName,
        Validators.required
      ),
      statusId: new FormControl(pdfTemplate.statusId),
      currentUser: new FormControl(),
      remark: new FormControl(pdfTemplate.remark)
      // pdfResourceAttachmentList: new FormArray([])
    });
  }

  getPdfTemplateList(xmlTemplateId) {
    if (xmlTemplateId != null) {
      const pdfTemplate = this.fb.group({
        xmlTemplate: { xmlTemplateId: xmlTemplateId }
      });
      const pdfCriteria = pdfTemplate.getRawValue();
      pdfCriteria.pageRequestDto = this.pageRequest;
      // console.log(pdfCriteria);

      this.templateManagementService
        .getPdfTemplateByXmlTemplateId(pdfCriteria)
        .subscribe((res: any) => {
          this.pageResponse = res.data;
          this.pageResponse.totalPages = res.data.totalElements;
          // if status = approved then disable save button.
        });
    }
  }

  getXmlTemplateDetail(xmlTemplateId) {
    if (xmlTemplateId != null) {
      var departmentName = "";
      var departmentCode = "";
      this.templateManagementService
        .getXmlTemplateByXmlTemplateId(xmlTemplateId)
        .subscribe((res: any) => {
          const param = {
            xmlTemplateId: res.data.xmlTemplateId ? res.data.xmlTemplateId : "",
            xmlTemplateCode: res.data.xmlTemplateCode ? res.data.xmlTemplateCode : "",
            xmlTemplateName: res.data.xmlTemplateName ? res.data.xmlTemplateName : "",
            xmlTemplateFileName: res.data.xmlTemplateFileName ? res.data.xmlTemplateFileName : "",
            systemName: res.data.systemName ? res.data.systemName : "",
            departmentCode: res.data.departmentCode ? res.data.departmentCode : "",
            departmentName: "",
            email: res.data.email ? res.data.email : "",
            requestBy: res.data.requestBy ? res.data.requestBy : "",
            currentUser: this.currentUserInfo.userId
          };
          this.templateManagementService
            .getOfficeCode(res.data.departmentCode)
            .subscribe((subRes: any) => {
              this.xmlTemplateDetailForm
                .get("departmentName")
                .setValue(subRes.longDescription);
            });
          this.xmlTemplateDetailForm.setValue(param);
        });
    } else {
      this.templateManagementService
        .getOfficeCode(this.currentUserInfo.userOfficeCode)
        .subscribe((res: any) => {
          this.xmlTemplateDetailForm
            .get("departmentCode")
            .setValue(res.officeCode);
          this.xmlTemplateDetailForm
            .get("departmentName")
            .setValue(res.longDescription);
          this.xmlTemplateDetailForm
            .get("email")
            .setValue(this.currentUserInfo.email);
        });
    }
  }

  // the "use everypage" method musketeers
  pageChange(event: any, limit) {
    this.pageRequest = event;
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    // get data from PDF Detail
    // this.getPdfTemplateList();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop > 100
    ) {
      this.windowScrolled = true;
    } else if (
      (this.windowScrolled && window.pageYOffset) ||
      document.documentElement.scrollTop ||
      document.body.scrollTop < 10
    ) {
      this.windowScrolled = true;
    }
  }

  scrollToTop() {
    (function smoothscroll() {
      const currentScroll =
        document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - currentScroll / 8);
      }
    })();
  }

  goBack() {
    this.router.navigate([this.GOBACK]);
  }

  // end of musketeers

  newPdfTemplate(modal) {
    this.modalReference = this.blankModalService.open(modal, { size: "lg" });
    this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
      this.pdfTemplateDetail
    );

    this.pdfTemplateDetailForm
      .get("xmlTemplate")
      .get("xmlTemplateId")
      .setValue(this.xmlTemplateDetailForm.get("xmlTemplateId").value);
    this.pdfTemplateDetailForm.get("xmlTemplate.xmlTemplateName").setValue(this.xmlTemplateDetailForm.get("xmlTemplateName").value)
    this.pdfTemplateDetailForm.get("xmlTemplate.systemName").setValue(this.xmlTemplateDetailForm.get("systemName").value);
    this.pdfTemplateDetailForm.get("xmlTemplate.email").setValue(this.xmlTemplateDetailForm.get("email").value);
    this.pdfTemplateDetailForm
      .get("currentUser")
      .setValue(this.currentUserInfo.userId);
    this.pdfTemplateDetailForm.get("xmlTemplate.xmlTemplateFileName").setValue(this.xmlTemplateDetailForm.get("xmlTemplateFileName").value);
    this.pdfAttachmentList = [];
    this.pdfResourceFileToUpLoad = [];
    this.pdfAttachmentToUploadList = [];
    this.pdfAttachmentToDeleteList = [];

    this.isShow = false;
    this.isShowDownload = false;
  }

  viewPdfTemplateDetail(pdfTemplateId, modal) {
    this.pdfResourceFileToUpLoad = [];
    this.pdfAttachmentToUploadList = [];
    this.pdfAttachmentToDeleteList = [];
    this.modalReference = this.blankModalService.open(modal, { size: "lg" });
    this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
      this.pdfTemplateDetail
    );
    if (pdfTemplateId != null) {
      this.templateManagementService
        .getPdfTemplateByPdfTemplateId(pdfTemplateId)
        .subscribe((result: any) => {
          const param = {
            pdfTemplateId: result.data.pdfTemplateId,
            pdfTemplateName: result.data.pdfTemplateName,
            pdfTemplateCode: result.data.pdfTemplateCode,
            pdfTemplateDesc: result.data.pdfTemplateDesc,
            pdfTemplateFileName: result.data.pdfTemplateFileName,
            xmlTemplate: result.data.xmlTemplate,
            xmlTemplateIdBatch: result.data.xmlTemplateIdBatch,
            statusId: 0,
            currentUser: this.currentUserInfo.userId,
            remark: result.data.remark ? result.data.remark : ''
          };
          param.xmlTemplate.departmentName = "";
          param.xmlTemplate.currentUser = "";
          delete param.xmlTemplate.xmlTemplateDesc;
          delete param.xmlTemplate.xmlTemplateFilepath;
          delete param.xmlTemplate.requestDate;
          delete param.xmlTemplate.updatedDate;
          delete param.xmlTemplate.updatedBy;
          delete param.xmlTemplate.approveDate;
          delete param.xmlTemplate.approveBy;
          delete param.xmlTemplate.createdDate;
          delete param.xmlTemplate.createdBy;
          delete param.xmlTemplate.handler;
          delete param.xmlTemplate.hibernateLazyInitializer;
          this.pdfTemplateDetailForm.setValue(param);
        });
    }
    this.pdfTemplateDetailForm.disable();
    this.isShow = true;
    this.isShowDownload = true;

    this.getPdfAttachmentLists(pdfTemplateId);
  }

  editPdfTemplateDetail(pdfTemplateId, modal) {
    this.pdfResourceFileToUpLoad = [];
    this.pdfAttachmentToUploadList = [];
    this.pdfAttachmentToDeleteList = [];
    this.modalReference = this.blankModalService.open(modal, { size: "lg" });
    this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
      this.pdfTemplateDetail
    );
    if (pdfTemplateId != null) {
      this.templateManagementService
        .getPdfTemplateByPdfTemplateId(pdfTemplateId)
        .subscribe((result: any) => {
          const param = {
            pdfTemplateId: result.data.pdfTemplateId,
            pdfTemplateName: result.data.pdfTemplateName,
            pdfTemplateCode: result.data.pdfTemplateCode,
            pdfTemplateDesc: result.data.pdfTemplateDesc,
            pdfTemplateFileName: result.data.pdfTemplateFileName,
            xmlTemplate: result.data.xmlTemplate,
            xmlTemplateIdBatch: result.data.xmlTemplateIdBatch,
            statusId: 0,
            currentUser: this.currentUserInfo.userId,
            remark: result.data.remark ? result.data.remark : ''
          };
          param.xmlTemplate.departmentName = "";
          param.xmlTemplate.currentUser = "";
          delete param.xmlTemplate.xmlTemplateDesc;
          delete param.xmlTemplate.xmlTemplateFilepath;
          delete param.xmlTemplate.requestDate;
          delete param.xmlTemplate.updatedDate;
          delete param.xmlTemplate.updatedBy;
          delete param.xmlTemplate.approveDate;
          delete param.xmlTemplate.approveBy;
          delete param.xmlTemplate.createdDate;
          delete param.xmlTemplate.createdBy;
          delete param.xmlTemplate.handler;
          delete param.xmlTemplate.hibernateLazyInitializer;
          this.pdfTemplateDetailForm.setValue(param);
        });
    }
    this.isShow = false;
    this.isShowDownload = true;

    this.getPdfAttachmentLists(pdfTemplateId);
  }

  cancelPdfTemplate(pdfTemplateId) {
    if (pdfTemplateId != null) {
      this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
        this.pdfTemplateDetail
      );
      this.templateManagementService
        .getPdfTemplateByPdfTemplateId(pdfTemplateId)
        .subscribe((result: any) => {
          const param = {
            pdfTemplateId: result.data.pdfTemplateId,
            pdfTemplateName: result.data.pdfTemplateName,
            pdfTemplateCode: result.data.pdfTemplateCode,
            pdfTemplateDesc: result.data.pdfTemplateDesc,
            pdfTemplateFileName: result.data.pdfTemplateFileName,
            xmlTemplate: result.data.xmlTemplate,
            xmlTemplateIdBatch: result.data.xmlTemplateIdBatch,
            statusId: 0,
            currentUser: this.currentUserInfo.userId,
            remark: result.data.remark ? result.data.remark : ''
          };
          param.xmlTemplate.departmentName = "";
          param.xmlTemplate.currentUser = "";
          delete param.xmlTemplate.xmlTemplateDesc;
          delete param.xmlTemplate.xmlTemplateFilepath;
          delete param.xmlTemplate.requestDate;
          delete param.xmlTemplate.updatedDate;
          delete param.xmlTemplate.updatedBy;
          delete param.xmlTemplate.approveDate;
          delete param.xmlTemplate.approveBy;
          delete param.xmlTemplate.createdDate;
          delete param.xmlTemplate.createdBy;
          delete param.xmlTemplate.handler;
          delete param.xmlTemplate.hibernateLazyInitializer;
          this.pdfTemplateDetailForm.setValue(param);
        });
      const modalRef = this.templateModalService.openConfirmModal('ยืนยันการยกเลิก', 'ยืนยันการยกเลิก PDF Template');
      (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
        if (result) {
          this.templateManagementService.cancelPdfTemplate(this.pdfTemplateDetailForm.getRawValue()).subscribe(subResult => {
            this.toastr.success('การยกเลิก PDF Template', 'สำเร็จ', { timeOut: this.toastrTimespan });
            this.getPdfTemplateList(this.xmlTemplateDetailForm.get('xmlTemplateId').value);
          }, error => this.showError('การยกเลิก PDF Template'))

        }
      });

    }
  }

  saveXmlTemplate() {
    if (this.xmlTemplateDetailForm.valid) {
      const modalRef = this.templateModalService.openConfirmModal(
        "ยืนยันการบันทึก ",
        " รายการ "
      );
      (modalRef.content as AnswerModalComponent).answerEvent.subscribe(
        result => {
          if (result) {
            if (this.pageResponse.content !== undefined) {
              this.xmlTemplateDetailForm
                .get("currentUser")
                .setValue(this.currentUserInfo.userId);
              this.templateManagementService
                .saveXmlTemplate(this.xmlTemplateDetailForm.getRawValue())
                .subscribe(
                  res => {
                    this.toastr.success("การบันทึก XML Template", "สำเร็จ", { timeOut: this.toastrTimespan });
                    this.templateManagementService
                      .getXmlTemplateByXmlTemplateId(res.data.xmlTemplateId)
                      .subscribe((subRes: any) => {
                        const formData = new FormData();
                        if (this.xmlTemplateFileToUpLoad != null) {
                          formData.append("xmlTemplateCode", subRes.data.xmlTemplateCode);
                          formData.append("file", this.xmlTemplateFileToUpLoad);
                          this.templateManagementService
                            .uploadXmlTemplateAttachment(formData)
                            .subscribe(
                              sub1Res => {
                                this.toastr.success("อัพโหลดไฟล์เอกสาร", "สำเร็จ", { timeOut: this.toastrTimespan });
                              },
                              uploadFileError => {
                                this.toastr.error(
                                  "อัพโหลดไฟล์เอกสารไม่สำเร็จ กรุณาเลือกเอกสารให้ถูกต้อง",
                                  "แจ้งเตือน", { timeOut: this.toastrTimespan }
                                );
                                return;
                              }
                            );
                        }
                      })
                    this.goBack();
                  },
                  error1 =>
                    this.toastr.error("พบข้อผิดพลาดระหว่างดำเนินการ", "แจ้งเตือน", { timeOut: this.toastrTimespan })
                );
            } else {
              this.templateModalService.openConfirmModal(
                "ไม่สามารถทำรายการได้",
                "ต้องบันทึก PDF อย่างน้อย 1 รายการ"
              );
            }
          }
        }
      );
    } else {
      this.validateAllFormFields(this.xmlTemplateDetailForm);
      window.scrollTo(0, 0);
    }
  }

  gotoXmlTemplateDetail(xmlTemplateId) {
    this.router.navigate(["/pdf/user/pdfp020100/edit-xml"], {
      queryParams: { xmlTemplateId }
    });
  }

  savePdfTemplate() {
    if (this.pdfTemplateDetailForm.valid && this.xmlTemplateDetailForm.valid) {
      if (this.xmlTemplateDetailForm.get("xmlTemplateId").value == null) {
        this.xmlTemplateDetailForm
          .get("currentUser")
          .setValue(this.currentUserInfo.userId);
        this.templateManagementService
          .saveXmlTemplate(this.xmlTemplateDetailForm.getRawValue())
          .subscribe(
            res => {
              this.toastr.success("การบันทึก XML Template", "สำเร็จ", { timeOut: this.toastrTimespan });
              if (this.router.url.includes("add")) {
                this.xmlTemplateDetailForm
                  .get("xmlTemplateId")
                  .setValue(res.data.xmlTemplateId);
                this.xmlTemplateDetailForm
                  .get("xmlTemplateCode")
                  .setValue(res.data.xmlTemplateCode);
              }

              // xml template code need trigger to change so return entity is unusable
              this.templateManagementService
                .getXmlTemplateByXmlTemplateId(res.data.xmlTemplateId)
                .subscribe((subRes: any) => {
                  // upload file XML
                  const formData = new FormData();
                  if (this.xmlTemplateFileToUpLoad != null) {
                    this.xmlTemplateDetailForm.get("xmlTemplateCode").setValue(subRes.data.xmlTemplateCode)
                    formData.append("xmlTemplateCode", subRes.data.xmlTemplateCode);
                    formData.append("file", this.xmlTemplateFileToUpLoad);
                    this.templateManagementService
                      .uploadXmlTemplateAttachment(formData)
                      .subscribe(
                        sub1Res => {
                          this.toastr.success("อัพโหลดไฟล์เอกสาร", "สำเร็จ", { timeOut: this.toastrTimespan });
                        },
                        uploadFileError => {
                          this.toastr.error(
                            "อัพโหลดไฟล์เอกสารไม่สำเร็จ กรุณาเลือกเอกสารให้ถูกต้อง",
                            "แจ้งเตือน", { timeOut: this.toastrTimespan }
                          );
                          return;
                        }
                      );
                  }
                })

              const resouceFileToUpLoad = this.pdfResourceFileToUpLoad
              // TODO Validate goes here.
              var tempPdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
              this.pdfTemplateDetailForm
                .get("currentUser")
                .setValue(this.currentUserInfo.userId);
              this.pdfTemplateDetailForm.get("statusId").setValue(3);
              this.pdfTemplateDetailForm.get("xmlTemplate.xmlTemplateId").setValue(res.data.xmlTemplateId)
              this.templateManagementService
                .savePdfTemplate(this.pdfTemplateDetailForm.getRawValue())
                .subscribe(
                  saveRes => {
                    this.toastr.success("การบันทึก PDF Template", "สำเร็จ", { timeOut: this.toastrTimespan });
                    if (this.pdfTemplateDetailForm.get("pdfTemplateId").value == null) {
                      this.pdfTemplateDetailForm
                        .get("pdfTemplateId")
                        .setValue(saveRes.data.pdfTemplateId);
                      tempPdfTemplateId = saveRes.data.pdfTemplateId;
                    }

                    const formData = new FormData();
                    if (this.pdfTemplateFileToUpLoad != null) {
                      formData.append("pdfTemplateId", tempPdfTemplateId);
                      formData.append("file", this.pdfTemplateFileToUpLoad);

                      this.templateManagementService
                        .uploadPdfTemplateAttachment(formData)
                        .subscribe(
                          uploadRes => {
                            this.toastr.success("อัพโหลดไฟล์เอกสารแนบ PDF Template", "สำเร็จ", { timeOut: this.toastrTimespan });
                          },
                          uploadFileError => {
                            this.toastr.error(
                              "อัพโหลดไฟล์เอกสารแนบ PDF Template ไม่สำเร็จ กรุณาเลือกเอกสารให้ถูกต้อง",
                              "แจ้งเตือน", { timeOut: this.toastrTimespan }
                            );
                            return;
                          }
                        );
                    }

                    if (resouceFileToUpLoad != null) {
                      for (let index = 0; index < resouceFileToUpLoad.length; index++) {
                        const attachmentFormData = new FormData();
                        const file = resouceFileToUpLoad[index];
                        if (file != null) {
                          attachmentFormData.append("pdfTemplateId", tempPdfTemplateId);
                          attachmentFormData.append("file", file);

                          this.templateManagementService.uploadPdfResourceAttachment(attachmentFormData).subscribe(
                            res => {
                              this.toastr.success("อัพโหลดไฟล์เอกสาร " + file.name, "สำเร็จ", { timeOut: this.toastrTimespan });
                            },
                            uploadFileError => {
                              this.toastr.error(
                                "อัพโหลดไฟล์เอกสารแนบ" + file.name + " ไม่สำเร็จ",
                                "แจ้งเตือน", { timeOut: this.toastrTimespan }
                              );
                              return;
                            }
                          )
                        }
                      }
                    }

                    this.getPdfTemplateList(this.xmlTemplateDetailForm.get('xmlTemplateId').value);
                  },
                  error1 => this.showError("พบข้อผิดพลาดระหว่างดำเนินการ")
                );
            },
            error1 =>
              this.toastr.error("พบข้อผิดพลาดระหว่างดำเนินการ", "แจ้งเตือน", { timeOut: this.toastrTimespan })
          );
      } else {
        const resouceFileToUpLoad = this.pdfResourceFileToUpLoad
        // TODO Validate goes here.
        var tempPdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
        this.pdfTemplateDetailForm
          .get("currentUser")
          .setValue(this.currentUserInfo.userId);
        // this.pdfTemplateDetailForm.get("xmlTemplateIdBatch").setValue(1)
        this.pdfTemplateDetailForm.get("statusId").setValue(3);
        this.templateManagementService
          .savePdfTemplate(this.pdfTemplateDetailForm.getRawValue())
          .subscribe(
            res => {
              this.toastr.success("การบันทึก PDF Template", "สำเร็จ", { timeOut: this.toastrTimespan });
              if (this.pdfTemplateDetailForm.get("pdfTemplateId").value == null) {
                this.pdfTemplateDetailForm
                  .get("pdfTemplateId")
                  .setValue(res.data.pdfTemplateId);
                tempPdfTemplateId = res.data.pdfTemplateId;
                // this.gotoXmlTemplateDetail(res.data.xmlTemplateId);
              }

              const formData = new FormData();
              if (this.pdfTemplateFileToUpLoad != null) {
                formData.append("pdfTemplateId", tempPdfTemplateId);
                formData.append("file", this.pdfTemplateFileToUpLoad);

                this.templateManagementService
                  .uploadPdfTemplateAttachment(formData)
                  .subscribe(
                    res => {
                      this.toastr.success("อัพโหลดไฟล์เอกสารแนบ PDF Template", "สำเร็จ", { timeOut: this.toastrTimespan });
                    },
                    uploadFileError => {
                      this.toastr.error(
                        "อัพโหลดไฟล์เอกสารแนบ PDF Template ไม่สำเร็จ กรุณาเลือกเอกสารให้ถูกต้อง",
                        "แจ้งเตือน", { timeOut: this.toastrTimespan }
                      );
                      return;
                    }
                  );
              }

              if (resouceFileToUpLoad != null) {
                for (let index = 0; index < resouceFileToUpLoad.length; index++) {
                  const attachmentFormData = new FormData();
                  const file = resouceFileToUpLoad[index];
                  if (file != null) {
                    attachmentFormData.append("pdfTemplateId", tempPdfTemplateId);
                    attachmentFormData.append("file", file);

                    this.templateManagementService.uploadPdfResourceAttachment(attachmentFormData).subscribe(
                      res => {
                        this.toastr.success("อัพโหลดไฟล์เอกสาร " + file.name, "สำเร็จ", { timeOut: this.toastrTimespan });
                      },
                      uploadFileError => {
                        this.toastr.error(
                          "อัพโหลดไฟล์เอกสารแนบ" + file.name + " ไม่สำเร็จ",
                          "แจ้งเตือน", { timeOut: this.toastrTimespan }
                        );
                        return;
                      }
                    )
                  }
                }
              }

              this.getPdfTemplateList(this.xmlTemplateDetailForm.get('xmlTemplateId').value);
            },
            error1 => this.showError("พบข้อผิดพลาดระหว่างดำเนินการ")
          );

      }

      this.modalReference.close();
    } else {
      this.validateAllFormFields(this.xmlTemplateDetailForm);
      this.validateAllFormFields(this.pdfTemplateDetailForm);
    }
  }

  showError(msg: string) {
    if (msg !== undefined) {
      this.toastr.error(msg, "ไม่สำเร็จ", { timeOut: this.toastrTimespan });
    } else {
      this.toastr.error("พบข้อผิดพลาด ไม่สามารถทำรายการได้", "ไม่สำเร็จ", { timeOut: this.toastrTimespan });
    }
  }

  setXmlTemplateAttachment(element) {
    let xmlTemplateFileName = this.xmlTemplateDetailForm;
    let inputElement = element as HTMLInputElement;
    let file = inputElement.files[0];
    if (file !== undefined) {
      if (file.size > this.xmlTemplateMaxFileSize) {
        this.templateModalService.openModal(
          "แจ้งเตือน",
          "ไฟล์มีขนาดใหญ่เกิน 100 MB"
        );
      } else {
        xmlTemplateFileName.get("xmlTemplateFileName").setValue(file.name);
        this.xmlTemplateFileToUpLoad = file;
      }
    }
    // console.log(this.xmlTemplateDetailForm);
  }

  setPdfTemplateAttachment(element) {
    let pdfTemplateFileName = this.pdfTemplateDetailForm;
    let inputElement = element as HTMLInputElement;
    let file = inputElement.files[0];
    if (file !== undefined) {
      if (file.size > this.pdfTemplateMaxFileSize) {
        this.templateModalService.openModal(
          "แจ้งเตือน",
          "ไฟล์มีขนาดใหญ่เกิน 100 MB"
        );
      } else {
        pdfTemplateFileName.get("pdfTemplateFileName").setValue(file.name);
        this.pdfTemplateFileToUpLoad = file;
      }
    }
  }

  setPdfResourceAttachment(element) {
    let inputElement = element as HTMLInputElement;
    let files = inputElement.files;
    for (let index = 0; index < files.length; index++) {
      const file = files[index];
      if (file !== undefined) {
        if (file.size > this.pdfResourceMaxFileSize) {
          this.templateModalService.openModal(
            "แจ้งเตือน",
            "ไฟล์มีขนาดใหญ่เกิน 100 MB"
          );
        } else {
          let item = new PdfResourceAttachment();
          item.fileName = file.name;
          item.filePath = "test";
          if (!this.pdfAttachmentToUploadList.some((aitem) => aitem.fileName == item.fileName)) {
            this.pdfAttachmentToUploadList.push(item);
            this.pdfResourceFileToUpLoad.push(file);
          }
        }
      }
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  downloadXmlTemplate() {
    const xmlTemplateForm = this.xmlTemplateDetailForm;

    if (xmlTemplateForm !== null && xmlTemplateForm !== undefined) {
      if (
        xmlTemplateForm.get("xmlTemplateFileName").value !== null ||
        xmlTemplateForm.get("xmlTemplateFileName").value !== undefined
      ) {
        let formData = new FormData();
        formData.append(
          "xmlTemplateId",
          xmlTemplateForm.get("xmlTemplateId").value
        );
        formData.append(
          "filename",
          xmlTemplateForm.get("xmlTemplateFileName").value
        );

        this.templateManagementService
          .downloadXmlTemplateAttachment(formData)
          .subscribe(
            (res: any) => {
              if (res.status === 200 || res.status === 0) {
                this.templateManagementService.convertBase64FileToSafeLink(
                  res.data
                );
              } else {
                this.templateModalService.openModal(
                  "แจ้งเตือน",
                  "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
                );
              }
            },
            error => {
              this.templateModalService.openModal(
                "แจ้งเตือน",
                "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
              );
            }
          );
      }
    }
  }

  downloadPdfTemplate(pdfTemplateId) {
    if (pdfTemplateId !== null && pdfTemplateId !== undefined) {
      if (pdfTemplateId == 0) {
        pdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
      }
      let formData = new FormData();
      formData.append("pdfTemplateId", pdfTemplateId);

      this.templateManagementService
        .downloadPdfTemplateAttachment(formData)
        .subscribe(
          (res: any) => {
            if (res.status === 200 || res.status === 0) {
              this.templateManagementService.convertBase64FileToSafeLink(
                res.data
              );
            } else {
              this.templateModalService.openModal(
                "แจ้งเตือน",
                "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
              );
            }
          },
          error => {
            this.templateModalService.openModal(
              "แจ้งเตือน",
              "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
            );
          }
        );
    }
  }

  downloadPdfResource(pdfTemplateAttachmentId, pdfTemplateId) {
    if (pdfTemplateId !== null && pdfTemplateId !== undefined) {
      if (pdfTemplateId == 0) {
        pdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
      }
      let formData = new FormData();
      formData.append("pdfTemplateId", pdfTemplateId);
      formData.append("pdfTemplateAttachmentId", pdfTemplateAttachmentId)

      this.templateManagementService
        .downloadPdfResourceAttachment(formData)
        .subscribe(
          (res: any) => {
            if (res.status === 200 || res.status === 0) {
              this.templateManagementService.convertBase64FileToSafeLink(
                res.data
              );
            } else {
              this.templateModalService.openModal(
                "แจ้งเตือน",
                "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
              );
            }
          },
          error => {
            this.templateModalService.openModal(
              "แจ้งเตือน",
              "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
            );
          }
        );
    }
  }

  deletePdfResource(pdfTemplateAttachmentId, pdfTemplateId) {
    if (pdfTemplateId !== null && pdfTemplateId !== undefined) {
      if (pdfTemplateId == 0) {
        pdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
      }

      const modalRef = this.templateModalService.openConfirmModal(
        "ยืนยันการลบไฟล์แนบ ",
        "การลบไฟล์จะเกิดขึ้นทันที โดยไม่ต้องกดบันทึกอีกครั้ง"
      );
      (modalRef.content as AnswerModalComponent).answerEvent.subscribe(
        result => {
          if (result) {
            let formData = new FormData();
            formData.append("pdfTemplateId", pdfTemplateId);
            formData.append("pdfTemplateAttachmentId", pdfTemplateAttachmentId)

            this.templateManagementService
              .deletePdfResourceAttachment(formData)
              .subscribe(
                (res: any) => {
                  if (res.data) {
                    this.templateModalService.openModal(
                      "แจ้งเตือน",
                      "ลบไฟล์แนบ สำเร็จ"
                    );
                  } else {
                    this.templateModalService.openModal(
                      "แจ้งเตือน",
                      "ลบไฟล์แนบไม่สำเร็จ กรุณาติดต่อเจ้าหน้าที่"
                    );
                  }

                },
                error => {
                  this.templateModalService.openModal(
                    "แจ้งเตือน",
                    "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
                  );
                }
              );
          }
        })
    }
  }

  previewPdfTemplate(pdfTemplateId) {
    if (pdfTemplateId !== null && pdfTemplateId !== undefined) {
      if (pdfTemplateId == 0) {
        pdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
      }
      let formData = new FormData();
      formData.append("pdfTemplateId", pdfTemplateId);

      this.templateManagementService
        .downloadPdfTemplateAttachment(formData)
        .subscribe(
          (res: any) => {
            if (res.status === 200 || res.status === 0) {
              this.templateManagementService.openFileOnNewTab(res.data);
            } else {
              this.templateModalService.openModal(
                "แจ้งเตือน",
                "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
              );
            }
          },
          error => {
            this.templateModalService.openModal(
              "แจ้งเตือน",
              "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
            );
          }
        );
    }
  }

  getPdfAttachmentLists(pdfTemplateId) {
    this.templateManagementService.getPdfAttachmentLists(pdfTemplateId).subscribe(
      resultType => {
        this.pdfAttachmentList = resultType.data;
      },
      err => console.log("timeout")
    );
  }

  deleteResourceInList(number) {
    this.pdfResourceFileToUpLoad.splice(number, 1);
    this.pdfAttachmentToUploadList.splice(number, 1);
  }

  refresh(): void {
    window.location.reload();
  }

  get email() {
    return this.xmlTemplateDetailForm.get("email");
  }
  get systemName() {
    return this.xmlTemplateDetailForm.get("systemName");
  }
  get xmlTemplateName() {
    return this.xmlTemplateDetailForm.get("xmlTemplateName");
  }
  get pdfTemplateName() {
    return this.pdfTemplateDetailForm.get("pdfTemplateName");
  }
  get pdfTemplateFileNameControl() {
    return this.pdfTemplateDetailForm.get("pdfTemplateFileName");
  }
  get xmlTemplateFileNameControl() {
    return this.xmlTemplateDetailForm.get("xmlTemplateFileName");
  }
}
