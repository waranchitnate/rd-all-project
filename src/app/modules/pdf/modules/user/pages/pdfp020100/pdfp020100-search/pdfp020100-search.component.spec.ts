import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pdfp020100SearchComponent } from './pdfp020100-search.component';

describe('Pdfp020100SearchComponent', () => {
  let component: Pdfp020100SearchComponent;
  let fixture: ComponentFixture<Pdfp020100SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pdfp020100SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pdfp020100SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
