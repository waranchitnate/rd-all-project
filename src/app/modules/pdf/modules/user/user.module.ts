import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SharedModule } from "src/app/shared/shared.module";
import { UserRoutingModule } from "./user-routing.module";
import { Pdfp020100FormComponent } from "./pages/pdfp020100/pdfp020100-form/pdfp020100-form.component";
import { Pdfp020100SearchComponent } from "./pages/pdfp020100/pdfp020100-search/pdfp020100-search.component";

@NgModule({
  declarations: [Pdfp020100FormComponent, Pdfp020100SearchComponent],
  imports: [CommonModule, SharedModule, UserRoutingModule],
  bootstrap: [Pdfp020100FormComponent, Pdfp020100SearchComponent]
})
export class UserModule {}
