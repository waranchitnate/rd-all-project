import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { Pdfp020100FormComponent } from "./pages/pdfp020100/pdfp020100-form/pdfp020100-form.component";
import { Pdfp020100SearchComponent } from "./pages/pdfp020100/pdfp020100-search/pdfp020100-search.component";

const routes: Routes = [
  { path: "pdfp020100/search", component: Pdfp020100SearchComponent },
  { path: "pdfp020100/add-xml", component: Pdfp020100FormComponent },
  { path: "pdfp020100/edit-xml", component: Pdfp020100FormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
