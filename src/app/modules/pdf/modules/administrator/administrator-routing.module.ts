import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { Pdfp010100FormComponent } from "./pages/pdfp010100/pdfp010100-form/pdfp010100-form.component";
import { Pdfp010100SearchComponent } from "./pages/pdfp010100/pdfp010100-search/pdfp010100-search.component";

const routes: Routes = [
  { path: "pdfp010100/search", component: Pdfp010100SearchComponent },
  { path: "pdfp010100/edit-xml", component: Pdfp010100FormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministratorRoutingModule {}
