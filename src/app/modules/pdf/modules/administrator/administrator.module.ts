import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { AdministratorRoutingModule } from './administrator-routing.module';
import { Pdfp010100SearchComponent } from './pages/pdfp010100/pdfp010100-search/pdfp010100-search.component';
import { Pdfp010100FormComponent } from './pages/pdfp010100/pdfp010100-form/pdfp010100-form.component';

@NgModule({
  declarations: [Pdfp010100SearchComponent, Pdfp010100FormComponent],
  imports: [
    CommonModule,
    SharedModule,
    AdministratorRoutingModule
  ]
})
export class AdministratorModule { }
