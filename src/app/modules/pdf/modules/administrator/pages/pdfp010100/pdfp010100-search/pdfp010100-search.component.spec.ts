import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pdfp010100SearchComponent } from './pdfp010100-search.component';

describe('Pdfp010100SearchComponent', () => {
  let component: Pdfp010100SearchComponent;
  let fixture: ComponentFixture<Pdfp010100SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pdfp010100SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pdfp010100SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
