import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pdfp010100FormComponent } from './pdfp010100-form.component';

describe('Pdfp010100FormComponent', () => {
  let component: Pdfp010100FormComponent;
  let fixture: ComponentFixture<Pdfp010100FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pdfp010100FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pdfp010100FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
