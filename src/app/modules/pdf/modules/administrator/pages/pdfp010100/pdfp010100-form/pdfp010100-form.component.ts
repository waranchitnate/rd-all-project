import { Component, OnInit, HostListener } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {
  NgbModalConfig,
  NgbModal,
  NgbModalRef
} from "@ng-bootstrap/ng-bootstrap";
import { PageResponse } from "src/app/shared/models/page-response";
import { PageRequest } from "src/app/shared/models/page-request";
import { SsoUserModel } from "src/app/core/models/sso-user.model";
import { XmlTemplateDetail } from "src/app/modules/pdf/models/xml-template-detail.model";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
  AbstractControl
} from "@angular/forms";
import { PdfTemplateDetail } from "src/app/modules/pdf/models/pdf-template-detail.model";
import { ModalService } from "src/app/shared/services/modal.service";
import { AuthenticationService } from "src/app/core/authentication/authentication.service";
import { ToastrService } from "ngx-toastr";
import { AdminTemplateManagementService } from "src/app/modules/pdf/services/admin-template-management.service";
import { AnswerModalComponent } from "src/app/shared/modals/answer-modal/answer-modal.component";
import { PdfTemplateAttachment } from "src/app/modules/pdf/models/pdf-resource-attachment.model";

@Component({
  selector: "app-pdfp010100-form",
  templateUrl: "./pdfp010100-form.component.html",
  styleUrls: ["./pdfp010100-form.component.css"],
  providers: [NgbModalConfig, NgbModal]
})
export class Pdfp010100FormComponent implements OnInit {
  xmlTemplateList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: "10", value: 10 },
    { key: "25", value: 25 },
    { key: "50", value: 50 },
    { key: "100", value: 100 }
  ];
  windowScrolled: boolean;
  modalReference: NgbModalRef;
  modal2Reference: NgbModalRef;
  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  GOBACK = "pdf/admin/pdfp010100/search";
  toastrTimespan = 5000;
  // end of mesketeers

  currentUserInfo: SsoUserModel;
  isShow = false;
  isApproved = false;
  isReadonly = false;

  // xmlTemplateList: Array<any>;
  xmlTemplateId = this.actRoute.snapshot.queryParamMap.get("xmlTemplateId");

  xmlTemplateDetail: XmlTemplateDetail;
  xmlTemplateDetailForm: FormGroup;

  pdfTemplateDetail: PdfTemplateDetail;
  pdfTemplateDetailForm: FormGroup;
  pdfTemplateDemoMaxFileSize: number;
  pdfTemplateFileDemoToUpLoad: File = null;

  xmlTemplateSubmit = false;

  pdfAttachmentList = <PdfTemplateAttachment[]>[];

  reasonToDeny: string;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private blankModalService: NgbModal,
    private templateModalService: ModalService,
    private actRoute: ActivatedRoute,
    private authenService: AuthenticationService,
    private toastr: ToastrService,
    private adminTemplateManagementService: AdminTemplateManagementService
  ) { }

  ngOnInit() {
    this.pageRequest.sortFieldName = "PDF_TEMPLATE_CODE";
    this.pageRequest.sortDirection = "ASC";
    this.currentUserInfo = this.authenService.ssoUserDetailSnapshot;

    this.xmlTemplateDetail = new XmlTemplateDetail();
    this.xmlTemplateDetailForm = this.initXmlTemplateDetailForm(
      this.xmlTemplateDetail
    );

    this.pdfTemplateDetail = new PdfTemplateDetail();
    this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
      this.pdfTemplateDetail
    );

    this.getXmlTemplateDetail(
      this.actRoute.snapshot.queryParamMap.get("xmlTemplateId")
    );
    this.getPdfTemplateList(
      this.actRoute.snapshot.queryParamMap.get("xmlTemplateId")
    );

    // currently set maxsize
    this.pdfTemplateDemoMaxFileSize = 100000001;
  }

  initXmlTemplateDetailForm(xmlTemplate: XmlTemplateDetail): FormGroup {
    return new FormGroup({
      xmlTemplateId: new FormControl(xmlTemplate.xmlTemplateId),
      xmlTemplateCode: new FormControl(
        { value: xmlTemplate.xmlTemplateCode, disabled: true },
        Validators.required
      ),
      xmlTemplateName: new FormControl(
        xmlTemplate.xmlTemplateName,
        Validators.required
      ),
      xmlTemplateFileName: new FormControl(
        xmlTemplate.xmlTemplateFileName,
        Validators.required
      ),
      systemName: new FormControl(xmlTemplate.systemName, Validators.required),
      departmentCode: new FormControl(xmlTemplate.departmentCode),
      departmentName: new FormControl(
        { value: xmlTemplate.departmentName, disabled: true },
        Validators.required
      ),
      email: new FormControl(xmlTemplate.email, [
        Validators.required,
        Validators.email
      ]),
      currentUser: new FormControl()
    });
  }

  initPdfTemplateDetailForm(pdfTemplate: PdfTemplateDetail): FormGroup {
    return new FormGroup({
      pdfTemplateId: new FormControl(pdfTemplate.pdfTemplateId),
      xmlTemplate: new FormControl(pdfTemplate.xmlTemplate),
      pdfTemplateCode: new FormControl(
        { value: pdfTemplate.pdfTemplateCode, disabled: true },
        Validators.required
      ),
      pdfTemplateName: new FormControl(pdfTemplate.pdfTemplateName, [
        Validators.required,
        Validators.maxLength(85)
      ]),
      pdfTemplateDesc: new FormControl(pdfTemplate.pdfTemplateDesc),
      xmlTemplateIdBatch: new FormControl(pdfTemplate.xmlTemplateIdBatch),
      pdfTemplateFileName: new FormControl(pdfTemplate.pdfTemplateFileName,
        Validators.required
      ),
      pdfTemplateDemoFileName: new FormControl(pdfTemplate.pdfTemplateDemoFileName, Validators.required
      ),
      statusId: new FormControl(pdfTemplate.statusId),
      startDate: new FormControl(
        { value: pdfTemplate.startDate },
        Validators.required
      ),
      endDate: new FormControl(
        { value: pdfTemplate.startDate },
        Validators.required
      ),
      currentUser: new FormControl(),
      remark: new FormControl(pdfTemplate.remark)
    });
  }

  // the "use everypage" method musketeers
  pageChange(event: any, limit) {
    this.pageRequest = event;
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    // get data from PDF Detail
    // this.getPdfTemplateList();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.windowScrolled = true;
    } else if (
      (this.windowScrolled && window.pageYOffset) || document.documentElement.scrollTop || document.body.scrollTop < 10) {
      this.windowScrolled = true;
    }
  }

  scrollToTop() {
    (function smoothscroll() {
      const currentScroll =
        document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - currentScroll / 8);
      }
    })();
  }

  goBack() {
    this.router.navigate([this.GOBACK]);
  }

  // end of musketeers

  getPdfTemplateList(xmlTemplateId) {
    if (xmlTemplateId != null) {
      const pdfTemplate = this.fb.group({
        xmlTemplate: { xmlTemplateId: xmlTemplateId }
      });
      const pdfCriteria = pdfTemplate.getRawValue();
      pdfCriteria.pageRequestDto = this.pageRequest;

      this.adminTemplateManagementService
        .getPdfTemplateByXmlTemplateId(pdfCriteria)
        .subscribe((res: any) => {
          this.pageResponse = res.data;
          this.pageResponse.totalPages = res.data.totalElements;

          // if status = approved then disable save button.
        });
    }
  }

  getXmlTemplateDetail(xmlTemplateId) {
    if (xmlTemplateId != null) {
      this.adminTemplateManagementService
        .getXmlTemplateByXmlTemplateId(xmlTemplateId)
        .subscribe((res: any) => {
          const param = {
            xmlTemplateId: res.data.xmlTemplateId ? res.data.xmlTemplateId : "",
            xmlTemplateCode: res.data.xmlTemplateCode ? res.data.xmlTemplateCode : "",
            xmlTemplateName: res.data.xmlTemplateName ? res.data.xmlTemplateName : "",
            xmlTemplateFileName: res.data.xmlTemplateFileName ? res.data.xmlTemplateFileName : "",
            systemName: res.data.systemName ? res.data.systemName : "",
            departmentCode: res.data.departmentCode ? res.data.departmentCode : "",
            departmentName: "",
            email: res.data.email ? res.data.email : "",
            currentUser: this.currentUserInfo.userId
          };
          this.adminTemplateManagementService
            .getOfficeCode(res.data.departmentCode)
            .subscribe((subRes: any) => {
              this.xmlTemplateDetailForm
                .get("departmentName")
                .setValue(subRes.longDescription);
            });
          this.xmlTemplateDetailForm.setValue(param);

          // if status = approved then disable save button.
        });
    }
  }

  newPdfTemplate(modal) {
    this.modalReference = this.blankModalService.open(modal, { size: "lg" });
    this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
      this.pdfTemplateDetail
    );
    this.pdfTemplateDetailForm
      .get("xmlTemplateId")
      .setValue(this.xmlTemplateDetailForm.get("xmlTemplateId").value);
    this.pdfTemplateDetailForm
      .get("currentUser")
      .setValue(this.currentUserInfo.userId);
  }

  viewPdfTemplateDetail(pdfTemplateId, modal) {
    this.modalReference = this.blankModalService.open(modal, { size: "lg" });
    this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
      this.pdfTemplateDetail
    );
    if (pdfTemplateId != null) {
      this.adminTemplateManagementService
        .getPdfTemplateByPdfTemplateId(pdfTemplateId)
        .subscribe((result: any) => {
          const param = {
            pdfTemplateId: result.data.pdfTemplateId,
            pdfTemplateName: result.data.pdfTemplateName,
            pdfTemplateCode: result.data.pdfTemplateCode,
            pdfTemplateDesc: result.data.pdfTemplateDesc,
            pdfTemplateFileName: result.data.pdfTemplateFileName,
            pdfTemplateDemoFileName:
              result.data.pdfTemplateDemoFileName === undefined
                ? ""
                : result.data.pdfTemplateDemoFileName,
            xmlTemplate: result.data.xmlTemplate,
            xmlTemplateIdBatch: result.data.xmlTemplateIdBatch,
            statusId: result.data.statusId,
            startDate: new Date(result.data.startDate),
            endDate: new Date(result.data.endDate),
            currentUser: this.currentUserInfo.userId,
            remark: ""
          };
          this.pdfTemplateDetailForm.setValue(param);
        });
    }
    this.pdfTemplateDetailForm.disable();
    this.isShow = true;
    this.getPdfAttachmentLists(pdfTemplateId);
  }

  editPdfTemplateDetail(pdfTemplateId, modal) {
    this.modalReference = this.blankModalService.open(modal, { size: "lg" });
    this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
      this.pdfTemplateDetail
    );
    if (pdfTemplateId != null) {
      this.adminTemplateManagementService
        .getPdfTemplateByPdfTemplateId(pdfTemplateId)
        .subscribe((result: any) => {
          const param = {
            pdfTemplateId: result.data.pdfTemplateId,
            pdfTemplateName: result.data.pdfTemplateName,
            pdfTemplateCode: result.data.pdfTemplateCode,
            pdfTemplateDesc: result.data.pdfTemplateDesc,
            pdfTemplateFileName: result.data.pdfTemplateFileName,
            pdfTemplateDemoFileName:
              result.data.pdfTemplateDemoFileName === undefined
                ? ""
                : result.data.pdfTemplateDemoFileName,
            xmlTemplate: result.data.xmlTemplate,
            xmlTemplateIdBatch: result.data.xmlTemplateIdBatch,
            statusId: result.data.statusId,
            startDate: new Date(),
            endDate: new Date(result.data.endDate),
            currentUser: this.currentUserInfo.userId,
            remark: ""
          };

          if (result.data.statusId == '1') {
            this.isApproved = true;
            this.isReadonly = true;
          } else {
            this.isApproved = false;
            this.isReadonly = false;
          }
          this.pdfTemplateDetailForm.setValue(param);
        });
    }
    this.isShow = false;
    this.getPdfAttachmentLists(pdfTemplateId);
  }

  cancelPdfTemplate(pdfTemplateId) {
    if (pdfTemplateId != null) {
      this.pdfTemplateDetailForm = this.initPdfTemplateDetailForm(
        this.pdfTemplateDetail
      );
      this.adminTemplateManagementService
        .getPdfTemplateByPdfTemplateId(pdfTemplateId)
        .subscribe((result: any) => {
          const param = {
            pdfTemplateId: result.data.pdfTemplateId,
            pdfTemplateName: result.data.pdfTemplateName,
            pdfTemplateCode: result.data.pdfTemplateCode,
            pdfTemplateDesc: result.data.pdfTemplateDesc,
            pdfTemplateFileName: result.data.pdfTemplateFileName,
            pdfTemplateDemoFileName:
              result.data.pdfTemplateDemoFileName === undefined
                ? ""
                : result.data.pdfTemplateDemoFileName,
            xmlTemplate: result.data.xmlTemplate,
            xmlTemplateIdBatch: result.data.xmlTemplateIdBatch,
            statusId: result.data.statusId,
            startDate: new Date(result.data.startDate),
            endDate: new Date(result.data.endDate),
            currentUser: this.currentUserInfo.userId
          };

          this.pdfTemplateDetailForm.setValue(param);
        });
      const modalRef = this.templateModalService.openConfirmModal('ยืนยันการยกเลิก', 'ยืนยันการยกเลิก PDF Template');
      (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
        if (result) {
          this.adminTemplateManagementService.cancelPdfTemplate(this.pdfTemplateDetailForm.getRawValue()).subscribe(subResult => {
            this.toastr.success('การยกเลิก PDF Template', 'สำเร็จ', { timeOut: this.toastrTimespan });
            this.getPdfTemplateList(this.xmlTemplateDetailForm.get('xmlTemplateId').value);
          }, error => this.showError('การยกเลิก PDF Template'))

        }
      });
    }
  }

  saveXmlTemplate() {
    // TODO validate data goes here.
    const modalRef = this.templateModalService.openConfirmModal(
      "ยืนยันการบันทึก ",
      " รายการ "
    );
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        this.xmlTemplateDetailForm
          .get("currentUser")
          .setValue(this.currentUserInfo.userId);
        this.adminTemplateManagementService
          .saveXmlTemplate(this.xmlTemplateDetailForm.value)
          .subscribe(
            res => {
              this.toastr.success("การบันทึก XML Template", "สำเร็จ", { timeOut: this.toastrTimespan });
              this.goBack();
            },
            error1 => this.showError("พบข้อผิดพลาดระหว่างดำเนินการ")
          );
      }
    });
  }

  approvePdfTemplate() {
    if (this.pdfTemplateDetailForm.valid) {
      const formData = new FormData();
      if (this.pdfTemplateFileDemoToUpLoad != null) {
        formData.append("pdfTemplateId", this.pdfTemplateDetailForm.get("pdfTemplateId").value);
        formData.append("file", this.pdfTemplateFileDemoToUpLoad);

        this.adminTemplateManagementService
          .uploadPdfTemplateDemoAttachment(formData)
          .subscribe(
            res => { },
            uploadFileError => {
              this.showError("อัพโหลดไฟล์เอกสารไม่สำเร็จ กรุณาเลือกเอกสารให้ถูกต้อง")
              return;
            }
          );
      }

      this.pdfTemplateDetailForm
        .get("currentUser")
        .setValue(this.currentUserInfo.userId);
      this.adminTemplateManagementService
        .approvePdfTemplate(this.pdfTemplateDetailForm.getRawValue())
        .subscribe(
          res => {
            this.toastr.success("การบันทึก PDF Template", "สำเร็จ", { timeOut: this.toastrTimespan });
            this.ngOnInit();
          },
          error1 => this.showError("พบข้อผิดพลาดระหว่างดำเนินการ")
        );
      this.modalReference.close();
    } else {
      this.validateAllFormFields(this.pdfTemplateDetailForm);
      window.scrollTo(0, 0);
    }
  }

  denyPdfTemplate(modal) {
    this.modal2Reference = this.blankModalService.open(modal, { size: "lg" });
    this.modalReference.close();
  }

  confirmDeny() {
    // deny don't need to check it valid or not, just update status
    this.pdfTemplateDetailForm
      .get("currentUser")
      .setValue(this.currentUserInfo.userId);
    this.pdfTemplateDetailForm.get("remark").setValue(this.reasonToDeny)

    this.adminTemplateManagementService
      .denyPdfTemplate(this.pdfTemplateDetailForm.getRawValue())
      .subscribe(
        res => {
          this.toastr.success("บันทึกสถานะ แจ้งส่งกลับแก้ไข", "สำเร็จ", { timeOut: this.toastrTimespan });
          this.ngOnInit();
        },
        error1 => this.showError("พบข้อผิดพลาดระหว่างดำเนินการ")
      );
    this.modal2Reference.close();
  }

  savePdfTemplate() {
    if (this.pdfTemplateDetailForm.valid) {
      const formData = new FormData();
      if (this.pdfTemplateFileDemoToUpLoad != null) {
        formData.append("pdfTemplateId", this.pdfTemplateDetailForm.get("pdfTemplateId").value);
        formData.append("file", this.pdfTemplateFileDemoToUpLoad);

        this.adminTemplateManagementService
          .uploadPdfTemplateDemoAttachment(formData)
          .subscribe(
            res => { },
            uploadFileError => {
              this.showError("อัพโหลดไฟล์เอกสารไม่สำเร็จ กรุณาเลือกเอกสารให้ถูกต้อง")
              return;
            }
          );
      }

      this.pdfTemplateDetailForm
        .get("currentUser")
        .setValue(this.currentUserInfo.userId);
      this.adminTemplateManagementService
        .savePdfTemplate(this.pdfTemplateDetailForm.getRawValue())
        .subscribe(
          res => {
            this.toastr.success("การบันทึก PDF Template", "สำเร็จ", { timeOut: this.toastrTimespan });
            this.ngOnInit();
          },
          error1 => this.showError("พบข้อผิดพลาดระหว่างดำเนินการ")
        );
      this.modalReference.close();
    } else {
      this.validateAllFormFields(this.xmlTemplateDetailForm);
      window.scrollTo(0, 0);
    }
  }

  showError(msg: string) {
    if (msg !== undefined) {
      this.toastr.error(msg, "ไม่สำเร็จ", { timeOut: this.toastrTimespan });
    } else {
      this.toastr.error("พบข้อผิดพลาด ไม่สามารถทำรายการได้", "ไม่สำเร็จ", { timeOut: this.toastrTimespan });
    }
  }

  setPdfTemplateDemoAttachment(element) {
    let pdfTemplateDemoFileName = this.pdfTemplateDetailForm;
    let inputElement = element as HTMLInputElement;
    let file = inputElement.files[0];
    if (file !== undefined) {
      if (file.size > this.pdfTemplateDemoMaxFileSize) {
        this.templateModalService.openModal(
          "แจ้งเตือน",
          "ไฟล์มีขนาดใหญ่เกิน 100 MB"
        );
      } else {
        pdfTemplateDemoFileName
          .get("pdfTemplateDemoFileName")
          .setValue(file.name);
        this.pdfTemplateFileDemoToUpLoad = file;
      }
    }
  }

  downloadXmlTemplate() {
    const xmlTemplateForm = this.xmlTemplateDetailForm;

    if (xmlTemplateForm !== null && xmlTemplateForm !== undefined) {
      if (
        xmlTemplateForm.get("xmlTemplateFileName").value !== null ||
        xmlTemplateForm.get("xmlTemplateFileName").value !== undefined
      ) {
        let formData = new FormData();
        formData.append(
          "xmlTemplateId",
          xmlTemplateForm.get("xmlTemplateId").value
        );
        formData.append(
          "filename",
          xmlTemplateForm.get("xmlTemplateFileName").value
        );

        this.adminTemplateManagementService
          .downloadXmlTemplateAttachment(formData)
          .subscribe(
            (res: any) => {
              if (res.status === 200 || res.status === 0) {
                this.adminTemplateManagementService.convertBase64FileToSafeLink(
                  res.data
                );
              } else {
                this.templateModalService.openModal(
                  "แจ้งเตือน",
                  "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
                );
              }
            },
            error => {
              this.templateModalService.openModal(
                "แจ้งเตือน",
                "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
              );
            }
          );
      }
    }
  }

  downloadPdfTemplate(pdfTemplateId) {
    if (pdfTemplateId !== null && pdfTemplateId !== undefined) {
      if (pdfTemplateId == 0) {
        pdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
      }
      let formData = new FormData();
      formData.append("pdfTemplateId", pdfTemplateId);

      this.adminTemplateManagementService
        .downloadPdfTemplateAttachment(formData)
        .subscribe(
          (res: any) => {
            if (res.status === 200 || res.status === 0) {
              this.adminTemplateManagementService.convertBase64FileToSafeLink(
                res.data
              );
            } else {
              this.templateModalService.openModal(
                "แจ้งเตือน",
                "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
              );
            }
          },
          error => {
            this.templateModalService.openModal(
              "แจ้งเตือน",
              "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
            );
          }
        );
    }
  }

  downloadPdfDemo(pdfTemplateId) {
    if (pdfTemplateId !== null && pdfTemplateId !== undefined) {
      if (pdfTemplateId == 0) {
        pdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
      }
      let formData = new FormData();
      formData.append("pdfTemplateId", pdfTemplateId);

      this.adminTemplateManagementService
        .downloadPdfDemoAttachment(formData)
        .subscribe(
          (res: any) => {
            if (res.status === 200 || res.status === 0) {
              this.adminTemplateManagementService.convertBase64FileToSafeLink(
                res.data
              );
            } else {
              this.templateModalService.openModal(
                "แจ้งเตือน",
                "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
              );
            }
          },
          error => {
            this.templateModalService.openModal(
              "แจ้งเตือน",
              "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
            );
          }
        );
    }
  }

  previewPdfTemplate(pdfTemplateId) {
    if (pdfTemplateId !== null && pdfTemplateId !== undefined) {
      if (pdfTemplateId == 0) {
        pdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
      }
      let formData = new FormData();
      formData.append("pdfTemplateId", pdfTemplateId);

      this.adminTemplateManagementService
        .downloadPdfTemplateAttachment(formData)
        .subscribe(
          (res: any) => {
            if (res.status === 200 || res.status === 0) {
              this.adminTemplateManagementService.openFileOnNewTab(res.data);
            } else {
              this.templateModalService.openModal(
                "แจ้งเตือน",
                "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
              );
            }
          },
          error => {
            this.templateModalService.openModal(
              "แจ้งเตือน",
              "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
            );
          }
        );
    }
  }

  getPdfAttachmentLists(pdfTemplateId) {
    this.adminTemplateManagementService.getPdfAttachmentLists(pdfTemplateId).subscribe(
      resultType => {
        this.pdfAttachmentList = resultType.data;
      },
      err => console.log("timeout")
    );
  }

  downloadPdfResource(pdfTemplateAttachmentId, pdfTemplateId) {
    if (pdfTemplateId !== null && pdfTemplateId !== undefined) {
      if (pdfTemplateId == 0) {
        pdfTemplateId = this.pdfTemplateDetailForm.get("pdfTemplateId").value;
      }
      let formData = new FormData();
      formData.append("pdfTemplateId", pdfTemplateId);
      formData.append("pdfTemplateAttachmentId", pdfTemplateAttachmentId)

      this.adminTemplateManagementService
        .downloadPdfResourceAttachment(formData)
        .subscribe(
          (res: any) => {
            if (res.status === 200 || res.status === 0) {
              this.adminTemplateManagementService.convertBase64FileToSafeLink(
                res.data
              );
            } else {
              this.templateModalService.openModal(
                "แจ้งเตือน",
                "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
              );
            }
          },
          error => {
            this.templateModalService.openModal(
              "แจ้งเตือน",
              "ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง"
            );
          }
        );
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  get pdfTemplateName() {
    return this.pdfTemplateDetailForm.get("pdfTemplateName");
  }
  get startDateControl(): AbstractControl {
    return this.pdfTemplateDetailForm.get("startDate");
  }
  get endDateControl(): AbstractControl {
    return this.pdfTemplateDetailForm.get("endDate");
  }
  get pdfTemplateDemoFileNameControl() {
    return this.pdfTemplateDetailForm.get("pdfTemplateDemoFileName");
  }

  get email() {
    return this.xmlTemplateDetailForm.get("email");
  }
  get systemName() {
    return this.xmlTemplateDetailForm.get("systemName");
  }
  get xmlTemplateName() {
    return this.xmlTemplateDetailForm.get("xmlTemplateName");
  }
}
