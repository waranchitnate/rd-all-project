import { Component, OnInit, HostListener } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { PageResponse } from "src/app/shared/models/page-response";
import { PageRequest } from "src/app/shared/models/page-request";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { DropdownMPdfTemplateStatus } from "src/app/modules/pdf/models/vuser-xml-template.model";
import { AdminTemplateManagementService } from "src/app/modules/pdf/services/admin-template-management.service";

@Component({
  selector: "app-pdfp010100-search",
  templateUrl: "./pdfp010100-search.component.html",
  styleUrls: ["./pdfp010100-search.component.css"]
})
export class Pdfp010100SearchComponent implements OnInit {
  xmlTemplateList: Array<any>;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  currentSortField = "XML_TEMPLATE_CODE DESC, PDF_TEMPLATE_CODE";
  currentSortDirection = "DESC";
  LIMITS = [
    { key: "10", value: 10 },
    { key: "25", value: 25 },
    { key: "50", value: 50 },
    { key: "100", value: 100 }
  ];
  windowScrolled: boolean;
  modalReference: NgbModalRef;

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  pdfp010100SearchCondition = this.fb.group({
    departmentName: [""],
    systemName: [""],
    xmlTemplateId: [""],
    xmlTemplateName: [""],
    xmlTemplateCode: [""],
    pdfTemplateCode: [""],
    pdfTemplateName: [""],
    pdfTemplateStatusId: [""],
    pdfTemplateStatusName: [""]
  });

  dropdownMPdfTemplateStatus = <DropdownMPdfTemplateStatus[]>[];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private vuserpdfService: AdminTemplateManagementService
  ) { }

  ngOnInit() {
    this.pageRequest.sortFieldName = this.currentSortField;
    this.pageRequest.sortDirection = this.currentSortDirection;
    this.getData();
    this.getAllMPdfTemplateStatus();
  }

  clear() {
    this.pdfp010100SearchCondition.reset();
    this.pdfp010100SearchCondition.get("xmlTemplateCode").setValue("");
    this.pdfp010100SearchCondition.get("pdfTemplateStatusId").setValue("");
    this.getData();
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = this.currentSortField;
    this.pageRequest.sortDirection = this.currentSortDirection;
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  sort(fieldName) {
    this.currentSortField = fieldName;
    if (this.pageRequest.sortFieldName === this.currentSortField) {
      this.currentSortDirection =
        this.pageRequest.sortDirection === "ASC" ? "DESC" : "ASC";
      this.pageRequest.sortDirection = this.currentSortDirection;
    } else {
      this.pageRequest.sortFieldName = this.currentSortField;
      this.currentSortDirection = "ASC";
      this.pageRequest.sortDirection = "ASC";
    }
    this.getData();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  getData() {
    const criteria = this.pdfp010100SearchCondition.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.vuserpdfService.getSearchCriteria(criteria).subscribe((res: any) => {
      this.pageResponse = res.data;
      this.pageResponse.totalPages = res.data.totalElements;
    });
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop > 100
    ) {
      this.windowScrolled = true;
    } else if (
      (this.windowScrolled && window.pageYOffset) ||
      document.documentElement.scrollTop ||
      document.body.scrollTop < 10
    ) {
      this.windowScrolled = true;
    }
  }
  scrollToTop() {
    (function smoothscroll() {
      const currentScroll =
        document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - currentScroll / 8);
      }
    })();
  }

  // goToEdit(row) {
  //   this.router.navigateByUrl("/pdf/admin/pdfp010100/edit/" + row.taxTypeId);
  // }

  viewXmlTemplateDetail(xmlTemplateId) {
    this.router.navigate(["/pdf/admin/pdfp010100/edit-xml"], {
      queryParams: { xmlTemplateId }
    });
  }

  approveXmlTemplateDetail(xmlTemplateId) {
    this.router.navigate(["/pdf/admin/pdfp010100/edit-xml"], {
      queryParams: { xmlTemplateId }
    });
  }

  getAllMPdfTemplateStatus() {
    this.vuserpdfService.getAllMPdfTemplateStatus().subscribe(
      resultType => {
        this.dropdownMPdfTemplateStatus = resultType.data;
      },
      err => console.log("timeout")
    );
  }
}
