import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from "../../shared/shared.module";

import { PdfRoutingModule } from './pdf-routing.module';
import { AdministratorModule } from "./modules/administrator/administrator.module";
import { UserModule } from "./modules/user/user.module";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PdfRoutingModule,
    RouterModule,
    SharedModule,
    AdministratorModule,
    UserModule
  ]
})
export class PdfModule { }
