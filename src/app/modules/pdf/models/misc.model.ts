export interface RdbOffice {
  officeId: string;
  officeCode: string;
  shortDescription: string;
  longDescription: string;
}
