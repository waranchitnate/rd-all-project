import { XmlTemplateDetail } from "./xml-template-detail.model";

export class PdfTemplateDetail {
  xmlTemplate: XmlTemplateDetail;
  xmlTemplateIdBatch: number;
  pdfTemplateId: number;
  pdfTemplateCode: string;
  pdfTemplateName: string;
  pdfTemplateDesc: string;
  pdfTemplateFileName: string;
  pdfTemplateFilePath: string;
  pdfTemplateDemoFileName: string;
  pdfTemplateDemoFilePath: string;
  startDate: Date;
  endDate: Date;
  statusId: number;
  remark: string;
}
