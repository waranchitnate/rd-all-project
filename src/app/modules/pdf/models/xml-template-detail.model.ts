export class XmlTemplateDetail {
  xmlTemplateId: number;
  xmlTemplateCode: string;
  xmlTemplateName: string;
  xmlTemplateFileName: string;
  systemName: string;
  departmentCode: string;
  departmentName: string;
  email: string;
  requestBy: string;
}
