export interface VUserXmlTemplate {
  xmlTemplateId: number;
  xmlTemplateName: string;
  pdfTemplateId: number;
  pdfTemplateName: string;
  systemName: string;
  startDate: Date;
  endDate: Date;
  status: string;
}

export interface DropdownMPdfTemplateStatus {
  pdfTemplateStatusId: number;
  pdfTemplateStatusName: string;
}
