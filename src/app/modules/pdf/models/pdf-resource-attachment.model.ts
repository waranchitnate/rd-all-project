export class PdfResourceAttachment {
  pdfTemplateAttachmentId: number;
  filePath: string;
  // attachmentTypeId: number;
  // attachmentTypeName: string;
  file?: File;
  fileName?: string;
  fileEncode?: string;
}

export class PdfTemplateAttachment {
  pdfTemplateAttachmentId: number;
  fileName: string;
  pdfTemplateId: number;
}