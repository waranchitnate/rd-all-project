export interface MPdfTemplatestatus {
  pdfTemplateStatusId: number;
  pdfTemplateStatusName: string;
  pdfTemplateStatusDesc: string;
  createdDate: Date;
  createdBy: any;
  updatedDate: Date;
  updatedBy: any;
}
