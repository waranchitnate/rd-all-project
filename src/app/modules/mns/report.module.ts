import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportRoutingModule } from './report-routing.module';
import { Mnsp010100Component } from './page/mnsp010100/mnsp010100.component';
import { Mnsp010101Component } from './page/mnsp010101/mnsp010101.component';
import { Mnsp010102Component } from './page/mnsp010102/mnsp010102.component';
import { Mnsp010103Component } from './page/mnsp010103/mnsp010103.component';

import { Mnsp010200Component } from './page/mnsp010200/mnsp010200.component';
import { Mnsp010201Component } from './page/mnsp010201/mnsp010201.component';

import { Mnsp010300Component } from './page/mnsp010300/mnsp010300.component';
import { Mnsp010301Component } from './page/mnsp010301/mnsp010301.component';
import { Mnsp010302Component } from './page/mnsp010302/mnsp010302.component';
import { Mnsp010303Component } from './page/mnsp010303/mnsp010303.component';

import { Mnsp010400Component } from './page/mnsp010400/mnsp010400.component';
import { Mnsp010401Component } from './page/mnsp010401/mnsp010401.component';

import { Mnsp010500Component } from './page/mnsp010500/mnsp010500.component';
import { Mnsp010501Component } from './page/mnsp010501/mnsp010501.component';
import { Mnsp010502Component } from './page/mnsp010502/mnsp010502.component';

import { Mnsp010600Component } from './page/mnsp010600/mnsp010600.component';
import { Mnsp010601Component } from './page/mnsp010601/mnsp010601.component';

import { Mnsp010700Component } from './page/mnsp010700/mnsp010700.component';

import { Mnsp010800Component } from './page/mnsp010800/mnsp010800.component';

import { Mnsp010900Component } from './page/mnsp010900/mnsp010900.component';

import { Mnsp011000Component } from './page/mnsp011000/mnsp011000.component';

import { Mnsp011100Component } from './page/mnsp011100/mnsp011100.component';

import { Mnsp011200Component } from './page/mnsp011200/mnsp011200.component';

import { Mnsp011300Component } from './page/mnsp011300/mnsp011300.component';
import { Mnsp011301Component } from './page/mnsp011301/mnsp011301.component';

import { Mnsp011400Component } from './page/mnsp011400/mnsp011400.component';

import { Mnsp011500Component } from './page/mnsp011500/mnsp011500.component';

import { Mnsp011600Component } from './page/mnsp011600/mnsp011600.component';
import { Mnsp011601Component } from './page/mnsp011601/mnsp011601.component';

import { Mnsp011700Component } from './page/mnsp011700/mnsp011700.component';
import { Mnsp011701Component } from './page/mnsp011701/mnsp011701.component';

import { Mnsp011800Component } from './page/mnsp011800/mnsp011800.component';
import { Mnsp011801Component } from './page/mnsp011801/mnsp011801.component';
import { Mnsp011802Component } from './page/mnsp011802/mnsp011802.component';

import { Mnsp011900Component } from './page/mnsp011900/mnsp011900.component';
import { Mnsp011901Component } from './page/mnsp011901/mnsp011901.component';

import { Mnsp012000Component } from './page/mnsp012000/mnsp012000.component';

import { Mnsp012100Component } from './page/mnsp012100/mnsp012100.component';

import { ProgramService } from './service/program-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { TooltipModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [Mnsp010100Component,Mnsp010101Component,Mnsp010102Component,Mnsp010103Component,
                 Mnsp010200Component,Mnsp010201Component,
                 Mnsp010300Component,Mnsp010301Component,Mnsp010302Component,Mnsp010303Component,
                 Mnsp010400Component,Mnsp010401Component,
                 Mnsp010500Component,Mnsp010501Component,Mnsp010502Component,
                 Mnsp010600Component,Mnsp010601Component,
                 Mnsp010700Component,
                 Mnsp010800Component,
                 Mnsp010900Component,
                 Mnsp011000Component,
                 Mnsp011100Component,
                 Mnsp011200Component,
                 Mnsp011300Component,Mnsp011301Component,
                 Mnsp011400Component,
                 Mnsp011500Component,
                 Mnsp011600Component,Mnsp011601Component,
                 Mnsp011700Component,Mnsp011701Component,
                 Mnsp011800Component,Mnsp011801Component,Mnsp011802Component,
                 Mnsp011900Component,Mnsp011901Component,
                 Mnsp012000Component,
                 Mnsp012100Component],
  imports: [
    CommonModule,
    ReportRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TooltipModule,
  ],
  providers: [ProgramService]
})
export class ReportModule { }
