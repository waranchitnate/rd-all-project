import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010502Component } from './mnsp010502.component';

describe('Mnsp010502Component', () => {
  let component: Mnsp010502Component;
  let fixture: ComponentFixture<Mnsp010502Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010502Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010502Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
