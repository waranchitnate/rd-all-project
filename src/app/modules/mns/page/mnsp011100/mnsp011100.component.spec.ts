import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011100Component } from './mnsp011100.component';

describe('Mnsp011100Component', () => {
  let component: Mnsp011100Component;
  let fixture: ComponentFixture<Mnsp011100Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011100Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011100Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
