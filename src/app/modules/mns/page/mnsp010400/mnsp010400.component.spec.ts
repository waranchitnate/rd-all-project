import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010400Component } from './mnsp010400.component';

describe('Mnsp010400Component', () => {
  let component: Mnsp010400Component;
  let fixture: ComponentFixture<Mnsp010400Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010400Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010400Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
