import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010102Component } from './mnsp010102.component';

describe('Mnsp010102Component', () => {
  let component: Mnsp010102Component;
  let fixture: ComponentFixture<Mnsp010102Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010102Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010102Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
