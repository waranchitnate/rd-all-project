import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011901Component } from './mnsp011901.component';

describe('Mnsp011901Component', () => {
  let component: Mnsp011901Component;
  let fixture: ComponentFixture<Mnsp011901Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011901Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011901Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
