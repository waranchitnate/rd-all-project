import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011801Component } from './mnsp011801.component';

describe('Mnsp011801Component', () => {
  let component: Mnsp011801Component;
  let fixture: ComponentFixture<Mnsp011801Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011801Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011801Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
