import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010500Component } from './mnsp010500.component';

describe('Mnsp010500Component', () => {
  let component: Mnsp010500Component;
  let fixture: ComponentFixture<Mnsp010500Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010500Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010500Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
