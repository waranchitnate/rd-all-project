import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010600Component } from './mnsp010600.component';

describe('Mnsp010600Component', () => {
  let component: Mnsp010600Component;
  let fixture: ComponentFixture<Mnsp010600Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010600Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010600Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
