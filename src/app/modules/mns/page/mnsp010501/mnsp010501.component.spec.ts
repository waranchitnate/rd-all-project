import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010501Component } from './mnsp010501.component';

describe('Mnsp010501Component', () => {
  let component: Mnsp010501Component;
  let fixture: ComponentFixture<Mnsp010501Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010501Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010501Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
