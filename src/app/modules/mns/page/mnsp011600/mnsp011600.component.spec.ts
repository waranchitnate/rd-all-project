import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011600Component } from './mnsp011600.component';

describe('Mnsp011600Component', () => {
  let component: Mnsp011600Component;
  let fixture: ComponentFixture<Mnsp011600Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011600Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011600Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
