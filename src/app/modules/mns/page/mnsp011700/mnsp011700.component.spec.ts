import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011700Component } from './mnsp011700.component';

describe('Mnsp011700Component', () => {
  let component: Mnsp011700Component;
  let fixture: ComponentFixture<Mnsp011700Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011700Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011700Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
