import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010303Component } from './mnsp010303.component';

describe('Mnsp010303Component', () => {
  let component: Mnsp010303Component;
  let fixture: ComponentFixture<Mnsp010303Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010303Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010303Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
