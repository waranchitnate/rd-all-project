import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010302Component } from './mnsp010302.component';

describe('Mnsp010302Component', () => {
  let component: Mnsp010302Component;
  let fixture: ComponentFixture<Mnsp010302Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010302Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010302Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
