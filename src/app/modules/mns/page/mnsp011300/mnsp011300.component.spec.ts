import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011300Component } from './mnsp011300.component';

describe('Mnsp011300Component', () => {
  let component: Mnsp011300Component;
  let fixture: ComponentFixture<Mnsp011300Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011300Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011300Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
