import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010101Component } from './mnsp010101.component';

describe('Mnsp010101Component', () => {
  let component: Mnsp010101Component;
  let fixture: ComponentFixture<Mnsp010101Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
