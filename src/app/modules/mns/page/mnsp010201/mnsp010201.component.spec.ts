import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010201Component } from './mnsp010201.component';

describe('Mnsp010201Component', () => {
  let component: Mnsp010201Component;
  let fixture: ComponentFixture<Mnsp010201Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010201Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010201Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
