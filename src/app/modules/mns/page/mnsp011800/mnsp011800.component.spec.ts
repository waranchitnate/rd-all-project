import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011800Component } from './mnsp011800.component';

describe('Mnsp011800Component', () => {
  let component: Mnsp011800Component;
  let fixture: ComponentFixture<Mnsp011800Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011800Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011800Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
