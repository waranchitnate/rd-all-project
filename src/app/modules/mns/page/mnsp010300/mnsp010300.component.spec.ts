import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010300Component } from './mnsp010300.component';

describe('Mnsp010300Component', () => {
  let component: Mnsp010300Component;
  let fixture: ComponentFixture<Mnsp010300Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010300Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010300Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
