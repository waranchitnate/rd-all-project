import { Component, OnInit, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { Group } from 'src/app/modules/admin/group-management/models/group.model';
import { ProgramModel } from 'src/app/core/models/program.model';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { FormControl, FormBuilder, FormGroup, ValidatorFn, AbstractControl, ValidationErrors, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-search',
  templateUrl: './mnsp010103.component.html',
  styleUrls: ['./mnsp010103.component.css']
})
export class Mnsp010103Component implements OnInit {
  private _getmnsp010103Url: string = environment.mnsApiUrl + 'mnsp0101/mnsp010103';

  searchForm: FormGroup;
  receiveDate: FormControl;
  paymentDate: FormControl;
  dataTypeID: FormControl;
  statusGrp: FormControl;
  senderID: FormControl;
  stateID: FormControl;
  fileName: FormControl;
  receiveData: any[] = [];

  programs: ProgramModel[];
  currentPage = 1;
  totalItem = 0;
  pageSize = 10;
  @ViewChild(FloatButtonComponent) floatComp;

  constructor(private activatedRoute: ActivatedRoute, private fb: FormBuilder, private http: HttpClient) {
  }

  ngOnInit() {
    //this.floatComp.urlAdd = '/thumb-drive/form';
    //this.floatComp.tooltipMsg = 'เพิ่มข้อมูล อุปกรณ์ Token';
    //this.search();
    this.searchForm = new FormGroup({
      receiveDate: this.fb.control(this.activatedRoute.snapshot.paramMap.get('receiveDate')),
      paymentDate: this.fb.control(this.activatedRoute.snapshot.paramMap.get('paymentDate')),
      dataTypeID: this.fb.control(this.activatedRoute.snapshot.paramMap.get('dataTypeID')),
      statusGrp: this.fb.control(this.activatedRoute.snapshot.paramMap.get('statusGrp')),
      senderID: this.fb.control(this.activatedRoute.snapshot.paramMap.get('senderID')),
      stateID: this.fb.control(this.activatedRoute.snapshot.paramMap.get('stateID')),
      fileName: this.fb.control(this.activatedRoute.snapshot.paramMap.get('fileName'))
    });
      
    this.receiveDate= this.fb.control(this.activatedRoute.snapshot.paramMap.get('receiveDate')),
    this.paymentDate= this.fb.control(this.activatedRoute.snapshot.paramMap.get('paymentDate')),
    this.dataTypeID= this.fb.control(this.activatedRoute.snapshot.paramMap.get('dataTypeID')),
    this.statusGrp= this.fb.control(this.activatedRoute.snapshot.paramMap.get('statusGrp')),
    this.senderID= this.fb.control(this.activatedRoute.snapshot.paramMap.get('senderID')),
    this.stateID= this.fb.control(this.activatedRoute.snapshot.paramMap.get('stateID')),
    this.fileName= this.fb.control(this.activatedRoute.snapshot.paramMap.get('fileName'))
      this.search();
  }

  search() {
    this.getData(this.searchForm.value).subscribe((res) => {
      console.log(res.data);
      this.receiveData = res.data;
    });
  }

  getData(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._getmnsp010103Url+'/searchFileTransferLogLevel3', data);
  }
}
