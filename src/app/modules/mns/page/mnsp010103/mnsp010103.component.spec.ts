import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010103Component } from './mnsp010103.component';

describe('Mnsp010103Component', () => {
  let component: Mnsp010103Component;
  let fixture: ComponentFixture<Mnsp010103Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010103Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010103Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
