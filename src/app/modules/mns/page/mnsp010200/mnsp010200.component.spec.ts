import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010200Component } from './mnsp010200.component';

describe('Mnsp010200Component', () => {
  let component: Mnsp010200Component;
  let fixture: ComponentFixture<Mnsp010200Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010200Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010200Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
