import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011802Component } from './mnsp011802.component';

describe('Mnsp011802Component', () => {
  let component: Mnsp011802Component;
  let fixture: ComponentFixture<Mnsp011802Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011802Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011802Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
