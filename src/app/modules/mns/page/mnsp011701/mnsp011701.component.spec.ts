import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011701Component } from './mnsp011701.component';

describe('Mnsp011701Component', () => {
  let component: Mnsp011701Component;
  let fixture: ComponentFixture<Mnsp011701Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011701Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011701Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
