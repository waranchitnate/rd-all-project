import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010301Component } from './mnsp010301.component';

describe('Mnsp010301Component', () => {
  let component: Mnsp010301Component;
  let fixture: ComponentFixture<Mnsp010301Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010301Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010301Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
