import { Component, OnInit, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, ValidatorFn, AbstractControl, ValidationErrors, Validators } from '@angular/forms';
import { Group } from 'src/app/modules/admin/group-management/models/group.model';
import { ProgramModel } from 'src/app/core/models/program.model';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-search',
  templateUrl: './mnsp010301.component.html',
  styleUrls: ['./mnsp010301.component.css']
})
export class Mnsp010301Component implements OnInit {
  private _getmnsp010301Url: string = environment.mnsApiUrl + 'mnsp0103/mnsp010301';

  searchForm: FormGroup;
  processDate: FormControl;
  receiveDate: FormControl;
  paymentDate: FormControl;
  dataTypeID: FormControl;
  statusGrp: FormControl;
  senderID: FormControl;
  stateID: FormControl;
  
  receiveData: any[] = [];

  programs: ProgramModel[];
  currentPage = 1;
  totalItem = 0;
  pageSize = 10;
  @ViewChild(FloatButtonComponent) floatComp;

  constructor(private activatedRoute: ActivatedRoute, private fb: FormBuilder, private http: HttpClient) {
  }

  ngOnInit() {
    //this.floatComp.urlAdd = '/thumb-drive/form';
    //this.floatComp.tooltipMsg = 'เพิ่มข้อมูล อุปกรณ์ Token';
    //this.search();
    this.searchForm = new FormGroup({
      processDate: this.fb.control(this.activatedRoute.snapshot.paramMap.get('processDate')),
      receiveDate: this.fb.control(this.activatedRoute.snapshot.paramMap.get('receiveDate')),
      paymentDate: this.fb.control(this.activatedRoute.snapshot.paramMap.get('paymentDate')),
      dataTypeID: this.fb.control(this.activatedRoute.snapshot.paramMap.get('dataTypeID')),
      statusGrp: this.fb.control(this.activatedRoute.snapshot.paramMap.get('statusGrp')),
      senderID: this.fb.control(''),
      stateID: this.fb.control('')
    });
    /*this.receiveDate = this.fb.control(this.activatedRoute.snapshot.paramMap.get('receiveDate'));
    this.paymentDate = this.fb.control(this.activatedRoute.snapshot.paramMap.get('paymentDate'));
    this.dataTypeID = this.fb.control(this.activatedRoute.snapshot.paramMap.get('dataTypeID'));
    this.statusGrp = this.fb.control(this.activatedRoute.snapshot.paramMap.get('statusGrp'));
    this.senderID = this.fb.control(this.activatedRoute.snapshot.paramMap.get('senderID'));
    this.stateID = this.fb.control(this.activatedRoute.snapshot.paramMap.get('stateID'));*/

    /*console.log(this.receiveDate.value);
    console.log(this.paymentDate.value);
    console.log(this.dataTypeID.value);
    console.log(this.statusGrp.value);
    console.log(this.senderID.value);
    console.log(this.stateID.value);*/
    this.search();
    /*const url = require('url');
    const search_params = url.searchParams;

    const receiveDate = search_params.get('receiveDate');
    console.log(receiveDate);

    const paymentDate = search_params.get('paymentDate');
    console.log(paymentDate);

    const dataTypeID = search_params.get('dataTypeID');
    console.log(dataTypeID);

    const statusGrp = search_params.get('statusGrp');
    console.log(statusGrp);

    const senderID = search_params.get('senderID');
    console.log(senderID);

    const stateID = search_params.get('stateID');
    console.log(stateID);*/
  }

  search() {
    console.log(this.searchForm.value);
    this.getData(this.searchForm.value).subscribe((res) => {
      console.log(res.data);
      this.receiveData = res.data;
    });
  }

  getData(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._getmnsp010301Url+'/searchDataProcessLogLevel2', data);
  }

}
