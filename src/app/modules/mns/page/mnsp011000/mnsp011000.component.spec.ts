import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011000Component } from './mnsp011000.component';

describe('Mnsp011000Component', () => {
  let component: Mnsp011000Component;
  let fixture: ComponentFixture<Mnsp011000Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011000Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011000Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
