import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011400Component } from './mnsp011400.component';

describe('Mnsp011400Component', () => {
  let component: Mnsp011400Component;
  let fixture: ComponentFixture<Mnsp011400Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011400Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011400Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
