import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp012100Component } from './mnsp012100.component';

describe('Mnsp012100Component', () => {
  let component: Mnsp012100Component;
  let fixture: ComponentFixture<Mnsp012100Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp012100Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp012100Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
