import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011900Component } from './mnsp011900.component';

describe('Mnsp011900Component', () => {
  let component: Mnsp011900Component;
  let fixture: ComponentFixture<Mnsp011900Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011900Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011900Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
