import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp012000Component } from './mnsp012000.component';

describe('Mnsp012000Component', () => {
  let component: Mnsp012000Component;
  let fixture: ComponentFixture<Mnsp012000Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp012000Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp012000Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
