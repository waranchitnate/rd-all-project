import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010601Component } from './mnsp010601.component';

describe('Mnsp010601Component', () => {
  let component: Mnsp010601Component;
  let fixture: ComponentFixture<Mnsp010601Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010601Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010601Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
