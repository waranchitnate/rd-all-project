import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010900Component } from './mnsp010900.component';

describe('Mnsp010900Component', () => {
  let component: Mnsp010900Component;
  let fixture: ComponentFixture<Mnsp010900Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010900Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010900Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
