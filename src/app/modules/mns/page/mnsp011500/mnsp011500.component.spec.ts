import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011500Component } from './mnsp011500.component';

describe('Mnsp011500Component', () => {
  let component: Mnsp011500Component;
  let fixture: ComponentFixture<Mnsp011500Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011500Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011500Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
