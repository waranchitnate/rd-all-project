import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010800Component } from './mnsp010800.component';

describe('Mnsp010800Component', () => {
  let component: Mnsp010800Component;
  let fixture: ComponentFixture<Mnsp010800Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010800Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010800Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
