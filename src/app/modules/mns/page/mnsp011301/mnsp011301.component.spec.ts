import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011301Component } from './mnsp011301.component';

describe('Mnsp011301Component', () => {
  let component: Mnsp011301Component;
  let fixture: ComponentFixture<Mnsp011301Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011301Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011301Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
