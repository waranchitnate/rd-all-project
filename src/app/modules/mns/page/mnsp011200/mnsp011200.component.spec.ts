import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011200Component } from './mnsp011200.component';

describe('Mnsp011200Component', () => {
  let component: Mnsp011200Component;
  let fixture: ComponentFixture<Mnsp011200Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011200Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011200Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
