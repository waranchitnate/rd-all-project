import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010700Component } from './mnsp010700.component';

describe('Mnsp010700Component', () => {
  let component: Mnsp010700Component;
  let fixture: ComponentFixture<Mnsp010700Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010700Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010700Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
