import { Component, OnInit, HostListener, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, ValidatorFn, AbstractControl, ValidationErrors, Validators } from '@angular/forms';
import { Group } from 'src/app/modules/admin/group-management/models/group.model';
import { ProgramModel } from 'src/app/core/models/program.model';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './mnsp010100.component.html',
  styleUrls: ['./mnsp010100.component.css']
})
export class Mnsp010100Component implements OnInit {

  private _getmnsp010100Url: string = environment.mnsApiUrl + 'mnsp0101/mnsp010100';

  searchForm: FormGroup;
  fromDate: FormControl;
  fromTime: FormControl;
  toDate: FormControl;
  toTime: FormControl;
  senderType: FormControl;
  senderID: FormControl;
  dataTypeID: FormControl;
  stateID: FormControl;
  statusGrp: FormControl;
  receiveData: any[] = [];

  programs: ProgramModel[];
  currentPage = 1;
  totalItem = 0;
  pageSize = 10;
  @ViewChild(FloatButtonComponent) floatComp;


  constructor(private fb: FormBuilder, private http: HttpClient) { 
    this.initForm();
  }

  ngOnInit() {
    //this.floatComp.urlAdd = '/thumb-drive/form';
    //this.floatComp.tooltipMsg = 'เพิ่มข้อมูล อุปกรณ์ Token';
    //this.search();
  }

  initForm() {
    
    this.searchForm = new FormGroup({
      fromDate: new FormControl(),
      fromTime: new FormControl(),
      toDate: new FormControl(),
      toTime: new FormControl(),
      senderType: new FormControl(),
      senderID: new FormControl(),
      dataTypeID: new FormControl(),
      stateID: new FormControl(),
      statusGrp: new FormControl()
    });

    let fromDate = new Date();
    fromDate.setDate(fromDate.getDate() - 1);
    fromDate.setHours(0);
    fromDate.setMinutes(0);
    fromDate.setSeconds(0);
    this.fromDate = this.fb.control(fromDate);
    this.fromTime = this.fb.control('12:00');

    let toDate = new Date();
    toDate.setHours(23);
    toDate.setMinutes(59);
    toDate.setSeconds(59);
    this.toDate = this.fb.control(toDate);
    this.toTime = this.fb.control('12:00');
    this.senderType = this.fb.control('');
    this.senderID = this.fb.control('');

    this.dataTypeID = this.fb.control('');
    this.stateID = this.fb.control('');
    this.statusGrp = this.fb.control('');

    this.searchForm = this.fb.group({
      fromDate: this.fromDate,
      fromTime: this.fromTime,
      toDate: this.toDate,
      toTime: this.toTime,
      senderType: this.senderType,
      senderID: this.senderID,
      dataTypeID: this.dataTypeID,
      stateID: this.stateID,
      statusGrp: this.statusGrp
    }, {
        validators: [
        ]
    });

  }

  search() {
    this.getData(this.searchForm.value).subscribe((res) => {
      console.log(res.data);
      this.receiveData = res.data;
    });
  }

  getData(data): Observable<any> {
    //const  params = new  HttpParams({fromString:  'pin='+pin +'&newYear='+year});
    //console.log(params);
		return this.http.post<any>(this._getmnsp010100Url+'/searchFileTransferLogLevel1', data);
  }
}
