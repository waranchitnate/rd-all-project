import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010100Component } from './mnsp010100.component';

describe('Mnsp010100Component', () => {
  let component: Mnsp010100Component;
  let fixture: ComponentFixture<Mnsp010100Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010100Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010100Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
