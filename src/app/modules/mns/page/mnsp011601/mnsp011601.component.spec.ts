import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp011601Component } from './mnsp011601.component';

describe('Mnsp011601Component', () => {
  let component: Mnsp011601Component;
  let fixture: ComponentFixture<Mnsp011601Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp011601Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp011601Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
