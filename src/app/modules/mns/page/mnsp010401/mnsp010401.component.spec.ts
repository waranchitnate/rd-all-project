import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mnsp010401Component } from './mnsp010401.component';

describe('Mnsp010401Component', () => {
  let component: Mnsp010401Component;
  let fixture: ComponentFixture<Mnsp010401Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mnsp010401Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mnsp010401Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
