import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Mnsp010100Component } from './page/mnsp010100/mnsp010100.component';
import { Mnsp010101Component } from './page/mnsp010101/mnsp010101.component';
import { Mnsp010102Component } from './page/mnsp010102/mnsp010102.component';
import { Mnsp010103Component } from './page/mnsp010103/mnsp010103.component';

import { Mnsp010200Component } from './page/mnsp010200/mnsp010200.component';
import { Mnsp010201Component } from './page/mnsp010201/mnsp010201.component';

import { Mnsp010300Component } from './page/mnsp010300/mnsp010300.component';
import { Mnsp010301Component } from './page/mnsp010301/mnsp010301.component';
import { Mnsp010302Component } from './page/mnsp010302/mnsp010302.component';
import { Mnsp010303Component } from './page/mnsp010303/mnsp010303.component';

import { Mnsp010400Component } from './page/mnsp010400/mnsp010400.component';
import { Mnsp010401Component } from './page/mnsp010401/mnsp010401.component';

import { Mnsp010500Component } from './page/mnsp010500/mnsp010500.component';
import { Mnsp010501Component } from './page/mnsp010501/mnsp010501.component';
import { Mnsp010502Component } from './page/mnsp010502/mnsp010502.component';

import { Mnsp010600Component } from './page/mnsp010600/mnsp010600.component';
import { Mnsp010601Component } from './page/mnsp010601/mnsp010601.component';

import { Mnsp010700Component } from './page/mnsp010700/mnsp010700.component';

import { Mnsp010800Component } from './page/mnsp010800/mnsp010800.component';

import { Mnsp010900Component } from './page/mnsp010900/mnsp010900.component';

import { Mnsp011000Component } from './page/mnsp011000/mnsp011000.component';

import { Mnsp011100Component } from './page/mnsp011100/mnsp011100.component';

import { Mnsp011200Component } from './page/mnsp011200/mnsp011200.component';

import { Mnsp011300Component } from './page/mnsp011300/mnsp011300.component';
import { Mnsp011301Component } from './page/mnsp011301/mnsp011301.component';

import { Mnsp011400Component } from './page/mnsp011400/mnsp011400.component';

import { Mnsp011500Component } from './page/mnsp011500/mnsp011500.component';

import { Mnsp011600Component } from './page/mnsp011600/mnsp011600.component';
import { Mnsp011601Component } from './page/mnsp011601/mnsp011601.component';

import { Mnsp011700Component } from './page/mnsp011700/mnsp011700.component';
import { Mnsp011701Component } from './page/mnsp011701/mnsp011701.component';

import { Mnsp011800Component } from './page/mnsp011800/mnsp011800.component';
import { Mnsp011801Component } from './page/mnsp011801/mnsp011801.component';
import { Mnsp011802Component } from './page/mnsp011802/mnsp011802.component';

import { Mnsp011900Component } from './page/mnsp011900/mnsp011900.component';
import { Mnsp011901Component } from './page/mnsp011901/mnsp011901.component';

import { Mnsp012000Component } from './page/mnsp012000/mnsp012000.component';

import { Mnsp012100Component } from './page/mnsp012100/mnsp012100.component';

const mnsp010000routes: Routes = [{ path: 'mnsp010100', component: Mnsp010100Component}, 
                                      { path: 'mnsp010101/:receiveDate/:paymentDate/:dataTypeID/:statusGrp', component: Mnsp010101Component}, 
                                      { path: 'mnsp010102/:receiveDate/:paymentDate/:dataTypeID/:statusGrp/:stateID', component: Mnsp010102Component}, 
                                      { path: 'mnsp010103/:receiveDate/:paymentDate/:dataTypeID/:statusGrp/:stateID/:senderID/:fileName', component: Mnsp010103Component},
                                  { path: 'mnsp010200', component: Mnsp010200Component}, { path: 'mnsp010201', component: Mnsp010201Component},
                                  { path: 'mnsp010300', component: Mnsp010300Component}, 
                                      { path: 'mnsp010301/:processDate/:receiveDate/:paymentDate/:dataTypeID/:statusGrp', component: Mnsp010301Component}, 
                                      { path: 'mnsp010302/:processDate/:receiveDate/:paymentDate/:dataTypeID/:statusGrp/:stateID', component: Mnsp010302Component}, 
                                      { path: 'mnsp010303/:processDate/:receiveDate/:paymentDate/:dataTypeID/:statusGrp/:stateID/:senderID/:fileName', component: Mnsp010303Component},
                                  { path: 'mnsp010400', component: Mnsp010400Component}, { path: 'mnsp010401', component: Mnsp010401Component},
                                  { path: 'mnsp010500', component: Mnsp010500Component}, { path: 'mnsp010501', component: Mnsp010501Component}, { path: 'mnsp010502', component: Mnsp010502Component},
                                  { path: 'mnsp010600', component: Mnsp010600Component}, { path: 'mnsp010601', component: Mnsp010601Component},
                                  { path: 'mnsp010700', component: Mnsp010700Component},
                                  { path: 'mnsp010800', component: Mnsp010800Component},
                                  { path: 'mnsp010900', component: Mnsp010900Component},
                                  { path: 'mnsp011000', component: Mnsp011000Component},
                                  { path: 'mnsp011100', component: Mnsp011100Component},
                                  { path: 'mnsp011200', component: Mnsp011200Component},
                                  { path: 'mnsp011300', component: Mnsp011300Component}, { path: 'mnsp011301', component: Mnsp011301Component},
                                  { path: 'mnsp011400', component: Mnsp011400Component},
                                  { path: 'mnsp011500', component: Mnsp011500Component},
                                  { path: 'mnsp011600', component: Mnsp011600Component}, { path: 'mnsp011601', component: Mnsp011601Component},
                                  { path: 'mnsp011700', component: Mnsp011700Component}, { path: 'mnsp011701', component: Mnsp011701Component},
                                  { path: 'mnsp011800', component: Mnsp011800Component}, { path: 'mnsp011801', component: Mnsp011801Component}, { path: 'mnsp011802', component: Mnsp011802Component},
                                  { path: 'mnsp011900', component: Mnsp011900Component}, { path: 'mnsp011901', component: Mnsp011901Component},
                                  { path: 'mnsp012000', component: Mnsp012000Component},
                                  { path: 'mnsp012100', component: Mnsp012100Component}
                                ];

@NgModule({
  imports: [RouterModule.forChild(mnsp010000routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
