import { TestBed } from '@angular/core/testing';

import { CertUserRoutingGuardService } from './cert-user-routing-guard.service';

describe('CertUserRoutingGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CertUserRoutingGuardService = TestBed.get(CertUserRoutingGuardService);
    expect(service).toBeTruthy();
  });
});
