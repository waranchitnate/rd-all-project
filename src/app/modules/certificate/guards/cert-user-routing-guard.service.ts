import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import { CertURL } from '../certificate.url';

@Injectable({
  providedIn: 'root'
})
export class CertUserRoutingGuardService implements CanActivateChild {

  constructor(private router: Router) { }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const appRootAndChildRootUrl = state.url.replace(childRoute.routeConfig.path, '');
    const currentUrl = this.router.routerState.snapshot.url.replace(appRootAndChildRootUrl, '');
    const realRootUrl = appRootAndChildRootUrl.substr(0, appRootAndChildRootUrl.indexOf(CertURL.user.request));
    let canRoute: any = this.router.parseUrl(realRootUrl + 'search');
    if (childRoute.url.toString().indexOf(CertURL.user.step1) !== -1  && (currentUrl === '/cert/user/search')) {
      canRoute = true;
    } else if (childRoute.url.toString() === CertURL.user.step2 && (currentUrl.indexOf(CertURL.user.step1) !== -1  || currentUrl === CertURL.user.step3)) {
      canRoute = true;
    } else if (childRoute.url.toString() === CertURL.user.step3 && (currentUrl === CertURL.user.step2)) {
      canRoute = true;
    } else if (childRoute.url.toString() === CertURL.user.step4 && currentUrl === CertURL.user.step3) {
      canRoute = true;
    } else if (currentUrl === CertURL.user.step2) {
      canRoute = true;
    } else if (childRoute.url.toString().indexOf(CertURL.user.search) !== -1  && (currentUrl === CertURL.user.step4)) {
      canRoute = '/cert/user/search';
    }

    return canRoute;
  }
}
