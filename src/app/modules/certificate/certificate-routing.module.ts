import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CertURL} from './certificate.url';
import {ChangeCertificatePinComponent} from './modules/change-certificate-pin/change-certificate-pin.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'admin/home'},
  {path: CertURL.changePin, component: ChangeCertificatePinComponent},
  {path: CertURL.user.root , loadChildren: './modules/cert-user/cert-user.module#CertUserModule'},
  {path: CertURL.admin.root, loadChildren: './modules/cert-admin/cert-admin.module#CertAdminModule'},
  {path: CertURL.master.root, loadChildren: './modules/cert-master/cert-master.module#CertMasterModule'},
  {path: CertURL.deviceManagement.root, loadChildren: './modules/device-management/device-management.module#DeviceManagementModule'},
  {path: CertURL.thumbDrive.root, loadChildren: './modules/thumb-drive/thumb-drive.module#ThumbDriveModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificateRoutingModule {
}
