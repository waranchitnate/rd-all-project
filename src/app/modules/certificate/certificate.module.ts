import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CertificateRoutingModule} from './certificate-routing.module';
import {CertCommonModule} from './shared/cert-common.module';
import {CertUserModule} from './modules/cert-user/cert-user.module';
import {RouterModule} from '@angular/router';
import {CertAdminModule} from './modules/cert-admin/cert-admin.module';
import {SharedModule} from '../../shared/shared.module';
import { CertMasterModule } from './modules/cert-master/cert-master.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';
import { AlertModalComponent } from '../../shared/modals/alert-modal/alert-modal.component';
import { ChangeCertificatePinComponent } from './modules/change-certificate-pin/change-certificate-pin.component';
import { CertVerifyUsbComponent } from './modules/cert-verify-usb/cert-verify-usb.component';


@NgModule({
  declarations: [ChangeCertificatePinComponent, CertVerifyUsbComponent],
  imports: [
    CommonModule,
    CertificateRoutingModule,
    CertCommonModule,
    RouterModule,
    SharedModule,
    ReactiveFormsModule,
    UiSwitchModule,
    FormsModule
  ],
  exports: [
    AlertModalComponent,
    CertVerifyUsbComponent
  ]
})
export class CertificateModule {
}
