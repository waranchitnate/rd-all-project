import {CertificateBaseModel} from './certificate-base.model';
import {UserCertAttachment} from './user-cert-attachment.model';
import {CertificateDropdownModel} from './certificate-dropdown.model';

export class UserCertRequestFormModel {
  userCertId?: number;
  approvalStatusId?: number;
  certificateInfo?: CertificateInfo;
  certificateUserHolderInfo?: CertificateUserInfo;
  certificateUserRequestInfo?: CertificateUserInfo;
  userAttachmentDtoList?: AttachmentList;
}

export class CertificateInfo {
  reqId?: string | null;
  createdDate: Date;
  createdDateStr: string;
  userTypeId?: number;
  userTypeName?: string;
  deviceTypeId?: number;
  deviceTypeName?: string;
  requestTypeId?: number;
  requestTypeName?: string;
  templateTypeId?: number;
  templateTypeName?: string;
  objectiveCode?: string;
  objectiveName?: string;
  lifetime?: string;
  pinCode?: string;
  confirmPinCode?: string;
}

export class CertificateUserInfo {
  nid?: string | null;
  name?: string | null;
  orgCode?: string  | null;
  orgName?: string  | null;
  positionName?: string | null;
  email?: string | null;
  telephone?: string | null;
}

export class AttachmentList {
  attachments?: Array<UserCertAttachment>;
}
