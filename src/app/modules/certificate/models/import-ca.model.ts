export class ImportCaModel {
  id?: number;
  issuedName?: string;
  issuedBy?: string;
  uploadedBy?: string;
  uploadedDate?: Date;
  file?: File;
  fileName?: string;
  filePath?: string;
}
