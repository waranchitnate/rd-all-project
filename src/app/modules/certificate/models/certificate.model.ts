export class Certificate {
  certificateId: number;
  nationalId: string;
  taxPayer: string;
  certTypeId: number;
  certTypeName: string;
  certStatusId: number;
  passphrase: string;
  expiredYear: number;
  expiredDate: number;
  confirmPassphrase: string;
}
