import {MAttachmentType} from '../modules/cert-master/models/m-attachment-type.model';

export class UserCertAttachment {
  userAttachmentId: number;
  filePath: string;
  attachmentTypeId: number;
  attachmentTypeName: string;
  file?: File;
  fileName?: string;
  fileEncode?: string;
  isRequied: boolean;
  createdBy: string;
  userId: number;
  mattachmentType: MAttachmentType;
}
