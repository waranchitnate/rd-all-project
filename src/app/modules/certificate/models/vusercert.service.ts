import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {VUserCert} from './vusercert';
import {Observable, of} from 'rxjs';
import {ResponseT} from '../../../shared/models/response.model';
import {PageResponse} from '../../../shared/models/page-response';
import {MCertRequestType} from '../modules/cert-master/models/m-cert-request-type.model';
import {MApprovalstatus} from '../modules/cert-master/models/m-approval-status.model';
import {MUserType} from '../modules/cert-master/models/m-attachment-type.model';
import {FormGroup} from '@angular/forms';
import {UserCertAttachment} from './user-cert-attachment.model';
import {DeviceRegister} from '../modules/cert-master/models/device.model';
import {MCertReasonCode} from '../modules/cert-master/models/m-cert-reason-code.model';

@Injectable({
  providedIn: 'root'
})
export class VusercertService {
  url = environment.certApiUrl + 'certificate';

  mockupData= [
    {id: 1, startRequestedDate: '17/03/2563', IssuedName: 'Wisesoft Company',IssuedBy: 'Admin'}
  ];
  private master: Array<any> = this.mockupData;

  constructor(private http: HttpClient) {
  }

  public findAll(): Observable<Array<any>> {
    return of(this.master);
  }

  getSearchCriteria(vUserCert: VUserCert): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/search', vUserCert);
  }

  approveCerts(ids: number[]): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>>this.http.post(environment.certApiUrl + 'certificate/admin/approveCerts', ids);
  }

  getAllMCertRequestType(): Observable<ResponseT<MCertRequestType[]>> {
    return <Observable<ResponseT<MCertRequestType[]>>>this.http.get(this.url + '/findAllMCertRequestType');
  }

  getAllMApprovalStatus(): Observable<ResponseT<MApprovalstatus[]>> {
    return <Observable<ResponseT<MApprovalstatus[]>>>this.http.get(this.url + '/findAllMApprovalStatus');
  }

  getAllMUserType(): Observable<ResponseT<MUserType[]>> {
    return <Observable<ResponseT<MUserType[]>>>this.http.get(this.url + '/findAllMUserType');
  }

  getAllMcertReasonCode(): Observable<ResponseT<MCertReasonCode[]>> {
    return <Observable<ResponseT<MCertReasonCode[]>>>this.http.get(this.url + '/findAllMcertReasonCode');
  }

  getVuserCertById(rowNum: number): Observable<ResponseT<VUserCert>> {
    return <Observable<ResponseT<VUserCert>>>this.http.get(this.url + '/getVuserCertById?rowNum=' + rowNum);
  }

  UpdateApprovalStatusWithRemark(formVUerCertApprove: FormGroup): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>>this.http.post(environment.certApiUrl + 'certificate/admin/updateApprovalStatusWithRemark', formVUerCertApprove);
  }

  updateRequestTypeIdAndApprovalStatusIdByUserCertId(reasonCodeFormGroup: FormGroup): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>>this.http.post(this.url + '/updateUserCert', reasonCodeFormGroup);
  }

  getUserAttachmentByUsername(username: string): Observable<ResponseT<UserCertAttachment>> {
    return <Observable<ResponseT<UserCertAttachment>>>this.http.get(this.url + '/getUserAttachmentByUsername?username=' + username);
  }

  sendToDownloadCert(rowNum: number): Observable<VUserCert> {
    return <Observable<VUserCert>>this.http.get(environment.certApiUrl + 'certificate/admin/downloadCert/' + rowNum);
  }

  getAvailiableDevice(): Observable<ResponseT<DeviceRegister[]>> {
    return <Observable<ResponseT<DeviceRegister[]>>>this.http.get(this.url + '/getAvailiableDevice');
  }

  sendEmail(vUserCertId: number): Observable<VUserCert> {
    return <Observable<VUserCert>>this.http.get(environment.certApiUrl + 'certificate/admin/sendPFXFileViaEmail/' + vUserCertId);
  }
}
