export class InstallCertificateModel {
  reqId: string;
  pfxBase64: string;
  key: string;
  drive: string;
}

export class RequestDataModel {
  requestData: InstallCertificateModel[];
}

export class PreparedCertificateInfo {
  reqId?: string;
  userKey?: string;
  pfxBase64?: string;
}
