import {UserCert} from './user-cert.model';
import {UserCertAttachment} from './user-cert-attachment.model';
import {Certificate} from './certificate.model';

export class UserCertRegister {
  userCertId: number;
  userDto: UserCert;
  userAttachmentDtoList: Array<UserCertAttachment>;
  certificateDto: Certificate;
  objectiveCode: string;
  objective: string;
  objectiveDesc: string;
  deviceId: number;
  deviceName: string;
  certRequestTypeId: number;
  reqId: string;
  approvalStatusId: number;
  certRequestTypeDesc: string;
  createdDate?: Date;
  requestedDate: Date;
  approvedDate: Date;
  approvalStatusDesc: string;
}
