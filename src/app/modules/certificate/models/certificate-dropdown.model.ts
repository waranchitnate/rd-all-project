export interface CertificateDropdownModel {
  id?: number;
  code?: string;
  name?: string;
  value?: string;
}
