export interface Certification {

  userCertId: number;
  userId: number;
  taxId: string;
  taxPayer: string;
  dept: string;
  branchId: string;
  email: string;
  reqId: string;
  startDate: Date;
  endDate: Date;
  passPhrase: string;
  requestType: any;
  reasonCode: any;
  approvalStatus: any;
  approveBy: string;
  telephone: string;
  createdDate?: string;
  updatedDate?: Date;
  drive: string;
}

export interface ReasonCode {
  reasonCode: number;
  reasonDesc: string;
}

export interface RevokeCertDto {
  userCertId: number;
  reasonCode: number;
}


