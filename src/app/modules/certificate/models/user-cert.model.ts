export class UserCert {
  userId?: number;
  userName?: string;
  userTypeId?: number;
  orgCode?: string;
  orgName?: string;
  branch?: string;
  ipAddress?: string;
  idNumber?: string;
  email?: string;
  telephone?: string;
  userTypeName?: string;
  positionName: string;
}
