import { SendMailLogTable } from "./send-mail-log-form-table.model";
import { Pageable } from "./pageable-model";


export interface SendMailLogInformation{
    content: SendMailLogTable[];
    pageable: Pageable;
    last: boolean;
    totalPages: number;
    totalElements: number;
    numberOfElements: number;
    first: boolean;
    sort: object[];
    size: number;
    number: number;
}