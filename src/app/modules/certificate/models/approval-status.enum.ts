export enum ApprovalStatusEnum {
  SAVEDRAFFT = 1,
  WAIT_APPPROVE = 2,
  APPROVED = 3,
  UNAPPROVED = 6,
  REJECTED = 4
}
