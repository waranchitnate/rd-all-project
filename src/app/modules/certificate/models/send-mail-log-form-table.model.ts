export class SendMailLogTable{
    private name:     string;
    private userType: string;
    private certType: string;
    private email:    string;
    private details:  string;
    private sentDate: number;


    public getName(): string{ 
        return this.name; 
    }
    public setName(name: string): void{ 
        this.name = name; 
    }

    public getUserType(): string{ 
        return this.userType; 
    }
    public setUserType(userType: string): void{ 
        this.userType = userType; 
    }

    public getCertType(): string{ 
        return this.certType; 
    }
    public setCertType(certType: string): void{ 
        this.certType = certType; 
    }

    public getEmail(): string{ 
        return this.email; 
    }
    public setEmail(email: string): void{ 
        this.email = email; 
    }

    public getDetails(): string{ 
        return this.details; 
    }
    public setDetails(deatals: string): void{ 
        this.details = this.details; 
    }

    public getSentDate(): number{ 
        return this.sentDate; 
    }
    public setSentDate(sentDate: Date){
        this.sentDate = sentDate.getTime();
    }
}