export interface VUserCert {
    no: number;
    rowNum: number;
    reqId: string;
    requestedDate: Date;
    startRequestedDate: Date;
    endRequestedDate: Date;
    nid: string;
    name: string;
    expiredYear: string;
    DropdownMcerRequestType: DropdownMcertRequestType;
    certificateTypeId: number;
    certificateTypeName: string;
    userTypeId: number;
    userTypeName: string;
    approvalStatusId: number;
    approvalStatusName: string;
    objective: string;
    certSerialNumber: string;
    issueDate: Date;
    issueBy: string;
    expiredDate: Date;
    reasonCode: number;
    orgCode: string;
    orgName: string;
    branch: string;
    ipAddress: string;
    deviceId: number;
    addressId: number;
    userCertId: number;
    userId: number;
    certificateId: number;
    deviceName: string;
    serialNumber: string;
    deviceTypeId: number;
    deviceTypeName: string;
    deviceStatusName: string;
    dbType: string;
    status: string;
    remark: string;
    requestTypeId: number;
    requestTypeDesc: string;
    email: string;
    telephone: string;
    listcertificateTypeId :Array<number>
    dropdownMcertRequestType: DropdownMcertRequestType;
    dropdownMApprovalStatus: DropdownMApprovalStatus;
    dropdownMUserType: DropdownMUserType;
}

export interface DropdownMcertRequestType {
    requestTypeId: number;
    requestTypeDesc: string;
}

export interface DropdownMApprovalStatus {
    approvalStatusId: number;
    approvalStatusName: string;
}

export interface DropdownMUserType {
    userTypeId: number;
    userTypeName: string;
}
