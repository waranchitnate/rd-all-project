import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseT} from '../../../shared/models/response.model';
import {Certification, ReasonCode, RevokeCertDto} from '../models/certification';
import {ResponseDataModel} from '../../../core/models/response-data.model';
import {ChangePinResponseModel} from '../models/change-pin-response.model';

@Injectable({
  providedIn: 'root'
})

export class CertificateService {

  url = environment.certApiUrl + 'certificate/admin';
  certRegisUrl = environment.certApiUrl + 'certificate/register';
  certSenderUrl = environment.certApiUrl + 'certificate/sender';
  certificateApi = environment.certApiUrl + 'certificate/';
  certCommonApi = environment.certApiUrl + 'common';

  constructor(private http: HttpClient) {
  }

  getAllUserCert(): Observable<ResponseT<Certification[]>> {
    return <Observable<ResponseT<Certification[]>>>this.http.get(this.url + '/getAllUserRequestCert');
  }

  getAllUserCertByApprovalStatus(statusId: number): Observable<ResponseT<Certification[]>> {
    return <Observable<ResponseT<Certification[]>>>this.http.post(this.url + '/getAllUserRequestCertByApprovalStatus', statusId);
  }

  sendRequest(cert: Certification): Observable<ResponseT<Certification>> {
    return <Observable<ResponseT<Certification>>>this.http.post(this.url + '/sendRequest', cert);
  }

  approveCerts(ids: number[]): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>>this.http.post(this.url + '/approveCerts', ids);
  }

  revokeCert(cert: RevokeCertDto): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>>this.http.post(this.url + '/revokeCert', cert);
  }

  getAllReasonCode(): Observable<ResponseT<OrientationType[]>> {
    return <Observable<ResponseT<OrientationType[]>>>this.http.get(this.url + '/getAllReasonCode');
  }

  getAllObjective(): Observable<ResponseT<any[]>> {
  return <Observable<ResponseT<any[]>>>this.http.get(this.certCommonApi + '/getObjective');
}

  getActiveUserCert(): Observable<ResponseT<Certification[]>> {
    return <Observable<ResponseT<Certification[]>>>this.http.get(this.url + '/getActiveUsers');
  }

  saveUserDetailToLocalStorage(userDetail: object) {
    localStorage.setItem('userCertDetail', JSON.stringify(userDetail));
  }

  getDataFromLocalStorage() {
    const data = localStorage.getItem('userCertDetail');
    if (data) {
      return JSON.parse(data);
    }
  }

  public downloadRootCA(): Observable<any> {
    return this.http.get(this.certRegisUrl + '/downloadRootCA');
  }

  public downloadPfxFile(reqId: string): Observable<any> {
    return this.http.post(this.certRegisUrl + '/downloadCertificate-file', reqId);
  }

  public issueCertForSender(reqId: string): Observable<any> {
    return this.http.post(this.certSenderUrl + '/issue', reqId);
  }

  public getSenderPFXToLocal(reqId: string): Observable<any> {
    return this.http.post(this.certSenderUrl + '/getSenderPFXToLocal', reqId);
  }

  public changePin(oldPin: string, newPin: string, pfxBase64: string): Observable<ResponseDataModel<ChangePinResponseModel>> {
    const body = {
      requestData : [
        {
          oldPin: oldPin,
          newPin: newPin,
          pfxBase64: pfxBase64
        }
      ]
    };
    return this.http.post<ResponseDataModel<ChangePinResponseModel>>(this.certificateApi + 'change-pin-code', body);
  }
}
