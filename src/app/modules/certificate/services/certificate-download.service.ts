import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class CertificateDownloadService {

  constructor(private sanitizer: DomSanitizer) { }

  convertBase64FileToSafeLink(fileContent: string){
    const blob = new Blob([fileContent], { type: 'application/octet-stream' });
    // return <Observable<SafeResourceUrl>>this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
    return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  dataURItoBlob(dataURI, contentType) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: contentType });
    return blob;
 }

 openImageAnotherTab(file){
  const imageBlob = this.dataURItoBlob(file.dataBase64, 'application/octet-stream');
  // const imageFile = new File([imageBlob], { type: 'image/jpeg' });
  let url = URL.createObjectURL(imageBlob);
  window.open(url);
 }

 openImageAnotherTabFromCurrentFile(file){
  // const imageBlob = this.dataURItoBlob(file.dataBase64, file.contentType);
  // const imageFile = new File([imageBlob], { type: 'image/jpeg' });
  let url = URL.createObjectURL(file);
  window.open(url);
 }

  downloadFileIe(file) {
    if(window.navigator.msSaveOrOpenBlob) {
      console.log('ie' + file.fileBaseName);
      const byteCharacters = atob(file.dataBase64);
      const byteNumbers = new Array(byteCharacters.length);
      for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      const blob = new Blob([byteArray], file.contentType);
      window.navigator.msSaveOrOpenBlob(blob, file.fileBaseName +  '.' + file.fileExtension);
    } else {
      const fileBlob = this.dataURItoBlob(file.dataBase64, file.contentType);
      const url = window.URL.createObjectURL(fileBlob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = file.fileBaseName;
      a.click();
    }
  }

}
