import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseT } from 'src/app/shared/models/response.model';
import { DropDownList } from '../models/drop-down-list.model';
import { SendMailLogInformation } from '../models/send-mail-log-information.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';

@Injectable({
  providedIn: 'root'
})
export class SentMailLogService {

   requestedDistination: string = environment.certApiUrl + 'certificate/sendmaillog/';

constructor(private xmlRequester: HttpClient){  }

getMailType(): Observable<ResponseT<DropDownList[]>>{
  return this.xmlRequester.get(
      this.requestedDistination + 'getMailType'
  ) as Observable<ResponseT<DropDownList[]>>;
}

getCertType(): Observable<ResponseT<DropDownList[]>>{
  return this.xmlRequester.get(
      this.requestedDistination + 'getCertType'
  ) as Observable<ResponseT<DropDownList[]>>;
}

getUserType(): Observable<ResponseT<DropDownList[]>>{
  return this.xmlRequester.get(
      this.requestedDistination + 'getUserType'
  ) as Observable<ResponseT<DropDownList[]>>;
}

search(criteria: JSON, page: number, numPerPage: number, pageRequestAssistor: PageRequest):
  Observable<ResponseT<SendMailLogInformation>> {
  var counter: number = 0;
  for(let crite in criteria){
      if(criteria[crite] !== null) counter++;
  }
  criteria["numberOfcriteria"] = counter;
  criteria["page"] = page;
  criteria["elementPerPage"] = numPerPage;
  criteria["pageRequestDto"] = pageRequestAssistor;


  return this.xmlRequester.post(
      this.requestedDistination + "search", criteria
  ) as Observable<ResponseT<SendMailLogInformation>>;
}
}
