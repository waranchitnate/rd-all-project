import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseT} from '../../../shared/models/response.model';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthenticationService} from '../../../core/authentication/authentication.service';
import {InstallCertificateModel, PreparedCertificateInfo} from '../models/install-certificate.model';

@Injectable({
  providedIn: 'root'
})
export class SignatureService {

  certUrl = environment.certApiUrl + 'signature'
  caUrl = environment.caApiUrl + 'sign';

  constructor(private authenticationService: AuthenticationService,
              private http: HttpClient) { }

  headers = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
    'SkipAuth': '1'
  })

  getLocalDrives(): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>> this.http.post(this.caUrl + '/listAllDrives', null, { headers: this.headers });
  }

  verifyUSB(reqObj: any): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>> this.http.post(this.caUrl + '/verifyUSB', reqObj);
  }

  savePFXIntoUsb(cert: any): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>> this.http.post(this.caUrl + '/savePFX', cert);
  }
  genKeyAndCSR(cert: any): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>> this.http.post(this.caUrl + '/genKeyAndCSR', cert);
  }
  saveCerToToken(cert: any): Observable<ResponseT<Response>> {
    return <Observable<ResponseT<Response>>> this.http.post(this.caUrl + '/saveCerToToken', cert);
  }
  issueCerToken(cert: any): Observable<ResponseT<PreparedCertificateInfo>> {
    return <Observable<ResponseT<PreparedCertificateInfo>>> this.http.post(this.certUrl + '/issueCerToken', cert);
  }
  preparedInfo(reqId: string): Observable<ResponseT<PreparedCertificateInfo>> {
    return <Observable<ResponseT<PreparedCertificateInfo>>> this.http.post(this.certUrl + '/preparedInfo', reqId);
  }
  preparedInfoByUserName(username: string): Observable<ResponseT<PreparedCertificateInfo>> {
    return <Observable<ResponseT<PreparedCertificateInfo>>> this.http.post(this.certUrl + '/preparedInfoByUsername', username);
  }
  findUserInfoOfCertificate(reqId: string): Observable<ResponseT<PreparedCertificateInfo>> {
    return <Observable<ResponseT<PreparedCertificateInfo>>> this.http.post(this.certUrl + '/findUserInfoOfCertificate', reqId);
  }
  getUserKeyByUsername(username: string): Observable<ResponseT<string>> {
    return <Observable<ResponseT<string>>> this.http.post(this.certUrl + '/getUserKeyFromUsername', username);
  }
}
