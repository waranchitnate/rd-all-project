import {Injectable} from '@angular/core';
import {Step} from '../../../shared/models/step.model';
import {Subject, Subscription} from 'rxjs';
import {NavigationEnd, Router} from '@angular/router';
import {environment} from 'src/environments/environment';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CertificateStepService {

  url = environment.certApiUrl;
  finishStep = 0;
  currentStepSubject = new Subject<number>();
  currentStep = 0;
  previousStep = 0;

  info = null;

  address = null;

  upload = null;

  routeSub: Subscription;

  listStep: Step[] = [
    {circle: '1', title: 'ข้อมูลส่วนตัว', path: '/cert/user/request/home'},
    {circle: '2', title: 'ข้อมูลที่อยู่', path: '/cert/user/request/step1'},
    {circle: '3', title: 'อัพโหลดเอกสาร', path: '/cert/user/request/step2'},
    {circle: '4', title: 'ตรวจสอบข้อมูล', path: '/cert/user/request/step3'},
    {circle: '5', title: 'ตรวจสอบข้อมูล', path: '/cert/user/request/step4'},
    {circle: '6', title: 'สำเร็จ', path: '/cert/user/request/success'},
  ];

  constructor(private router: Router) {
    this.init();
  }

  init() {
    if (!this.routeSub || this.routeSub.closed) {
      this.routeSub = this.router.events.pipe(filter(ev => ev instanceof NavigationEnd)).subscribe((ev: NavigationEnd) => {
        const stepIndexMatch = this.listStep.findIndex(step => step.path === ev.url);
        this.currentStepSubject.next(stepIndexMatch);
        this.currentStep = stepIndexMatch;
        if (this.currentStep - this.finishStep > 1) {
          this.routeSub.unsubscribe();
          this.init();
        }
        if (this.currentStep === (this.listStep.length - 1)) {
          // this.finishRegister();
        }

      });
    }
    this.defaultData();
    this.setCurrentStep(this.finishStep + 1);
  }

  defaultData() {
    this.finishStep = -1;
    this.currentStep = 0;
    this.previousStep = 0;
    this.currentStepSubject = new Subject<number>();
    this.currentStepSubject.next(0);
    this.info = null;
    this.address = null;
    this.upload = null;
  }

  setCurrentStep(currentStep: number) {
    this.previousStep = this.currentStep;
    this.goToAddress(this.currentStep, true);
  }

  nextStep(replace = false) {
    this.previousStep = this.currentStep;
    this.finishStep = this.currentStep;
    const next = this.currentStep + 1;
    this.goToAddress(next, replace);
  }

  goToAddress(gotoStep, replace = false) {
    if (this.listStep[gotoStep].path) {
      this.router.navigate([this.listStep[gotoStep].path], {replaceUrl: replace});
    }
  }

  goBack(replace = false) {
    const previous = this.currentStep - 1;
    this.goToAddress(previous, replace);
  }

}
