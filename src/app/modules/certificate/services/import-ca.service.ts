import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ImportCaModel} from '../models/import-ca.model';
import {Observable} from 'rxjs';
import {ResponseT} from '../../../shared/models/response.model';
import {PageResponse} from '../../../shared/models/page-response';
import {Injectable} from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ImportCAService {

  importCAUrl = environment.certApiUrl + 'importCA';

  constructor(private http: HttpClient) { }

  search(condition: ImportCaModel): Observable<ResponseT<Array<ImportCaModel>>> {
    return <Observable<ResponseT<Array<ImportCaModel>>>>this.http.post(this.importCAUrl + '/searchImportCAByCondition', condition);
  }

  getImportCAById(id: number): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.importCAUrl + '/getImportCAById', id);
  }

  saveImportCA(importCaModel: ImportCaModel): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.importCAUrl + '/saveImportCA', importCaModel);
  }

  deleteImportCAById(id: number): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.importCAUrl + '/deleteImportCAById', id);
  }

  uploadCARootFile(formData): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.importCAUrl + '/uploadCARootFile', formData);
  }

  downloadFile(downloadForm): Observable<any> {
    return this.http.post(this.importCAUrl + '/downloadFile', downloadForm);
  }

}
