import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertMasterRoutingModule } from './cert-master-routing.module';
import { ApprovalStatusSearchComponent } from './pages/approval-status/approval-status-search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { TooltipModule, ModalModule } from 'ngx-bootstrap-th';
import { DeviceTypeSearchComponent } from './pages/device-type/device-type-search.component';
import { DeviceStatusSearchComponent } from './pages/device-status/device-status-search.component';
import { MCertRequestTypeSearchComponent } from './pages/m-cert-request-type/m-cert-request-type-search.component';
import { MCertReasonCodeSearchComponent } from './pages/m-cert-reason-code/m-cert-reason-code-search.component';
import { MAttachmentTypeSearchComponent } from './pages/m-attachment-type/m-attachment-type-search.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { MUserTypeSearchComponent } from './pages/m-user-type/m-user-type-search.component';
import { MObjectiveSearchComponent } from './pages/m-objective/m-objective-search/m-objective-search.component';

@NgModule({
  declarations: [ApprovalStatusSearchComponent, DeviceTypeSearchComponent, DeviceStatusSearchComponent, MCertRequestTypeSearchComponent, MCertReasonCodeSearchComponent, MAttachmentTypeSearchComponent, MUserTypeSearchComponent, MObjectiveSearchComponent],
  imports: [
    CommonModule,
    CertMasterRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TooltipModule,
    UiSwitchModule,
    ModalModule
  ]
})
export class CertMasterModule { }
