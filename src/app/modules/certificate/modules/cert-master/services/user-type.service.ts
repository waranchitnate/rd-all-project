import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseT } from '../../../../../shared/models/response.model';
import { MUserType } from '../models/m-attachment-type.model';
import { MUserTypeModel } from '../models/m-user-type.model';
import { PageResponse } from '../../../../../shared/models/page-response';

@Injectable({
  providedIn: 'root'
})
export class UserTypeService {

  url = environment.certApiUrl + 'certificate/master/mUserType';
  constructor(private http: HttpClient) { }

  getAllMUserType(): Observable<ResponseT<MUserType[]>> {
    return <Observable<ResponseT<MUserType[]>>>this.http.get(this.url + '/findAllMUserType');
  }
  getSearchCriteria(mUserType: MUserTypeModel): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/search', mUserType);
  }

  saveOrUpdateMUserType(mUserType: MUserTypeModel): Observable<ResponseT<MUserTypeModel[]>> {
    return <Observable<ResponseT<MUserTypeModel[]>>>this.http.post(this.url + '/saveOrUpdateMUserType', mUserType);
  }

  getMUserTypeById(userTypeId: number): Observable<ResponseT<MUserTypeModel>> {
    return <Observable<ResponseT<MUserTypeModel>>>this.http.get(this.url + '/getMUserTypeById?userTypeId=' + userTypeId);
  }

  deleteMUserType(userTypeId: number): Observable<ResponseT<MUserTypeModel[]>> {
    return <Observable<ResponseT<MUserTypeModel[]>>>this.http.get(this.url + '/deleteMUserType?userTypeId=' + userTypeId);
  }
}
