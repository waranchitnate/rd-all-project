import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { PageResponse } from '../../../../../shared/models/page-response';
import { MCertRequestType } from '../models/m-cert-request-type.model';
import { Observable } from 'rxjs';
import { ResponseT } from '../../../../../shared/models/response.model';

@Injectable({
  providedIn: 'root'
})
export class CertRequestTypeService {
  url = environment.certApiUrl + 'certificate/master/mCertRequestType';

  constructor(private http: HttpClient) { }

  getSearchCriteria(cerrequestType: MCertRequestType): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/search', cerrequestType);
  }

  saveOrUpdateCertRequestType(cerrequestType: MCertRequestType): Observable<ResponseT<MCertRequestType[]>> {
    return <Observable<ResponseT<MCertRequestType[]>>>this.http.post(this.url + '/saveOrUpdateCertRequestType', cerrequestType);
  }

  getCertRequestTypeById(requestTypeId: number): Observable<ResponseT<MCertRequestType>> {
    return <Observable<ResponseT<MCertRequestType>>>this.http.get(this.url + '/getCertRequestTypeById?requestTypeId=' + requestTypeId);
  }

  deleteCertRequestType(cerrequestType: MCertRequestType): Observable<ResponseT<MCertRequestType[]>> {
    return <Observable<ResponseT<MCertRequestType[]>>>this.http.post(this.url + '/deleteCertRequestType' , cerrequestType);
  }
}
