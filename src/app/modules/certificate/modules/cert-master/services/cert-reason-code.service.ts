import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { MCertReasonCode } from '../models/m-cert-reason-code.model';
import { PageResponse } from '../../../../../shared/models/page-response';
import { Observable } from 'rxjs';
import { ResponseT } from '../../../../../shared/models/response.model';

@Injectable({
  providedIn: 'root'
})
export class CertReasonCodeService {
  url = environment.certApiUrl + 'certificate/master/mCertReasonCode';

  constructor(private http: HttpClient) { }

  getSearchCriteria(certreasoncode: MCertReasonCode): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/search', certreasoncode);
  }

  saveOrUpdateCerReasonCode(certreasoncode: MCertReasonCode): Observable<ResponseT<MCertReasonCode[]>> {
    return <Observable<ResponseT<MCertReasonCode[]>>>this.http.post(this.url + '/saveOrUpdateCerReasonCode', certreasoncode);
  }

  getCerReasonCodeById(reasonCode: number): Observable<ResponseT<MCertReasonCode>> {
    return <Observable<ResponseT<MCertReasonCode>>>this.http.get(this.url + '/getCerReasonCodeById?reasonCode=' + reasonCode);
  }

  deleteCerReasonCode(certreasoncode: MCertReasonCode): Observable<ResponseT<MCertReasonCode[]>> {
    return <Observable<ResponseT<MCertReasonCode[]>>>this.http.post(this.url + '/deleteCerReasonCode' , certreasoncode);
  }
}
