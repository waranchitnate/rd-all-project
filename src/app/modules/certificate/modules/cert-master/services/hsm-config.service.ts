import { Injectable } from '@angular/core';
import {DeviceStatus} from '../models/device.model';
import {Observable} from 'rxjs';
import {ResponseT} from '../../../../../shared/models/response.model';
import {PageResponse} from '../../../../../shared/models/page-response';
import {environment} from '../../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {HsmConfig} from '../models/hsm-config';

@Injectable({
  providedIn: 'root'
})
export class HsmConfigService {

  hsmUrl = environment.certApiUrl + 'hsm';
  constructor(private http: HttpClient) { }

  LIMITS = [
    {key: '10', value: 10},
    {key: '25', value: 25},
    {key: '50', value: 50},
    {key: '100', value: 100}
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  search(): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.get(this.hsmUrl + '/getList');
  }

  getHSMById(id: number): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.hsmUrl + '/getHSMById', id);
  }

  saveHSM(hsmConfig: HsmConfig): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.hsmUrl + '/saveHSM', hsmConfig);
  }

  deleteHSMById(id: number): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.hsmUrl + '/deleteHSMById', id);
  }

}
