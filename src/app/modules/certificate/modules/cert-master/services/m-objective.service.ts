import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { MObjectiveModel } from '../models/m-objective.model';
import { Observable } from 'rxjs';
import { PageResponse } from 'src/app/shared/models/page-response';
import { ResponseT } from 'src/app/shared/models/response.model';

@Injectable({
  providedIn: 'root'
})
export class MObjectiveService {

  url = environment.certApiUrl + 'certificate/master/objective';
  constructor(private http: HttpClient) { }

  getSearchCriteria(mObjective: MObjectiveModel): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/search', mObjective);
  }

  saveOrUpdateMObjective(mObjective: MObjectiveModel): Observable<ResponseT<MObjectiveModel[]>> {
    return <Observable<ResponseT<MObjectiveModel[]>>>this.http.post(this.url + '/save', mObjective);
  }

  getMobjectiveByObjectiveCode(objectiveCode: String): Observable<ResponseT<MObjectiveModel>> {
    return <Observable<ResponseT<MObjectiveModel>>>this.http.get(this.url + '/getObjectiveByCode?objectiveCode=' + objectiveCode);
  }

  deleteMObjective(mObjective: MObjectiveModel): Observable<ResponseT<MObjectiveModel[]>> {
    return <Observable<ResponseT<MObjectiveModel[]>>>this.http.post(this.url + '/delete' , mObjective);
  }
}
