import { TestBed } from '@angular/core/testing';

import { HsmConfigService } from './hsm-config.service';

describe('HsmConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HsmConfigService = TestBed.get(HsmConfigService);
    expect(service).toBeTruthy();
  });
});
