import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import { DeviceStatus } from '../models/device.model';
import { PageResponse } from '../../../../../shared/models/page-response';
import { ResponseT } from '../../../../../shared/models/response.model';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class DeviceStatusService {
  url = environment.certApiUrl + 'certificate/master/mDeviceStatus';

  constructor(private http: HttpClient) { }

  getSearchCriteria(devicestatus: DeviceStatus): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/search', devicestatus);
  }

  saveOrUpdateDeviceStatus(devicestatus: DeviceStatus): Observable<ResponseT<DeviceStatus[]>> {
    return <Observable<ResponseT<DeviceStatus[]>>>this.http.post(this.url + '/saveOrUpdateDeviceStatus', devicestatus);
  }

  getDeviceStatusById(deviceStatusId: number): Observable<ResponseT<DeviceStatus>> {
    return <Observable<ResponseT<DeviceStatus>>>this.http.get(this.url + '/getDeviceStatusById?deviceStatusId=' + deviceStatusId);
  }

  deleteDeviceStatus(devicestatus: DeviceStatus): Observable<ResponseT<DeviceStatus[]>> {
    return <Observable<ResponseT<DeviceStatus[]>>>this.http.post(this.url + '/deleteDeviceStatus' , devicestatus);
  }
}
