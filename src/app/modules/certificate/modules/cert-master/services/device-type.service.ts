import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { DeviceType } from '../models/device.model';
import { PageResponse } from '../../../../../shared/models/page-response';
import { Observable } from 'rxjs';
import { ResponseT } from '../../../../../shared/models/response.model';

@Injectable({
  providedIn: 'root'
})
export class DeviceTypeService {
  url = environment.certApiUrl + 'certificate/master/mDeviceType';
  constructor(private http: HttpClient) { }

  getSearchCriteria(deviceType: DeviceType): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/search', deviceType);
  }

  saveOrUpdateDeviceType(deviceType: DeviceType): Observable<ResponseT<DeviceType[]>> {
    return <Observable<ResponseT<DeviceType[]>>>this.http.post(this.url + '/saveOrUpdateDeviceType', deviceType);
  }

  getDeviceTypeById(deviceTypeId: number): Observable<ResponseT<DeviceType>> {
    return <Observable<ResponseT<DeviceType>>>this.http.get(this.url + '/getDeviceTypeById?deviceTypeId=' + deviceTypeId);
  }

  deleteDeviceType(deviceType: DeviceType): Observable<ResponseT<DeviceType[]>> {
    return <Observable<ResponseT<DeviceType[]>>>this.http.post(this.url + '/deleteDeviceType' , deviceType);
  }
}
