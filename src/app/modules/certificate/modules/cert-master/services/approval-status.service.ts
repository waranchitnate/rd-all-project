import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';
import { MApprovalstatus } from '../models/m-approval-status.model';
import { ResponseT } from '../../../../../shared/models/response.model';
import { PageResponse } from '../../../../../shared/models/page-response';





@Injectable({
  providedIn: 'root'
})

export class ApprovalStatusService {

  url = environment.certApiUrl + 'certificate/master/mApprovalStatus';

  constructor(private http: HttpClient) {
  }

  getSearchCriteria(mapprovalstatus: MApprovalstatus): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/search', mapprovalstatus);
  }

  saveOrUpdateApprovalStatus(mapprovalstatus: MApprovalstatus): Observable<ResponseT<MApprovalstatus[]>> {
    return <Observable<ResponseT<MApprovalstatus[]>>>this.http.post(this.url + '/saveOrUpdateApprovalStatus', mapprovalstatus);
  }

  getApprovalStatusById(approvalStatusId: number): Observable<ResponseT<MApprovalstatus>> {
    return <Observable<ResponseT<MApprovalstatus>>>this.http.get(this.url + '/getApprovalStatusById?approvalStatusId=' + approvalStatusId);
  }

  deleteApprovalStatusById(mapprovalstatus: MApprovalstatus): Observable<ResponseT<MApprovalstatus[]>> {
    return <Observable<ResponseT<MApprovalstatus[]>>>this.http.post(this.url + '/deleteApprovalStatus' , mapprovalstatus);
  }
}
