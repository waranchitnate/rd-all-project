import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseT } from '../../../../../shared/models/response.model';
import { MUserType, MAttachmentType } from '../models/m-attachment-type.model';
import { PageResponse } from '../../../../../shared/models/page-response';

@Injectable({
  providedIn: 'root'
})
export class AttachmentTypeService {
  url = environment.certApiUrl + 'certificate/master/mAttachmentType';
  constructor(private http: HttpClient) { }

  getAllMUserType(): Observable<ResponseT<MUserType[]>> {
    return <Observable<ResponseT<MUserType[]>>>this.http.get(this.url + '/findAllMUserType');
  }

  getSearchCriteria(mAttachmentType: MAttachmentType): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/search', mAttachmentType);
  }

  saveOrUpdateMAttachmentType(mAttachmentType: MAttachmentType): Observable<ResponseT<MAttachmentType[]>> {
    return <Observable<ResponseT<MAttachmentType[]>>>this.http.post(this.url + '/saveOrUpdateMAttachmentType', mAttachmentType);
  }

  getMAttachmentTypeById(attachmentTypeId: number): Observable<ResponseT<MAttachmentType>> {
    return <Observable<ResponseT<MAttachmentType>>>this.http.get(this.url + '/getMAttachmentTypeById?attachmentTypeId=' + attachmentTypeId);
  }

  deleteMAttachmentType(attachmentTypeId: number): Observable<ResponseT<MAttachmentType[]>> {
    return <Observable<ResponseT<MAttachmentType[]>>>this.http.get(this.url + '/deleteMAttachmentType?attachmentTypeId=' + attachmentTypeId);
  }
}
