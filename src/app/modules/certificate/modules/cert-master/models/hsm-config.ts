export interface HsmConfig {
  hsmId?: number;
  hsmName?: string;
  hsmDescription?: string;
  driverPath?: string;
  brandName?: string;
  partitionName?: string;
  slotNo?: string;
  password?: string;
}
