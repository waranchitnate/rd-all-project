export interface MUserTypeModel {
    userTypeId: number;
    userTypeName: string;
    userTypeCode: string;
    createdDate: Date;
    createdBy: any;
    updatedDate: Date;
    updatedBy: any;

  }
