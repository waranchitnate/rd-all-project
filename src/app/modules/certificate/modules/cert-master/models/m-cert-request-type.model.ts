import { Certification } from "../../../models/certification";

export interface MCertRequestType {
    requestTypeId: number;
    requestTypeName: string;
    requestTypeDesc: string;
    createdDate: Date;
    createdBy: any;
    updatedDate: Date;
    updatedBy: any;
    userCertId: Certification;
  }




