import { Certification } from "../../../models/certification";

export interface MApprovalstatus {
    approvalStatusId: number;
    approvalStatusName: string;
    createdDate: Date;
    createdBy: any;
    updatedDate: Date;
    updatedBy: any;
    userCertId: Certification;
  }
