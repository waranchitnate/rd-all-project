export interface MAttachmentType {
    attachmentTypeId: number;
    attachmentTypeName: string;
    mUserType: MUserType;
    isRequired: number;
    createdDate: Date;
    createdBy: any;
    updatedDate: Date;
    updatedBy: any;
   
  }

  export interface MUserType {
    userTypeId: number;
    userTypeName: string;
  }