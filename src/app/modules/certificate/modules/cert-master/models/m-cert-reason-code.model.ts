import { Certification } from "../../../models/certification";

export interface MCertReasonCode {
    reasonCode: number;
    reasonDesc: string;
    createdDate: Date;
    createdBy: any;
    updatedDate: Date;
    updatedBy: any;
    userCertId: Certification;
  }
