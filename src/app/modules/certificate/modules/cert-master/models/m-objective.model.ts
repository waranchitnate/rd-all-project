
export interface MObjectiveModel {
    objectiveCode: string;
    objectiveDesc: string;
  }
