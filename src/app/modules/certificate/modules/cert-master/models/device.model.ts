import {Certification} from '../../../models/certification';

export interface DeviceRegister {
  deviceId: number;
  deviceName: string;
  serialNumber: string;
  usbToken: string;
  userId: number;
  userCertId: Certification;
  userName: string;
  deviceModel: any;
  createdDate: Date;
  createdBy: any;
  updatedDate: Date;
  updatedBy: any;
  remark: string;
  deviceStatus:DeviceStatus;
  deviceType: DeviceType;
  receivedDate: Date;
  receivedDateStr: String;
  refundDate: Date;
}
export interface DeviceStatus {
  deviceStatusId: number;
  deviceStatusName: string;
}

export interface DeviceType {
  deviceTypeId: number;
  deviceTypeName: string;
}



