import { Component, OnInit, TemplateRef, HostListener } from '@angular/core';
import { MApprovalstatus } from 'src/app/modules/certificate/modules/cert-master/models/m-approval-status.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { ApprovalStatusService } from 'src/app/modules/certificate/modules/cert-master/services/approval-status.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
@Component({
  selector: 'app-approval-status-search',
  templateUrl: './approval-status-search.component.html',
  styleUrls: ['./approval-status-search.component.css']
})
export class ApprovalStatusSearchComponent implements OnInit {
  mapprovalstatus = <MApprovalstatus[]>[];
  selectedData: MApprovalstatus;
  modalRef: BsModalRef;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  approvalStatusSubmit = false;
  windowScrolled: boolean;
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];
  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  getCurrentUserInfo: SsoUserModel;

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.windowScrolled = true;
    }
    else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
      this.windowScrolled = true;
    }
  }
  scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 8));
      }
    })();
  }
  constructor(private approvalStatusService: ApprovalStatusService, private fb: FormBuilder, private router: Router, private actRoute: ActivatedRoute, private toastr: ToastrService,
    private modalService: BsModalService,private modalAlertService: ModalService,
    public authenService: AuthenticationService) {

  }
  searchApprovalStatusCriteria = this.fb.group({
    approvalStatusId: [''],
    approvalStatusName: ['', Validators.required]
  });



  approvalStatusFormGroup = this.fb.group({
    approvalStatusId: [''],
    approvalStatusName: ['', Validators.required]
  });

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequest.sortFieldName = 'APPROVAL_STATUS_ID';
    this.pageRequest.sortDirection = 'ASC';
    this.getData();

  }
  get field(): any{
    return this.approvalStatusFormGroup.controls;
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest,this.limit);
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  // open(modal) {
  //   this.approvalStatusSubmit = false;
  //   this.approvalStatusFormGroup.reset();
  //   this.modalService.open(modal);
  // }
  open(template: TemplateRef<any>) {
    this.approvalStatusSubmit = false;
    this.approvalStatusFormGroup.reset();
    this.modalRef = this.modalService.show(template);
  }
  clear() {
    this.searchApprovalStatusCriteria.reset();
  }

  pageChange(event: any,limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'APPROVAL_STATUS_ID';
    this.pageRequest.sortDirection = 'ASC';
    if(limit != null){
      event.itemsPerPage =limit;
    }
    this.getData();
  }

  getData() {

    const criteria = this.searchApprovalStatusCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.approvalStatusService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }


  saveApprovalStatus() {

    this.approvalStatusSubmit = true;
    if (this.approvalStatusFormGroup.invalid) {
      return;
    }
    this.approvalStatusService.saveOrUpdateApprovalStatus(this.approvalStatusFormGroup.value)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error => this.showError(),
        () => {
          this.modalRef.hide();
          // this.modalService.dismissAll();
          this.clear();
          this.ngOnInit();
        }
      );
  }
  editApprovalStatus(approvalStatusId, template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    if (approvalStatusId != null) {
      this.approvalStatusService.getApprovalStatusById(approvalStatusId).subscribe((result: any) => {
        const param = {
          approvalStatusId: result.data.approvalStatusId,
          approvalStatusName: result.data.approvalStatusName,
        };
        this.approvalStatusFormGroup.setValue(param);
      });

    }
  }

  deleteApprovalStatus(approvalStatusId) {
    const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ','คุณต้องการลบข้อมูล?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        console.log('true');
        this.approvalStatusService.deleteApprovalStatusById(approvalStatusId)
        .subscribe(
          res => {
            this.showDeleteSuccess();
          },
          error => this.showDeleteError(),
          () => {
            // this.modalService.dismissAll();
            this.ngOnInit();
          }
        );

      } else {
        console.log('false');
      }
    });
  }

  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }
  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ');
  }
  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล ไม่สามารถบันทึกข้อมูลซ้ำได้', 'ไม่สำเร็จ');
  }

}
