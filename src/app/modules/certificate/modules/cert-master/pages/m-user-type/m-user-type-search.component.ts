import { Component, OnInit, TemplateRef, HostListener } from '@angular/core';
// import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MUserTypeModel } from 'src/app/modules/certificate/modules/cert-master/models/m-user-type.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { UserTypeService } from 'src/app/modules/certificate/modules/cert-master/services/user-type.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';

@Component({
  selector: 'app-musertype-search',
  templateUrl: './m-user-type-search.component.html',
  styleUrls: ['./m-user-type-search.component.css']
})
export class MUserTypeSearchComponent implements OnInit {
  modalRef: BsModalRef;
  mUserTypeModel = <MUserTypeModel[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  mUserTypeSubmit = false;
  windowScrolled: boolean;
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  getCurrentUserInfo: SsoUserModel;

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.windowScrolled = true;
    }
    else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
      this.windowScrolled = true;
    }
  }
  scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 8));
      }
    })();
  }

  constructor(private musertypeService: UserTypeService, private fb: FormBuilder, private router: Router, private actRoute: ActivatedRoute, private toastr: ToastrService,
    private modalService: BsModalService, private modalAlertService: ModalService,
    public authenService: AuthenticationService) {
  }

  searchUserTypeCriteria = this.fb.group({
    userTypeId: [''],
    userTypeName: ['', Validators.required],
    userTypeCode: ['', Validators.required]
  });

  mUserTypeFormGroup = this.fb.group({
    userTypeId: [''],
    userTypeName: ['', Validators.required],
    userTypeCode: ['', Validators.required]
  });

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequest.sortFieldName = 'USER_TYPE_ID';
    this.pageRequest.sortDirection = 'ASC';
    this.getData();
    this.clear();
  }
  get field() {
    return this.mUserTypeFormGroup.controls;
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  // open(modal) {

  //  this.mUserTypeSubmit = false;
  //  this.mUserTypeFormGroup.reset();
  //  this.modalService.open(modal);
  // }
  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }
  open(template: TemplateRef<any>) {
    this.mUserTypeSubmit = false;
    this.mUserTypeFormGroup.reset();
    this.modalRef = this.modalService.show(template);
  }

  clear() {
    this.searchUserTypeCriteria.reset();
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'USER_TYPE_ID';
    this.pageRequest.sortDirection = 'ASC';
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  getData() {
    const criteria = this.searchUserTypeCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.musertypeService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  saveMUserType() {
    this.mUserTypeSubmit = true;
    if (this.mUserTypeFormGroup.invalid) {
      return;
    }
    this.musertypeService.saveOrUpdateMUserType(this.mUserTypeFormGroup.value)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error => this.showError(),
        () => {
          this.modalRef.hide();
          // this.modalService.dismissAll();
          this.clear();
          this.ngOnInit();
        }
      );
  }

  editMUserType(userTypeId, template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    if (userTypeId != null) {
      this.musertypeService.getMUserTypeById(userTypeId).subscribe((result: any) => {
        const param = {
          userTypeId: result.data.userTypeId,
          userTypeName: result.data.userTypeName,
          userTypeCode: result.data.userTypeCode
        };
        this.mUserTypeFormGroup.setValue(param);
      });

    }
  }

  deleteMUserType(userTypeId) {
    const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ', 'คุณต้องการลบข้อมูล?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        console.log('true');
        this.musertypeService.deleteMUserType(userTypeId)
          .subscribe(
            res => {
              this.showDeleteSuccess();
            },
            error => this.showDeleteError(),
            () => {
              // this.modalService.dismissAll();
              this.ngOnInit();

            }
          );

      } else {
        console.log('false');
      }
    });
  }

  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }
  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ', {
      timeOut: 3000
    });
  }
  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล ไม่สามารถบันทึกข้อมูลซ้ำได้', 'ไม่สำเร็จ', {
      timeOut: 3000
    });
  }
}
