import { Component, OnInit, TemplateRef, HostListener } from '@angular/core';
import { MAttachmentType, MUserType } from 'src/app/modules/certificate/modules/cert-master/models/m-attachment-type.model';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AttachmentTypeService } from 'src/app/modules/certificate/modules/cert-master/services/attachment-type.service';
// import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
@Component({
  selector: 'app-mattachmenttype-search',
  templateUrl: './m-attachment-type-search.component.html',
  styleUrls: ['./m-attachment-type-search.component.css']
})
export class MAttachmentTypeSearchComponent implements OnInit {
  modalRef: BsModalRef;
  mAttachmentType = <MAttachmentType[]>[];
  mUserType = <MUserType[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  mAttachmentTypeSubmit = false;
  windowScrolled: boolean;
  currentPage = 0;

  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  getCurrentUserInfo: SsoUserModel;

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.windowScrolled = true;
    }
    else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
      this.windowScrolled = true;
    }
  }
  scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 8));
      }
    })();
  }

  constructor(private mattachmenttypeService: AttachmentTypeService, private fb: FormBuilder, private router: Router, private actRoute: ActivatedRoute, private toastr: ToastrService,
              private modalService: BsModalService, private modalAlertService: ModalService,
              public authenService: AuthenticationService) {

  }

  searchAttachmentTypeCriteria = this.fb.group({
    attachmentTypeId: [''],
    attachmentTypeName: [''],
    userTypeId: ['']
  });

  attachmentTypeFormGroup = this.fb.group({
    attachmentTypeId: [''],
    attachmentTypeName: ['', Validators.required],
    userTypeId: ['', Validators.required],
    isRequired: [false, Validators.required]
  });


  pageChange(event: any,limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'ATTACHMENT_TYPE_ID';
    this.pageRequest.sortDirection = 'ASC';
    if(limit != null){
      event.itemsPerPage =limit;
    }
    this.getData();
  }
  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequest.sortFieldName = 'ATTACHMENT_TYPE_ID';
    this.pageRequest.sortDirection = 'ASC';
    this.getMUserTypeDropdown();
    this.getData();
  }
  get field() {
    return this.attachmentTypeFormGroup.controls;
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest,this.limit);
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  // open(modal) {

  //  this.mAttachmentTypeSubmit = false;
  //  this.attachmentTypeFormGroup.reset();
  //  this.modalService.open(modal);
  // }
  open(template: TemplateRef<any>) {
    this.mAttachmentTypeSubmit = false;
   this.attachmentTypeFormGroup.reset();
    this.modalRef = this.modalService.show(template);
  }
  clear() {
    this.searchAttachmentTypeCriteria = this.fb.group({
      attachmentTypeId: [''],
      attachmentTypeName: [''],
      userTypeId: ['']
    });
  }

  getMUserTypeDropdown() {
    this.mattachmenttypeService.getAllMUserType()
      .subscribe(
        resultType => {
          this.mUserType = resultType.data;
        },
        err => console.log('timeout'),
      );
  }
  getData() {
    const criteria = this.searchAttachmentTypeCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.mattachmenttypeService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;

      });
  }


  editAttachmentType(attachmentTypeId, template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
    if (attachmentTypeId != null) {
      this.mattachmenttypeService.getMAttachmentTypeById(attachmentTypeId).subscribe((result: any) => {
        const param = {
          attachmentTypeId: result.data.attachmentTypeId,
          attachmentTypeName: result.data.attachmentTypeName,
          userTypeId: result.data.userTypeId,
          isRequired: result.data.isRequired
        };
        this.attachmentTypeFormGroup.setValue(param);
      });

    }
  }

  saveAttachmentType(){
 this.mAttachmentTypeSubmit = true;

    if(this.attachmentTypeFormGroup.get('isRequired').value === true){
      this.attachmentTypeFormGroup.setValue({
        attachmentTypeId:  this.attachmentTypeFormGroup.get('attachmentTypeId').value,
        attachmentTypeName:  this.attachmentTypeFormGroup.get('attachmentTypeName').value,
        userTypeId:  this.attachmentTypeFormGroup.get('userTypeId').value,
        isRequired: 1
      })

    }else{
      this.attachmentTypeFormGroup.setValue({
        attachmentTypeId:  this.attachmentTypeFormGroup.get('attachmentTypeId').value,
        attachmentTypeName:  this.attachmentTypeFormGroup.get('attachmentTypeName').value,
        userTypeId:  this.attachmentTypeFormGroup.get('userTypeId').value,
        isRequired:0
      })
    }
    if (this.attachmentTypeFormGroup.invalid) {
      return;
    }
    this.mattachmenttypeService.saveOrUpdateMAttachmentType(this.attachmentTypeFormGroup.value)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error => this.showError(),
        () => {
          this.modalRef.hide();
          // this.modalService.dismissAll();
          this.clear();
          this.ngOnInit();
        }
      );
  }
  deleteAttachmentType(attachmentTypeId) {
    const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ','คุณต้องการลบข้อมูล?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        console.log('true');
        this.mattachmenttypeService.deleteMAttachmentType(attachmentTypeId)
      .subscribe(
        res => {
          this.showDeleteSuccess();
        },
        error => this.showDeleteError(),
        () => {
          // this.modalService.dismissAll();
          this.ngOnInit();

        }
      );

      } else {
        console.log('false');
      }
    });
  }

  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }
  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ',{
      timeOut: 3000
    });
  }
  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล ไม่สามารถบันทึกข้อมูลซ้ำได้', 'ไม่สำเร็จ',{
      timeOut: 3000
    });
  }
}
