import {Component, OnInit, TemplateRef} from '@angular/core';
import {PageResponse} from 'src/app/shared/models/page-response';
import {PageRequest} from 'src/app/shared/models/page-request';
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {DeviceTypeService} from '../../services/device-type.service';
import {DeviceRegister, DeviceType} from '../../models/device.model';

@Component({
  selector: 'app-device-type-search',
  templateUrl: './device-type-search.component.html',
  styleUrls: ['./device-type-search.component.css']
})
export class DeviceTypeSearchComponent implements OnInit {
  modalRef: BsModalRef;
  devices = <DeviceRegister[]>[];
  deviceType = <DeviceType[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  deviceTypeSubmit = false;
  currentPage = 0;
  LIMITS = [
    {key: '10', value: 10},
    {key: '25', value: 25},
    {key: '50', value: 50},
    {key: '100', value: 100}
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;

  constructor(private deviceTypeService: DeviceTypeService, private fb: FormBuilder, private router: Router, private actRoute: ActivatedRoute, private toastr: ToastrService,
              private modalService: BsModalService) {

  }

  searchDeviceTypeCriteria = this.fb.group({
    deviceTypeId: [''],
    deviceTypeName: ['', Validators.required]
  });


  deviceTypeFormGroup = this.fb.group({
    deviceTypeId: [''],
    deviceTypeName: ['', Validators.required]
  });

  ngOnInit() {
    this.getData();
    this.clear();
  }

  get field() {
    return this.deviceTypeFormGroup.controls;
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  // open(modal) {

  //  this.deviceTypeSubmit = false;
  //  this.deviceTypeFormGroup.reset();
  //  this.modalService.open(modal);
  // }

  open(template: TemplateRef<any>) {
    this.deviceTypeSubmit = false;
    this.deviceTypeFormGroup.reset();
    this.modalRef = this.modalService.show(template);
  }

  clear() {
    this.searchDeviceTypeCriteria.reset();
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  getData() {
    const criteria = this.searchDeviceTypeCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.deviceTypeService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  saveDeviceType() {
    this.deviceTypeSubmit = true;
    if (this.deviceTypeFormGroup.invalid) {
      return;
    }
    this.deviceTypeService.saveOrUpdateDeviceType(this.deviceTypeFormGroup.value)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error => this.showError(),
        () => {
          // this.modalService.dismissAll();
          this.clear();
          this.ngOnInit();
        }
      );
  }

  editDeviceType(deviceTypeId, template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    if (deviceTypeId != null) {
      this.deviceTypeService.getDeviceTypeById(deviceTypeId).subscribe((result: any) => {
        const param = {
          deviceTypeId: result.data.deviceTypeId,
          deviceTypeName: result.data.deviceTypeName,
        };
        this.deviceTypeFormGroup.setValue(param);
      });

    }
  }

  deleteDeviceType(deviceStatusId) {
    this.deviceTypeService.deleteDeviceType(deviceStatusId)
      .subscribe(
        res => {
          this.showDeleteSuccess();
        },
        error => this.showDeleteError(),
        () => {
          // this.modalService.dismissAll();
          this.ngOnInit();

        }
      );
  }

  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ');
  }

  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล', 'ไม่สำเร็จ');
  }

}
