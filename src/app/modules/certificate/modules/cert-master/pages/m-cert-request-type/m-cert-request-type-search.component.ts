import { Component, OnInit, TemplateRef, HostListener } from '@angular/core';
// import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MCertRequestType } from 'src/app/modules/certificate/modules/cert-master/models/m-cert-request-type.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { CertRequestTypeService } from 'src/app/modules/certificate/modules/cert-master/services/cert-request-type.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
@Component({
  selector: 'app-mcerrequest-type-search',
  templateUrl: './m-cert-request-type-search.component.html',
  styleUrls: ['./m-cert-request-type-search.component.css']
})
export class MCertRequestTypeSearchComponent implements OnInit {
  modalRef: BsModalRef;
  mcerRequestType = <MCertRequestType[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  cerrequestTypeSubmit = false;
  windowScrolled: boolean;
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  getCurrentUserInfo: SsoUserModel;

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.windowScrolled = true;
    }
    else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
      this.windowScrolled = true;
    }
  }
  scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 8));
      }
    })();
  }

  constructor(private mcerrequestTypeService : CertRequestTypeService, private fb: FormBuilder, private router: Router, private actRoute: ActivatedRoute, private toastr: ToastrService,
              private modalService: BsModalService, private modalAlertService: ModalService,
              public authenService: AuthenticationService) {
    }

    searchCerRequestTypeCriteria = this.fb.group({
      requestTypeId: [''],
      requestTypeName: ['', Validators.required],
      requestTypeDesc: ['', Validators.required]
    });

    cerRequestTypeFormGroup = this.fb.group({
      requestTypeId: [''],
      requestTypeName: ['', Validators.required],
      requestTypeDesc: ['', Validators.required]
    });

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequest.sortFieldName = 'REQUEST_TYPE_ID';
    this.pageRequest.sortDirection = 'ASC';
    this.getData();
    this.clear();
  }

  get field() {
    return this.cerRequestTypeFormGroup.controls;
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest,this.limit);
  }

  // open(modal) {

  //  this.cerrequestTypeSubmit = false;
  //  this.cerRequestTypeFormGroup.reset();
  //  this.modalService.open(modal);
  // }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  open(template: TemplateRef<any>) {
    this.cerrequestTypeSubmit = false;
    this.cerRequestTypeFormGroup.reset();
    this.modalRef = this.modalService.show(template);
  }

  clear() {
    this.searchCerRequestTypeCriteria.reset();
  }

  pageChange(event: any,limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'REQUEST_TYPE_ID';
    this.pageRequest.sortDirection = 'ASC';
    if(limit != null){
      event.itemsPerPage =limit;
    }
    this.getData();
  }
  getData() {
    const criteria = this.searchCerRequestTypeCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.mcerrequestTypeService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  saveCerRequestType() {
    this.cerrequestTypeSubmit = true;
    if (this.cerRequestTypeFormGroup.invalid) {
      return;
    }
    this.mcerrequestTypeService.saveOrUpdateCertRequestType(this.cerRequestTypeFormGroup.value)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error => this.showError(),
        () => {
          this.modalRef.hide();
          // this.modalService.dismissAll();
          this.clear();
          this.ngOnInit();
        }
      );
  }

  editCerRequestType(requestTypeId, template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    if (requestTypeId != null) {
      this.mcerrequestTypeService.getCertRequestTypeById(requestTypeId).subscribe((result: any) => {
        const param = {
          requestTypeId: result.data.requestTypeId,
          requestTypeName: result.data.requestTypeName,
          requestTypeDesc: result.data.requestTypeDesc
        };
        this.cerRequestTypeFormGroup.setValue(param);
      });

    }
  }

  deleteCerRequestType(requestTypeId) {
    const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ','คุณต้องการลบข้อมูล?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        console.log('true');
        this.mcerrequestTypeService.deleteCertRequestType(requestTypeId)
      .subscribe(
        res => {
          this.showDeleteSuccess();
        },
        error => this.showDeleteError(),
        () => {
          // this.modalService.dismissAll();
          this.ngOnInit();

        }
      );

      } else {
        console.log('false');
      }
    });
  }


  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }
  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ',{
      timeOut: 3000
    });
  }
  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล ไม่สามารถบันทึกข้อมูลซ้ำได้', 'ไม่สำเร็จ',{
      timeOut: 3000
    });
  }

}
