import { Component, OnInit, HostListener, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { MObjectiveModel } from '../../../models/m-objective.model';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { Validators, FormBuilder } from '@angular/forms';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { MObjectiveService } from '../../../services/m-objective.service';
import { ToastrService } from 'ngx-toastr';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';

@Component({
  selector: 'app-m-objective-search',
  templateUrl: './m-objective-search.component.html',
  styleUrls: ['./m-objective-search.component.css']
})
export class MObjectiveSearchComponent implements OnInit {
  modalRef: BsModalRef;
  mObjectiveModel = <MObjectiveModel[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  mObjectiveSubmit = false;
  windowScrolled: boolean;
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  getCurrentUserInfo: SsoUserModel;

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.windowScrolled = true;
    }
    else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
      this.windowScrolled = true;
    }
  }
  scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 8));
      }
    })();
  }

  constructor(private fb: FormBuilder,
    private modalService: BsModalService,
    private modalAlertService: ModalService,
    public authenService: AuthenticationService,
    private mObjectiveService: MObjectiveService,
    private toastr: ToastrService) { }

  searchObjectiveCriteria = this.fb.group({
    objectiveCode: [''],
    objectiveDesc: ['', Validators.required]
  });

  objectiveFormGroup = this.fb.group({
    objectiveCode: [''],
    objectiveDesc: ['', Validators.required]
  });

  get field() {
    return this.objectiveFormGroup.controls;
  }

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequest.sortFieldName = 'OBJECTIVE_CODE';
    this.pageRequest.sortDirection = 'ASC';
    this.getData();
    this.clear();
  }
  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest,this.limit);
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  pageChange(event: any,limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'OBJECTIVE_CODE';
    this.pageRequest.sortDirection = 'ASC';
    if(limit != null){
      event.itemsPerPage =limit;
    }
    this.getData();
  }

  getData(){
    const criteria = this.searchObjectiveCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.mObjectiveService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  clear(){
    this.searchObjectiveCriteria.reset();
  }

  open(template: TemplateRef<any>) {
    this.mObjectiveSubmit = false;
    this.objectiveFormGroup.reset();
    this.modalRef = this.modalService.show(template);
  }
  
  saveObjective(){
    this.mObjectiveSubmit = true;
    if (this.objectiveFormGroup.invalid) {
      return;
    }
    this.mObjectiveService.saveOrUpdateMObjective(this.objectiveFormGroup.value)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error => this.showError(),
        () => {
          this.modalRef.hide();
          // this.modalService.dismissAll();
          this.clear();
          this.ngOnInit();
        }
      );
  }

editObjective(objectiveCode, template: TemplateRef<any>) {
  this.modalRef = this.modalService.show(template);
  if (objectiveCode != null) {
    this.mObjectiveService.getMobjectiveByObjectiveCode(objectiveCode).subscribe((result: any) => {
      const param = {
        objectiveCode: result.data.objectiveCode,
        objectiveDesc: result.data.objectiveDesc,
      };
      this.objectiveFormGroup.setValue(param);
    });

  }
}

deleteObjective(Objective){
  const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ','คุณต้องการลบข้อมูล?');
  (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
    if (result) {
      console.log('true');
      this.mObjectiveService.deleteMObjective(Objective)
    .subscribe(
      res => {
        this.showDeleteSuccess();
      },
      error => this.showDeleteError(),
      () => {
        // this.modalService.dismissAll();
        this.ngOnInit();

      }
    );

    } else {
      console.log('false');
    }
  });
}
  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล', 'ไม่สำเร็จ');
  }

  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }
  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ');
  }
}
