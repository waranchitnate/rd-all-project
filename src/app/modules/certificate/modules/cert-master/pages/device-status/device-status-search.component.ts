import {Component, OnInit, TemplateRef, HostListener} from '@angular/core';
import {PageResponse} from 'src/app/shared/models/page-response';
import {PageRequest} from 'src/app/shared/models/page-request';
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {DeviceStatusService} from '../../services/device-status.service';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';

@Component({
  selector: 'app-device-status-search',
  templateUrl: './device-status-search.component.html',
  styleUrls: ['./device-status-search.component.css']
  // providers: [NgbModalConfig, NgbModal]
})
export class DeviceStatusSearchComponent implements OnInit {
  modalRef: BsModalRef;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  deviceStatusSubmit = false;
  windowScrolled: boolean;
  currentPage = 0;
  LIMITS = [
    {key: '10', value: 10},
    {key: '25', value: 25},
    {key: '50', value: 50},
    {key: '100', value: 100}
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  getCurrentUserInfo: SsoUserModel;

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
      this.windowScrolled = true;
    }
    else if (this.windowScrolled && window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop < 10) {
      this.windowScrolled = true;
    }
  }
  scrollToTop() {
    (function smoothscroll() {
      var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - (currentScroll / 8));
      }
    })();
  }
  constructor(private deviceStatusService: DeviceStatusService, private fb: FormBuilder, private router: Router, private actRoute: ActivatedRoute, private toastr: ToastrService,
              private modalService: BsModalService,private modalAlertService: ModalService,
              public authenService: AuthenticationService) {

  }

  searchDeviceStatusCriteria = this.fb.group({
    deviceStatusId: [''],
    deviceStatusName: ['', Validators.required]
  });


  deviceStatusFormGroup = this.fb.group({
    deviceStatusId: [''],
    deviceStatusName: ['', Validators.required]
  });

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequest.sortFieldName = 'DEVICE_STATUS_ID';
    this.pageRequest.sortDirection = 'ASC';
    this.getData();
    this.clear();
  }

  get field() {
    return this.deviceStatusFormGroup.controls;
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  open(template: TemplateRef<any>) {
    this.deviceStatusSubmit = false;
    this.deviceStatusFormGroup.reset();
    this.modalRef = this.modalService.show(template);
  }

  clear() {
    this.searchDeviceStatusCriteria.reset();
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'DEVICE_STATUS_ID';
    this.pageRequest.sortDirection = 'ASC';
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  getData() {

    const criteria = this.searchDeviceStatusCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.deviceStatusService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
      });
  }

  saveDeviceStatus() {

    this.deviceStatusSubmit = true;
    if (this.deviceStatusFormGroup.invalid) {
      return;
    }
    this.deviceStatusService.saveOrUpdateDeviceStatus(this.deviceStatusFormGroup.value)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error => this.showError(),
        () => {
          this.modalRef.hide();
          // this.modalService.dismissAll();
          this.clear();
          this.ngOnInit();
        }
      );
  }

  editDeviceStatus(deviceStatusId, template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    if (deviceStatusId != null) {
      this.deviceStatusService.getDeviceStatusById(deviceStatusId).subscribe((result: any) => {
        const param = {
          deviceStatusId: result.data.deviceStatusId,
          deviceStatusName: result.data.deviceStatusName,
        };
        this.deviceStatusFormGroup.setValue(param);
      });

    }
  }

  deleteDeviceStatus(deviceStatusId) {
    const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ','คุณต้องการลบข้อมูล?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        console.log('true');
        this.deviceStatusService.deleteDeviceStatus(deviceStatusId)
      .subscribe(
        res => {
          this.showDeleteSuccess();
        },
        error => this.showDeleteError(),
        () => {
          // this.modalService.dismissAll();
          this.ngOnInit();

        }
      );

      } else {
        console.log('false');
      }
    });
  }

  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ');
  }

  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล ไม่สามารถบันทึกข้อมูลซ้ำได้', 'ไม่สำเร็จ');
  }

}
