import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApprovalStatusSearchComponent } from './pages/approval-status/approval-status-search.component';
import { CertURL } from '../../certificate.url';
import { DeviceTypeSearchComponent } from './pages/device-type/device-type-search.component';
import { DeviceStatusSearchComponent } from './pages/device-status/device-status-search.component';
import { MCertRequestTypeSearchComponent } from './pages/m-cert-request-type/m-cert-request-type-search.component';
import { MCertReasonCodeSearchComponent } from './pages/m-cert-reason-code/m-cert-reason-code-search.component';
import { MAttachmentTypeSearchComponent } from './pages/m-attachment-type/m-attachment-type-search.component';
import { MUserTypeSearchComponent } from './pages/m-user-type/m-user-type-search.component';
import { MObjectiveSearchComponent } from './pages/m-objective/m-objective-search/m-objective-search.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: CertURL.master.home},
  {path: CertURL.master.approve, component: ApprovalStatusSearchComponent},
  {path: CertURL.master.deviceType, component: DeviceTypeSearchComponent},
  {path: CertURL.master.deviceStatus, component: DeviceStatusSearchComponent},
  {path: CertURL.master.mcerRequestType, component: MCertRequestTypeSearchComponent},
  {path: CertURL.master.mcertReasonCode, component: MCertReasonCodeSearchComponent},
  {path: CertURL.master.mattachmentType, component: MAttachmentTypeSearchComponent},
  {path: CertURL.master.musertype, component: MUserTypeSearchComponent},
  {path: CertURL.master.mobjective, component: MObjectiveSearchComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertMasterRoutingModule { }
