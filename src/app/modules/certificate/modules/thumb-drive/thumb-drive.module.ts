import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ThumbDriveRoutingModule} from './thumb-drive-routing.module';
import {ThumbDriveSearchComponent} from './pages/thumb-drive-search/thumb-drive-search.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../../shared/shared.module';
import {ThumbDriveFormComponent} from './pages/thumb-drive-form/thumb-drive-form.component';
import {ThumbDriveRegisterComponent} from './pages/thumb-drive-register/thumb-drive-register.component';
import {TooltipModule} from 'ngx-bootstrap-th';


@NgModule({
  declarations: [ThumbDriveSearchComponent, ThumbDriveFormComponent, ThumbDriveRegisterComponent],
  imports: [
    CommonModule,
    ThumbDriveRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TooltipModule,
  ]
})

export class ThumbDriveModule {
}
