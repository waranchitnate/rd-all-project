import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DeviceRegisterService } from 'src/app/modules/certificate/modules/device-management/services/device-register.service';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { DeviceRegister, DeviceStatus, DeviceType } from 'src/app/modules/certificate/modules/cert-master/models/device.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ThumbDriveSearchComponent } from '../thumb-drive-search/thumb-drive-search.component';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';


@Component({
  selector: 'app-thumb-drive-form',
  templateUrl: './thumb-drive-form.component.html',
  styleUrls: ['./thumb-drive-form.component.css']
})
export class ThumbDriveFormComponent implements OnInit {
  // bsValue = new Date();
  // bsRangeValue: Date[];
  minDate = new Date();
  // maxDate = new Date();
  // bsConfig: Partial<BsDatepickerConfig>;
  thumbDriveSubmit = false;
  thumbDriveForm: FormGroup;
  // todayDate = moment();
  devices : DeviceRegister;
  devicesStatus = <DeviceStatus[]>[];
  devicesType = <DeviceType[]>[];
  getCurrentUserInfo: SsoUserModel;

  constructor(private deviceRegisterService: DeviceRegisterService,
    private fb: FormBuilder,
    private router: Router,
    private actRoute: ActivatedRoute,
    private toastr: ToastrService,
    public authenService: AuthenticationService) {
      // this.minDate.setDate(this.minDate.getDate());
      // this.maxDate.setDate(this.maxDate.getDate());
      // this.bsRangeValue = [this.bsValue, this.maxDate];
      // this.bsConfig = Object.assign({}, { containerClass: 'theme-orange' , dateInputFormat: 'DD/MM/YYYY hh:mm:ss' });
     }

  formThumbDriveCriteria = this.fb.group({
    deviceId: [''],
    serialNumber: ['', Validators.required],
    deviceStatusId: ['', Validators.required],
    deviceTypeId: ['', Validators.required],
    receivedDate: [null, Validators.required],
    remark: ['', Validators.required]
  });

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.deviceRegisterService.getAllDeviceStatus()
      .subscribe(
        resultStatus => {
          this.devicesStatus = resultStatus.data; },
        err => console.log('timeout'),
        () => {
          this.deviceRegisterService.getAllDeviceType()
      .subscribe(
        resultType => { this.devicesType = resultType.data; },
        err => console.log('timeout'),
        ()=> {
          this.getThumbDrive(this.actRoute.snapshot.queryParamMap.get('deviceId'))
        }
          )
        }
      );

  }
  get field() {
    return this.formThumbDriveCriteria.controls;
  }
  getDeviceTypeDropdown() {
    this.deviceRegisterService.getAllDeviceType()
      .subscribe(
        resultType => {
          this.devicesType = resultType.data;
        },
        err => console.log('timeout'),
      );
  }

  getDeviceStatusDropdown() {
    this.deviceRegisterService.getAllDeviceStatus()
      .subscribe(
        resultStatus => {
          this.devicesStatus = resultStatus.data;
        },
        err => console.log('timeout'),
      );
  }

  saveThumbDrive() {

    this.thumbDriveSubmit = true;
    if (this.formThumbDriveCriteria.invalid) {
      return;
    }
      this.deviceRegisterService.saveThumbDrive(this.formThumbDriveCriteria.value).subscribe((result: any) => {
        this.showSuccess();
        this.router.navigate(['thumb-drive/search']);
      },
      err => {this.showError();});

  }

  getThumbDrive(deviceId) {
    if (deviceId != null) {
      this.deviceRegisterService.getThumbDriveById(deviceId).subscribe((result: any) => {
        // console.log(result.data.receivedDate)
        // console.log(moment(result.data.receivedDate, 'DD/MM/YYYY HH:mm:ss').toDate())
        // console.log("moment : "+moment(result.data.receivedDate, 'DD/MM/YYYY HH:mm:ss').format('DD/MM/YYYY HH:mm:ss'))

        this.formThumbDriveCriteria.setValue({
          deviceId: result.data.deviceId,
          serialNumber: result.data.serialNumber,
          deviceStatusId: result.data.mdeviceStatus.deviceStatusId,
          deviceTypeId:  result.data.mdeviceType.deviceTypeId,
          receivedDate: new Date(result.data.receivedDate),
          remark: result.data.remark
        });
      });
    }
  }

  black() {
    this.router.navigate(['thumb-drive/search']);
  }

  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล', 'ไม่สำเร็จ');
  }

}
