import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DeviceRegisterService } from '../../../device-management/services/device-register.service';
import { DeviceRegister, DeviceStatus, DeviceType } from '../../../cert-master/models/device.model';
import { Router } from '@angular/router';
import { PageResponse } from '../../../../../../shared/models/page-response';
import { PageRequest } from '../../../../../../shared/models/page-request';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { DOCUMENT } from '@angular/common';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import {ModalService} from 'src/app/shared/services/modal.service';
import { ToastrService } from 'ngx-toastr';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';


@Component({
  selector: 'app-device-form',
  templateUrl: './thumb-drive-search.component.html',
  styleUrls: ['./thumb-drive-search.component.css']
})

export class ThumbDriveSearchComponent implements OnInit {
  // bsValue = new Date();
  // bsRangeValue: Date[];
  // minDate = new Date();
  // maxDate = new Date();
  // bsConfig: Partial<BsDatepickerConfig>;
  @ViewChild(FloatButtonComponent) floatComp;
  devices = <DeviceRegister[]>[];
  devicesStatus = <DeviceStatus[]>[];
  devicesType = <DeviceType[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  getCurrentUserInfo: SsoUserModel;

  constructor(private deviceRegisterService: DeviceRegisterService,
    private fb: FormBuilder,
    private router: Router,
    private modalService: ModalService,
    private toastr: ToastrService,
    private authenService: AuthenticationService
    ) {
    // this.minDate.setDate(this.maxDate.getDate());
    // this.maxDate.setDate(this.maxDate.getDate());
    // this.bsRangeValue = [this.bsValue, this.maxDate];
    // this.bsConfig = Object.assign({}, { containerClass: 'theme-orange' });
  }

  searchThumbDriveCriteria = this.fb.group({
    serialNumber: [''],
    deviceStatusId: [''],
    deviceTypeId: [''],
    receivedDate: [''],
    remark: ['']
  });

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequest.sortFieldName = 'DEVICE_ID';
    this.pageRequest.sortDirection = 'ASC';
    this.floatComp.urlAdd = '/thumb-drive/form';
    this.floatComp.tooltipMsg = 'เพิ่มข้อมูลอุปกรณ์';
    this.getDeviceStatusDropdown();
    this.getDeviceTypeDropdown();
    this.getData();
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  getDeviceTypeDropdown() {
    this.deviceRegisterService.getAllDeviceType()
      .subscribe(
        resultType => {
          this.devicesType = resultType.data;
        },
        err => console.log('timeout'),
      );
  }

  getDeviceStatusDropdown() {
    this.deviceRegisterService.getAllDeviceStatus()
      .subscribe(
        resultStatus => {
          this.devicesStatus = resultStatus.data;
        },
        err => console.log('timeout'),
      );
  }

  goToThumbDriveFrom() {
    this.router.navigate(['/thumb-drive/form']);
  }

  goToSendBackRegisterFrom(deviceId) {
    this.router.navigate(['/thumb-drive/register'], { queryParams: { deviceId } });
  }

  getThumbDrive(deviceId) {
    this.router.navigate(['/thumb-drive/form'], { queryParams: { deviceId } });
  }

  clear() {
    // this.searchThumbDriveCriteria.reset();
    this.searchThumbDriveCriteria = this.fb.group({
      serialNumber: [''],
      deviceStatusId: [''],
      deviceTypeId: [''],
      receivedDate: [''],
      remark: ['']
    });
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'DEVICE_ID';
    this.pageRequest.sortDirection = 'ASC';
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  getData() {
    const criteria = this.searchThumbDriveCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.deviceRegisterService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
        console.log(res.data)

      });
  }

  sendBack(dv) {
    if (dv != null) {
      const modalRef = this.modalService.openConfirmModal('ยืนยันการส่งคืนอุปกรณ์', 'ยืนยันการส่งคืนหมายเลขอุปกรณ์ ' + dv.serialNumber);
      (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
        if (result) {
          console.log('true');
          this.deviceRegisterService.sendBackDevice(dv.deviceId)
        .subscribe(
          res => {
            this.toastr.success('ยืนยันการส่งคืนอุปกรณ์', 'สำเร็จ');
          },
          error => this.showSendError(),
          () => {
            // this.modalService.dismissAll();
            this.ngOnInit();

          }
        );

        } else {
          console.log('false');
        }

      });

    }

  }
  showSendError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างส่งคืนอุปกรณ์ ไม่สามารถส่งคืนอุปกรณ์ได้', 'ไม่สำเร็จ');
  }

}
