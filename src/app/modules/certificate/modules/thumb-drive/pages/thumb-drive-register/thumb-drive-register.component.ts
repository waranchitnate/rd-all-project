import { Component, OnInit } from '@angular/core';
import { OauthCredential } from 'src/app/core/models/oauth-credential';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { DeviceRegister, DeviceType } from 'src/app/modules/certificate/modules/cert-master/models/device.model';
import { Certification } from 'src/app/modules/certificate/models/certification';
import { FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { DeviceRegisterService } from 'src/app/modules/certificate/modules/device-management/services/device-register.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { VusercertService } from 'src/app/modules/certificate/models/vusercert.service';

@Component({
  selector: 'app-thumb-drive-register',
  templateUrl: './thumb-drive-register.component.html',
  styleUrls: ['./thumb-drive-register.component.css']
})
export class ThumbDriveRegisterComponent implements OnInit {
  thumbDriveSubmit = false;
  isCollapsed = false;
  selected: string;
  userDetail: OauthCredential;
  devices = <DeviceRegister[]>[];
  userCertId = <Certification[]>[];
  devicesType = <DeviceType[]>[];
  idn: number;
  isUserNull: any;
  buttonDisabled: boolean;

  constructor(private authService: AuthenticationService,
    private fb: FormBuilder,
    private deviceRegisterService: DeviceRegisterService,
    private vusercertService: VusercertService,
    private actRoute: ActivatedRoute,
    private router: Router, private toastr: ToastrService) {
  }

  registerform = this.fb.group({
    deviceId: [''],
    idn: [''],
    userName: [''],
    receivedDate: null,
    serialNumber: [null, Validators.required],
    deviceTypeId: null,
    deviceTypeName: null,
    deviceModel: null,
    remark: null,
    userId: null,
    userCertId: null,
  });

  ngOnInit() {
    const rowNum = this.actRoute.snapshot.queryParamMap.get('rowNum');
    this.vusercertService.getAvailiableDevice()
      .subscribe(
        result => {
          this.devices = result.data;
        },
        err => console.log('timeout')
      );
    this.getVuserCert(rowNum);
  }

  getVuserCert(rowNum) {
    if (rowNum != null) {
      this.vusercertService.getVuserCertById(rowNum).subscribe((result: any) => {
        this.registerform = this.fb.group({
          deviceId: null,
          idn: result.data.nid,
          userName: result.data.name,
          receivedDate: null,
          serialNumber: [null, Validators.required],
          deviceTypeId: null,
          deviceTypeName: null,
          deviceModel: null,
          remark: null,
          userId: result.data.userId,
          userCertId: result.data.userCertId
        });

      });

    }

  }

  clear() {
    const rowNum = this.actRoute.snapshot.queryParamMap.get('rowNum');
    if (rowNum == null) {
      return this.router.navigate(['thumb-drive/search']);
    } else {
      return this.router.navigate(['cert/admin/pending-list/form'], { queryParams: { rowNum } });
    }
  }

  saveThumbDrive() {
    this.thumbDriveSubmit = true;
    if (this.registerform.invalid) {
      return;
    }
    const rowNum = this.actRoute.snapshot.queryParamMap.get('rowNum');
    this.deviceRegisterService.saveThumbDriveRegister(this.registerform.value).subscribe((result: any) => {
      this.showSuccess();
      return this.router.navigate(['cert/admin/pending-list/form'], { queryParams: { rowNum } });
    },
      err => {
        this.showError();
      });
  }

  change(event): void {
    const deviceId = event.target.value;
    if (deviceId != null) {
      this.deviceRegisterService.getThumbDriveById(deviceId).subscribe((result: any) => {
        console.log(result.data)
        const param = {
          deviceId: deviceId,
          idn: this.registerform.get('idn').value
          , userName: this.registerform.get('userName').value
          , receivedDate: null
          , serialNumber: this.registerform.get('serialNumber').value
          , deviceTypeId: result.data.mdeviceType.deviceTypeId
          , deviceTypeName: result.data.mdeviceType.deviceTypeName
          , deviceModel: result.data.deviceModel
          , remark: result.data.remark
          , userId: this.registerform.get('userId').value
          , userCertId: this.registerform.get('userCertId').value
        };
        this.registerform.setValue(param);
        this.buttonDisabled = false;
      });
    }
  }

  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล', 'ไม่สำเร็จ');
  }

}
