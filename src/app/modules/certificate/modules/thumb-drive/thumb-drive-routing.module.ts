import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ThumbDriveSearchComponent} from './pages/thumb-drive-search/thumb-drive-search.component';
import {ThumbDriveFormComponent} from './pages/thumb-drive-form/thumb-drive-form.component';
import {ThumbDriveRegisterComponent} from './pages/thumb-drive-register/thumb-drive-register.component';

const routes: Routes = [
  {path: '', redirectTo: 'search', pathMatch: 'full'},
  {path: 'search', component: ThumbDriveSearchComponent},
  {path: 'form', component: ThumbDriveFormComponent},
  {path: 'register', component: ThumbDriveRegisterComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThumbDriveRoutingModule {
}
