import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {HttpProgressService} from '../../../../shared/services/http-progress.service';
import {CertificateService} from '../../services/certificate.service';
import {Router} from '@angular/router';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-change-certificate-pin',
  templateUrl: './change-certificate-pin.component.html',
  styleUrls: ['./change-certificate-pin.component.css']
})
export class ChangeCertificatePinComponent implements OnInit {

  newPfxFileName: string;
  changeCertificatePinForm: FormGroup;
  @ViewChild('newPfxFileHyperLinkEl') newPfxFileHyperLinkEl: ElementRef<HTMLHtmlElement>;
  constructor(private toastrService: ToastrService, private httpProgressService: HttpProgressService,
              private certificateService: CertificateService, private router: Router) { }

  ngOnInit() {
    this.changeCertificatePinForm = new FormGroup({
      certificateFile: new FormControl(null, Validators.required),
      pfxBase64: new FormControl(null),
      oldPin: new FormControl(null, [Validators.required]),
      newPin: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
      confirmNewPin: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
    }, this.confirmNewPinValidator);
  }

  changePin() {
    if (this.changeCertificatePinForm.invalid) {
      this.markFormGroupTouched(this.changeCertificatePinForm);
      this.toastrService.error('กรุณาระบุข้อมูลให้ถูกต้อง', 'ข้อผิดพลาด');
    } else {
      this.certificateService.changePin(this.oldPinControl.value, this.newPinControl.value, this.pfxBase64Control.value).subscribe(res => {
        const linkSource = 'data:application/octet-stream;base64,' + res.responseData.pfxBase64;
        const downloadLink = document.createElement('a');
        const fileName = this.newPfxFileName;
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        this.changeCertificatePinForm.reset();
        this.toastrService.success('เปลี่ยนแปลงรหัสผ่านใบรับรองอิเล็กทรอนิกส์สำเร็จ');
      }, err => {
        this.toastrService.error('ไม่สามารถเปลี่ยนรหัสผ่านใบรับรองอิเล็กทรอนิกส์ได้เนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
      });
    }
  }

  setPfxBase64(certFile: File) {
    this.newPfxFileName = 'new-' + certFile.name;
    this.httpProgressService.pushRequestToHttpProgress('upload-file');
    const reader = new FileReader();
    reader.readAsDataURL(certFile.slice());
    reader.onloadend = (() => {
      const file = reader.result as string;
      this.changeCertificatePinForm.get('pfxBase64').setValue(file.substring(file.indexOf('base64,') + 7));
      this.httpProgressService.popRequestFromHttpProgress('upload-file');
    });
  }

  confirmNewPinValidator(control: AbstractControl): ValidationErrors | null {
    let validationResult = null;
    const formGroup = control as FormGroup;
    if (formGroup.controls.confirmNewPin.value != null && formGroup.controls.confirmNewPin.value.length === 6 && formGroup.controls.newPin.value !== formGroup.controls.confirmNewPin.value) {
      validationResult = {unmatchedPin: 'กรุณาระบุพินใหม่ให้ตรงกัน'};
    }
    return validationResult;
  }

  markFormGroupTouched(formGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  get certificateFileControl(): AbstractControl {
    return this.changeCertificatePinForm.get('certificateFile');
  }

  get oldPinControl(): AbstractControl {
    return this.changeCertificatePinForm.get('oldPin');
  }

  get newPinControl(): AbstractControl {
    return this.changeCertificatePinForm.get('newPin');
  }

  get confirmNewPinControl(): AbstractControl {
    return this.changeCertificatePinForm.get('confirmNewPin');
  }

  get pfxBase64Control(): AbstractControl {
    return this.changeCertificatePinForm.get('pfxBase64');
  }
}
