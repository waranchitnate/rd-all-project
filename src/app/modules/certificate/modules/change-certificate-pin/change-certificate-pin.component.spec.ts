import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeCertificatePinComponent } from './change-certificate-pin.component';

describe('ChangeCertificatePinComponent', () => {
  let component: ChangeCertificatePinComponent;
  let fixture: ComponentFixture<ChangeCertificatePinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeCertificatePinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeCertificatePinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
