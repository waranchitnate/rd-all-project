import {Injectable} from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseT} from '../../../../../shared/models/response.model';
import {DeviceRegister, DeviceStatus, DeviceType} from '../../cert-master/models/device.model';
import {PageResponse} from '../../../../../shared/models/page-response';
import { MUserType } from '../../cert-master/models/m-attachment-type.model';

@Injectable({
  providedIn: 'root'
})

export class DeviceRegisterService {

  url = environment.certApiUrl + 'thumbDriveController';

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<ResponseT<DeviceRegister[]>> {
    return <Observable<ResponseT<DeviceRegister[]>>>this.http.get(this.url + '/findAll');
  }

  getAllDeviceStatus(): Observable<ResponseT<DeviceStatus[]>> {
    return <Observable<ResponseT<DeviceStatus[]>>>this.http.get(this.url + '/findAllDeviceStatus');
  }

  getAllDeviceType(): Observable<ResponseT<DeviceType[]>> {
    return <Observable<ResponseT<DeviceType[]>>>this.http.get(this.url + '/findAllDeviceType');
  }

  getAllRegisterDevice(): Observable<ResponseT<DeviceRegister[]>> {
    return <Observable<ResponseT<DeviceRegister[]>>>this.http.get(this.url + '/findAllRegisterDevice');
  }

  getSearchCriteria(devices: DeviceRegister): Observable<ResponseT<PageResponse<any>>> {
    return <Observable<ResponseT<PageResponse<any>>>>this.http.post(this.url + '/findByCriteria', devices);

  }

  saveThumbDrive(devices: DeviceRegister): Observable<ResponseT<DeviceRegister[]>> {
    return <Observable<ResponseT<DeviceRegister[]>>>this.http.post(this.url + '/saveThumbDrive', devices);
  }

  saveThumbDriveRegister(devices: DeviceRegister): Observable<ResponseT<DeviceRegister[]>> {
    return <Observable<ResponseT<DeviceRegister[]>>>this.http.post(this.url + '/saveThumbDriveRegister', devices);
  }

  getThumbDriveById(deviceId: number): Observable<ResponseT<DeviceRegister>> {
    return <Observable<ResponseT<DeviceRegister>>>this.http.get(this.url + '/getThumbDriveById?deviceId=' + deviceId);
  }

  getSendBackThumbDriveById(userId: number): Observable<ResponseT<DeviceRegister>> {
    return <Observable<ResponseT<DeviceRegister>>>this.http.get(this.url + '/getSendBackThumbDriveById?userId=' + userId);
  }

  getVuserCertById(rowNum: number): Observable<ResponseT<MUserType>> {
    return <Observable<ResponseT<MUserType>>>this.http.get(environment.certApiUrl + 'certificate/getVuserCertById?rowNum=' + rowNum);
  }
  sendBackDevice(deviceId: number): Observable<ResponseT<DeviceRegister>> {
    return <Observable<ResponseT<DeviceRegister>>>this.http.get(this.url + '/sendBackDevice?deviceId=' + deviceId);
  }
}
