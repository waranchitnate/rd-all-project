import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DeviceSearchComponent} from './pages/device-search/device-search.component';
import {DeviceFormComponent} from './pages/device-form/device-form.component';

const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full'},
  { path: 'search', component: DeviceSearchComponent },
  { path: 'form', component: DeviceFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeviceManagementRoutingModule { }
