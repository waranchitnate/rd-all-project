import {Component, OnInit} from '@angular/core';
import {DeviceRegister} from '../../../cert-master/models/device.model';
import {DeviceRegisterService} from '../../services/device-register.service';

@Component({
  selector: 'app-device-search',
  templateUrl: './device-search.component.html',
  styleUrls: ['./device-search.component.css']
})
export class DeviceSearchComponent implements OnInit {

  devices = <DeviceRegister[]>[];

  constructor(private deviceRegisterService: DeviceRegisterService) { }

  ngOnInit() {

    this.deviceRegisterService.getAll()
      .subscribe(
        result => { this.devices = result.data; },
        err => console.log('timeout'),
      );

  }

}
