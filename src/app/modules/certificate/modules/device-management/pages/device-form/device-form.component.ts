import {Component, OnInit} from '@angular/core';
import {DeviceRegisterService} from '../../services/device-register.service';
import {CertificateService} from '../../../../services/certificate.service';
import {Certification} from '../../../../models/certification';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-device-form',
  templateUrl: './device-form.component.html',
  styleUrls: ['./device-form.component.css']
})
export class DeviceFormComponent implements OnInit {
  addDeviceFormGroup: FormGroup;
  activeUsers = <Certification[]>[];
  submitted = false;


  constructor(private deviceRegisterService: DeviceRegisterService,
              private certService: CertificateService,
              private formBuilder: FormBuilder,
              private toastr: ToastrService) {
  }

  ngOnInit() {

    this.initFormGroup();
    this.certService.getActiveUserCert()
      .subscribe(
        res => {
          this.activeUsers = res.data;
        });
  }

  initFormGroup() {
    this.addDeviceFormGroup = this.formBuilder.group({
      user: ['', Validators.required],
      deviceName: ['', Validators.required],
      deviceModel: ['', Validators.required],
      serialNumber: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get field() {
    return this.addDeviceFormGroup.controls;
  }


  saveData() {

    this.submitted = true;

    if (this.addDeviceFormGroup.invalid) {
      return;
    }

    this.showSuccess();


  }

  showSuccess() {
    this.toastr.success('สำเร็จจ้าา', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล', 'ไม่สำเร็จ');
  }

}
