import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeviceManagementRoutingModule } from './device-management-routing.module';
import { DeviceSearchComponent } from './pages/device-search/device-search.component';
import { DeviceFormComponent } from './pages/device-form/device-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [DeviceSearchComponent, DeviceFormComponent],
  imports: [
    CommonModule,
    DeviceManagementRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class DeviceManagementModule { }
