import {CertUserRoutingGuardService} from '../../guards/cert-user-routing-guard.service';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserRequestComponent} from './pages/user-request/user-request.component';
import {UserRequestStep1Component} from './pages/user-request-step1/user-request-step1.component';
import {UserRequestStep2Component} from './pages/user-request-step2/user-request-step2.component';
import {UserRequestStep3Component} from './pages/user-request-step3/user-request-step3.component';
import {UserRequestStep4Component} from './pages/user-request-step4/user-request-step4.component';
import {CertURL} from '../../certificate.url';
import {UserRequestViewComponent} from './pages/user-request-view/user-request-view.component';
import {UserRequestSearchComponent} from './pages/user-request-search/user-request-search.component';
import {UserUploadOutsourceCertComponent} from './pages/user-upload-outsource-cert/user-upload-outsource-cert.component';
import { UserUploadOutsourceCertFormComponent } from './pages/user-upload-outsource-cert-form/user-upload-outsource-cert-form.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: CertURL.user.search},
  {path: CertURL.user.search, component: UserRequestSearchComponent},
  {path: 'view/:id', component: UserRequestViewComponent},
  {path: 'user-upload-outsource-cert', component: UserUploadOutsourceCertComponent},
  {path: 'user-upload-outsource-cert/form', component: UserUploadOutsourceCertFormComponent},
  {
    path: 'request', component: UserRequestComponent,
    canActivateChild: [CertUserRoutingGuardService],
    children: [
      {path: 'step1', component: UserRequestStep1Component, data: {prevUrl: null, nextUrl: 'step1', step: 0}},
      {path: 'step1/edit/:id', component: UserRequestStep1Component, data: {prevUrl: null, nextUrl: 'step2', step: 0}},
      {path: 'step2', component: UserRequestStep2Component, data: {prevUrl: 'step2', nextUrl: 'step3', step: 1}},
      {path: 'step3', component: UserRequestStep3Component, data: {prevUrl: 'step3', nextUrl: 'step4', step: 2}},
      {path: 'step4', component: UserRequestStep4Component, data: {prevUrl: 'step4', nextUrl: 'home', step: 3}}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertUserRequestRoutingModule {
}
