import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserRequestComponent} from './pages/user-request/user-request.component';
import {UserRequestStep1Component} from './pages/user-request-step1/user-request-step1.component';
import {UserRequestStep2Component} from './pages/user-request-step2/user-request-step2.component';
import {CertUserRequestRoutingModule} from './cert-user-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CertCommonModule} from '../../shared/cert-common.module';
import {SharedModule} from '../../../../shared/shared.module';
import {UserRequestStep3Component} from './pages/user-request-step3/user-request-step3.component';
import {UserRequestSuccessComponent} from './pages/user-request-success/user-request-success.component';
import {UserRequestStep4Component} from './pages/user-request-step4/user-request-step4.component';
import {UiSwitchModule} from 'ngx-ui-switch';
import {AlertModalComponent} from 'src/app/shared/modals/alert-modal/alert-modal.component';
import {UserRequestViewComponent} from './pages/user-request-view/user-request-view.component';
import {CertificateInfoSectionComponent} from './components/certificate-info-section/certificate-info-section.component';
import { UserRequestSearchComponent } from './pages/user-request-search/user-request-search.component';
import { UserHolderInfoSectionComponent } from './components/user-holder-info-section/user-holder-info-section.component';
import {UserRequestInfoSectionComponent} from './components/user-request-info-section/user-request-info-section.component';
import {UserRequestAttachmentTableSectionComponent} from './components/user-request-attachment-table-section/user-request-attachment-table-section.component';
import {UserRequestAttachmentTableViewSectionComponent} from './components/user-request-attachment-table-view-section/user-request-attachment-table-view-section.component';
import { UserUploadOutsourceCertComponent } from './pages/user-upload-outsource-cert/user-upload-outsource-cert.component';
import { UserUploadOutsourceCertFormComponent } from './pages/user-upload-outsource-cert-form/user-upload-outsource-cert-form.component';

@NgModule({
  declarations: [UserRequestComponent, UserRequestStep1Component, UserRequestAttachmentTableSectionComponent, UserRequestAttachmentTableViewSectionComponent,
                 UserRequestStep2Component, UserRequestStep3Component, UserRequestSuccessComponent,
                 UserRequestStep4Component, UserRequestViewComponent, CertificateInfoSectionComponent, UserRequestSearchComponent, UserHolderInfoSectionComponent, UserRequestInfoSectionComponent, UserUploadOutsourceCertComponent, UserUploadOutsourceCertFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    CertCommonModule,
    ReactiveFormsModule,
    CertUserRequestRoutingModule,
    SharedModule,
    UiSwitchModule,
  ],
  exports: [
    UserRequestComponent,
    AlertModalComponent
  ]
})
export class CertUserModule {
}

