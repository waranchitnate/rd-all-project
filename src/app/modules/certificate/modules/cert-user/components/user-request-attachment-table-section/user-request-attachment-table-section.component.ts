import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserCertService} from '../../services/user-cert.service';
import {UserCertAttachment} from '../../../../models/user-cert-attachment.model';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {CertificateDownloadService} from '../../../../services/certificate-download.service';
import {SsoUserModel} from '../../../../../../core/models/sso-user.model';
import {AuthenticationService} from '../../../../../../core/authentication/authentication.service';

@Component({
  selector: 'app-user-request-attachment-table-section',
  templateUrl: './user-request-attachment-table-section.component.html',
  styleUrls: ['./user-request-attachment-table-section.component.css']
})
export class UserRequestAttachmentTableSectionComponent implements OnInit {

  @Input() userTypeIdInput: number;
  @Input() attachmentDtoInput: Array<UserCertAttachment>;

  @Output() formReady = new EventEmitter<FormGroup>();

  constructor( private userCertService: UserCertService
             , private _formBuilder: FormBuilder
             , private certDownloadService: CertificateDownloadService
             , private authenService: AuthenticationService
             , private toastr: ToastrService) {
  }

  attachmentFormGroup: FormGroup;
  maxFileSize = 2000001;

  currentUserInfo: SsoUserModel;

  ngOnInit() {
    this.initAttachmentFormGroup();
    this.getMasterAttachmentTypeByUserTypeId();

    this.currentUserInfo = this.authenService.ssoUserDetailSnapshot;

    // Emit the form group to the parent components to do whatever it wishes
    this.formReady.emit(this.attachmentFormGroup);
  }

  initAttachmentFormGroup() {
    this.attachmentFormGroup = this._formBuilder.group({
      attachments : this._formBuilder.array([])
    });
  }

  getMasterAttachmentTypeByUserTypeId() {
    this.userCertService.getAttachmentTypeByUserTypeId(this.userTypeIdInput).subscribe((resultAttachment: any) => {
      if (resultAttachment.data != null && resultAttachment.data !== undefined) {
          resultAttachment.data.forEach(element => { this.addAttachmentForm(element); });
      }
    });
  }

  get attachmentsFormArray(): FormArray {
    return this.attachmentFormGroup.get('attachments') as FormArray;
  }

  addAttachmentForm(mAttachment: any) {
    if (this.attachmentDtoInput != null) {
      this.attachmentDtoInput.forEach( attachment => {
        if (attachment.attachmentTypeId === mAttachment.id) {
            mAttachment.fileName = attachment.fileName;
        }
      });
    }
    const isRequired = mAttachment.value === '1' ? true : false;
    const form = this._formBuilder.group({
      userAttachmentId: new FormControl(''),
      attachmentTypeId: new FormControl(mAttachment.id, Validators.required),
      attachmentTypeName: new FormControl({value: mAttachment.name, disabled: true}),
      file: new FormControl(mAttachment.file !== undefined ? mAttachment.file : null , ((mAttachment.file == null || mAttachment.file === undefined) && (mAttachment.fileName == null || mAttachment.fileName === undefined) && isRequired) ? Validators.required : null),
      fileName: new FormControl(mAttachment.fileName !== undefined ? mAttachment.fileName : null , mAttachment.isRequied ? Validators.required : null),
      isRequired: new FormControl(isRequired)
    });
    this.attachmentsFormArray.push(form);
  }

  setFileName = function (fileList, index) {
    if (fileList.length > 0) {
      const file = fileList[0];
      if (file.size > this.maxFileSize) {
        this.toastr.info('ไม่สามารถอัพโหลดได้: ไฟล์ต้องมีขนาดไม่เกิน 2 MB เท่านั้น');
      } else {
        this.attachmentsFormArray.at(index).get('file').setValue(file);
        this.attachmentsFormArray.at(index).get('fileName').setValue(file.name);
      }
    }
  };

  fileControl(i): AbstractControl { return this.attachmentsFormArray.at(i).get('file'); }
  fileNameControl(i): AbstractControl { return this.attachmentsFormArray.at(i).get('fileName'); }

  removeAttachmentForm(i) {
    this.attachmentsFormArray.at(i).get('file').setValue(null);
    this.attachmentsFormArray.at(i).get('fileName').setValue(null);
  }

  viewAttachment(i) {
    const file = this.attachmentsFormArray.at(i).get('file').value;
    const fileName = this.attachmentsFormArray.at(i).get('fileName').value;
    if (file !== null) {
      this.certDownloadService.openImageAnotherTabFromCurrentFile(file);
    } else {
      const formData = new FormData();
      formData.append('userName', this.currentUserInfo.username);
      formData.append('fileName', fileName);

      this.userCertService.downloadAttachment(formData).subscribe((res: any) => {
        if (res.status === 200) {
          this.certDownloadService.openImageAnotherTab(res.data);
        } else {
          this.toastr.error('ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง', 'แจ้งเตือน');
        }
      }, error => {
        this.toastr.error('ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง', 'แจ้งเตือน');
      });

    }
  }

}
