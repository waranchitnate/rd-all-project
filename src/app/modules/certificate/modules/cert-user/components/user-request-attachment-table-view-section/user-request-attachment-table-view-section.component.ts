import {Component, Input, OnInit} from '@angular/core';
import {UserCertAttachment} from '../../../../models/user-cert-attachment.model';
import {CertificateDownloadService} from '../../../../services/certificate-download.service';

@Component({
  selector: 'app-user-request-attachment-table-view-section',
  templateUrl: './user-request-attachment-table-view-section.component.html',
  styleUrls: ['./user-request-attachment-table-view-section.component.css']
})
export class UserRequestAttachmentTableViewSectionComponent implements OnInit {

  @Input() attachmentDtoInput: Array<UserCertAttachment>;


  constructor(private certDownloadService: CertificateDownloadService) { }

  ngOnInit() {
  }

  viewAttachment(file) {
    this.certDownloadService.openImageAnotherTabFromCurrentFile(file);
  }

}
