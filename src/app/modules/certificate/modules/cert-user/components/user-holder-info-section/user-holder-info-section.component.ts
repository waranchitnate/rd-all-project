import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CertificateUserInfo} from '../../../../models/user-cert-request-form.model';
import {template} from '@angular/core/src/render3';

@Component({
  selector: 'app-user-holder-info-section',
  templateUrl: './user-holder-info-section.component.html',
  styleUrls: ['./user-holder-info-section.component.css']
})
export class UserHolderInfoSectionComponent implements OnInit {

  constructor(private _formBuilder: FormBuilder) { }

  @Input() isView: boolean;
  @Input() userHolderInfoInput: CertificateUserInfo;

  @Output() formReady = new EventEmitter<FormGroup>();

  userHolderInfoFormGroup: FormGroup;
  showUserCertOSInfo = true;

  ngOnInit() {
    this.initUserHolderInfoFormGroup();
    this.userHolderInfoFormGroup.patchValue(this.userHolderInfoInput);

    // Emit the form group to the parent components to do whatever it wishes
    this.formReady.emit(this.userHolderInfoFormGroup);
    this.setViewOnly(this.isView);
  }

  toggleInfo(templateTypeId: number) {
    this.showUserCertOSInfo = false;
    if (templateTypeId === 4) {
      this.showUserCertOSInfo = true;
    }
  }

  initUserHolderInfoFormGroup() {
    this.userHolderInfoFormGroup = this._formBuilder.group({
      nid: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      orgName: new FormControl('', Validators.required),
      positionName: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      telephone: new FormControl('', Validators.required)
    });
  }

  get nidControl():            AbstractControl { return this.userHolderInfoFormGroup.get('nid'); }
  get nameControl():           AbstractControl { return this.userHolderInfoFormGroup.get('name'); }
  get orgNameControl():        AbstractControl { return this.userHolderInfoFormGroup.get('orgName'); }
  get positionNameControl():   AbstractControl { return this.userHolderInfoFormGroup.get('positionName'); }
  get emailControl():          AbstractControl { return this.userHolderInfoFormGroup.get('email'); }
  get telephoneControl():      AbstractControl { return this.userHolderInfoFormGroup.get('telephone'); }

  isInvalidAndTouched(myControl: AbstractControl) {
    return myControl.invalid && (myControl.dirty || myControl.touched);
  }

  setViewOnly(isView: Boolean) {
    if (isView) {
      const state = isView ? 'disable' : 'enable';
      Object.keys(this.userHolderInfoFormGroup.controls).forEach((controlName) => {
        this.userHolderInfoFormGroup.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
      });
    }
  }

  markFormGroupTouched(formGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  isValid(): boolean {
    this.markFormGroupTouched(this.userHolderInfoFormGroup);
    return this.userHolderInfoFormGroup.valid;
  }
}
