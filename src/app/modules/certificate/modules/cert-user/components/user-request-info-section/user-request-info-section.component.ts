import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CertificateUserInfo} from '../../../../models/user-cert-request-form.model';

@Component({
  selector: 'app-user-request-info-section',
  templateUrl: './user-request-info-section.component.html',
  styleUrls: ['./user-request-info-section.component.css']
})
export class UserRequestInfoSectionComponent implements OnInit {

  constructor(private _formBuilder: FormBuilder) { }

  @Input() isView: boolean;
  @Input() userRequestInfoInput: CertificateUserInfo;

  @Output() formReady = new EventEmitter<FormGroup>();

  userRequestInfoFormGroup: FormGroup;

  ngOnInit() {
    this.initUserRequestInfoFormGroup();
    this.userRequestInfoFormGroup.patchValue(this.userRequestInfoInput);

    // Emit the form group to the parent components to do whatever it wishes
    this.formReady.emit(this.userRequestInfoFormGroup);
    this.setViewOnly(this.isView);
  }

  initUserRequestInfoFormGroup() {
    this.userRequestInfoFormGroup = this._formBuilder.group({
      nid: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      orgName: new FormControl('', Validators.required),
      positionName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required)
    });
  }

  get nidControl():            AbstractControl { return this.userRequestInfoFormGroup.get('nid'); }
  get nameControl():       AbstractControl { return this.userRequestInfoFormGroup.get('name'); }
  get orgNameControl():        AbstractControl { return this.userRequestInfoFormGroup.get('orgName'); }
  get positionNameControl():   AbstractControl { return this.userRequestInfoFormGroup.get('positionName'); }
  get emailControl():          AbstractControl { return this.userRequestInfoFormGroup.get('email'); }
  get telephoneControl():      AbstractControl { return this.userRequestInfoFormGroup.get('telephone'); }

  isInvalidAndTouched(myControl: AbstractControl) {
    return myControl.invalid && (myControl.dirty || myControl.touched);
  }

  setViewOnly(isView: Boolean) {
    if (isView) {
      const state = isView ? 'disable' : 'enable';
      Object.keys(this.userRequestInfoFormGroup.controls).forEach((controlName) => {
        this.userRequestInfoFormGroup.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
      });
    }
  }

  markFormGroupTouched(formGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  isValid(): boolean {
    this.markFormGroupTouched(this.userRequestInfoFormGroup);
    return this.userRequestInfoFormGroup.valid;
  }

}
