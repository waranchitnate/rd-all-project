import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {MustMatch} from '../../../../shared/must-match.service';
import * as moment from 'moment';
import {UserCertService} from '../../services/user-cert.service';
import {CertificateDropdownModel} from '../../../../models/certificate-dropdown.model';
import {CertificateInfo} from '../../../../models/user-cert-request-form.model';
import {template} from '@angular/core/src/render3';

@Component({
  selector: 'app-certificate-info-section',
  templateUrl: './certificate-info-section.component.html',
  styleUrls: ['./certificate-info-section.component.css']
})
export class CertificateInfoSectionComponent implements OnInit {

  @Input() isView: boolean;
  @Input() certificateInfoInput: CertificateInfo;

  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(private _formBuilder: FormBuilder, private userCertService: UserCertService) {}

  certificateInfoFormGroup: FormGroup;
  userTypeDropdown: CertificateDropdownModel[];
  objectiveDropdown: CertificateDropdownModel[];
  deviceTypeDropdown: CertificateDropdownModel[];
  deviceTypeAllDropdown: CertificateDropdownModel[];
  requestTypeDropdown: CertificateDropdownModel[];
  templateTypeDropdown: CertificateDropdownModel[];

  ngOnInit() {
    this.initDropdown();
    this.initCertificateInfoFormGroup();
    this.certificateInfoFormGroup.patchValue(this.certificateInfoInput);

    // Emit the form group to the parent components to do whatever it wishes
    this.formReady.emit(this.certificateInfoFormGroup);
    this.setViewOnly(this.isView);
  }

  initCertificateInfoFormGroup() {
    this.certificateInfoFormGroup = this._formBuilder.group({
      reqId: new FormControl({value: '', disabled: true}),
      createdDateStr: new FormControl({value: '', disabled: true}),
      requestTypeId: new FormControl({value: '', disabled: true}, Validators.required),
      userTypeId: new FormControl({value: '', disabled: true}, Validators.required),
      templateTypeId: new FormControl('', Validators.required),
      lifetime: new FormControl({value: '', disabled: true}),
      deviceTypeId: new FormControl('', Validators.required),
      deviceTypeName: new FormControl(''),
      objectiveCode: new FormControl('', Validators.required),
      pinCode: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      confirmPinCode: new FormControl('', [Validators.required, Validators.minLength(6)])
    }, {validator: MustMatch('pinCode', 'confirmPinCode')});
  }

  get requestTypeIdControl():  AbstractControl { return this.certificateInfoFormGroup.get('requestTypeId'); }
  get userTypeIdControl():     AbstractControl { return this.certificateInfoFormGroup.get('userTypeId'); }
  get templateTypeIdControl(): AbstractControl { return this.certificateInfoFormGroup.get('templateTypeId'); }
  get deviceTypeIdControl():   AbstractControl { return this.certificateInfoFormGroup.get('deviceTypeId'); }
  get objectiveCodeControl():  AbstractControl { return this.certificateInfoFormGroup.get('objectiveCode'); }
  get pinCodeControl():        AbstractControl { return this.certificateInfoFormGroup.get('pinCode'); }
  get confirmPinCodeControl(): AbstractControl { return this.certificateInfoFormGroup.get('confirmPinCode'); }

  markFormGroupTouched(formGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  isInvalidAndTouched(myControl: AbstractControl) {
    return myControl.invalid && (myControl.dirty || myControl.touched);
  }

  initDropdown() {
    this.userCertService.getUserCertType().subscribe((result: any) => {
      this.userTypeDropdown = result.data;
    });

    this.userCertService.getDeviceType().subscribe((result: any) => {
      this.deviceTypeAllDropdown = result.data;
      if (!this.isView) {
        this.getFilterDeviceType(null);
      } else {
        this.deviceTypeDropdown = this.deviceTypeAllDropdown;
      }
    });

    this.userCertService.getCertificateType().subscribe((result: any) => {
       this.templateTypeDropdown = result.data;
    });

    this.userCertService.getCertRequestType().subscribe((result: any) => {
      this.requestTypeDropdown = result.data;
    });

    this.userCertService.getObjective().subscribe((result: any) => {
      this.objectiveDropdown = result.data;
    });
  }

  changeTemplate(e) {
    const templateId = e.target.value.substr(0, 1);
    this.getFilterDeviceType(templateId);
    const lifetime = templateId === '4' ? '1 ปี' : '2 ปี';
    this.certificateInfoFormGroup.get('lifetime').setValue(lifetime, {onlySelf: true});
  }

  getFilterDeviceType(templateId: string) {
    this.deviceTypeDropdown = this.deviceTypeAllDropdown;
    if (templateId === '3') {
      this.deviceTypeDropdown = this.deviceTypeAllDropdown.filter( obj => obj.name.includes('11') );
    } else {
      this.deviceTypeDropdown = this.deviceTypeAllDropdown.filter( obj => obj.name.includes('USB') );
    }
    this.deviceTypeIdControl.setValue(this.deviceTypeDropdown[0].id);
  }

  setViewOnly(isView: Boolean) {
    if (isView) {
      const state = isView ? 'disable' : 'enable';
      Object.keys(this.certificateInfoFormGroup.controls).forEach((controlName) => {
        this.certificateInfoFormGroup.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
      });
    }
  }

  isValid(): boolean {
    this.markFormGroupTouched(this.certificateInfoFormGroup);
    return this.certificateInfoFormGroup.valid;
  }

  getFormValues(): CertificateInfo {
    return this.certificateInfoFormGroup.value as CertificateInfo;
  }

}
