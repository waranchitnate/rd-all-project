import { Component, OnInit } from '@angular/core';
import {CertificateStepService} from '../../../../services/certificate-step.service';

@Component({
  selector: 'app-user-request-success',
  templateUrl: './user-request-success.component.html',
  styleUrls: ['./user-request-success.component.css']
})
export class UserRequestSuccessComponent implements OnInit {

  constructor(private certStepService: CertificateStepService) { }

  ngOnInit() {
  }

  nextPage(){
    this.certStepService.nextStep();
  }

  goBack() {
    this.certStepService.goBack();
  }

}
