import {Component, OnInit} from '@angular/core';
import {UserCertService} from '../../services/user-cert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserCertRegister} from '../../../../models/user-cert-register.model';
import {UserRegistrationDetailModel} from '../../../../../admin/public-user-management/models/user-registration-detail.model';
import {ModalService} from '../../../../../../shared/services/modal.service';
import {SsoUserModel} from '../../../../../../core/models/sso-user.model';
import {AuthenticationService} from '../../../../../../core/authentication/authentication.service';
import {CertificateDownloadService} from '../../../../services/certificate-download.service';
import {MRegisteredAttachmentService} from '../../../../../admin/public-user-management/services/m-registered-attachment.service';
import {FileService} from '../../../../../../shared/services/file.service';
import {NameValuePairModel} from '../../../../../../shared/models/NameValuePair.model';
import {MRegisteredAttachmentModel} from '../../../../../admin/public-user-management/models/m-registered-attachment.model';
import {UserAttachmentModel} from '../../../../../admin/public-user-management/models/user-attachment.model';

@Component({
  selector: 'app-user-request-view',
  templateUrl: './user-request-view.component.html',
  styleUrls: ['./user-request-view.component.css']
})
export class UserRequestViewComponent implements OnInit {

  userCertId: number;
  currentUserInfo: SsoUserModel;
  userCertRegister: UserCertRegister;

  userAttachmentDtoList: Array<UserAttachmentModel> = [];
  requiredAttachmentUserTypes: Array<NameValuePairModel>;
  registeredAttachmentList: Array<MRegisteredAttachmentModel> = [];


  constructor(private activatedRoute: ActivatedRoute,
              private userCertService: UserCertService,
              private modalService: ModalService,
              private router: Router,
              private certDownloadService: CertificateDownloadService,
              public authenticationService: AuthenticationService,
              private mRegisteredAttachmentService: MRegisteredAttachmentService,
              private fileService: FileService
              ) {
  }

  ngOnInit() {
    this.getCurrentUserInfo();
    this.activatedRoute.paramMap.subscribe(v => {
      this.userCertId = +v.get('id');
      if (this.userCertId != null) {
        this.initViewData();
      }
    });
  }

  getCurrentUserInfo() {
    this.currentUserInfo = this.authenticationService.ssoUserDetailSnapshot;
    return this.currentUserInfo;
  }

  initViewData() {
    this.userCertService.getUserCertById(this.userCertId).subscribe((res: any) => {
      this.userCertRegister = res.data;
    }, (error: any) => {
      console.error(error);
      this.modalService.openModal('แจ้งเตือน', error.message);
    });

    // sender only.
    if (this.userCertRegister != null && this.userCertRegister.userDto.userTypeId === 4) {
      this.mRegisteredAttachmentService.getRequiredAttachmentsByUserTypeKey('S').subscribe(registeredAttachmentList => {
        this.registeredAttachmentList = registeredAttachmentList;
        this.setUserAttachmentIdToMRegisteredAttachment();
      });

    }

  }

  setUserAttachmentIdToMRegisteredAttachment() {
    this.registeredAttachmentList.forEach(registeredAttachment => {
      const userAttachment = this.userAttachmentDtoList.find(item => item.mRegisteredAttachmentId === registeredAttachment.id);
      if (userAttachment !== undefined) {
        this.fileService.getUserAttachment(userAttachment.userAttachmentId).subscribe(res => {
          registeredAttachment.fileModel = res;
        });
      }
    });
  }

  downloadFileAttachment(i){
    const userAttachmentForm = this.userCertRegister.userAttachmentDtoList;
    if (userAttachmentForm !== null && userAttachmentForm !== undefined) {
      if(userAttachmentForm[i].file == null || userAttachmentForm[i].file === undefined) {
        const formData = new FormData();
        formData.append('userName', this.userCertRegister.userDto.userName);
        formData.append('fileName', userAttachmentForm[i].fileName);

        this.userCertService.downloadAttachment(formData).subscribe((res: any) => {
          if (res.status === 200) {
            this.certDownloadService.openImageAnotherTab(res.data);
          } else {
            this.modalService.openModal('แจ้งเตือน', 'ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง');
          }
        }, error => {
          this.modalService.openModal('แจ้งเตือน', 'ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง');
        });
      } else {
        this.certDownloadService.openImageAnotherTabFromCurrentFile(userAttachmentForm[i].file);
      }
    }
  }

  backPage() {
    return this.router.navigate(['cert/user/search']);
  }

}
