import {Component, OnInit, ViewChild} from '@angular/core';
import { CertificateDownloadService } from '../../../../services/certificate-download.service';
import {ActivatedRoute, Router} from '@angular/router';
import { UserCertService } from '../../services/user-cert.service';
import { ModalService } from 'src/app/shared/services/modal.service';
import * as moment from 'moment';
import { UserRequestComponent } from '../user-request/user-request.component';
import { UserCertRegister } from 'src/app/modules/certificate/models/user-cert-register.model';
import {SsoUserModel} from '../../../../../../core/models/sso-user.model';
import {CertificateUserInfo, UserCertRequestFormModel} from '../../../../models/user-cert-request-form.model';
import {AuthenticationService} from '../../../../../../core/authentication/authentication.service';
import {UserCert} from '../../../../models/user-cert.model';
import {Certificate} from '../../../../models/certificate.model';
import {UserCertAttachment} from '../../../../models/user-cert-attachment.model';
import {UserHolderInfoSectionComponent} from '../../components/user-holder-info-section/user-holder-info-section.component';

@Component({
  selector: 'app-user-request-step3',
  templateUrl: './user-request-step3.component.html',
  styleUrls: ['./user-request-step3.component.css']
})
export class UserRequestStep3Component implements OnInit {

  getCurrentUserInfo: SsoUserModel;
  templateTypeId: number;
  @ViewChild('userHolderInfoSectionComponent') userHolderInfoSectionComponent: UserHolderInfoSectionComponent;

  constructor(private activatedRoute: ActivatedRoute,
              private certDownloadService: CertificateDownloadService,
              private userCertService: UserCertService,
              private modalService: ModalService ,
              public  userRequestMainComponent: UserRequestComponent,
              private authenService: AuthenticationService,
              private router: Router) {
  }

  ngOnInit() {
    moment.locale('th');
    window.scrollTo({top: 10, behavior: 'smooth'});
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.userRequestMainComponent.userCertRequestForm.certificateUserRequestInfo = this.initDefaultUserInfo(this.getCurrentUserInfo);
    this.userRequestMainComponent.setParentStepAndPath(this.activatedRoute);
    this.userRequestMainComponent.childComponent = this;

    this.templateTypeId = this.userRequestMainComponent.userCertRequestForm.certificateInfo.templateTypeId;
    this.userHolderInfoSectionComponent.toggleInfo(this.templateTypeId);

  }

  initDefaultUserInfo(userDetail: SsoUserModel): CertificateUserInfo {
    const userRequest = new CertificateUserInfo();
    userRequest.nid = userDetail.username;
    userRequest.name = userDetail.nameTH + ' ' + userDetail.surNameTH;
    userRequest.orgCode =  userDetail.userOfficeCode;
    userRequest.orgName = userDetail.userOfficeCode;
    userRequest.positionName = userDetail.userRdOfficeCode;
    userRequest.email = userDetail.email;
    userRequest.telephone = '-';
    return userRequest;
  }

  saveDraft() {
    const saveData = this.convertToUserCertDto(this.userRequestMainComponent.userCertRequestForm, 1);
    this.userRequestMainComponent.registerUserCert(saveData, this.userRequestMainComponent.nextPath, true);
  }

  nextStep(nextPath: string) {
    const saveData = this.convertToUserCertDto(this.userRequestMainComponent.userCertRequestForm, 2);
    this.userRequestMainComponent.registerUserCert(saveData, this.userRequestMainComponent.nextPath, false);
  }

  previousStep(prevPath: string) {
    return this.router.navigateByUrl('/cert/user/request/step2');
  }

  convertToUserCertDto(form: UserCertRequestFormModel, approvalStatusId: number): UserCertRegister {
    const  saveData = new UserCertRegister();
           saveData.userCertId = form.userCertId;
           saveData.approvalStatusId = approvalStatusId;
           saveData.certRequestTypeId = form.certificateInfo.requestTypeId;
           saveData.deviceId = form.certificateInfo.deviceTypeId;
           saveData.objective = form.certificateInfo.objectiveCode;
           saveData.objectiveDesc = form.certificateInfo.objectiveName;
           saveData.userDto = this.getUserDto(form);
           saveData.certificateDto = this.getCertificateDto(form);
           if (form.userAttachmentDtoList !== undefined){
             saveData.userAttachmentDtoList = form.userAttachmentDtoList.attachments;
           }
    return saveData;
  }

  getUserDto(form: UserCertRequestFormModel): UserCert {
    const userDto = new UserCert();
          userDto.userName = form.certificateUserRequestInfo.nid;
          userDto.userTypeId = form.certificateInfo.userTypeId;
          userDto.orgCode = form.certificateUserRequestInfo.orgCode;
          userDto.orgName = form.certificateUserRequestInfo.orgName;
          userDto.email = form.certificateUserHolderInfo.email;
          userDto.telephone = form.certificateUserHolderInfo.telephone;
          userDto.positionName = form.certificateUserHolderInfo.positionName;
    return userDto;
  }

  getCertificateDto(form: UserCertRequestFormModel): Certificate {
    const certificate = new Certificate();
    certificate.nationalId = form.certificateUserHolderInfo.nid;
    certificate.taxPayer = form.certificateUserHolderInfo.name;
    certificate.certTypeId = form.certificateInfo.templateTypeId;
    certificate.passphrase = form.certificateInfo.pinCode;
    certificate.expiredYear = form.certificateInfo.templateTypeId === 4 ? 1 : 2;
    return certificate;
  }



}
