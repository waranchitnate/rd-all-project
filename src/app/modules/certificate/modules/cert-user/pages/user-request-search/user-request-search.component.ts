import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FloatButtonComponent } from '../../../../../../shared/components/float-button/float-button.component';
import { environment } from '../../../../../../../environments/environment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SsoUserModel } from '../../../../../../core/models/sso-user.model';
import {
  DropdownMApprovalStatus,
  DropdownMcertRequestType,
  DropdownMUserType,
  VUserCert
} from '../../../../models/vusercert';
import { MCertReasonCode } from '../../../cert-master/models/m-cert-reason-code.model';
import { PageResponse } from '../../../../../../shared/models/page-response';
import { PageRequest } from '../../../../../../shared/models/page-request';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { VusercertService } from '../../../../models/vusercert.service';
import { UserCertService } from '../../services/user-cert.service';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../../../../core/authentication/authentication.service';
import { CertificateService } from '../../../../services/certificate.service';
import { DomSanitizer } from '@angular/platform-browser';
import { CertificateDownloadService } from '../../../../services/certificate-download.service';
import { UserCertRegister } from '../../../../models/user-cert-register.model';
import { AnswerModalComponent } from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import { ModalService } from '../../../../../../shared/services/modal.service';
import { SignatureService } from 'src/app/modules/certificate/services/signature.service';

@Component({
  selector: 'app-user-request-search',
  templateUrl: './user-request-search.component.html',
  styleUrls: ['./user-request-search.component.css']
})
export class UserRequestSearchComponent implements OnInit {


  @ViewChild(FloatButtonComponent) floatComp;
  downloadCAApi = environment.certApiUrl + 'certificate/register/downloadCertificate-file/';
  downloadRootCAApi = environment.certApiUrl + 'certificate/register/downloadRootCA-file';
  modalRef: BsModalRef;
  getCurrentUserInfo: SsoUserModel;
  rootCAFileUrl;

  dropdownMcerRequestType = <DropdownMcertRequestType[]>[];
  dropdownMApprovalStatus = <DropdownMApprovalStatus[]>[];
  dropdowMUserType = <DropdownMUserType[]>[];
  mcertReasonCode = <MCertReasonCode[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  reasonCodeSubmit = false;
  renewFormGroup: FormGroup;
  selectedReqId: string;
  usbDrive: any;
  selectedDevice: any;
  selectedDrive: any;
  invalidDrive = false;
  warningMsg: any;
  manualDrive: string;
  pin: string;
  pinControl: any;
  manualDriveControl: any;

  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;


  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private vusercertService: VusercertService,
    private fb: FormBuilder, private userCertService: UserCertService,
    private modalService: BsModalService,
    private toastr: ToastrService,
    private authenService: AuthenticationService,
    private certificateService: CertificateService,
    private sanitizer: DomSanitizer,
    private modalAlertService: ModalService,
    private certificateDownloadService: CertificateDownloadService,
    private signatureService: SignatureService) {
  }

  searchVuserCertCriteria = this.fb.group({
    rowNum: [''],
    reqId: [''],
    requestTypeId: [''],
    requestTypeDesc: [''],
    nid: [''],
    name: [''],
    startRequestedDate: [''],
    endRequestedDate: [''],
    userTypeId: ['3'],
    userTypeName: [''],
    approvalStatusId: [''],
    approvalStatusName: [''],
    dbType: ['INTERNAL'],
    certificateId: [''],
  });

  reasonCodeFormGroup = this.fb.group({
    certificateId: [''],
    userCertId: [''],
    reqId: [''],
    requestTypeId: [''],
    requestTypeDesc: [''],
    name: [''],
    reasonCode: ['', Validators.required],
    approvalStatusId: ['']
  });

  vUserCertFormGroup = this.fb.group({
    drive: null
  });
  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequest.sortFieldName = 'REQUESTED_DATE';
    this.pageRequest.sortDirection = 'DESC';
    this.floatComp.urlAdd = '/cert/user/request/step1';
    this.floatComp.tooltipMsg = 'เพิ่มข้อมูล';
    this.getAllMCertRequestType();
    this.getAllMUserType();
    this.getAllMApprovalStatus();
    this.getAllMcertReasonCode();
    this.getData();
  }

  get field() {
    return this.reasonCodeFormGroup.controls;
  }
  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'REQUESTED_DATE';
    this.pageRequest.sortDirection = 'DESC';
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }
  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  clear() {
    this.searchVuserCertCriteria = this.fb.group({
      rowNum: [''],
      reqId: [''],
      requestTypeId: [''],
      requestTypeDesc: [''],
      nid: [''],
      name: [''],
      startRequestedDate: [''],
      endRequestedDate: [''],
      userTypeId: ['3'],
      userTypeName: [''],
      approvalStatusId: [''],
      approvalStatusName: [''],
      dbType: ['INTERNAL'],
      certificateId: [''],
    });
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  getData() {
    const criteria = this.searchVuserCertCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    if (this.getCurrentUserInfo != null) {
      criteria.nid = this.getCurrentUserInfo.username;
    }
    this.vusercertService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.pageResponse.totalPages = res.data.totalElements;
        if (this.pageResponse.size > 0) {
          this.initRootCAFile();
        }
      });
  }

  getAllMUserType() {
    this.vusercertService.getAllMUserType()
      .subscribe(
        resultType => {
          this.dropdowMUserType = resultType.data;
        },
        err => console.log('timeout'),
      );
  }

  getAllMApprovalStatus() {
    this.vusercertService.getAllMApprovalStatus()
      .subscribe(
        resultType => {
          this.dropdownMApprovalStatus = resultType.data;
        },
        err => console.log('timeout'),
      );
  }

  getAllMCertRequestType() {
    this.vusercertService.getAllMCertRequestType()
      .subscribe(
        resultType => {
          this.dropdownMcerRequestType = resultType.data;
        },
        err => console.log('timeout'),
      );
  }
  getAllMcertReasonCode() {
    this.vusercertService.getAllMcertReasonCode()
      .subscribe(
        result => {
          this.mcertReasonCode = result.data;
        },
        err => console.log('timeout'),
      );
  }
  // goToAdd() {
  //   this.router.navigateByUrl('/cert/user/request/step1');

  // }

  goToEdit(row) {
    return this.router.navigate(['/cert/user/request/step1/edit/' + row.userCertId]);
  }

  goToView(row) {
    return this.router.navigate(['/cert/user/view/' + row.userCertId]);
  }

  goToCancelCert(vusercert, template: TemplateRef<any>) {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.modalRef = this.modalService.show(template);
    if (vusercert != null) {
      this.reasonCodeFormGroup = this.fb.group({
        certificateId: vusercert.certificateId,
        userCertId: vusercert.userCertId,
        reqId: vusercert.reqId,
        requestTypeId: '2',
        requestTypeDesc: 'เพิกถอน',
        name: vusercert.name,
        reasonCode: ['', Validators.required],
        approvalStatusId: '2'
      });

    }
  }

  deleteById(row: VUserCert) {
    const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ', 'ยืนยันการลบข้อมูลที่ยังไม่ส่งพิจารณา ชื่อผู้ใช้งาน' + row.name + ' ประเภท ' + row.certificateTypeName);
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        this.userCertService.deleteUserCertById(row.userCertId).subscribe((res: any) => {
          this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
        }, (error: any) => {
          this.toastr.error('ไม่สามารถลบข้อมูลได้', 'แจ้งเตือน');
        });
        this.getData();
      } else {
        // do nothing.
      }
    });
  }




  goToRenew(vusercert, template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    if (vusercert != null) {
      this.renewFormGroup = this.fb.group({
        certificateId: vusercert.certificateId,
        userCertId: vusercert.userCertId,
        reqId: vusercert.reqId,
        requestTypeId: '5',
        requestTypeDesc: 'ต่ออายุ',
        name: vusercert.name,
        approvalStatusId: '2'
      });

    }
  }

  downloadPfx(data) {
    const reqId = data.reqId;
    this.certificateService.downloadPfxFile(reqId).subscribe((res: any) => {
      if (res.status === 200) {
        const linkSource = 'data:application/octet-stream;base64,' + res.responseData.pfxBase64;
        const downloadLink = document.createElement('a');
        const fileName = res.data.fileBaseName + '.' + res.data.fileExtension;
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
      } else {
        this.toastr.info('แจ้งเตือน', 'ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง');
      }
    }, error => {
      this.toastr.info('แจ้งเตือน', 'ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง');
    });
  }

  saveRenew() {
    if (this.renewFormGroup.invalid) {
      return;
    }

    this.vusercertService.updateRequestTypeIdAndApprovalStatusIdByUserCertId(this.renewFormGroup.value)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error => this.showError(),
        () => {
          this.modalRef.hide();
          this.clear();
          this.ngOnInit();
        }
      );
  }

  saveCerRequestType() {
    this.reasonCodeSubmit = true;
    if (this.reasonCodeFormGroup.invalid) {
      return;
    }

    this.vusercertService.updateRequestTypeIdAndApprovalStatusIdByUserCertId(this.reasonCodeFormGroup.value)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error => this.showError(),
        () => {
          this.modalRef.hide();
          this.clear();
          this.ngOnInit();
        }
      );
  }


  showPopup(reqId: string, template: TemplateRef<any>) {
    this.selectedReqId = reqId;
    this.invalidDrive = false;
    this.selectedDrive = undefined;
    this.selectedDevice = undefined;
    this.manualDrive = '';
    this.modalRef = this.modalService.show(template);
    this.getDrivesList();
  }

  getDrivesList() {
    this.usbDrive = [];
    this.signatureService.getLocalDrives().subscribe(result => {
      this.usbDrive = result.data;
    });
  }
  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล ไม่สามารถบันทึกข้อมูลซ้ำได้', 'ไม่สำเร็จ');
  }

  get startDateControl(): AbstractControl {
    return this.searchVuserCertCriteria.get('startRequestedDate');
  }

  get endDateControl(): AbstractControl {
    return this.searchVuserCertCriteria.get('endRequestedDate');
  }

  initRootCAFile() {
    this.certificateService.downloadRootCA().subscribe((res: any) => {
      if (res.status === 200) {
        const blob = this.certificateDownloadService.dataURItoBlob(res.data.dataBase64, 'application/octet-stream');
        this.rootCAFileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
      } else {
      }
    }, error => {
    });
  }

  manualDriveTyping(event) {
    this.manualDrive = event.target.value;
    this.manualDriveControl = event.target;
    this.selectedDrive = undefined;
    this.invalidDrive = false;
  }
  pinTyping(event) {
    this.pin = event.target.value;
    this.pinControl = event.target;
    // this.selectedDrive = undefined;
    // this.invalidDrive = false;
  }
  selectDrive(driveName) {
    this.selectedDrive = driveName;
    this.manualDrive = undefined;
    this.manualDriveControl.value = '';
    this.invalidDrive = false;
  }
  toggleUSB() { this.selectedDevice = 'USB'; this.invalidDrive = false; this.selectedDrive = undefined; }
  toggleToken() { this.selectedDevice = 'Token'; this.invalidDrive = false; this.manualDrive = ''; }

}
