import {UserCertService} from '../../services/user-cert.service';
import {UserRequestComponent} from '../user-request/user-request.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ModalService} from 'src/app/shared/services/modal.service';
import {UserCertRegister} from 'src/app/modules/certificate/models/user-cert-register.model';
import {UserCert} from 'src/app/modules/certificate/models/user-cert.model';
import {Certificate} from 'src/app/modules/certificate/models/certificate.model';
import {UserInfoComponent} from 'src/app/shared/components/user-info/user-info.component';
import {SsoUserModel} from '../../../../../../core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import {
  AttachmentList,
  CertificateInfo,
  CertificateUserInfo,
  UserCertRequestFormModel
} from '../../../../models/user-cert-request-form.model';
import * as moment from 'moment';
import {UserCertAttachment} from '../../../../models/user-cert-attachment.model';


@Component({
  selector: 'app-user-request-step1',
  templateUrl: './user-request-step1.component.html',
  styleUrls: ['./user-request-step1.component.css']
})
export class UserRequestStep1Component implements OnInit {

  getCurrentUserInfo: SsoUserModel;

  @ViewChild(UserInfoComponent) userInfoComponent: UserInfoComponent;
  userCertId;

  constructor(private userRequestMainComponent: UserRequestComponent, private router: Router
    , private activatedRoute: ActivatedRoute, private certService: UserCertService
    , private modalService: ModalService, private authenService: AuthenticationService) {
    moment.locale('th');
  }

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    if (this.userRequestMainComponent.userCertRegister == null) {
      this.userRequestMainComponent.userCertRegister = new UserCertRegister();
    }

    if (this.userRequestMainComponent.userCertRequestForm == null) {
      this.userRequestMainComponent.userCertRequestForm = new UserCertRequestFormModel();
    }

    this.userRequestMainComponent.setParentStepAndPath(this.activatedRoute);
    this.userRequestMainComponent.childComponent = this;

    const userDetail = this.getCurrentUserInfo;

    this.activatedRoute.paramMap.subscribe(v => {
      this.userCertId = v.get('id');
      if (this.userCertId != null) {
        this.getUserCertById(+this.userCertId, userDetail);
      } else {
        this.setAddUserInfo(userDetail);
        return this.router.navigate(['cert/user/request/step2']);
      }
    });
  }

  setAddUserInfo(userDetail: SsoUserModel) {
    const userCertRegister = this.userRequestMainComponent.userCertRegister;
    userCertRegister.userDto = this.convertUserDetailToUserCertRegister(userDetail);
    userCertRegister.certificateDto = this.convertUserDetailToCertificate(userDetail);

    const userCertRequestForm = this.userRequestMainComponent.userCertRequestForm;
          userCertRequestForm.certificateInfo = this.initDefaultCertificateInfo(null);
          userCertRequestForm.certificateUserRequestInfo = this.initDefaultUserInfo(userDetail);
          userCertRequestForm.certificateUserHolderInfo = this.initDefaultUserInfo(userDetail);
          userCertRequestForm.userAttachmentDtoList = this.initDefaultAttachmentList();
  }

  setEditUserInfo(userCertRegister: UserCertRegister) {
    const userCertRequestForm = this.userRequestMainComponent.userCertRequestForm;
    userCertRequestForm.userCertId = userCertRegister.userCertId;
    userCertRequestForm.certificateInfo = this.initDefaultCertificateInfo(userCertRegister);
    userCertRequestForm.certificateUserRequestInfo = this.initEditUserInfo(userCertRegister);
    userCertRequestForm.certificateUserHolderInfo = this.initEditUserInfo(userCertRegister);
    userCertRequestForm.userAttachmentDtoList = this.initEditAttachmentList(userCertRegister);
  }

  initDefaultCertificateInfo(userCertRegister: UserCertRegister): CertificateInfo {
    const certInfo = new CertificateInfo();
    if (userCertRegister !== null) {
        certInfo.reqId = userCertRegister.reqId;
        certInfo.createdDate = userCertRegister.createdDate;
        certInfo.createdDateStr = moment(userCertRegister.createdDate).add(543, 'year').format('DD MMMM YYYY');
        certInfo.userTypeId = userCertRegister.userDto.userTypeId;
        certInfo.templateTypeId = userCertRegister.certificateDto.certTypeId;
        certInfo.lifetime = userCertRegister.certificateDto.certTypeId === 4 ? '2 ปี' : '1 ปี';
        certInfo.deviceTypeId = userCertRegister.deviceId;
        certInfo.requestTypeId = userCertRegister.certRequestTypeId;
        certInfo.objectiveCode = userCertRegister.objectiveCode;
    } else {
        certInfo.reqId = '-';
        certInfo.createdDateStr = moment().add(543, 'year').format('DD MMMM YYYY'),
        certInfo.createdDate = new Date();
        certInfo.userTypeId = 3;
        certInfo.templateTypeId = 4;
        certInfo.lifetime = '1 ปี'
        certInfo.deviceTypeId = 2;
        certInfo.requestTypeId = 1;
        certInfo.objectiveCode = '001';
    }
    return certInfo;
  }

  initDefaultUserInfo(userDetail: SsoUserModel): CertificateUserInfo {
    const userRequest = new CertificateUserInfo();
          userRequest.nid = userDetail.username;
          userRequest.name = userDetail.nameTH + ' ' + userDetail.surNameTH;
          userRequest.orgCode =  userDetail.userOfficeCode;
          userRequest.orgName = userDetail.userOfficeCode;
          userRequest.positionName = userDetail.userRdOfficeCode;
          userRequest.email = userDetail.email;
          userRequest.telephone = '-'
    return userRequest;
  }

  initDefaultAttachmentList(): AttachmentList {
    const attachmentList = new AttachmentList();
    attachmentList.attachments = new Array<UserCertAttachment>();
    return attachmentList;
  }

  initEditUserInfo(userCertRegister: UserCertRegister): CertificateUserInfo {
    const userRequest = new CertificateUserInfo();
    userRequest.nid = userCertRegister.certificateDto.nationalId;
    userRequest.name = userCertRegister.certificateDto.taxPayer;
    userRequest.orgCode =  userCertRegister.userDto.orgCode;
    userRequest.orgName = userCertRegister.userDto.orgName;
    userRequest.positionName = userCertRegister.userDto.positionName;
    userRequest.email = userCertRegister.userDto.email;
    userRequest.telephone = userCertRegister.userDto.telephone;
    return userRequest;
  }

  initEditAttachmentList(userCertRegister: UserCertRegister): AttachmentList{
    const attachmentList = new AttachmentList();
          attachmentList.attachments = userCertRegister.userAttachmentDtoList;
    return attachmentList;
  }

  convertUserDetailToUserCertRegister(userDetail: SsoUserModel) {
    const userCert = new UserCert();
    if (Object.keys(userDetail).length ===  0) { //test only
      userCert.userId = null;
      userCert.userName = '6058166522771';
      userCert.idNumber = '6058166522771';
      userCert.email = 'weeratl@wisesoft.co.th';
      userCert.positionName = 'นักผจญภัย';
      userCert.orgCode = '01110000';
      userCert.orgName = 'have to get from our service';
      return userCert;
    }

    userCert.userId = null;
    userCert.userName = ((userDetail.nameTH !== null && userDetail.surNameTH != null) ? userDetail.nameTH + userDetail.surNameTH : "-");
    userCert.idNumber = (userDetail.username !== null ? userDetail.username : '-') ;
    userCert.email = (userDetail.email !== null ? userDetail.email : '-');

    //no telephone
    userCert.positionName = (userDetail.userPositionNo !== null ? userDetail.userPositionNo : '-');
    userCert.orgCode = userDetail.userOfficeCode;
    userCert.orgName = 'หน่วยงานทดสอบ';
    return userCert;
  }

  getValidStr(text) {
    return text === undefined || text === null ? '' : text;
  }

  convertUserDetailToCertificate(userDetail: SsoUserModel) {
    const certificate = new Certificate();
    if (Object.keys(userDetail).length ===  0) {//test only
      certificate.taxPayer = 'วีระ แหลมทอง';
      certificate.nationalId = '6058166522771';
      return certificate;
    }

    certificate.taxPayer = this.getValidStr(userDetail.nameTH) + ' ' + this.getValidStr(userDetail.surNameTH);
    certificate.nationalId = userDetail.username;

    return certificate;
  }

  getUserCertById(id: number, userDetail: SsoUserModel) {
    this.certService.getUserCertById(id).subscribe((res: any) => {
      this.userRequestMainComponent.userCertRegister = new UserCertRegister();
      this.userRequestMainComponent.userCertRegister = res.data;
      this.setEditUserInfo(res.data);
      this.nextStep(this.userRequestMainComponent.nextPath);
    }, (error: any) => {
      this.modalService.openModal('แจ้งเตือน', error.message);
    });
  }

  nextStep(nextPath: string) {
    this.router.navigateByUrl(nextPath);
  }

}
