import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { VusercertService } from 'src/app/modules/certificate/models/vusercert.service';
import { Router } from '@angular/router';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';

@Component({
  selector: 'app-user-upload-outsource-cert',
  templateUrl: './user-upload-outsource-cert.component.html',
  styleUrls: ['./user-upload-outsource-cert.component.css']
})
export class UserUploadOutsourceCertComponent implements OnInit {
  @ViewChild(FloatButtonComponent) floatComp;
  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  record: Array<any>;
  constructor(private vusercertService: VusercertService,
    private fb: FormBuilder,
    private router: Router) { }

  searchUserUploadCriteria = this.fb.group({
    startRequestedDate: [''],
    endRequestedDate: [''],
  });

  ngOnInit() {
    this.floatComp.urlAdd = 'cert/user/user-upload-outsource-cert/form';
    this.floatComp.tooltipMsg = 'ลงทะเบียนขอใช้งานใบรับรองอิเล็กทรอนิกส์จาก CA ภายนอก';
    this.getData();
  }

  getData(){
    const criteria = this.searchUserUploadCriteria.getRawValue();
      // criteria.pageRequestDto = this.pageRequest;
      this.vusercertService.findAll().subscribe((res) => {
        this.record = res;
      });
  }
  clear(){
    this.searchUserUploadCriteria.reset();
  }
  changeRowLimits(event) {
    this.limit = event.target.value;
    // this.pageChange(this.pageRequest, this.limit);
  }
  viewUpload(id){
    this.router.navigate(['cert/user/user-upload-outsource-cert/form'], { queryParams: {id} });
  }
  get startDateControl(): AbstractControl {
    return this.searchUserUploadCriteria.get('startRequestedDate');
  }

  get endDateControl(): AbstractControl {
    return this.searchUserUploadCriteria.get('endRequestedDate');
  }

}
