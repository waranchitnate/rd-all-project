import { CertURL } from '../../../../certificate.url';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UserCertRegister } from 'src/app/modules/certificate/models/user-cert-register.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalService } from 'src/app/shared/services/modal.service';
import { BsModalService } from 'ngx-bootstrap';
import { UserCertService } from '../../services/user-cert.service';
import { FormGroup} from '@angular/forms';
import {AuthenticationService} from '../../../../../../core/authentication/authentication.service';
import {SsoUserModel} from '../../../../../../core/models/sso-user.model';
import {UserCertRequestFormModel} from '../../../../models/user-cert-request-form.model';


@Component({
  selector: 'app-user-request',
  templateUrl: './user-request.component.html',
  styleUrls: ['./user-request.component.css']
})
export class UserRequestComponent implements OnInit {

  prevPath: string;
  nextPath: string;
  currentStep = 1;
  public childComponent: any;
  private rootModuleUrl;
  userCertRegister: UserCertRegister;
  userCertRequestForm: UserCertRequestFormModel;
  getCurrentUserInfo: SsoUserModel;


  constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: ModalService, private changeDetectorRef: ChangeDetectorRef
    , public bsModalService: BsModalService , private userCertService: UserCertService, public authenService: AuthenticationService) { }

  ngOnInit() {
    this.userCertRegister = new UserCertRegister();
    this.rootModuleUrl = this.activatedRoute.snapshot.parent.routeConfig.path;
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
  }

  previousStep() {
    return this.router.navigateByUrl(this.prevPath);
  }

  setParentStepAndPath(activatedRoute: ActivatedRoute) {
    activatedRoute.data.subscribe(data => {
      this.currentStep = data['step'];
      this.prevPath = (data['prevUrl'] == null ? null : 'cert' + '/'  + this.rootModuleUrl + '/' + activatedRoute.snapshot.parent.routeConfig.path + '/' + data['prevUrl']);
      this.nextPath = (data['nextUrl'] == null ? null : 'cert' + '/'  + this.rootModuleUrl + '/' + (this.currentStep !== 3 ? activatedRoute.snapshot.parent.routeConfig.path + '/' : '') + data['nextUrl']);
    });
    this.changeDetectorRef.detectChanges();
  }

  registerUserCert(userCertRegister: UserCertRegister, nextPath: string, isSaveDraft: boolean) {
    if (userCertRegister.userAttachmentDtoList !== undefined) {
      if (this.currentStep === 2) {
        const formData = new FormData();
        formData.append('userName', userCertRegister.userDto.userName);
        for (const userAttachment of userCertRegister.userAttachmentDtoList) {
          if (userAttachment.file != null) {
            formData.append('file', userAttachment.file);
          }
        }
        this.userCertService.uploadUserAttachment(formData).subscribe(res => {
          this.saveUserCertData(userCertRegister, isSaveDraft);
        }, uploadFileError => {
          this.modalService.openModal('แจ้งเตือน', 'อัพโหลดไฟล์เอกสารไม่สำเร็จ กรุณาเลือกเอกสารให้ถูกต้อง');
        });
      }
    } else {
      this.saveUserCertData(userCertRegister, isSaveDraft);
    }
  }

  saveUserCertData(userCertRegister: UserCertRegister, isSaveDraft: boolean){
    this.userCertService.saveUserCert(userCertRegister).subscribe((saveResult: any) => {
      if (saveResult.status === 200) {
        this.userCertRegister.reqId = saveResult.data.reqId;
        this.modalService.openModal('บันทึกสำเร็จ', saveResult.message);
        if (isSaveDraft) {
          this.router.navigateByUrl('/cert/user/search');
        } else {
          this.router.navigateByUrl(this.nextPath);
        }
      } else {
        this.modalService.openModal('บันทึกไม่สำเร็จ', saveResult.message);
      }
    }, saveError => {
      this.modalService.openModal('แจ้งเตือน', 'บันทึกไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');
    });
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

}
