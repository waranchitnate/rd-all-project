import { Component, OnInit } from '@angular/core';
import { UserCertRegister } from 'src/app/modules/certificate/models/user-cert-register.model';
import { UserRequestComponent } from '../user-request/user-request.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-request-step4',
  templateUrl: './user-request-step4.component.html',
  styleUrls: ['./user-request-step4.component.css']
})
export class UserRequestStep4Component implements OnInit {
  userCertRegister: UserCertRegister;

  constructor(private userRequestMainComponent: UserRequestComponent, private router: Router
    , private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.userCertRegister = this.userRequestMainComponent.userCertRegister;
    this.userRequestMainComponent.setParentStepAndPath(this.activatedRoute);
    this.userRequestMainComponent.childComponent = this;
  }

  nextStep() {
    return this.router.navigateByUrl('/cert/user/search');
}

}
