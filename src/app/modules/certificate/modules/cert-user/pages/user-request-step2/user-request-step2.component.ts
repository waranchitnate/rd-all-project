import { UserRequestComponent } from '../user-request/user-request.component';
import {Component, OnInit, ChangeDetectorRef, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators, AbstractControl } from '@angular/forms';
import { UserCertService } from '../../services/user-cert.service';
import { BsModalService } from 'ngx-bootstrap';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { CertificateService} from '../../../../services/certificate.service';
import { CertificateInfo} from '../../../../models/user-cert-request-form.model';
import {ApproveUserAttachmentTableComponent} from '../../../../../admin/public-user-management/components/approve-user-attachment-table/approve-user-attachment-table.component';
import {UserHolderInfoSectionComponent} from '../../components/user-holder-info-section/user-holder-info-section.component';

@Component({
  selector :  'app-user-request-step2',
  templateUrl :  './user-request-step2.component.html',
  styleUrls :  ['./user-request-step2.component.css']
})
export class UserRequestStep2Component implements OnInit {

  userCertRegisterForm: FormGroup;
  attachmentTmpFormGroup: FormGroup;
  getCurrentUserInfo: SsoUserModel;
  templateTypeIdSelected: number;
  @ViewChild('userHolderInfoSectionComponent') userHolderInfoSectionComponent: UserHolderInfoSectionComponent;

  constructor(private activatedRoute: ActivatedRoute
    , private router: Router
    , public  userRequestMainComponent: UserRequestComponent
    , private modalService: ModalService
    , private bsModalService: BsModalService
    , private userCertService: UserCertService
    , private certService: CertificateService
    , private authenService: AuthenticationService
    , private _formBuilder: FormBuilder) {
  }

  ngOnInit() {

    this.initUserCertRegisterForm();
    window.scrollTo({top: 10, behavior: 'smooth'});
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.userRequestMainComponent.setParentStepAndPath(this.activatedRoute);
    this.userRequestMainComponent.childComponent = this;
    // on init.
    const templateTypeId = this.certificateInfoControl.get('templateTypeId').value;
    this.onChangeTemplateType(templateTypeId);

    // on change.
    this.certificateInfoControl.get('templateTypeId').valueChanges.subscribe(val => {
      this.templateTypeIdSelected = val;
      this.onChangeTemplateType(this.templateTypeIdSelected);
    });
  }

  onChangeTemplateType(val: any) {
    this.userHolderInfoSectionComponent.toggleInfo(val);
    this.userCertRegisterForm.removeControl('userAttachmentDtoList');
    if (val === 4) {
      this.certificateInfoControl.get('lifetime').setValue('1 ปี');
      this.userCertRegisterForm.setControl('userAttachmentDtoList', this.attachmentTmpFormGroup);
    }
  }

  initUserCertRegisterForm() {
    this.userCertRegisterForm = this._formBuilder.group({
      userCertId: new FormControl(this.userRequestMainComponent.userCertRequestForm.userCertId)
    });
  }

  formInitialized(name: string, form: FormGroup) {
    this.userCertRegisterForm.setControl(name, form);
  }

  formAttachmentInitialized(name: string, form: FormGroup) {
    this.attachmentTmpFormGroup = form;
    this.userCertRegisterForm.setControl(name, form);
  }

  nextStep(nextPath: string) {
    if (this.userCertRegisterForm.valid) {
      this.userRequestMainComponent.userCertRequestForm =  this.userCertRegisterForm.getRawValue();
      return this.router.navigateByUrl(nextPath);
    } else {
      this.userRequestMainComponent.markFormGroupTouched(this.userCertRegisterForm);
      this.modalService.openModal('แจ้งเตือน', 'กรุณาระบุข้อมูลให้ถูกต้อง');
    }
  }

  get certificateInfoControl(): AbstractControl { return this.userCertRegisterForm.get('certificateInfo'); }

  get certificateInfo(): CertificateInfo { return this.userCertRegisterForm.get('certificateInfo').value; }




}
