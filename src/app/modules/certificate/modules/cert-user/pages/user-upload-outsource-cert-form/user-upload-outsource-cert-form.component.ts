import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-upload-outsource-cert-form',
  templateUrl: './user-upload-outsource-cert-form.component.html',
  styleUrls: ['./user-upload-outsource-cert-form.component.css']
})
export class UserUploadOutsourceCertFormComponent implements OnInit {
  userUploadFormGroup: FormGroup;
  id = this.actRoute.snapshot.queryParamMap.get('id');
  constructor(private formBuilder: FormBuilder,
    private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.initUserUploadFormGroup();
     if(this.id !=null){
      this.initUserUploadFormGroup();
    }else{
      this.userUploadFormGroup.reset();
    } 
    
  }

  initUserUploadFormGroup() {
    this.userUploadFormGroup = this.formBuilder.group({
      id: [''],
      IssuedName: '',
      IssuedBy:  [''],
      CaFile: [''],
      Objective: [''],
      idn: ['3709900170464'],
      fullName: ['นางสาว วราภรณ์ แสงทอง'],
      department: ['ศูนย์เทคโนโลยีสารสนเทศ'],
      position: ['พนักงานบริษัท'],
      mail: ['waraporn@gmail.com'],
      phone: ['0912224444']
    });
  }
  save() {
  }
}
