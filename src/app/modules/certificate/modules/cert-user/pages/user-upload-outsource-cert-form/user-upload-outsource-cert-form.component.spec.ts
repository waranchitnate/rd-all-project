import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserUploadOutsourceCertFormComponent } from './user-upload-outsource-cert-form.component';

describe('UserUploadOutsourceCertFormComponent', () => {
  let component: UserUploadOutsourceCertFormComponent;
  let fixture: ComponentFixture<UserUploadOutsourceCertFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserUploadOutsourceCertFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserUploadOutsourceCertFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
