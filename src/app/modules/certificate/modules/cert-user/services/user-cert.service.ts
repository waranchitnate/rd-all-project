import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {ResponseT} from 'src/app/shared/models/response.model';
import {VUserCert} from 'src/app/modules/certificate/models/vusercert';
import {Observable} from 'rxjs';
import {UserCertRegister} from 'src/app/modules/certificate/models/user-cert-register.model';
import {UserCertRequestFormModel} from '../../../models/user-cert-request-form.model';

@Injectable({
  providedIn: 'root'
})
export class UserCertService {

  constructor(private http: HttpClient) {
  }

  userManagementEndpointUrl = environment.certApiUrl;
  userCertEndpointUrl = 'certificate/register/';
  commonEndpointUrl = 'certificate/';

  public saveUserCert(userCertRegister: UserCertRegister) {
    return this.http.post<ResponseT<any>>(this.userManagementEndpointUrl + this.userCertEndpointUrl + 'saveUserCert', userCertRegister);
  }

  public uploadUserAttachment(file) {
    return this.http.post(this.userManagementEndpointUrl + this.userCertEndpointUrl + 'uploadUserAttachment', file);
  }

  public getUserCertsByCondition(condition: VUserCert): Observable<any> {
    return this.http.post(this.userManagementEndpointUrl + this.userCertEndpointUrl + 'getUserCertsByCondition', condition);
  }

  public getUserCertById(id: number): Observable<any> {
    return this.http.post(this.userManagementEndpointUrl + this.userCertEndpointUrl + 'getUserCertById', id);
  }

  public deleteUserCertById(id: number): Observable<any> {
    return this.http.post(this.userManagementEndpointUrl + this.userCertEndpointUrl + 'deleteUserCertById', id);
  }

  public downloadAttachment(downloadForm): Observable<any> {
    return this.http.post(this.userManagementEndpointUrl + this.userCertEndpointUrl + 'downloadAttachment', downloadForm);
  }

  public getUserCertType(): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.userManagementEndpointUrl + this.commonEndpointUrl + 'getCertUserType');
  }

  public getDeviceType(): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.userManagementEndpointUrl + this.commonEndpointUrl + 'getDeviceType');
  }

  public getCertificateType(): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.userManagementEndpointUrl + this.commonEndpointUrl + 'getCertificateType');
  }

  public getAttachmentTypeByUserTypeId(userTypeId: number): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.userManagementEndpointUrl + this.commonEndpointUrl + 'getAttachmentTypeByUserTypeId?userTypeId=' + userTypeId);
  }

  public getCertRequestType(): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.userManagementEndpointUrl + this.commonEndpointUrl + 'getCertRequestType');
  }

  public getApprovalStatus(): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.userManagementEndpointUrl + this.commonEndpointUrl + 'getApprovalStatus');
  }

  public getObjective(): Observable<Array<any>> {
    return this.http.get<Array<any>>(this.userManagementEndpointUrl + 'common/getObjective');
  }
}
