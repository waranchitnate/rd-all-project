import { TestBed } from '@angular/core/testing';

import { UserCertService } from './user-cert.service';

describe('UserCertService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserCertService = TestBed.get(UserCertService);
    expect(service).toBeTruthy();
  });
});
