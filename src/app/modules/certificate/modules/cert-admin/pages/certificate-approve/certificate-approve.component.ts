import {Component, OnInit} from '@angular/core';
import {Certification} from '../../../../models/certification';
import {CertificateService} from '../../../../services/certificate.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-certificate-approve',
  templateUrl: './certificate-approve.component.html',
  styleUrls: ['./certificate-approve.component.css']
})
export class CertificateApproveComponent implements OnInit {

  userCert = <Certification[]>[];
  ids: number[];

  constructor(private toastr: ToastrService, private certificateService: CertificateService) {
  }

  ngOnInit() {
    this.ids = [];
    this.certificateService.getAllUserCertByApprovalStatus(1)
      .subscribe(
        result => {
          this.userCert = result.data;
        },
        err => console.log('timeout')
      );

  }

  approveCert(id) {
    this.ids.push(id);
    this.certificateService.approveCerts(this.ids).subscribe(res => {
      this.ids = [];
      this.showSuccess();
      this.ngOnInit();
    }, error1 => this.showError());
  }

  approveCerts() {
    if (this.ids.length === 0) {
      alert('กรุณาเลือกอย่างน้อย 1 รายการ');
      return ;
    } else {
      alert('ยืนยันการอนุมัติ ' + this.ids.length + ' รายการ ');
    }
    //   this.certificateService.approveCerts(this.ids).subscribe(res => {
    //     alert(JSON.stringify(res));
    //     this.ids = [];
    //   });
    // }
  }

  toggleId(id) {
    if (this.ids.includes(id)) {
      this.ids = this.ids.filter(ids => ids !== id);
    } else {
      this.ids.push(id);
    }
  }

  showSuccess() {
    this.toastr.success('อนุมัติใบรับรองอิเลคทรอนิกส์สำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดเกิดขึ้น', 'ไม่สำเร็จ');
  }


}
