import {Component, HostListener, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {HsmConfigService} from '../../../cert-master/services/hsm-config.service';
import {Router} from '@angular/router';
import {CertURL} from '../../../../certificate.url';
import {HsmConfig} from '../../../cert-master/models/hsm-config';
import {PageResponse} from '../../../../../../shared/models/page-response';
import {PageRequest} from '../../../../../../shared/models/page-request';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-hsm-config',
  templateUrl: './hsm-config.component.html',
  styleUrls: ['./hsm-config.component.css']
})
export class HsmConfigComponent implements OnInit {

  LIMITS = [
    {key: '10', value: 10},
    {key: '25', value: 25},
    {key: '50', value: 50},
    {key: '100', value: 100}
  ];

  searchHSMFormgroup: FormGroup;
  windowScrolled: boolean;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  hsmList: any;

  constructor(  private formBuilder: FormBuilder
              , private hsmService: HsmConfigService
              , private router: Router
              , private toastr: ToastrService) { }

  ngOnInit() {
    this.initSearchFormGroup();
    this.doSearch();
  }

  initSearchFormGroup(){
    this.searchHSMFormgroup = this.formBuilder.group({
      hsmName: new FormControl(''),
      hsmSlot: new FormControl('')
    });
  }

  doSearch(){
    this.hsmService.search().subscribe(res => {
      this.pageResponse = res.data;
      this.hsmList = res.data;
      this.pageResponse.totalPages = res.data.totalElements;
    });
  }

  goToEdit(row) {
    this.router.navigateByUrl('cert/admin/' + CertURL.admin.hsmConfigView + '/' + row.hsmId);
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'REQUESTED_DATE';
    this.pageRequest.sortDirection = 'DESC';
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.doSearch();
  }

  deleteItem(item){
    this.hsmService.deleteHSMById(item.hsmId).subscribe(res => {
      if (res.status  === 200){
        this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
        this.doSearch();
      } else {
        this.toastr.error('ไม่สามารถลบข้อมูลได้', 'แจ้งเตือน');
      }
    });
  }

  add(){
    this.router.navigateByUrl('cert/admin/' + CertURL.admin.hsmConfigForm);
  }

  clear(){
    this.searchHSMFormgroup.reset();
  }



}
