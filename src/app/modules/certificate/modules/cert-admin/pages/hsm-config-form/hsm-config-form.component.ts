import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {HsmConfigService} from '../../../cert-master/services/hsm-config.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {CertURL} from '../../../../certificate.url';
import {MustMatch} from '../../../../shared/must-match.service';

@Component({
  selector: 'app-hsm-config-form',
  templateUrl: './hsm-config-form.component.html',
  styleUrls: ['./hsm-config-form.component.css']
})
export class HsmConfigFormComponent implements OnInit {

  constructor(private _formBuilder: FormBuilder, private router: Router
  , private hsmService: HsmConfigService, private activatedRoute: ActivatedRoute
  , private toastr: ToastrService) { }

  get hsmNameControl():  AbstractControl { return this.hsmFormGroup.get('hsmName'); }
  get hsmDescriptionControl():  AbstractControl { return this.hsmFormGroup.get('hsmDescription'); }
  get driverPathControl():  AbstractControl { return this.hsmFormGroup.get('driverPath'); }
  get brandNameControl():  AbstractControl { return this.hsmFormGroup.get('brandName'); }
  get partitionNameControl(): AbstractControl { return this.hsmFormGroup.get('partitionName'); }
  get slotNoControl(): AbstractControl { return this.hsmFormGroup.get('slotNo'); }
  get passwordControl(): AbstractControl { return this.hsmFormGroup.get('password'); }
  get confirmPasswordControl(): AbstractControl { return this.hsmFormGroup.get('confirmPassword'); }

  hsmFormGroup: FormGroup;
  hsmId: number = null;
  isShowDeleteBtn = false;

  ngOnInit() {
    this.initHSMFormGroup();
    this.hsmId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    if (this.hsmId > 0) {
      this.getHsmById(this.hsmId);
      this.isShowDeleteBtn = false;
    }
  }

  initHSMFormGroup() {
    this.hsmFormGroup = this._formBuilder.group({
      hsmId: [''],
      hsmName: ['', Validators.required],
      hsmDescription:  ['', Validators.required],
      driverPath: ['', Validators.required],
      brandName: ['', Validators.required],
      partitionName: ['', Validators.required],
      slotNo: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, {validator: MustMatch('password', 'confirmPassword')});
  }

  getHsmById(id: number) {
    this.hsmService.getHSMById(id).subscribe(res => {
      this.hsmFormGroup.patchValue(res.data);
    });
  }

  delete() {
    const id = this.hsmId;
    this.hsmService.deleteHSMById(id).subscribe(res => {
      if (res.status  === 200) {
        this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
        this.goHome();
      } else {
        this.toastr.error('ไม่สามารถลบข้อมูลได้', 'แจ้งเตือน');
      }
    });
  }

  isInvalidAndTouched(myControl: AbstractControl) {
    return myControl.invalid && (myControl.dirty || myControl.touched);
  }
  clear() {
    this.hsmFormGroup.reset();
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  save() {
    this.markFormGroupTouched(this.hsmFormGroup);
    if (this.hsmFormGroup.valid) {
      this.hsmService.saveHSM(this.hsmFormGroup.getRawValue())
        .subscribe(result => {
          this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
          this.goHome();
        }, error => this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล', 'แจ้งเตือน') );
    }
  }

  goHome() {
    return this.router.navigateByUrl('/cert/admin/' + CertURL.admin.hsmConfig);
  }


}
