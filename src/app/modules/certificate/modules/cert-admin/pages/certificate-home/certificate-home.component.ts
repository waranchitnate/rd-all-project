import {Component, OnInit} from '@angular/core';
import {CertificateService} from '../../../../services/certificate.service';
import {Certification, ReasonCode, RevokeCertDto} from '../../../../models/certification';
import {ToastrService} from 'ngx-toastr';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-certificate-home',
  templateUrl: './certificate-home.component.html',
  styleUrls: ['./certificate-home.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class CertificateHomeComponent implements OnInit {
  revokeSubmit = false;
  revokeFormGroup: FormGroup;
  reasonCodeList = <ReasonCode[]>[];
  userCert = <Certification[]>[];
  selectedData: Certification;
  revokeCertDto: RevokeCertDto;
  ids: number[];

  constructor(private formBuilder: FormBuilder,
              private certificateService: CertificateService,
              private toastr: ToastrService,
              private config: NgbModalConfig,
              private modalService: NgbModal) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {

    this.ids = [];
    this.initFormGroup();

    this.certificateService.getAllUserCert()
      .subscribe(
        result => {
          this.userCert = result.data;
        },
        err => console.log('timeout'),
      );

    this.certificateService.getAllReasonCode().subscribe((result: any) => {
      this.reasonCodeList = result;
    });

  }

  initFormGroup() {
    this.revokeFormGroup = this.formBuilder.group({
      reasonCode: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get field() {
    return this.revokeFormGroup.controls;
  }

  revokeCert() {

    this.revokeSubmit = true;


    if (this.revokeFormGroup.invalid) {
      return;
    }

    this.revokeCertDto = {reasonCode: this.field.reasonCode.value, userCertId: this.selectedData.userCertId};

    this.certificateService.revokeCert(this.revokeCertDto)
      .subscribe(
        res => {
          this.showSuccess();
        },
        error1 => this.showError(),
        () => {
          this.modalService.dismissAll();
          this.ngOnInit();
        }
      );
  }

  showSuccess() {
    this.toastr.success('แจ้งเพิกถอนใบอนุญาตสำเร็จ', 'สำเร็จ');
  }

  showApproveSuccess() {
    this.toastr.success('อนุมัติสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดเกิดขึ้น', 'ไม่สำเร็จ');
  }

  open(modal, data) {
    this.initFormGroup();
    this.selectedData = data;
    this.modalService.open(modal);
  }

  approveCert(id) {
    this.ids.push(id);
    this.certificateService.approveCerts(this.ids).subscribe(res => {
      this.showApproveSuccess();
      this.ngOnInit();
      this.ids = [];
    });
  }

}
