import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {ImportCAService} from '../../../../services/import-ca.service';
import {CertURL} from '../../../../certificate.url';
import {SsoUserModel} from '../../../../../../core/models/sso-user.model';
import {AuthenticationService} from '../../../../../../core/authentication/authentication.service';
import {ImportCaModel} from '../../../../models/import-ca.model';

@Component({
  selector: 'app-import-ca-detail',
  templateUrl: './import-ca-detail.component.html',
  styleUrls: ['./import-ca-detail.component.css']
})
export class ImportCaDetailComponent implements OnInit {

  importCAFormGroup: FormGroup;
  importId: number = null;
  isShowDeleteBtn = false;
  fileUpload: File;
  getCurrentUserInfo: SsoUserModel;

  constructor(  private _formBuilder: FormBuilder
              , private router: Router
              , private importCAService: ImportCAService
              , private activatedRoute: ActivatedRoute
              , private authenticationService: AuthenticationService
              , private toastr: ToastrService) { }

  ngOnInit() {
    window.scrollTo({top: 10, behavior: 'smooth'});
    this.getCurrentUserInfo = this.authenticationService.ssoUserDetailSnapshot;
    this.initFormGroup();
    this.importId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    if (this.importId > 0) {
      this.getImportCAInfoById(this.importId);
      this.isShowDeleteBtn = false;
    }
  }

  setFileName(e) {
    // @ts-ignore
    this.fileUpload = <File> event.target.files[0];
    this.fileNameControl.setValue(this.fileUpload.name);
  }

  getImportCAInfoById(id: number) {
    this.importCAService.getImportCAById(id).subscribe(res => {
      this.importCAFormGroup.patchValue(res.data);
    });
  }

  get issuedByControl(): AbstractControl { return this.importCAFormGroup.get('issuedBy'); }
  get issuedNameControl(): AbstractControl { return this.importCAFormGroup.get('issuedName'); }
  get fileNameControl(): AbstractControl { return this.importCAFormGroup.get('fileName'); }

  initFormGroup() {
    this.importCAFormGroup = this._formBuilder.group({
      id: [''],
      issuedBy: ['', Validators.required],
      issuedName:  ['', Validators.required],
      fileName: ['', Validators.required],
      filePath: [''],
      uploadedBy: [''],
      uploadedDate: ['']
    });
  }

  isInvalidAndTouched(myControl: AbstractControl) {
    return myControl.invalid && (myControl.dirty || myControl.touched);
  }

  submitForm() {
    this.markFormGroupTouched(this.importCAFormGroup);
    if (this.importCAFormGroup.valid) {
      if (this.fileUpload !== undefined) {
        this.storedFileAndSaveData();
      } else {
        this.saveOrUpdate();
      }
    }
  }

  storedFileAndSaveData() {
    const username = this.getCurrentUserInfo == null ? 'SYSTEM' : this.getCurrentUserInfo.username;
    const formData = new FormData();
          formData.append('file', this.fileUpload);
          formData.append('username', username);
    this.importCAService.uploadCARootFile(formData).subscribe(
        res => {
          this.toastr.success('บันทึกไฟล์สำเร็จ', 'สำเร็จ');
          this.importCAFormGroup.get('filePath').setValue(res.data);
          this.importCAFormGroup.get('uploadedBy').setValue(username);
          this.saveOrUpdate(); }
      , error => { this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกไฟล์', 'แจ้งเตือน'); }
    );
  }

  saveOrUpdate() {
    this.importCAService.saveImportCA(this.importCAFormGroup.getRawValue())
      .subscribe(
          result =>
          { this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
            this.goHome();
            }
          , error => this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล', 'แจ้งเตือน') );
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  goHome() {
    return this.router.navigateByUrl('/cert/admin/' + CertURL.admin.importCA);
  }

  delete() {
    const id = this.importId;
    this.importCAService.deleteImportCAById(id).subscribe(res => {
      if (res.status  === 200) {
        this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
        this.goHome();
      } else {
        this.toastr.error('ไม่สามารถลบข้อมูลได้', 'แจ้งเตือน');
      }
    });
  }
}
