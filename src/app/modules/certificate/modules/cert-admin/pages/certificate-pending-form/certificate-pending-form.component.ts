import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {VUserCert, DropdownMcertRequestType} from 'src/app/modules/certificate/models/vusercert';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {VusercertService} from 'src/app/modules/certificate/models/vusercert.service';
import {UserCertRegister} from 'src/app/modules/certificate/models/user-cert-register.model';
import {UserCertService} from '../../../cert-user/services/user-cert.service';
import {UserCertAttachment} from 'src/app/modules/certificate/models/user-cert-attachment.model';
import {CertificateDownloadService} from '../../../../services/certificate-download.service';
import {ModalService} from '../../../../../../shared/services/modal.service';
import { MCertReasonCode } from '../../../cert-master/models/m-cert-reason-code.model';
import { CertReasonCodeService } from '../../../cert-master/services/cert-reason-code.service';
import { CertRequestTypeService } from '../../../cert-master/services/cert-request-type.service';
import {MRegisteredAttachmentService} from '../../../../../admin/public-user-management/services/m-registered-attachment.service';
import {FileService} from '../../../../../../shared/services/file.service';
import {UserAttachmentModel} from '../../../../../admin/public-user-management/models/user-attachment.model';
import {NameValuePairModel} from '../../../../../../shared/models/NameValuePair.model';
import {MRegisteredAttachmentModel} from '../../../../../admin/public-user-management/models/m-registered-attachment.model';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import {ApprovalStatusEnum} from '../../../../models/approval-status.enum';
import {Location} from '@angular/common';
import {toNumber} from 'ng-zorro-antd';
import {PublicUserModel} from '../../../../../../shared/models/public-user.model';
import {PublicUserService} from '../../../../../admin/public-user-management/services/public-user.service';
@Component({
  selector: 'app-certificate-pending-form',
  templateUrl: './certificate-pending-form.component.html',
  styleUrls: ['./certificate-pending-form.component.css']
})
export class CertificatePendingFormComponent implements OnInit {
  @ViewChild('divNid') divNid: ElementRef;
  submitted = false;
  vusercert: VUserCert;
  mCertReasonCode: MCertReasonCode;
  userCertAttachment: UserCertAttachment;
  userAttachment = <UserCertAttachment[]>[];
  formVUserCertApprove: FormGroup;
  rowNum = this.actRoute.snapshot.queryParamMap.get('rowNum');
  size: number;
  buttonDisabled: boolean;
  getCurrentUserInfo: SsoUserModel;
  canEdit: boolean;

  constructor(private vUserCertService: VusercertService, private fb: FormBuilder,
              private router: Router,
              private _location: Location,
              private actRoute: ActivatedRoute,
              private toastr: ToastrService,
              private userCertService: UserCertService,
              private certDownloadService: CertificateDownloadService,
              private modalService: ModalService,
              private mcertreasoncodeService: CertReasonCodeService,
              private mcerrequestTypeService: CertRequestTypeService,
              private authenService: AuthenticationService ) {
  }


  initFormGroup() {
    this.formVUserCertApprove = this.fb.group({
      id: this.rowNum,
      requestTypeId: '',
      approvalStatusId: '',
      approvalStatus: 'APPROVED',
      remark: [''],
      deviceTypeName: ['']
    });
  }

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.submitted = false;
    this.initFormGroup();
    this.getVuserCert(this.rowNum);
  }

  get field() {
    return this.formVUserCertApprove.controls;
  }

  saveVUserCert() {
    this.submitted = true;

    const approvalStatusName = this.field.approvalStatus.value;
    const remark = this.field.remark.value
    const deviceTypeId = this.vusercert.deviceTypeId;
    const requestTypeId = this.vusercert.requestTypeId;
    const approvalStatusId = ApprovalStatusEnum[approvalStatusName];

    this.field.approvalStatusId.setValue(approvalStatusId);
    if (toNumber(approvalStatusId) !== ApprovalStatusEnum.APPROVED) {
      if (!remark || /^\s*$/.test(remark)) {
        this.field.remark.setErrors({'required': true});
        return ;
      }
    }

    this.vUserCertService.UpdateApprovalStatusWithRemark(this.formVUserCertApprove.value).subscribe((result: any) => {
        this.showSuccess();
        this.backPage();
      },
      err => {
        this.showError();
      });
  }

  getVuserCert(rowNum) {
    if (rowNum != null) {
      this.vUserCertService.getVuserCertById(rowNum).subscribe((result: any) => {
        this.vusercert = result.data;
        this.getUserAttachmentByUsername(result.data.userId);

        if(this.vusercert.approvalStatusId === ApprovalStatusEnum.APPROVED
         || this.vusercert.approvalStatusId === ApprovalStatusEnum.REJECTED
         || this.vusercert.approvalStatusId === ApprovalStatusEnum.UNAPPROVED){
            this.canEdit = false;
        } else {
          if (this.vusercert.userTypeId === 4) {
            this.canEdit = this.vusercert.requestTypeId !== 1 ? true : false;
          } else {
            this.canEdit = true;
          }
        }

        if (result.data.deviceTypeId != null) {
          this.buttonDisabled = true;
        } else {
          this.buttonDisabled = false;
        }
        if(result.data.reasonCode != null){
          this.mcertreasoncodeService.getCerReasonCodeById(result.data.reasonCode).subscribe(
            res => {
              this.mCertReasonCode = res.data;
            },
            err => console.log('timeout'),
          );
        }

      });

      this.submitted = false;
    }
  }


  getUserAttachmentByUsername(userId) {
    if (userId != null) {
      this.vUserCertService.getUserAttachmentByUsername(userId).subscribe((result: any) => {
        this.userAttachment = result.data;
      });
    }
  }

  addDeviceType(rowNum) {
    if (rowNum != null) {
      return this.router.navigate(['thumb-drive/register'], {queryParams: {rowNum: rowNum}});
    }
  }

  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล', 'ไม่สำเร็จ');
  }

  clear() {
    this.formVUserCertApprove = this.fb.group({
      id: this.rowNum,
      approvalStatus: 'APPROVE',
      remark: null
    });
  }

  backPage() {
    this._location.back();
  }

  downloadFileAttachment(i) {
    const userAttachmentForm = this.userAttachment;

    if (userAttachmentForm !== null && userAttachmentForm !== undefined) {
      if (userAttachmentForm[i].filePath !== null || userAttachmentForm[i].filePath !== undefined) {
        const formData = new FormData();
        formData.append('userName', userAttachmentForm[i].createdBy);
        formData.append('fileName', userAttachmentForm[i].filePath);

        this.userCertService.downloadAttachment(formData).subscribe((res: any) => {
          if (res.status === 200) {
            this.certDownloadService.openImageAnotherTab(res.data);
          } else {
            this.modalService.openModal('แจ้งเตือน', 'ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง');
          }
        }, error => {
          this.modalService.openModal('แจ้งเตือน', 'ไม่สามารถดาวน์โหลดไฟล์ได้ กรุณาลองใหม่อีกครั้ง');
        });
      }
    }
  }

}
