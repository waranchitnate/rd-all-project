import { Component, OnInit } from '@angular/core';
import { VUserCert, DropdownMcertRequestType, DropdownMApprovalStatus, DropdownMUserType } from 'src/app/modules/certificate/models/vusercert';
import { PageResponse } from 'src/app/shared/models/page-response';
import { PageRequest } from 'src/app/shared/models/page-request';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { VusercertService } from 'src/app/modules/certificate/models/vusercert.service';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {ModalService} from 'src/app/shared/services/modal.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import {CertificateService} from '../../../../../services/certificate.service';

@Component({
  selector: 'app-server-certificate',
  templateUrl: './server-certificate.component.html',
  styleUrls: ['./server-certificate.component.css']
})
export class ServerCertificateComponent implements OnInit {

  vusercert = <VUserCert[]>[];
  dropdownMcerRequestType = <DropdownMcertRequestType[]>[];
  dropdownMApprovalStatus = <DropdownMApprovalStatus[]>[];
  dropdowMUserType = <DropdownMUserType[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  ids: number[];
  LIMITS = [
    {key: '10', value: 10},
    {key: '25', value: 25},
    {key: '50', value: 50},
    {key: '100', value: 100}
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  getCurrentUserInfo: SsoUserModel;
  constructor(private vusercertService: VusercertService,
    private fb: FormBuilder,
    private router: Router,
    private actRoute: ActivatedRoute,
    private toastr: ToastrService,
    private modalService: ModalService,
    private authenService: AuthenticationService,
    private certificateService: CertificateService) { }

    searchVuserCertCriteria = this.fb.group({
      rowNum: [''],
      reqId: [''],
      requestTypeId: [''],
      requestTypeDesc: [''],
      nid: [''],
      name: [''],
      startRequestedDate: [''],
      endRequestedDate: [''],
      userTypeId: [''],
      userTypeName: [''],
      approvalStatusId: [''],
      approvalStatusName: [''],
      certificateTypeId: ['2']
    });

    pageChange(event: any, limit) {
      this.pageRequest = event;
      this.pageRequest.sortFieldName = 'REQUESTED_DATE';
      this.pageRequest.sortDirection = 'DESC';
      if (limit != null) {
        event.itemsPerPage = limit;
      }
      this.getData();
    }

    changeRowLimits(event) {
      this.limit = event.target.value;
      this.pageChange(this.pageRequest, this.limit);
    }

    ngOnInit() {
      this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
      this.pageRequest.sortFieldName = 'REQUESTED_DATE';
      this.pageRequest.sortDirection = 'DESC';
      this.ids = [];
      this.getAllMCertRequestType();
      this.getAllMUserType();
      this.getAllMApprovalStatus();
      this.getData();
    }

    sort(fieldName) {
      if (this.pageRequest.sortFieldName === fieldName) {
        this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
      } else {
        this.pageRequest.sortFieldName = fieldName;
        this.pageRequest.sortDirection = 'ASC';
      }
      this.getData();
    }

    clear() {
      // this.searchVuserCertCriteria.reset();
      this.searchVuserCertCriteria = this.fb.group({
        rowNum: [''],
        reqId: [''],
        requestTypeId: [''],
        requestTypeDesc: [''],
        nid: [''],
        name: [''],
        startRequestedDate: [''],
        endRequestedDate: [''],
        userTypeId: [''],
        userTypeName: [''],
        approvalStatusId: [''],
        approvalStatusName: [''],
        certificateTypeId: ['2']
      });
    }

    getData() {
      const criteria = this.searchVuserCertCriteria.getRawValue();
      criteria.pageRequestDto = this.pageRequest;
      this.vusercertService.getSearchCriteria(criteria).subscribe(
        (res: any) => {
          this.pageResponse = res.data;
          this.pageResponse.totalPages = res.data.totalElements;
        });
    }

    getAllMCertRequestType() {
      this.vusercertService.getAllMCertRequestType()
        .subscribe(
          resultType => {
            this.dropdownMcerRequestType = resultType.data;
          },
          err => console.log('timeout')
        );
    }

    viewSendApproveCert(rowNum) {
      this.router.navigate(['/cert/admin/pending-list/form'], {queryParams: {rowNum}});
    }

    downloadCert(rowNum) {
      this.vusercertService.sendToDownloadCert(rowNum).subscribe(res => {
        this.toastr.success('ดาวน์โหลดสำเร็จ', 'สำเร็จ');
      }, error1 => this.showError('พบข้อผิดพลาดระหว่างดาวน์โหลดไฟล์'));
    }

    sendEmail(v) {
      if (v != null) {
        const modalRef = this.modalService.openConfirmModal('ยืนยันการจัดส่งอีเมล', 'ยืนยันการจัดส่งใบรับรองอิเล็กทรอนิกส์ให้แก่ คุณ ' + v.name + ' ผ่านอีเมลแอดเดรส ' + v.email);
        (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
          if (result) {
            this.vusercertService.sendEmail(v.rowNum).subscribe(res => {
              this.toastr.success('ยืนยันการจัดส่งอีเมล', 'สำเร็จ');
            }, error1 => this.showError('พบข้อผิดพลาดระหว่างจัดส่งอีเมล'));

          }
        });

      }

    }

    getAllMUserType() {
      this.vusercertService.getAllMUserType()
        .subscribe(
          resultType => {
            this.dropdowMUserType = resultType.data;
          },
          err => console.log('timeout'),
        );
    }

    getAllMApprovalStatus() {
      this.vusercertService.getAllMApprovalStatus()
        .subscribe(
          resultType => {
            this.dropdownMApprovalStatus = resultType.data;
          },
          err => console.log('timeout'),
        );
    }

    toggleId(rowNum) {
      if (this.ids.includes(rowNum)) {
        this.ids = this.ids.filter(ids => ids !== rowNum);
      } else {
        this.ids.push(rowNum);
      }

    }

    approveCert() {
      if (this.ids.length === 0) {
        this.toastr.error('กรุณาเลือกอย่างน้อย 1 รายการ', 'เกิดข้อผิดพลาด');
        return;
      } else {
        const modalRef = this.modalService.openConfirmModal('ยืนยันการอนุมัติ ', this.ids.length + ' รายการ ');
        (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
          if (result) {
            this.vusercertService.approveCerts(this.ids).subscribe(res => {
              this.toastr.success('ยืนยันการอนุมัติ', 'สำเร็จ');
              this.ngOnInit();
            }, error1 => this.showError('พบข้อผิดพลาดระหว่างดำเนินการ'));
          } else {
          }
        });
      }
    }

    issueCertForSender(data) {
      this.certificateService.issueCertForSender(data.reqId).subscribe(res => {
        this.toastr.success('ออกใบรับรองอิเล็กทรอนิกส์สำเร็จ', 'สำเร็จ');
        this.getData();
      }, error => this.toastr.error('พบข้อผิดพลาดระหว่างออกใบรับรองอิเล็กทรอนิกส์'));
    }

  saveFileToLocal(data) {
    this.certificateService.getSenderPFXToLocal(data.reqId).subscribe(res => {
      this.toastr.success('จัดเก็บไฟล์สำเร็จ พร้อมสำหรับดาวน์โหลด', 'สำเร็จ');
    }, error => this.toastr.error('ไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ'));
  }

  showSuccess() {
      this.toastr.success('อนุมัติข้อมูลสำเร็จ', 'สำเร็จ');
    }

    showError(msg: string) {
      if (msg !== undefined) {
        this.toastr.error(msg, 'ไม่สำเร็จ');
      } else {
        this.toastr.error('พบข้อผิดพลาดระหว่างอนุมัติข้อมูล ไม่สามารถอนุมัติได้', 'ไม่สำเร็จ', {
          timeOut: 3000
        });
      }
    }

    get startDateControl(): AbstractControl {
      return this.searchVuserCertCriteria.get('startRequestedDate');
    }

    get endDateControl(): AbstractControl {
      return this.searchVuserCertCriteria.get('endRequestedDate');
    }

}
