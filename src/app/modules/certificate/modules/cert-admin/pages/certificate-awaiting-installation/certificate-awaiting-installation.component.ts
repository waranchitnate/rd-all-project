import { Component, OnInit, TemplateRef } from '@angular/core';

import { VusercertService } from '../../../../models/vusercert.service';
import { FormBuilder, AbstractControl } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PageRequest } from '../../../../../../shared/models/page-request';
import { PageResponse } from '../../../../../../shared/models/page-response';
import { VUserCert } from '../../../../models/vusercert';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CertificateService } from '../../../../services/certificate.service';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import {SignatureService} from '../../../../services/signature.service';
import {InstallCertificateModel, RequestDataModel} from '../../../../models/install-certificate.model';

@Component({
  selector: 'app-certificate-awaiting-installation',
  templateUrl: './certificate-awaiting-installation.component.html',
  styleUrls: ['./certificate-awaiting-installation.component.css']
})
export class CertificateAwaitingInstallationComponent implements OnInit {
  modalRef: BsModalRef;
  minDate = new Date();
  maxDate = new Date();
  bsConfig: Partial<BsDatepickerConfig>;
  vUserCert = <VUserCert[]>[];
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  currentPage = 0;
  usbDrive: any;
  selectedDevice: any;
  selectedDrive: any;
  invalidDrive = false;
  warningMsg: any;
  selectedReqId: string;
  manualDrive: string;
  manualDriveControl: any;
  pin: string;
  pinControl: any;
  requestDataModel: RequestDataModel;
  installCertList: InstallCertificateModel[] = [];


  LIMITS = [
    { key: '10', value: 10 },
    { key: '25', value: 25 },
    { key: '50', value: 50 },
    { key: '100', value: 100 }
  ];

  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  getCurrentUserInfo: SsoUserModel;

  constructor(private vUserCertService: VusercertService,
    private fb: FormBuilder,
    private router: Router,
    private actRoute: ActivatedRoute,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private certificateService: CertificateService,
    private signatureService: SignatureService,
    private authenService: AuthenticationService) {

    this.minDate = new Date();
    this.maxDate = new Date();
    this.minDate.setDate(this.minDate.getDate());
    this.maxDate.setDate(this.maxDate.getDate());
    this.bsConfig = Object.assign({}, { containerClass: 'theme-orange' });
  }

  searchVUserCertCriteria = this.fb.group({
    reqId: [''],
    nid: [''],
    name: [''],
    startRequestedDate: [''],
    endRequestedDate: [''],
    requestTypeId: ['1'],
    userTypeId: ['3'],
    approvalStatusId: ['3']
  });

  vUserCertFormGroup = this.fb.group({
    drive: null
  });

  ngOnInit() {
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequest.sortFieldName = 'REQUESTED_DATE';
    this.pageRequest.sortDirection = 'DESC';
    this.getData();
  }

  getData() {
    const criteria = this.searchVUserCertCriteria.getRawValue();
    criteria.pageRequestDto = this.pageRequest;
    this.vUserCertService.getSearchCriteria(criteria).subscribe(
      (res: any) => {
        this.pageResponse = res.data;
        this.vUserCert = this.pageResponse.content;
        this.pageResponse.totalPages = res.data.totalElements;

      });
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.getData();
  }

  showPopup(reqId: string, template: TemplateRef<any>) {
    this.selectedReqId = reqId;
    this.invalidDrive = false;
    this.selectedDrive = undefined;
    this.selectedDevice = undefined;
    this.manualDrive = '';
    this.modalRef = this.modalService.show(template);
    this.getDrivesList();
  }

  getDrivesList() {
    this.usbDrive = [];
    this.signatureService.getLocalDrives().subscribe(result => {
      this.usbDrive = result.data;
    }) ;
  }

  clear() {
    this.searchVUserCertCriteria = this.fb.group({
      reqId: [''],
      nid: [''],
      name: [''],
      startRequestedDate: [''],
      endRequestedDate: [''],
      requestTypeId: ['1'],
      userTypeId: ['3'],
      approvalStatusId: ['3']
    });
    // this.searchVUserCertCriteria.reset();
  }

  minMaxDate(startRequestedDate) {
    if (startRequestedDate != null && startRequestedDate !== '') {
      this.minDate = startRequestedDate;
      this.minDate.setDate(this.minDate.getDate());
    }
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'REQUESTED_DATE';
    this.pageRequest.sortDirection = 'DESC';
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.getData();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

  save() {
    if (this.selectedDevice !== undefined) {
      if (this.selectedDevice === 'USB' && (this.selectedDrive === undefined && this.manualDrive === "")) {
        this.invalidDrive = true;
        this.warningMsg = 'กรุณาเลือกไดร์ฟสำหรับติดตั้ง';
      }else if (this.selectedDevice === 'Token' ) {
          this.installToken(this.pin);
      } else {
        this.selectedDrive = this.selectedDrive ? this.selectedDrive : this.manualDrive;
        this.installCertificateIntoDevice(this.selectedDevice, this.selectedDrive);
      }
    } else {
      this.invalidDrive = true;
      this.warningMsg = 'กรุณาเลือกรูปแบบอุปกรณ์สำหรับติดตั้ง';
    }
  }

  installCertificateIntoDevice(device, drive) {
    if (device === 'Token') {
      this.modalRef.hide();
      this.toastr.success('ติดตั้งใบรับรองอิเล็กทรอนิกส์ลงอุปกรณ์เรียบร้อย', 'สำเร็จ');
    } else { // usb
      this.signatureService.preparedInfo(this.selectedReqId)
        .subscribe((result: any) => {
        if (result.data.pfxBase64 == null) {
          this.toastr.error('ไม่สามารถติดตั้งใบรับรองอิเล็กทรอนิกส์ลงอุปกรณ์ได้', 'ไม่สำเร็จ');
        } else {
          this.installCertificate(result.data, drive);
        }
      });
    }
  }
  installToken(data) {
    var requestData = null
    this.signatureService.findUserInfoOfCertificate(this.selectedReqId)
    .subscribe((result: any) => {
      requestData  = {
        pin: data,
        reqId :result.data.reqId,
        taxId :result.data.taxId,
        taxpayer :result.data.taxpayer,
        branchId :result.data.branchId,
        dept :result.data.dept,
        email :result.data.email,
        userId :result.data.userId
      };
      
  });
   

    const obj  = { requestData: [] };
    obj.requestData.push(requestData);
    console.log(obj);

    this.signatureService.genKeyAndCSR(obj).subscribe((result: any) => {
      console.log(result);
      
      if (result.code === '200') {
        const token  = {
          csrBase64: result.data,
          reqId :this.selectedReqId
        };
        this.signatureService.issueCerToken(token).subscribe((result2: any) => {
          console.log(result2.data.responseData[0].cerBase64);
         
          
            const reqData  = {
              base64FileEncrypt: result2.data.responseData[0].cerBase64,
              pin: data
            };
            const obj2  = { requestData: [] };
            obj2.requestData.push(reqData);
            this.signatureService.saveCerToToken(obj2).subscribe((result3: any) => {
              if (result3.code === '200') {
                  this.toastr.success('แจ้งผลการดำเนินการ', 'สำเร็จ');
              } else {
                this.toastr.info('แจ้งผลการดำเนินการ', result.msg);
              }
            });
           
        
    
        });
        // this.toastr.success('แจ้งผลการดำเนินการ', 'สำเร็จ');
      } else {
        // this.toastr.info('แจ้งผลการดำเนินการ', result.msg);
      }
      this.modalRef.hide();
    });
  }
  installCertificate(data, drive) {

    const requestData  = {
      key: data.userKey,
      drive: drive,
      pfxBase64: data.pfxBase64
    };

    const obj  = { requestData: [] };
    obj.requestData.push(requestData);
    console.log(obj);

    this.signatureService.savePFXIntoUsb(obj).subscribe((result: any) => {
      console.log(result);
      this.modalRef.hide();
      if (result.code === '200') {
        this.toastr.success('แจ้งผลการดำเนินการ', 'สำเร็จ');
      } else {
        this.toastr.info('แจ้งผลการดำเนินการ', result.msg);
      }

    });
  }

  manualDriveTyping(event) {
    this.manualDrive = event.target.value;
    this.manualDriveControl = event.target;
    this.selectedDrive = undefined;
    this.invalidDrive = false;
  }
  pinTyping(event) {
    this.pin = event.target.value;
    this.pinControl = event.target;
    // this.selectedDrive = undefined;
    // this.invalidDrive = false;
  }


  selectDrive(driveName) {
    this.selectedDrive = driveName;
    this.manualDrive = undefined;
    this.manualDriveControl.value = '';
    this.invalidDrive = false;
  }

  toggleUSB()   { this.selectedDevice = 'USB'; this.invalidDrive = false; this.selectedDrive = undefined; }
  toggleToken() { this.selectedDevice = 'Token'; this.invalidDrive = false; this.manualDrive = ''; }

  get startDateControl(): AbstractControl { return this.searchVUserCertCriteria.get('startRequestedDate'); }
  get endDateControl(): AbstractControl { return this.searchVUserCertCriteria.get('endRequestedDate'); }

}
