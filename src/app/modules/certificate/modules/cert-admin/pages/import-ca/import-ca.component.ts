import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {PageResponse} from '../../../../../../shared/models/page-response';
import {PageRequest} from '../../../../../../shared/models/page-request';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {CertURL} from '../../../../certificate.url';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {ModalService} from '../../../../../../shared/services/modal.service';
import {ImportCAService} from '../../../../services/import-ca.service';
import {ImportCaModel} from '../../../../models/import-ca.model';
import {CertificateDownloadService} from '../../../../services/certificate-download.service';

@Component({
  selector: 'app-import-ca',
  templateUrl: './import-ca.component.html',
  styleUrls: ['./import-ca.component.css']
})
export class ImportCaComponent implements OnInit {

  LIMITS = [
    {key: '10', value: 10},
    {key: '25', value: 25},
    {key: '50', value: 50},
    {key: '100', value: 100}
  ];

  searchImportCAFormGroup: FormGroup;
  windowScrolled: boolean;
  pageResponse = new PageResponse<any>();
  pageRequest = new PageRequest();
  limit: number = this.LIMITS[0].value;
  rowLimits: Array<any> = this.LIMITS;
  resultSize = 0;
  resultList: Array<ImportCaModel>;

  constructor(  private formBuilder: FormBuilder
    , private importCAService: ImportCAService
    , private router: Router
    , private modalAlertService: ModalService
    , private certificateDownloadService: CertificateDownloadService
    , private toastr: ToastrService) { }

  ngOnInit() {
    this.initSearchFormGroup();
    this.doSearch();
  }

  initSearchFormGroup() {
    this.searchImportCAFormGroup = this.formBuilder.group({
      issuedName: new FormControl(''),
      issuedBy: new FormControl('')
    });
  }

  doSearch() {
    this.importCAService.search(this.searchImportCAFormGroup.getRawValue()).subscribe(res => {
      this.resultList = res.data;
      this.resultSize = this.resultList.length;
    });
  }

  add() {
    return this.router.navigateByUrl('cert/admin/' + CertURL.admin.importCADetail);
  }

  downloadFile(data: ImportCaModel) {
    const downloadForm = new FormData();
          downloadForm.append('filePath', data.filePath);
          downloadForm.append('filename', data.fileName);
    this.importCAService.downloadFile(downloadForm).subscribe(
      result => { this.certificateDownloadService.downloadFileIe(result.data); }
      , error => { this.toastr.error('ไม่พบไฟล์ที่ต้องการดาวน์โหลด', 'แจ้งเตือน'); }
    );
  }

  goToEdit(row) {
    return this.router.navigateByUrl('cert/admin/' + CertURL.admin.importCADetail + '/' + row.id);
  }

  deleteItem(id) {
    const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ', 'ยืนยันการลบข้อมูล?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
      if (result) {
        this.importCAService.deleteImportCAById(id)
          .subscribe(
            res => {
              this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
            },
            error =>  this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล ไม่สามารถเชื่อมต่อเซิร์ฟเวอร์ได้', 'ไม่สำเร็จ') ,
            () => {
              this.ngOnInit();
            }
          );
      } else {}
    });
  }

  clear() {
    this.searchImportCAFormGroup.reset();
  }

  pageChange(event: any, limit) {
    this.pageRequest = event;
    this.pageRequest.sortFieldName = 'REQUESTED_DATE';
    this.pageRequest.sortDirection = 'DESC';
    if (limit != null) {
      event.itemsPerPage = limit;
    }
    this.doSearch();
  }

  changeRowLimits(event) {
    this.limit = event.target.value;
    this.pageChange(this.pageRequest, this.limit);
  }

}
