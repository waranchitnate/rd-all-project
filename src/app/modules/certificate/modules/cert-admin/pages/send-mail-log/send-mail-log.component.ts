import { Component, OnInit } from '@angular/core';
import { PageRequest } from 'src/app/shared/models/page-request';
import { PageResponse } from 'src/app/shared/models/page-response';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SendMailLogTable } from '../../../../models/send-mail-log-form-table.model';
import { NumberListElements } from '../../../../models/number-list-elements.model';

import { ResponseT } from 'src/app/shared/models/response.model';
import { SendMailLogInformation } from '../../../../models/send-mail-log-information.model';

import { DropDownList } from '../../../../models/drop-down-list.model';
import { SentMailLogService } from '../../../../services/sent-mail-log.service';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-send-mail-log',
  templateUrl: './send-mail-log.component.html',
  styleUrls: ['./send-mail-log.component.css']
})
export class SendMailLogComponent implements OnInit {


   pageRequestAssistor: PageRequest = new PageRequest();
   pageResponseAssistor: PageResponse<any> = new PageResponse<any>();

   sendMailForm: FormGroup;

   userTypes: DropDownList[];
   certTypes: DropDownList[];
   mailTypes: DropDownList[];
   currentElement: number  = 0;
   elementShowedList: number[] = [5, 10, 15, 20];
   dataLists: SendMailLogTable[] = [];
   isbeingSearch: boolean = false;
   zeroResultMessage: string = "ไม่พบข้อมูล"
   placeholderMessage: string = "ทุกท่าน";
   formBuildererBackup: any;
   previousDataForm: JSON;
   realElementShowedList: NumberListElements[];
   getCurrentUserInfo: SsoUserModel;

  constructor( private sendMailLogService: SentMailLogService,
     formBuilderer: FormBuilder,
     private authenService: AuthenticationService){
      this.sendMailForm = formBuilderer.group({
        name:     null,
        mailType: null,
        userType: null,
        certType: null,
      });
    }

   getMailLog(): void{

    this.sendMailLogService.getMailType().subscribe(
      (responseData: ResponseT<DropDownList[]>) => {
        this.mailTypes = responseData.data;
      }
    );
  }

   getUserType(): void{
    this.sendMailLogService.getUserType().subscribe(
      (responseData: ResponseT<DropDownList[]>) => {
        this.userTypes = responseData.data;
      }
    );
  }

   getCertType(): void{
    this.sendMailLogService.getCertType().subscribe(
      (responseData: ResponseT<DropDownList[]>) => {
        this.certTypes = responseData.data;
      }
    );
  }

   search(criteria: JSON, page: number, numPerPage: number,pageRequestAssistor: PageRequest): void{


    this.sendMailLogService.search(criteria, page, numPerPage,pageRequestAssistor).subscribe(
      (responseData: ResponseT<SendMailLogInformation>) => {
        this.pageRequestAssistor.page = responseData.data.totalPages["pageNumber"] + 1; //starts 0
        this.pageResponseAssistor.totalElements = responseData.data.totalElements;
        this.dataLists = responseData.data.content;
        this.pageRequestAssistor.pageSize = responseData.data["pageable"].pageSize;
        this.pageResponseAssistor.numberOfElements = responseData.data.numberOfElements;
        this.currentElement =
          (this.pageRequestAssistor.page * this.pageRequestAssistor.pageSize) -
          this.pageRequestAssistor.pageSize +
          this.pageResponseAssistor.numberOfElements;

        if(this.isbeingSearch){
          this.elementShowedListChecking();
        }
        this.isbeingSearch = this.isbeingSearch && false;
      }
    );
  }

   elementShowedListChecking(): void{
    var time: number = Date.now();
    var element: NumberListElements;

    this.realElementShowedList = [];


    for(let number of this.elementShowedList){
      if(number <= this.pageResponseAssistor.totalElements){

        element = new NumberListElements();
        element.timestamp = time;
        element.value = number;

        this.realElementShowedList.push(element);
      }
    }
  }

  sort(fieldName) {
    if (isNaN(this.pageRequestAssistor.page)) {
      this.pageRequestAssistor.page = 1;
    }
    if (this.pageRequestAssistor.sortFieldName === fieldName) {
      this.pageRequestAssistor.sortDirection = (this.pageRequestAssistor.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequestAssistor.sortFieldName = fieldName;
      this.pageRequestAssistor.sortDirection = 'ASC';

    }
    this.search(
      this.getData(),
      this.pageRequestAssistor.page - 1,
      this.pageRequestAssistor.pageSize,this.pageRequestAssistor);
  }
   elementPerpageChaging(event: any): void{
    this.pageRequestAssistor.pageSize = event.target.value;
    this.pageRequestAssistor.page = 1;
    this.collectionData();
  }

   pageChaging(event: number): void{
    this.pageRequestAssistor.page = event["page"];

    this.search(
      this.getData(),
      this.pageRequestAssistor.page - 1,
      this.pageRequestAssistor.pageSize,this.pageRequestAssistor);
  }

   backToBasic(): void{
    this.sendMailForm.reset();
  }

   searchByCriteria(): void{
    this.pageRequestAssistor.page = 1;
    this.previousDataForm = this.sendMailForm.getRawValue()
    this.isbeingSearch = this.isbeingSearch || true;
    this.collectionData();
  }

   collectionData(): void{
    var criterias: JSON = this.getData();
    var pageSize: number;

    if(this.isbeingSearch){
      pageSize = this.elementShowedList[0];
    } else {
      pageSize = this.pageRequestAssistor.pageSize;
    }

    for(let crite in criterias){
      if(criterias[crite] === "null" || criterias[crite] === ""){
        criterias[crite] = null;
      } else if(criterias[crite] !== null && criterias[crite].toString().match(/^[0-9]+$/g)){
        criterias[crite] = eval(criterias[crite]);
      }
    }

    this.sendMailForm.setValue(criterias);

    this.search(
        criterias,
        this.pageRequestAssistor.page - 1,
        pageSize,this.pageRequestAssistor
      );
  }

   getData(): JSON{
    if(this.isbeingSearch) return Object.assign({}, this.sendMailForm.getRawValue());
    else return Object.assign({}, this.previousDataForm);
  }

  public ngOnInit(): void{
    this.getCurrentUserInfo = this.authenService.ssoUserDetailSnapshot;
    this.pageRequestAssistor.sortFieldName = 'DCS.CERTIFICATE.TAX_PAYER';
    this.pageRequestAssistor.sortDirection = 'ASC';
    let firstCriteria: JSON = this.sendMailForm.getRawValue();
    this.getMailLog();
    this.getUserType();
    this.getCertType();
    this.isbeingSearch = this.isbeingSearch || true;
    this.previousDataForm = this.sendMailForm.getRawValue();
    this.pageRequestAssistor.pageSize = this.elementShowedList[0];
    this.search(
        firstCriteria,
        this.pageRequestAssistor.page - 1,
        this.pageRequestAssistor.pageSize,this.pageRequestAssistor
      );
  }

}
