import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendMailLogComponent } from './send-mail-log.component';

describe('SendMailLogComponent', () => {
  let component: SendMailLogComponent;
  let fixture: ComponentFixture<SendMailLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendMailLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMailLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
