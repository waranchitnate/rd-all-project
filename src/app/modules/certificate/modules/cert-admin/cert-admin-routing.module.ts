import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CertificateHomeComponent} from './pages/certificate-home/certificate-home.component';
import {CertURL} from '../../certificate.url';
import {CertificateApproveComponent} from './pages/certificate-approve/certificate-approve.component';
import {CertificatePendingComponent} from './pages/certificate-pending/certificate-pending.component';
import {CertificatePendingFormComponent} from './pages/certificate-pending-form/certificate-pending-form.component';
import {SendMailLogComponent} from './pages/send-mail-log/send-mail-log.component';
import {CertificateAwaitingInstallationComponent} from './pages/certificate-awaiting-installation/certificate-awaiting-installation.component';
import { ServerCertificateComponent } from './pages/server-certificate/server-certificate/server-certificate.component';
import { HsmComponent } from './pages/hsm/hsm/hsm.component';
import {CertVerifyUsbComponent} from '../cert-verify-usb/cert-verify-usb.component';
import {HsmConfigComponent} from './pages/hsm-config/hsm-config.component';
import {HsmConfigFormComponent} from './pages/hsm-config-form/hsm-config-form.component';
import {ImportCaComponent} from './pages/import-ca/import-ca.component';
import {ImportCaDetailComponent} from './pages/import-ca-detail/import-ca-detail.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: CertURL.admin.home},
  {path: CertURL.admin.home, component: CertificateHomeComponent},
  {path: CertURL.admin.approve, component: CertificateApproveComponent},
  {path: CertURL.admin.pendingList, component: CertificatePendingComponent},
  {path: CertURL.admin.pendingListForm, component: CertificatePendingFormComponent},
  {path: CertURL.admin.mailLog, component: SendMailLogComponent},
  {path: CertURL.admin.awaitingInstallation, component: CertificateAwaitingInstallationComponent},
  {path: CertURL.admin.serverCertificate, component: ServerCertificateComponent},
  {path: CertURL.admin.hsm, component: HsmComponent},
  {path: CertURL.admin.verifyUsb, component: CertVerifyUsbComponent},
  {path: CertURL.admin.hsmConfig, component: HsmConfigComponent},
  {path: CertURL.admin.hsmConfigForm, component: HsmConfigFormComponent},
  {path: CertURL.admin.hsmConfigView + '/:id', component: HsmConfigFormComponent},
  {path: CertURL.admin.importCA, component: ImportCaComponent},
  {path: CertURL.admin.importCADetail, component: ImportCaDetailComponent},
  {path: CertURL.admin.importCADetail + '/:id', component: ImportCaDetailComponent},
  ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertAdminRoutingModule {
}
