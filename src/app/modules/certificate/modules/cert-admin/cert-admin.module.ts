import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CertAdminRoutingModule} from './cert-admin-routing.module';
import {CertificateHomeComponent} from './pages/certificate-home/certificate-home.component';
import {CertificateApproveComponent} from './pages/certificate-approve/certificate-approve.component';
import {CertificatePendingComponent} from './pages/certificate-pending/certificate-pending.component';
import {CertCommonModule} from '../../shared/cert-common.module';
import {SharedModule} from 'src/app/shared/shared.module';
import {CertificatePendingFormComponent} from './pages/certificate-pending-form/certificate-pending-form.component';
import {SendMailLogComponent} from './pages/send-mail-log/send-mail-log.component';
import {CertificateAwaitingInstallationComponent} from './pages/certificate-awaiting-installation/certificate-awaiting-installation.component';
import { ServerCertificateComponent } from './pages/server-certificate/server-certificate/server-certificate.component';
import { HsmComponent } from './pages/hsm/hsm/hsm.component';
import { HsmConfigComponent } from './pages/hsm-config/hsm-config.component';
import { HsmConfigFormComponent } from './pages/hsm-config-form/hsm-config-form.component';
import {CertificateModule} from '../../certificate.module';
import {ImportCaComponent} from './pages/import-ca/import-ca.component';
import {ImportCaDetailComponent} from './pages/import-ca-detail/import-ca-detail.component';

@NgModule({
  declarations: [
      CertificateHomeComponent
    , CertificateApproveComponent
    , CertificatePendingComponent
    , CertificatePendingFormComponent
    , SendMailLogComponent
    , CertificateAwaitingInstallationComponent
    , ServerCertificateComponent
    , HsmComponent
    , HsmConfigComponent
    , HsmConfigFormComponent
    , ImportCaComponent
    , ImportCaDetailComponent
  ],
  imports: [
    CommonModule,
    CertAdminRoutingModule,
    CertCommonModule,
    SharedModule,
    CertificateModule
  ]
})
export class CertAdminModule {
}
