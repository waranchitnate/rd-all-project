import {Component, OnInit, TemplateRef} from '@angular/core';
import {ModalService} from '../../../../core/services/modal.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-cert-verify-usb',
  templateUrl: './cert-verify-usb.component.html',
  styleUrls: ['./cert-verify-usb.component.css']
})
export class CertVerifyUsbComponent implements OnInit {

  modalRef: BsModalRef;

  constructor(private modalService: BsModalService) {
  }

  ngOnInit() {
  }

  showPopup(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  onVerify(isValid: boolean) {
    alert(isValid);
  }

}
