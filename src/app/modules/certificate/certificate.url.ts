export const CertURL = {
  admin: {
    root: 'admin',
    pendingList: 'pending-list',
    pendingListForm: 'pending-list/form',
    approve: 'approve',
    home: 'home',
    mailLog: 'mail-log/search',
    awaitingInstallation: 'awaiting-installation-list/search',
    serverCertificate: 'server-certificate/search',
    hsm: 'hsm/search',
    verifyUsb: 'verifyUsb',
    hsmConfig: 'HSMConfig',
    hsmConfigForm: 'HSMConfig/form',
    hsmConfigView: 'HSMConfig/view',
    importCA: 'import-CA',
    importCADetail: 'import-CA/detail'
  },
  user: {
    root: 'user',
    request: 'request',
    search: 'search',
    step1: 'step1',
    step2: 'step2',
    step3: 'step3',
    step4: 'step4',
    success: 'success'
  },
  master: {
    root: 'master',
    home: 'home',
    approve: 'mApprovalStatus/search',
    deviceType: 'mDeviceType/search',
    deviceStatus: 'mDeviceStatus/search',
    mcerRequestType: 'mCertRequestType/search',
    mcertReasonCode: 'mCertReasonCode/search',
    mattachmentType: 'mAttachmentType/search',
    musertype: 'mUserType/search',
    mobjective: 'mObjective/search'
  },
 deviceManagement: {
    root: 'device-management'
 },
  thumbDrive: {
    root: 'thumb-drive'
  },
  changePin : 'change-pin'
};
