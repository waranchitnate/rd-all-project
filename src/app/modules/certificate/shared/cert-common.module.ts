import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CertNavbarComponent} from './cert-navbar/cert-navbar.component';
import {RouterModule} from '@angular/router';
import {CertHeaderComponent} from './cert-header/cert-header.component';
import {CertRequestHeaderComponent} from './cert-request-header/cert-request-header.component';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  declarations: [CertNavbarComponent, CertHeaderComponent, CertRequestHeaderComponent],
  imports: [
    CommonModule, RouterModule, SharedModule
  ], exports: [CertNavbarComponent, CertHeaderComponent, CertRequestHeaderComponent]
})
export class CertCommonModule {
}
