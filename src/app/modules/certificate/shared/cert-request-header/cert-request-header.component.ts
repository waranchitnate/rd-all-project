import {Component, Input, OnInit} from '@angular/core';
import {CertificateStepService} from '../../services/certificate-step.service';
import {Step} from '../../../../shared/models/step.model';

@Component({
  selector: 'app-cert-request-header',
  templateUrl: './cert-request-header.component.html',
  styleUrls: ['./cert-request-header.component.css']
})
export class CertRequestHeaderComponent implements OnInit {

  listStep: Step[];

  activePosition = 0;

  constructor(private certStepService: CertificateStepService) {
  }

  ngOnInit() {
    this.activePosition = this.certStepService.currentStep;
    this.listStep = this.certStepService.listStep;
    this.certStepService.currentStepSubject.subscribe(currentStep => {
      this.activePosition = currentStep;
    });

  }

}
