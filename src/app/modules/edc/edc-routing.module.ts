import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReportValidateComponent} from './etl/components/report-validate/report-validate.component';
import {ReportSelectDataComponent} from './etl/components/report-select-data/report-select-data.component';
import {ReportImproveComponent} from './etl/components/report-improve/report-improve.component';
import {ReportNotFoundComponent} from './etl/components/report-not-found/report-not-found.component';

const routes: Routes = [
  // { path: 'home', component: SenderTransferPushConfig, canActivate: [AuthGuard] },login
  // {path: '', redirectTo: 'mft/data-type', pathMatch: 'full'},
  {path: 'etl/report-validate', component: ReportValidateComponent},
  {path: 'etl/report-select-data', component: ReportSelectDataComponent},
  {path: 'etl/report-improve', component: ReportImproveComponent},
  {path: 'etl/report-not-found', component: ReportNotFoundComponent},
  {
    path: 'etl/report-data-error',
    loadChildren: './etl/components/report-data-error/report-data-error.module#ReportDataErrorModule'
  },
  {
    path: 'etl/financial-institution',
    loadChildren: './etl/components/financial-institution/financial-institution.module#FinancialInstitutionModule'
  },
  {
    path: 'etl/vat-rate',
    loadChildren: './etl/components/vat-rate/vat-rate.module#VatRateModule'
  },
  {
    path: 'etl/vat-type',
    loadChildren: './etl/components/vat-type/vat-type.module#VatTypeModule'
  },
  {
    path: 'etl/currency',
    loadChildren: './etl/components/currency/currency.module#CurrencyModule'
  },
  {
    path: 'etl/country',
    loadChildren: './etl/components/country/country.module#CountryModule'
  },
  {
    path: 'etl/discrepancy',
    loadChildren: './etl/components/discrepancy/discrepancy.module#DiscrepancyModule'
  },
  {
    path: 'etl/crossborder-dta',
    loadChildren: './etl/components/crossborder-dta/crossborder-dta.module#CrossborderDtaModule'
  },
  {
    path: 'etl/cross-border-income-type',
    loadChildren: './etl/components/income-cross/income-cross.module#IncomeCrossModule'
  },
  {
    path: 'etl/domestic-income-type',
    loadChildren: './etl/components/income-dom/income-dom.module#IncomeDomModule'
  },
  {
    path: 'etl/title-code',
    loadChildren: './etl/components/title-code/title-code.module#TitleCodeModule'
  },
  {
    path: 'etl/province',
    loadChildren: './etl/components/province/province.module#ProvinceModule'
  },
  {
    path: 'etl/amphur',
    loadChildren: './etl/components/amphur/amphur.module#AmphurModule'
  },
  {
    path: 'etl/thambol',
    loadChildren: './etl/components/thambol/thambol.module#ThambolModule'
  },
  {
    path: 'etl/postcode',
    loadChildren: './etl/components/postcode/postcode.module#PostcodeModule'
  },
  {
    path: 'etl/section',
    loadChildren: './etl/components/section/section.module#SectionModule'
  },
  {
    path: 'mft/data-type',
    // path: '',
    loadChildren: './mft/components/data-type/data-type.module#DataTypeModule'
  },
  {
    path: 'mft/schedule', loadChildren: './mft/components/schedule/schedule.module#ScheduleModule'
  },
  {
    path: 'mft/sender-transfer-push',
    loadChildren: './mft/components/sender-transfer-push/sender-transfer-push.module#SenderTransferPushModule'
  },
  {
    path: 'mft/sender-transfer-pull',
    loadChildren: './mft/components/sender-transfer-pull/sender-transfer-pull.module#SenderTransferPullModule'
  },
  {
    path: 'mft/notification-template',
    loadChildren: './mft/components/notification-template/notification-template.module#NotiTemplateModule'
  },
  {
    path: 'mft/report',
    loadChildren: './mft/components/admin-report/admin-report.module#AdminReportModule'
  },
  {
    path: 'mft/config-code',
    loadChildren: './mft/components/config-code/config-code.module#ConfigCodeModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EdcRoutingModule {
}
