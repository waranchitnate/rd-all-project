import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {NidGovAssoService} from '../edcNidGovAsso.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcNidGovAssoModel} from '../../../models/edcNidGovAsso.model';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {ConfigCodeService} from './../../../edcConfigCode.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcNidGovAssoList = <EdcNidGovAssoModel[]>[];
    ConfigCodeList = <EdcConfigCodeModel[]>[];
    taxPayerTypeList = <EdcConfigCodeModel[]>[];
    taxPayerTypeC = <EdcConfigCodeModel[]>[];
    tableTaxPTypeMap = new Map();
    items: FormArray;
    nidGovAssoForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private nidGovAssoService: NidGovAssoService,
        private modal: ModalService,
        private router: Router,
        private configCodeService: ConfigCodeService
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
        this.getTaxPayerTypeList();
        this.getRateType();
    }

  getTaxPayerTypeList() {
    this.configCodeService.listConfig({}).subscribe(result => {
      this.ConfigCodeList = result.data;
      this.taxPayerTypeList = this.ConfigCodeList.filter(res => res.type === 'TAXPAYER_TYPE');
      for (let i = 0; i <= this.taxPayerTypeList.length - 1; i++) {
        // console.log(this.PndList[i]);
        this.tableTaxPTypeMap.set(this.taxPayerTypeList[i].code, this.taxPayerTypeList[i].description);


      }
      // console.log(this.tablePndMap);
    });
  }
  getRateType() {
    this.configCodeService.listConfig({}).subscribe(result => {
      this.ConfigCodeList = result.data;
      this.taxPayerTypeC = this.ConfigCodeList.filter(res => res.type === 'TAXPAYER_TYPE');
      console.log(this.taxPayerTypeC);
    });
  }

    onSubmit() {
         console.log(this.nidGovAssoForm.valid);
        this.getList(this.nidGovAssoForm.value.nidGovAssoGroup);
    }

    initialForm() {
        this.nidGovAssoForm = new FormGroup({
             nidGovAssoGroup: new FormGroup({
                'taxId': new FormControl(null),
                'taxPayerName': new FormControl(null),
                'taxPayerType': new FormControl(null)
            })
        });
    }

    getList(param) {
        this.nidGovAssoService.list(param).subscribe(result => {
             // console.log(result.data);
            this.edcNidGovAssoList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }


    resetForm() {
        this.nidGovAssoForm.reset();
        this.initialForm();
        this.getList(this.nidGovAssoForm.value.nidGovAssoGroup);
    }

    getPage(page: number) {
        this.nidGovAssoForm.value.nidGovAssoGroup.page = page;
        // console.log(this.postcodeForm.value.postcodeGroup);
        this.getList(this.nidGovAssoForm.value.nidGovAssoGroup);
    }

    getPageSize(pageSize: number) {
        this.nidGovAssoForm.value.nidGovAssoGroup.pageSize = pageSize;
        this.getList(this.nidGovAssoForm.value.nidGovAssoGroup);
    }

    onView(obj) {
        console.log(obj);
        this.router.navigateByUrl('/etl/nid-gov-asso/view', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลประเภทผู้เสียภาษีอาการใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.nidGovAssoService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.nidGovAssoForm.value.nidGovAssoGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลประเภทผู้เสียภาษีอาการข้อมูลสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('เกิดข้อผิดพลาด', 'ไม่สามารถทำการลบได้กรุณาทำรายการใหม่ภายหลัง');
                    }
                );
            }
            sub.unsubscribe();
        });
    }
}
