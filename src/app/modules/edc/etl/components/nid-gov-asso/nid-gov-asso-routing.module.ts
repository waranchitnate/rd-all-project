import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListComponent} from '../nid-gov-asso/list/list.component';
import {ViewComponent} from '../nid-gov-asso/view/view.component';
import {ImportComponent} from '../nid-gov-asso/import/import.component';

const routes: Routes = [
    {path: '', component: ListComponent},
    {path: 'view', component: ViewComponent},
  { path: 'import', component: ImportComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NidGovAssoRoutingModule {
}
