import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {NidGovAssoService} from '../edcNidGovAsso.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';
import {EdcNidGovAssoModel} from '../../../models/edcNidGovAsso.model';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {ConfigCodeService} from './../../../edcConfigCode.service';

@Component({
    selector: 'app-view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

    state: Observable<EdcNidGovAssoModel>;
    private edcNidGovAssoModel: EdcNidGovAssoModel;
    ConfigCodeList = <EdcConfigCodeModel[]>[];
    edcNidGovAssoList = <EdcNidGovAssoModel[]>[];
    TaxPayerTypeList = <EdcConfigCodeModel[]>[];
    tableTaxPTypeMap = new Map();
    checkDataTypeCode = false;

    items: FormArray;
    nidGovAssoForm: FormGroup;

    constructor(
        public activatedRoute: ActivatedRoute,
        public router: Router,
        private nidGovAssoService: NidGovAssoService,
        private modal: ModalService,
        private alertText: AlertTextModule,
        private configCodeService: ConfigCodeService
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcNidGovAssoModel = e;

        });
        if (this.edcNidGovAssoModel.id === undefined) {
            this.router.navigateByUrl('/etl/nidGovAsso');
        }
        this.initialForm();
        this.getTaxPayerTypeList();
    }

    onSubmit() {
        console.log(this.nidGovAssoForm.value);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลรหัสไปรษณีย์ใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.nidGovAssoForm.value.nidGovAssoGroup.id = this.edcNidGovAssoModel.id;
                this.nidGovAssoService.update(this.nidGovAssoForm.value.nidGovAssoGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลประเภทข้อมูลสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/nid-gov-asso');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสไปรษณีย์' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        // const modalRef = this.modal.modalService.show(AnswerModalComponent);
        // (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        // (modalRef.content as AnswerModalComponent).content = 'ข้อมูลรหัสไปรษณีย์ยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
        //     if (a) {
                this.router.navigateByUrl('/etl/nid-gov-asso');
        //         sub.unsubscribe();
        //     }
        // });
    }

    initialForm() {
        this.nidGovAssoForm = new FormGroup({
          nidGovAssoGroup: new FormGroup({
                'taxId': new FormControl(this.edcNidGovAssoModel.taxId, Validators.required),
                'taxPayerType': new FormControl(this.edcNidGovAssoModel.taxPayerType),
                'taxPayerName': new FormControl(this.edcNidGovAssoModel.taxPayerName),
                'whtRateType': new FormControl(this.edcNidGovAssoModel.whtRateType)
            })
        });

    }

    resetForm() {
        this.nidGovAssoForm.reset();
    }
  getTaxPayerTypeList() {
    this.configCodeService.listConfig({}).subscribe(result => {
      this.ConfigCodeList = result.data;
      this.TaxPayerTypeList = this.ConfigCodeList.filter(res => res.type === 'TAXPAYER_TYPE');
      console.log(this.TaxPayerTypeList);
    });
  }

}
