import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcNidGovAssoModel} from '../../models/edcNidGovAsso.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class NidGovAssoService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcNidGovAssoModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcNidGovAssoModel[]>>>this.http.post(this.url + 'nidGovAsso', request);
    }

    save(request: EdcNidGovAssoModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'nidGovAsso/add', request);
    }

    update(request: EdcNidGovAssoModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'nidGovAsso/update', request);
    }

    delete(request: EdcNidGovAssoModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'nidGovAsso/delete', request);
    }

  import(file: File): Observable<any> {
    const data = new FormData();
    data.append('file', file);
    console.log(data);
    return this.http.post(this.url + 'nidGovAsso/import', data);
  }
}
