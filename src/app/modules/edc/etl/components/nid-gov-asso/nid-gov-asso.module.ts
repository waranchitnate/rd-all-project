import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NidGovAssoRoutingModule} from './nid-gov-asso-routing.module';
import {ViewComponent} from './view/view.component';
import {ListComponent} from './list/list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {TypeaheadModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../../../shared/shared.module';
import {ImportComponent} from '../nid-gov-asso/import/import.component';

@NgModule({
    declarations: [ ViewComponent, ListComponent, ImportComponent],
  imports: [
    CommonModule,
    NidGovAssoRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    BsDatepickerModule,
    TypeaheadModule,
    SharedModule
  ]
})
export class NidGovAssoModule {
}
