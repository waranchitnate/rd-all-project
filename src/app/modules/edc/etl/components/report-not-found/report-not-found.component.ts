import {Component, OnInit} from '@angular/core';
import {ConfigCodeService} from '../../../mft/edcConfigCode.service';
import {SenderTransferPushService} from '../../../mft/components/sender-transfer-push/sender-transfer-push.service';
import {ReportService} from '../../edcReport.service';
import {FormControl, FormGroup} from '@angular/forms';
import {EdcTaxIdNotFoundModel} from '../../models/edcTaxIdNotFound.model';
import {UserInfoModel} from '../../../mft/models/userInfo.model';
import {EdcfinancialInstitutionModel} from '../../models/edcFinancial-insitution.model';
import {TypeaheadMatch} from 'ngx-bootstrap';
import * as moment from 'moment';
import {FinancialInstitutionService} from '../financial-institution/edcfinancial-institution.service';

@Component({
  selector: 'app-report-notregisted',
  templateUrl: './report-not-found.component.html',
  styleUrls: ['./report-not-found.component.css']
})
export class ReportNotFoundComponent implements OnInit {

  edcTaxIdNotFoundList = <EdcTaxIdNotFoundModel[]>[];
  financialList = <EdcfinancialInstitutionModel[]>[];
  reportIdNotFoundForm: FormGroup;
  financialMap = new Map();
  selectedOption: any;
  minDate: Date;
  total: number;
  pageSize: number;
  page: number;
  userInfoList = <UserInfoModel[]>[];
  trxFlagList = {1: 'ไม่พบในระบบ NID', 2: 'ไม่พบในระบบ VAT', 4: 'ไม่พบในระบบตรวจสอบและแนะนำ'};
   taxFlagList = {S: 'ผู้นำจ่าย', R: 'ผู้รับ'};

  constructor(
    private configCodeService: ConfigCodeService,
    private senderTransferPushService: SenderTransferPushService,
    private reportService: ReportService,
    private financialInstitutionService: FinancialInstitutionService,
  ) {
  }

  ngOnInit() {
    this.initialForm();
    this.getReport({});
    this.getListSenderUser();
    this.getListFinancial();
  }

  onSubmit() {
    if (this.reportIdNotFoundForm.get('reportIdNotFoundC').value.dateRange !== null) {
      this.reportIdNotFoundForm.value.reportIdNotFoundC.dateStart = moment(this.reportIdNotFoundForm.get('reportIdNotFoundC').value.dateRange[0]).format('YYYY-MM-DD');
      this.reportIdNotFoundForm.value.reportIdNotFoundC.dateEnd = moment(this.reportIdNotFoundForm.get('reportIdNotFoundC').value.dateRange[1]).format('YYYY-MM-DD');
    }
    this.getReport(this.reportIdNotFoundForm.value.reportIdNotFoundC);
  }

  resetForm() {
    this.reportIdNotFoundForm.reset();
    this.getReport({});
  }

  initialForm() {
    this.reportIdNotFoundForm = new FormGroup({
      reportIdNotFoundC: new FormGroup({
        'businessAuthorityName': new FormControl(null),
        'senderUserId': new FormControl(null),
        'dateRange': new FormControl(null),
        'trxFlag': new FormControl(null),
        'fiCode': new FormControl(null),
        'fiNameTh': new FormControl(null)
      })
    });
  }

  getListSenderUser() {
    this.senderTransferPushService.listUser().subscribe(result => {
      this.userInfoList = result.data;
    });
  }

  getReport(param) {
    if (param.trxFlag === 'null' ) {
      param.trxFlag = null;
    }
    this.reportService.listTaxIdFound(param).subscribe(res => {
      this.edcTaxIdNotFoundList = res.data;
      this.page = res.page;
      this.total = res.total;
      this.pageSize = res.pageSize;
    });
  }

  getPage(page: number) {
    this.reportIdNotFoundForm.value.reportIdNotFoundC.page = page;
    this.getReport(this.reportIdNotFoundForm.value.reportIdNotFoundC);
  }

  getPageSize(pageSize: number) {
    this.reportIdNotFoundForm.value.reportIdNotFoundC.pageSize = pageSize;
    this.getReport(this.reportIdNotFoundForm.value.reportIdNotFoundC);
  }

  onSelectFi(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.reportIdNotFoundForm.get('reportIdNotFoundC.fiCode').setValue(this.selectedOption.fiCode);
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }
}
