import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {ModalService} from '../../../../../../core/services/modal.service';
import {CurrencyService} from '../../currency/edcCurrency.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {

  currencyForm: FormGroup;
  currentFileUpload: File;
  minDate: Date;

  constructor(
    private modal: ModalService,
    private currencyService: CurrencyService,
    public router: Router,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    this.initialForm();
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);
  }

  initialForm() {
    this.currencyForm = new FormGroup({
      currencyGroup: new FormGroup({
        'fileCurrency': new FormControl(null)
      })
    });
  }

  resetForm() {
    this.currentFileUpload = null;
    this.currencyForm.reset();
  }

  selectFile(event) {
    this.currentFileUpload = <File>event.target.files[0];
    console.log(this.currentFileUpload);
  }

  onSubmit() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลสกุลเงินใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.currencyService.import(this.currentFileUpload, this.currencyForm.value.currencyGroup).subscribe(
          response => {
            console.log(response);
            this.resetForm();
            this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลสกุลเงินสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('/etl/currency');
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสสกุลเงิน' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    this.router.navigateByUrl('/etl/currency');
    // const modalRef = this.modal.modalService.show(AnswerModalComponent);
    //
    // (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    // (modalRef.content as AnswerModalComponent).content = 'ข้อมูลสกุลเงินยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
    // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
    //     if (a) {
    //         this.router.navigateByUrl('/etl/currency');
    //         sub.unsubscribe();
    //     }
    // });
  }
}
