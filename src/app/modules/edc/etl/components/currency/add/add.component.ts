import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {ModalService} from '../../../../../../core/services/modal.service';
import {CurrencyService} from '../../currency/edcCurrency.service';
import {EdcCountryModel} from '../../../models/edcCountry.model';
import {CountryService} from '../../country/edcCountry.service';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Router} from '@angular/router';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';
import {validate} from 'codelyzer/walkerFactory/walkerFn';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    currencyForm: FormGroup;
    minDate: Date;
    edcCountryList = <EdcCountryModel[]>[];
    private selectedOption: EdcCountryModel;

    constructor(
        private modal: ModalService,
        private currencyService: CurrencyService,
        private countryService: CountryService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.getCountryList();
        this.initialForm();
        this.minDate = new Date();
        this.minDate.setDate(this.minDate.getDate() + 1);
    }

    initialForm() {
        this.currencyForm = new FormGroup({
            currencyGroup: new FormGroup({
                'currencyCode': new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z]+$')]),
                'currencyNameTh': new FormControl(null, [Validators.required]),
                'currencyNameEn': new FormControl(null,  Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/? ]+$')),
                'unit': new FormControl(null, Validators.required),
                'countryId': new FormControl(null),
                'countryName': new FormControl(null),
                'remark': new FormControl(null)
            })
        });
    }

    getCountryList() {
        this.countryService.listSelect().subscribe(res => {
            this.edcCountryList = res.data;
        });
    }

    onSelect(event: TypeaheadMatch): void {
        this.selectedOption = event.item;
        this.currencyForm.get('currencyGroup.countryId').setValue(this.selectedOption.id);
    }

    resetForm() {
        this.currencyForm.reset();
    }

    onSubmit() {
        console.log(this.currencyForm.value);


        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลสกุลเงินใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.currencyForm.valid) {
                this.currencyService.save(this.currencyForm.value.currencyGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลสกุลเงินสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/currency');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสสกุลเงิน' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลสกุลเงินยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/currency');
                sub.unsubscribe();
            }
        });
    }
}
