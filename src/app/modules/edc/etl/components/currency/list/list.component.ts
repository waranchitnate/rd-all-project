import {CurrencyService} from '../edcCurrency.service';
import {EdcCurrencyModel} from '../../../models/edcCurrency.model';

import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcCurrencyList = <EdcCurrencyModel[]>[];
    currencyForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private currencyService: CurrencyService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.currencyForm.value);
        this.getList(this.currencyForm.value.currencyGroup);
    }

    downloadFile() {
        const link = document.createElement('a');
        link.download = 'temp_currency';
        link.href = 'assets/filestemplate/temp_currency.xlsx';
        link.click();
    }

    initialForm() {
        this.currencyForm = new FormGroup({
            currencyGroup: new FormGroup({
                'currencyCode': new FormControl(null),
                'currencySearchName': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.currencyForm.reset();
        this.initialForm();
        this.getList(this.currencyForm.value.currencyGroup);
    }

    getList(param) {
        this.currencyService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcCurrencyList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.currencyForm.value.currencyGroup.page = page;
        // console.log(this.currencyForm.value.currencyGroup);
        this.getList(this.currencyForm.value.currencyGroup);
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/currency/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลสกุลเงินใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.currencyService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.currencyForm.value.currencyGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลสกุลเงินสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.currencyForm.value.currencyGroup.pageSize = pageSize;
        this.getList(this.currencyForm.value.currencyGroup);
    }
}
