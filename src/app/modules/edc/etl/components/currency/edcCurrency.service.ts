import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {EdcCurrencyModel} from '../../models/edcCurrency.model';
import {ResponseEtl} from '../../models/responseEtl.model';
import {environment} from '../../../../../../environments/environment';


@Injectable()
export class CurrencyService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcCurrencyModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcCurrencyModel[]>>>this.http.post(this.url + 'currencies', request);
    }

    save(request: EdcCurrencyModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'currency/add', request);
    }

    import(file: File, request: EdcCurrencyModel): Observable<any> {
        console.log(JSON.stringify(request));
        const data = new FormData();
        data.append('file', file);
        data.append('JsonData', JSON.stringify(request));
        console.log(data);
        return this.http.post(this.url + 'currency/import', data);
    }

    update(request: EdcCurrencyModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'currency/update', request);
    }

    delete(request: EdcCurrencyModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'currency/delete', request);
    }
}
