import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {EdcCurrencyModel} from '../../../models/edcCurrency.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {CurrencyService} from '../edcCurrency.service';
import {map} from 'rxjs/operators';
import {EdcCountryModel} from '../../../models/edcCountry.model';
import {CountryService} from '../../country/edcCountry.service';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  state: Observable<EdcCurrencyModel>;
  edcCurrency: EdcCurrencyModel;
  currencyForm: FormGroup;
  edcCountryList = <EdcCountryModel[]>[];
  selectedOption: EdcCountryModel;
  countryNameThfield: string;
  countryIdField: number;
  minDate: Date;

  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private modal: ModalService,
    private currencyService: CurrencyService,
    private countryService: CountryService,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {

    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe(e => {
      this.edcCurrency = e;
    });
    if (this.edcCurrency.id === undefined) {
      this.router.navigateByUrl('/etl/currency');
    }
    this.getCountryList();
    this.initialForm();
    console.log(this.edcCurrency.edcCountryEntity);
    console.log(this.edcCurrency);
  }

  initialForm() {
    if (this.edcCurrency.edcCountryEntity === null) {
      this.countryNameThfield = null;
      this.countryIdField = null;
    }
    if (this.edcCurrency.edcCountryEntity != null) {
      this.countryIdField = this.edcCurrency.edcCountryEntity.id;
      this.countryNameThfield = this.edcCurrency.edcCountryEntity.countryNameTh;
    }

    this.currencyForm = new FormGroup({
      currencyGroup: new FormGroup({
        'currencyCode': new FormControl(this.edcCurrency.currencyCode, [Validators.required, Validators.pattern('^[A-Za-z]+$')]),
        'currencyNameTh': new FormControl(this.edcCurrency.currencyNameTh, [Validators.required]),
        'currencyNameEn': new FormControl(this.edcCurrency.currencyNameEn,  Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/? ]+$')),
        'unit': new FormControl(this.edcCurrency.unit, Validators.required),
        'countryId': new FormControl(this.countryIdField),
        'countryName': new FormControl(this.countryNameThfield),
        'remark': new FormControl(this.edcCurrency.remark),
        'createdDate': new FormControl(this.edcCurrency.createdDate),
        'createdBy': new FormControl(this.edcCurrency.createdBy)
      })
    });
  }

  getCountryList() {
    this.countryService.listSelect().subscribe(res => {
      this.edcCountryList = res.data;
    });
  }

  onSelect(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.currencyForm.get('currencyGroup.countryId').setValue(this.selectedOption.id);
  }

  resetForm() {
    this.currencyForm.reset();
  }

  onSubmit() {
    console.log(this.currencyForm.value);

    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของสกุลเงินใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.currencyForm.valid) {
        this.currencyForm.value.currencyGroup.id = this.edcCurrency.id;
        this.currencyService.update(this.currencyForm.value.currencyGroup).subscribe(
          response => {
            console.log(response);
            this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลสกุลเงินสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('/etl/currency');
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสสกุลเงิน' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);

    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลสกุลเงินยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.router.navigateByUrl('/etl/currency');
        sub.unsubscribe();
      }
    });
  }
}
