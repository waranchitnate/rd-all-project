import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from '../currency/list/list.component';
import {AddComponent} from '../currency/add/add.component';
import {EditComponent} from '../currency/edit/edit.component';
import {ImportComponent} from '../currency/import/import.component';

const routes: Routes = [
  { path: '', component: ListComponent},
  { path: 'add', component: AddComponent},
  { path: 'edit', component: EditComponent},
  { path: 'import', component: ImportComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrencyRoutingModule { }
