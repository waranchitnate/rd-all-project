import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProvinceService} from '../edcProvince.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-edit',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

    state: Observable<EdcProvinceModel>;
    private edcProvince: EdcProvinceModel;
    provinceForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        public activatedRoute: ActivatedRoute,
        private provinceService: ProvinceService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcProvince = e;
        });
        if (this.edcProvince.id === undefined) {
            this.router.navigateByUrl('/etl/province');
        }
        this.initialForm();
    }

    initialForm() {
        this.provinceForm = new FormGroup({
            provinceGroup: new FormGroup({
                'provinceCode': new FormControl(this.edcProvince.provinceCode, [Validators.required, Validators.pattern('^[0-9]+$')]),
                'provinceNameTh': new FormControl(this.edcProvince.provinceNameTh, Validators.required),
                'provinceNameEn': new FormControl(this.edcProvince.provinceNameEn, Validators.pattern('^[a-zA-Z0-9 ]+$')),
                'comment': new FormControl(this.edcProvince.comment)
            })
        });
    }

    resetForm() {
        this.provinceForm.reset();
        this.initialForm();
    }

    // onSubmit() {
    //     console.log(this.provinceForm.value);
    //     const modalRef = this.modal.modalService.show(AnswerModalComponent);
    //     (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    //     (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของจังหวัดใช่หรือไม่';
    //
    //     const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
    //         if (a && this.provinceForm.valid) {
    //             this.provinceForm.value.provinceGroup.id = this.edcProvince.id;
    //             this.provinceService.update(this.provinceForm.value.provinceGroup).subscribe(
    //                 response => {
    //                     console.log(response);
    //                     this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลจังหวัดสำเร็จ');
    //                     const modalHide = this.modal.modalService.onHide.subscribe(e => {
    //                         modalHide.unsubscribe();
    //                     });
    //                     this.router.navigateByUrl('/etl/province');
    //                 },
    //                 err => {
    //                     if (err.error.errorCode === 'ST098') {
    //                         this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสจังหวัด' + this.alertText.getTail);
    //                     } else {
    //                         this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
    //                     }
    //                 }
    //             );
    //         }
    //         sub.unsubscribe();
    //     });
    // }

    onBack() {
        // const modalRef = this.modal.modalService.show(AnswerModalComponent);
        //
        // (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        // (modalRef.content as AnswerModalComponent).content = 'ข้อมูลจังหวัดข้อมูลยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
        //     if (a) {
                this.router.navigateByUrl('/etl/province');
        //         sub.unsubscribe();
        //     }
        // });
    }
}
