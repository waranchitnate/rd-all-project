import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcProvinceModel} from '../../models/edcProvince.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class ProvinceService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcProvinceModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcProvinceModel[]>>>this.http.post(this.url + 'provinces', request);
    }

    listSelect(): Observable<ResponseEtl<EdcProvinceModel[]>> {
        return <Observable<ResponseEtl<EdcProvinceModel[]>>>this.http.get(this.url + 'provinces');
    }

    save(request: EdcProvinceModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'province/add', request);
    }

    //
    // update(request: EdcProvinceModel): Observable<any> {
    //     console.log(request);
    //     return this.http.post(this.url + 'province/update', request);
    // }

    delete(request: EdcProvinceModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'province/delete', request);
    }

  import(file: File): Observable<any> {
    const data = new FormData();
    data.append('file', file);
    console.log(data);
    return this.http.post(this.url + 'province/import', data);
  }
}
