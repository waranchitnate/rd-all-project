import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProvinceService} from '../edcProvince.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    provinceForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private provinceService: ProvinceService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.initialForm();
    }

    initialForm() {
        this.provinceForm = new FormGroup({
            provinceGroup: new FormGroup({
                'provinceCode': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]+$')]),
                'provinceNameTh': new FormControl(null, Validators.required),
                'provinceNameEn': new FormControl(null, Validators.pattern('^[a-zA-Z0-9 ]+$')),
                'comment': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.provinceForm.reset();
        this.initialForm();
    }

    onSubmit() {
        console.log(this.provinceForm.value);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลจังหวัดใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.provinceForm.valid) {
                this.provinceService.save(this.provinceForm.value.provinceGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลจังหวัดสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/province');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสจังหวัด' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลจังหวัดยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/province');
                sub.unsubscribe();
            }
        });
    }
}
