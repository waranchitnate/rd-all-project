import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {EdcProvinceModel} from '../../../models/edcProvince.model';

import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {ProvinceService} from '../edcProvince.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcProvinceList = <EdcProvinceModel[]>[];
    provinceForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private provinceService: ProvinceService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.provinceForm.value);
        this.getList(this.provinceForm.value.provinceGroup);
    }

    initialForm() {
        this.provinceForm = new FormGroup({
            provinceGroup: new FormGroup({
                'provinceNameTh': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.provinceForm.reset();
        this.initialForm();
        this.getList(this.provinceForm.value.provinceGroup);
    }

    getList(param) {
        this.provinceService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcProvinceList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.provinceForm.value.provinceGroup.page = page;
        // console.log(this.provinceForm.value.provinceGroup);
        this.getList(this.provinceForm.value.provinceGroup);
    }

    onView(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/province/view', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลจังหวัดใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.provinceService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.provinceForm.value.provinceGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลจังหวัดสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('เกิดข้อผิดพลาด', 'ไม่สามารถทำการลบได้กรุณาทำรายการใหม่ภายหลัง');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.provinceForm.value.provinceGroup.pageSize = pageSize;
        this.getList(this.provinceForm.value.provinceGroup);
    }
}
