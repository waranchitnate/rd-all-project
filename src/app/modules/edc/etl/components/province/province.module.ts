import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProvinceRoutingModule } from './province-routing.module';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { ImportComponent } from './import/import.component';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../../../shared/shared.module';

@NgModule({
  declarations: [AddComponent, ViewComponent, ListComponent, ImportComponent],
  imports: [
    CommonModule,
    ProvinceRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    BsDatepickerModule,
    SharedModule
  ]
})
export class ProvinceModule { }
