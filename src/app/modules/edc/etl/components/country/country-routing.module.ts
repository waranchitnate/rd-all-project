import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from '../country/list/list.component';
import {AddComponent} from '../country/add/add.component';
import {EditComponent} from '../country/edit/edit.component';
import {ImportComponent} from '../country/import/import.component';

const routes: Routes = [
  { path: '', component: ListComponent},
  { path: 'add', component: AddComponent},
  { path: 'edit', component: EditComponent},
  { path: 'import', component: ImportComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryRoutingModule { }
