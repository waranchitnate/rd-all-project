import {CountryService} from '../edcCountry.service';
import {EdcCountryModel} from '../../../models/edcCountry.model';

import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcCountryList = <EdcCountryModel[]>[];
    countryForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private countryService: CountryService,
        private modal: ModalService,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.countryForm.value);
        this.getList(this.countryForm.value.countryGroup);
    }

    initialForm() {
        this.countryForm = new FormGroup({
            countryGroup: new FormGroup({
                'countryCode2': new FormControl(null),
                'countryNameTh': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.countryForm.reset();
        this.initialForm();
        this.getList(this.countryForm.value.countryGroup);
    }

    getList(param) {
        this.countryService.list(param).subscribe(result => {
            this.edcCountryList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.countryForm.value.countryGroup.page = page;
        // console.log(this.countryForm.value.countryGroup);
        this.getList(this.countryForm.value.countryGroup);
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/country/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลประเทศใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.countryService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.countryForm.value.countryGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลประเทศสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    downloadFile() {
        const link = document.createElement('a');
        link.download = 'temp_country';
        link.href = 'assets/filestemplate/temp_country.xlsx';
        link.click();
    }

    getPageSize(pageSize: number) {
        this.countryForm.value.countryGroup.pageSize = pageSize;
        this.getList(this.countryForm.value.countryGroup);
    }
}
