import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EdcCountryModel} from '../../../models/edcCountry.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {CountryService} from '../edcCountry.service';
import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  state: Observable<EdcCountryModel>;
  edcCountry: EdcCountryModel;
  countryForm: FormGroup;
  popUpName = '';
  minDate: Date;

  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private modal: ModalService,
    private countryService: CountryService,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe(e => {
      this.edcCountry = e;
    });
    if (this.edcCountry.id === undefined) {
      this.router.navigateByUrl('/etl/country');
    }
    this.initialForm();
  }

  initialForm() {
    this.countryForm = new FormGroup({
      countryGroup: new FormGroup({
        'countryCode2': new FormControl(this.edcCountry.countryCode2, [Validators.required, Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]+$')]),
        'countryCode3': new FormControl(this.edcCountry.countryCode3, Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]+$')),
        'countryNameTh': new FormControl(this.edcCountry.countryNameTh, [Validators.required]),
        'countryNameEn': new FormControl(this.edcCountry.countryNameEn, [Validators.required, Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\_\\-=\\[\\]{};\':"\\\\|,.<>\\/? ]+$')]),
        'countryCodeNid': new FormControl(this.edcCountry.countryCodeNid, [Validators.required, Validators.pattern('^[A-Za-z0-9]+$')]),
        'remark': new FormControl(this.edcCountry.remark),
        'createdDate': new FormControl(this.edcCountry.createdDate),
        'createdBy': new FormControl(this.edcCountry.createdBy)
      })
    });
  }

  resetForm() {
    this.countryForm.reset();
  }

  onSubmit() {
    this.countryForm.value.countryGroup.id = this.edcCountry.id;
    const modalRef = this.modal.modalService.show(AnswerModalComponent);

    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของประเทศใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        // this.countryForm.value.countryGroup.effectiveDate = new Date(this.countryForm.value.countryGroup.effectiveDate);
        this.countryService.update(this.countryForm.value.countryGroup).subscribe(
          response => {
            console.log(response);
            this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลประเทศสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('/etl/country');
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสประเทศ (2 หลัก) หรือ รหัสประเทศ (3 หลัก)' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
        sub.unsubscribe();
      }
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    if (this.countryForm.value.countryGroup.countryCode2 === null) {
      this.popUpName = '';
    } else {
      this.popUpName = this.countryForm.value.countryGroup.countryCode2;
    }

    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลประเทศยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        console.log(this.countryForm.value.countryGroup);
        this.router.navigateByUrl('/etl/country');
        sub.unsubscribe();
      }
    });
  }
}
