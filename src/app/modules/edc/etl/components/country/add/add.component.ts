import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {CountryService} from '../../country/edcCountry.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';
import {Router} from '@angular/router';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    countryForm: FormGroup;
    minDate: Date;

    constructor(
        private modal: ModalService,
        private countryService: CountryService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.initialForm();
        this.minDate = new Date();
        this.minDate.setDate(this.minDate.getDate() + 1);
    }

    initialForm() {
        this.countryForm = new FormGroup({
            countryGroup: new FormGroup({
                'countryCode2': new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]+$')]),
                'countryCode3': new FormControl(null, Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]+$')),
                'countryNameTh': new FormControl(null, [Validators.required]),
                'countryNameEn': new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\_\\-=\\[\\]{};\':"\\\\|,.<>\\/? ]+$')]),
                'countryCodeNid': new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z0-9]+$')]),
                'remark': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.countryForm.reset();
    }

    onSubmit() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลประเทศใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.countryForm.valid ) {
                this.countryService.save(this.countryForm.value.countryGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลประเทศสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/country');
                    },
                    err => {
                      if (err.error.errorCode === 'ST098') {
                        this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสประเทศ (2 หลัก) หรือ รหัสประเทศ (3 หลัก)' + this.alertText.getTail);
                      } else {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                      }
                    }
                );
                sub.unsubscribe();
            }
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลประเทศยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                console.log(this.countryForm.value.countryGroup);
                this.router.navigateByUrl('/etl/country');
                sub.unsubscribe();
            }
        });
    }
}
