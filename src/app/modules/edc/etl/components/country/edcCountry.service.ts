import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {EdcCountryModel} from '../../models/edcCountry.model';
import {ResponseEtl} from '../../models/responseEtl.model';
import {environment} from '../../../../../../environments/environment';


@Injectable()
export class CountryService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcCountryModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcCountryModel[]>>>this.http.post(this.url + 'countries', request);
    }

    listSelect(): Observable<ResponseEtl<EdcCountryModel[]>> {
        return <Observable<ResponseEtl<EdcCountryModel[]>>>this.http.get(this.url + 'countries');
    }

    save(request: EdcCountryModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'country/add', request);
    }

    import(file: File, request: EdcCountryModel): Observable<any> {
        console.log(JSON.stringify(request));
        const data = new FormData();
        data.append('file', file);
        data.append('JsonData', JSON.stringify(request));
        console.log(data);
        return this.http.post(this.url + 'country/import', data);
    }

    update(request: EdcCountryModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'country/update', request);
    }

    delete(request: EdcCountryModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'country/delete', request);
    }
}
