import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

import {ModalService} from '../../../../../../core/services/modal.service';
import {CountryService} from '../edcCountry.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {

  countryForm: FormGroup;
  currentFileUpload: File;
  minDate: Date;

  constructor(
    private modal: ModalService,
    private countryService: CountryService,
    public router: Router) {
  }

  ngOnInit() {
    this.initialForm();
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);
  }

  initialForm() {
    this.countryForm = new FormGroup({
      countryGroup: new FormGroup({
        'countryFile': new FormControl(null)
      })
    });
  }

  resetForm() {
    this.currentFileUpload = null;
    this.countryForm.reset();
  }

  selectFile(event) {
    this.currentFileUpload = <File>event.target.files[0];
    console.log(this.currentFileUpload);
  }

  onSubmit() {
    // console.log(this.countryForm.value);
    // console.log(this.countryForm.value.countryGroup.file);
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลประเทศใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.countryService.import(this.currentFileUpload, this.countryForm.value.countryGroup).subscribe(
          response => {
            console.log(response);
            this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลประเทศสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('/etl/country');
          },
          err => {
            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    this.router.navigateByUrl('/etl/country');
    // const modalRef = this.modal.modalService.show(AnswerModalComponent);
    //
    // (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    // (modalRef.content as AnswerModalComponent).content = 'ข้อมูลนำเข้าประเทศไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
    // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
    //   if (a) {
    //     this.router.navigateByUrl('/etl/country');
    //     sub.unsubscribe();
    //   }
    // });
  }
}
