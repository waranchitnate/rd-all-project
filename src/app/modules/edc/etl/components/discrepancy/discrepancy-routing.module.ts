import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from '../discrepancy/list/list.component';
import {AddComponent} from '../discrepancy/add/add.component';
import {EditComponent} from '../discrepancy/edit/edit.component';

const routes: Routes = [
  { path: '', component: ListComponent},
  { path: 'add', component: AddComponent},
  { path: 'edit', component: EditComponent},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscrepancyRoutingModule { }
