import {Component, OnInit} from '@angular/core';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';

import {DiscrepancyService} from '../../discrepancy/edcDiscrepancy.service';
import {EdcDiscrepancyModel} from '../../../models/edcDiscrepancy.model';

import {ConfigCodeService} from './../../../edcConfigCode.service';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    ConfigCodeList = <EdcConfigCodeModel[]>[];
    discrepancyConfigList = <EdcConfigCodeModel[]>[];
    edcDiscrepancyList = <EdcDiscrepancyModel[]>[];
    private discrepancyConfigMap = new Map();

    discrepancyForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private discrepancyService: DiscrepancyService,
        private modal: ModalService,
        private router: Router,
        private configCodeService: ConfigCodeService
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
        // console.log(this.discrepancyConfigList);
    }

    onSubmit() {
        // console.log(this.discrepancyForm.value);
        this.getList(this.discrepancyForm.value.discrepancyGroup);
    }

    initialForm() {
        this.discrepancyForm = new FormGroup({
            discrepancyGroup: new FormGroup({
                'discrepancyType': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.discrepancyForm.reset();
        this.initialForm();
        this.getList(this.discrepancyForm.value.discrepancyGroup);
    }

    getList(param) {
        this.discrepancyService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcDiscrepancyList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;

        });

        this.configCodeService.listConfig(param).subscribe(result => {
            // console.log(result.data);
            this.ConfigCodeList = result.data;
            this.discrepancyConfigList = this.ConfigCodeList.filter(res => res.type === 'DISCREPANCY');
            // console.log(this.discrepancyConfigList);
            for (let i = 0; i <= this.discrepancyConfigList.length - 1; i++) {
                this.discrepancyConfigMap.set(this.discrepancyConfigList[i].code, this.discrepancyConfigList[i].description);
            }
            // console.log(this.discrepancyConfigMap);
        });
    }

    getPage(page: number) {
        this.discrepancyForm.value.discrepancyGroup.page = page;
        // console.log(this.discrepancyForm.value.discrepancyGroup);
        this.getList(this.discrepancyForm.value.discrepancyGroup);
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/discrepancy/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลค่าความคลาดเคลื่อนใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.discrepancyService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.discrepancyForm.value.discrepancyGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลค่าความคลาดเคลื่อนสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.discrepancyForm.value.discrepancyGroup.pageSize = pageSize;
        this.getList(this.discrepancyForm.value.discrepancyGroup);
    }
}
