import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {EdcDiscrepancyModel} from '../../models/edcDiscrepancy.model';
import {ResponseEtl} from '../../models/responseEtl.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class DiscrepancyService{
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcDiscrepancyModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcDiscrepancyModel[]>>>this.http.post(this.url + 'discrepancies', request);
    }
    save(request: EdcDiscrepancyModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'discrepancy/add', request);
    }

    update(request: EdcDiscrepancyModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'discrepancy/update', request);
    }

    delete(request: EdcDiscrepancyModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'discrepancy/delete', request);
    }
}
