import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {EdcDiscrepancyModel} from '../../../models/edcDiscrepancy.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {DiscrepancyService} from '../edcDiscrepancy.service';
import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  state: Observable<EdcDiscrepancyModel>;
  edcDiscrepancy: EdcDiscrepancyModel;
  discrepancyForm: FormGroup;
  minDate: Date;

  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private modal: ModalService,
    private discrepancyService: DiscrepancyService,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe(e => {
      this.edcDiscrepancy = e;
    });
    if (this.edcDiscrepancy.id === undefined) {
      this.router.navigateByUrl('/etl/currency');
    }
    this.minDate = new Date(this.edcDiscrepancy.effectiveDate);
    this.initialForm();
  }

  initialForm() {
    this.discrepancyForm = new FormGroup({
      discrepancyGroup: new FormGroup({
        'discrepancyType': new FormControl(this.edcDiscrepancy.discrepancyType, Validators.required),
        'posAmount': new FormControl(this.edcDiscrepancy.posAmount, [Validators.required, Validators.pattern('^[0-9]{1,3}(\\.[0-9]{1,2})?$')]),
        'minusAmount': new FormControl(this.edcDiscrepancy.minusAmount, [Validators.required, Validators.pattern('^[0-9]{1,3}(\\.[0-9]{1,2})?$')]),
        'effectiveDate': new FormControl(this.edcDiscrepancy.effectiveDate, Validators.required),
        'endDate': new FormControl(this.edcDiscrepancy.endDate),
        'remark': new FormControl(this.edcDiscrepancy.remark),
        'createdDate': new FormControl(this.edcDiscrepancy.createdDate),
        'createdBy': new FormControl(this.edcDiscrepancy.createdBy)
      })
    });
  }

  resetForm() {
    this.discrepancyForm.reset();
  }

  onSubmit() {
    console.log(this.discrepancyForm.value);

    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของค่าความคลาดเคลื่อนใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.discrepancyForm.value.discrepancyGroup.id = this.edcDiscrepancy.id;
        this.discrepancyService.update(this.discrepancyForm.value.discrepancyGroup).subscribe(
          response => {
            console.log(response);
            this.modal.openModal('แจ้งเตือน ', 'แก้ไขข้อมูลค่าความคลาดเคลื่อนสำเร็จ');

            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('/etl/discrepancy');
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'วันที่มีผล' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);

    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลค่าความคลาดเคลื่อนยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.router.navigateByUrl('/etl/discrepancy');
        sub.unsubscribe();
      }
    });
  }
}
