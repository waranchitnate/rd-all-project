import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {DiscrepancyService} from '../edcDiscrepancy.service';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    discrepancyForm: FormGroup;
    minDate: Date;
    endDate: Date;

    constructor(
        private modal: ModalService,
        private discrepancyService: DiscrepancyService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.initialForm();
        this.minDate = new Date();
        this.minDate.setDate(this.minDate.getDate());
        this.endDate = new Date();
    }

    initialForm() {
        this.discrepancyForm = new FormGroup({
            discrepancyGroup: new FormGroup({
                'discrepancyType': new FormControl(null, Validators.required),
                'posAmount': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]{1,3}(\\.[0-9]{1,2})?$')]),
                'minusAmount': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]{1,3}(\\.[0-9]{1,2})?$')]),
                'effectiveDate': new FormControl(null, Validators.required),
                'endDate': new FormControl(null),
                'remark': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.discrepancyForm.reset();
    }

    onSubmit() {
        console.log(this.discrepancyForm.value);

        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลค่าความคลาดเคลื่อนใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.discrepancyForm.valid) {
                this.discrepancyService.save(this.discrepancyForm.value.discrepancyGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลค่าความคลาดเคลื่อนสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/discrepancy');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'วันที่มีผล' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลค่าความคลาดเคลื่อนยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/discrepancy');
                sub.unsubscribe();
            }
        });
    }

    checkEndDate() {
      console.log('pass check effectiveDate =');
      console.log(this.discrepancyForm.value.discrepancyGroup.effectiveDate);
      this.endDate = new Date(this.discrepancyForm.value.discrepancyGroup.effectiveDate + 1);
    }
}

