import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportImproveComponent } from './report-improve.component';

describe('ReportImproveComponent', () => {
  let component: ReportImproveComponent;
  let fixture: ComponentFixture<ReportImproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportImproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportImproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
