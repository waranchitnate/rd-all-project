import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportNotFoundComponent } from './report-not-found.component';

describe('ReportNotFoundComponent', () => {
  let component: ReportNotFoundComponent;
  let fixture: ComponentFixture<ReportNotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportNotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
