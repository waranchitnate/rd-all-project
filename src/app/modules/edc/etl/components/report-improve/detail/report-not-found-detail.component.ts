import {Component, OnInit} from '@angular/core';
import {ConfigCodeService} from '../../../../mft/edcConfigCode.service';
import {SenderTransferPushService} from '../../../../mft/components/sender-transfer-push/sender-transfer-push.service';
import {ReportService} from '../../../edcReport.service';
import {FormControl, FormGroup} from '@angular/forms';
import {EdcTaxIdNotFoundModel} from '../../../models/edcTaxIdNotFound.model';
import {UserInfoModel} from '../../../../mft/models/userInfo.model';
import {EdcfinancialInstitutionModel} from '../../../models/edcFinancial-insitution.model';
import {TypeaheadMatch} from 'ngx-bootstrap';
import * as moment from 'moment';
import {FinancialInstitutionService} from '../../financial-institution/edcfinancial-institution.service';
import {map} from 'rxjs/operators';
import {EdcProcessingDetailModel} from '../../../models/edcProcessingDetail.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-report-notregisted',
  templateUrl: './report-not-found-detail.component.html',
  styleUrls: ['./report-not-found-detail.component.css']
})
export class ReportNotFoundDetailComponent implements OnInit {

  edcTaxIdNotFoundList = <EdcTaxIdNotFoundModel[]>[];
  financialList = <EdcfinancialInstitutionModel[]>[];
  reportIdNotFoundForm: FormGroup;
  financialMap = new Map();
  selectedOption: any;
  minDate: Date;
  total: number;
  pageSize: number;
  page: number;
  userInfoList = <UserInfoModel[]>[];
  trxFlagList = {1: 'ไม่พบในระบบ NID', 2: 'ไม่พบในระบบ VAT', 4: 'ไม่พบในระบบตรวจสอบและแนะนำ'};
  taxFlagList = {S: 'ผู้จ่าย', R: 'ผู้รับ'};
  state: Observable<{edcTaxIdNotFoundList: [], page: 0, total: 0, pageSize: 0, taxFlag: '' , trxFlag: ''}>;

  constructor(
    private configCodeService: ConfigCodeService,
    private senderTransferPushService: SenderTransferPushService,
    private reportService: ReportService,
    private financialInstitutionService: FinancialInstitutionService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  resetForm() {
    this.reportIdNotFoundForm.reset();
    this.getReport({});
  }

  initialForm() {
    this.reportIdNotFoundForm = new FormGroup({
      reportIdNotFoundC: new FormGroup({
        'fiCode': new FormControl(this.edcTaxIdNotFoundList[0].fiCode),
        'receiveDate': new FormControl(this.edcTaxIdNotFoundList[0].receiveDate),
        'dataTypeCode': new FormControl(this.edcTaxIdNotFoundList[0].dataTypeCode),
        'taxFlag': new FormControl(this.edcTaxIdNotFoundList[0].taxFlag),
        'trxFlag': new FormControl(this.edcTaxIdNotFoundList[0].trxFlag)
      })
    });
  }

  ngOnInit() {
    // const edcTaxIdNotFoundObj = {edcTaxIdNotFoundList: {}, page: '', total: '', pageSize: ''};
    // this.getReport({});
    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe((res) => {
      console.log('res edcTaxIdNotFoundList :' , res);
      this.edcTaxIdNotFoundList = res.edcTaxIdNotFoundList;
      this.page = res.page;
      this.total = res.total;
      this.pageSize = res.pageSize;
      if (this.edcTaxIdNotFoundList.length > 0) {
        this.initialForm();
      }
    });
    if (this.edcTaxIdNotFoundList === undefined) {
      this.router.navigateByUrl('/etl/report-improve');
    }
    this.getListSenderUser();
    this.getListFinancial();
  }

  getListSenderUser() {
    this.senderTransferPushService.listUser().subscribe(result => {
      this.userInfoList = result.data;
    });
  }

  getReport(param) {
    this.reportService.listTaxIdFoundDetail(param).subscribe(res => {
      this.edcTaxIdNotFoundList = res.data;
      this.page = res.page;
      this.total = res.total;
      this.pageSize = res.pageSize;
      // console.log(this.edcTaxIdNotFoundList);
    });
  }

  getPage(page: number) {
    console.log('page >', page);
    this.reportIdNotFoundForm.value.reportIdNotFoundC.page = page;
    console.log('this.reportIdNotFoundForm >', this.reportIdNotFoundForm);
    this.getReport(this.reportIdNotFoundForm.value.reportIdNotFoundC);
  }

  getPageSize(pageSize: number) {
    this.reportIdNotFoundForm.value.reportIdNotFoundC.pageSize = pageSize;
    this.getReport(this.reportIdNotFoundForm.value.reportIdNotFoundC);
  }

  onSelectFi(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.reportIdNotFoundForm.get('reportIdNotFoundC.fiCode').setValue(this.selectedOption.fiCode);
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }

  onBack() {
    this.router.navigateByUrl('/etl/report-improve');
  }
}
