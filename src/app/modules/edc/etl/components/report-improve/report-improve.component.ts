import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UserInfoModel} from '../../../mft/models/userInfo.model';
import {DataTypeModel} from '../../../mft/models/dataType.model';
import {EdcDataTypeService} from '../../../mft/components/data-type/edcDataType.service';
import {ConfigCodeService} from '../../../mft/edcConfigCode.service';
import {SenderTransferPushService} from '../../../mft/components/sender-transfer-push/sender-transfer-push.service';
import {ReportService} from '../../edcReport.service';
import * as moment from 'moment';
import {TypeaheadMatch} from 'ngx-bootstrap-th';
import {EdcReportCrossValidationModel} from '../../models/edcReportCrossValidation.model';
import {FinancialInstitutionService} from '../financial-institution/edcfinancial-institution.service';
import {EdcfinancialInstitutionModel} from '../../models/edcFinancial-insitution.model';
import {EdcTaxIdNotFoundModel} from '../../models/edcTaxIdNotFound.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-report-improve',
  templateUrl: './report-improve.component.html',
  styleUrls: ['./report-improve.component.css']
})
export class ReportImproveComponent implements OnInit {

  reportImproveSearchForm: FormGroup;
  userInfoList = <UserInfoModel[]>[];
  dataTypeList = <DataTypeModel[]>[];
  financialList = <EdcfinancialInstitutionModel[]>[];
  edcReportCrossValidationList = <EdcReportCrossValidationModel[]>[];
  edcTaxIdNotFoundList = <EdcTaxIdNotFoundModel[]>[];
  minDate: Date;
  financialMap = new Map();
  edcCountryList: any;
  selectedOption: any;
  total: number;
  pageSize: number;
  page: number;
  totalRecord: number;
  totalFoundNidRecord: number;
  totalNotFoundNidRecord: number;
  totalVatRegisteredRecord: number;
  totalNotVatRegisteredRecord: number;
  totalFoundTeamRecord: number;
  totalNotFoundTeamRecord: number;
  obj: EdcReportCrossValidationModel;
  taxDataTypeList = {DT002: 'ผู้จ่าย', DT003: 'ผู้รับ'};

  constructor(
    private edcDataTypeService: EdcDataTypeService,
    private configCodeService: ConfigCodeService,
    private senderTransferPushService: SenderTransferPushService,
    private reportService: ReportService,
    private router: Router,
    private financialInstitutionService: FinancialInstitutionService,
  ) {
  }

  ngOnInit() {
    this.initialForm();
    this.getListDataType();
    this.getListSenderUser();
    this.getListFinancial();
    this.getReport({});
  }

  initialForm() {
    this.reportImproveSearchForm = new FormGroup({
      reportImproveSearchC: new FormGroup({
        'senderBankCode': new FormControl(null),
        'senderId': new FormControl(null),
        'dataTypeId': new FormControl(null),
        'dateRange': new FormControl(null),
        'fiCode': new FormControl(null),
        'fiNameTh': new FormControl(null)
      })
    });
  }

  onSubmit() {
    if (this.reportImproveSearchForm.get('reportImproveSearchC').value.dateRange !== null) {
      this.reportImproveSearchForm.value.reportImproveSearchC.dateStart = moment(this.reportImproveSearchForm.get('reportImproveSearchC').value.dateRange[0]).format('YYYY-MM-DD');
      this.reportImproveSearchForm.value.reportImproveSearchC.dateEnd = moment(this.reportImproveSearchForm.get('reportImproveSearchC').value.dateRange[1]).format('YYYY-MM-DD');
    }
    this.getReport(this.reportImproveSearchForm.value.reportImproveSearchC);
  }

  onSelect(event: TypeaheadMatch) {
    this.selectedOption = event.item;
    this.reportImproveSearchForm.get('reportImproveSearchC.senderId').setValue(this.selectedOption.userId);
  }

  resetForm() {
    this.reportImproveSearchForm.reset();
    this.getReport({});
  }

  getReport(param) {
    console.log(param);
    this.reportService.listCrossValidation(param).subscribe(res => {
      this.edcReportCrossValidationList = res.data;
      this.totalRecord = this.edcReportCrossValidationList.reduce((totalSum, e) => totalSum + e.totalRecord, 0);
      this.totalFoundNidRecord = this.edcReportCrossValidationList.reduce((totalSum, e) => totalSum + e.totalFoundNidRecord, 0);
      this.totalNotFoundNidRecord = this.edcReportCrossValidationList.reduce((totalSum, e) => totalSum + e.totalNotFoundNidRecord, 0);

      this.totalNotFoundTeamRecord = this.edcReportCrossValidationList.reduce((totalSum, e) => totalSum + e.totalNotFoundTeamRecord, 0);
      this.totalFoundTeamRecord = this.edcReportCrossValidationList.reduce((totalSum, e) => totalSum + e.totalFoundTeamRecord, 0);
      this.totalVatRegisteredRecord = this.edcReportCrossValidationList.reduce((totalSum, e) => totalSum + e.totalVatRegisteredRecord, 0);
      this.totalNotVatRegisteredRecord = this.edcReportCrossValidationList.reduce((totalSum, e) => totalSum + e.totalNotVatRegisteredRecord, 0);
      console.log('edcReportCrossValidationList : ', this.edcReportCrossValidationList);
      this.page = res.page;
      this.total = res.total;
      this.pageSize = res.pageSize;
      console.log(res.data);
    });
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }

  getListDataType() {
    this.edcDataTypeService.listSelect().subscribe(res => {
      this.dataTypeList = res.data;
    });
  }

  getListSenderUser() {
    this.senderTransferPushService.listUser().subscribe(result => {
      this.userInfoList = result.data;
    });
  }

  getPage(page: number) {
    this.reportImproveSearchForm.value.reportImproveSearchC.page = page;
    this.getReport(this.reportImproveSearchForm.value.reportImproveSearchC);
  }

  getPageSize(pageSize: number) {
    this.reportImproveSearchForm.value.reportImproveSearchC.pageSize = pageSize;
    this.getReport(this.reportImproveSearchForm.value.reportImproveSearchC);
  }

  onSelectFi(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.reportImproveSearchForm.get('reportImproveSearchC.fiCode').setValue(this.selectedOption.fiCode);
  }

  onViewDetail(el: EdcReportCrossValidationModel, taxFlag: string , trxFlag: string): void {
    this.obj = el;
    console.log('EdcReportCrossValidationModel click :', el);
    this.obj.dataTypeCode = this.obj.dataType.dataTypeCode;
    this.obj.taxFlag = taxFlag;
    this.obj.trxFlag = trxFlag;
    this.reportService.listTaxIdFoundDetail(this.obj).subscribe(a => {
      console.log('a result :', a);
      console.log('data result :', a.data);
      const edcTaxIdNotFoundObj = {edcTaxIdNotFoundList: a.data, page: a.page, total: a.total, pageSize: a.pageSize, taxFlag: taxFlag , trxFlag: trxFlag};
      console.log('edcTaxIdNotFoundObj result :', edcTaxIdNotFoundObj);
      this.router.navigateByUrl('/etl/report-not-found-detail', {state: edcTaxIdNotFoundObj});
    });
  }
}
