import {Component, OnInit} from '@angular/core';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {EdcAmphurModel} from '../../../models/edcAmphur.model';
import {EdcThambolModel} from '../../../models/edcThambol.model';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ThambolService} from '../../thambol/edcThambol.service';
import {AmphurService} from '../../amphur/edcAmphur.service';
import {ProvinceService} from '../../province/edcProvince.service';
import {OfficeCodeService} from '../edcOfficecode.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';
import {EdcOfficeCodeModel} from "../../../models/edcOfficeCode.model";
import {EdcConfigCodeModel} from "../../../models/edcConfigCode.model";
import {ConfigCodeService} from './../../../edcConfigCode.service';

@Component({
    selector: 'app-view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

    state: Observable<EdcOfficeCodeModel>;
    private edcOfficeCodeModel: EdcOfficeCodeModel;

    edcProvinceList = <EdcProvinceModel[]>[];
    edcAmphurList = <EdcAmphurModel[]>[];
    ConfigCodeList = <EdcConfigCodeModel[]>[];
    officeTypeList = <EdcConfigCodeModel[]>[];
    private officeTypeConfigMap = new Map();
    minDate: Date;

    items: FormArray;
    officecodeForm: FormGroup;

    private selectedOptionAmphur: EdcAmphurModel;
    private selectedOptionProvince: EdcProvinceModel;

    constructor(
        public activatedRoute: ActivatedRoute,
        public router: Router,
        private thambolService: ThambolService,
        private amphurService: AmphurService,
        private  provinceService: ProvinceService,
        private  officeCodeService: OfficeCodeService,
        private modal: ModalService,
        private alertText: AlertTextModule,
        private configCodeService: ConfigCodeService
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcOfficeCodeModel = e;
        });
        if (this.edcOfficeCodeModel.id === undefined) {
            this.router.navigateByUrl('/etl/officecode');
        }
      this.minDate = new Date(this.edcOfficeCodeModel.updatedate);
        this.getProvinceList();
        this.getAmphurList();
        this.getOfficeTypeList({});
        this.initialForm();
    }
  getOfficeTypeList(param) {
    this.configCodeService.listConfig(param).subscribe(result => {
      // console.log(result.data);
      this.ConfigCodeList = result.data;
      this.officeTypeList = this.ConfigCodeList.filter(res => res.type === 'OFFICECODE_TYPE');
      for (let i = 0; i <= this.officeTypeList.length - 1; i++) {
        this.officeTypeConfigMap.set(this.officeTypeList[i].code, this.officeTypeList[i].description);
      }
// console.log(this.discrepancyConfigMap);
    });
  }
    onSubmit() {
        console.log(this.officecodeForm.value);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลรหัสไปรษณีย์ใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.officecodeForm.value.officecodeGroup.id = this.edcOfficeCodeModel.id;
                this.officeCodeService.update(this.officecodeForm.value.officecodeGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลประเภทข้อมูลสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/officecode');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสไปรษณีย์' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        // const modalRef = this.modal.modalService.show(AnswerModalComponent);
        // (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        // (modalRef.content as AnswerModalComponent).content = 'ข้อมูลรหัสไปรษณีย์ยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
        //     if (a) {
                this.router.navigateByUrl('/etl/officecode');
        //         sub.unsubscribe();
        //     }
        // });
    }

    initialForm() {
        this.officecodeForm = new FormGroup({
          officecodeGroup: new FormGroup({
                'rdOfficecode': new FormControl(this.edcOfficeCodeModel.rdOfficecode, Validators.required),
                'provinceId': new FormControl(this.edcOfficeCodeModel.edcProvinceEntity.id),
                'provinceNameTh': new FormControl(this.edcOfficeCodeModel.edcProvinceEntity.provinceNameTh),
                'amphurId': new FormControl(this.edcOfficeCodeModel.edcAmphurEntity.id),
                'amphurNameTh': new FormControl(this.edcOfficeCodeModel.edcAmphurEntity.amphurNameTh),
                'shotDescription': new FormControl(this.edcOfficeCodeModel.shotDescription),
                'longDescription': new FormControl(this.edcOfficeCodeModel.longDescription),
                'officeType': new FormControl(this.edcOfficeCodeModel.officeType),
                'prvOfficecode': new FormControl(this.edcOfficeCodeModel.prvOfficecode),
                 'regionCode': new FormControl(this.edcOfficeCodeModel.regionCode),
                'comment': new FormControl(this.edcOfficeCodeModel.comment),
                 'updatedate': new FormControl(this.edcOfficeCodeModel.updatedate),
            })
        });

        console.log("obj officeCode >>"+this.officecodeForm);
    }

    resetForm() {
        this.officecodeForm.reset();
        this.resetAmphur();
    }

    onSelectProvince(event: TypeaheadMatch): void {
        this.selectedOptionProvince = event.item;
        this.officecodeForm.get('officecodeGroup.provinceId').setValue(this.selectedOptionProvince.id);
        this.edcAmphurList = this.edcAmphurList.filter(res => res.provinceId === this.selectedOptionProvince.id);
    }

    onSelectAmphur(event: TypeaheadMatch): void {
        this.selectedOptionAmphur = event.item;
        this.officecodeForm.get('officecodeGroup.amphurId').setValue(this.selectedOptionAmphur.id);

        if (this.officecodeForm.get('officecodeGroup.provinceId') != null) {
            this.officecodeForm.get('officecodeGroup.provinceId').setValue(this.selectedOptionAmphur.provinceId);
            this.officecodeForm.get('officecodeGroup.provinceNameTh').setValue(this.selectedOptionAmphur.edcProvinceEntity.provinceNameTh);
        }
    }

    // onSelectThambol(event: TypeaheadMatch): void {
    //     this.selectedOptionThambol = event.item;
    //     this.officecodeForm.get('officecodeGroup.thambolId').setValue(this.selectedOptionThambol.id);
    //     if (this.officecodeForm.get('officecodeGroup.provinceId') != null) {
    //         this.officecodeForm.get('officecodeGroup.amphurId').setValue(this.selectedOptionThambol.amphurId);
    //         this.officecodeForm.get('officecodeGroup.amphurNameTh').setValue(this.selectedOptionThambol.edcAmphurEntity.amphurNameTh);
    //     }
    //     if (this.officecodeForm.get('officecodeGroup.provinceId') != null) {
    //         this.officecodeForm.get('officecodeGroup.provinceId').setValue(this.selectedOptionThambol.provinceId);
    //         this.officecodeForm.get('officecodeGroup.provinceNameTh').setValue(this.selectedOptionThambol.edcProvinceEntity.provinceNameTh);
    //     }
    // }

    getProvinceList() {
        this.provinceService.listSelect().subscribe(res => {
            this.edcProvinceList = res.data;
        });
    }

    getAmphurList() {
        this.amphurService.listSelect().subscribe(res => {
            this.edcAmphurList = res.data;
        });
    }

    resetAmphur() {
        this.getAmphurList();
        this.officecodeForm.get('officecodeGroup.amphurId').reset();
        this.officecodeForm.get('officecodeGroup.amphurNameTh').reset();
    }

}
