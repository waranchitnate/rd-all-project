import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListComponent} from '../officecode/list/list.component';
import {ViewComponent} from '../officecode/view/view.component';
import {ImportComponent} from '../officecode/import/import.component';

const routes: Routes = [
    {path: '', component: ListComponent},
    {path: 'view', component: ViewComponent},
  { path: 'import', component: ImportComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OfficecodeRoutingModule {
}
