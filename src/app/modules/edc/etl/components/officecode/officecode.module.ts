import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OfficecodeRoutingModule} from './officecode-routing.module';
import {ViewComponent} from './view/view.component';
import {ListComponent} from './list/list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {TypeaheadModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../../../shared/shared.module';
import {ImportComponent} from "../officecode/import/import.component";

@NgModule({
    declarations: [ ViewComponent, ListComponent, ImportComponent],
  imports: [
    CommonModule,
    OfficecodeRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    BsDatepickerModule,
    TypeaheadModule,
    SharedModule
  ]
})
export class OfficecodeModule {
}
