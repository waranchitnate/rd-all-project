import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcOfficeCodeModel} from '../../models/edcOfficeCode.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class OfficeCodeService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcOfficeCodeModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcOfficeCodeModel[]>>>this.http.post(this.url + 'officecode', request);
    }

    save(request: EdcOfficeCodeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'officecode/add', request);
    }

    update(request: EdcOfficeCodeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'officecode/update', request);
    }

    delete(request: EdcOfficeCodeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'officecode/delete', request);
    }

  import(file: File): Observable<any> {
    const data = new FormData();
    data.append('file', file);
    console.log(data);
    return this.http.post(this.url + 'officecode/import', data);
  }
}
