import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {FinancialInstitutionService} from '../../financial-institution/edcfinancial-institution.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Router} from '@angular/router';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  fiForm: FormGroup;
  minDate: Date;

  constructor(
    private modal: ModalService,
    private fiService: FinancialInstitutionService,
    private router: Router,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    this.initialForm();
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);
  }

  initialForm() {
    this.fiForm = new FormGroup({
      fiGroup: new FormGroup({
        'fiCode': new FormControl(null, [Validators.required, Validators.pattern('^([A-Za-z0-9]{3}|[A-Za-z0-9]{13})$')]),
        'fiNameTh': new FormControl(null, Validators.required),
        'fiNameEn': new FormControl(null, Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/? ]+$')),
        'activateDate': new FormControl(null, Validators.required),
        'closeDate': new FormControl(null),
        'remark': new FormControl(null),
        'taxId': new FormControl(null, Validators.pattern('^[0-9]{13}?$'))
      })
    });
  }

  resetForm() {
    this.fiForm.reset();
  }

  onSubmit() {
    console.log(this.fiForm.value);

    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลสถาบันการเงินใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.fiForm.valid) {
        this.fiService.save(this.fiForm.value.fiGroup).subscribe(
          response => {
            console.log(response);
            this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลสถาบันการเงินสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('/etl/financial-institution');
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสสถาบันการเงิน' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);

    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลสถาบันการเงินยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.router.navigateByUrl('/etl/financial-institution');
        sub.unsubscribe();
      }
    });
  }
}
