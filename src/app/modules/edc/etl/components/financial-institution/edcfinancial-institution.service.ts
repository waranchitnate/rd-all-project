import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {EdcfinancialInstitutionModel} from '../../models/edcFinancial-insitution.model';
import {ResponseEtl} from '../../models/responseEtl.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class FinancialInstitutionService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcfinancialInstitutionModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcfinancialInstitutionModel[]>>>this.http.post(this.url + 'financialCodes', request);
    }

    listSelect(): Observable<ResponseEtl<EdcfinancialInstitutionModel[]>> {
        return <Observable<ResponseEtl<EdcfinancialInstitutionModel[]>>>this.http.get(this.url + 'financialCodes');
    }

    save(request: EdcfinancialInstitutionModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'financialCode/add', request);
    }

    import(file: File): Observable<any> {
        const data = new FormData();
        data.append('file', file);
        console.log(data);
        return this.http.post(this.url + 'financialCode/import', data);
    }

    update(request: EdcfinancialInstitutionModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'financialCode/update', request);
    }

    delete(request: EdcfinancialInstitutionModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'financialCode/delete', request);
    }
}
