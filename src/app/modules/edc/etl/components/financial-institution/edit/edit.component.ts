import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

import {map} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';

import {FinancialInstitutionService} from '../edcfinancial-institution.service';
import {EdcfinancialInstitutionModel} from '../../../models/edcFinancial-insitution.model';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  state: Observable<EdcfinancialInstitutionModel>;
  edcFi: EdcfinancialInstitutionModel;
  fiForm: FormGroup;
  minDate: Date;

  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private modal: ModalService,
    private fiService: FinancialInstitutionService,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe(e => {
      this.edcFi = e;
    });
    if (this.edcFi.id === undefined) {
      this.router.navigateByUrl('/etl/financial-institution');
    }
    this.minDate = new Date(this.edcFi.activateDate);
    this.initialForm();
  }

  initialForm() {
    this.fiForm = new FormGroup({
      fiGroup: new FormGroup({
        'fiCode': new FormControl(this.edcFi.fiCode, [Validators.required, Validators.pattern('^([A-Za-z0-9]{3}|[A-Za-z0-9]{13})$')]),
        'fiNameTh': new FormControl(this.edcFi.fiNameTh, Validators.required),
        'fiNameEn': new FormControl(this.edcFi.fiNameEn, Validators.pattern('^[A-Za-z0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/? ]+$')),
        'activateDate': new FormControl(this.edcFi.activateDate, Validators.required),
        'closeDate': new FormControl(this.edcFi.closeDate),
        'remark': new FormControl(this.edcFi.remark),
        'taxId': new FormControl(this.edcFi.taxId, Validators.pattern('^[0-9]{13}?$')),
        'createdDate': new FormControl(this.edcFi.createdDate),
        'createdBy': new FormControl(this.edcFi.createdBy)
      })
    });
  }

  resetForm() {
    this.fiForm.reset();
  }

  onSubmit() {
    console.log(this.fiForm.value);

    const modalRef = this.modal.modalService.show(AnswerModalComponent);

    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของสถาบันการเงินใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.fiForm.valid) {
        this.fiForm.value.fiGroup.id = this.edcFi.id;
        this.fiService.update(this.fiForm.value.fiGroup).subscribe(
          response => {
            console.log(response);
            this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลสถาบันการเงินสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
              this.router.navigateByUrl('/etl/financial-institution');
            });
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสสถาบันการเงิน' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);

    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลสถาบันการเงินยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.router.navigateByUrl('/etl/financial-institution');
        sub.unsubscribe();
      }
    });
  }
}
