import {FinancialInstitutionService} from '../edcfinancial-institution.service';
import {EdcfinancialInstitutionModel} from '../../../models/edcFinancial-insitution.model';

import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {


    edcFiList = <EdcfinancialInstitutionModel[]>[];
    fiForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private financialInstitutionService: FinancialInstitutionService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.fiForm.value);
        this.getList(this.fiForm.value.fiGroup);
    }

    initialForm() {
        this.fiForm = new FormGroup({
            fiGroup: new FormGroup({
                'fiCode': new FormControl(null),
                'fiNameTh': new FormControl(null),
                'taxId': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.fiForm.reset();
        this.initialForm();
        this.getList(this.fiForm.value.fiGroup);
    }

    downloadFile() {
        const link = document.createElement('a');
        link.download = 'temp_FICode';
        link.href = 'assets/filestemplate/temp_FICode.xlsx';
        link.click();
    }

    getList(param) {
        this.financialInstitutionService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcFiList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.fiForm.value.fiGroup.page = page;
        // console.log(this.fiForm.value.fiGroup);
        this.getList(this.fiForm.value.fiGroup);
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/financial-institution/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลสถาบันการเงินใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.financialInstitutionService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.fiForm.value.fiGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลสถาบันการเงินสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.fiForm.value.fiGroup.pageSize = pageSize;
        this.getList(this.fiForm.value.fiGroup);
    }

}
