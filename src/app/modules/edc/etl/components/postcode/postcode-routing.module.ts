import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListComponent} from '../postcode/list/list.component';
import {AddComponent} from '../postcode/add/add.component';
import {ViewComponent} from '../postcode/view/view.component';
import {ImportComponent} from "../postcode/import/import.component";

const routes: Routes = [
    {path: '', component: ListComponent},
    {path: 'add', component: AddComponent},
    {path: 'view', component: ViewComponent},
  { path: 'import', component: ImportComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PostcodeRoutingModule {
}
