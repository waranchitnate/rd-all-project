import {Component, OnInit} from '@angular/core';
import {EdcAmphurModel} from '../../../models/edcAmphur.model';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {EdcPostcodeModel} from '../../../models/edcPostcode.model';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {EdcThambolModel} from '../../../models/edcThambol.model';
import {ThambolService} from '../../thambol/edcThambol.service';
import {AmphurService} from '../../amphur/edcAmphur.service';
import {ProvinceService} from '../../province/edcProvince.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {PostcodeService} from '../edcPostcode.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {TypeaheadMatch} from 'ngx-bootstrap';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcProvinceList = <EdcProvinceModel[]>[];
    edcAmphurList = <EdcAmphurModel[]>[];
    edcThambolList = <EdcThambolModel[]>[];
    edcPostcodeList = <EdcPostcodeModel[]>[];

    private selectedOptionAmphur: EdcAmphurModel;
    private selectedOptionProvince: EdcProvinceModel;
    private selectedOptionThambol: EdcThambolModel;

    items: FormArray;
    postcodeForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private thambolService: ThambolService,
        private amphurService: AmphurService,
        private  provinceService: ProvinceService,
        private  postcodeService: PostcodeService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getProvinceList();
        this.getAmphurList();
        this.getThambolList();
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.postcodeForm.value);
        this.getList(this.postcodeForm.value.postcodeGroup);
    }

    initialForm() {
        this.postcodeForm = new FormGroup({
            postcodeGroup: new FormGroup({
                'postcode': new FormControl(null),
                'provinceId': new FormControl(null),
                'provinceNameTh': new FormControl(null),
                'amphurId': new FormControl(null),
                'amphurNameTh': new FormControl(null),
                'thambolId': new FormControl(null),
                'thambolNameTh': new FormControl(null),
            })
        });
    }

    onSelectAmphur(event: TypeaheadMatch): void {
        this.selectedOptionAmphur = event.item;
        this.postcodeForm.get('postcodeGroup.amphurId').setValue(this.selectedOptionAmphur.id);
        this.edcThambolList = this.edcThambolList.filter(res => res.amphurId === this.selectedOptionAmphur.id);
    }

    onSelectThambol(event: TypeaheadMatch): void {
        this.selectedOptionThambol = event.item;
        this.postcodeForm.get('postcodeGroup.thambolId').setValue(this.selectedOptionThambol.id);
    }

    onSelectProvince(event: TypeaheadMatch): void {
        this.selectedOptionProvince = event.item;
        this.postcodeForm.get('postcodeGroup.provinceId').setValue(this.selectedOptionProvince.id);
        this.edcAmphurList = this.edcAmphurList.filter(res => res.provinceId === this.selectedOptionProvince.id);
    }

    resetAmphur() {
        this.getAmphurList();
        this.postcodeForm.get('postcodeGroup.amphurId').reset();
        this.postcodeForm.get('postcodeGroup.amphurNameTh').reset();
    }

    getList(param) {
        this.postcodeService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcPostcodeList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getProvinceList() {
        this.provinceService.listSelect().subscribe(res => {
            this.edcProvinceList = res.data;
        });
    }

    getAmphurList() {
        this.amphurService.listSelect().subscribe(res => {
            this.edcAmphurList = res.data;
        });
    }

    getThambolList() {
        this.thambolService.listSelect().subscribe(res => {
            this.edcThambolList = res.data;
        });
    }

    resetForm() {
        // this.postcodeForm.reset();
        this.initialForm();
        // this.getAmphurList();
        // this.getThambolList();
        this.getList(this.postcodeForm.value.postcodeGroup);
    }

    getPage(page: number) {
        this.postcodeForm.value.postcodeGroup.page = page;
        // console.log(this.postcodeForm.value.postcodeGroup);
        this.getList(this.postcodeForm.value.postcodeGroup);
    }

    getPageSize(pageSize: number) {
        this.postcodeForm.value.postcodeGroup.pageSize = pageSize;
        this.getList(this.postcodeForm.value.postcodeGroup);
    }

    onView(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/postcode/view', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลรหัสไปรษณีย์ใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.postcodeService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.postcodeForm.value.postcodeGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลรหัสไปรษณีย์ข้อมูลสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('เกิดข้อผิดพลาด', 'ไม่สามารถทำการลบได้กรุณาทำรายการใหม่ภายหลัง');
                    }
                );
            }
            sub.unsubscribe();
        });
    }
}
