import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {PostcodeService} from '../edcPostcode.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Router} from '@angular/router';

@Component({
    selector: 'app-import',
    templateUrl: './import.component.html',
    styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {

  postCodeForm: FormGroup;
    currentFileUpload: File;

    constructor(
        private modal: ModalService,
        private postcodeService: PostcodeService,
        private router: Router) {
    }

    ngOnInit() {
        this.initialForm();
    }

    initialForm() {
        this.postCodeForm = new FormGroup({
          postCodeGroup: new FormGroup({
            'filePostCode': new FormControl(null)
          })
        });
    }

    resetForm() {
        this.currentFileUpload = null;
        this.postCodeForm.reset();
    }

    selectFile(event) {
        this.currentFileUpload = <File>event.target.files[0];
        console.log(this.currentFileUpload);
    }

    onSubmit() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลรหัสไปรษณีย์ใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.postcodeService.import(this.currentFileUpload).subscribe(
                    response => {
                        console.log(response);
                      // @ts-ignore\
                      const FileSaver = require('file-saver');
                      if (response.fileNameBase !== '') {
                        try {
                          const byteCharacters = atob(response.fileDownload);
                          const byteNumbers = new Array(byteCharacters.length);
                          for (let i = 0; i < byteCharacters.length; i++) {
                            byteNumbers[i] = byteCharacters.charCodeAt(i);
                          }
                          const byteArray = new Uint8Array(byteNumbers);
                          const file = new Blob([byteArray], {type: 'application/text'});
                          window.navigator.msSaveBlob(file, response.fileNameBase);
                        } catch (err) {
                          console.log('err');
                          const byteCharacters = atob(response.fileDownload);
                          const byteNumbers = new Array(byteCharacters.length);
                          for (let i = 0; i < byteCharacters.length; i++) {
                            byteNumbers[i] = byteCharacters.charCodeAt(i);
                          }
                          const byteArray = new Uint8Array(byteNumbers);
                          const file = new Blob([byteArray], {type: 'application/text'});
                          FileSaver.saveAs(file, response.fileNameBase);
                        }
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลรหัสไปรษณีย์ไม่สำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                          modalHide.unsubscribe();
                        });
                      } else {
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลรหัสไปรษณีย์สำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                          modalHide.unsubscribe();
                        });
                      }
                        this.router.navigateByUrl('/etl/postcode');
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
      this.router.navigateByUrl('/etl/postcode');
        // const modalRef = this.modal.modalService.show(AnswerModalComponent);
        //
        // (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        // (modalRef.content as AnswerModalComponent).content = 'ข้อมูล รหัสไปรษณีย์ยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
        //     if (a) {
        //         this.router.navigateByUrl('/etl/postcode');
        //         sub.unsubscribe();
        //     }
        // });
    }
}
