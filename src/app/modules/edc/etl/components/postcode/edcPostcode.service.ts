import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcPostcodeModel} from '../../models/edcPostcode.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class PostcodeService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcPostcodeModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcPostcodeModel[]>>>this.http.post(this.url + 'postcodes', request);
    }

    save(request: EdcPostcodeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'postcode/add', request);
    }

    update(request: EdcPostcodeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'postcode/update', request);
    }

    delete(request: EdcPostcodeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'postcode/delete', request);
    }

  import(file: File): Observable<any> {
    const data = new FormData();
    data.append('file', file);
    console.log(data);
    return this.http.post(this.url + 'postcode/import', data);
  }
}
