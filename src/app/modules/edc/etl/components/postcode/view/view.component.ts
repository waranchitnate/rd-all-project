import {Component, OnInit} from '@angular/core';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {EdcAmphurModel} from '../../../models/edcAmphur.model';
import {EdcThambolModel} from '../../../models/edcThambol.model';
import {EdcPostcodeModel} from '../../../models/edcPostcode.model';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ThambolService} from '../../thambol/edcThambol.service';
import {AmphurService} from '../../amphur/edcAmphur.service';
import {ProvinceService} from '../../province/edcProvince.service';
import {PostcodeService} from '../edcPostcode.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

    state: Observable<EdcPostcodeModel>;
    private edcEdcPostcode: EdcPostcodeModel;

    edcProvinceList = <EdcProvinceModel[]>[];
    edcAmphurList = <EdcAmphurModel[]>[];
    edcThambolList = <EdcThambolModel[]>[];

    items: FormArray;
    postcodeForm: FormGroup;

    private selectedOptionAmphur: EdcAmphurModel;
    private selectedOptionProvince: EdcProvinceModel;
    private selectedOptionThambol: EdcThambolModel;

    constructor(
        public activatedRoute: ActivatedRoute,
        public router: Router,
        private thambolService: ThambolService,
        private amphurService: AmphurService,
        private  provinceService: ProvinceService,
        private  postcodeService: PostcodeService,
        private modal: ModalService,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcEdcPostcode = e;
        });
        if (this.edcEdcPostcode.id === undefined) {
            this.router.navigateByUrl('/etl/postcode');
        }

        this.getProvinceList();
        this.getAmphurList();
        this.getThambolList();
        this.initialForm();
    }

    onSubmit() {
        console.log(this.postcodeForm.value);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลรหัสไปรษณีย์ใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.postcodeForm.value.postcodeGroup.id = this.edcEdcPostcode.id;
                this.postcodeService.update(this.postcodeForm.value.postcodeGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลประเภทข้อมูลสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/postcode');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสไปรษณีย์' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        // const modalRef = this.modal.modalService.show(AnswerModalComponent);
        // (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        // (modalRef.content as AnswerModalComponent).content = 'ข้อมูลรหัสไปรษณีย์ยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
        //     if (a) {
                this.router.navigateByUrl('/etl/postcode');
        //         sub.unsubscribe();
        //     }
        // });
    }

    initialForm() {
        this.postcodeForm = new FormGroup({
            postcodeGroup: new FormGroup({
                'postcode': new FormControl(this.edcEdcPostcode.postcode, Validators.required),
                'provinceId': new FormControl(this.edcEdcPostcode.edcProvinceEntity.id),
                'provinceNameTh': new FormControl(this.edcEdcPostcode.edcProvinceEntity.provinceNameTh, Validators.required),
                'amphurId': new FormControl(this.edcEdcPostcode.edcAmphurEntity.id),
                'amphurNameTh': new FormControl(this.edcEdcPostcode.edcAmphurEntity.amphurNameTh, Validators.required),
                'thambolId': new FormControl(this.edcEdcPostcode.edcThambolEntity.id),
                'thambolNameTh': new FormControl(this.edcEdcPostcode.edcThambolEntity.thambolNameTh, Validators.required),
                'comment': new FormControl(this.edcEdcPostcode.comment),
            })
        });
    }

    resetForm() {
        this.postcodeForm.reset();
        this.resetAmphur();
        this.resetThambol();
    }

    onSelectProvince(event: TypeaheadMatch): void {
        this.selectedOptionProvince = event.item;
        this.postcodeForm.get('postcodeGroup.provinceId').setValue(this.selectedOptionProvince.id);
        this.edcAmphurList = this.edcAmphurList.filter(res => res.provinceId === this.selectedOptionProvince.id);
        this.edcThambolList = this.edcThambolList.filter(res => res.provinceId === this.selectedOptionProvince.id);
    }

    onSelectAmphur(event: TypeaheadMatch): void {
        this.selectedOptionAmphur = event.item;
        this.postcodeForm.get('postcodeGroup.amphurId').setValue(this.selectedOptionAmphur.id);

        if (this.postcodeForm.get('postcodeGroup.provinceId') != null) {
            this.postcodeForm.get('postcodeGroup.provinceId').setValue(this.selectedOptionAmphur.provinceId);
            this.postcodeForm.get('postcodeGroup.provinceNameTh').setValue(this.selectedOptionAmphur.edcProvinceEntity.provinceNameTh);
        }
        this.edcThambolList = this.edcThambolList.filter(res => res.amphurId === this.selectedOptionAmphur.id);
    }

    onSelectThambol(event: TypeaheadMatch): void {
        this.selectedOptionThambol = event.item;
        this.postcodeForm.get('postcodeGroup.thambolId').setValue(this.selectedOptionThambol.id);
        if (this.postcodeForm.get('postcodeGroup.provinceId') != null) {
            this.postcodeForm.get('postcodeGroup.amphurId').setValue(this.selectedOptionThambol.amphurId);
            this.postcodeForm.get('postcodeGroup.amphurNameTh').setValue(this.selectedOptionThambol.edcAmphurEntity.amphurNameTh);
        }
        if (this.postcodeForm.get('postcodeGroup.provinceId') != null) {
            this.postcodeForm.get('postcodeGroup.provinceId').setValue(this.selectedOptionThambol.provinceId);
            this.postcodeForm.get('postcodeGroup.provinceNameTh').setValue(this.selectedOptionThambol.edcProvinceEntity.provinceNameTh);
        }
    }

    getProvinceList() {
        this.provinceService.listSelect().subscribe(res => {
            this.edcProvinceList = res.data;
        });
    }

    getAmphurList() {
        this.amphurService.listSelect().subscribe(res => {
            this.edcAmphurList = res.data;
        });
    }

    getThambolList() {
        this.thambolService.listSelect().subscribe(res => {
            this.edcThambolList = res.data;
        });
    }

    resetAmphur() {
        this.getAmphurList();
        this.postcodeForm.get('postcodeGroup.amphurId').reset();
        this.postcodeForm.get('postcodeGroup.amphurNameTh').reset();
        this.resetThambol();
    }

    resetThambol() {
        this.getThambolList();
        this.postcodeForm.get('postcodeGroup.thambolId').reset();
        this.postcodeForm.get('postcodeGroup.thambolNameTh').reset();
    }
}
