import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PostcodeRoutingModule} from './postcode-routing.module';
import {AddComponent} from './add/add.component';
import {ViewComponent} from './view/view.component';
import {ListComponent} from './list/list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {TypeaheadModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../../../shared/shared.module';
import {ImportComponent} from "../postcode/import/import.component";

@NgModule({
    declarations: [AddComponent, ViewComponent, ListComponent, ImportComponent],
  imports: [
    CommonModule,
    PostcodeRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    BsDatepickerModule,
    TypeaheadModule,
    SharedModule
  ]
})
export class PostcodeModule {
}
