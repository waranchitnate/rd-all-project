import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {EdcAmphurModel} from '../../../models/edcAmphur.model';
import {EdcThambolModel} from '../../../models/edcThambol.model';
import {ThambolService} from '../../thambol/edcThambol.service';
import {AmphurService} from '../../amphur/edcAmphur.service';
import {ProvinceService} from '../../province/edcProvince.service';
import {PostcodeService} from '../edcPostcode.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    edcProvinceList = <EdcProvinceModel[]>[];
    edcAmphurList = <EdcAmphurModel[]>[];
    edcThambolList = <EdcThambolModel[]>[];

    items: FormArray;
    postcodeForm: FormGroup;

    private selectedOptionAmphur: EdcAmphurModel;
    private selectedOptionProvince: EdcProvinceModel;
    private selectedOptionThambol: EdcThambolModel;

    constructor(
        private thambolService: ThambolService,
        private amphurService: AmphurService,
        private  provinceService: ProvinceService,
        private  postcodeService: PostcodeService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.getProvinceList();
        this.getAmphurList();
        this.getThambolList();
        this.initialForm();
    }

    onSubmit() {
        console.log(this.postcodeForm.value);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลรหัสไปรษณีย์ใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.postcodeService.save(this.postcodeForm.value.postcodeGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลรหัสไปรษณีย์สำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/postcode');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสไปรษณีย์' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลรหัสไปรษณีย์ยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/postcode');
                sub.unsubscribe();
            }
        });
    }

    initialForm() {
        this.postcodeForm = new FormGroup({
            postcodeGroup: new FormGroup({
                'postcode': new FormControl(null, Validators.required),
                'provinceId': new FormControl(null),
                'provinceNameTh': new FormControl(null, Validators.required),
                'amphurId': new FormControl(null),
                'amphurNameTh': new FormControl(null, Validators.required),
                'thambolId': new FormControl(null),
                'thambolNameTh': new FormControl(null, Validators.required),
                'comment': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.postcodeForm.reset();
        this.resetAmphur();
        this.resetThambol();
    }

    onSelectProvince(event: TypeaheadMatch): void {
        console.log(' this.edcAmphurList ==' + this.edcAmphurList);
        console.log('this.edcThambolList ==' + this.edcThambolList);
        this.selectedOptionProvince = event.item;
        this.postcodeForm.get('postcodeGroup.provinceId').setValue(this.selectedOptionProvince.id);
        this.edcAmphurList = this.edcAmphurList.filter(res => res.provinceId === this.selectedOptionProvince.id);
        this.edcThambolList = this.edcThambolList.filter(res => res.provinceId === this.selectedOptionProvince.id);
        // console.log(this.selectedOptionProvince.provinceCode + ' this.edcAmphurList ==' + this.edcAmphurList);
        // console.log('this.edcThambolList ==' + this.edcThambolList);
    }

    onSelectAmphur(event: TypeaheadMatch): void {
        this.selectedOptionAmphur = event.item;
        this.postcodeForm.get('postcodeGroup.amphurId').setValue(this.selectedOptionAmphur.id);
        if (this.postcodeForm.get('postcodeGroup.provinceId') != null) {
            this.postcodeForm.get('postcodeGroup.provinceId').setValue(this.selectedOptionAmphur.provinceId);
            this.postcodeForm.get('postcodeGroup.provinceNameTh').setValue(this.selectedOptionAmphur.edcProvinceEntity.provinceNameTh);
            this.postcodeForm.get('postcodeGroup.provinceId').setValue(this.selectedOptionAmphur.edcProvinceEntity.id);
        }
        this.edcThambolList = this.edcThambolList.filter(res => res.amphurId === this.selectedOptionAmphur.id);
        console.log(this.edcThambolList);
    }

    onSelectThambol(event: TypeaheadMatch): void {
        this.selectedOptionThambol = event.item;
        this.postcodeForm.get('postcodeGroup.thambolId').setValue(this.selectedOptionThambol.id);
        if (this.postcodeForm.get('postcodeGroup.provinceId') != null) {
            this.postcodeForm.get('postcodeGroup.provinceId').setValue(this.selectedOptionThambol.edcProvinceEntity.id);
            this.postcodeForm.get('postcodeGroup.provinceNameTh').setValue(this.selectedOptionThambol.edcProvinceEntity.provinceNameTh);
            this.postcodeForm.get('postcodeGroup.provinceId').setValue(this.selectedOptionThambol.edcProvinceEntity.id);
        }
        if (this.postcodeForm.get('postcodeGroup.amphurId') != null) {
            this.postcodeForm.get('postcodeGroup.amphurId').setValue(this.selectedOptionThambol.edcAmphurEntity.id);
            this.postcodeForm.get('postcodeGroup.amphurNameTh').setValue(this.selectedOptionThambol.edcAmphurEntity.amphurNameTh);
        }
    }

    getProvinceList() {
        this.provinceService.listSelect().subscribe(res => {
            this.edcProvinceList = res.data;
        });
    }

    getAmphurList() {
        this.amphurService.listSelect().subscribe(res => {
            this.edcAmphurList = res.data;
        });
    }

    getThambolList() {
        this.thambolService.listSelect().subscribe(res => {
            this.edcThambolList = res.data;
        });
    }

    resetAmphur() {
        this.getAmphurList();
        this.postcodeForm.get('postcodeGroup.amphurId').reset();
        this.postcodeForm.get('postcodeGroup.amphurNameTh').reset();
        this.resetThambol();
    }

    resetThambol() {
        this.getThambolList();
        this.postcodeForm.get('postcodeGroup.thambolId').reset();
        this.postcodeForm.get('postcodeGroup.thambolNameTh').reset();
    }
}
