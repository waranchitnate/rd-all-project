import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {EdcCrossBorderIncomeModel} from '../../models/edcCrossborderIncome.model';
import {EdcCrossborderIncomeRateModel} from '../../models/edcCrossborderIncomeRateModel.model';
import {ResponseEtl} from '../../models/responseEtl.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class CrossBorderIncomeService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcCrossBorderIncomeModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcCrossBorderIncomeModel[]>>>this.http.post(this.url + 'crossIncomeTypes', request);
    }

    listSelectIncome(): Observable<ResponseEtl<EdcCrossBorderIncomeModel[]>> {
        return <Observable<ResponseEtl<EdcCrossBorderIncomeModel[]>>>this.http.get(this.url + 'crossIncomeTypes');
    }

    save(request: EdcCrossBorderIncomeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'crossIncomeType/add', request);
    }

    update(request: EdcCrossBorderIncomeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'crossIncomeType/update', request);
    }

    delete(request: EdcCrossBorderIncomeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'crossIncomeType/delete', request);
    }

    // listRate(request: any): Observable<ResponseEtl<EdcCrossborderIncomeRateModel[]>> {
    //     console.log(request);
    //     return <Observable<ResponseEtl<EdcCrossborderIncomeRateModel[]>>>this.http.post(this.url + 'incomeTypeRates', request);
    // }

    saveRate(request: EdcCrossborderIncomeRateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'incomeTypeRate/add', request);
    }

    updateRate(request: EdcCrossborderIncomeRateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'incomeTypeRate/update', request);
    }

    // listConfig(request: any): Observable<ResponseEtl<EdcConfigCodeModel[]>> {
    //     console.log(request);
    //     return <Observable<ResponseEtl<EdcConfigCodeModel[]>>>this.http.post(this.url + 'configCodes', request);
    // }
}
