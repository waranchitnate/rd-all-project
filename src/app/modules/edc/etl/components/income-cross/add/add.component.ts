import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';

import {ModalService} from '../../../../../../core/services/modal.service';
import {CrossBorderIncomeService} from '../edcCrossBorderIncome.service';
import {ConfigCodeService} from './../../../edcConfigCode.service';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Router} from '@angular/router';
import {EdcSectionModel} from '../../../models/edcSection.model';
import {SectionService} from '../../section/edcSection.service';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  ConfigCodeList = <EdcConfigCodeModel[]>[];
  EdcFilterCodeList = <EdcConfigCodeModel[]>[];
  TaxRateTypeList = <EdcConfigCodeModel[]>[];
  PndList = <EdcConfigCodeModel[]>[];
  SectionList = <EdcSectionModel[]>[];

  minDate: Date;
  incomeForm: FormGroup;
  items: FormArray;
  incomeFormTemp: any;
  count: number;

  constructor(
    private modal: ModalService,
    private crossBorderIncomeService: CrossBorderIncomeService,
    private configCodeService: ConfigCodeService,
    private sectionService: SectionService,
    private router: Router,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    this.count = 0;
    this.initialForm();
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.getSectionList({});
    this.getList({});
    this.items = this.incomeForm.get('tableGroup.tableList') as FormArray;
    this.initialName();
  }

  showDetail(index, item) {
    console.log((this.count++) + ' : ' + index + ' : ' + item);
  }

  getList(param) {
    this.configCodeService.listConfig(param).subscribe(result => {

      this.ConfigCodeList = result.data;
      this.EdcFilterCodeList = this.ConfigCodeList.filter(res => res.type === 'TAXPAYER_TYPE');
      this.TaxRateTypeList = this.ConfigCodeList.filter(res => res.type === 'RATE_TYPE');
      this.PndList = this.ConfigCodeList.filter(res => res.type === 'PND');

      // console.log(this.EdcFilterCodeList);
    });
  }

  getSectionList(param) {
    this.sectionService.listSelect().subscribe(result => {
      console.log('result');
      console.log(result.data);
      this.SectionList = result.data;
      console.log(this.SectionList);
    });
  }

  findSectionListByPnd(pnd) {
    if (this.SectionList != null && pnd != null) {
      return this.SectionList.filter(item => item.pnd === pnd);
    } else {
    }
    return null;
  }

  initialForm() {
    this.incomeForm = new FormGroup({
      incomeGroup: new FormGroup({
        'incomeCode': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]+$')]),
        'incomeNameTh': new FormControl(null, Validators.required),
        'incomeNameEn': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'effectiveDate': new FormControl(null, Validators.required),
        'endDate': new FormControl(null)
      }),
      tableGroup: new FormGroup({
        tableList: new FormArray([])
      })
    });
    this.incomeFormTemp = this.incomeForm.get('tableGroup.tableList');
  }

  createItem(nameAgent, namePayer): FormGroup {
    return new FormGroup({
      'agentTaxPayerType': new FormControl(nameAgent),
      'taxPayerType': new FormControl(namePayer),
      'rateType': new FormControl(0),
      'whtRate': new FormControl(),
      'pnd': new FormControl(),
      'sectionId': new FormControl()
    });
  }

  resetForm() {
    this.incomeForm.reset();
    this.items.controls.splice(0);
    this.initialName();
  }

  // resetWhtRate() {
  //   console.log('pass resetWhtRate' + index);
  //   if (this.incomeForm.value.tableGroup.tableList[index].rateType !== '3') {
  //     console.log('pass resetWhtRate rateType !== 3');
  //     // this.incomeForm.value.tableGroup.tableList[index].whtRate = null;
  //     // this.incomeForm.get('tableGroup.tableList' + [index] + '.whtRate').setValue(null);
  //     // console.log(this.incomeForm.get('tableGroup.tableList')[index].rateType + ' : ' + this.incomeForm.get('tableGroup.tableList')[index].whtRate);
  //   }
  //   return null;
  // }

  onSubmit() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.incomeForm.valid) {
        this.crossBorderIncomeService.save(this.incomeForm.value.incomeGroup).subscribe(
          response => {
            console.log(response.data.id);
            for (let i = 0; i < this.incomeForm.value.tableGroup.tableList.length; i++) {
              this.incomeForm.value.tableGroup.tableList[i].crossIncomeId = response.data.id;
              console.log(this.incomeForm.value.tableGroup.tableList[i]);
            }

            this.crossBorderIncomeService.saveRate(this.incomeForm.value.tableGroup.tableList).subscribe(
              responseRate => {
                this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศสำเร็จ');
                const modalHide = this.modal.modalService.onHide.subscribe(e => {
                  modalHide.unsubscribe();
                });
                this.router.navigateByUrl('etl/cross-border-income-type');
              },
              err => {
                this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
              }
            );
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสประเภทรายได้' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.router.navigateByUrl('/etl/cross-border-income-type');
        sub.unsubscribe();
      }
    });
  }

  initialName() {
    for (let i = 1; i <= 16; i++) {
      let agentTaxPayerType;
      let taxPayerType;
      if (i === 1) {
        agentTaxPayerType = 100;
        taxPayerType = 100;
      }
      if (i === 2) {
        agentTaxPayerType = 100;
        taxPayerType = 200;
      }
      if (i === 3) {
        agentTaxPayerType = 100;
        taxPayerType = 300;
      }
      if (i === 4) {
        agentTaxPayerType = 100;
        taxPayerType = 400;
      }
      if (i === 5) {
        agentTaxPayerType = 200;
        taxPayerType = 100;
      }
      if (i === 6) {
        agentTaxPayerType = 200;
        taxPayerType = 200;
      }
      if (i === 7) {
        agentTaxPayerType = 200;
        taxPayerType = 300;
      }
      if (i === 8) {
        agentTaxPayerType = 200;
        taxPayerType = 400;
      }
      if (i === 9) {
        agentTaxPayerType = 300;
        taxPayerType = 100;
      }
      if (i === 10) {
        agentTaxPayerType = 300;
        taxPayerType = 200;
      }
      if (i === 11) {
        agentTaxPayerType = 300;
        taxPayerType = 300;
      }
      if (i === 12) {
        agentTaxPayerType = 300;
        taxPayerType = 400;
      }
      if (i === 13) {
        agentTaxPayerType = 400;
        taxPayerType = 100;
      }
      if (i === 14) {
        agentTaxPayerType = 400;
        taxPayerType = 200;
      }
      if (i === 15) {
        agentTaxPayerType = 400;
        taxPayerType = 300;
      }
      if (i === 16) {
        agentTaxPayerType = 400;
        taxPayerType = 400;
      }
      this.items.push(this.createItem(agentTaxPayerType, taxPayerType));
    }
  }
}

