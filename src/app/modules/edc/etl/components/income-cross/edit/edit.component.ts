import {Component, OnInit} from '@angular/core';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {CrossBorderIncomeService} from '../edcCrossBorderIncome.service';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {EdcCrossBorderIncomeModel} from '../../../models/edcCrossborderIncome.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcSectionModel} from '../../../models/edcSection.model';
import {SectionService} from '../../section/edcSection.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  state: Observable<EdcCrossBorderIncomeModel>;
  ConfigCodeList = <EdcConfigCodeModel[]>[];
  EdcFilterCodeList = <EdcConfigCodeModel[]>[];
  TaxRateTypeList = <EdcConfigCodeModel[]>[];
  PndList = <EdcConfigCodeModel[]>[];
  SectionList = <EdcSectionModel[]>[];

  minDate: Date;
  incomeForm: FormGroup;
  edcCrossBorderIncome: EdcCrossBorderIncomeModel;
  items: FormArray;
  incomeFormTemp: any;

  // tableForm: FormGroup;
  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private modal: ModalService,
    private sectionService: SectionService,
    private crossBorderIncomeService: CrossBorderIncomeService,
    private configCodeService: ConfigCodeService
  ) {
  }

  ngOnInit() {
    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe(e => {
      this.edcCrossBorderIncome = e;

    });

    if (this.edcCrossBorderIncome.id === undefined) {
      this.router.navigateByUrl('/etl/cross-border-income-type');
    }
    this.minDate = new Date(this.edcCrossBorderIncome.effectiveDate);
    this.initialForm();
    this.getList({});
    this.getSectionList({});
    this.items = this.incomeForm.get('tableGroup.tableList') as FormArray;
    this.initialName();
  }

  initialName() {
    for (let i = 0; i < 16; i++) {
      let agentTaxPayerType;
      let taxPayerType;
      let rateType;
      let whtRate;
      let pnd;
      let oldId;
      let crossIncomeId;
      let sectionId;

      oldId = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].id;
      crossIncomeId = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].crossIncomeId;
      agentTaxPayerType = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].agentTaxPayerType;
      taxPayerType = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].taxPayerType;
      rateType = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].rateType;
      whtRate = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].whtRate;
      pnd = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].pnd;
      sectionId = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].sectionId;

      this.items.push(this.createItem(agentTaxPayerType, taxPayerType, rateType, whtRate, pnd, oldId, crossIncomeId, sectionId));
    }
  }

  getSectionList(param) {
    this.sectionService.listSelect().subscribe(result => {
      console.log('result');
      console.log(result.data);
      this.SectionList = result.data;
      console.log(this.SectionList);
    });
  }

  getList(param) {
    this.configCodeService.listConfig(param).subscribe(result => {
      this.ConfigCodeList = result.data;
      this.EdcFilterCodeList = this.ConfigCodeList.filter(res => res.type === 'TAXPAYER_TYPE');
      this.TaxRateTypeList = this.ConfigCodeList.filter(res => res.type === 'RATE_TYPE');
      this.PndList = this.ConfigCodeList.filter(res => res.type === 'PND');
    });
  }

  initialForm() {
    this.incomeForm = new FormGroup({
      incomeGroup: new FormGroup({
        'incomeCode': new FormControl(this.edcCrossBorderIncome.incomeCode, [Validators.required, Validators.pattern('^[0-9]+$')]),
        'incomeNameTh': new FormControl(this.edcCrossBorderIncome.incomeNameTh, Validators.required),
        'incomeNameEn': new FormControl(this.edcCrossBorderIncome.incomeNameEn, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'effectiveDate': new FormControl(this.edcCrossBorderIncome.effectiveDate, Validators.required),
        'endDate': new FormControl(this.edcCrossBorderIncome.endDate),
        'createdDate': new FormControl(this.edcCrossBorderIncome.createdDate),
        'createdBy': new FormControl(this.edcCrossBorderIncome.createdBy),
      }),
      tableGroup: new FormGroup({
        tableList: new FormArray([])
      })
    });
    this.incomeFormTemp = this.incomeForm.get('tableGroup.tableList');
  }

  createItem(agentTaxPayerType, taxPayerType, rateType, whtRate, pnd, oldId, crossIncomeId, sectionId): FormGroup {
    return new FormGroup({
      'agentTaxPayerType': new FormControl(agentTaxPayerType),
      'taxPayerType': new FormControl(taxPayerType),
      'rateType': new FormControl(rateType),
      'whtRate': new FormControl(whtRate),
      'pnd': new FormControl(pnd),
      'id': new FormControl(oldId),
      'crossIncomeId': new FormControl(crossIncomeId),
      'sectionId': new FormControl(sectionId)
    });
  }

  resetForm() {
    // this.incomeForm.reset();
    // this.incomeForm.get('tableGroup').reset();
    this.incomeForm.get('incomeGroup.incomeNameTh').reset();
    this.incomeForm.get('incomeGroup.incomeNameEn').reset();
    this.incomeForm.get('incomeGroup.effectiveDate').reset();
    this.incomeForm.get('incomeGroup.endDate').reset();
    this.incomeForm.updateValueAndValidity();
    this.items.controls.splice(0);
    this.resetInitialForm();
  }

  resetInitialForm() {
    for (let i = 0; i < 16; i++) {
      // console.log(this.edcCrossBorderIncome.edcIncomeTypeRateList[i]);
      let agentTaxPayerType;
      let taxPayerType;
      let oldId;
      let crossIncomeId;
      let sectionId;
      oldId = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].id;
      crossIncomeId = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].crossIncomeId;
      agentTaxPayerType = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].agentTaxPayerType;
      taxPayerType = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].taxPayerType;
      sectionId = this.edcCrossBorderIncome.edcIncomeTypeRateList[i].sectionId;

      this.items.push(this.createItem(agentTaxPayerType, taxPayerType, 0, null, null, null, null, null));
    }
  }

  onSubmit() {
    this.incomeForm.value.incomeGroup.id = this.edcCrossBorderIncome.id;

    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.incomeForm.valid) {
        this.crossBorderIncomeService.update(this.incomeForm.value.incomeGroup).subscribe(
          response => {
            // console.log(response.data.id);

            // for (let i = 0; i < this.incomeForm.value.tableGroup.tableList.length; i++) {
            //     console.log(this.incomeForm.value.tableGroup.tableList[i]);
            // }
            this.crossBorderIncomeService.updateRate(this.incomeForm.value.tableGroup.tableList).subscribe(
              responseRate => {
                this.modal.openModal('แจ้งเตือน ', 'แก้ไขข้อมูลประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศสำเร็จ');
                const modalHide = this.modal.modalService.onHide.subscribe(e => {
                  modalHide.unsubscribe();
                });
                this.router.navigateByUrl('/etl/cross-border-income-type');
              },
              err => {
                this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
              }
            );

          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.router.navigateByUrl('/etl/cross-border-income-type');
        sub.unsubscribe();
      }
    });
  }

  findSectionListByPnd(pnd) {
    if (this.SectionList != null && pnd != null) {
      return this.SectionList.filter(item => item.pnd === pnd);
    } else {
    }
    return null;
  }
}
