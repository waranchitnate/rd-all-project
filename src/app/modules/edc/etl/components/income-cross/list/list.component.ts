import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';

import {CrossBorderIncomeService} from '../edcCrossBorderIncome.service';
import {EdcCrossBorderIncomeModel} from '../../../models/edcCrossborderIncome.model';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcCrossborderIncomeList = <EdcCrossBorderIncomeModel[]>[];
    incomeForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private crossBorderIncomeService: CrossBorderIncomeService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.incomeForm.value);
        this.getList(this.incomeForm.value.incomeGroup);
    }

    initialForm() {
        this.incomeForm = new FormGroup({
            incomeGroup: new FormGroup({
                'incomeCode': new FormControl(null),
                'incomeNameTh': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.incomeForm.reset();
        this.initialForm();
        this.getList(this.incomeForm.value.incomeGroup);
    }

    getList(param) {
        this.crossBorderIncomeService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcCrossborderIncomeList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.incomeForm.value.incomeGroup.page = page;
        // console.log(this.incomeForm.value.incomeGroup);
        this.getList(this.incomeForm.value.incomeGroup);
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/cross-border-income-type/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.crossBorderIncomeService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.incomeForm.value.incomeGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.incomeForm.value.incomeGroup.pageSize = pageSize;
        this.getList(this.incomeForm.value.incomeGroup);
    }

}
