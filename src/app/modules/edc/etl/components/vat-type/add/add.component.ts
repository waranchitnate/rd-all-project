import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {VatTypeService} from '../edcVatType.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcVatTypeModel} from '../../../models/edcVatType.model';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    edcVatTypeList = <EdcVatTypeModel[]>[];
    vatTypeForm: FormGroup;
    minDate: Date;

    constructor(
        private vatTypeService: VatTypeService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.initialForm();
        this.getlist();
    }

    onSubmit() {
        console.log(this.vatTypeForm.value);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลรายการนำส่งภาษีมูลค่าเพิ่มใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.vatTypeService.save(this.vatTypeForm.value.vatTypeGroup).subscribe(
                    response => {
                        console.log(response);
                        this.resetForm();
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลรายการนำส่งภาษีมูลค่าเพิ่มสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/vat-type');
                    },
                    err => {
                      if (err.error.errorCode === 'ST098') {
                        this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'ข้อมูลรายการนำส่งภาษีมูลค่าเพิ่ม' + this.alertText.getTail);
                      } else {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                      }
                    }
                );
                sub.unsubscribe();
            }
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลรายการนำส่งภาษีมูลค่าเพิ่มยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/vat-type');
                sub.unsubscribe();
            }
        });
    }

    getlist() {
        this.vatTypeService.listVatType().subscribe(result => {
            console.log(result.data);
            this.edcVatTypeList = result.data;
        });
    }

    initialForm() {
        this.vatTypeForm = new FormGroup({
            vatTypeGroup: new FormGroup({
                'effectiveDate': new FormControl(null, Validators.required),
                'vatTypeId': new FormControl(null),
            })
        });
    }

    resetForm() {
        this.vatTypeForm.reset();
    }

}
