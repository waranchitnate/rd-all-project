import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcPpVatTypeModel} from '../../models/EdcPpVatType.model';
import {EdcVatTypeModel} from '../../models/edcVatType.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class VatTypeService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcPpVatTypeModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcPpVatTypeModel[]>>>this.http.post(this.url + 'pp-vat-types', request);
    }

    listVatType(): Observable<ResponseEtl<EdcVatTypeModel[]>> {
        return <Observable<ResponseEtl<EdcVatTypeModel[]>>>this.http.get(this.url + 'vat-types');
    }
    save(request: EdcPpVatTypeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'pp-vat-type/add', request);
    }

    update(request: EdcPpVatTypeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'pp-vat-type/update', request);
    }

    delete(request: EdcPpVatTypeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'pp-vat-type/delete', request);
    }
}
