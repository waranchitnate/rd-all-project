import {Component, OnInit} from '@angular/core';
import {EdcVatTypeModel} from '../../../models/edcVatType.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {VatTypeService} from '../edcVatType.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {EdcPpVatTypeModel} from '../../../models/EdcPpVatType.model';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    state: Observable<EdcPpVatTypeModel>;
    edcVatType: EdcPpVatTypeModel;
    edcVatTypeList = <EdcVatTypeModel[]>[];
    vatTypeForm: FormGroup;
    minDate: Date;

    constructor(
        private vatTypeService: VatTypeService,
        private modal: ModalService,
        private router: Router,
        public activatedRoute: ActivatedRoute,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcVatType = e;
        });
        if (this.edcVatType.id === undefined) {
            this.router.navigateByUrl('/etl/vat-type');
        }
        this.minDate = new Date(this.edcVatType.effectiveDate);
        this.initialForm();
        this.getlist();
    }

    onSubmit() {
        console.log(this.vatTypeForm.value);

        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของรายการนำส่งภาษีมูลค่าเพิ่มใช่หรือไม่';

        this.vatTypeForm.value.vatTypeGroup.id = this.edcVatType.id;
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.vatTypeService.update(this.vatTypeForm.value.vatTypeGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลรายการนำส่งภาษีมูลค่าเพิ่มสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/vat-type');
                    },
                    err => {
                      if (err.error.errorCode === 'ST098') {
                        this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'ข้อมูลรายการนำส่งภาษีมูลค่าเพิ่ม' + this.alertText.getTail);
                      } else {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                      }
                    }
                );
                sub.unsubscribe();
            }
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลรายการนำส่งภาษีมูลค่าเพิ่มยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/vat-type');
                sub.unsubscribe();
            }
        });
    }

    getlist() {
        this.vatTypeService.listVatType().subscribe(result => {
            console.log(result.data);
            this.edcVatTypeList = result.data;
        });
    }

    initialForm() {
        this.vatTypeForm = new FormGroup({
            vatTypeGroup: new FormGroup({
                'effectiveDate': new FormControl(this.edcVatType.effectiveDate, Validators.required),
                'vatTypeId': new FormControl(this.edcVatType.vatTypeId),
                'createdDate': new FormControl(this.edcVatType.createdDate),
                'createdBy': new FormControl(this.edcVatType.createdBy)
            })
        });
    }

    resetForm() {
        this.vatTypeForm.reset();
    }

}
