import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {EdcPpVatTypeModel} from '../../../models/EdcPpVatType.model';
import {VatTypeService} from '../edcVatType.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {ConfigCodeService} from '../../../edcConfigCode.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    private ConfigCodeList = <EdcConfigCodeModel[]>[];
    private PpList = <EdcConfigCodeModel[]>[];
    private vatTypepPMap = new Map();
    edcVatTypeList = <EdcPpVatTypeModel[]>[];
    vatTypeForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private configCodeService: ConfigCodeService,
        private vatTypeService: VatTypeService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.getPpList();
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.vatTypeForm.value);
        this.getList(this.vatTypeForm.value.vatTypeGroup);
    }

    initialForm() {
        this.vatTypeForm = new FormGroup({
            vatTypeGroup: new FormGroup({
                'effectiveDate': new FormControl(null)
            })
        });
    }

    getPpList() {
        this.configCodeService.listConfig({}).subscribe(result => {
            this.ConfigCodeList = result.data;
            this.PpList = this.ConfigCodeList.filter(res => res.type === 'PP');
            for (let i = 0; i <= this.PpList.length - 1; i++) {
                this.vatTypepPMap.set(this.PpList[i].code, this.PpList[i].description);
            }
            // console.log(this.vatTypepPMap);
        });
    }

    resetForm() {
        this.vatTypeForm.reset();
        this.initialForm();
        this.getList(this.vatTypeForm.value.vatTypeGroup);
    }

    getList(param) {
        this.vatTypeService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcVatTypeList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.vatTypeForm.value.vatTypeGroup.page = page;
        // console.log(this.vatTypeForm.value.vatTypeGroup);
        this.getList(this.vatTypeForm.value.vatTypeGroup);
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/vat-type/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลรายการนำส่งภาษีมูลค่าเพิ่มใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.vatTypeService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.vatTypeForm.value.vatTypeGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลรายการนำส่งภาษีมูลค่าเพิ่มสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.vatTypeForm.value.vatTypeGroup.pageSize = pageSize;
        this.getList(this.vatTypeForm.value.vatTypeGroup);
    }
}
