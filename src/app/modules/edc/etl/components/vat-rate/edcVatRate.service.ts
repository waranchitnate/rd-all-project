import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {EdcVatRateModel} from '../../models/edcVatRate.model';
import {ResponseEtl} from '../../models/responseEtl.model';
import {environment} from '../../../../../../environments/environment';


@Injectable()
export class VatRateService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcVatRateModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcVatRateModel[]>>>this.http.post(this.url + 'vatRates', request);
    }

    save(request: EdcVatRateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'vatRate/add', request);
    }

    update(request: EdcVatRateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'vatRate/update', request);
    }

    delete(request: EdcVatRateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'vatRate/delete', request);
    }
}
