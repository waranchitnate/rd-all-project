import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VatRateRoutingModule } from './vat-rate-routing.module';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {BsDatepickerModule} from 'ngx-bootstrap-th';
import {SharedModule} from '../../../../../shared/shared.module';

@NgModule({
  declarations: [AddComponent, EditComponent, ListComponent],
  imports: [
    CommonModule,
    VatRateRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    BsDatepickerModule,
    SharedModule
  ]
})
export class VatRateModule { }
