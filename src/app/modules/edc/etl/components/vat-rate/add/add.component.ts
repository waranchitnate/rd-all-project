import {VatRateService} from '../edcVatRate.service';

import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Router} from '@angular/router';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    vatRateForm: FormGroup;
    minDate: Date;

    constructor(
        private modal: ModalService,
        private vatRateService: VatRateService,
        private router: Router,
        private alertText: AlertTextModule

    ) {
    }

    ngOnInit() {
        this.initialForm();
        this.minDate = new Date();
        this.minDate.setDate(this.minDate.getDate() + 1);
    }

    initialForm() {
        this.vatRateForm = new FormGroup({
            vatRateGroup: new FormGroup({
                'vatName': new FormControl(null, Validators.required),
                'vatRate': new FormControl(null, [Validators.required, Validators.maxLength(99.99)]),
                'effectiveDate': new FormControl(null, Validators.required),
                'endDate': new FormControl(null),
                'remark': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.vatRateForm.reset();
    }

    onSubmit() {
        console.log(this.vatRateForm.value);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลอัตราภาษีมูลค่าเพิ่มใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.vatRateForm.valid) {
                this.vatRateService.save(this.vatRateForm.value.vatRateGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลอัตราภาษีมูลค่าเพิ่มสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/vat-rate');
                    },
                    err => {
                      console.log('ddd');
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'ข้อมูล' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลอัตราภาษีมูลค่าเพิ่มยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                console.log(this.vatRateForm.value.vatRateGroup);
                this.router.navigateByUrl('/etl/vat-rate');
                sub.unsubscribe();
            }
        });
    }
}
