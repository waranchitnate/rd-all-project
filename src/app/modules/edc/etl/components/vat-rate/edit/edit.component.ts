import {VatRateService} from '../edcVatRate.service';
import {EdcVatRateModel} from '../../../models/edcVatRate.model';

import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';

import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    state: Observable<EdcVatRateModel>;
    edcVatRate: EdcVatRateModel;
    vatRateForm: FormGroup;
    minDate: Date;

    constructor(
        public activatedRoute: ActivatedRoute,
        public router: Router,
        private modal: ModalService,
        private vatRateService: VatRateService,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcVatRate = e;
        });
        if (this.edcVatRate.id === undefined) {
            this.router.navigateByUrl('/etl/vat-rate');
        }
        this.minDate = new Date(this.edcVatRate.effectiveDate);
        this.initialForm();
    }

    initialForm() {
        this.vatRateForm = new FormGroup({
            vatRateGroup: new FormGroup({
                'vatName': new FormControl(this.edcVatRate.vatName, Validators.required),
                'vatRate': new FormControl(this.edcVatRate.vatRate, [Validators.required, Validators.maxLength(99.99)]),
                'effectiveDate': new FormControl(this.edcVatRate.effectiveDate, Validators.required),
                'endDate': new FormControl(this.edcVatRate.endDate),
                'remark': new FormControl(this.edcVatRate.remark),
                'createdDate': new FormControl(this.edcVatRate.createdDate),
                'createdBy': new FormControl(this.edcVatRate.createdBy)
            })
        });
    }

    resetForm() {
        this.vatRateForm.reset();
    }

    onSubmit() {
        console.log(this.vatRateForm.value);

        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลอัตราภาษีมูลค่าเพิ่มใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.vatRateForm.value.vatRateGroup.id = this.edcVatRate.id;
                this.vatRateService.update(this.vatRateForm.value.vatRateGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลอัตราภาษีมูลค่าเพิ่มสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });

                        this.router.navigateByUrl('/etl/vat-rate');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'วันที่มีผล' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลอัตราภาษีมูลค่าเพิ่มยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                console.log(this.vatRateForm.value.vatRateGroup);
                this.router.navigateByUrl('/etl/vat-rate');
                sub.unsubscribe();
            }
        });
    }
}
