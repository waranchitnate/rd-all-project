import {VatRateService} from '../edcVatRate.service';
import {EdcVatRateModel} from '../../../models/edcVatRate.model';

import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';


@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcVatRateModel = <EdcVatRateModel[]>[];
    vatRateForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private vatRateService: VatRateService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.vatRateForm.value);
        this.getList(this.vatRateForm.value.vatRateGroup);
    }

    initialForm() {
        this.vatRateForm = new FormGroup({
            vatRateGroup: new FormGroup({
                'vatName': new FormControl(null, Validators.required),
                'vatRate': new FormControl(null, Validators.required)
            })
        });
    }

    resetForm() {
        this.vatRateForm.reset();
        this.initialForm();
        this.getList(this.vatRateForm.value.vatRateGroup);
    }

    getList(param) {
        this.vatRateService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcVatRateModel = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.vatRateForm.value.vatRateGroup.page = page;
        // console.log(this.vatRateForm.value.vatRateGroup);
        this.getList(this.vatRateForm.value.vatRateGroup);
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/vat-rate/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบอัตราภาษีมูลค่าเพิ่มใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.vatRateService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.vatRateForm.value.vatRateGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลอัตราภาษีมูลค่าเพิ่มสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.vatRateForm.value.vatRateGroup.pageSize = pageSize;
        this.getList(this.vatRateForm.value.vatRateGroup);
    }
}
