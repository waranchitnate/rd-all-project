import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ProvinceService} from '../../province/edcProvince.service';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {AmphurService} from '../edcAmphur.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Router} from '@angular/router';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    amphurForm: FormGroup;
    minDate: Date;
    edcProvinceList = <EdcProvinceModel[]>[];
    private selectedOption: EdcProvinceModel;
    private popUpName = '';

    constructor(
        private modal: ModalService,
        private amphurService: AmphurService,
        private provinceService: ProvinceService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.getProvinceList();
        this.initialForm();
    }

    initialForm() {
        this.amphurForm = new FormGroup({
            amphurGroup: new FormGroup({
                'amphurCode': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]+$')]),
                'amphurNameTh': new FormControl(null, Validators.required),
                'amphurNameEn': new FormControl(null),
                'comment': new FormControl(null),
                'provinceId': new FormControl(null),
                'provinceNameTh': new FormControl(null, Validators.required)
            })
        });
    }

    getProvinceList() {
        this.provinceService.listSelect().subscribe(res => {
            this.edcProvinceList = res.data;
        });
    }

    onSelect(event: TypeaheadMatch): void {
        this.selectedOption = event.item;
        this.amphurForm.get('amphurGroup.provinceId').setValue(this.selectedOption.id);
    }

    resetForm() {
        this.amphurForm.reset();
    }

    onSubmit() {
        console.log(this.amphurForm.value);

        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลอำเภอใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.amphurForm.valid) {
                this.amphurService.save(this.amphurForm.value.amphurGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลอำเภอสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/amphur');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสอำเภอ' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลอำเภอยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/amphur');
                sub.unsubscribe();
            }
        });
    }
}
