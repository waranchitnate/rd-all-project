import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AmphurService} from '../edcAmphur.service';
import {ProvinceService} from '../../province/edcProvince.service';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {Observable} from 'rxjs';
import {EdcAmphurModel} from '../../../models/edcAmphur.model';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  state: Observable<EdcAmphurModel>;
  private edcAmphur: EdcAmphurModel;
  amphurForm: FormGroup;
  minDate: Date;
  edcProvinceList = <EdcProvinceModel[]>[];
  private selectedOption: EdcProvinceModel;

  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private modal: ModalService,
    private amphurService: AmphurService,
    private provinceService: ProvinceService,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe(e => {
      this.edcAmphur = e;
    });
    if (this.edcAmphur.id === undefined) {
      this.router.navigateByUrl('/etl/amphur');
    }
    this.getProvinceList();
    this.initialForm();
  }

  initialForm() {
    this.amphurForm = new FormGroup({
      amphurGroup: new FormGroup({
        'amphurCode': new FormControl(this.edcAmphur.amphurCode, [Validators.required, Validators.pattern('^[0-9]+$')]),
        'amphurNameTh': new FormControl(this.edcAmphur.amphurNameTh, Validators.required),
        'amphurNameEn': new FormControl(this.edcAmphur.amphurNameEn),
        'comment': new FormControl(this.edcAmphur.comment),
        'provinceId': new FormControl(this.edcAmphur.edcProvinceEntity.id),
        'provinceNameTh': new FormControl(this.edcAmphur.edcProvinceEntity.provinceNameTh, Validators.required),
        'createdDate': new FormControl(this.edcAmphur.createdDate),
        'createdBy': new FormControl(this.edcAmphur.createdBy)
      })
    });
  }

  getProvinceList() {
    this.provinceService.listSelect().subscribe(res => {
      this.edcProvinceList = res.data;
    });
  }

  onSelect(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.amphurForm.get('amphurGroup.provinceId').setValue(this.selectedOption.id);
  }

  resetForm() {
    this.amphurForm.reset();
  }

  onSubmit() {
    console.log(this.amphurForm.value);
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของอำเภอใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.amphurForm.valid) {
        this.amphurForm.value.amphurGroup.id = this.edcAmphur.id;
        this.amphurService.update(this.amphurForm.value.amphurGroup).subscribe(
          response => {
            console.log(response);
            this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลอำเภอสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('/etl/amphur');
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสอำเภอ' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    this.router.navigateByUrl('/etl/amphur');
  }
}
