import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcAmphurModel} from '../../models/edcAmphur.model';
import {environment} from '../../../../../../environments/environment';


@Injectable()
export class AmphurService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcAmphurModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcAmphurModel[]>>>this.http.post(this.url + 'amphurs', request);
    }

    listSelect(): Observable<ResponseEtl<EdcAmphurModel[]>> {
        return <Observable<ResponseEtl<EdcAmphurModel[]>>>this.http.get(this.url + 'amphurs');
    }

    save(request: EdcAmphurModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'amphur/add', request);
    }

  import(file: File): Observable<any> {
    const data = new FormData();
    data.append('file', file);
    console.log(data);
    return this.http.post(this.url + 'amphur/import', data);
  }

    update(request: EdcAmphurModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'amphur/update', request);
    }

    delete(request: EdcAmphurModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'amphur/delete', request);
    }
}
