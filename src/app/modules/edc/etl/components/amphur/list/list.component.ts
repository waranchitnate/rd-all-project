import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {EdcAmphurModel} from '../../../models/edcAmphur.model';
import {AmphurService} from '../edcAmphur.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {ProvinceService} from '../../province/edcProvince.service';
import {TypeaheadMatch} from 'ngx-bootstrap';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcAmphurList = <EdcAmphurModel[]>[];
    edcProvinceList = <EdcProvinceModel[]>[];
    private selectedOption: EdcProvinceModel;
    amphurForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private amphurService: AmphurService,
        private  provinceService: ProvinceService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getProvinceList();
        this.getList({});
        this.initialForm();
    }

    getProvinceList() {
        this.provinceService.listSelect().subscribe(res => {
            this.edcProvinceList = res.data;
        });
        // console.log(this.edcProvinceList);
    }

    onSelect(event: TypeaheadMatch): void {
        this.selectedOption = event.item;
        this.amphurForm.get('amphurGroup.provinceId').setValue(this.selectedOption.id);
    }

    onSubmit() {
        // console.log(this.amphurForm.value);
        this.getList(this.amphurForm.value.amphurGroup);
    }

    initialForm() {
        this.amphurForm = new FormGroup({
            amphurGroup: new FormGroup({
                'amphurNameTh': new FormControl(null),
                'provinceId': new FormControl(null),
                'provinceNameTh': new FormControl(null),
                'provinceCode': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.amphurForm.reset();
        this.initialForm();
        this.getList(this.amphurForm.value.amphurGroup);
    }

    getList(param) {
        this.amphurService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcAmphurList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.amphurForm.value.amphurGroup.page = page;
        // console.log(this.amphurForm.value.amphurGroup);
        this.getList(this.amphurForm.value.amphurGroup);
    }

    onView(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/amphur/view', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'คุณต้องการลบอำเภอนี้ใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.amphurService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.amphurForm.value.amphurGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลอำเภอสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.amphurForm.value.amphurGroup.pageSize = pageSize;
        this.getList(this.amphurForm.value.amphurGroup);
    }
}
