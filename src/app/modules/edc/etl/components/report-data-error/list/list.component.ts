import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UserInfoModel} from '../../../../mft/models/userInfo.model';
import {DataTypeModel} from '../../../../mft/models/dataType.model';
import {ConfigCodeService} from '../../../../mft/edcConfigCode.service';
import {SenderTransferPushService} from '../../../../mft/components/sender-transfer-push/sender-transfer-push.service';
import {ReportService} from '../../../edcReport.service';
import * as moment from 'moment';
import {TypeaheadMatch} from 'ngx-bootstrap-th';
import {EdcDataTypeService} from '../../../../mft/components/data-type/edcDataType.service';
import { Router } from '@angular/router';
import {EdcProcessingModel} from '../../../models/edcProcessing.model';
import {EdcfinancialInstitutionModel} from '../../../models/edcFinancial-insitution.model';
import {FinancialInstitutionService} from '../../financial-institution/edcfinancial-institution.service';
import {EdcProcessingDetailModel} from '../../../models/edcProcessingDetail.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  reportComplexSearchForm: FormGroup;
  userInfoList = <UserInfoModel[]>[];
  dataTypeList = <DataTypeModel[]>[];
  financialList = <EdcfinancialInstitutionModel[]>[];
  edcReportComplexValidationList = <EdcProcessingDetailModel[]>[];
  edcReportComplexValidationListTemp = <EdcProcessingModel[]>[];
  minDate: Date;
  financialMap = new Map();
  edcCountryList: any;
  selectedOption: any;
  total: number;
  pageSize: number;
  page: number;

  constructor(
      private edcDataTypeService: EdcDataTypeService,
      private configCodeService: ConfigCodeService,
      private senderTransferPushService: SenderTransferPushService,
      private reportService: ReportService,
      private router: Router,
      private financialInstitutionService: FinancialInstitutionService,
  ) {
  }

  ngOnInit() {
    this.initialForm();
    this.getListDataType();
    this.getListSenderUser();
    this.getListFinancial();
    this.getReport({});
  }

  initialForm() {
    this.reportComplexSearchForm = new FormGroup({
      reportComplexSearchC: new FormGroup({
        'senderName': new FormControl(null),
        'senderId': new FormControl(null),
        'dataTypeId': new FormControl(null),
        'dataTypeCode': new FormControl(null),
        'dateRange': new FormControl(null),
        'fiCode': new FormControl(null),
        'fiNameTh': new FormControl(null),
      })
    });
  }

  onSubmit() {
    if (this.reportComplexSearchForm.get('reportComplexSearchC').value.dateRange !== null) {
      this.reportComplexSearchForm.value.reportComplexSearchC.dateStart = moment(this.reportComplexSearchForm.get('reportComplexSearchC').value.dateRange[0]).format('YYYY-MM-DD');
      this.reportComplexSearchForm.value.reportComplexSearchC.dateEnd = moment(this.reportComplexSearchForm.get('reportComplexSearchC').value.dateRange[1]).format('YYYY-MM-DD');
    }
    this.getReport(this.reportComplexSearchForm.value.reportComplexSearchC);
  }

  onSelect(event: TypeaheadMatch) {
    this.selectedOption = event.item;
    this.reportComplexSearchForm.get('reportComplexSearchC.senderId').setValue(this.selectedOption.userId);
  }

  resetForm() {
    this.reportComplexSearchForm.reset();
    this.getReport({});
  }

  getReport(param) {
    this.edcReportComplexValidationListTemp = <EdcProcessingModel[]>[];
    this.reportService.listProcessingDetail(param).subscribe(a => {
      this.edcReportComplexValidationList = a.data;
      this.page = a.page;
      this.total = a.total;
      this.pageSize = a.pageSize;
      console.log(this.edcReportComplexValidationListTemp);
    });
  }

  getListDataType() {
    this.edcDataTypeService.listSelect().subscribe(res => {
      this.dataTypeList = res.data;
    });
  }

  getListSenderUser() {
    this.senderTransferPushService.listUser().subscribe(result => {
      this.userInfoList = result.data;
    });
  }

  getPage(page: number) {
    this.reportComplexSearchForm.value.reportComplexSearchC.page = page;
    this.getReport(this.reportComplexSearchForm.value.reportComplexSearchC);
  }

  getPageSize(pageSize: number) {
    this.reportComplexSearchForm.value.reportComplexSearchC.pageSize = pageSize;
    this.getReport(this.reportComplexSearchForm.value.reportComplexSearchC);
  }

  onView(el: EdcProcessingDetailModel) {
    console.log(el);
    this.router.navigateByUrl('/etl/report-data-error/detail', {state: el});
  }

  onSelectFi(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.reportComplexSearchForm.get('reportComplexSearchC.fiCode').setValue(this.selectedOption.fiCode);
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }
}
