import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TypeaheadModule} from 'ngx-bootstrap';
import {BsDatepickerModule} from 'ngx-bootstrap-th';
import {NgxPaginationModule} from 'ngx-pagination';
import {ReportDataErrorRoutingModule} from './report-data-error-routing.module';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import {SharedModule} from '../../../../../shared/shared.module';

@NgModule({
  declarations: [ListComponent, DetailComponent],
  imports: [
    CommonModule,
    ReportDataErrorRoutingModule,
    ReactiveFormsModule,
    TypeaheadModule,
    BsDatepickerModule,
    FormsModule,
    NgxPaginationModule,
    SharedModule
  ]
})
export class ReportDataErrorModule { }
