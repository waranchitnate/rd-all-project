import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {EdcCrossBorderDtaModel} from '../../models/edcCrossborder-dta.model';
import {ResponseEtl} from '../../models/responseEtl.model';
import {environment} from '../../../../../../environments/environment';


@Injectable()
export class CrossBorderDtaService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcCrossBorderDtaModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcCrossBorderDtaModel[]>>>this.http.post(this.url + 'Dtas', request);
    }

    save(request: EdcCrossBorderDtaModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'Dta/add', request);
    }

    update(request: EdcCrossBorderDtaModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'Dta/update', request);
    }

    delete(request: EdcCrossBorderDtaModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'Dta/delete', request);
    }
}
