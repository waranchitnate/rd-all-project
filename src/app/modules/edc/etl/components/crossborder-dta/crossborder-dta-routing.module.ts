import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from '../crossborder-dta/list/list.component';
import {AddComponent} from '../crossborder-dta/add/add.component';
import {EditComponent} from '../crossborder-dta/edit/edit.component';

const routes: Routes = [
  { path: '', component: ListComponent},
  { path: 'add', component: AddComponent},
  { path: 'edit', component: EditComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrossborderDtaRoutingModule { }
