import {Component, OnInit, TemplateRef} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {EdcCountryModel} from '../../../models/edcCountry.model';
import {EdcCrossBorderIncomeModel} from '../../../models/edcCrossborderIncome.model';
import {BsModalRef} from 'ngx-bootstrap-th';
import {ModalService} from '../../../../../../core/services/modal.service';
import {CrossBorderDtaService} from '../edcCrossborderDta.service';
import {CrossBorderIncomeService} from '../../income-cross/edcCrossBorderIncome.service';
import {CountryService} from '../../country/edcCountry.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {EdcCrossBorderDtaModel} from '../../../models/edcCrossborder-dta.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    state: Observable<EdcCrossBorderDtaModel>;

    addDtaForm: FormGroup;
    searchCrossIncomeType: FormGroup;
    edcCountryList = <EdcCountryModel[]>[];
    edcCrossIncomeTypeList = <EdcCrossBorderIncomeModel[]>[];
    edcCrossIncomeTypeListInput = <EdcCrossBorderIncomeModel[]>[];
    minDate: Date;
    items: FormArray;
    modalRef: BsModalRef;
    masterSelected: boolean;
    selectedOption: EdcCountryModel;
    EdcCrossBorderDta: EdcCrossBorderDtaModel;
    addDtaFormTemp: any;

    constructor(
        public activatedRoute: ActivatedRoute,
        private modal: ModalService,
        private crossBorderDtaService: CrossBorderDtaService,
        private crossBorderIncomeService: CrossBorderIncomeService,
        private countryService: CountryService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.EdcCrossBorderDta = e;
        });

        if (this.EdcCrossBorderDta.id === undefined) {
            this.router.navigateByUrl('/etl/crossborder-dta');
        }

        // console.log(this.EdcCrossBorderDta);
        // console.log('list ---->  ' + this.EdcCrossBorderDta.edcCrossDtaIncomeList);
        // console.log('EdcCrossBorderDta.id ---->  ' + this.EdcCrossBorderDta.id);
        this.minDate = new Date(this.EdcCrossBorderDta.effectiveDate);
        this.getCountryList();
        this.getCrossIncomeTypeList();
        this.initialForm();

        for (let i = 0; i < this.EdcCrossBorderDta.edcCrossDtaIncomeList.length; i++) {
            this.addItem(
                this.EdcCrossBorderDta.edcCrossDtaIncomeList[i].edcCrossBorderIncomeTypeEntity.id,
                this.EdcCrossBorderDta.edcCrossDtaIncomeList[i].edcCrossBorderIncomeTypeEntity.incomeCode,
                this.EdcCrossBorderDta.edcCrossDtaIncomeList[i].edcCrossBorderIncomeTypeEntity.incomeNameTh,
                this.EdcCrossBorderDta.edcCrossDtaIncomeList[i].dtaRate,
                this.EdcCrossBorderDta.edcCrossDtaIncomeList[i].id);
        }
    }

    createItem(id: number, incomeCode: String, incomeNameTh: String, dtaRate: number, dtaIncome): FormGroup {
        return new FormGroup({
            'incomeCode': new FormControl(incomeCode, Validators.required),
            'crossDtaIncomeTypeName': new FormControl(incomeNameTh, Validators.required),
            'dtaRate': new FormControl(dtaRate, Validators.required),
            'crossIncomeId': new FormControl(id),
            'id': new FormControl(dtaIncome)
        });
    }

    onSubmit() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลอนุสัญญาภาษีซ้อนใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.addDtaForm.value.edcCrossDtaObj.id = this.EdcCrossBorderDta.id;
                // console.log(this.addDtaForm.value);
                this.crossBorderDtaService.update(this.addDtaForm.value.edcCrossDtaObj).subscribe(res => {
                    this.modal.openModal('แจ้งเตือน ', 'แก้ไขข้อมูลอนุสัญญาภาษีซ้อนสำเร็จ');
                    const modalHide = this.modal.modalService.onHide.subscribe(e => {
                        modalHide.unsubscribe();
                        this.router.navigate(['/etl/crossborder-dta']);
                    });
                }, err => {
                    this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                });
            }
            sub.unsubscribe();
        });
    }

    resetForm() {
        this.addDtaForm.reset();
        this.resetCrossIncomeTypeList();
        this.masterSelected = true;
        this.checkUncheckAll();
    }

    initialForm() {
        this.addDtaForm = new FormGroup({
            edcCrossDtaObj: new FormGroup({
                'countryId': new FormControl(this.EdcCrossBorderDta.edcCountryEntity.id, Validators.required),
                'countryName': new FormControl(this.EdcCrossBorderDta.edcCountryEntity.countryNameTh, Validators.required),
                'effectiveDate': new FormControl(this.EdcCrossBorderDta.effectiveDate, Validators.required),
                'endDate': new FormControl(this.EdcCrossBorderDta.endDate),
                'id': new FormControl(this.EdcCrossBorderDta.id),
                'createdDate': new FormControl(this.EdcCrossBorderDta.createdDate),
                'createdBy': new FormControl(this.EdcCrossBorderDta.createdBy),
                edcCrossDtaIncomeList: new FormArray([])
            })
        });

        this.searchCrossIncomeType = new FormGroup({
            edcCrossIncomeObjC: new FormGroup({
                'incomeCode': new FormControl(null),
                'incomeNameTh': new FormControl(null)
            })
        });
        this.addDtaFormTemp = this.addDtaForm.get('edcCrossDtaObj.edcCrossDtaIncomeList');
    }

    addItem(id: number, incomeCode: String, incomeNameTh: String, vatRate, dtaIncome) {
        this.items = this.addDtaForm.get('edcCrossDtaObj.edcCrossDtaIncomeList') as FormArray;
        this.items.push(this.createItem(id, incomeCode, incomeNameTh, vatRate, dtaIncome));
    }

    removeItem(index: number) {
        this.items = this.addDtaForm.get('edcCrossDtaObj.edcCrossDtaIncomeList') as FormArray;
        this.items.removeAt(index);
    }

    getCountryList() {
        this.countryService.listSelect().subscribe(res => {
            this.edcCountryList = res.data;
        });
    }

    getCrossIncomeTypeList() {
        this.crossBorderIncomeService.listSelectIncome().subscribe(res => {
            for (let index = 0; index < res.data.length; index++) {
                res.data[index].isSelect = this.masterSelected;
            }
            this.edcCrossIncomeTypeList = res.data;
            this.edcCrossIncomeTypeListInput = res.data;
        });
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modal.modalService.show(template);
    }

    onSubmitOnIncomeType() {
        this.resetCrossIncomeTypeList();
        this.modalRef.hide();
        this.edcCrossIncomeTypeList.forEach(e => {
            if (e.isSelect) {
                this.addItem(e.id, e.incomeCode, e.incomeNameTh, null, null);
            }
        });
        this.getCrossIncomeTypeList();
    }

    onSubmitSearch() {
      const incomeNameTh = this.searchCrossIncomeType.value.edcCrossIncomeObjC.incomeNameTh;
      const incomeCode = this.searchCrossIncomeType.value.edcCrossIncomeObjC.incomeCode;
      this.edcCrossIncomeTypeList = this.edcCrossIncomeTypeListInput.filter(e => {
        if (incomeCode !== null && incomeNameTh !== null) {
          return e.incomeCode === incomeCode && e.incomeNameTh === incomeNameTh;
        } else if (incomeCode != null && incomeNameTh === null) {
          return e.incomeCode === incomeCode;
        } else if (incomeCode === null && incomeNameTh !== null) {
          return e.incomeNameTh === incomeNameTh;
        }

        // e.incomeNameTh === incomeNameTh && e.incomeCode === incomeCode
      })
      ;
      // this.searchCrossIncomeType.reset();

    }

    resetFormIncomeType() {
        this.searchCrossIncomeType.reset();
        this.getCrossIncomeTypeList();
    }

    closeModal() {
        this.modalRef.hide();
        this.resetFormIncomeType();
        this.masterSelected = true;
        this.checkUncheckAll();
    }

    checkUncheckAll() {
        this.masterSelected = !this.masterSelected;
        // console.log(this.masterSelected);
        for (let i = 0; i < this.edcCrossIncomeTypeList.length; i++) {
            this.edcCrossIncomeTypeList[i].isSelect = this.masterSelected;
        }
    }

    isSelected(obj: EdcCrossBorderIncomeModel) {
        obj.isSelect = !obj.isSelect;
        console.log(obj);
    }

    resetCrossIncomeTypeList() {
        this.items = this.addDtaForm.get('edcCrossDtaObj.edcCrossDtaIncomeList') as FormArray;
        while (0 !== this.items.length) {
            this.items.removeAt(0);
        }
    }

    onSelect(event: TypeaheadMatch): void {
        this.selectedOption = event.item;
        this.addDtaForm.get('edcCrossDtaObj.countryId').setValue(this.selectedOption.id);
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลอนุสัญญาภาษีซ้อนยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/crossborder-dta');
                sub.unsubscribe();
            }
        });
    }
}
