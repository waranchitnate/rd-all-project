import {Component, OnInit} from '@angular/core';
import {CrossBorderDtaService} from '../edcCrossborderDta.service';
import {EdcCrossBorderDtaModel} from '../../../models/edcCrossborder-dta.model';
import {FormControl, FormGroup} from '@angular/forms';

import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcCountryModel} from '../../../models/edcCountry.model';
import {CountryService} from '../../country/edcCountry.service';
import {TypeaheadMatch} from 'ngx-bootstrap';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  edcCountryList = <EdcCountryModel[]>[];
  edcCrossBorderDtaList = <EdcCrossBorderDtaModel[]>[];
  crossBorderDtaForm: FormGroup;
  private selectedOption: EdcCountryModel;
  p: number;
  total: number;
  pageSize: number;

  constructor(
    private crossBorderDtaService: CrossBorderDtaService,
    private countryService: CountryService,
    private modal: ModalService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.getList({});
    this.getCountryList()
    this.initialForm();
  }

  onSubmit() {
    // console.log(this.crossBorderDtaForm.value);
    this.getList(this.crossBorderDtaForm.value.crossBorderDtaGroup);
  }

  getCountryList() {
    this.countryService.listSelect().subscribe(res => {
      this.edcCountryList = res.data;
    });
  }

  initialForm() {
    this.crossBorderDtaForm = new FormGroup({
      crossBorderDtaGroup: new FormGroup({
        'countryNameTh': new FormControl(null),
        'countryId': new FormControl(null)
      })
    });
  }

  onSelect(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.crossBorderDtaForm.get('crossBorderDtaGroup.countryId').setValue(this.selectedOption.id);
  }

  resetForm() {
    this.crossBorderDtaForm.reset();
    this.initialForm();
    this.getList(this.crossBorderDtaForm.value.crossBorderDtaGroup);
  }

  getList(param) {
    this.crossBorderDtaService.list(param).subscribe(result => {
      console.log(result.data);
      this.edcCrossBorderDtaList = result.data;
      this.p = result.page;
      this.total = result.total;
      this.pageSize = result.pageSize;
    });
  }

  getPage(page: number) {
    this.crossBorderDtaForm.value.crossBorderDtaGroup.page = page;
    this.getList(this.crossBorderDtaForm.value.crossBorderDtaGroup);
  }

  onEdit(obj) {
    // console.log(obj);
    this.router.navigateByUrl('/etl/crossborder-dta/edit', {state: obj});
  }

  onDelete(obj) {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลอนุสัญญาภาษีซ้อนใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.crossBorderDtaService.delete(obj).subscribe(
          response => {
            // console.log(response);
            this.getList(this.crossBorderDtaForm.value.crossborderDtaGroup);
            this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลอนุสัญญาภาษีซ้อนสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
          },
          err => {
            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
          }
        );
      }
      sub.unsubscribe();
    });

  }

  getPageSize(pageSize: number) {
    this.crossBorderDtaForm.value.crossBorderDtaGroup.pageSize = pageSize;
    this.getList(this.crossBorderDtaForm.value.crossBorderDtaGroup);
  }
}
