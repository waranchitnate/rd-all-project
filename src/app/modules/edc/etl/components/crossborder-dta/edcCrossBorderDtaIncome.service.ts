import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {EdcCrossBorderDtaIncomeModel} from '../../models/EdcCrossBorderDtaIncome.model';
import {ResponseEtl} from '../../models/responseEtl.model';
import {environment} from '../../../../../../environments/environment';


@Injectable()
export class CrossBorderDtaIncomeService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcCrossBorderDtaIncomeModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcCrossBorderDtaIncomeModel[]>>>this.http.post(this.url + 'DtaIncomes', request);
    }

    // save(request: EdcCrossBorderDtaIncomeModel): Observable<any> {
    //     console.log(request);
    //     return this.http.post(this.url + 'Dta/add', request);
    // }
    //
    // update(request: EdcCrossBorderDtaIncomeModel): Observable<any> {
    //     console.log(request);
    //     return this.http.post(this.url + 'Dta/update', request);
    // }
    //
    // delete(request: EdcCrossBorderDtaIncomeModel): Observable<any> {
    //     console.log(request);
    //     return this.http.post(this.url + 'Dta/delete', request);
    // }
}
