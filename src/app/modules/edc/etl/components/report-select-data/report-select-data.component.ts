import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UserInfoModel} from '../../../mft/models/userInfo.model';
import {DataTypeModel} from '../../../mft/models/dataType.model';
import {ConfigCodeService} from '../../../mft/edcConfigCode.service';
import {SenderTransferPushService} from '../../../mft/components/sender-transfer-push/sender-transfer-push.service';
import {ReportService} from '../../edcReport.service';
import * as moment from 'moment';
import {TypeaheadMatch} from 'ngx-bootstrap-th';
import {EdcReportDataExtractionModel} from '../../models/edcReportDataExtraction.model';
import {FinancialInstitutionService} from '../financial-institution/edcfinancial-institution.service';
import {EdcfinancialInstitutionModel} from '../../models/edcFinancial-insitution.model';
import {EdcDataTypeService} from '../../../mft/components/data-type/edcDataType.service';

@Component({
  selector: 'app-report-select-data',
  templateUrl: './report-select-data.component.html',
  styleUrls: ['./report-select-data.component.css']
})
export class ReportSelectDataComponent implements OnInit {


  reportDataExtractionSearchForm: FormGroup;
  userInfoList = <UserInfoModel[]>[];
  dataTypeList = <DataTypeModel[]>[];
  EdcReportDataExtractionList = <EdcReportDataExtractionModel[]>[];
  financialList = <EdcfinancialInstitutionModel[]>[];
  financialMap = new Map();
  dataTypeMap = new Map();
  minDate: Date;
  edcCountryList: any;
  selectedOption: any;
  total: number;
  pageSize: number;
  page: number;

  constructor(
      private configCodeService: ConfigCodeService,
      private senderTransferPushService: SenderTransferPushService,
      private reportService: ReportService,
      private financialInstitutionService: FinancialInstitutionService,
      private edcDataTypeService: EdcDataTypeService,
  ) {
  }

  ngOnInit() {
    this.initialForm();
    this.getListSenderUser();
    this.getListFinancial();
    this.getReport({});
    this.getListDatatype();
  }

  initialForm() {
    this.reportDataExtractionSearchForm = new FormGroup({
      reportDataExtractionSearchC: new FormGroup({
        'senderBankCode': new FormControl(null),
        'senderId': new FormControl(null),
        'dateRange': new FormControl(null),
        'fiCode': new FormControl(null),
        'fiNameTh': new FormControl(null)
      })
    });
  }

  onSelectFi(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.reportDataExtractionSearchForm.get('reportDataExtractionSearchC.fiCode').setValue(this.selectedOption.fiCode);
  }
  onSubmit() {
    if (this.reportDataExtractionSearchForm.get('reportDataExtractionSearchC').value.dateRange !== null) {
      this.reportDataExtractionSearchForm.value.reportDataExtractionSearchC.dateStart = moment(this.reportDataExtractionSearchForm.get('reportDataExtractionSearchC').value.dateRange[0]).format('YYYY-MM-DD');
      this.reportDataExtractionSearchForm.value.reportDataExtractionSearchC.dateEnd = moment(this.reportDataExtractionSearchForm.get('reportDataExtractionSearchC').value.dateRange[1]).format('YYYY-MM-DD');
    }
    this.getReport(this.reportDataExtractionSearchForm.value.reportDataExtractionSearchC);
  }

  onSelect(event: TypeaheadMatch) {
    this.selectedOption = event.item;
    this.reportDataExtractionSearchForm.get('reportDataExtractionSearchC.senderId').setValue(this.selectedOption.userId);
  }

  resetForm() {
    this.reportDataExtractionSearchForm.reset();
    this.getReport({});
  }

  getReport(param) {
    this.reportService.listDataExtraction(param).subscribe(res => {
      this.EdcReportDataExtractionList = res.data;
      this.page = res.page;
      this.total = res.total;
      this.pageSize = res.pageSize;
      console.log(res.data);
    });
  }

  getListSenderUser() {
    this.senderTransferPushService.listUser().subscribe(result => {
      this.userInfoList = result.data;
    });
  }

  getPage(page: number) {
    this.reportDataExtractionSearchForm.value.reportDataExtractionSearchC.page = page;
    this.getReport(this.reportDataExtractionSearchForm.value.reportDataExtractionSearchC);
  }

  getPageSize(pageSize: number) {
    this.reportDataExtractionSearchForm.value.reportDataExtractionSearchC.pageSize = pageSize;
    this.getReport(this.reportDataExtractionSearchForm.value.reportDataExtractionSearchC);
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }

  getListDatatype() {
    this.edcDataTypeService.listSelect().subscribe(res => {
      this.dataTypeList = res.data;
      for (let i = 0; i < this.dataTypeList.length; i++) {
        this.dataTypeMap.set(this.dataTypeList[i].dataTypeCode, this.dataTypeList[i].dataTypeDesTh);
      }
    });
  }
}
