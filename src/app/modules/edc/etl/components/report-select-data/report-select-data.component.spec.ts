import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSelectDataComponent } from './report-select-data.component';

describe('ReportSelectDataComponent', () => {
  let component: ReportSelectDataComponent;
  let fixture: ComponentFixture<ReportSelectDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSelectDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSelectDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
