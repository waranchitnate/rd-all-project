import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {DomesticIncomeTypeService} from '../edcDomesticIncomeType.service';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {EdcDomesticIncomeTypeModel} from '../../../models/edcDomesticIncomeType.model';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcSectionModel} from '../../../models/edcSection.model';
import {SectionService} from '../../section/edcSection.service';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    state: Observable<EdcDomesticIncomeTypeModel>;
    ConfigCodeList = <EdcConfigCodeModel[]>[];
    EdcFilterCodeList = <EdcConfigCodeModel[]>[];
    TaxRateTypeList = <EdcConfigCodeModel[]>[];
    PndList = <EdcConfigCodeModel[]>[];
    SectionList = <EdcSectionModel[]>[];

    minDate: Date;
    domesticIncomeTypeForm: FormGroup;
    domesticIncomeTypeFormTemp: FormArray;
    edcDomesticIncomeType: EdcDomesticIncomeTypeModel;
    items: FormArray;

    constructor(
        public activatedRoute: ActivatedRoute,
        public router: Router,
        private modal: ModalService,
        private sectionService: SectionService,
        private domesticIncomeTypeService: DomesticIncomeTypeService,
        private configCodeService: ConfigCodeService,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcDomesticIncomeType = e;

        });

        if (this.edcDomesticIncomeType.id === undefined) {
            this.router.navigateByUrl('/etl/domestic-income-type');
        }

        this.initialForm();

        this.minDate = new Date(this.edcDomesticIncomeType.effectiveDate);

        this.getSectionList({});
        this.getList({});
        this.items = this.domesticIncomeTypeForm.get('tableGroup.tableList') as FormArray;
        this.initialName();
    }

    initialName() {
        for (let i = 0; i < 16; i++) {
            let agentTaxPayerType;
            let taxPayerType;
            let rateType;
            let whtRate;
            let pnd;
            let oldId;
            let domesticIncomeId;

            let sectionId;
            oldId = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].id;
            domesticIncomeId = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].domesticIncomeId;
            agentTaxPayerType = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].agentTaxPayerType;
            taxPayerType = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].taxPayerType;
            rateType = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].rateType;
            whtRate = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].whtRate;
            pnd = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].pnd;
            sectionId = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].sectionId;
            this.items.push(this.createItem(agentTaxPayerType, taxPayerType, rateType, whtRate, pnd, oldId, domesticIncomeId, sectionId));
        }
    }

    getSectionList(param) {
        this.sectionService.listSelect().subscribe(result => {
            console.log('result');
            console.log(result.data);
            this.SectionList = result.data;
            console.log(this.SectionList);
        });
    }

    getList(param) {
        this.configCodeService.listConfig(param).subscribe(result => {
            // console.log('result');
            // console.log(result.data);
            this.ConfigCodeList = result.data;
            this.EdcFilterCodeList = this.ConfigCodeList.filter(res => res.type === 'TAXPAYER_TYPE');
            this.TaxRateTypeList = this.ConfigCodeList.filter(res => res.type === 'RATE_TYPE');
            this.PndList = this.ConfigCodeList.filter(res => res.type === 'PND');
          for (let i = 0; i < this.PndList.length; i++) {
            if (this.PndList[i].code === 'PND54') {
              this.PndList.splice(i, 1);
            }
          }
        });
    }

    initialForm() {
        this.domesticIncomeTypeForm = new FormGroup({
            domesticIncomeTypeGroup: new FormGroup({
                'incomeCode': new FormControl(this.edcDomesticIncomeType.incomeCode, [Validators.required, Validators.pattern('^[0-9]+$')]),
                'incomeNameTh': new FormControl(this.edcDomesticIncomeType.incomeNameTh, Validators.required),
                'incomeNameEn': new FormControl(this.edcDomesticIncomeType.incomeNameEn, [Validators.required, Validators.pattern('^[a-zA-Z\\s0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/? ]+$')]),
                'effectiveDate': new FormControl(this.edcDomesticIncomeType.effectiveDate, Validators.required),
                'endDate': new FormControl(this.edcDomesticIncomeType.endDate),
                'createdDate': new FormControl(this.edcDomesticIncomeType.createdDate),
                'createdBy': new FormControl(this.edcDomesticIncomeType.createdBy),
            }),
            tableGroup: new FormGroup({
                tableList: new FormArray([])
            })
        });
        this.domesticIncomeTypeFormTemp = this.domesticIncomeTypeForm.get('tableGroup.tableList') as FormArray;
    }

    createItem(agentTaxPayerType, taxPayerType, rateType, whtRate, pnd, oldId, domesticIncomeId, sectionId): FormGroup {
        return new FormGroup({
            'agentTaxPayerType': new FormControl(agentTaxPayerType),
            'taxPayerType': new FormControl(taxPayerType),
            'rateType': new FormControl(rateType),
            'whtRate': new FormControl(whtRate),
            'pnd': new FormControl(pnd),
            'id': new FormControl(oldId),
            'domesticIncomeId': new FormControl(domesticIncomeId),
            'sectionId': new FormControl(sectionId)
        });
    }

    resetForm() {
        // this.domesticIncomeTypeForm.reset();
        this.domesticIncomeTypeForm.get('domesticIncomeTypeGroup.incomeNameTh').reset();
        this.domesticIncomeTypeForm.get('domesticIncomeTypeGroup.incomeNameEn').reset();
        this.domesticIncomeTypeForm.get('domesticIncomeTypeGroup.effectiveDate').reset();
        this.domesticIncomeTypeForm.get('domesticIncomeTypeGroup.endDate').reset();
        this.domesticIncomeTypeForm.updateValueAndValidity();
        this.items.controls.splice(0);
        this.resetInitialForm();
    }

    resetInitialForm() {
        for (let i = 0; i < 16; i++) {
            let agentTaxPayerType;
            let taxPayerType;
            let oldId;
            let domesticIncomeId;
            let sectionId;

            oldId = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].id;
            domesticIncomeId = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].domesticIncomeId;
            agentTaxPayerType = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].agentTaxPayerType;
            taxPayerType = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].taxPayerType;
            sectionId = this.edcDomesticIncomeType.edcDomesticIncomeTypeRateList[i].sectionId;
            this.items.push(this.createItem(agentTaxPayerType, taxPayerType, 0, null, null, null, null, null));
        }
    }

    onSubmit() {
        this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup.id = this.edcDomesticIncomeType.id;

        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของประเภทรายได้ของผู้รับเงินอยู่ในประเทศใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.domesticIncomeTypeService.update(this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup).subscribe(
                    response => {
                        console.log(response.data.id);

                        // for (let i = 0; i < this.domesticIncomeTypeForm.value.tableGroup.tableList.length; i++) {
                        //   console.log(this.domesticIncomeTypeForm.value.tableGroup.tableList[i]);
                        // }
                        this.domesticIncomeTypeService.updateRate(this.domesticIncomeTypeForm.value.tableGroup.tableList).subscribe(
                            responseRate => {
                                this.modal.openModal('แจ้งเตือน ', 'แก้ไขข้อมูลประเภทรายได้ของผู้รับเงินอยู่ในประเทศสำเร็จ');
                                const modalHide = this.modal.modalService.onHide.subscribe(e => {
                                    modalHide.unsubscribe();
                                });
                                this.router.navigateByUrl('/etl/domestic-income-type');
                            },
                            err => {
                                if (err.error.errorCode === 'ST098') {
                                    this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสประเภทรายได้' + this.alertText.getTail);
                                } else {
                                    this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                                }
                            }
                        );

                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลประเภทรายได้ของผู้รับเงินอยู่ในประเทศยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/domestic-income-type');
                sub.unsubscribe();
            }
        });
    }

  findSectionListByPnd(pnd) {
    if (this.SectionList != null && pnd != null) {
      return this.SectionList.filter(item => item.pnd === pnd);
    } else {
    }
    return null;
  }
}
