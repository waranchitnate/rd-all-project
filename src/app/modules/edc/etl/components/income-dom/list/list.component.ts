import {Component, OnInit} from '@angular/core';

import {DomesticIncomeTypeService} from '../edcDomesticIncomeType.service';
import {EdcDomesticIncomeTypeModel} from '../../../models/edcDomesticIncomeType.model';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcDomesticIncomeList = <EdcDomesticIncomeTypeModel[]>[];
    domesticIncomeTypeForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private domesticIncomeTypeService: DomesticIncomeTypeService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.domesticIncomeTypeForm.value);
        this.getList(this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup);
    }

    initialForm() {
        this.domesticIncomeTypeForm = new FormGroup({
            domesticIncomeTypeGroup: new FormGroup({
                'incomeCode': new FormControl(null),
                'incomeNameTh': new FormControl(null),
                'incomeNameEn': new FormControl(null),
                'effectiveDate': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.domesticIncomeTypeForm.reset();
        this.initialForm();
        this.getList(this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup);
    }

    getList(param) {
        this.domesticIncomeTypeService.list(param).subscribe(result => {
            // console.log('List Domestic >>>', result.data);
            this.edcDomesticIncomeList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/domestic-income-type/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลประเภทรายได้ของผู้รับเงินอยู่ในประเทศใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.domesticIncomeTypeService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลประเภทรายได้ของผู้รับเงินอยู่ในประเทศสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('เกิดข้อผิดพลาด', 'ไม่สามารถบันทึกได้กรุณาทำรายการใหม่ภายหลัง');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPage(page: number) {
        this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup.page = page;
        // console.log(this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup);
        this.getList(this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup);
    }

    getPageSize(pageSize: number) {
        this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup.pageSize = pageSize;
        this.getList(this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup);
    }

}
