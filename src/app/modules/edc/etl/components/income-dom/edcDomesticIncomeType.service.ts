import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {EdcDomesticIncomeTypeModel} from '../../models/edcDomesticIncomeType.model';
import {EdcConfigCodeModel} from '../../models/edcConfigCode.model';
import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcDomesticIncomeTypeRateModel} from '../../models/edcDomesticIncomeTypeRate.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class DomesticIncomeTypeService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }
    list(request: any): Observable<ResponseEtl<EdcDomesticIncomeTypeModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcDomesticIncomeTypeModel[]>>>this.http.post(this.url + 'domIncomeTypes', request);
    }

    save(request: EdcDomesticIncomeTypeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'domIncomeType/add', request);
    }

    update(request: EdcDomesticIncomeTypeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'domIncomeType/update', request);
    }

    delete(request: EdcDomesticIncomeTypeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'domIncomeType/delete', request);
    }

    listConfig(request: any): Observable<ResponseEtl<EdcConfigCodeModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcConfigCodeModel[]>>>this.http.post(this.url + 'configCodes', request);
    }

    saveRate(request: EdcDomesticIncomeTypeRateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'domesticTypeRate/add', request);
    }

    updateRate(request: EdcDomesticIncomeTypeRateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'domesticTypeRate/update', request);
    }
}
