import {Component, OnInit} from '@angular/core';
import {DomesticIncomeTypeService} from '../edcDomesticIncomeType.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {ActivatedRoute, Router} from '@angular/router';
import {EdcSectionModel} from '../../../models/edcSection.model';
import {SectionService} from '../../section/edcSection.service';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  ConfigCodeList = <EdcConfigCodeModel[]>[];
  EdcFilterCodeList = <EdcConfigCodeModel[]>[];
  TaxRateTypeList = <EdcConfigCodeModel[]>[];
  PndList = <EdcConfigCodeModel[]>[];
  SectionList = <EdcSectionModel[]>[];

  // edcConfigCodeList = <EdcConfigCodeModel[]>[];
  minDate: Date;
  domesticIncomeTypeForm: FormGroup;
  private items: FormArray;
  domesticIncomeTypeFormTemp: FormArray;

  constructor(
    private modal: ModalService,
    private domesticIncomeTypeService: DomesticIncomeTypeService,
    private configCodeService: ConfigCodeService,
    private sectionService: SectionService,
    private router: Router,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {

    this.initialForm();
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.getSectionList({});
    this.getList({});
    this.items = this.domesticIncomeTypeForm.get('tableGroup.tableList') as FormArray;
    this.initialNameForm();
  }

  getSectionList(param) {
    this.sectionService.listSelect().subscribe(result => {
      // console.log('result');
      // console.log(result.data);
      this.SectionList = result.data;
      // console.log(this.SectionList);
    });
  }


  getList(param) {
    this.configCodeService.listConfig(param).subscribe(result => {
      this.ConfigCodeList = result.data;
      this.EdcFilterCodeList = this.ConfigCodeList.filter(res => res.type === 'TAXPAYER_TYPE');
      this.TaxRateTypeList = this.ConfigCodeList.filter(res => res.type === 'RATE_TYPE');
      this.PndList = this.ConfigCodeList.filter(res => res.type === 'PND');
      for (let i = 0; i < this.PndList.length; i++) {
        if (this.PndList[i].code === 'PND54') {
          this.PndList.splice(i, 1);
        }
      }
    });
  }

  initialForm() {
    this.domesticIncomeTypeForm = new FormGroup({
      domesticIncomeTypeGroup: new FormGroup({
        'incomeCode': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]+$')]),
        'incomeNameTh': new FormControl(null, Validators.required),
        'incomeNameEn': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z\\s0-9!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]+$')]),
        'effectiveDate': new FormControl(null, Validators.required),
        'endDate': new FormControl(null)
      }),
      tableGroup: new FormGroup({
        tableList: new FormArray([])
      })
    });
    this.domesticIncomeTypeFormTemp = this.domesticIncomeTypeForm.get('tableGroup.tableList') as FormArray;
  }

  resetForm() {
    this.domesticIncomeTypeForm.reset();
    this.items.controls.splice(0);
    this.initialNameForm();
  }

  createItem(agentTaxPayerType, taxPayerType): FormGroup {
    return new FormGroup({
      'agentTaxPayerType': new FormControl(agentTaxPayerType),
      'taxPayerType': new FormControl(taxPayerType),
      'rateType': new FormControl(0),
      'whtRate': new FormControl(),
      'pnd': new FormControl(null),
      'sectionId': new FormControl()
    });
  }

  onSubmit() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลประเภทรายได้ของผู้รับเงินอยู่ในประเทศใช่หรือไม่';


    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.domesticIncomeTypeForm.valid) {
        this.domesticIncomeTypeService.save(this.domesticIncomeTypeForm.value.domesticIncomeTypeGroup).subscribe(
          response => {
            for (let i = 0; i < this.domesticIncomeTypeForm.value.tableGroup.tableList.length; i++) {
              this.domesticIncomeTypeForm.value.tableGroup.tableList[i].domesticIncomeId = response.data.id;
            }
            this.domesticIncomeTypeService.saveRate(this.domesticIncomeTypeForm.value.tableGroup.tableList).subscribe(
              responseRate => {
                // console.log(this.domesticIncomeTypeForm.value.tableGroup.tableList);
                this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลประเภทรายได้ของผู้รับเงินอยู่ในประเทศสำเร็จ');
                const modalHide = this.modal.modalService.onHide.subscribe(e => {
                  modalHide.unsubscribe();
                });
                this.router.navigateByUrl('/etl/domestic-income-type');
              },
              err => {
                this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
              }
            );
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสประเภทรายได้' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.router.navigateByUrl('/etl/domestic-income-type');
        sub.unsubscribe();
      }
    });
  }

  initialNameForm() {
    for (let i = 1; i <= 16; i++) {
      let agentTaxPayerType;
      let taxPayerType;
      if (i === 1) {
        agentTaxPayerType = 100;
        taxPayerType = 100;
      }
      if (i === 2) {
        agentTaxPayerType = 100;
        taxPayerType = 200;
      }
      if (i === 3) {
        agentTaxPayerType = 100;
        taxPayerType = 300;
      }
      if (i === 4) {
        agentTaxPayerType = 100;
        taxPayerType = 400;
      }
      if (i === 5) {
        agentTaxPayerType = 200;
        taxPayerType = 100;
      }
      if (i === 6) {
        agentTaxPayerType = 200;
        taxPayerType = 200;
      }
      if (i === 7) {
        agentTaxPayerType = 200;
        taxPayerType = 300;
      }
      if (i === 8) {
        agentTaxPayerType = 200;
        taxPayerType = 400;
      }
      if (i === 9) {
        agentTaxPayerType = 300;
        taxPayerType = 100;
      }
      if (i === 10) {
        agentTaxPayerType = 300;
        taxPayerType = 200;
      }
      if (i === 11) {
        agentTaxPayerType = 300;
        taxPayerType = 300;
      }
      if (i === 12) {
        agentTaxPayerType = 300;
        taxPayerType = 400;
      }
      if (i === 13) {
        agentTaxPayerType = 400;
        taxPayerType = 100;
      }
      if (i === 14) {
        agentTaxPayerType = 400;
        taxPayerType = 200;
      }
      if (i === 15) {
        agentTaxPayerType = 400;
        taxPayerType = 300;
      }
      if (i === 16) {
        agentTaxPayerType = 400;
        taxPayerType = 400;
      }
      this.items.push(this.createItem(agentTaxPayerType, taxPayerType));
    }
  }

  findSectionListByPnd(pnd) {
    if (this.SectionList != null && pnd != null) {
      return this.SectionList.filter(item => item.pnd === pnd);
    } else {
    }
    return null;
  }
}
