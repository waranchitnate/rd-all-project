import {Component, OnInit} from '@angular/core';
import {EdcThambolModel} from '../../../models/edcThambol.model';
import {EdcAmphurModel} from '../../../models/edcAmphur.model';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ThambolService} from '../edcThambol.service';
import {AmphurService} from '../../amphur/edcAmphur.service';
import {ProvinceService} from '../../province/edcProvince.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    edcThambolList = <EdcThambolModel[]>[];
    edcAmphurList = <EdcAmphurModel[]>[];
    edcProvinceList = <EdcProvinceModel[]>[];
    private selectedOptionAmphur: EdcAmphurModel;
    private selectedOptionProvince: EdcProvinceModel;

    items: FormArray;
    thambolForm: FormGroup;

    constructor(
        private thambolService: ThambolService,
        private amphurService: AmphurService,
        private  provinceService: ProvinceService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.getProvinceList();
        this.getAmphurList();
        this.initialForm();
    }

    resetAmphur() {
        this.getAmphurList();
        this.thambolForm.get('thambolGroup.amphurId').reset();
        this.thambolForm.get('thambolGroup.amphurNameTh').reset();
    }

    onSelectAmphur(event: TypeaheadMatch): void {
        this.selectedOptionAmphur = event.item;
        this.thambolForm.get('thambolGroup.amphurId').setValue(this.selectedOptionAmphur.id);

        if (this.thambolForm.get('thambolGroup.provinceId') != null) {
            this.thambolForm.get('thambolGroup.provinceId').setValue(this.selectedOptionAmphur.provinceId);
            this.thambolForm.get('thambolGroup.provinceNameTh').setValue(this.selectedOptionAmphur.edcProvinceEntity.provinceNameTh);
        }
    }

    onSelectProvince(event: TypeaheadMatch): void {
        this.selectedOptionProvince = event.item;
        this.thambolForm.get('thambolGroup.provinceId').setValue(this.selectedOptionProvince.id);
        this.edcAmphurList = this.edcAmphurList.filter(res => res.provinceId === this.selectedOptionProvince.id);
    }

    initialForm() {
        this.thambolForm = new FormGroup({
            thambolGroup: new FormGroup({
                'thambolCode': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]+$')]),
                'thambolNameTh': new FormControl(null, Validators.required),
                'thambolNameEn': new FormControl(null),
                'comment': new FormControl(null),
                'amphurId': new FormControl(null),
                'amphurNameTh': new FormControl(null, Validators.required),
                'provinceId': new FormControl(null),
                'provinceNameTh': new FormControl(null, Validators.required)
            })
        });
    }

    resetForm() {
        this.thambolForm.reset();
    }

    getProvinceList() {
        this.provinceService.listSelect().subscribe(res => {
            this.edcProvinceList = res.data;
        });
    }

    getAmphurList() {
        this.amphurService.listSelect().subscribe(res => {
            this.edcAmphurList = res.data;
        });
    }

    onSubmit() {
        console.log(this.thambolForm.value);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลตำบลใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.thambolService.save(this.thambolForm.value.thambolGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลตำบลสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/thambol');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสตำบล' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลตำบลยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/thambol');
                sub.unsubscribe();
            }
        });
    }
}
