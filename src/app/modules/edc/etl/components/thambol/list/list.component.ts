import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {EdcThambolModel} from '../../../models/edcThambol.model';
import {EdcAmphurModel} from '../../../models/edcAmphur.model';
import {EdcProvinceModel} from '../../../models/edcProvince.model';
import {AmphurService} from '../../amphur/edcAmphur.service';
import {ProvinceService} from '../../province/edcProvince.service';
import {ThambolService} from '../edcThambol.service';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcThambolList = <EdcThambolModel[]>[];
    edcAmphurList = <EdcAmphurModel[]>[];
    edcProvinceList = <EdcProvinceModel[]>[];
    private selectedOptionAmphur: EdcAmphurModel;
    private selectedOptionProvince: EdcProvinceModel;

    items: FormArray;
    thambolForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private thambolService: ThambolService,
        private amphurService: AmphurService,
        private  provinceService: ProvinceService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getProvinceList();
        this.getAmphurList();
        this.getList({});
        this.initialForm();
    }

    resetAmphur() {
        this.getAmphurList();
        this.thambolForm.get('thambolGroup.amphurId').reset();
        this.thambolForm.get('thambolGroup.amphurNameTh').reset();
    }

    onSelectAmphur(event: TypeaheadMatch): void {
        this.selectedOptionAmphur = event.item;
        this.thambolForm.get('thambolGroup.amphurId').setValue(this.selectedOptionAmphur.id);
    }

    onSelectProvince(event: TypeaheadMatch): void {
        this.selectedOptionProvince = event.item;
        this.thambolForm.get('thambolGroup.provinceId').setValue(this.selectedOptionProvince.id);

        // console.log(this.selectedOptionProvince.id);
        this.amphurService.list(this.selectedOptionProvince.id).subscribe(result => {
            // console.log(result.data);
            this.edcAmphurList = result.data;
        });
        this.edcAmphurList = this.edcAmphurList.filter(res => res.provinceId === this.selectedOptionProvince.id);
        // console.log(this.edcAmphurList);
    }

    onSubmit() {
        // console.log(this.thambolForm.value);
        this.getList(this.thambolForm.value.thambolGroup);
    }

    initialForm() {
        this.thambolForm = new FormGroup({
            thambolGroup: new FormGroup({
                'thambolNameTh': new FormControl(null),
                'amphurId': new FormControl(null),
                'amphurNameTh': new FormControl(null),
                'provinceId': new FormControl(null),
                'provinceNameTh': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.thambolForm.reset();
        this.initialForm();
        this.getAmphurList();
        this.getList(this.thambolForm.value.thambolGroup);
    }

    getProvinceList() {
        this.provinceService.listSelect().subscribe(res => {
            this.edcProvinceList = res.data;
        });
    }

    getAmphurList() {
        this.amphurService.listSelect().subscribe(res => {
            this.edcAmphurList = res.data;
        });
    }

    getList(param) {
        this.thambolService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcThambolList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.thambolForm.value.thambolGroup.page = page;
        // console.log(this.thambolForm.value.thambolGroup);
        this.getList(this.thambolForm.value.thambolGroup);
    }

    onView(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/thambol/view', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลตำบลใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.thambolService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.thambolForm.value.thambolGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลตำบลสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.thambolForm.value.thambolGroup.pageSize = pageSize;
        this.getList(this.thambolForm.value.thambolGroup);
    }
}
