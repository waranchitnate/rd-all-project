import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcThambolModel} from '../../models/edcThambol.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class ThambolService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcThambolModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcThambolModel[]>>>this.http.post(this.url + 'thambols', request);
    }

    listSelect(): Observable<ResponseEtl<EdcThambolModel[]>> {
        return <Observable<ResponseEtl<EdcThambolModel[]>>>this.http.get(this.url + 'thambols');
    }

    save(request: EdcThambolModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'thambol/add', request);
    }

    // update(request: EdcThambolModel): Observable<any> {
    //     console.log(request);
    //     return this.http.post(this.url + 'thambol/update', request);
    // }
  import(file: File): Observable<any> {
    const data = new FormData();
    data.append('file', file);
    console.log(data);
    return this.http.post(this.url + 'thambol/import', data);
  }

    delete(request: EdcThambolModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'thambol/delete', request);
    }


}
