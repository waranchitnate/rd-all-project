import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListComponent} from '../thambol/list/list.component';
import {AddComponent} from '../thambol/add/add.component';
import {ViewComponent} from './view/view.component';
import {ImportComponent} from "../thambol/import/import.component";

const routes: Routes = [
    {path: '', component: ListComponent},
    {path: 'add', component: AddComponent},
    {path: 'view', component: ViewComponent},
  { path: 'import', component: ImportComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ThambolRoutingModule {
}
