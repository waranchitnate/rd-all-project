
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcTitleCodeModel} from '../../models/edcTitleCode.model';
import {environment} from '../../../../../../environments/environment';


@Injectable()
export class TitleCodeService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseEtl<EdcTitleCodeModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcTitleCodeModel[]>>>this.http.post(this.url + 'titlecodes', request);
    }

    save(request: EdcTitleCodeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'titlecode/add', request);
    }

    update(request: EdcTitleCodeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'titlecode/update', request);
    }

    delete(request: EdcTitleCodeModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'titlecode/delete', request);
    }

  import(file: File): Observable<any> {
    const data = new FormData();
    data.append('file', file);
    console.log(data);
    return this.http.post(this.url + 'titlecode/import', data);
  }
}
