import {AddComponent} from '../title-code/add/add.component';
import {ViewComponent} from '../title-code/view/view.component';
import {ListComponent} from '../title-code/list/list.component';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ImportComponent} from "../title-code/import/import.component";

const routes: Routes = [
    { path: '', component: ListComponent},
    { path: 'add', component: AddComponent},
    { path: 'view', component: ViewComponent},
  { path: 'import', component: ImportComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TitleCodeRoutingModule  { }
