import {NgModule} from '@angular/core';
import {AddComponent} from '../title-code/add/add.component';
import {ViewComponent} from '../title-code/view/view.component';
import {ListComponent} from '../title-code/list/list.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {TypeaheadModule} from 'ngx-bootstrap';
import {TitleCodeRoutingModule} from './title-code-routing.module';
import {SharedModule} from '../../../../../shared/shared.module';
import {ImportComponent} from "../title-code/import/import.component";

@NgModule({
    declarations: [AddComponent, ViewComponent, ListComponent, ImportComponent],
  imports: [
    CommonModule,
    TitleCodeRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    BsDatepickerModule,
    TypeaheadModule,
    SharedModule
  ]
})
export class TitleCodeModule { }
