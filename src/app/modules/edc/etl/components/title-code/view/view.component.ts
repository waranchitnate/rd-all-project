import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EdcTitleCodeModel} from '../../../models/edcTitleCode.model';
import {TitleCodeService} from '../edcTitleCode.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-view',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

    state: Observable<EdcTitleCodeModel>;
    titleCodeForm: FormGroup;
    private edcTitleCode: EdcTitleCodeModel;

    constructor(
        public activatedRoute: ActivatedRoute,
        private titleCodeService: TitleCodeService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcTitleCode = e;
        });
        if (this.edcTitleCode.id === undefined) {
            this.router.navigateByUrl('/etl/title-code');
        }
        this.initialForm();
    }

    initialForm() {
        this.titleCodeForm = new FormGroup({
            titleCodeGroup: new FormGroup({
                'code': new FormControl(this.edcTitleCode.code, Validators.required),
                'description': new FormControl(this.edcTitleCode.description, Validators.required),
                'comment': new FormControl(this.edcTitleCode.comment)
            })
        });
    }

    resetForm() {
        this.titleCodeForm.reset();
    }

    onSubmit() {
        // console.log(this.titleCodeForm.value);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของคำนำหน้าชื่อใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.titleCodeForm.value.titleCodeGroup.id = this.edcTitleCode.id;
                this.titleCodeService.update(this.titleCodeForm.value.titleCodeGroup).subscribe(
                    response => {
                        // console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลคำนำหน้าชื่อสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/title-code');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสคำนำหน้าชื่อ' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        // const modalRef = this.modal.modalService.show(AnswerModalComponent);
        //
        // (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        // (modalRef.content as AnswerModalComponent).content = 'ข้อมูลคำนำหน้าชื่อยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
        //     if (a) {
                this.router.navigateByUrl('/etl/title-code');
        //         sub.unsubscribe();
        //     }
        // });
    }
}
