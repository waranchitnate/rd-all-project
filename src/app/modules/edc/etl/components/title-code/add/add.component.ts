import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {TitleCodeService} from '../edcTitleCode.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {DiscrepancyService} from '../../discrepancy/edcDiscrepancy.service';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    titleCodeForm: FormGroup;
    minDate: Date;

    constructor(
        private titleCodeService: TitleCodeService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.initialForm();
    }

    initialForm() {
        this.titleCodeForm = new FormGroup({
            titleCodeGroup: new FormGroup({
                'code': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]+$')]),
                'description': new FormControl(null, Validators.required),
                'comment': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.titleCodeForm.reset();
    }

    onSubmit() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลคำนำหน้าชื่อใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.titleCodeForm.valid) {
                this.titleCodeService.save(this.titleCodeForm.value.titleCodeGroup).subscribe(
                    response => {
                        // console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลคำนำหน้าชื่อสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/title-code');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสคำนำหน้าชื่อ' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลคำนำหน้าชื่อ ยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/title-code');
                sub.unsubscribe();
            }
        });
    }
}
