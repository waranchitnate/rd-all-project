import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {EdcTitleCodeModel} from '../../../models/edcTitleCode.model';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {TitleCodeService} from '../edcTitleCode.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcTitleCodeList = <EdcTitleCodeModel[]>[];
    titleCodeForm: FormGroup;
    p: number;
    total: number;
    pageSize: number;

    constructor(
        private titleCodeService: TitleCodeService,
        private modal: ModalService,
        private router: Router) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        // console.log(this.titleCodeForm.value);
        this.getList(this.titleCodeForm.value.titleCodeGroup);
    }

    initialForm() {
        this.titleCodeForm = new FormGroup({
            titleCodeGroup: new FormGroup({
                'description': new FormControl(null)
            })
        });
    }

    resetForm() {
        this.titleCodeForm.reset();
        this.initialForm();
        this.getList(this.titleCodeForm.value.titleCodeGroup);
    }

    getList(param) {
        this.titleCodeService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcTitleCodeList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.titleCodeForm.value.titleCodeGroup.page = page;
        // console.log(this.titleCodeForm.value.titleCodeGroup);
        this.getList(this.titleCodeForm.value.titleCodeGroup);
    }

    onView(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/title-code/view', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลคำนำหน้าชื่อใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.titleCodeService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.titleCodeForm.value.titleCodeGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลคำนำหน้าชื่อสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPageSize(pageSize: number) {
        this.titleCodeForm.value.titleCodeGroup.pageSize = pageSize;
        this.getList(this.titleCodeForm.value.titleCodeGroup);
    }
}
