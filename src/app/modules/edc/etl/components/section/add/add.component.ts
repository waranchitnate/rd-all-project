import {Component, OnInit} from '@angular/core';
import {EdcSectionModel} from '../../../models/edcSection.model';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {SectionService} from '../edcSection.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {VatRateService} from '../../vat-rate/edcVatRate.service';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    ConfigCodeList = <EdcConfigCodeModel[]>[];
    PndList = <EdcConfigCodeModel[]>[];
    sectionForm: FormGroup;

    constructor(
        private configCodeService: ConfigCodeService,
        private sectionService: SectionService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.getPndList();
        this.initialForm();
    }

    resetForm() {
        this.sectionForm.reset();
    }

    onSubmit() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลมาตรากฏหมายใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.sectionForm.valid) {
                this.sectionService.save(this.sectionForm.value.sectionGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลมาตรากฏหมายสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/section');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสมาตรากฏหมาย' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    initialForm() {
        this.sectionForm = new FormGroup({
            sectionGroup: new FormGroup({
                'sectionCode': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]),
                'sectionSeq': new FormControl(null,  [Validators.min(0), Validators.max(999)]),
                'sectionName': new FormControl(null, Validators.required),
                'sectionDesc': new FormControl(null),
                'pnd': new FormControl(null, Validators.required),
            })
        });
    }

    getPndList() {
        this.configCodeService.listConfig({}).subscribe(result => {
            this.ConfigCodeList = result.data;
            this.PndList = this.ConfigCodeList.filter(res => res.type === 'PND');
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลมาตรากฏหมายยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/section');
                sub.unsubscribe();
            }
        });
    }
}
