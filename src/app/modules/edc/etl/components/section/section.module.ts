import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TypeaheadModule} from 'ngx-bootstrap';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';
import { SectionRoutingModule } from './section-routing.module';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import {SharedModule} from '../../../../../shared/shared.module';

@NgModule({
  declarations: [AddComponent, EditComponent, ListComponent],
  imports: [
    CommonModule,
    SectionRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    BsDatepickerModule,
    TypeaheadModule,
    SharedModule
  ]
})
export class SectionModule { }
