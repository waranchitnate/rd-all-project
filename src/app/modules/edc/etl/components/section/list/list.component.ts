import {Component, OnInit} from '@angular/core';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {EdcSectionModel} from '../../../models/edcSection.model';
import {SectionService} from '../edcSection.service';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {FormControl, FormGroup} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    SectionList = <EdcSectionModel[]>[];
    ConfigCodeList = <EdcConfigCodeModel[]>[];
    PndList = <EdcConfigCodeModel[]>[];
    tablePndMap = new Map();
    sectionForm: FormGroup;

    p: number;
    total: number;
    pageSize: number;

    constructor(
        private configCodeService: ConfigCodeService,
        private sectionService: SectionService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getPndList();
        this.getList({});
        this.initialForm();
    }

    initialForm() {
        this.sectionForm = new FormGroup({
            sectionGroup: new FormGroup({
                'pnd': new FormControl(null),
                'sectionCode': new FormControl(null),
                'sectionDesc': new FormControl(null)
            })
        });
    }

    onSubmit() {
        // console.log(this.sectionForm.value);
        this.getList(this.sectionForm.value.sectionGroup);
    }

    getPndList() {
        this.configCodeService.listConfig({}).subscribe(result => {
            this.ConfigCodeList = result.data;
            this.PndList = this.ConfigCodeList.filter(res => res.type === 'PND');
            for (let i = 0; i <= this.PndList.length - 1; i++) {
                // console.log(this.PndList[i]);
                this.tablePndMap.set(this.PndList[i].code, this.PndList[i].description);
            }
            // console.log(this.tablePndMap);
        });
    }

    getList(param) {
        this.sectionService.list(param).subscribe(result => {
            this.SectionList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    resetForm() {
        this.sectionForm.reset();
        this.initialForm();
        this.getPndList();
        this.getList(this.sectionForm.value.sectionGroup);
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/etl/section/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลมาตรากฏหมายใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.sectionService.delete(obj).subscribe(
                    response => {
                        console.log(response);
                        this.getList(this.sectionForm.value.sectionGroup);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลมาตรากฏหมายสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    getPage(page: number) {
        this.sectionForm.value.sectionGroup.page = page;
        // console.log(this.sectionForm.value.sectionGroup);
        this.getList(this.sectionForm.value.sectionGroup);
    }

    getPageSize(pageSize: number) {
        this.sectionForm.value.sectionGroup.pageSize = pageSize;
        this.getList(this.sectionForm.value.sectionGroup);
    }
}
