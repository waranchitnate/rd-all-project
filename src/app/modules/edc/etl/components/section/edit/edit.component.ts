import {Component, OnInit} from '@angular/core';
import {EdcSectionModel} from '../../../models/edcSection.model';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {SectionService} from '../edcSection.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {VatRateService} from '../../vat-rate/edcVatRate.service';
import {AlertTextModule} from '../../../../mft/edcAlertText.module';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    state: Observable<EdcSectionModel>;
    edcSection: EdcSectionModel;
    ConfigCodeList = <EdcConfigCodeModel[]>[];
    PndList = <EdcConfigCodeModel[]>[];
    sectionForm: FormGroup;

    constructor(
        public activatedRoute: ActivatedRoute,
        public router: Router,
        private configCodeService: ConfigCodeService,
        private sectionService: SectionService,
        private modal: ModalService,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcSection = e;
        });
        if (this.edcSection.id === undefined) {
            this.router.navigateByUrl('/etl/section');
        }
        this.getPndList();
        this.initialForm();
        console.log(this.PndList);
    }

    resetForm() {
        this.sectionForm.reset();
    }

    onSubmit() {
        console.log(this.sectionForm.value);
        this.sectionForm.value.sectionGroup.id = this.edcSection.id;

        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลมาตรากฏหมายใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.sectionForm.valid) {
                this.sectionService.update(this.sectionForm.value.sectionGroup).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลมาตรากฏหมายสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/etl/section');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสมาตรากฏหมาย' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
                sub.unsubscribe();
            }
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลมาตรากฏหมายยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.router.navigateByUrl('/etl/section');
                sub.unsubscribe();
            }
        });
    }

    initialForm() {
        this.sectionForm = new FormGroup({
            sectionGroup: new FormGroup({
                'sectionCode': new FormControl(this.edcSection.sectionCode, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]),
                'sectionSeq': new FormControl(this.edcSection.sectionSeq,  [Validators.min(0), Validators.max(999)]),
                'sectionName': new FormControl(this.edcSection.sectionName, Validators.required),
                'sectionDesc': new FormControl(this.edcSection.sectionDesc),
                'pnd': new FormControl(this.edcSection.pnd, Validators.required),
                'createdDate': new FormControl(this.edcSection.createdDate),
                'createdBy': new FormControl(this.edcSection.createdBy)
            })
        });
    }

    getPndList() {
        this.configCodeService.listConfig({}).subscribe(result => {
            this.ConfigCodeList = result.data;
            this.PndList = this.ConfigCodeList.filter(res => res.type === 'PND');
            console.log(this.PndList);
        });
    }
}
