import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseEtl} from '../../models/responseEtl.model';
import {EdcSectionModel} from '../../models/edcSection.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class SectionService {
  url = environment.streamApiUrl;

  constructor(private http: HttpClient) {
  }

  list(request: any): Observable<ResponseEtl<EdcSectionModel[]>> {
    return <Observable<ResponseEtl<EdcSectionModel[]>>>this.http.post(this.url + 'sections', request);
  }

  listSelect(): Observable<ResponseEtl<EdcSectionModel[]>> {
    return <Observable<ResponseEtl<EdcSectionModel[]>>>this.http.get(this.url + 'sections');
  }

  save(request: EdcSectionModel): Observable<any> {
    console.log(request);
    return this.http.post(this.url + 'section/add', request);
  }

  update(request: EdcSectionModel): Observable<any> {
    console.log(request);
    return this.http.post(this.url + 'section/update', request);
  }

  delete(request: EdcSectionModel): Observable<any> {
    console.log(request);
    return this.http.post(this.url + 'section/delete', request);
  }
}
