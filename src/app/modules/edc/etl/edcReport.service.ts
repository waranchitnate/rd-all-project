import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseEtl} from './models/responseEtl.model';
import {EdcReportComplexValidationModel} from './models/edcReportComplexValidation.model';
import {EdcReportDataExtractionModel} from './models/edcReportDataExtraction.model';
import {EdcProcessingDetailModel} from './models/edcProcessingDetail.model';
import { EdcProcessingModel } from './models/edcProcessing.model';
import {EdcReportDataErrorModel} from './models/edcReportDataError.model';
import {EdcReportCrossValidationModel} from './models/edcReportCrossValidation.model';
import {EdcTaxIdNotFoundModel} from './models/edcTaxIdNotFound.model';
import {EdcMt940transModel} from '../mft/models/edcMt940trans.model';

@Injectable()
export class ReportService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }
    listComplexValidation(request: any): Observable<ResponseEtl<EdcReportComplexValidationModel[]>> {
        return <Observable<ResponseEtl<EdcReportComplexValidationModel[]>>>this.http.post(this.url + 'edcReportComplexValidation', request);
    }

    listDataExtraction(request: any): Observable<ResponseEtl<EdcReportDataExtractionModel[]>> {
        return <Observable<ResponseEtl<EdcReportDataExtractionModel[]>>>this.http.post(this.url + 'edcReportDataExtraction', request);
    }

    listReportDataError(request: any): Observable<ResponseEtl<EdcReportDataErrorModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcReportDataErrorModel[]>>>this.http.post(this.url + 'edcReportSimpleValidation', request);
    }

    listProcessing(request: any): Observable<ResponseEtl<EdcProcessingModel[]>> {
        return <Observable<ResponseEtl<EdcProcessingModel[]>>>this.http.post(this.url + 'edcDataProcessings', request);
    }

    listProcessingDetail(request: any): Observable<ResponseEtl<EdcProcessingDetailModel[]>> {
      return <Observable<ResponseEtl<EdcProcessingDetailModel[]>>>this.http.post(this.url + 'edcDataProcessingDetails', request);
    }

    listCrossValidation(request: any): Observable<ResponseEtl<EdcReportCrossValidationModel[]>> {
        return <Observable<ResponseEtl<EdcReportCrossValidationModel[]>>>this.http.post(this.url + 'edcReportCrossValidation', request);
    }

    listTaxIdFound(request: any): Observable<ResponseEtl<EdcTaxIdNotFoundModel[]>> {
        return <Observable<ResponseEtl<EdcTaxIdNotFoundModel[]>>>this.http.post(this.url + 'EdcTaxIdNotFounds', request);
    }

    listTaxIdFoundDetail(request: any): Observable<ResponseEtl<EdcTaxIdNotFoundModel[]>> {
     return <Observable<ResponseEtl<EdcTaxIdNotFoundModel[]>>>this.http.post(this.url + 'EdcTaxIdNotFounds/detail', request);
    }

    listMt940trans(request: any): Observable<ResponseEtl<EdcMt940transModel[]>> {
      return <Observable<ResponseEtl<EdcMt940transModel[]>>>this.http.post(this.url + 'EdcTaxIdNotFounds', request);
    }
}
