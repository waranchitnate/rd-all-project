import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseEtl} from './models/responseEtl.model';
import {EdcConfigCodeModel} from './models/edcConfigCode.model';

@Injectable()
export class ConfigCodeService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    listConfig(request: any): Observable<ResponseEtl<EdcConfigCodeModel[]>> {
        console.log(request);
        return <Observable<ResponseEtl<EdcConfigCodeModel[]>>>this.http.get(this.url + 'configCodes');
    }
}
