import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsDatepickerModule} from 'ngx-bootstrap-th';
import {PaginationModule, TypeaheadModule} from 'ngx-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';
import {EtlRoutingModule} from './etl-routing.module';
import {VatRateService} from './components/vat-rate/edcVatRate.service';
import {CurrencyService} from './components/currency/edcCurrency.service';
import {CountryService} from './components/country/edcCountry.service';
import {FinancialInstitutionService} from './components/financial-institution/edcfinancial-institution.service';
import {DiscrepancyService} from './components/discrepancy/edcDiscrepancy.service';
import {CrossBorderDtaService} from './components/crossborder-dta/edcCrossborderDta.service';
import {CrossBorderIncomeService} from './components/income-cross/edcCrossBorderIncome.service';
import {DomesticIncomeTypeService} from './components/income-dom/edcDomesticIncomeType.service';
import {SectionService} from './components/section/edcSection.service';
import {ConfigCodeService} from './edcConfigCode.service';
import {ReportService} from './edcReport.service';
import {TitleCodeService} from './components/title-code/edcTitleCode.service';
import {ProvinceService} from './components/province/edcProvince.service';
import {AmphurService} from './components/amphur/edcAmphur.service';
import {ThambolService} from './components/thambol/edcThambol.service';
import {PostcodeService} from './components/postcode/edcPostcode.service';
import { ReportValidateComponent } from './components/report-validate/report-validate.component';
import { ReportSelectDataComponent } from './components/report-select-data/report-select-data.component';
import { ReportImproveComponent } from './components/report-improve/report-improve.component';
import { ReportNotFoundComponent } from './components/report-not-found/report-not-found.component';
import { ReportNotFoundDetailComponent } from './components/report-improve/detail/report-not-found-detail.component';
import {VatTypeService} from './components/vat-type/edcVatType.service';
import {OfficeCodeService} from './components/officecode/edcOfficecode.service';
import {NidGovAssoService} from './components/nid-gov-asso/edcNidGovAsso.service';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    EtlRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    TypeaheadModule,
    NgxPaginationModule,
    PaginationModule,
    SharedModule

  ],
    providers: [
        VatRateService,
        CurrencyService,
        CountryService,
        FinancialInstitutionService,
        DiscrepancyService,
        CrossBorderDtaService,
        CrossBorderIncomeService,
        DomesticIncomeTypeService,
        ConfigCodeService,
        ReportService,
        TitleCodeService,
        ProvinceService,
        AmphurService,
        ThambolService,
        PostcodeService,
        SectionService,
        VatTypeService,
        OfficeCodeService,
        NidGovAssoService,

    ], declarations: [ReportValidateComponent, ReportSelectDataComponent, ReportImproveComponent, ReportNotFoundComponent, ReportNotFoundDetailComponent]
})
export class EtlModule {
}
