import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReportValidateComponent} from './components/report-validate/report-validate.component';
import {ReportSelectDataComponent} from './components/report-select-data/report-select-data.component';
import {ReportNotFoundComponent} from './components/report-not-found/report-not-found.component';
import {ReportImproveComponent} from './components/report-improve/report-improve.component';
import {ReportNotFoundDetailComponent} from './components/report-improve/detail/report-not-found-detail.component';

const routes: Routes = [
  // { path: 'home', component: SenderTransferPushConfig, canActivate: [AuthGuard] },
  {path: 'etl/report-validate', component: ReportValidateComponent},
  {path: 'etl/report-select-data', component: ReportSelectDataComponent},
  {path: 'etl/report-improve', component: ReportImproveComponent},
  {path: 'etl/report-not-found', component: ReportNotFoundComponent},
  {path: 'etl/report-not-found-detail', component: ReportNotFoundDetailComponent},
  {
    path: 'etl/report-data-error',
    loadChildren: './components/report-data-error/report-data-error.module#ReportDataErrorModule'
  },
  {
    path: 'etl/financial-institution',
    loadChildren: './components/financial-institution/financial-institution.module#FinancialInstitutionModule'
  },
  {
    path: 'etl/vat-rate',
    loadChildren: './components/vat-rate/vat-rate.module#VatRateModule'
  },
  {
    path: 'etl/vat-type',
    loadChildren: './components/vat-type/vat-type.module#VatTypeModule'
  },
  {
    path: 'etl/currency',
    loadChildren: './components/currency/currency.module#CurrencyModule'
  },
  {
    path: 'etl/country',
    loadChildren: './components/country/country.module#CountryModule'
  },
  {
    path: 'etl/discrepancy',
    loadChildren: './components/discrepancy/discrepancy.module#DiscrepancyModule'
  },
  {
    path: 'etl/crossborder-dta',
    loadChildren: './components/crossborder-dta/crossborder-dta.module#CrossborderDtaModule'
  },
  {
    path: 'etl/cross-border-income-type',
    loadChildren: './components/income-cross/income-cross.module#IncomeCrossModule'
  },
  {
    path: 'etl/domestic-income-type',
    loadChildren: './components/income-dom/income-dom.module#IncomeDomModule'
  },
  {
    path: 'etl/title-code',
    loadChildren: './components/title-code/title-code.module#TitleCodeModule'
  },
  {
    path: 'etl/province',
    loadChildren: './components/province/province.module#ProvinceModule'
  },
  {
    path: 'etl/amphur',
    loadChildren: './components/amphur/amphur.module#AmphurModule'
  },
  {
    path: 'etl/thambol',
    loadChildren: './components/thambol/thambol.module#ThambolModule'
  },
  {
    path: 'etl/postcode',
    loadChildren: './components/postcode/postcode.module#PostcodeModule'
  },
  {
    path: 'etl/section',
    loadChildren: './components/section/section.module#SectionModule'
  },
  {
    path: 'etl/officecode',
    loadChildren: './components/officecode/officecode.module#OfficecodeModule'
  },
  {
    path: 'etl/nid-gov-asso',
    loadChildren: './components/nid-gov-asso/nid-gov-asso.module#NidGovAssoModule'
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EtlRoutingModule {
}
