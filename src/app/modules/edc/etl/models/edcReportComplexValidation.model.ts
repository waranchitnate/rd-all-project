import {BaseModel} from './base.model';
import {UserInfoModel} from '../../mft/models/userInfo.model';
import {DataTypeModel} from '../../mft/models/dataType.model';
import {EdcfinancialInstitutionModel} from './edcFinancial-insitution.model';

export interface EdcReportComplexValidationModel extends BaseModel {

    senderId: number;
    fiCode: string;
    totalAmount: number;
    totalNotcalAmount: number;
    totalNotcalRecord: number;
    senderBankCode: string;
    dataTypeId: number;
    receiveDate: Date;
    paymentDate: Date;
    totalRecord: number;
    totalSuccessRecord: number;
    totalSuccessAmount: number;
    totalSuccessWhtAmount: number;
    totalSuccessVatAmount: number;
    totalFailRecord: number;
    totalFailAmount: number;
    totalFailWhtAmount: number;
    totalFailVatAmount: number;

    totalCalRecord: number;
    totalCalAmount: number;
    calWhtAmount: number;
    calVatAmount: number;
    diffWhtMinusAmount: number;
    diffWhtPosAmount: number;
    diffVatMinusAmount: number;
    diffVatPosAmount: number;
  // senderUserInfo: UserInfoModel;
    dataType: DataTypeModel;
    // edcFinancialCode: EdcfinancialInstitutionModel;

}
