import {BaseModel} from './base.model';
import {UserInfoModel} from '../../mft/models/userInfo.model';
import {DataTypeModel} from '../../mft/models/dataType.model';
import {EdcfinancialInstitutionModel} from './edcFinancial-insitution.model';

export interface EdcReportCrossValidationModel extends BaseModel {
  fiCode: string;
  // senderUserId: number;
  senderBankCode: string;
  dataTypeId: number;
  receiveDate: Date;
  paymentDate: string;
  totalRecord: number;
  totalFoundNidRecord: number;
  totalNotFoundNidRecord: number;
  totalVatRegisteredRecord: number;
  totalNotVatRegisteredRecord: number;
  totalFoundTeamRecord: number;
  totalNotFoundTeamRecord: number;
  totalNotVatRecord: number;

  dataTypeCode: String;
  // senderUserInfo: UserInfoModel;
  dataType: DataTypeModel;
  // edcFinancialCode: EdcfinancialInstitutionModel;
  rTotalRecord: number;
  rTotalFoundNidRecord: number;
  rTotalNotFoundNidRecord: number;
  rTotalVatRegisteredRecord: number;
  rTotalNotVatRegisteredRecord: number;
  rTotalFoundTeamRecord: number;
  rTotalNotFoundTeamRecord: number;
  rTotalNotVatRecord: number;

  taxFlag: string;
  trxFlag: string;
}
