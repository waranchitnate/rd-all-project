import { BaseModel } from './base.model';

export interface EdcDiscrepancyModel extends BaseModel {
  id: number;
    posAmount: number;
    minusAmount: number;
    effectiveDate: Date;
    endDate: Date;
    remark: string;
    discrepancyType: String;
}
