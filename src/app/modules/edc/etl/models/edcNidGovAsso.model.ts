import {BaseModel} from './base.model';

export interface EdcNidGovAssoModel extends BaseModel {
  id: number;
  taxId: string;
  taxPayerType: string;
  whtRateType: string;
  taxPayerName: string;
}
