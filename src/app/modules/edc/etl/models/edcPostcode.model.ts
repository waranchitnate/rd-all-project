import {BaseModel} from './base.model';
import {EdcProvinceModel} from './edcProvince.model';
import {EdcAmphurModel} from './edcAmphur.model';
import {EdcThambolModel} from './edcThambol.model';

export interface EdcPostcodeModel extends BaseModel {
  id: number;
  postcode: string;
  provinceId: number;
  amphurId: number;
  thambolId: number;
  comment: string;
  edcProvinceEntity: EdcProvinceModel;
  edcAmphurEntity: EdcAmphurModel;
  edcThambolEntity: EdcThambolModel;
  description: string;
  descriptionen: string;
  updatedate: string;
  provinceCode: string;
  amphurCode: string;
  thambolCode: string;
}
