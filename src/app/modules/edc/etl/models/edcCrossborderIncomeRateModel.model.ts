import { BaseModel } from './base.model';

export interface EdcCrossborderIncomeRateModel extends BaseModel {
  id: number;
    sectionId: number;
    crossIncomeId: number;
    agentTaxPayerType: string;
    taxPayerType: string;
    rateType: string;
    whtRate: number;
    pnd: string;
}
