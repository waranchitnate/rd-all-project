import { BaseModel } from './base.model';

export interface EdcfinancialInstitutionModel extends BaseModel {
  id: number;
    fiCode: string;
    fiNameTh: string;
    fiNameEn: string;
    activateDate: Date;
    closeDate: Date;
    remark: string;
    taxId: string;
}
