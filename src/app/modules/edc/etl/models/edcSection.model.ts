import { BaseModel } from './base.model';

export interface EdcSectionModel extends BaseModel {
    id: number;
    sectionCode: string;
    pnd: string;
    sectionSeq: number;
    sectionName: string;
    sectionDesc: string;
}
