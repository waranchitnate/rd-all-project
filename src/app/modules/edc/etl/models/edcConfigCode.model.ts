import { BaseModel } from './base.model';

export interface EdcConfigCodeModel extends BaseModel {
    id: number;
    sort: number;
    code: string;
    description: string;
    type: string;
}
