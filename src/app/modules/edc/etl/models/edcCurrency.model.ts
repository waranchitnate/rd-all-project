import {BaseModel} from './base.model';
import {EdcCountryModel} from './edcCountry.model';

export interface EdcCurrencyModel extends BaseModel {
  id: number;
    currencyCode: string;
    currencyNameTh: string;
    currencyNameEn: string;
    countryId: number;
    remark: string;
    unit: number;
    edcCountryEntity: EdcCountryModel;
}
