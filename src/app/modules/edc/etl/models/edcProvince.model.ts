import { BaseModel } from './base.model';

export interface EdcProvinceModel extends BaseModel {
    id: number;
    provinceCode: string;
    provinceNameTh: string;
    provinceNameEn: string;
    comment: string;
    updateDate: string;
}
