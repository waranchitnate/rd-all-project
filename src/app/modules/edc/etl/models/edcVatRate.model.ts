import { BaseModel } from './base.model';

export interface EdcVatRateModel extends BaseModel {
    id: number;
    vatName: string;
    vatRate: number;
    effectiveDate: Date;
    endDate: Date;
    remark: string;
}
