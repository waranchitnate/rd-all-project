import { BaseModel } from './base.model';

export interface EdcDomesticIncomeTypeRateModel extends BaseModel {
  id: number;
    sectionId: number;
    domesticIncomeId: number;
    agentTaxPayerType: string;
    taxPayerType: string;
    rateType: string;
    whtRate: number;
    pnd: string;
}
