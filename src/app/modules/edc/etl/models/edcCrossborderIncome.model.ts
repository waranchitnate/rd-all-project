import { BaseModel } from './base.model';
import {EdcCrossborderIncomeRateModel} from './edcCrossborderIncomeRateModel.model';

export interface EdcCrossBorderIncomeModel extends BaseModel {
    id: number;
    incomeCode: string;
    incomeNameTh: string;
    incomeNameEn: string;
    effectiveDate: Date;
    endDate: Date;
    isSelect: boolean;
    edcIncomeTypeRateList: EdcCrossborderIncomeRateModel[];
}
