import {BaseModel} from './base.model';
import {EdcProvinceModel} from './edcProvince.model';
import {EdcAmphurModel} from './edcAmphur.model';

export interface EdcThambolModel extends BaseModel {
  id: number;

  thambolCode: string;
  thambolNameTh: string;
  thambolNameEn: string;
  comment: string;
  provinceId: number;
  amphurId: number;
  edcProvinceEntity: EdcProvinceModel;
  edcAmphurEntity: EdcAmphurModel;
  updatedate: string;
  amphurCode: string;
  approvedBy: string;
}
