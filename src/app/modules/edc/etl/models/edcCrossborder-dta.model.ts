import { BaseModel } from './base.model';
import {EdcCountryModel} from './edcCountry.model.js';
import {EdcCrossBorderIncomeModel} from './edcCrossborderIncome.model';
import {EdcCrossBorderDtaIncomeModel} from './EdcCrossBorderDtaIncome.model';

export interface EdcCrossBorderDtaModel extends BaseModel {
    id: number;
    countryName: string;
    effectiveDate: Date;
    endDate: Date;
    edcCountryEntity: EdcCountryModel;
    edcCrossBorderIncomeTypeEntity: EdcCrossBorderIncomeModel;
    edcCrossDtaIncomeList: Array<EdcCrossBorderDtaIncomeModel>;
}
