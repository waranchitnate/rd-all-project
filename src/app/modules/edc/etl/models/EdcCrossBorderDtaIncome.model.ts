import { BaseModel } from './base.model';

import {EdcCrossBorderIncomeModel} from './edcCrossborderIncome.model';

export interface EdcCrossBorderDtaIncomeModel extends BaseModel {
    id: number;
    edcCrossBorderIncomeTypeEntity: EdcCrossBorderIncomeModel;
    dtaId: string;
    crossIncomeId: string;
    dtaRate: number;
}

