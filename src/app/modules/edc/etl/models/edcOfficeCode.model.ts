import {BaseModel} from './base.model';
import {EdcProvinceModel} from './edcProvince.model';
import {EdcAmphurModel} from './edcAmphur.model';
import {EdcThambolModel} from './edcThambol.model';

export interface EdcOfficeCodeModel extends BaseModel {
  id: number;
  rdOfficecode: string;
  provinceId: number;
  amphurId: number;
  prvOfficecode: string;
  comment: string;
  edcProvinceEntity: EdcProvinceModel;
  edcAmphurEntity: EdcAmphurModel;
  regionCode: string;
  officeType: string;
  updatedate: string;
  provinceCode: string;
  amphurCode: string;
  thambolCode: string;
  shotDescription: string;
  longDescription: string;
}
