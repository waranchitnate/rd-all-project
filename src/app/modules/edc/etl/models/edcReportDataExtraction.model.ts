import {BaseModel} from './base.model';

export interface EdcReportDataExtractionModel extends BaseModel {
  senderId: string;
  fiCode: string;
  senderBankCode: string;
  receiveDate: Date;
  paymentDate: Date;
  processingDate: Date;
  dataTypeCode: string;
  totalRecord: number;
  totalAmount: number;
  totalTaxInputRecord: number;
  totalTaxInputAmount: number;
  totalTaxOutputRecord: number;
  totalTaxOutputAmount: number;
  totalWhtAgentRecord: number;
  totalWhtAgentAmount: number;
  totalWhtTaxpayerRecord: number;
  totalWhtTaxpayerAmount: number;
  totalTaxInputEtaxRecord: number;
  totalTaxInputEtaxAmount: number;
  totalTaxOutputEtaxRecord: number;
  totalTaxOutputEtaxAmount: number;
  totalTaxSpendingRecord: number;
  totalTaxSpendingAmount: number;

  totalTaxSpendingWhtAmount: number;
  totalTaxSpendingVatAmount: number;

  totalNotExtRecord: number;
  totalNotExtAmount: number;
  totalExtRecord: number;
  totalExtAmount: number;


  // senderUserInfo: UserInfoModel;
  // edcFinancialCode: EdcfinancialInstitutionModel;

}

