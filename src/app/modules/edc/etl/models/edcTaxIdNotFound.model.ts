import {BaseModel} from './base.model';
import {UserInfoModel} from '../../mft/models/userInfo.model';
import {DataTypeModel} from '../../mft/models/dataType.model';

export interface EdcTaxIdNotFoundModel extends BaseModel {
    fiCode: string;
    seq: number;
    processingDetailId: string;
    processingId: string;
    // senderUserId: number;
    fileName: string;
    receiveDate: Date;
    taxId: string;
    referenceNo: string;
    taxFlag: string;
    trxFlag: string;
    description: string;
    type: string;
  dataTypeCode: string;
    // edcUserEntity: UserInfoModel;
    dataType: DataTypeModel;
}
