import {BaseModel} from './base.model';
import {EdcProvinceModel} from './edcProvince.model';

export interface EdcAmphurModel extends BaseModel {
  id: number;
  amphurCode: string;
  amphurNameTh: string;
  amphurNameEn: string;
  comment: string;
  provinceId: number;
  provinceCode: string;
  updateDate: string;
  edcProvinceEntity: EdcProvinceModel;
}
