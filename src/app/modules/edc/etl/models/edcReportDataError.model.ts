import {BaseModel} from './base.model';
import {UserInfoModel} from '../../mft/models/userInfo.model';
import {EdcfinancialInstitutionModel} from './edcFinancial-insitution.model';
import {EdcReportSimpleValidationErrorPKModel} from './edcReportSimpleValidationErrorPK';

export interface EdcReportDataErrorModel extends BaseModel {

    seq: number;
    processingDetailId: string;
    processingId: string;
    errorReason: string;
    edcReportSimpleValidationErrorPK: EdcReportSimpleValidationErrorPKModel;

}

