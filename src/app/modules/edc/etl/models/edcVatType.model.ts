import {BaseModel} from './base.model';

export interface EdcVatTypeModel extends BaseModel {
    id: number;
    vatTypeId: number;
    formType: string;
    seq: number;
    vatTypeName: string;
    subSeq: number;
    subVatTypeName: string;
}
