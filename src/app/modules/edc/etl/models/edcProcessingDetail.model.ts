import {BaseModel} from './base.model';
import {EdcConfigCodeModel} from './edcConfigCode.model';
import {EdcProcessingModel} from './edcProcessing.model';

export interface EdcProcessingDetailModel extends BaseModel {
    id: string;
    fiCode: string;
    processingId: number;
    fileMonitorDetailId: number;
    fileName: string;
    totalRecord: string;
    totalAmount: number;
    totalWhtAmount: number;
    totalVatAmount: number;
    inputSite: string;
    processState: string;
    etlReportPath: string;
    errorReason: string;
    status: string;
    approvedBy: string;
    approvedDate: Date;
    createdBy: string;
    createdDate: Date;
    updatedBy: string;
    updatedDate: Date;
    configCode: EdcConfigCodeModel;
    edcDataProcessingEntity: EdcProcessingModel;
}
