import { BaseModel } from './base.model';
import {EdcDomesticIncomeTypeRateModel} from './edcDomesticIncomeTypeRate.model';

export interface EdcDomesticIncomeTypeModel extends BaseModel {
    id: number;
    incomeCode: string;
    incomeNameTh: string;
    incomeNameEn: string;
    effectiveDate: Date;
    endDate: Date;
    isSelect: boolean;
    edcDomesticIncomeTypeRateList: EdcDomesticIncomeTypeRateModel[];
}
