import { BaseModel } from './base.model';

export interface EdcCountryModel extends BaseModel {
    id: number;
    countryCode2: string;
    countryCode3: string;
    countryNameTh: string;
    countryNameEn: string;
    countryCodeNid: string;
    remark: string;
}
