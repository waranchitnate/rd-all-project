import {BaseModel} from './base.model';
import {UserInfoModel} from '../../mft/models/userInfo.model';
import {EdcfinancialInstitutionModel} from './edcFinancial-insitution.model';

export interface EdcReportSimpleValidationErrorPKModel extends BaseModel {

  seq: number;
  processingDetailId: string;

}
