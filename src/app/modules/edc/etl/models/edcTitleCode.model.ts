import { BaseModel } from './base.model';

export interface EdcTitleCodeModel extends BaseModel {
    id: number;
    code: string;
    description: string;
    comment: string;
    taxpayerType: number;
}
