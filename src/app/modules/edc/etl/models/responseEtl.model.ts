export class ResponseEtl<T> {
    [x: string]: any;
    httpStatus: number;
    message: string;
    data: T;
}
