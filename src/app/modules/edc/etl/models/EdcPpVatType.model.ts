import { BaseModel } from './base.model';
import {EdcVatTypeModel} from './edcVatType.model';

export interface EdcPpVatTypeModel extends BaseModel {
  id: number;
    vatTypeId: number;
    effectiveDate: Date;
    edcVatTypeEntity: EdcVatTypeModel;
    formType: String;
}
