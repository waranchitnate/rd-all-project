import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EtlModule} from './etl/etl.module';
import {MftModule} from './mft/mft.module';
import {EdcRoutingModule} from './edc-routing.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        EdcRoutingModule,
        SharedModule,
        EtlModule,
        MftModule
    ]
})
export class EdcModule {
}
