import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MftRoutingModule} from './mft-routing.module';

import {SenderService} from './services/senders.service';
import {SchduleService} from './components/schedule/edcSchdule.service';
import {NotiTemplateService} from './components/notification-template/edcNotiTemplate.service';
import {EdcDataTypeService} from './components/data-type/edcDataType.service';
import {SenderTransferPushService} from './components/sender-transfer-push/sender-transfer-push.service';
import {AdminReportService} from './components/admin-report/admin-report.service';
import {ReleaseNoteComponent} from './components/release-note/release-note.component';
import {SenderTransferPullService} from './components/sender-transfer-pull/edcSenderTransferPull.service';
import {AlertTextModule} from './edcAlertText.module';
import {ConfigCodeService} from './edcConfigCode.service';

@NgModule({
  declarations: [
    ReleaseNoteComponent,
  ],
  imports: [
    CommonModule,
    MftRoutingModule,
    FormsModule,
    ReactiveFormsModule,

  ], providers: [
    SenderService,
    SchduleService,
    NotiTemplateService,
    EdcDataTypeService,
    SenderTransferPushService,
    ConfigCodeService,
    AdminReportService,
    SenderTransferPullService,
    AlertTextModule,
    DatePipe
  ]
})
export class MftModule {
}
