import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


import { Sender } from '../models/sender.model';
import {ResponseT} from '../../../../shared/models/response.model';
import {environment} from '../../../../../environments/environment';



@Injectable()
export class SenderService {
  url = environment.streamApiUrl;
  constructor(private http: HttpClient) { }

  getAllSenders(): Observable<ResponseT<Sender[]>> {
    return <Observable<ResponseT<Sender[]>>>this.http.get(this.url + 'users');
  }

  save(request: Sender): Observable<any> {
    return this.http.post(this.url, request);
  }
}
