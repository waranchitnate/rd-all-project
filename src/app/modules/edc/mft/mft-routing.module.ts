import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ReleaseNoteComponent} from './components/release-note/release-note.component';

const routes: Routes = [
  // { path: 'home', component: SenderTransferPushConfig, canActivate: [AuthGuard] },
  {
    path: 'mft/data-type',
    // path: '',
    loadChildren: './components/data-type/data-type.module#DataTypeModule'
  },
  {
    path: 'mft/schedule', loadChildren: './components/schedule/schedule.module#ScheduleModule'
  },
  {
    path: 'mft/sender-transfer-push',
    loadChildren: './components/sender-transfer-push/sender-transfer-push.module#SenderTransferPushModule'
  },
  {
    path: 'mft/sender-transfer-pull',
    loadChildren: './components/sender-transfer-pull/sender-transfer-pull.module#SenderTransferPullModule'
  },
  {
    path: 'mft/notification-template',
    loadChildren: './components/notification-template/notification-template.module#NotiTemplateModule'
  },
  {
    path: 'mft/report',
    loadChildren: './components/admin-report/admin-report.module#AdminReportModule'
  },
  {
    path: 'mft/release-note',
    component: ReleaseNoteComponent
  },
  {
    path: 'mft/config-code',
    loadChildren: './components/config-code/config-code.module#ConfigCodeModule'
  }

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MftRoutingModule {
}
