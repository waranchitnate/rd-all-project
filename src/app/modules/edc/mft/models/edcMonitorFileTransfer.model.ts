import {BaseModel} from './base.model';
import {UserInfoModel} from './userInfo.model';
import {DataTypeModel} from './dataType.model';
import {EdcMonitorFileTransferDetailModel} from './edcMonitorFileTransferDetail.model';
import {EdcfinancialInstitutionModel} from '../../etl/models/edcFinancial-insitution.model';

export interface EdcMonitorFileTransferModel extends BaseModel {
    id: string;
    totalSuccess: number;
    totalFailed: number;
    totalRecord: number;
    senderUserId: number;
    dataTypeId: number;
    receiveDate: Date;
    paymentDate: string;
    totalFiles: number;
    processState: string;
    inputSite: string;
    errorReason: string;
    ackPath: string;
    ackFileName: string;
    ackStatus: string;
    fiCode: string;
    monitorGroup: string;
    transferReceiveDate: Date;
    senderUserInfo: UserInfoModel;
    dataType: DataTypeModel;
    edcSenderMonitorFileTransferDetailList: EdcMonitorFileTransferDetailModel[];
    edcFinancialCodeEntity: EdcfinancialInstitutionModel[];
}
