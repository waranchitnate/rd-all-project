import {BaseModel} from './base.model';
import {UserInfoModel} from './userInfo.model';
import {DataTypeModel} from './dataType.model';

export interface EdcSenderConfigFileTransferModel extends BaseModel {
    id: number;
    fiCode: string;
    fiNameTh: string;
    titleName_firstName_lastName: string;
    fiNameTh_titleName_firstName_lastName: string;
    senderUserId: number;
    dataTypeId: number;
    senderEmailTo: string;
    senderEmailCc: string;
    senderEmailBcc: string;
    inputSite: string;
    edcUserEntity: UserInfoModel;
    edcDatatypeEntity: DataTypeModel;
}
