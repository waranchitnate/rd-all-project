import { BaseModel } from './base.model';

export interface EdcNotiTemplateModel extends BaseModel {
  id: number;
  subjectSender: string;
  subjectAdmin: string;
  contentSender: string;
  contentAdmin: string;
  attachment: string;
  emailAdminCc: string;
  emailAdminTo: string;
  state: string;
}
