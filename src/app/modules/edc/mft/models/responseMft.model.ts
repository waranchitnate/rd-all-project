export class ResponseMft<T> {
    [x: string]: any;
    httpStatus: number;
    message: string;
    data: T;
}
