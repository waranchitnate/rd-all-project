import {BaseModel} from './base.model';

export interface EdcConfigCodeModel extends BaseModel {
    id: number;
    code: string;
    description: string;
    type: string;
}

