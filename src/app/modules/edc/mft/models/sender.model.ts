import { BaseModel } from './base.model';

export interface Sender extends BaseModel {
  senderUserId: number;
  transferTemplateId: number;
  senderEmailTo: string;
  senderEmailCc: string;
  senderEmailBcc: string;
}
