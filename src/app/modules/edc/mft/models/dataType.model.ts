import { BaseModel } from './base.model';

export interface DataTypeModel extends BaseModel {
    id: number;
    dataTypeCode: String;
    dataTypeDesTh: String;
    dataTypeDesEn: String;
    fileSizeLimit: number;
    encPkiCert: String;
    isProcess: String;
    inputPath: String;
    statusPath: String;
    reportPath: String;
    certFile: File;
    certKey: String;
    passPhraseKey: String;
    patternFileNameType: String;
    patternFileName: String;
    dataTypeCodeAbb: String;
    sendEpayMentFlag: String;
    rootDirectoryEpayment: String;
    inputPathEpayment: String;
    sendReportEpayment: String;
    reportPathEpayment: String;
    certFileName: String;
}
