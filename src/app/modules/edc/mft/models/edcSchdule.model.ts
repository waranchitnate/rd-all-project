import { BaseModel } from './base.model';

export interface EdcScheduleModel extends BaseModel {
  id: number;
  scheduleName: string;
  scheduleType: string;
  dayOfmonth: string;
  intvFlag: string;
  intvTime: number;
  startTime: string;
  endTime: string;
  executeTime: string;
  dayOfWeek: string;
  dayOfMonth: string;
  dayOfYear: string;
}
