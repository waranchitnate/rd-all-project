import {BaseModel} from './base.model';
import { EdcProcessingDetailModel } from './edcProcessingDetail.model';
import {EdcMonitorFileTransferModel} from './edcMonitorFileTransfer.model';

export interface ReportMonitorError extends BaseModel {
    edcDataProcessingDetail: EdcProcessingDetailModel;
    edcTransferMonitors: EdcMonitorFileTransferModel;

}
