import {BaseModel} from './base.model';
import {EdcConfigCodeModel} from '../../etl/models/edcConfigCode.model';
import {EdcProcessingDetailModel} from '../../etl/models/edcProcessingDetail.model';

export interface EdcMonitorFileTransferDetailModel extends BaseModel {
    id: number;
    totalSuccess: number;
    totalFailed: number;
    fileMonitorId: number;
    senderUserId: number;
    inputSite: string;
    fileName: string;
    totalRecord: number;
    totalAmount: number;
    processState: string;
    errorReason: string;
    mftInputPath: string;
    etlPath: string;
    mftReportPath: string;
    mftEtlPath: string;
    mftDgPath: string;
    flagSendfileEtl: string;
    flagSendfileBigdata: string;
    monitorGroup: string;
    mftReconcilePath: string;
    flagSendfileReport: string;

    totalWhtAmount: number;
    totalVatAmount: number;
    statusCode: EdcConfigCodeModel;
    processStateCode: EdcConfigCodeModel;
}
