import { BaseModel } from './base.model';

export interface EdcMftSftpServerProfileModel extends BaseModel {
  id: number;
  serverName: string;
  hostName: string;
  port: string;
  url: string;
  retryCount: number;
  retryDelay: number;
}
