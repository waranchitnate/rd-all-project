import {BaseModel} from './base.model';

export interface UserInfoModel extends BaseModel {
  userId: number;
  nid: String;
  username: String;
  password: String;
  titleCode: String;
  titleName: String;
  nameEn: String;
  firstName: String;
  middleName: String;
  lastName: String;
  dataTypeCode: String;
  fiCode: String;
  businessAuthorityTitleCode: String;
  businessAuthorityTitleName: String;
  businessAuthorityFirstName: String;
  businessAuthorityLastName: String;
  dataStatus: String;
  emailTo: String;
  emailCc: String;
  activeStatus: String;
}

