import { BaseModel } from './base.model';

export interface HolidayModel extends BaseModel {
  holidayDate: Date;
  holidayName: String;
  comment: String;
}
