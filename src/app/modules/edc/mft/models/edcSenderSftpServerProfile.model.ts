import { BaseModel } from './base.model';
import {UserInfoModel} from './userInfo.model';

export interface EdcSenderSftpServerProfileModel extends BaseModel {
    titleName_firstName_lastName: string;
    fiNameTh_titleName_firstName_lastName: string;
    id: number;
    fiCode: string;
    fiNameTh: string;
    senderUserId: number;
    dataTypeId: number;
    scheduleId: number;
    serverName: string;
    network: string;
    hostName: string;
    port: number;
    serverKey: string;
    url: string;
    username: string;
    password: string;
    retryCount: number;
    retryDelay: number;
    inputPath: string;
    statusPath: string;
    reportPath: string;
    senderEmailTo: string;
    senderEmailCc: string;
    senderEmailBcc: string;
    verifiedPath: string;
    inputSite: string;
    protocol: string;
    edcUserEntity: UserInfoModel;
}
