export class BaseModel {
  // id: number;
  status: string;
  approvedBy: string;
  approvedDate: Date;
  createdBy: string;
  createdDate: Date;
  updatedBy: string;
  updatedDate: Date;

  constructor(json?: {
    // id?: number,
    status?: string,
    approvedBy?: string,
    approvedDate?: Date,
    createdBy?: string,
    createdDate?: Date,
    updatedBy?: string,
    updatedDate?: Date
}) {
    if (!json) {return; }
    // this.id = json.id;
    this.status = json.status;
    this.approvedBy = json.approvedBy;
    this.approvedDate = json.approvedDate;
    this.createdBy = json.createdBy;
    this.createdDate = json.createdDate;
    this.updatedBy = json.updatedBy;
    this.updatedDate = json.updatedDate;
}
}
