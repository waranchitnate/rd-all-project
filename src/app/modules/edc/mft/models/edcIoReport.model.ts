import { BaseModel } from './base.model';

export interface EdcIoReport extends BaseModel {
    dataTypeId: number;
    scheduleId: number;
    mftSftpServerId: number;
}
