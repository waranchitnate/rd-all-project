import {BaseModel} from './base.model';
import {EdcConfigCodeModel} from './edcConfigCode.model';
import {UserInfoModel} from '../../mft/models/userInfo.model';
import {DataTypeModel} from '../../mft/models/dataType.model';
import { EdcProcessingDetailModel } from './edcProcessingDetail.model';

export interface EdcProcessingModel extends BaseModel {
    id: string;
    fiCode: string;
    dataTypeId: number;
    errorReason: string;
    inputSite: string;
    paymentDate: string;
    processState: string;
    processingDate: Date;
    senderUserId: number;
    totalAmount: number;
    totalFiles: number;
    totalRecord: number;
    totalRecordAccount: number;
    totalVatAmount: number;
    totalWhtAmount: number;
    transferReceiveDate: Date;
    configCode: EdcConfigCodeModel;
    senderUserInfo: UserInfoModel;
    dataType: DataTypeModel;
    edcDataProcessingDetailList: EdcProcessingDetailModel[];
    edcDataProcessingDetail: EdcProcessingDetailModel;
}
