import {BaseModel} from './base.model';


export interface EdcMt940transModel extends BaseModel {
  seq: number;
  processingDetailId: String;
  processingId: String;
  bankCode: String;
  hearderOne: String;
  hearderTwo: String;
  hearderFour: String;
  referenceNo: String;
  accountNo: String;
  statementNo: String;
  dataTypeId: number;
  dataTypeCode: String;
  openBalanceType: String;
  openBalanceDate: String;
  openBalanceCurrency: String;
  openBalanceAmount: number;
  paymentDate: String;
  statementType: String;
  amount: number;
  openBalanceSeq: String;
  transactionCode: String;
  description: String;
  chqNo: String;
  branchCode: String;
  userId: String;
  refNo: String;
  comment: String;
  paymentDataDate: String;
  closeBalanceType: String;
  closeBalanceDate: String;
  closeBalanceCurrency: String;
  closeBalanceAmount: number;
  closeAvaiBalanceType: String;
  closeAvaiBalanceDate: String;
  closeAvaiBalanceCurrency: String;
  closeAvaiBalanceAmount: number;
  senderId: number;
  errorReason: String;
}
