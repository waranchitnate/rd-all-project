import { BaseModel } from './base.model';

export interface EdcTransferMonitorsDetail extends BaseModel {
    senderUserId: number;
    dataTypeId: number;
    receiveDate: number;
    paymentDate: string;
    totalFiles: number;
    processState: string;
    inputSite: string;
    errorReason: string;
}
