import { BaseModel } from './base.model';
import {DataTypeModel} from './dataType.model';
import {EdcScheduleModel} from './edcSchdule.model';
import {EdcMftSftpServerProfileModel} from './edcMftSftpServerProfile.model';

export interface EdcTranferDataTypeTemplateModel extends BaseModel {
    dataTypeId: number;
    scheduleId: number;
    mftSftpServerId: number;
    edcDataTypeEntity: DataTypeModel;
    edcScheduleEntity: EdcScheduleModel;
    edcMftSftpServerProfileEntity: EdcMftSftpServerProfileModel;
}
