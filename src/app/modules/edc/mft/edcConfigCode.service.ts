import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseMft} from './models/responseMft.model';
import {EdcConfigCodeModel} from '../etl/models/edcConfigCode.model';

@Injectable()
export class ConfigCodeService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    listConfig(): Observable<ResponseMft<EdcConfigCodeModel[]>> {
        return <Observable<ResponseMft<EdcConfigCodeModel[]>>>this.http.get(this.url + 'configCodes');
    }

    // Components ConfigCode
    list(request: any): Observable<ResponseMft<EdcConfigCodeModel[]>> {
      console.log(request);
      return <Observable<ResponseMft<EdcConfigCodeModel[]>>>this.http.post(this.url + 'configCodes', request);
    }
    listSelect(): Observable<ResponseMft<EdcConfigCodeModel[]>> {
      return <Observable<ResponseMft<EdcConfigCodeModel[]>>>this.http.get(this.url + 'configCodes');
    }

    save(request: EdcConfigCodeModel): Observable<any> {
      console.log(request);
      return this.http.post(this.url + 'configCode/add', request);
    }

    update(request: EdcConfigCodeModel): Observable<any> {
      console.log(request);
      return this.http.post(this.url + 'configCode/update', request);
    }

    delete(request: EdcConfigCodeModel): Observable<any> {
      console.log(request);
      return this.http.post(this.url + 'configCode/delete', request);
    }
}
