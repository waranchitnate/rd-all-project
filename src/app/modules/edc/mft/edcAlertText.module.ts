import {EdcDataTypeService} from './components/data-type/edcDataType.service';
import {ModalService} from '../../../core/services/modal.service';
import {Router} from '@angular/router';

export class AlertTextModule {
    private head: string;
    private tail: string;
    private _multiDuplicate: string;

    constructor() {
        this.head = 'ขออภัย ';
        this.tail = 'ซ้ำ กรุณาระบุใหม่';
        this._multiDuplicate = 'ขออภัย ข้อมูลนี้มีอยู่บนระบบแล้ว กรุณาระบุใหม่';
    }

    get getHead() {
        return this.head;
    }

    get getTail() {
        return this.tail;
    }

    get multiDuplicate(): string {
        return this._multiDuplicate;
    }
}
