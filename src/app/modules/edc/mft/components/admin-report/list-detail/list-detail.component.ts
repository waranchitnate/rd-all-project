import {Component, OnInit} from '@angular/core';
import {map} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {EdcMonitorFileTransferModel} from '../../../models/edcMonitorFileTransfer.model';
import {EdcConfigCodeModel} from '../../../../etl/models/edcConfigCode.model';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {EdcfinancialInstitutionModel} from '../../../../etl/models/edcFinancial-insitution.model';
import {FinancialInstitutionService} from '../../../../etl/components/financial-institution/edcfinancial-institution.service';
import {EdcMonitorFileTransferDetailModel} from '../../../models/edcMonitorFileTransferDetail.model';
import {EdcMt940transModel} from '../../../models/edcMt940trans.model';
import {AdminReportService} from '../admin-report.service';
import {EdcDataTypeService} from '../../data-type/edcDataType.service';
import {DataTypeModel} from '../../../models/dataType.model';
import {EdcProcessingDetailModel} from '../../../../etl/models/edcProcessingDetail.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ReportMonitorError} from '../../../models/reportMonitorError.model';
import {EdcProcessingModel} from '../../../../etl/models/edcProcessing.model';
@Component({
  selector: 'app-list-detail',
  templateUrl: './list-detail.component.html'
})
export class ListDetailComponent implements OnInit {

  errorConfigList = <EdcConfigCodeModel[]>[];
  ackConfigList = <EdcConfigCodeModel[]>[];
  processStateConfigList = <EdcConfigCodeModel[]>[];
  financialList = <EdcfinancialInstitutionModel[]>[];
  dataTypeList = <DataTypeModel[]>[];
  m940List = <EdcMt940transModel[]>[];
  state: Observable<EdcMonitorFileTransferModel>;
  edcDataProcessingObj: EdcMonitorFileTransferModel;
  edcProcessingDetail: EdcProcessingDetailModel;
  reportMonitorError: ReportMonitorError;
  ProcessingDetailForm: FormGroup;
  financialMap = new Map();
  ackAndErrorCodeConfigMap = new Map();
  dataTypeMap = new Map();
  processStateConfigMap = new Map();
  configCodeErrorMap = new Map();
  pageSize: number;
  page: number;
  total: number;
  nameFileError: string;
  statusList = {'00': 'สำเร็จ', '01': 'ไม่สำเร็จ', '02': 'สำเร็จ', '03': 'ไม่สำเร็จ', '04': 'ยกเลิก'};
  processList = {'00': 'Simple Validation', '01': 'Simple Validation', '02': 'Reconcile', '03': 'Reconcile', '04': 'Reconcile'};
  checkNoData: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private configCodeService: ConfigCodeService,
    private financialInstitutionService: FinancialInstitutionService,
    private adminReportService: AdminReportService,
    private datatypeService: EdcDataTypeService
  ) {
  }

  ngOnInit() {
    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe((res: EdcMonitorFileTransferModel) => {
      this.edcDataProcessingObj = res;
      this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList.filter(e => {
        e.totalSuccess = this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList.filter(a => a.status === 'ST003').length;
        e.totalFailed = this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList.filter(a => a.status === 'ST005').length;
      });
    });
    if (this.edcDataProcessingObj.id === undefined) {
      this.router.navigateByUrl('/mft/report');
    }
    console.log(this.edcDataProcessingObj);
    this.getListConfigCode();
    this.getListFinancial();
    this.getListDatatype();
    if ( this.edcDataProcessingObj.dataType.dataTypeCode === 'DT003' && this.edcDataProcessingObj.status === 'ST005') {
      this.getErrorSpecial();
    }
    if (this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList.length > 0 && this.edcDataProcessingObj.dataType.dataTypeCode === 'DT008' && this.edcDataProcessingObj.processState === 'PS007' && this.edcDataProcessingObj.status === 'ST003') {
      this.adminReportService.listMt940trans({processingDetailId: this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList[0].id}).subscribe(a => {
        this.m940List = a.data;
        // console.log(this.m940List);
      });
    }
    for (let i = 0 ; i < this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList.length ; i++) {
      if (this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList[i].status === 'ST008') {
        this.checkNoData = 'Y';
      }
    }
    console.log(this.edcDataProcessingObj);
  }

  onView(el: EdcMonitorFileTransferDetailModel) {
    // el.id
    this.adminReportService.listMt940trans({processingDetailId: el.id}).subscribe(a => {
      this.m940List = a.data;
      // console.log(this.m940List);
    });
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });

    // financialMap
    // console.log('fi ', this.financialList);
  }

  getListDatatype() {
    this.datatypeService.listSelect().subscribe(res => {
      this.dataTypeList = res.data;
      for (let i = 0; i < this.dataTypeList.length; i++) {
        this.dataTypeMap.set(this.dataTypeList[i].dataTypeCode, this.dataTypeList[i].dataTypeDesTh);
      }
    });
  }

  getErrorSpecial() {
      for (let i = 0; i < this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList.length; i++) {
          if (this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList[i].errorReason !== 'ER004') {
            if (this.nameFileError === undefined) {
              this.nameFileError = ' - ' + this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList[i].fileName;
            } else {
              this.nameFileError =  this.nameFileError + ' , ' + this.edcDataProcessingObj.edcSenderMonitorFileTransferDetailList[i].fileName;
            }
            console.log('this.nameFileError >', this.nameFileError);
          }
      }
  }

  getListConfigCode() {
    this.configCodeService.listConfig().subscribe(res => {
      this.errorConfigList = res.data.filter(e => e.type === 'RECONCILE_ERROR');
      for (let i = 0; i <= this.errorConfigList.length - 1; i++) {
        this.configCodeErrorMap.set(this.errorConfigList[i].code, this.errorConfigList[i].description);
      }

      this.ackConfigList = res.data.filter(e => e.type === 'ACK');
      for (let i = 0; i <= this.ackConfigList.length - 1; i++) {
        this.ackAndErrorCodeConfigMap.set(this.ackConfigList[i].code, this.ackConfigList[i].description);
      }

      this.ackConfigList = res.data.filter(e => e.type === 'ERROR_CODE');
      for (let i = 0; i <= this.ackConfigList.length - 1; i++) {
        this.ackAndErrorCodeConfigMap.set(this.ackConfigList[i].code, this.ackConfigList[i].description);
      }

      this.ackConfigList = res.data.filter(e => e.type === 'RECONCILE_ERROR');
      for (let i = 0; i <= this.ackConfigList.length - 1; i++) {
        this.ackAndErrorCodeConfigMap.set(this.ackConfigList[i].code, this.ackConfigList[i].description);
      }

      this.processStateConfigList = res.data.filter(e => e.type === 'PROCESS_STATE');
      for (let i = 0; i <= this.processStateConfigList.length - 1; i++) {
        this.processStateConfigMap.set(this.processStateConfigList[i].code, this.processStateConfigList[i].description);
      }
      // console.log(this.ackAndErrorCodeConfigMap);
    });
  }

  onViewError(el: EdcMonitorFileTransferDetailModel) {
    console.log( 'el >>' , el);
    // this.ProcessingDetailForm.value.edcProcessingDetail.fileMonitorDetailId = el.id;
    // this.edcProcessingDetail.fileMonitorDetailId = el.id;
    this.adminReportService.listProcessingDetail({fileMonitorDetailId : el.id}).subscribe(a => {
      console.log('data >>' , a.data);
      const reportMonitorErrorObj = {edcDataProcessingDetail: {}, edcTransferMonitors: {}};
      reportMonitorErrorObj.edcDataProcessingDetail = a.data[0];
      reportMonitorErrorObj.edcTransferMonitors = this.edcDataProcessingObj;
    // this.reportMonitorError.edcTransferMonitors = this.edcDataProcessingObj;
      console.log('reportMonitorError >>' , reportMonitorErrorObj);
      this.router.navigateByUrl('/mft/report/detail', {state: reportMonitorErrorObj});
    });
    console.log('EdcProcessingDetailModel Detail' , this.edcProcessingDetail);
  }

  onBack() {
    this.router.navigateByUrl('/mft/report');
  }

}
