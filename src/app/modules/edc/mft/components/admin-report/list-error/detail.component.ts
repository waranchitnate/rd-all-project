import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {ReportService} from '../../../../etl/edcReport.service';
import {EdcReportDataErrorModel} from '../../../../etl/models/edcReportDataError.model';
import {FinancialInstitutionService} from '../../../../etl/components/financial-institution/edcfinancial-institution.service';
import {EdcfinancialInstitutionModel} from '../../../../etl/models/edcFinancial-insitution.model';
import {EdcProcessingDetailModel} from '../../../../etl/models/edcProcessingDetail.model';
import {EdcMonitorFileTransferModel} from '../../../models/edcMonitorFileTransfer.model';
import {ReportMonitorError} from '../../../models/reportMonitorError.model';
import {state} from '@angular/animations';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

    state: Observable<ReportMonitorError>;
    edcReportComplexValidation: EdcProcessingDetailModel;
    edcMonitorFileTrans: EdcMonitorFileTransferModel;
    financialList = <EdcfinancialInstitutionModel[]>[];
    edcReportDataErrorList: EdcReportDataErrorModel[];
    edcReportDataErrorListLength: number;
    financialMap = new Map();
    pageSize: number;
    page: number;
    total: number;
    processingId: string;
    processingDetailId: string;
    tempObj: Object;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private reportService: ReportService,
        private financialInstitutionService: FinancialInstitutionService,
    ) {

    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe((res: ReportMonitorError) => {
          console.log('EdcProcessingDetailModel error' , res);
            this.edcReportComplexValidation = res.edcDataProcessingDetail;
            this.edcMonitorFileTrans = res.edcTransferMonitors;
            this.processingId = res.edcDataProcessingDetail.edcDataProcessingEntity.id;
            this.processingDetailId = res.edcDataProcessingDetail.id;
            this.tempObj = {
                processingId: this.processingId,
                processingDetailId: this.processingDetailId
            };
            this.getReport(this.tempObj);
        });
        if (this.edcReportComplexValidation.id === undefined) {
            this.router.navigateByUrl('/mft/report');
        }
        this.getListFinancial();
    }

    getPage(page: number) {
        this.tempObj = {
            processingId: this.processingId,
            processingDetailId: this.processingDetailId,
            page: page,
            pageSize: this.pageSize,
            total: this.total
        };
        this.getReport(this.tempObj);
    }

    getPageSize(pageSize: number) {
        this.tempObj = {
            processingId: this.processingId,
            processingDetailId: this.processingDetailId,
            page: this.page,
            pageSize: pageSize,
            total: this.total
        };
        this.getReport(this.tempObj);
    }

    getReport(param) {
        this.reportService.listReportDataError(param).subscribe(res => {
            console.log('edcReportDataErrorList >>', res);
            this.edcReportDataErrorList = res.data;
            this.page = res.page;
            this.total = res.total;
            this.pageSize = res.pageSize;
            this.edcReportDataErrorListLength = this.edcReportDataErrorList.length;
        });
    }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }

  onBack() {
    this.router.navigateByUrl('/mft/report/list-detail', {state : this.edcMonitorFileTrans});
  }
}
