import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseMft} from '../../models/responseMft.model';
import {EdcMonitorFileTransferModel} from '../../models/edcMonitorFileTransfer.model';
import {EdcMonitorFileTransferDetailModel} from '../../models/edcMonitorFileTransferDetail.model';
import {environment} from '../../../../../../environments/environment';
import {ResponseEtl} from '../../../etl/models/responseEtl.model';
import {EdcMt940transModel} from '../../models/edcMt940trans.model';
import {EdcProcessingDetailModel} from '../../../etl/models/edcProcessingDetail.model';

@Injectable({
  providedIn: 'root'
})
export class AdminReportService {

  url = environment.streamApiUrl;

  constructor(
      private http: HttpClient
  ) {
  }

  list(request: any): Observable<ResponseMft<EdcMonitorFileTransferModel[]>> {
    return <Observable<ResponseMft<EdcMonitorFileTransferModel[]>>>this.http.post(this.url + 'edcSenderMonitorFileTransfers', request);
  }

  listDetail(request: any): Observable<ResponseMft<EdcMonitorFileTransferDetailModel[]>> {
    return <Observable<ResponseMft<EdcMonitorFileTransferDetailModel[]>>>this.http.post(this.url + 'edcSenderMonitorFileTransfers', request);
  }

  listMt940trans(request: any): Observable<ResponseEtl<EdcMt940transModel[]>> {
    return <Observable<ResponseEtl<EdcMt940transModel[]>>>this.http.post(this.url + 'mt940trans', request);
  }

  listProcessingDetail(request: any): Observable<ResponseEtl<EdcProcessingDetailModel[]>> {
    return <Observable<ResponseEtl<EdcProcessingDetailModel[]>>>this.http.post(this.url + 'edcDataProcessingDetails/all', request);
  }
}
