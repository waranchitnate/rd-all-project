import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminReportRoutingModule } from './admin-report-routing.module';
import { ListComponent } from './list/list.component';
import { ListDetailComponent } from './list-detail/list-detail.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsDatepickerModule} from 'ngx-bootstrap-th';
import {PaginationModule, TypeaheadModule} from 'ngx-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';
import {SharedModule} from '../../../../../shared/shared.module';
import { DetailComponent } from '././list-error/detail.component';

@NgModule({
  declarations: [ListComponent, ListDetailComponent, DetailComponent],
  imports: [
    CommonModule,
    AdminReportRoutingModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    TypeaheadModule,
    NgxPaginationModule,
    PaginationModule,
    FormsModule,
    SharedModule
  ]
})
export class AdminReportModule { }
