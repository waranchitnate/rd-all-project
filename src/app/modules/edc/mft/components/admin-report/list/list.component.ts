import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {EdcDataTypeService} from '../../data-type/edcDataType.service';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {UserInfoModel} from '../../../models/userInfo.model';
import {DataTypeModel} from '../../../models/dataType.model';
import {EdcConfigCodeModel} from '../../../../etl/models/edcConfigCode.model';
import {SenderTransferPushService} from '../../sender-transfer-push/sender-transfer-push.service';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {EdcMonitorFileTransferModel} from '../../../models/edcMonitorFileTransfer.model';
import {AdminReportService} from '../admin-report.service';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {EdcfinancialInstitutionModel} from '../../../../etl/models/edcFinancial-insitution.model';
import {FinancialInstitutionService} from '../../../../etl/components/financial-institution/edcfinancial-institution.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  // styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  adminReportSearchForm: FormGroup;
  userInfoList = <UserInfoModel[]>[];
  financialList = <EdcfinancialInstitutionModel[]>[];
  dataTypeList = <DataTypeModel[]>[];
  masterConfigCodeList = <EdcConfigCodeModel[]>[];
  errorConfigCodeList = <EdcConfigCodeModel[]>[];
  ackConfigList = <EdcConfigCodeModel[]>[];
  processStateConfigList = <EdcConfigCodeModel[]>[];
  ackConfigAndErrorMap = new Map();
  processStateConfigMap = new Map();
  financialMap = new Map();
  edcMonitorFileTransList = <EdcMonitorFileTransferModel[]>[];
  // minDate: Date;
  edcCountryList: any;
  selectedOption: any;
  total: number;
  pageSize: number;
  page: number;
  totalFile: number;
  totalRecord: number;
  totalSuccess: number;
  totalFailed: number;
  paymentDate: string;

  constructor(
    private edcDataTypeService: EdcDataTypeService,
    private configCodeService: ConfigCodeService,
    private senderTransferPushService: SenderTransferPushService,
    private adminReportService: AdminReportService,
    private financialInstitutionService: FinancialInstitutionService,
    private router: Router,
  ) {
    // this.minDate = new Date();
    // this.minDate.setDate(this.minDate.getDate() - 30);
    this.totalFile = 0;
    this.totalRecord = 0;
    this.totalSuccess = 0;
    this.totalFailed = 0;
  }

  ngOnInit() {
    this.getListConfigCode();
    this.getListDataType();
    this.getListFinancial();
    this.getListEdcMonitorFileTrans({});
    this.initialForm();
    // console.log(this.errorConfigCodeList);
    // console.log(this.ackConfigMap);
    // console.log(this.processStateConfigMap);
    // console.log(this.edcDataProcessingList);
  }

  initialForm() {
    this.adminReportSearchForm = new FormGroup({
      adminReportSearchC: new FormGroup({
        'senderName': new FormControl(null),
        'senderId': new FormControl(null),
        'dataTypeId': new FormControl(null),
        'fiCode': new FormControl(null),
        'fiNameTh': new FormControl(null),
        'dateRange': new FormControl(null),
        'statusCode': new FormControl(null)
      })
    });
  }

  onSubmit() {
    if (this.adminReportSearchForm.get('adminReportSearchC').value.dateRange !== null) {
      this.adminReportSearchForm.value.adminReportSearchC.dateStart = moment(this.adminReportSearchForm.get('adminReportSearchC').value.dateRange[0]).format('YYYY-MM-DD');
      this.adminReportSearchForm.value.adminReportSearchC.dateEnd = moment(this.adminReportSearchForm.get('adminReportSearchC').value.dateRange[1]).format('YYYY-MM-DD');
    }
    this.getListEdcMonitorFileTrans(this.adminReportSearchForm.value.adminReportSearchC);
  }

  getListConfigCode() {
    this.configCodeService.listConfig().subscribe(res => {
      this.masterConfigCodeList = res.data.filter(e => e.type === 'STATUS');
      this.errorConfigCodeList = res.data.filter(e => e.type === 'ERROR_CODE');
      this.processStateConfigList = res.data.filter(e => e.type === 'PROCESS_STATE');
      this.ackConfigList = res.data.filter(e => e.type === 'ACK');

      for (let i = 0; i <= this.ackConfigList.length - 1; i++) {
        // console.log(this.ackConfigList[i]);
        this.ackConfigAndErrorMap.set(this.ackConfigList[i].code, this.ackConfigList[i].description);
      }
      // console.log(this.ackConfigMap);
      // console.log(this.ackConfigAndErrorMap);

      for (let i = 0; i <= this.errorConfigCodeList.length - 1; i++) {
        // console.log(this.errorConfigCodeList[i]);
        this.ackConfigAndErrorMap.set(this.errorConfigCodeList[i].code, this.errorConfigCodeList[i].description);
      }
      console.log(this.ackConfigAndErrorMap);
      for (let i = 0; i <= this.processStateConfigList.length - 1; i++) {
        // console.log(this.processStateConfigList[i]);
        this.processStateConfigMap.set(this.processStateConfigList[i].code, this.processStateConfigList[i].description);
      }
      // console.log(this.processStateConfigMap);
    });
  }

  getListDataType() {
    this.edcDataTypeService.listSelect().subscribe(res => {
      this.dataTypeList = res.data;
    });
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }

  // getListSenderUser() {
  //   this.senderTransferPushService.listUser().subscribe(result => {
  //     this.userInfoList = result.data;
  //   });
  // }

  getListEdcMonitorFileTrans(param) {
    console.log('param >>', param);
    this.adminReportService.list(param).subscribe(result => {
      this.edcMonitorFileTransList = result.data;
      this.page = result.page;
      this.total = result.total;
      this.pageSize = result.pageSize;
      // console.log(result.data);
      this.totalFile = result.data.reduce((totalSum, e) => totalSum + e.totalFiles, 0);
      this.totalRecord = result.data.reduce((totalSum, e) => totalSum + e.totalRecord, 0);
      this.totalSuccess = result.data.reduce((totalSum, e) => totalSum + e.totalSuccess, 0);
      this.totalFailed = result.data.reduce((totalSum, e) => totalSum + e.totalFailed, 0);

      // console.log('totalRecord ==> ' + this.totalRecord);
      // console.log('totalSuccess ==> ' + this.totalSuccess);
      // console.log('totalFailed ==> ' + this.totalFailed);

      for (let i = 0; i < this.edcMonitorFileTransList.length; i++) {
        // console.log('paymentDate ==>' + this.edcMonitorFileTransList[i].paymentDate);
        if (this.edcMonitorFileTransList[i].paymentDate != null) {
          this.paymentDate = this.edcMonitorFileTransList[i].paymentDate;
          if (this.paymentDate != null) {
            if (this.paymentDate.length === 8) {
              this.edcMonitorFileTransList[i].paymentDate = [this.paymentDate.slice(0, 4), '-', this.paymentDate.slice(4, 6), '-', this.paymentDate.slice(6)].join('');
              // [this.edcDataProcessingList[i].paymentDate.slice(0, 4), '-', this.edcDataProcessingList[i].paymentDate.slice(4, 6), '-', this.edcDataProcessingList[i].paymentDate.slice(6)].join('');
            } else if (this.paymentDate.length === 4) {
              this.edcMonitorFileTransList[i].paymentDate = this.paymentDate;
            }
          }
        }
      }

      // result.data.filter(e => {
      //   const g = this.errorConfigCodeList.filter(a => a.code === e.errorReason);
      //   if (g.length > 0) {
      //     e.errorReason = g[0].description;
      //   }
      // });
    });
  }

  resetForm() {
    this.adminReportSearchForm.reset();
    this.getListEdcMonitorFileTrans({});
  }

  onSelect(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.adminReportSearchForm.get('adminReportSearchC.senderId').setValue(this.selectedOption.userId);
  }

  onSelectFi(event: TypeaheadMatch): void {
    this.selectedOption = event.item;
    this.adminReportSearchForm.get('adminReportSearchC.fiCode').setValue(this.selectedOption.fiCode);
  }

  onView(el: EdcMonitorFileTransferModel) {
    // console.log(el);
    this.router.navigateByUrl('/mft/report/list-detail', {state: el});
  }

  getPage(page: number) {
    this.adminReportSearchForm.value.adminReportSearchC.page = page;
    this.getListEdcMonitorFileTrans(this.adminReportSearchForm.value.adminReportSearchC);
  }

  getPageSize(pageSize: number) {
    this.adminReportSearchForm.value.adminReportSearchC.pageSize = pageSize;
    this.getListEdcMonitorFileTrans(this.adminReportSearchForm.value.adminReportSearchC);
  }
}
