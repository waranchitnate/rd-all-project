import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from './list/list.component';
import {ListDetailComponent} from './list-detail/list-detail.component';
import { DetailComponent } from '././list-error/detail.component';

const routes: Routes = [
  {path: '', component: ListComponent},
  {path: 'list-detail', component: ListDetailComponent},
  { path: 'detail', component: DetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminReportRoutingModule { }
