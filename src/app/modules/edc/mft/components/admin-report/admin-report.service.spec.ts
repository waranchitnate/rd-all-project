import { TestBed } from '@angular/core/testing';

import { AdminReportService } from './admin-report.service';

describe('AdminReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminReportService = TestBed.get(AdminReportService);
    expect(service).toBeTruthy();
  });
});
