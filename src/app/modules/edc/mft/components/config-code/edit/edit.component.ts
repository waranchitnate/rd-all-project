import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {ModalService} from '../../../../../../core/services/modal.service';
import {map} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../edcAlertText.module';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    state$: Observable<EdcConfigCodeModel>;
    configCodeForm: FormGroup;
    configCodeList = <EdcConfigCodeModel[]>[];
    edcConfigCodeObj: EdcConfigCodeModel;

    constructor(
        private activatedRoute: ActivatedRoute,
        private configCodeService: ConfigCodeService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
      this.state$ = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
      this.state$.subscribe(e => {
        this.edcConfigCodeObj = e;
      });
        if (this.edcConfigCodeObj.id === undefined) {
            this.router.navigateByUrl('/mft/config-code');
        }
        this.initialForm();
    }

    resetForm() {
        this.configCodeForm.reset();
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลการจัดการรหัสของระบบยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                console.log(this.configCodeForm.value.configCodeObj);
                this.router.navigateByUrl('/mft/config-code');
                sub.unsubscribe();
            }
        });
    }

    onSubmit() {
        console.log(this.configCodeForm.value);

        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลการจัดการรหัสของระบบใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.configCodeForm.valid) {
                this.configCodeForm.value.configCodeObj.id = this.edcConfigCodeObj.id;
                this.configCodeService.update(this.configCodeForm.value.configCodeObj).subscribe(result => {
                        this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลการจัดการรหัสของระบบสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/mft/config-code');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสของระบบ' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

  initialForm() {
    this.configCodeForm = new FormGroup({
      configCodeObj: new FormGroup({
        'type': new FormControl(this.edcConfigCodeObj.type, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'code': new FormControl(this.edcConfigCodeObj.code, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]),
        'description': new FormControl(this.edcConfigCodeObj.description, Validators.required),
      })
    });
  }
}
