import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {EdcConfigCodeModel } from '../../../models/edcConfigCode.model';

import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {ConfigCodeService} from '../../../edcConfigCode.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    edcConfigCodeModelList = <EdcConfigCodeModel[]>[];
    configCodeSearchForm: FormGroup;
    page: number;
    total: number;
    pageSize: number;

    constructor(
        private configCodeService: ConfigCodeService,
        private modal: ModalService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        this.getList(this.configCodeSearchForm.value.configCodeSearchFormC);
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลการจัดการรหัสของระบบใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.configCodeService.delete(obj).subscribe(
                    response => {
                        // console.log(response);
                        this.getList(this.configCodeSearchForm.value.configCodeSearchFormC);
                        this.modal.openModal('แจ้งเตือน ', 'ลบข้อมูลการจัดการรหัสของระบบสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    onEdit(obj) {
        // console.log(obj);
        this.router.navigateByUrl('/mft/config-code/edit', {state: obj});
    }

    resetForm() {
        this.configCodeSearchForm.reset();
        this.initialForm();
        this.getList({});
    }

    getList(param) {
        this.configCodeService.list(param).subscribe(result => {
            // console.log(result.data);
            this.edcConfigCodeModelList = result.data;
            this.page = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.configCodeSearchForm.value.configCodeSearchFormC.page = page;
        // console.log(this.senderConfigSearchForm.value.senderConfigSearchC);
        this.getList(this.configCodeSearchForm.value.configCodeSearchFormC);
    }

    getPageSize(pageSize: number) {
        this.configCodeSearchForm.value.configCodeSearchFormC.pageSize = pageSize;
        // console.log(this.senderConfigSearchForm.value.senderConfigSearchC);
        this.getList(this.configCodeSearchForm.value.configCodeSearchFormC);
    }

    initialForm() {
        this.configCodeSearchForm = new FormGroup({
          configCodeSearchFormC: new FormGroup({
                'code': new FormControl(null),
                'description': new FormControl(null),
                'type': new FormControl(null)
            })
        });
    }

}
