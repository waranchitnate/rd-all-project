import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NotiTemplateService} from '../../notification-template/edcNotiTemplate.service';

import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Router} from '@angular/router';
import {AlertTextModule} from '../../../edcAlertText.module';
import {ConfigCodeService} from '../../../edcConfigCode.service';
import {EdcConfigCodeModel} from '../../../models/edcConfigCode.model';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    configCodeForm: FormGroup;
    edcConfigCodeModelList = <EdcConfigCodeModel[]>[];

    constructor(
        private configCodeService: ConfigCodeService,
        private modal: ModalService,
        private router: Router,
        private alertText: AlertTextModule
    ) {
    }

    ngOnInit() {
        this.initialForm();
        this.getListConfig();
    }

    resetForm() {
        this.configCodeForm.reset();
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลการจัดการรหัสของระบบยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                console.log(this.configCodeForm.value.configCodeObj);
                this.router.navigateByUrl('/mft/config-code');
                sub.unsubscribe();
            }
        });
    }

    onSubmit() {
        console.log(this.configCodeForm.value);

        const modalRef = this.modal.modalService.show(AnswerModalComponent);

        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลการจัดการรหัสของระบบใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.configCodeForm.valid ) {
                this.configCodeService.save(this.configCodeForm.value.configCodeObj).subscribe(result => {
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มการจัดการรหัสของระบบสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/mft/config-code');
                    },
                    err => {
                        if (err.error.errorCode === 'ST098') {
                            this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสของระบบ' + this.alertText.getTail);
                        } else {
                            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                        }
                    }
                    );
            }
            sub.unsubscribe();
        });
    }

    initialForm() {
        this.configCodeForm = new FormGroup({
          configCodeObj: new FormGroup({
                'type': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
                'code': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]),
                'description': new FormControl(null, Validators.required),
            })
        });
    }

    getListConfig() {
        this.configCodeService.listSelect().subscribe(result => {
            this.edcConfigCodeModelList = result.data;
        });
    }
}
