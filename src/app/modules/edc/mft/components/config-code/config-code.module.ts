import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ConfigCodeRoutingModule} from './config-code-routing.module';
import {AddComponent} from './add/add.component';
import {EditComponent} from './edit/edit.component';
import {ListComponent} from './list/list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {SharedModule} from '../../../../../shared/shared.module';

@NgModule({
    declarations: [AddComponent, EditComponent, ListComponent],
  imports: [
    CommonModule,
    ConfigCodeRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    SharedModule
  ]
})
export class ConfigCodeModule {
}
