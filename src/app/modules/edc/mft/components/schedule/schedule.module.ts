import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduleRoutingModule } from './schedule-routing.module';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {BsDatepickerModule, TimepickerModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../../../shared/shared.module';

@NgModule({
    declarations: [ListComponent, AddComponent, EditComponent],
  imports: [
    CommonModule,
    ScheduleRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    TimepickerModule.forRoot(),
    BsDatepickerModule,
    SharedModule
  ],
    providers: []
})
export class ScheduleModule { }
