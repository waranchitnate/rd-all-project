import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {environment} from '../../../../../../environments/environment';

import {EdcScheduleModel} from '../../models/edcSchdule.model';
import {ResponseMft} from '../../models/responseMft.model';


@Injectable()
export class SchduleService {
    url = environment.streamApiUrl;
    constructor(private http: HttpClient) { }

    list(request: any): Observable<ResponseMft<EdcScheduleModel[]>> {
        console.log(request);
        return <Observable<ResponseMft<EdcScheduleModel[]>>>this.http.post(this.url + 'schedules', request);
    }

    listSelect(): Observable<ResponseMft<EdcScheduleModel[]>> {
        return <Observable<ResponseMft<EdcScheduleModel[]>>>this.http.get(this.url + 'schedules');
    }

    save(request: EdcScheduleModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'schedule/add', request);
    }

    update(request: EdcScheduleModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'schedule/update', request);
    }

    delete(request: EdcScheduleModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'schedule/delete', request);
    }

}
