import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {EdcScheduleModel} from '../../../models/edcSchdule.model';
import {SchduleService} from '../edcSchdule.service';

import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';

import {map} from 'rxjs/operators';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../edcAlertText.module';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  state: Observable<EdcScheduleModel>;
  private edcSchedule: EdcScheduleModel;
  scheduleForm: FormGroup;

  endTime: Date;
  startTime: Date;
  executeTime: Date;
  dayOfWeekArray = [];
  dayOfWeekArrayTh = [];
  dayOfMonthArray = [];
  monthIndex = [];
  dayOfWeekChecked = new Map();
  dayOfMonthChecked = new Map();

  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private modal: ModalService,
    private schduleService: SchduleService,
    private alertText: AlertTextModule) {
  }

  ngOnInit() {
    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe(e => {
      this.edcSchedule = e;
    });
    if (this.edcSchedule.id === undefined) {
      this.router.navigateByUrl('/mft/schedule');
    }

    for (let i = 1; i <= 31; i++) {
      this.monthIndex.push(i);
    }
    this.initialTime();
    this.initialForm();
    // this.scheduleForm.get('scheduleGroup.dayOfWeekTh').setValue(this.edcSchedule.dayOfWeek.toString());
    console.log(this.edcSchedule);
    console.log(this.dayOfMonthChecked);
  }

  initialTime() {
    if (this.edcSchedule.startTime != null) {
      this.startTime = new Date('01-01-2000 ' + this.edcSchedule.startTime + ':00');
    }
    if (this.edcSchedule.endTime != null) {
      this.endTime = new Date('01-01-2000 ' + this.edcSchedule.endTime + ':00');
    }
    if (this.edcSchedule.executeTime != null) {
      this.executeTime = new Date('01-01-2000 ' + this.edcSchedule.executeTime + ':00');
    }
  }

  initialForm() {
    this.scheduleForm = new FormGroup({
      scheduleGroup: new FormGroup({
        'scheduleName': new FormControl(this.edcSchedule.scheduleName, Validators.required),
        'scheduleType': new FormControl(this.edcSchedule.scheduleType, Validators.required),
        'intvFlag': new FormControl(this.edcSchedule.intvFlag.toString()),
        'startTime': new FormControl(this.edcSchedule.startTime),
        'endTime': new FormControl(this.edcSchedule.endTime),
        'intvTime': new FormControl(this.edcSchedule.intvTime),
        'executeTime': new FormControl(this.edcSchedule.executeTime),
        'dayOfWeek': new FormControl(this.edcSchedule.dayOfWeek),
        'dayOfWeekTh': new FormControl(null),
        'dayOfMonth': new FormControl(this.edcSchedule.dayOfMonth),
        'dayOfYear': new FormControl(this.edcSchedule.dayOfYear),
        'createdDate': new FormControl(this.edcSchedule.createdDate),
        'createdBy': new FormControl(this.edcSchedule.createdBy)
      })

    });
    if (this.edcSchedule.dayOfWeek != null) {
      this.dayOfWeekArray = this.edcSchedule.dayOfWeek.toString().split(',');

      this.dayOfWeekArrayTh = this.edcSchedule.dayOfWeek.split(',');

      for (let i = 0; i < this.dayOfWeekArrayTh.length; i++) {
        console.log(this.dayOfWeekArrayTh[i]);
        this.dayOfWeekChecked.set(this.dayOfWeekArrayTh[i], true);
      }
    }
    if (this.edcSchedule.dayOfMonth != null) {

      this.dayOfMonthArray = this.edcSchedule.dayOfMonth.split(',');

      for (let i = 0; i < this.dayOfMonthArray.length; i++) {
        console.log(this.dayOfMonthArray[i]);
        this.dayOfMonthChecked.set(this.dayOfMonthArray[i], true);
      }
    }
  }

  onScheduleTypeReset() {
    this.scheduleForm.get('scheduleGroup.dayOfWeek').setValidators(null);
    this.scheduleForm.get('scheduleGroup.dayOfMonth').setValidators(null);
    this.scheduleForm.get('scheduleGroup.dayOfYear').setValidators(null);

    this.scheduleForm.get('scheduleGroup.dayOfWeek').setValue(null);
    this.scheduleForm.get('scheduleGroup.dayOfWeekTh').setValue(null);
    this.scheduleForm.get('scheduleGroup.dayOfMonth').setValue(null);
    this.scheduleForm.get('scheduleGroup.dayOfYear').setValue(null);

    this.dayOfWeekChecked.clear();
    this.dayOfMonthChecked.clear();

    this.dayOfWeekArray = [];
    this.dayOfMonthArray = [];

    if (this.scheduleForm.get('scheduleGroup.scheduleType').value === 'Current') {
      this.scheduleForm.get('scheduleGroup.intvFlag').setValue(1);
      this.scheduleForm.get('scheduleGroup.startTime').reset();
      this.scheduleForm.get('scheduleGroup.endTime').reset();
      this.scheduleForm.get('scheduleGroup.intvTime').reset();
      this.scheduleForm.get('scheduleGroup.executeTime').reset();
      this.scheduleForm.get('scheduleGroup.dayOfWeek').reset();
      this.scheduleForm.get('scheduleGroup.dayOfWeekTh').reset();
      this.scheduleForm.get('scheduleGroup.dayOfMonth').reset();
      this.scheduleForm.get('scheduleGroup.dayOfYear').reset();
    }
    if (this.scheduleForm.get('scheduleGroup.scheduleType').value === 'DayOfWeek') {
      console.log('set validate DayOfWeek');
      this.scheduleForm.get('scheduleGroup.dayOfWeek').setValidators(Validators.required);
    }
    if (this.scheduleForm.get('scheduleGroup.scheduleType').value === 'DayOfMonth') {
      this.scheduleForm.get('scheduleGroup.dayOfMonth').setValidators(Validators.required);
    }
    if (this.scheduleForm.get('scheduleGroup.scheduleType').value === 'dayOfYear') {
      this.scheduleForm.get('scheduleGroup.dayOfYear').setValidators(Validators.required);
    }
    this.scheduleForm.get('scheduleGroup.startTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.endTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.intvTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.executeTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.dayOfWeek').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.dayOfWeekTh').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.dayOfMonth').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.dayOfYear').updateValueAndValidity();
  }

  onCheckedMonth(inputMonth: any) {
    const inpMonthDay = inputMonth.target.defaultValue.toString();
    const found = this.dayOfMonthArray.find(element => element === inpMonthDay);

    if (found != null) {
      this.dayOfMonthArray.forEach((item, index) => {
        if (item === inpMonthDay) {
          this.dayOfMonthArray.splice(index, 1);
        }
      });
      this.scheduleForm.get('scheduleGroup.dayOfMonth').setValue(this.dayOfMonthArray.sort((a, b) => a.value - b.value).toString());
    } else {
      this.dayOfMonthArray.push(inpMonthDay);
      this.scheduleForm.get('scheduleGroup.dayOfMonth').setValue(this.dayOfMonthArray.sort((a, b) => a.value - b.value).toString());
    }
  }

  resetForm() {
    this.scheduleForm.reset();
  }

  onCheckedWeek(inputWeek: any) {
    // console.log(inputWeek.target.defaultValue);

    const inpWeekDay = inputWeek.target.defaultValue.toString();
    const found = this.dayOfWeekArray.find(element => element === inpWeekDay);

    if (found != null) {
      this.dayOfWeekArray.forEach((item, index) => {
        if (item === inpWeekDay) {
          this.dayOfWeekArray.splice(index, 1);
        }
      });
      this.scheduleForm.get('scheduleGroup.dayOfWeek').setValue(this.dayOfWeekArray.toString());
    } else {
      this.dayOfWeekArray.push(inpWeekDay);
      this.scheduleForm.get('scheduleGroup.dayOfWeek').setValue(this.dayOfWeekArray.toString());
    }
  }

  onSubmit() {
    // console.log(this.edcSchedule);
    // console.log(this.scheduleForm.valid);
    this.scheduleForm.get('scheduleGroup.startTime').clearValidators();
    this.scheduleForm.get('scheduleGroup.endTime').clearValidators();
    this.scheduleForm.get('scheduleGroup.intvTime').clearValidators();
    this.scheduleForm.get('scheduleGroup.executeTime').clearValidators();

    this.scheduleForm.get('scheduleGroup.startTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.endTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.intvTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.executeTime').updateValueAndValidity();

    if (this.scheduleForm.get('scheduleGroup.intvFlag').value === '2') {
      this.scheduleForm.get('scheduleGroup.executeTime').setValidators(Validators.required);
      this.scheduleForm.get('scheduleGroup.executeTime').updateValueAndValidity();
    }
    if (this.scheduleForm.get('scheduleGroup.intvFlag').value === '1' && this.scheduleForm.get('scheduleGroup.scheduleType').value !== 'Current') {
      this.scheduleForm.get('scheduleGroup.startTime').setValidators(Validators.required);
      this.scheduleForm.get('scheduleGroup.startTime').updateValueAndValidity();

      this.scheduleForm.get('scheduleGroup.endTime').setValidators(Validators.required);
      this.scheduleForm.get('scheduleGroup.endTime').updateValueAndValidity();

      this.scheduleForm.get('scheduleGroup.intvTime').setValidators(Validators.required);
      this.scheduleForm.get('scheduleGroup.intvTime').updateValueAndValidity();
    }

    this.scheduleForm.value.scheduleGroup.id = this.edcSchedule.id;
    console.log(this.edcSchedule.id);
    console.log(this.scheduleForm.value.scheduleGroup.id);
    if (this.startTime != null) {
      this.scheduleForm.value.scheduleGroup.startTime = this.startTime.getHours() + ':' + this.startTime.getMinutes();
    }
    if (this.endTime != null) {
      this.scheduleForm.value.scheduleGroup.endTime = this.endTime.getHours() + ':' + this.endTime.getMinutes();
    }
    if (this.executeTime != null) {
      this.scheduleForm.value.scheduleGroup.executeTime = this.executeTime.getHours() + ':' + this.executeTime.getMinutes();
    }


    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของตารางเวลาการทำงานใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.scheduleForm.valid) {
        this.scheduleForm.value.scheduleGroup.id = this.edcSchedule.id;
        this.schduleService.update(this.scheduleForm.value.scheduleGroup).subscribe(
          response => {
            console.log(response);
            this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลตารางเวลาการทำงานสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('/mft/schedule');
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'ชื่อตารางเวลาการทำงาน' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
        sub.unsubscribe();
      }
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลตารางเวลาการทำงานยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.router.navigateByUrl('/mft/schedule');
        sub.unsubscribe();
      }
    });
  }
}
