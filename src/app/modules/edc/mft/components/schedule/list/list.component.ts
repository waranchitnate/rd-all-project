import {SchduleService} from '../edcSchdule.service';
import {EdcScheduleModel} from '../../../models/edcSchdule.model';

import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  edcSchedule = <EdcScheduleModel[]>[];
  scheduleForm: FormGroup;
  scheduleTypeList = {Current: 'ดำเนินการทันที', Daily: 'ทุกวัน', DayOfWeek: 'วันในสัปดาห์', DayOfMonth: 'วันในเดือน', DayOfYear: 'วันในปี'};
  p: number;
  total: number;
  pageSize: number;

  constructor(
    private schduleService: SchduleService,
    private modal: ModalService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.getList({});
    this.initialForm();
  }

  onSubmit() {
    this.getList(this.scheduleForm.value.scheduleGroup);
  }

  initialForm() {
    this.scheduleForm = new FormGroup({
      scheduleGroup: new FormGroup({
        'scheduleName': new FormControl(null),
        'scheduleType': new FormControl(null),
      })
    });
  }

  resetForm() {
    this.scheduleForm.reset();
    this.initialForm();
    this.getList(this.scheduleForm.value.scheduleGroup);
  }


  getList(param) {
    this.schduleService.list(param).subscribe(result => {
      // console.log(result.data);
      this.edcSchedule = result.data;
      this.p = result.page;
      this.total = result.total;
      this.pageSize = result.pageSize;
    });
  }

  getPage(page: number) {
    this.scheduleForm.value.scheduleGroup.page = page;
    console.log(this.scheduleForm.value.scheduleGroup);
    // this.getList(this.scheduleForm.value.scheduleGroup);
  }

  onEdit(obj) {
    // console.log(obj);
    this.router.navigateByUrl('/mft/schedule/edit', {state: obj});
  }

  onDelete(obj) {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลตารางเวลาการทำงานใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.schduleService.delete(obj).subscribe(
          response => {
            // console.log(response);
            this.getList(this.scheduleForm.value.scheduleGroup);
            this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลตารางเวลาการทำงานสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
          },
          err => {
            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
          }
        );
      }
      sub.unsubscribe();
    });
  }

  getPageSize(pageSize: number) {
    this.scheduleForm.value.scheduleGroup.pageSize = pageSize;
    this.getList(this.scheduleForm.value.scheduleGroup);
  }
}
