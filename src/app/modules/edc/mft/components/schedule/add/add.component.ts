import {SchduleService} from '../edcSchdule.service';

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Router} from '@angular/router';
import {AlertTextModule} from '../../../edcAlertText.module';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  scheduleForm: FormGroup;
  optionSelected: string;

  endTime: Date;
  startTime: Date;
  executeTime: Date;
  dayOfWeekArray = [];
  dayOfMonthArray = [];
  monthIndex = [];

  constructor(
    private modal: ModalService,
    private schduleService: SchduleService,
    private router: Router,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    // this.monthIndex = Array(31);
    for (let i = 1; i <= 31; i++) {
      this.monthIndex.push(i);
    }
    this.initialForm();
  }

  initialForm() {
    this.optionSelected = 'Daily';

    this.scheduleForm = new FormGroup({
      scheduleGroup: new FormGroup({
        'scheduleName': new FormControl(null, Validators.required),
        'scheduleType': new FormControl('Daily', Validators.required),
        'intvFlag': new FormControl('2', Validators.required),
        'startTime': new FormControl(null),
        'endTime': new FormControl(null),
        'intvTime': new FormControl(null),
        'executeTime': new FormControl(null),
        'dayOfWeek': new FormControl(null),
        'dayOfWeekTh': new FormControl(null),
        'dayOfMonth': new FormControl(null),
        'dayOfYear': new FormControl(null),
      })
    });
  }

  onScheduleTypeReset() {
    this.scheduleForm.get('scheduleGroup.dayOfWeek').setValidators(null);
    this.scheduleForm.get('scheduleGroup.dayOfMonth').setValidators(null);
    this.scheduleForm.get('scheduleGroup.dayOfYear').setValidators(null);

    this.scheduleForm.get('scheduleGroup.dayOfWeek').setValue(null);
    this.scheduleForm.get('scheduleGroup.dayOfWeekTh').setValue(null);
    this.scheduleForm.get('scheduleGroup.dayOfMonth').setValue(null);
    this.scheduleForm.get('scheduleGroup.dayOfYear').setValue(null);

    // this.dayOfWeekChecked.clear();
    // this.dayOfMonthChecked.clear();

    this.dayOfWeekArray = [];
    this.dayOfMonthArray = [];

    if (this.scheduleForm.get('scheduleGroup.scheduleType').value === 'Current') {
      this.scheduleForm.get('scheduleGroup.intvFlag').setValue(1);
      this.scheduleForm.get('scheduleGroup.startTime').reset();
      this.scheduleForm.get('scheduleGroup.endTime').reset();
      this.scheduleForm.get('scheduleGroup.intvTime').reset();
      this.scheduleForm.get('scheduleGroup.executeTime').reset();
      this.scheduleForm.get('scheduleGroup.dayOfWeek').reset();
      this.scheduleForm.get('scheduleGroup.dayOfWeekTh').reset();
      this.scheduleForm.get('scheduleGroup.dayOfMonth').reset();
      this.scheduleForm.get('scheduleGroup.dayOfYear').reset();
    }
    if (this.scheduleForm.get('scheduleGroup.scheduleType').value === 'DayOfWeek') {
      console.log('set validate DayOfWeek');
      this.scheduleForm.get('scheduleGroup.dayOfWeek').setValidators(Validators.required);
    }
    if (this.scheduleForm.get('scheduleGroup.scheduleType').value === 'DayOfMonth') {
      this.scheduleForm.get('scheduleGroup.dayOfMonth').setValidators(Validators.required);
    }
    if (this.scheduleForm.get('scheduleGroup.scheduleType').value === 'DayOfYear') {
      console.log('pass day of year');
      this.scheduleForm.get('scheduleGroup.dayOfYear').setValidators(Validators.required);
    }
    this.scheduleForm.get('scheduleGroup.startTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.endTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.intvTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.executeTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.dayOfWeek').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.dayOfWeekTh').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.dayOfMonth').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.dayOfYear').updateValueAndValidity();
  }

  onCheckedMonth(inputMonth: any) {
    const inpMonthDay = inputMonth.target.defaultValue.toString();
    const found = this.dayOfMonthArray.find(element => element === inpMonthDay);

    if (found != null) {
      this.dayOfMonthArray.forEach((item, index) => {
        if (item === inpMonthDay) {
          this.dayOfMonthArray.splice(index, 1);
        }
      });
      this.scheduleForm.get('scheduleGroup.dayOfMonth').setValue(this.dayOfMonthArray.sort((a, b) => a.value - b.value).toString());
    } else {
      this.dayOfMonthArray.push(inpMonthDay);
      this.scheduleForm.get('scheduleGroup.dayOfMonth').setValue(this.dayOfMonthArray.sort((a, b) => a.value - b.value).toString());
    }
  }

  onCheckedWeek(inputWeek: any) {
    // console.log(inputWeek.target.defaultValue);

    const inpWeekDay = inputWeek.target.defaultValue.toString();
    const found = this.dayOfWeekArray.find(element => element === inpWeekDay);

    if (found != null) {
      this.dayOfWeekArray.forEach((item, index) => {
        if (item === inpWeekDay) {
          this.dayOfWeekArray.splice(index, 1);
        }
      });
      this.scheduleForm.get('scheduleGroup.dayOfWeek').setValue(this.dayOfWeekArray.toString());
    } else {
      this.dayOfWeekArray.push(inpWeekDay);
      this.scheduleForm.get('scheduleGroup.dayOfWeek').setValue(this.dayOfWeekArray.toString());
    }
    // this.scheduleForm.get('scheduleGroup.dayOfWeek').setValue(this.dayOfWeekArray.toString()
    //   .replace('วันจันทร์', 'Monday')
    //   .replace('วันอังคาร', 'Tuesday')
    //   .replace('วันพุธ', 'Wednesday')
    //   .replace('วันพฤหัสบดี', 'Thursday')
    //   .replace('วันศุกร์', 'Friday')
    //   .replace('วันเสาร์', 'Saturday')
    //   .replace('วันอาทิตย์', 'Sunday')
    // );
    console.log(this.dayOfWeekArray);
    console.log(this.scheduleForm);
    // this.scheduleForm.value.scheduleGroup.dayOfMonth = 'asd';
    // this.dayOfWeekArray.push(element.value);
    // this.scheduleForm.get('scheduleGroup.dayOfWeek').setValue('b2');
    // console.log('${element.value} was');
    // document.getElementById('dayOfWeek').innerText = 'aaaaaaaaaaaaaaa';
  }

  resetForm() {
    this.scheduleForm.reset();
    this.dayOfWeekArray = [];
  }

  onSubmit() {
    // console.log(this.scheduleForm.value);
    // console.log(this.scheduleForm.valid);
    this.scheduleForm.get('scheduleGroup.startTime').clearValidators();
    this.scheduleForm.get('scheduleGroup.endTime').clearValidators();
    this.scheduleForm.get('scheduleGroup.intvTime').clearValidators();
    this.scheduleForm.get('scheduleGroup.executeTime').clearValidators();

    this.scheduleForm.get('scheduleGroup.startTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.endTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.intvTime').updateValueAndValidity();
    this.scheduleForm.get('scheduleGroup.executeTime').updateValueAndValidity();

    if (this.scheduleForm.get('scheduleGroup.intvFlag').value === '2') {
      this.scheduleForm.get('scheduleGroup.executeTime').setValidators(Validators.required);
      this.scheduleForm.get('scheduleGroup.executeTime').updateValueAndValidity();
    }
    if (this.scheduleForm.get('scheduleGroup.intvFlag').value === '1' && this.scheduleForm.get('scheduleGroup.scheduleType').value !== 'Current') {
      this.scheduleForm.get('scheduleGroup.startTime').setValidators(Validators.required);
      this.scheduleForm.get('scheduleGroup.startTime').updateValueAndValidity();

      this.scheduleForm.get('scheduleGroup.endTime').setValidators(Validators.required);
      this.scheduleForm.get('scheduleGroup.endTime').updateValueAndValidity();

      this.scheduleForm.get('scheduleGroup.intvTime').setValidators(Validators.required);
      this.scheduleForm.get('scheduleGroup.intvTime').updateValueAndValidity();
    }

    if (this.startTime != null) {
      this.scheduleForm.value.scheduleGroup.startTime = this.startTime.getHours() + ':' + this.startTime.getMinutes();
    }
    if (this.endTime != null) {
      this.scheduleForm.value.scheduleGroup.endTime = this.endTime.getHours() + ':' + this.endTime.getMinutes();
    }
    if (this.executeTime != null) {
      this.scheduleForm.value.scheduleGroup.executeTime = this.executeTime.getHours() + ':' + this.executeTime.getMinutes();
    }


    console.log(this.scheduleForm.value.scheduleGroup);
    console.log(this.scheduleForm.valid);
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลตารางเวลาการทำงานใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.scheduleForm.valid) {
        this.schduleService.save(this.scheduleForm.value.scheduleGroup).subscribe(
          response => {
            console.log(response);
            this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลตารางเวลาการทำงานสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('/mft/schedule');
          },
          err => {
            if (err.error.errorCode === 'ST098') {
              this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'ชื่อตารางเวลาการทำงาน' + this.alertText.getTail);
            } else {
              this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
            }
          }
        );
      }
      sub.unsubscribe();
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลตารางเวลาการทำงานยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.router.navigateByUrl('/mft/schedule');
        sub.unsubscribe();
      }
    });
  }
}
