import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {EditComponent} from './edit/edit.component';
import {AddComponent} from './add/add.component';
import {ListComponent} from './list/list.component';
import {SenderTransferPullRoutingModule} from './sender-transfer-pull-routing.module';
import {TypeaheadModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../../../shared/shared.module';
import {NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [EditComponent, AddComponent, ListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    SenderTransferPullRoutingModule,
    TypeaheadModule,
    SharedModule,
    NgbTypeaheadModule,
  ]
})
export class SenderTransferPullModule {
}
