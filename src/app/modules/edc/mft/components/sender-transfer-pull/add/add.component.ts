import {Component, OnInit} from '@angular/core';
import {EdcScheduleModel} from '../../../models/edcSchdule.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {SenderTransferPullService} from '../edcSenderTransferPull.service';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcDataTypeService} from '../../data-type/edcDataType.service';
import {SenderTransferPushService} from '../../sender-transfer-push/sender-transfer-push.service';
import {EdcSenderConfigFileTransferModel} from '../../../models/edcSenderConfigFileTransfer.model';
import {TypeaheadMatch} from 'ngx-bootstrap';
import {SchduleService} from '../../schedule/edcSchdule.service';
import {EdcfinancialInstitutionModel} from '../../../../etl/models/edcFinancial-insitution.model';
import {FinancialInstitutionService} from '../../../../etl/components/financial-institution/edcfinancial-institution.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  pullForm: FormGroup;
  financialForm: FormGroup;
  edcSchedule = <EdcScheduleModel[]>[];
  senderTransferPushList = <EdcSenderConfigFileTransferModel[]>[];
  datatypeUserList = <EdcSenderConfigFileTransferModel[]>[];
  userList = <EdcSenderConfigFileTransferModel[]>[];
  selectedOptionUser: EdcSenderConfigFileTransferModel;
  arrayUser = <EdcSenderConfigFileTransferModel[]>[];

  financialList = <EdcfinancialInstitutionModel[]>[];
  financialMap = new Map();

  constructor(
    private modal: ModalService,
    private pullService: SenderTransferPullService,
    private edcDataTypeService: EdcDataTypeService,
    private senderTransferPushService: SenderTransferPushService,
    private financialInstitutionService: FinancialInstitutionService,
    private scheduleService: SchduleService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.getListUser();
    this.getScheduleList({});
    this.initialForm();
    this.getListFinancial();
    console.log(this.senderTransferPushList);
  }

  initialForm() {
    this.pullForm = new FormGroup({
      pullGroup: new FormGroup({
        'senderUserId': new FormControl(null, Validators.required),
        'fiCode': new FormControl(null, Validators.required),
        'fiNameTh': new FormControl(null, Validators.required),
        'dataTypeId': new FormControl(null, Validators.required),
        'scheduleId': new FormControl(null, Validators.required),
        'serverName': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'network': new FormControl('Internet'),
        'hostName': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'port': new FormControl(null, [Validators.required, Validators.max(99999)]),
        'serverKey': new FormControl(null),
        'username': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'password': new FormControl(null, Validators.required),
        'protocol': new FormControl('FTP', Validators.required),
        'retryCount': new FormControl(null, Validators.pattern('^[0-9 ]+$')),
        'retryDelay': new FormControl(null, Validators.pattern('^[0-9 ]+$')),
        'inputPath': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'statusPath': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'reportPath': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'verifiedPath': new FormControl(null),
        'senderEmailTo': new FormControl(null),
        'senderEmailCc': new FormControl(null),
      })
    });

    this.financialForm = new FormGroup({
      financialGroup: new FormGroup({
        'fiCode': new FormControl(null),
      })
    });
  }

  onGenerate() {
    this.pullForm.get('pullGroup.serverKey').setValue('P@ssw0rd');
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }

  resetForm() {
    this.pullForm.reset();
  }

  onSubmit() {

    console.log(this.pullForm.value);
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลรายการผู้นำส่งข้อมูลแบบดึงไฟล์ ใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.pullForm.valid) {
        this.pullService.save(this.pullForm.value.pullGroup).subscribe(
          response => {
            console.log(response);
            this.resetForm();
            this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลรายการผู้นำส่งข้อมูลแบบดึงไฟล์ สำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('mft/sender-transfer-pull');
          },
          err => {
            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
          }
        );
        sub.unsubscribe();
      }
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลรายการผู้นำส่งข้อมูลแบบดึงไฟล์ ยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        console.log(this.pullForm.value.pullGroup);
        this.router.navigateByUrl('/mft/sender-transfer-pull');
        sub.unsubscribe();
      }
    });
  }

  getScheduleList(param) {
    this.scheduleService.list(param).subscribe(result => {
      // console.log(result.data);
      this.edcSchedule = result.data;
    });
  }

  getDatatypeUserList(event: TypeaheadMatch) {
    this.selectedOptionUser = event.item;
    this.pullForm.get('pullGroup.fiCode').setValue(this.selectedOptionUser.fiCode);
    this.pullForm.get('pullGroup.senderUserId').setValue(this.selectedOptionUser.senderUserId);
    console.log(this.selectedOptionUser);
    this.senderTransferPushService.list({fiCode: this.selectedOptionUser.fiCode}).subscribe(result => {
      // console.log(result.data);
      this.datatypeUserList = result.data;
      this.pullForm.get('pullGroup.senderEmailTo').setValue(this.datatypeUserList[0].senderEmailTo);
      this.pullForm.get('pullGroup.senderEmailCc').setValue(this.datatypeUserList[0].senderEmailCc);
    });
    // console.log(senderId);
  }

  getListUser() {
    this.senderTransferPushService.getList().subscribe(result => {
      this.senderTransferPushList = result.data;
      this.userList = result.data.filter(e => e.status === 'ACTIVE');

      if (this.arrayUser.length === 0) {
        this.userList[0].fiNameTh = this.financialList.find(a => a.fiCode === this.userList[0].fiCode).fiNameTh;
        this.arrayUser.push(this.userList[0]);
      }

      for (let i = 0; i <= this.userList.length - 1; i++) {
        // this.isDup = false;
        // for (let j = 0; j <= this.arrayUser.length - 1; j++) {
        //   if (this.userList[i].fiCode === this.arrayUser[j].fiCode) {
        //     this.isDup = true;
        //   }
        // }
        // if (this.isDup === false) {
        //   if (this.userList[i].fiCode != null) {
        //     this.userList[i].fiNameTh = this.financialList.find(a => a.fiCode === this.userList[i].fiCode).fiNameTh;
        //   }
        //   this.arrayUser.push(this.userList[i]);
        // }fiNameTh_titleName_firstName_lastName
        this.userList[i].fiNameTh = this.financialList.find(a => a.fiCode === this.userList[i].fiCode).fiNameTh;
        // console.log(this.financialList.find(a => a.fiCode === this.userList[i].fiCode).fiNameTh);
        this.userList[i].fiNameTh_titleName_firstName_lastName =
          (this.userList[i].edcUserEntity.titleName || ' ')
          + ' ' + (this.userList[i].edcUserEntity.firstName || ' ')
          + ' ' + (this.userList[i].edcUserEntity.lastName || ' '
          + ' ' + this.financialList.find(a => a.fiCode === this.userList[i].fiCode).fiNameTh);
        console.log(this.userList[i].fiNameTh_titleName_firstName_lastName);
      }
      console.log(this.arrayUser);
    });
  }


}
