import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {ModalService} from '../../../../../../core/services/modal.service';
import {Router} from '@angular/router';
import {EdcSenderSftpServerProfileModel} from '../../../models/edcSenderSftpServerProfile.model';
import {SenderTransferPullService} from '../edcSenderTransferPull.service';
import {SenderTransferPushService} from '../../sender-transfer-push/sender-transfer-push.service';
import {DataTypeModel} from '../../../models/dataType.model';
import {EdcDataTypeService} from '../../data-type/edcDataType.service';
import {EdcfinancialInstitutionModel} from '../../../../etl/models/edcFinancial-insitution.model';
import {FinancialInstitutionService} from '../../../../etl/components/financial-institution/edcfinancial-institution.service';
import {TypeaheadMatch} from 'ngx-bootstrap';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  edcPull = <EdcSenderSftpServerProfileModel[]>[];
  pullForm: FormGroup;
  p: number;
  total: number;
  pageSize: number;

  financialList = <EdcfinancialInstitutionModel[]>[];
  selectedOptionFi: EdcfinancialInstitutionModel;
  financialMap = new Map();
  dataTypeList = <DataTypeModel[]>[];
  datatypeMap = new Map;
  inputSiteList = {ARI: 'ศูนย์คอมพิวเตอร์ กรมสรรพากร', NON: 'ศูนย์คอมพิวเตอร์ กรมสรรพากร นนทบุรี'};

  constructor(
    private senderTransferPushService: SenderTransferPushService,
    private edcDataTypeService: EdcDataTypeService,
    private pullService: SenderTransferPullService,
    private financialInstitutionService: FinancialInstitutionService,
    private modal: ModalService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.getList({});
    this.initialForm();
    this.getListDataType();
    this.getListFinancial();
  }

  onSubmit() {
    console.log(this.pullForm.value);
    console.log(this.pullForm.value.pullGroup.dataTypeId);
    // console.log(typeof this.pullForm.value.pullGroup.dataTypeId);
    // this.pullForm.get('pullGroup.dataTypeId').setValue(+this.pullForm.value.pullGroup.dataTypeId);
    // console.log(this.pullForm.value.pullGroup.dataTypeId);
    // console.log(typeof this.pullForm.value.pullGroup.dataTypeId);
    this.getList(this.pullForm.value.pullGroup);
  }

  initialForm() {
    this.pullForm = new FormGroup({
      pullGroup: new FormGroup({
        'fiCode': new FormControl(null),
        'fiNameTh': new FormControl(null),
        'senderUserId': new FormControl(null),
        'dataTypeId': new FormControl(null),
        'inputSite': new FormControl(null),
      })
    });
  }

  getListDataType() {
    this.edcDataTypeService.list({}).subscribe(result => {
      this.dataTypeList = result.data;
      for (let i = 0; i <= this.dataTypeList.length - 1; i++) {
        // console.log(this.dataTypeList[i]);
        this.datatypeMap.set(this.dataTypeList[i].id, this.dataTypeList[i].dataTypeDesTh);
      }
    });
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }

  setFiCode(event: TypeaheadMatch) {
    this.selectedOptionFi = event.item;
    this.pullForm.get('pullGroup.fiCode').setValue(this.selectedOptionFi.fiCode);
    console.log(this.selectedOptionFi);
    // console.log(senderId);
  }

  resetForm() {
    this.pullForm.reset();
    this.getList(this.pullForm.value.pullGroup);
  }

  getList(param) {
    this.pullService.list(param).subscribe(result => {
      this.edcPull = result.data;
      this.p = result.page;
      this.total = result.total;
      this.pageSize = result.pageSize;

      for (let i = 0; i <= this.edcPull.length - 1; i++) {
        this.edcPull[i].titleName_firstName_lastName = (this.edcPull[i].edcUserEntity.titleName || '')
          + ' ' + (this.edcPull[i].edcUserEntity.firstName || '')
          + ' ' + (this.edcPull[i].edcUserEntity.lastName || '');
      }
    });
  }

  getPage(page: number) {
    this.pullForm.value.pullGroup.page = page;
    console.log(this.pullForm.value.pullGroup);
    this.getList(this.pullForm.value.pullGroup);
  }

  onEdit(obj) {
    console.log(obj);
    this.router.navigateByUrl('/mft/sender-transfer-pull/edit', {state: obj});
  }

  onDelete(obj) {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'ยืนยัน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลรายการผู้นำส่งข้อมูลแบบดึงไฟล์ ใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.pullService.delete(obj).subscribe(
          response => {
            console.log(response);
            this.getList(this.pullForm.value.pullGroup);
            this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลรายการผู้นำส่งข้อมูลแบบดึงไฟล์ สำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
          },
          err => {
            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
          }
        );
      }
      sub.unsubscribe();
    });
  }

  getPageSize(pageSize: number) {
    this.pullForm.value.pullGroup.pageSize = pageSize;
    this.getList(this.pullForm.value.pullGroup);
  }
}
