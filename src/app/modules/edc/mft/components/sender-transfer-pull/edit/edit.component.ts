import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {SenderTransferPullService} from '../edcSenderTransferPull.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcScheduleModel} from '../../../models/edcSchdule.model';
import {EdcDataTypeService} from '../../data-type/edcDataType.service';
import {SenderTransferPushService} from '../../sender-transfer-push/sender-transfer-push.service';
import {SchduleService} from '../../schedule/edcSchdule.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {EdcSenderSftpServerProfileModel} from '../../../models/edcSenderSftpServerProfile.model';
import {DataTypeModel} from '../../../models/dataType.model';
import {EdcfinancialInstitutionModel} from '../../../../etl/models/edcFinancial-insitution.model';
import {FinancialInstitutionService} from '../../../../etl/components/financial-institution/edcfinancial-institution.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  pullForm: FormGroup;
  edcSchedule = <EdcScheduleModel[]>[];
  state: Observable<EdcSenderSftpServerProfileModel>;
  edcSenderSftpServerProfile: EdcSenderSftpServerProfileModel;
  dataTypeModelList = <DataTypeModel[]>[];
  senderMap = new Map();
  datatypeMap = new Map();

  financialList = <EdcfinancialInstitutionModel[]>[];
  financialMap = new Map();

  constructor(
    public activatedRoute: ActivatedRoute,
    private modal: ModalService,
    private pullService: SenderTransferPullService,
    private edcDataTypeService: EdcDataTypeService,
    private senderTransferPushService: SenderTransferPushService,
    private scheduleService: SchduleService,
    private financialInstitutionService: FinancialInstitutionService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe(e => {
      this.edcSenderSftpServerProfile = e;
    });
    if (this.edcSenderSftpServerProfile.id === undefined) {
      this.router.navigateByUrl('/mft/sender-transfer-pull');
    }
    this.getDataTypeList();
    this.getListFinancial();
    this.getScheduleList({});
    this.initialForm();

  }

  initialForm() {
    this.pullForm = new FormGroup({
      pullGroup: new FormGroup({
        'senderUserId': new FormControl(this.edcSenderSftpServerProfile.senderUserId, Validators.required),
        'fiCode': new FormControl( this.edcSenderSftpServerProfile.fiCode, Validators.required),
        'fiNameTh_titleName_firstName_lastName': new FormControl(this.edcSenderSftpServerProfile.fiNameTh_titleName_firstName_lastName),
        'dataTypeId': new FormControl(this.edcSenderSftpServerProfile.dataTypeId, Validators.required),
        'dataTypeDesTh': new FormControl( null),
        'scheduleId': new FormControl(this.edcSenderSftpServerProfile.scheduleId, Validators.required),
        'serverName': new FormControl(this.edcSenderSftpServerProfile.serverName, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'network': new FormControl(this.edcSenderSftpServerProfile.network),
        'hostName': new FormControl(this.edcSenderSftpServerProfile.hostName, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'port': new FormControl(this.edcSenderSftpServerProfile.port, [Validators.required, Validators.max(99999) , Validators.pattern('^[0-9 ]+$'), Validators.maxLength(5)]),
        'serverKey': new FormControl(this.edcSenderSftpServerProfile.serverKey),
        'username': new FormControl(this.edcSenderSftpServerProfile.username, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'password': new FormControl(this.edcSenderSftpServerProfile.password, Validators.required),
        'protocol': new FormControl(this.edcSenderSftpServerProfile.protocol, Validators.required),
        'retryCount': new FormControl(this.edcSenderSftpServerProfile.retryCount, Validators.pattern('^[0-9 ]+$')),
        'retryDelay': new FormControl(this.edcSenderSftpServerProfile.retryDelay, Validators.pattern('^[0-9 ]+$')),
        'inputPath': new FormControl(this.edcSenderSftpServerProfile.inputPath, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'statusPath': new FormControl(this.edcSenderSftpServerProfile.statusPath, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'reportPath': new FormControl(this.edcSenderSftpServerProfile.reportPath, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'inputSite': new FormControl(this.edcSenderSftpServerProfile.inputSite),
        'verifiedPath': new FormControl(this.edcSenderSftpServerProfile.verifiedPath),
        'senderEmailTo': new FormControl(this.edcSenderSftpServerProfile.senderEmailTo),
        'senderEmailCc': new FormControl(this.edcSenderSftpServerProfile.senderEmailCc),
        'createdDate': new FormControl(this.edcSenderSftpServerProfile.createdDate),
        'createdBy': new FormControl(this.edcSenderSftpServerProfile.createdBy)
      })
    });
  }

  onGenerate() {
    this.pullForm.get('pullGroup.serverKey').setValue('P@ssw0rd');
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }

      console.log(this.edcSenderSftpServerProfile);
      this.edcSenderSftpServerProfile.fiNameTh = this.financialList.find(a => a.fiCode === this.edcSenderSftpServerProfile.fiCode).fiNameTh;
      this.edcSenderSftpServerProfile.fiNameTh_titleName_firstName_lastName =
        (this.edcSenderSftpServerProfile.edcUserEntity.titleName || '')
        + ' ' + (this.edcSenderSftpServerProfile.edcUserEntity.firstName || '')
        + ' ' + (this.edcSenderSftpServerProfile.edcUserEntity.lastName || '')
        + ',' + this.edcSenderSftpServerProfile.fiNameTh;
    });
  }

  resetForm() {
    this.pullForm.reset();
  }

  onSubmit() {
    console.log(this.pullForm.value);
    console.log(this.pullForm);
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลรายการผู้นำส่งข้อมูลแบบดึงไฟล์ ใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.pullForm.valid) {
        this.pullForm.value.pullGroup.id = this.edcSenderSftpServerProfile.id;
        this.pullService.update(this.pullForm.value.pullGroup).subscribe(
          response => {
            console.log(response);
            this.resetForm();
            this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลรายการผู้นำส่งข้อมูลแบบดึงไฟล์ สำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
            this.router.navigateByUrl('mft/sender-transfer-pull');
          },
          err => {
            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
          }
        );
        sub.unsubscribe();
      }
    });
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลรายการผู้นำส่งข้อมูลแบบดึงไฟล์ยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        console.log(this.pullForm.value.pullGroup);
        this.router.navigateByUrl('/mft/sender-transfer-pull');
        sub.unsubscribe();
      }
    });
  }

  getScheduleList(param) {
    this.scheduleService.list(param).subscribe(result => {
      // console.log(result.data);
      this.edcSchedule = result.data;
    });
  }


  getDataTypeList() {
    this.edcDataTypeService.listSelect().subscribe(result => {
      // console.log(result.data);
      this.dataTypeModelList = result.data;
      for (let i = 0; i <= this.dataTypeModelList.length - 1; i++) {
        this.datatypeMap.set(this.dataTypeModelList[i].id, this.dataTypeModelList[i].dataTypeDesTh);
      }
      console.log(this.datatypeMap);
    });
  }
}
