import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseMft} from '../../models/responseMft.model';
import {EdcSenderSftpServerProfileModel} from '../../models/edcSenderSftpServerProfile.model';
import {environment} from '../../../../../../environments/environment';

@Injectable()
export class SenderTransferPullService {
    url = environment.streamApiUrl;

    constructor(private http: HttpClient) {
    }

    list(request: any): Observable<ResponseMft<EdcSenderSftpServerProfileModel[]>> {
        console.log(request);
        return <Observable<ResponseMft<EdcSenderSftpServerProfileModel[]>>>this.http.post(this.url + 'senderSftpServerProfiles', request);
    }

    listSelect(): Observable<ResponseMft<EdcSenderSftpServerProfileModel[]>> {
        return <Observable<ResponseMft<EdcSenderSftpServerProfileModel[]>>>this.http.get(this.url + 'senderSftpServerProfiles');
    }

    save(request: EdcSenderSftpServerProfileModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'senderSftpServerProfile/add', request);
    }

    update(request: EdcSenderSftpServerProfileModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'senderSftpServerProfile/update', request);
    }

    delete(request: EdcSenderSftpServerProfileModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'senderSftpServerProfile/delete', request);
    }

}
