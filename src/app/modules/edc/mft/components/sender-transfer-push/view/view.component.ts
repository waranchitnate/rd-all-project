import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {EdcSenderConfigFileTransferModel} from '../../../models/edcSenderConfigFileTransfer.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {map} from 'rxjs/operators';
import {SenderTransferPushService} from '../sender-transfer-push.service';
import {UserInfoModel} from '../../../models/userInfo.model';
import {DataTypeModel} from '../../../models/dataType.model';
import {EdcDataTypeService} from '../../data-type/edcDataType.service';
import {FinancialInstitutionService} from '../../../../etl/components/financial-institution/edcfinancial-institution.service';
import {EdcfinancialInstitutionModel} from '../../../../etl/models/edcFinancial-insitution.model';

@Component({
    selector: 'app-edit',
    templateUrl: './view.component.html',
    styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

    state$: Observable<EdcSenderConfigFileTransferModel>;
    senderConfigFileForm: FormGroup;
    userInfoList = <UserInfoModel[]>[];
    dataTypeModelList = <DataTypeModel[]>[];
    financialList = <EdcfinancialInstitutionModel[]>[];
    financialMap = new Map();

    edcSenderConfigFileTransferModel: EdcSenderConfigFileTransferModel;

    constructor(
        public activatedRoute: ActivatedRoute,
        public router: Router,
        private senderTransferPushService: SenderTransferPushService,
        private financialInstitutionService: FinancialInstitutionService,
        private datatypeService: EdcDataTypeService,
    ) {
    }

    ngOnInit() {
        this.state$ = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state$.subscribe(e => {
            this.edcSenderConfigFileTransferModel = e;
        });
        if (this.edcSenderConfigFileTransferModel.id === undefined) {
            this.router.navigateByUrl('/mft/sender-transfer-push');
        }
        this.initialForm();
        this.getListUser();
        this.getListFinancial();
        this.selectEvent(this.edcSenderConfigFileTransferModel.senderUserId);
    }

    initialForm() {
        this.senderConfigFileForm = new FormGroup({
            edcSenderConfigFileTransferObj: new FormGroup({
                'senderUserId': new FormControl(this.edcSenderConfigFileTransferModel.fiCode, Validators.required),
                'dataTypeId': new FormControl(this.edcSenderConfigFileTransferModel.dataTypeId, Validators.required),
                // 'inputSite': new FormControl(this.edcSenderConfigFileTransferModel.inputSite, Validators.required),
                'senderEmailTo': new FormControl(this.edcSenderConfigFileTransferModel.senderEmailTo, [Validators.required, Validators.email]),
                'senderEmailCc': new FormControl(this.edcSenderConfigFileTransferModel.senderEmailCc, [Validators.required, Validators.email]),
                // 'senderEmailBcc': new FormControl(this.edcSenderConfigFileTransferModel.senderEmailBcc, [Validators.required, Validators.email])
            })
        });
    }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }

    onBack() {
        // const modalRef = this.modal.modalService.show(AnswerModalComponent);
        // (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        // (modalRef.content as AnswerModalComponent).content = 'ข้อมูลรายการผู้นำส่งข้อมูลแบบนำไฟล์มาวางยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        //
        // const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
        //     if (a) {
        //         console.log(this.senderConfigFileForm.value.edcSenderConfigFileTransferObj);
        //         sub.unsubscribe();
                this.router.navigateByUrl('/mft/sender-transfer-push');
        //     }
        // });
    }

    // resetForm() {
    //     this.senderConfigFileForm.reset();
    // }

    // onSubmit() {
    //     const modalRef = this.modal.modalService.show(AnswerModalComponent);
    //     (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    //     (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลของรายการผู้นำส่งข้อมูลแบบนำไฟล์มาวางใช่หรือไม่';
    //
    //     const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
    //         if (a) {
    //             this.senderConfigFileForm.value.edcSenderConfigFileTransferObj.id = this.edcSenderConfigFileTransferModel.id;
    //             this.senderTransferPushService.update(this.senderConfigFileForm.value.edcSenderConfigFileTransferObj).subscribe(
    //                 response => {
    //                     console.log(response);
    //                     this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลรายการผู้นำส่งข้อมูลแบบนำไฟล์มาวางสำเร็จ');
    //                     const modalHide = this.modal.modalService.onHide.subscribe(e => {
    //                         modalHide.unsubscribe();
    //                     });
    //                     this.router.navigateByUrl('/mft/sender-transfer-push');
    //                 },
    //                 err => {
    //                     this.modal.openModal('เกิดข้อผิดพลาด', 'ไม่สามารถบันทึกได้กรุณาทำรายการใหม่ภายหลัง');
    //                 }
    //             );
    //         }
    //         sub.unsubscribe();
    //     });
    // }

    getListUser() {
        this.senderTransferPushService.listUser().subscribe(result => {
            this.userInfoList = result.data;
            console.log(this.userInfoList);
        });
    }

    selectEvent(item) {
        this.datatypeService.listSelect().subscribe(result => {
            this.dataTypeModelList = result.data;
            console.log(this.dataTypeModelList);
        });
        // do something with selected item
    }


}
