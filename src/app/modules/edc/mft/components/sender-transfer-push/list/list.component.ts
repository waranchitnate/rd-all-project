import {Component, OnInit} from '@angular/core';
import {SenderTransferPushService} from '../sender-transfer-push.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {UserInfoModel} from '../../../models/userInfo.model';
import {DataTypeModel} from '../../../models/dataType.model';
import {EdcDataTypeService} from '../../data-type/edcDataType.service';
import {EdcSenderConfigFileTransferModel} from '../../../models/edcSenderConfigFileTransfer.model';
import {ModalService} from '../../../../../../core/services/modal.service';
import {EdcfinancialInstitutionModel} from '../../../../etl/models/edcFinancial-insitution.model';
import {FinancialInstitutionService} from '../../../../etl/components/financial-institution/edcfinancial-institution.service';
import {TypeaheadMatch} from 'ngx-bootstrap';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  senderConfigSearchForm: FormGroup;
  senderTransferPushList = <EdcSenderConfigFileTransferModel[]>[];
  financialList = <EdcfinancialInstitutionModel[]>[];
  selectedOptionFi: EdcfinancialInstitutionModel;
  financialMap = new Map();
  page: number;
  total: number;
  pageSize: number;

  userInfoList = <UserInfoModel[]>[];
  dataTypeList = <DataTypeModel[]>[];

  // inputSiteList = {ARI: 'ศูนย์คอมพิวเตอร์ กรมสรรพากร', NON: 'ศูนย์คอมพิวเตอร์ กรมสรรพากร นนทบุรี'};

  constructor(
    private senderTransferPushService: SenderTransferPushService,
    private edcDataTypeService: EdcDataTypeService,
    private financialInstitutionService: FinancialInstitutionService,
    private modal: ModalService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.getList({});
    this.initialForm();
    this.getListFinancial();
    this.getListUser();
    this.getListDataType();
    // console.log(this.dataTypeList);
    // console.log(this.financialMap);
    // console.log(this.senderTransferPushList);
  }

  onSubmit() {
    console.log(this.senderConfigSearchForm.value);
    this.getList(this.senderConfigSearchForm.value.senderConfigSearchC);
  }

  getList(param) {
    this.senderTransferPushService.list(param).subscribe(result => {
      // console.log(result.data);
      this.senderTransferPushList = result.data;
      this.page = result.page;
      this.total = result.total;
      this.pageSize = result.pageSize;

      for (let i = 0; i <= this.senderTransferPushList.length - 1; i++) {
        this.senderTransferPushList[i].titleName_firstName_lastName = (this.senderTransferPushList[i].edcUserEntity.titleName || '')
          + ' ' + (this.senderTransferPushList[i].edcUserEntity.firstName || '')
          + ' ' + (this.senderTransferPushList[i].edcUserEntity.lastName || '');
      }
    });
  }

  onView(obj) {
    // console.log(obj);
    this.router.navigateByUrl('/mft/sender-transfer-push/view', {state: obj});
  }

  getListFinancial() {
    this.financialInstitutionService.listSelect().subscribe(result => {
      this.financialList = result.data;
      for (let i = 0; i < this.financialList.length; i++) {
        this.financialMap.set(this.financialList[i].fiCode, this.financialList[i].fiNameTh);
      }
    });
  }

  getListUser() {
    this.senderTransferPushService.listUser().subscribe(result => {
      this.userInfoList = result.data;
    });
  }

  getListDataType() {
    this.edcDataTypeService.list({}).subscribe(result => {
      this.dataTypeList = result.data;
    });
  }

  initialForm() {
    this.senderConfigSearchForm = new FormGroup({
      senderConfigSearchC: new FormGroup({
        'fiCode': new FormControl(null),
        'fiNameTh': new FormControl(null),
        'dataTypeId': new FormControl(null),
      })
    });
  }

  resetForm() {
    this.senderConfigSearchForm.reset();
    this.initialForm();
    this.getList({});
  }

  setFiCode(event: TypeaheadMatch) {
    this.selectedOptionFi = event.item;
    this.senderConfigSearchForm.get('senderConfigSearchC.fiCode').setValue(this.selectedOptionFi.fiCode);
    console.log(this.selectedOptionFi);
    // console.log(senderId);
  }

  getPage(page: number) {
    this.senderConfigSearchForm.value.senderConfigSearchC.page = page;
    // console.log(this.senderConfigSearchForm.value.senderConfigSearchC);
    this.getList(this.senderConfigSearchForm.value.senderConfigSearchC);
  }

  getPageSize(pageSize: number) {
    this.senderConfigSearchForm.value.senderConfigSearchC.pageSize = pageSize;
    // console.log(this.senderConfigSearchForm.value.senderConfigSearchC);
    this.getList(this.senderConfigSearchForm.value.senderConfigSearchC);
  }

  // onDelete(obj) {
  //   const modalRef = this.modal.modalService.show(AnswerModalComponent);
  //   (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
  //   (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลรายการผู้นำส่งข้อมูลแบบนำไฟล์มาวางใช่หรือไม่';
  //
  //   const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
  //     if (a) {
  //       this.senderTransferPushService.delete(obj).subscribe(
  //         response => {
  //           // console.log(response);
  //           this.getList(this.senderConfigSearchForm.value.senderConfigSearchC);
  //           this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลรายการผู้นำส่งข้อมูลแบบนำไฟล์มาวางสำเร็จ');
  //           const modalHide = this.modal.modalService.onHide.subscribe(e => {
  //             modalHide.unsubscribe();
  //           });
  //         },
  //         err => {
  //           this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
  //         }
  //       );
  //     }
  //     sub.unsubscribe();
  //   });
  // }
}
