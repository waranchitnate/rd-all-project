import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SenderTransferPushRoutingModule} from './sender-transfer-push-routing.module';
import {ListComponent} from './list/list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {SharedModule} from '../../../../../shared/shared.module';
import {ViewComponent} from './view/view.component';

@NgModule({
    declarations: [ListComponent, ViewComponent],
  imports: [
    CommonModule,
    SenderTransferPushRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    SharedModule
  ]
})
export class SenderTransferPushModule {
}
