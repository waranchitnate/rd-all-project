import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResponseMft} from '../../models/responseMft.model';
import {EdcSenderConfigFileTransferModel} from '../../models/edcSenderConfigFileTransfer.model';
import {UserInfoModel} from '../../models/userInfo.model';

@Injectable()
export class SenderTransferPushService {

    url = environment.streamApiUrl;

    constructor(
        private http: HttpClient
    ) {
    }

    list(request: any): Observable<ResponseMft<EdcSenderConfigFileTransferModel[]>> {
        return <Observable<ResponseMft<EdcSenderConfigFileTransferModel[]>>>this.http.post(this.url + 'senderConfigFileTransfers', request);
    }

    getList(): Observable<ResponseMft<EdcSenderConfigFileTransferModel[]>> {
      return <Observable<ResponseMft<EdcSenderConfigFileTransferModel[]>>>this.http.get(this.url + 'senderConfigFileTransfers');
    }

    listUser(): Observable<ResponseMft<UserInfoModel[]>> {
      return <Observable<ResponseMft<UserInfoModel[]>>>this.http.get(this.url + 'userInfos');
    }
}
