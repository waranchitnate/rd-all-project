import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NotiTemplateService} from '../edcNotiTemplate.service';
import {EdcNotiTemplateModel} from '../../../models/edcNotiTemplate.model';
import {Router} from '@angular/router';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcConfigCodeModel} from '../../../../etl/models/edcConfigCode.model';
import {ConfigCodeService} from '../../../edcConfigCode.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListNotiComponent implements OnInit {

  edcNotiTemplate = <EdcNotiTemplateModel[]>[];
  configList = <EdcConfigCodeModel[]>[];
  configMap = new Map();
  notiTemplateSearchForm: FormGroup;
  p: number;
  total: number;
  pageSize: number;

  constructor(
    private notiTemplateService: NotiTemplateService,
    private modal: ModalService,
    private router: Router,
    private configCodeService: ConfigCodeService
  ) {
  }

  ngOnInit() {
    this.getListConfigCode();
    this.getList({});
    this.initialForm();
  }

  onSubmit() {
    console.log(this.notiTemplateSearchForm.value);
    this.getList(this.notiTemplateSearchForm.value.notiTemplateC);
  }

  initialForm() {
    this.notiTemplateSearchForm = new FormGroup({
      notiTemplateC: new FormGroup({
        'state': new FormControl(null),
      })
    });
  }

  resetForm() {
    this.notiTemplateSearchForm.reset();
    this.initialForm();
    this.getList(this.notiTemplateSearchForm.value.notiTemplateC);
  }

  getList(param) {
    this.notiTemplateService.list(param).subscribe(result => {
      console.log(result.data);
      this.edcNotiTemplate = result.data;
      this.p = result.page;
      this.total = result.total;
      this.pageSize = result.pageSize;
    });
  }

  onEdit(obj) {
    console.log(obj);
    this.router.navigateByUrl('/mft/notification-template/edit', {state: obj});
  }

  getPage(page: number) {
    console.log(page);
    this.notiTemplateSearchForm.value.notiTemplateC.page = page;
    console.log(this.notiTemplateSearchForm.value.notiTemplateC);
    this.getList(this.notiTemplateSearchForm.value.notiTemplateC);
  }

  onDelete(obj) {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลการแจ้งเตือนใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        this.notiTemplateService.delete(obj).subscribe(
          response => {
            console.log(response);
            this.getList(this.notiTemplateSearchForm.value.notiTemplateC);
            this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลการแจ้งเตือนสำเร็จ');
            const modalHide = this.modal.modalService.onHide.subscribe(e => {
              modalHide.unsubscribe();
            });
          },
          err => {
            this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
          }
        );
      }
      sub.unsubscribe();
    });
  }

  getPageSize(pageSize: number) {
    this.notiTemplateSearchForm.value.notiTemplateC.pageSize = pageSize;
    this.getList(this.notiTemplateSearchForm.value.notiTemplateC);
  }

  getListConfigCode() {
    this.configCodeService.listConfig().subscribe(res => {
      this.configList = res.data.filter(e => e.type === 'NOTIFICATION');

      for (let i = 0; i <= this.configList.length - 1; i++) {
        console.log(this.configList[i]);
        this.configMap.set(this.configList[i].code, this.configList[i].description);
      }
      console.log(this.configMap);
    });
  }
}
