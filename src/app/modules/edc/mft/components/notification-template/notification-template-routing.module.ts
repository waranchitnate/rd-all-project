import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListNotiComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import {EditComponent} from './edit/edit.component';

const routes: Routes = [
    { path: '', component: ListNotiComponent},
    { path: 'add', component: AddComponent},
    { path: 'edit', component: EditComponent},
    // { path: 'edit/:id', component: EditComponent, data: {title: 'แก้ไขกลุ่มสิทธิ์', disabled:  false}},
    // { path: 'view/:id', component: EditComponent, data: {title: 'เรียกดูกลุ่มสิทธิ์', disabled: true}}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NotificationtemplateRoutingModule { }
