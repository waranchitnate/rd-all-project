import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {environment} from '../../../../../../environments/environment';

import {EdcNotiTemplateModel} from '../../models/edcNotiTemplate.model';
import {ResponseMft} from '../../models/responseMft.model';

@Injectable()
export class NotiTemplateService {
    url = environment.streamApiUrl;
    constructor(private http: HttpClient) { }

    list(request: any): Observable<ResponseMft<EdcNotiTemplateModel[]>> {
        console.log(request);
        return <Observable<ResponseMft<EdcNotiTemplateModel[]>>>this.http.post(this.url + 'notificationTemplates', request);
    }

    listSelect(): Observable<ResponseMft<EdcNotiTemplateModel[]>> {
        return <Observable<ResponseMft<EdcNotiTemplateModel[]>>>this.http.get(this.url + 'notificationTemplates');
    }

    save(request: EdcNotiTemplateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'notificationTemplate/add', request);
    }

    update(request: EdcNotiTemplateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'notificationTemplate/update', request);
    }

    delete(request: EdcNotiTemplateModel): Observable<any> {
        console.log(request);
        return this.http.post(this.url + 'notificationTemplate/delete', request);
    }
}
