import {NotiTemplateService} from '../edcNotiTemplate.service';
import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {EdcNotiTemplateModel} from '../../../models/edcNotiTemplate.model';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcConfigCodeModel} from '../../../../etl/models/edcConfigCode.model';
import {ConfigCodeService} from '../../../edcConfigCode.service';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    state: Observable<EdcNotiTemplateModel>;
    edcNotiTemplate: EdcNotiTemplateModel;
    ConfigCodeList = <EdcConfigCodeModel[]>[];
    processStateList = <EdcConfigCodeModel[]>[];
    configList = <EdcConfigCodeModel[]>[];
    notiTemplateForm: FormGroup;

    constructor(
        public activatedRoute: ActivatedRoute,
        public router: Router,
        private modal: ModalService,
        private notiTemplateService: NotiTemplateService,
        private configCodeService: ConfigCodeService) {
    }

    ngOnInit() {
        this.state = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
        this.state.subscribe(e => {
            this.edcNotiTemplate = e;
        });
        if (this.edcNotiTemplate.id === undefined) {
            this.router.navigateByUrl('/mft/notification-template');
        }
        this.getListConfigCode();
        this.getProcessState();
        this.initialForm();
      if (this.notiTemplateForm.value.notiTemplate.state === 'PS003' || this.notiTemplateForm.value.notiTemplate.state === 'PS007' || this.notiTemplateForm.value.notiTemplate.state === 'PS008' || this.notiTemplateForm.value.notiTemplate.state === 'PSERR') {
          this.notiTemplateForm.get('notiTemplate.subjectSender').setValidators([Validators.required]);
          this.notiTemplateForm.get('notiTemplate.contentSender').setValidators([Validators.required]);
        } else {
        this.notiTemplateForm.get('notiTemplate.subjectSender').clearValidators();
        this.notiTemplateForm.get('notiTemplate.contentSender').clearValidators();
      }
      this.notiTemplateForm.get('notiTemplate.subjectSender').updateValueAndValidity();
      this.notiTemplateForm.get('notiTemplate.contentSender').updateValueAndValidity();
    }

  initialForm() {
    this.notiTemplateForm = new FormGroup({
      notiTemplate: new FormGroup({
        'state': new FormControl(this.edcNotiTemplate.state, Validators.required),
        'attachment': new FormControl(this.edcNotiTemplate.attachment, Validators.required),
        'subjectSender': new FormControl(this.edcNotiTemplate.subjectSender, Validators.required),
        'contentSender': new FormControl(this.edcNotiTemplate.contentSender, Validators.required),
        'subjectAdmin': new FormControl(this.edcNotiTemplate.subjectAdmin, Validators.required),
        'contentAdmin': new FormControl(this.edcNotiTemplate.contentAdmin, Validators.required),
        'emailAdminTo': new FormControl(this.edcNotiTemplate.emailAdminTo, [ Validators.required, Validators.pattern('^[A-Za-z!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]+$')]),
        'emailAdminCc': new FormControl(this.edcNotiTemplate.emailAdminCc, [ Validators.pattern('^[A-Za-z!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]+$')]),
        'createdDate': new FormControl(this.edcNotiTemplate.createdDate),
        'createdBy': new FormControl(this.edcNotiTemplate.createdBy)

      })
    });
  }

    getProcessState() {
        this.configCodeService.listConfig().subscribe(result => {
            console.log('result');
            console.log(result.data);
            this.ConfigCodeList = result.data;
            this.processStateList = this.ConfigCodeList.filter(res => res.type === 'PROCESS_STATE');
            console.log(this.processStateList);
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลการแจ้งเตือนยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                console.log(this.notiTemplateForm.value.notiTemplate);
                this.router.navigateByUrl('/mft/notification-template');
                sub.unsubscribe();
            }
        });
    }

    resetForm() {
        this.notiTemplateForm.reset();
    }

    onSubmit() {
        console.log(this.notiTemplateForm.value);
        this.notiTemplateForm.value.notiTemplate.id = this.edcNotiTemplate.id;
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลการแจ้งเตือนใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.notiTemplateForm.valid) {
              this.notiTemplateForm.value.notiTemplate.id = this.edcNotiTemplate.id;
                this.notiTemplateService.update(this.notiTemplateForm.value.notiTemplate).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลการแจ้งเตือนสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });

                        this.router.navigateByUrl('/mft/notification-template');
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

  getListConfigCode() {
    this.configCodeService.listConfig().subscribe(res => {
      this.configList = res.data.filter(e => e.type === 'NOTIFICATION');
    });
  }

  onSelectState() {
    console.log('onSelectState' + this.notiTemplateForm.value.notiTemplate.state);
    if (this.notiTemplateForm.value.notiTemplate.state === 'PS003' || this.notiTemplateForm.value.notiTemplate.state === 'PS007' || this.notiTemplateForm.value.notiTemplate.state === 'PS008' || this.notiTemplateForm.value.notiTemplate.state === 'PSERR') {
      this.notiTemplateForm.get('notiTemplate.subjectSender').setValidators([Validators.required]);
      this.notiTemplateForm.get('notiTemplate.contentSender').setValidators([Validators.required]);
    } else {
      this.notiTemplateForm.get('notiTemplate.subjectSender').clearValidators();
      this.notiTemplateForm.get('notiTemplate.contentSender').clearValidators();
    }
    this.notiTemplateForm.get('notiTemplate.subjectSender').updateValueAndValidity();
    this.notiTemplateForm.get('notiTemplate.contentSender').updateValueAndValidity();
  }

}
