import {NotiTemplateService} from '../edcNotiTemplate.service';

import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {EdcConfigCodeModel} from '../../../../etl/models/edcConfigCode.model';
import {Router} from '@angular/router';
import {ConfigCodeService} from '../../../edcConfigCode.service';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    notiTemplateForm: FormGroup;
    ConfigCodeList = <EdcConfigCodeModel[]>[];
    processStateList = <EdcConfigCodeModel[]>[];
    configList = <EdcConfigCodeModel[]>[];

    constructor(
        private modal: ModalService,
        private notiTemplateService: NotiTemplateService,
        private configCodeService: ConfigCodeService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.initialForm();
        this.getProcessState();
        this.getListConfigCode();
    }

    onSelectState() {
        console.log('onSelectState' + this.notiTemplateForm.value.notiTemplate.state);
        if (this.notiTemplateForm.value.notiTemplate.state === 'PS003' || this.notiTemplateForm.value.notiTemplate.state === 'PS007' || this.notiTemplateForm.value.notiTemplate.state === 'PS008' || this.notiTemplateForm.value.notiTemplate.state === 'PSERR') {
          this.notiTemplateForm.get('notiTemplate.subjectSender').setValidators([Validators.required]);
          this.notiTemplateForm.get('notiTemplate.contentSender').setValidators([Validators.required]);
        } else {
          this.notiTemplateForm.get('notiTemplate.subjectSender').clearValidators();
          this.notiTemplateForm.get('notiTemplate.contentSender').clearValidators();
        }
      this.notiTemplateForm.get('notiTemplate.subjectSender').updateValueAndValidity();
      this.notiTemplateForm.get('notiTemplate.contentSender').updateValueAndValidity();
    }
    initialForm() {
        this.notiTemplateForm = new FormGroup({
            notiTemplate: new FormGroup({
                'state': new FormControl(null, Validators.required),
                'attachment': new FormControl('Y', Validators.required),
                'subjectSender': new FormControl(null, Validators.required),
                'contentSender': new FormControl(null, Validators.required),
                'subjectAdmin': new FormControl(null, Validators.required),
                'contentAdmin': new FormControl(null, Validators.required),
                'emailAdminTo': new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]+$')]),
                'emailAdminCc': new FormControl(null, [Validators.pattern('^[A-Za-z!@#$%^&*()+\\-=\\[\\]{};\':"\\\\|,.<>\\/?]+$')]),

            })
        });
    }

    onBack() {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ข้อมูลการแจ้งเตือนยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                console.log(this.notiTemplateForm.value.notiTemplate);
                this.router.navigateByUrl('/mft/notification-template');
                sub.unsubscribe();
            }
        });
    }

    resetForm() {
        this.notiTemplateForm.reset();
    }

    getProcessState() {
        this.configCodeService.listConfig().subscribe(result => {
            this.ConfigCodeList = result.data;
            this.processStateList = this.ConfigCodeList.filter(res => res.type === 'PROCESS_STATE');
            console.log(this.processStateList);
        });
    }

    onSubmit() {
        console.log(this.notiTemplateForm.value.notiTemplate);
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลการแจ้งเตือนใช่หรือไม่';

        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a && this.notiTemplateForm.valid) {
                this.notiTemplateService.save(this.notiTemplateForm.value.notiTemplate).subscribe(
                    response => {
                        console.log(response);
                        this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลการแจ้งเตือนสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                        this.router.navigateByUrl('/mft/notification-template');
                    },
                    err => {
                        this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                    }
                );
            }
            sub.unsubscribe();
        });
    }

  getListConfigCode() {
    this.configCodeService.listConfig().subscribe(res => {
      this.configList = res.data.filter(e => e.type === 'NOTIFICATION');

      // for (let i = 0; i <= this.configList.length - 1; i++) {
      //   console.log(this.configList[i]);
      // }
    });
  }
}
