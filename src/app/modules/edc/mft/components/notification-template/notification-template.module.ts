import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationtemplateRoutingModule } from './notification-template-routing.module';
import { ListNotiComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {SharedModule} from '../../../../../shared/shared.module';

@NgModule({
    declarations: [ListNotiComponent, AddComponent, EditComponent],
  imports: [
    CommonModule,
    NotificationtemplateRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    SharedModule
  ],
    providers: []
})
export class NotiTemplateModule { }
