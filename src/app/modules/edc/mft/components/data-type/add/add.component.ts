import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EdcDataTypeService} from './../edcDataType.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {Router} from '@angular/router';
import {AlertTextModule} from '../../../edcAlertText.module';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  dataTypeAddForm: FormGroup;
  currentFileUpload: File;
  checkDataTypeCode = true;
  defaultPatternFile: string;
  showPassword: boolean;
  constructor(
    private edcDataTypeService: EdcDataTypeService,
    private modal: ModalService,
    private router: Router,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    this.showPassword = false;
    this.initialForm();
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลประเภทข้อมูลยังไม่ถูกเพิ่มลงบนระบบต้องการยกเลิกการเพิ่มข้อมูลใช่หรือไม่';
    (modalRef.content as AnswerModalComponent).idButtonTrue = 'dataType_add_back_confirm';
    (modalRef.content as AnswerModalComponent).idButtonFalse = 'dataType_add_back_cancel';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        console.log(this.dataTypeAddForm.value.edcDataTypeObj);
        this.router.navigateByUrl('/mft/data-type');
        sub.unsubscribe();
      }
    });
  }

  onSubmit() {
    console.log(this.dataTypeAddForm);
    console.log(this.dataTypeAddForm.valid);
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการเพิ่มข้อมูลประเภทข้อมูลใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.dataTypeAddForm.valid) {
        if ( this.dataTypeAddForm.value.edcDataTypeObj.encPkiCert === 'Y') {
          this.edcDataTypeService.checkPassword(this.currentFileUpload, this.dataTypeAddForm.value.edcDataTypeObj).subscribe(
            response => {
              // this.modal.openModal('แจ้งเตือน', 'รหัส Key passphrase หรือ PKCS#12 Key  ตรงกัน');
              this.edcDataTypeService.save(this.currentFileUpload, this.dataTypeAddForm.value.edcDataTypeObj).subscribe(
                res => {
                  this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลประเภทข้อมูลสำเร็จ');
                  const modalHide = this.modal.modalService.onHide.subscribe(e => {
                    modalHide.unsubscribe();
                  });
                  this.router.navigateByUrl('/mft/data-type');
                },
                err => {
                  if (err.error.errorCode === 'ST098') {
                    this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสประเภทข้อมูล' + this.alertText.getTail);
                  } else {
                    this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                  }
                }
              );
            },
            err => {
              this.modal.openModal('แจ้งเตือน', 'รหัส PKCS#12 Key  ไม่ตรงกัน');
            }
          );
        } else if (this.dataTypeAddForm.value.edcDataTypeObj.encPkiCert === 'N') {
          this.edcDataTypeService.save(this.currentFileUpload, this.dataTypeAddForm.value.edcDataTypeObj).subscribe(
            res => {
              this.modal.openModal('แจ้งเตือน', 'เพิ่มข้อมูลประเภทข้อมูลสำเร็จ');
              const modalHide = this.modal.modalService.onHide.subscribe(e => {
                modalHide.unsubscribe();
              });
              this.router.navigateByUrl('/mft/data-type');
            },
            err => {
              console.log('err > ', err);
              if (err.error.errorCode === 'ST098') {
                this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสประเภทข้อมูล' + this.alertText.getTail);
              } else {
                this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
              }
            }
          );
        }
      } else {
        console.log('valid', this.dataTypeAddForm);
      }
      sub.unsubscribe();
    });
  }

  initialForm() {
    this.dataTypeAddForm = new FormGroup({
      edcDataTypeObj: new FormGroup({
        'dataTypeCode': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]),
        'dataTypeDesTh': new FormControl(null, Validators.required),
        'dataTypeDesEn': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'fileSizeLimit': new FormControl(null, [Validators.required, Validators.max(10240), Validators.pattern('^[0-9]+$')]),
        'inputPath': new FormControl('input', [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'statusPath': new FormControl('status', [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'reportPath': new FormControl('report', [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'certFileName': new FormControl(null),
        'sendEpayMentFlag': new FormControl(false, Validators.required),
        'rootDirectoryEpayment': new FormControl(null),
        'inputPathEpayment': new FormControl(null),
        'sendReportEpayment': new FormControl(false),
        'reportPathEpayment': new FormControl(null),
        'encPkiCert': new FormControl('Y'),
        'isProcess': new FormControl('Y', Validators.required),
        'certFile': new FormControl(null),
        'certKey': new FormControl(null),
        'passPhraseKey': new FormControl(null),
        'patternFileName': new FormControl(null, [Validators.pattern('^[a-zA-Z0-9\\(\\)\\.\\[\\]\\_\\-]+$')]),
        'patternFileNameType': new FormControl('1', Validators.required),
        'dataTypeCodeAbb': new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
      })
    });
  }

  selectFile(event) {
    this.dataTypeAddForm.get('edcDataTypeObj.certFileName').setValue( <File>event.target.files[0].name);
    this.currentFileUpload = <File>event.target.files[0];
    console.log(this.currentFileUpload);
  }

  selectSendReportEpayment() {
    if ( this.dataTypeAddForm.value.edcDataTypeObj.sendReportEpayment === true) {
      this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').setValidators([Validators.required]);
      this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]);
    } else {
      this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').clearValidators();
      this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').clearValidators();
    }
    this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').reset();

    this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').updateValueAndValidity();
  }

  checkCert() {
    if (this.dataTypeAddForm.get('edcDataTypeObj.encPkiCert').value === 'N') {
      this.dataTypeAddForm.get('edcDataTypeObj.certKey').reset();
      this.dataTypeAddForm.get('edcDataTypeObj.certKey').updateValueAndValidity();
    }
  }

  resetForm() {
    this.dataTypeAddForm.reset();
    this.initialForm();
  }

  fixPath(event) {
    console.log(event);
  }

  checkSendEpayFlag() {
    if ( this.dataTypeAddForm.value.edcDataTypeObj.sendEpayMentFlag === true) {
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValue(this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCodeAbb);
      this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]);
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9\\-\\_ ]+$')]);
    }
    if (this.dataTypeAddForm.value.edcDataTypeObj.sendEpayMentFlag === false) {
      this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').setValue(false);
      this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').setValidators(null);
      this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').setValidators(null);
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValidators(null);
    }

    this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValue(null);
    this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').setValue(null);
    this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').setValue(null);

    this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').updateValueAndValidity();
  }
  defaltRoot() {
    if ( this.dataTypeAddForm.value.edcDataTypeObj.sendEpayMentFlag === true) {
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValue(this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCodeAbb);
    }
  }

  onInput() {
    // set Value condition
    if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '1') {
      if (this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCode === 'DT001') {
        this.defaultPatternFile = 'ED[Receiver_FI_Code][YYYYMMDD][SEQ NO]';
      } else if (this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCode === 'DT002') {
        this.defaultPatternFile = 'EC[Receiver_FI_Code][YYYYMMDD][SEQ NO]';
      } else if (this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCode === 'DT003') {
        this.defaultPatternFile = 'DS[Receiver_FI_Code][YYYYMMDD][SEQ NO],MS[Receiver FI Code][YYYYMMDD][SEQ NO]';
      } else if (this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCode === 'DT008') {
        this.defaultPatternFile = 'MT[Receiver_FI_Code][YYYYMMDD][SEQ NO]';
      } else {
        this.defaultPatternFile = '[Data_Type_ABB][Receiver_FI_Code][YYYYMMDD][SEQ_NO]';
      }
      this.checkDataTypeCode = true;
    }

    if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '0') {
      this.defaultPatternFile = null;
      this.checkDataTypeCode = false;
    }
    // set validate condition
    if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '0' || this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '1')  {
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9\\(\\)\\.\\[\\]\\_\\-]+$')]);
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').updateValueAndValidity();
    }
    if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '2') {
      this.defaultPatternFile = null;
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').setValue(null);
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').setValidators(null);
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').updateValueAndValidity();
    }
    // else {
    //   console.log('pass else');
    //   this.defaultPatternFile = null;
    //   this.checkDataTypeCode = false;
    // }
  }

  checkPassword() {
    this.edcDataTypeService.checkPassword(this.currentFileUpload, this.dataTypeAddForm.value.edcDataTypeObj).subscribe(
      response => {
        this.modal.openModal('แจ้งเตือน', 'รหัส PKCS#12 Key  ตรงกัน');
      },
      err => {
        this.modal.openModal('แจ้งเตือน', 'รหัส  PKCS#12 Key ไม่ตรงกัน');
      }
    );
  }

  password() {
    this.showPassword = !this.showPassword;
  }
}
