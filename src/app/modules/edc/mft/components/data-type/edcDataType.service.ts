import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {DataTypeModel} from '../../models/dataType.model';
import {ResponseMft} from '../../models/responseMft.model';
import {environment} from '../../../../../../environments/environment';


@Injectable()
export class EdcDataTypeService {
  url = environment.streamApiUrl;

  constructor(private http: HttpClient) {
  }

  list(request: any): Observable<ResponseMft<DataTypeModel[]>> {
    console.log(request);
    return <Observable<ResponseMft<DataTypeModel[]>>>this.http.post(this.url + 'edcDataTypes', request);
  }

  listSelect(): Observable<ResponseMft<DataTypeModel[]>> {
    return <Observable<ResponseMft<DataTypeModel[]>>>this.http.get(this.url + 'edcDataTypes');
  }

  save(file: File, request: DataTypeModel): Observable<any> {
    console.log(JSON.stringify(request));
    const data = new FormData();
    data.append('file', file);
    data.append('JsonData', JSON.stringify(request));
    return this.http.post(this.url + 'edcDataType/add', data);
  }

  update(file: File, request: DataTypeModel): Observable<any> {
    console.log(request);
    const data = new FormData();
    data.append('file', file);
    data.append('JsonData', JSON.stringify(request));

    return this.http.post(this.url + 'edcDataType/update', data);
  }

  delete(request: DataTypeModel): Observable<any> {
    console.log(request);
    return this.http.post(this.url + 'edcDataType/delete', request);
  }

  checkPassword(file: File, request: DataTypeModel): Observable<any> {
    console.log(JSON.stringify(request));
    const data = new FormData();
    data.append('file', file);
    data.append('JsonData', JSON.stringify(request));
    return this.http.post(this.url + 'edcDataType/check', data);
  }
}
