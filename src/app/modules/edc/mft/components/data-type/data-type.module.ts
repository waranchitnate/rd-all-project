import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DataTypeRoutingModule} from './data-type-routing.module';
import {ListComponent} from './list/list.component';
import {AddComponent} from './add/add.component';
import {EditComponent} from './edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {PaginationModule} from 'ngx-bootstrap';
import {SharedModule} from '../../../../../shared/shared.module';

@NgModule({
    declarations: [ListComponent, AddComponent, EditComponent],
  imports: [
    CommonModule,
    DataTypeRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    PaginationModule,
    SharedModule
  ],
    providers: []
})
export class DataTypeModule {
}
