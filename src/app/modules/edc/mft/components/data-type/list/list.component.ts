import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {EdcDataTypeService} from '../edcDataType.service';
import {DataTypeModel} from '../../../models/dataType.model';
import {Router} from '@angular/router';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {ModalService} from '../../../../../../core/services/modal.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

    dataTypeSearchForm: FormGroup;
    DataTypeModelList = <DataTypeModel[]>[];
    p: number;
    total: number;
    pageSize: number;
    encPkiCertList = {Y: 'ใช่', N: 'ไม่ใช่'};

    constructor(
        private edcDataTypeService: EdcDataTypeService,
        private router: Router,
        private modal: ModalService
    ) {
    }

    ngOnInit() {
        this.getList({});
        this.initialForm();
    }

    onSubmit() {
        console.log(this.dataTypeSearchForm.value);
        this.getList(this.dataTypeSearchForm.value.edcDataTypeC);
    }

    onEdit(obj: DataTypeModel) {
        console.log(obj);
        this.router.navigateByUrl('/mft/data-type/edit', {state: obj});
    }

    onDelete(obj) {
        const modalRef = this.modal.modalService.show(AnswerModalComponent);
        (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
        (modalRef.content as AnswerModalComponent).content = 'ต้องการลบข้อมูลประเภทข้อมูลใช่หรือไม่';
        const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
            if (a) {
                this.edcDataTypeService.delete(obj).subscribe(
                    response => {
                        console.log(response);
                        this.getList(this.dataTypeSearchForm.value.edcDataTypeC);
                        this.modal.openModal('แจ้งเตือน', 'ลบข้อมูลประเภทข้อมูลสำเร็จ');
                        const modalHide = this.modal.modalService.onHide.subscribe(e => {
                            modalHide.unsubscribe();
                        });
                    },
                    err => {
                    if (err.error.errorCode === 'ST096') {
                        this.modal.openModal('แจ้งเตือน', 'ไม่สามารถลบประเภทข้อมูลได้ เนื่องจากประเภทข้อมูลนี้มีผู้นำส่งลงทะเบียนแล้ว');
                      } else {
                      this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                     }
                    }
                );
            }
            sub.unsubscribe();
        });
    }

    initialForm() {
        this.dataTypeSearchForm = new FormGroup({
            edcDataTypeC: new FormGroup({
                'dataTypeCode': new FormControl(null),
                'description': new FormControl(null),
                'encPkiCert': new FormControl(null),
                'isProcess': new FormControl(null),
                'status': new FormControl('ACTIVE'),
            })
        });
    }

    resetForm() {
        this.dataTypeSearchForm.reset();
        this.initialForm();
        this.getList(this.dataTypeSearchForm.value.edcDataTypeC);
    }

    getList(param) {
        this.edcDataTypeService.list(param).subscribe(result => {
            // console.log(result.data);
            this.DataTypeModelList = result.data;
            this.p = result.page;
            this.total = result.total;
            this.pageSize = result.pageSize;
        });
    }

    getPage(page: number) {
        this.dataTypeSearchForm.value.edcDataTypeC.page = page;
        this.getList(this.dataTypeSearchForm.value.edcDataTypeC);
    }

    getPageSize(pageSize: number) {
        this.dataTypeSearchForm.value.edcDataTypeC.pageSize = pageSize;
        this.getList(this.dataTypeSearchForm.value.edcDataTypeC);
    }
}
