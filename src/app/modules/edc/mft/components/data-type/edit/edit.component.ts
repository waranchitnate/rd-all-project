import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EdcDataTypeService} from '../edcDataType.service';
import {ModalService} from '../../../../../../core/services/modal.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DataTypeModel} from '../../../models/dataType.model';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {AnswerModalComponent} from '../../../../../../shared/modals/answer-modal/answer-modal.component';
import {AlertTextModule} from '../../../edcAlertText.module';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  dataTypeAddForm: FormGroup;
  currentFileUpload: File;
  dataTypeObj: DataTypeModel;
  state$: Observable<DataTypeModel>;
  checkDataTypeCode = false;
  defaultPatternFile: string;

  constructor(
    private edcDataTypeService: EdcDataTypeService,
    private modal: ModalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private alertText: AlertTextModule
  ) {
  }

  ngOnInit() {
    this.state$ = this.activatedRoute.paramMap.pipe(map(() => window.history.state));
    this.state$.subscribe((e: DataTypeModel) => {
      this.dataTypeObj = e;
    });
    if (this.dataTypeObj.id === undefined) {
      this.router.navigateByUrl('/mft/data-type');
    }
    if (this.dataTypeObj.dataTypeCode === 'DT001' || this.dataTypeObj.dataTypeCode === 'DT002' || this.dataTypeObj.dataTypeCode === 'DT003' || this.dataTypeObj.dataTypeCode === 'DT008') {
      this.checkDataTypeCode = true;
    }
    this.initialForm();
    this.checkRequired();
  }

  onBack() {
    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ข้อมูลประเภทข้อมูลยังไม่ถูกแก้ไข ต้องการยกเลิกการแก้ไขข้อมูลใช่หรือไม่';
    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a) {
        console.log(this.dataTypeAddForm.value.edcDataTypeObj);
        this.router.navigateByUrl('/mft/data-type');
        sub.unsubscribe();
      }
    });
  }

  onSubmit() {
    // if (this.currentFileUpload == null) {
    //   this.currentFileUpload = new File([''], 'nullFile.txt', {type: 'text/plain'});
    //   console.log(this.currentFileUpload);
    // }

    const modalRef = this.modal.modalService.show(AnswerModalComponent);
    (modalRef.content as AnswerModalComponent).title = 'แจ้งเตือน';
    (modalRef.content as AnswerModalComponent).content = 'ต้องการแก้ไขข้อมูลประเภทข้อมูลใช่หรือไม่';

    const sub = (modalRef.content as AnswerModalComponent).answerEvent.subscribe(a => {
      if (a && this.dataTypeAddForm.valid) {
        this.dataTypeAddForm.value.edcDataTypeObj.id = this.dataTypeObj.id;
        if ( this.dataTypeAddForm.value.edcDataTypeObj.encPkiCert === 'Y') {
        this.edcDataTypeService.checkPassword(this.currentFileUpload, this.dataTypeAddForm.value.edcDataTypeObj).subscribe(
          response => {
            this.dataTypeAddForm.value.edcDataTypeObj.id = this.dataTypeObj.id;
            this.edcDataTypeService.update(this.currentFileUpload, this.dataTypeAddForm.value.edcDataTypeObj).subscribe(
              res => {
                this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลประเภทข้อมูลสำเร็จ');
                const modalHide = this.modal.modalService.onHide.subscribe(e => {
                  modalHide.unsubscribe();
                });
                this.router.navigateByUrl('/mft/data-type');
              },
              err => {
                if (err.error.errorCode === 'ST098') {
                  this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสประเภทข้อมูล' + this.alertText.getTail);
                } else if (err.error.errorCode === 'ST096') {
                  this.modal.openModal('แจ้งเตือน', 'ไม่สามารถแก้ไขประเภทข้อมูลแบบย่อได้ เนื่องจากประเภทข้อมูลนี้มีผู้นำส่งลงทะเบียนแล้ว');
                } else {
                  this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
                }
              }
            );
          },
          err => {
            this.modal.openModal('แจ้งเตือน', 'รหัส Key passphrase หรือ PKCS#12 Key  ไม่ตรงกัน');
          }
        );
      } else if (this.dataTypeAddForm.value.edcDataTypeObj.encPkiCert === 'N') {
          this.edcDataTypeService.update(this.currentFileUpload, this.dataTypeAddForm.value.edcDataTypeObj).subscribe(
            res => {
              this.modal.openModal('แจ้งเตือน', 'แก้ไขข้อมูลประเภทข้อมูลสำเร็จ');
              const modalHide = this.modal.modalService.onHide.subscribe(e => {
                modalHide.unsubscribe();
              });
              this.router.navigateByUrl('/mft/data-type');
            },
            err => {
              console.log('err > ', err);
              if (err.error.errorCode === 'ST098') {
                this.modal.openModal('แจ้งเตือน', this.alertText.getHead + 'รหัสประเภทข้อมูล' + this.alertText.getTail);
              } else if (err.error.errorCode === 'ST096') {
                this.modal.openModal('แจ้งเตือน', 'ไม่สามารถแก้ไขประเภทข้อมูลแบบย่อได้ เนื่องจากประเภทข้อมูลนี้มีผู้นำส่งลงทะเบียนแล้ว');
              } else {
                this.modal.openModal('แจ้งเตือน', 'ขออภัยระบบไม่สามารถดำเนินการได้ในขณะนี้ โปรดทำรายการใหม่อีกครั้งหรือติดต่อผู้ดูแลระบบเพื่อแก้ไขปัญหานี้');
              }
            }
          );
        }
      }
      sub.unsubscribe();
    });
  }

  initialForm() {
    // this.defaultPatternFile = this.dataTypeObj.patternFileName.toString();
    this.dataTypeAddForm = new FormGroup({
      edcDataTypeObj: new FormGroup({
        'dataTypeCode': new FormControl(this.dataTypeObj.dataTypeCode, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]),
        'dataTypeDesTh': new FormControl(this.dataTypeObj.dataTypeDesTh),
        'dataTypeDesEn': new FormControl(this.dataTypeObj.dataTypeDesEn, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\/\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'fileSizeLimit': new FormControl(this.dataTypeObj.fileSizeLimit, [Validators.required, Validators.max(10240), Validators.pattern('^[0-9]+$')]),
        'inputPath': new FormControl(this.dataTypeObj.inputPath, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'statusPath': new FormControl(this.dataTypeObj.statusPath, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'reportPath': new FormControl(this.dataTypeObj.reportPath, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿\\/ ]+$')]),
        'encPkiCert': new FormControl(this.dataTypeObj.encPkiCert),
        'isProcess': new FormControl(this.dataTypeObj.isProcess, Validators.required),
        'certFile': new FormControl(this.dataTypeObj.certFile),
        'certKey': new FormControl(this.dataTypeObj.certKey),
        'passPhraseKey': new FormControl(this.dataTypeObj.passPhraseKey),
        'patternFileName': new FormControl(this.dataTypeObj.patternFileName),
        'patternFileNameType': new FormControl(this.dataTypeObj.patternFileNameType, Validators.required),
        'dataTypeCodeAbb': new FormControl(this.dataTypeObj.dataTypeCodeAbb, [Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]),
        'certFileName': new FormControl(this.dataTypeObj.certFileName),
        'sendEpayMentFlag': new FormControl(this.dataTypeObj.sendEpayMentFlag, Validators.required),
        'rootDirectoryEpayment': new FormControl(this.dataTypeObj.rootDirectoryEpayment),
        'inputPathEpayment': new FormControl(this.dataTypeObj.inputPathEpayment),
        'sendReportEpayment': new FormControl(this.dataTypeObj.sendReportEpayment),
        'reportPathEpayment': new FormControl(this.dataTypeObj.reportPathEpayment),
        'createdDate': new FormControl(this.dataTypeObj.createdDate),
        'createdBy': new FormControl(this.dataTypeObj.createdBy)
      })
    });
  }

  selectFile(event) {
    this.dataTypeAddForm.get('edcDataTypeObj.certFileName').setValue( <File>event.target.files[0].name);
    this.currentFileUpload = <File>event.target.files[0];
    console.log(this.currentFileUpload);
  }

  selectSendReportEpayment() {
    if ( this.dataTypeAddForm.value.edcDataTypeObj.sendReportEpayment === true) {
      this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').setValidators([Validators.required]);
      this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]);
    } else {
      this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').clearValidators();
      this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').clearValidators();
    }
    this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').reset();

    this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').updateValueAndValidity();
  }

  checkCert() {
    if (this.dataTypeAddForm.get('edcDataTypeObj.encPkiCert').value === 'N') {
      this.dataTypeAddForm.get('edcDataTypeObj.certKey').reset();
      this.dataTypeAddForm.get('edcDataTypeObj.certKey').updateValueAndValidity();
    }
  }

  resetForm() {
    this.dataTypeAddForm.reset();
  }

  fixPath(event) {
    console.log(event);
  }

  checkRequired() {
    if ( this.dataTypeAddForm.value.edcDataTypeObj.sendEpayMentFlag === 'Y') {
      console.log('pass condition sendEpayMentFlag === Y');
      this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]);
      this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]);
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9\\-\\_ ]+$')]);
      this.dataTypeAddForm.get('edcDataTypeObj.sendEpayMentFlag').setValue(true);
      if ( this.dataTypeAddForm.value.edcDataTypeObj.sendReportEpayment === 'Y') {
        this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9\!\\"\\#\\$\\%\\&\\\'\\(\\)\\*\\+\\,\\-\\.\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\{\\|\\}\\฿ ]+$')]);
        this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').setValue(true);
      } else {
        this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').setValue(false);
      }
    } else {
      this.dataTypeAddForm.get('edcDataTypeObj.sendEpayMentFlag').setValue(false);
    }
    if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType !== '2') {
      this.defaultPatternFile = this.dataTypeObj.patternFileName.toString();
    } else if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '0') {
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').setValidators( [Validators.required]);
    } else {
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').setValidators( [Validators.pattern('^[a-zA-Z0-9\\(\\)\\.\\[\\]\\_\\-]+$')]);
    }
    //
    if (this.dataTypeAddForm.get('edcDataTypeObj.encPkiCert').value === 'N') {
      this.dataTypeAddForm.get('edcDataTypeObj.certKey').reset();
    }
    if ( this.dataTypeAddForm.value.edcDataTypeObj.sendEpayMentFlag === true) {
      this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]);
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9\\-\\_ ]+$')]);
    }
    if ( this.dataTypeAddForm.value.edcDataTypeObj.sendReportEpayment === true) {
      this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]);
    } else {
      this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').clearValidators();
      this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').clearValidators();
    }
    this.dataTypeAddForm.get('edcDataTypeObj.certKey').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').updateValueAndValidity();
  }

  onInput() {
    // set Value condition
    if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '1') {
      if (this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCode === 'DT001') {
        this.defaultPatternFile = 'ED[Receiver_FI_Code][YYYYMMDD][SEQ NO]';
      } else if (this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCode === 'DT002') {
        this.defaultPatternFile = 'EC[Receiver_FI_Code][YYYYMMDD][SEQ NO]';
      } else if (this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCode === 'DT003') {
        this.defaultPatternFile = 'DS[Receiver_FI_Code][YYYYMMDD][SEQ NO],MS[Receiver FI Code][YYYYMMDD][SEQ NO]';
      } else if (this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCode === 'DT008') {
        this.defaultPatternFile = 'MT[Receiver_FI_Code][YYYYMMDD][SEQ NO]';
      } else {
        this.defaultPatternFile = '[Data_Type_ABB][Receiver_FI_Code][YYYYMMDD][SEQ_NO]';
      }
      this.checkDataTypeCode = true;
    }

    if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '0') {
      this.defaultPatternFile = null;
      this.checkDataTypeCode = false;
    }
    // set validate condition
    if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '0' || this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '1')  {
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9\\(\\)\\.\\[\\]\\_\\-]+$')]);
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').updateValueAndValidity();
    }
    if (this.dataTypeAddForm.value.edcDataTypeObj.patternFileNameType === '2') {
      this.defaultPatternFile = null;
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').setValue(null);
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').setValidators(null);
      this.dataTypeAddForm.get('edcDataTypeObj.patternFileName').updateValueAndValidity();
    }
    // else {
    //   console.log('pass else');
    //   this.defaultPatternFile = null;
    //   this.checkDataTypeCode = false;
    // }
  }

  checkSendEpayFlag() {
    if ( this.dataTypeAddForm.value.edcDataTypeObj.sendEpayMentFlag === true) {
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValue(this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCodeAbb);
      this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]);
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValidators([Validators.required, Validators.pattern('^[a-zA-Z0-9 ]+$')]);
    }
    if (this.dataTypeAddForm.value.edcDataTypeObj.sendEpayMentFlag === false) {
      this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').setValue(false);
      this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').setValidators(null);
      this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').setValidators(null);
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValidators(null);
    }

    this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValue(null);
    this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').setValue(null);
    this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').setValue(null);

    this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.inputPathEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.sendReportEpayment').updateValueAndValidity();
    this.dataTypeAddForm.get('edcDataTypeObj.reportPathEpayment').updateValueAndValidity();
  }
  defaltRoot() {
    if ( this.dataTypeAddForm.value.edcDataTypeObj.sendEpayMentFlag === true) {
      this.dataTypeAddForm.get('edcDataTypeObj.rootDirectoryEpayment').setValue(this.dataTypeAddForm.value.edcDataTypeObj.dataTypeCodeAbb);
    }
  }

  checkPassword() {
    this.edcDataTypeService.checkPassword(this.currentFileUpload, this.dataTypeAddForm.value.edcDataTypeObj).subscribe(
      response => {
        this.modal.openModal('แจ้งเตือน', 'รหัส PKCS#12 Key ตรงกัน');
      },
      err => {
        this.modal.openModal('แจ้งเตือน', 'รหัส PKCS#12 Key ไม่ตรงกัน');
      }
    );

  }
}
