import { Organization } from 'src/app/modules/admin/setting-organization/models/organization.model';
import { ReasonRequestModel } from './../models/reason-request.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { MTaxTypeModel } from '../models/m-tax-type.model';
import { DocumentRequestModel } from '../models/document-request.model';

@Injectable({
  providedIn: 'root'
})
export class PpsMasterService {

  constructor(private http: HttpClient) { }

  internalPpsApiUrl = environment.ppsApiUrl;
  ppsMasterEndpointUrl = 'pps/master';

  public getTaxTypeList(): Observable<Array<MTaxTypeModel>> {
    return this.http.get<Array<MTaxTypeModel>>(this.internalPpsApiUrl + this.ppsMasterEndpointUrl + '/getTaxTypeList');
  }

  public getReasonRequestList(): Observable<Array<ReasonRequestModel>> {
    return this.http.get<Array<ReasonRequestModel>>(this.internalPpsApiUrl + this.ppsMasterEndpointUrl + '/getReasonRequestList');
  }

  public getDocumentRequestList(): Observable<Array<DocumentRequestModel>> {
    return this.http.get<Array<DocumentRequestModel>>(this.internalPpsApiUrl + this.ppsMasterEndpointUrl + '/getDocumentRequestList');
  }

  public getOrganizationList(): Observable<Array<Organization>> {
    return this.http.get<Array<Organization>>(this.internalPpsApiUrl + this.ppsMasterEndpointUrl + '/getOrganizationList');
  }

}
