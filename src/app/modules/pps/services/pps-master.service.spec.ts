import { TestBed } from '@angular/core/testing';

import { PpsMasterService } from './pps-master.service';

describe('PpsMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PpsMasterService = TestBed.get(PpsMasterService);
    expect(service).toBeTruthy();
  });
});
