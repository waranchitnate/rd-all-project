import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'request', loadChildren: './modules/request/request.module#RequestModule'},
  { path: 'reports', loadChildren: './modules/reports/reports.module#ReportsModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PpsRoutingModule { }
