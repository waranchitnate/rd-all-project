import {TaxRequestModel} from './tax-request.model';

export class TaxRequestAnalystModel {
  taxRequestAnalystId: number;
  taxRequest: TaxRequestModel;
  prevApproveType: string;
  currApproveType: string;
  remark: string;
}
