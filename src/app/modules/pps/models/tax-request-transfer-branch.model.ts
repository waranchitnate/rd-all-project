import {TaxRequestModel} from './tax-request.model';

export class TaxRequestTransferBranchModel {
  taxTransferBranchId: number;
  taxRequest: TaxRequestModel;
  prevBranch: string;
  currBranch: string;
  remark: string;
  createdDate: Date;
  officeCode: string;
}
