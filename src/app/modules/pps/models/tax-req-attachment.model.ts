export interface TaxReqAttachmentModel {
  hashTaxReqAttachmentId: string;
  docRequestCode: string;
  fileName: string;
  path: string;
  remark: string;
  file: File;
  checked: boolean;
  dataFlag: string;
  fileBase64: string;
}
