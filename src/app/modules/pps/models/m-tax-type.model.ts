export class MTaxTypeModel {
  taxTypeCode: string;
  taxTypeName: string;
  status: number;
}
