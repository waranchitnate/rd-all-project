import {DocumentRequestModel} from './document-request.model';

export class ConfigDocumentModel {
  configDocId: number;
  documentRequest: DocumentRequestModel;
  taxTypeCode: string;
  status: number;
}
