export class ReasonRequestModel {
  reasonCode: string;
  reasonName: string;
  status: number;
}
