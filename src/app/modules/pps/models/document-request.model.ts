import {SafeResourceUrl} from '@angular/platform-browser';

export class DocumentRequestModel {
  docRequestCode: string;
  docRequestName: string;
  status: number;
  file: File;
  safeResourceUrl: SafeResourceUrl;
  fileName: string;
  fileBase64: string;
}
