import {TaxRequestModel} from './tax-request.model';

export class TaxRequestLogModel {
  taxRequestLogId: number;
  taxRequest: TaxRequestModel;
  taxRequestStatus: string;
  taxRequestDesc: string;
  remark: string;
}
