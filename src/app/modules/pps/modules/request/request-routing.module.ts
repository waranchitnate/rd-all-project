import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'admin', loadChildren: './modules/request-admin/request-admin.module#RequestAdminModule'},
  {path: 'tax-request', loadChildren: './modules/tax-request/tax-request.module#TaxRequestModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestRoutingModule { }
