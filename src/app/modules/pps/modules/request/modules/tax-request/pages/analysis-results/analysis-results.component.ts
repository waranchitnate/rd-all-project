import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';
import {MTaxTypeModel} from '../../../../../../models/m-tax-type.model';
import {MApproveTypeEnum} from '../../../../../../enum/m-approve-type';
import {TaxRequestAnalystModel} from '../../../../../../models/tax-request-analyst.model';
import {Observable} from 'rxjs';
import {NameValuePairModel} from '../../../../../../../../shared/models/NameValuePair.model';
import {TaxRequestService} from '../../services/tax-request.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-analysis-results',
  templateUrl: './analysis-results.component.html',
  styleUrls: ['./analysis-results.component.css']
})
export class AnalysisResultsComponent implements OnInit {

  analysisFormGroup: FormGroup;
  mApproveTypeOption: any[];
  approveTypeListObs: Observable<Array<NameValuePairModel>>;
  approveTypeSelected: any;
  taxRequestModel: TaxRequestModel;
  disabledText: Boolean = false;

  constructor(private fb: FormBuilder, private taxRequestService: TaxRequestService, private toastrService: ToastrService,
              private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    const taxReqId = this.activatedRoute.snapshot.params.id;
    if (taxReqId == null) {
      this.toastrService.error('ไม่พบหมายเลขอ้างอิงรายการ', 'ข้อผิดพลาด');
      this.router.navigateByUrl('../../tax-refund-request-list');
    }

    this.approveTypeListObs = this.taxRequestService.getAllApproveType();
    this.analysisFormGroup = new FormGroup({
      taxRequestId: new FormControl((+taxReqId)),
      approveAmount: new FormControl(null, Validators.required),
      // TO DO CREATE APPROVE TYPE VALIDATOR
      approveType: new FormControl(null),
      refRequestNo: new FormControl(null, Validators.required),
      approveStatus: new FormControl(true, Validators.required),
    });
    this.taxRequestService.getTaxRequestById((+taxReqId)).subscribe(res => {
      this.taxRequestModel = res.data;
      this.approveAmountControl.setValue(res.data.requestAmount);
    });
  }

  private addDays(date: Date, days: number): Date {
    date = new Date();
    date.setDate(date.getDate() + days);
    return date;
  }

  public save() {
    console.log(this.analysisFormGroup.value);
    if (this.analysisFormGroup.invalid) {
      this.toastrService.error('กรุณาระบุข้อมูลให้ถูกต้อง', 'ข้อผิดพลาด');
      this.markFormGroupTouched(this.analysisFormGroup);
    } else {
      if (this.approveStatusControl.value) {
        this.taxRequestService.approveRequestK10(this.analysisFormGroup.value).subscribe(() => {
          this.toastrService.success('อนุมัติคำขอสำเร็จ');
        }, err => {
          this.toastrService.error('อนุมัติคำขอไม่สำเร็จเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
        });
      } else {
        this.approveAmountControl.setValue(null);
        this.taxRequestService.disapproveRequestK10(this.analysisFormGroup.value).subscribe(() => {
          this.toastrService.success('ไม่อนุมัติคำขอสำเร็จ');
        }, err => {
          this.toastrService.error('ไม่อนุมัติคำขอไม่สำเร็จเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
        });
      }
      this.disabledText = true;
    }
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  get approveAmountControl(): AbstractControl {
    return this.analysisFormGroup.get('approveAmount');
  }

  get approveTypeControl(): AbstractControl {
    return this.analysisFormGroup.get('approveType');
  }

  get refRequestNoControl(): AbstractControl {
    return this.analysisFormGroup.get('refRequestNo');
  }

  get approveStatusControl(): AbstractControl {
    return this.analysisFormGroup.get('approveStatus');
  }

}
