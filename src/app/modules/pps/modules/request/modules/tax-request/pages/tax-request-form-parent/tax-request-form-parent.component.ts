import { Component, OnInit } from '@angular/core';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject, Subject} from 'rxjs';
import {TaxRequestService} from '../../services/tax-request.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-tax-request-form-parent',
  templateUrl: './tax-request-form-parent.component.html',
  styleUrls: ['./tax-request-form-parent.component.css']
})
export class TaxRequestFormParentComponent implements OnInit {

  taxRequestId: string;
  taxRequestModelObs = new BehaviorSubject<TaxRequestModel>(null);
  constructor(private activatedRoute: ActivatedRoute, private taxRequestService: TaxRequestService, private toastrService: ToastrService) { }

  ngOnInit() {
    this.taxRequestId = this.activatedRoute.snapshot.params.id;
    if (this.taxRequestId !== undefined) {
      console.log('taxRequestId: ', this.taxRequestId);
      this.taxRequestService.getTaxRequestById(+this.taxRequestId).subscribe((res: any) => {
        this.taxRequestModelObs.next(res.data);
      }, err => {
        this.toastrService.error('ไม่สารถดึงข้อมูลได้เนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
      });
    } else {
      this.taxRequestModelObs.next(new TaxRequestModel());
    }
  }

}
