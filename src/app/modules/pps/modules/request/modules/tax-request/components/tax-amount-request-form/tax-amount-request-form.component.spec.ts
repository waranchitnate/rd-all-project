import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAmountRequestFormComponent } from './tax-amount-request-form.component';

describe('TaxRequestFormComponent', () => {
  let component: TaxAmountRequestFormComponent;
  let fixture: ComponentFixture<TaxAmountRequestFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxAmountRequestFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAmountRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
