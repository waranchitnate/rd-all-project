import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RptK10Component} from './pages/rpt-k10/rpt-k10.component';
import {TransferApprovalComponent} from './pages/transfer-approval/transfer-approval.component';
import {AnalysisResultsComponent} from './pages/analysis-results/analysis-results.component';
import {TaxRefundRequestListComponent} from './pages/tax-refund-request-list/tax-refund-request-list.component';
import {TaxRequestFormComponent} from './pages/tax-request-form/tax-request-form.component';
import {TransferApprovalDetailComponent} from './pages/transfer-approval-detail/transfer-approval-detail.component';
import {TaxRequestConsideredComponent} from './pages/tax-request-considered/tax-request-considered.component';
import {TaxRequestFormParentComponent} from './pages/tax-request-form-parent/tax-request-form-parent.component';
import {ConfirmTaxRequestFormComponent} from './pages/confirm-tax-request-form/confirm-tax-request-form.component';
import {TaxRequestDetailComponent} from './pages/tax-request-detail/tax-request-detail.component';

const routes: Routes = [
  {path: 'tax-refund-request-list', component: TaxRefundRequestListComponent},
  {
    path: 'create-request-k10', component: TaxRequestFormParentComponent, children: [
      {path: '', component: TaxRequestFormComponent},
      {path: 'confirm', component: ConfirmTaxRequestFormComponent}
    ]
  },
  {
    path: 'edit-request-k10/:id', component: TaxRequestFormParentComponent, children: [
      {path: '', component: TaxRequestFormComponent},
      {path: 'confirm', component: ConfirmTaxRequestFormComponent}
    ]
  },
  {path: 'consider-request-k10/:id', component: TaxRequestConsideredComponent},
  {path: 'request-k10-detail/:id', component: TaxRequestDetailComponent},
  {path: 'transfer-approval', component: TransferApprovalComponent},
  {path: 'transfer-approval-detail', component: TransferApprovalDetailComponent},
  {path: 'analysis-results/:id', component: AnalysisResultsComponent},
  {path: 'rpt-k10', component: RptK10Component}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxRequestRoutingModule {
}
