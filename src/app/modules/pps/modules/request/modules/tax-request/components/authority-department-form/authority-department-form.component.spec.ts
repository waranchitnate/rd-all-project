import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorityDepartmentFormComponent } from './authority-department-form.component';

describe('AutorityDepartmentFormComponent', () => {
  let component: AuthorityDepartmentFormComponent;
  let fixture: ComponentFixture<AuthorityDepartmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorityDepartmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorityDepartmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
