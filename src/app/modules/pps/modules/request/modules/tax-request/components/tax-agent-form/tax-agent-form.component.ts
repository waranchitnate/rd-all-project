import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-tax-agent-form',
  templateUrl: './tax-agent-form.component.html',
  styleUrls: ['./tax-agent-form.component.css']
})
export class TaxAgentFormComponent implements OnInit {

  @ViewChild('branchDigit1') branchDigit1: ElementRef;
  @ViewChild('branchDigit2') branchDigit2: ElementRef;
  @ViewChild('branchDigit3') branchDigit3: ElementRef;
  @ViewChild('branchDigit4') branchDigit4: ElementRef;
  @ViewChild('branchDigit5') branchDigit5: ElementRef;
  @Input() taxRequestForm: FormGroup;

  constructor() {
  }

  ngOnInit() {
    // TO DO SET FORM CONTROL VALUE WHEN BRANCH NO CHANGE
    this.setBranchDigit(this.taxRequestForm.get('branchNo').value);
  }

  branchDigitChange(value: string, key: string, element: HTMLInputElement) {
    const numberChar = '0123456789';
    if (value !== '' && numberChar.indexOf(key) !== -1) {
      element.focus();
    }
  }

  setBranchDigit(branchDigit: string) {
    if (branchDigit != null) {
      if (branchDigit.length > 1) {
        this.branchDigit1.nativeElement.value = '1';
      }
      if (branchDigit.length > 2) {
        this.branchDigit2.nativeElement.value = branchDigit[1];
      }
      if (branchDigit.length > 3) {
        this.branchDigit3.nativeElement.value = branchDigit[2];
      }
      if (branchDigit.length > 4) {
        this.branchDigit4.nativeElement.value = branchDigit[3];
      }
      if (branchDigit.length >= 5) {
        this.branchDigit5.nativeElement.value = branchDigit[4];
      }
    }
  }

  get taxAgentPinControl(): AbstractControl {
    return this.taxRequestForm.get('taxAgentPin');
  }

  get taxAgentNameControl(): AbstractControl {
    return this.taxRequestForm.get('taxAgentName');
  }

  get telephoneControl(): AbstractControl {
    return this.taxRequestForm.get('telephone');
  }
}
