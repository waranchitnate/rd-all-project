import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestAgentDetailComponent } from './tax-request-agent-detail.component';

describe('TaxRequestAgentDetailComponent', () => {
  let component: TaxRequestAgentDetailComponent;
  let fixture: ComponentFixture<TaxRequestAgentDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestAgentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestAgentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
