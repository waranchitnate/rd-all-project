import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmTaxRequestFormComponent } from './confirm-tax-request-form.component';

describe('ConfirmTaxRequestFormComponent', () => {
  let component: ConfirmTaxRequestFormComponent;
  let fixture: ComponentFixture<ConfirmTaxRequestFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmTaxRequestFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmTaxRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
