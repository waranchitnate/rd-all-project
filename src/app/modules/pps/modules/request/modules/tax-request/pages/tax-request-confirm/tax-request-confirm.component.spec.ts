import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestConfirmComponent } from './tax-request-confirm.component';

describe('TaxRequestConfirmComponent', () => {
  let component: TaxRequestConfirmComponent;
  let fixture: ComponentFixture<TaxRequestConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
