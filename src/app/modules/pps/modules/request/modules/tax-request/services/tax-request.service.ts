import {Injectable} from '@angular/core';
import {TaxRequestPageRequestModel} from '../models/tax-request-page-request.model';
import {Observable, of, throwError as observableThrowError} from 'rxjs';
import {PageResponse} from '../../../../../../../shared/models/page-response';
import {TaxRequestPageResponseModel} from '../models/tax-request-page-response.model';
import {TaxRequestModel} from '../../../../../models/tax-request.model';
import {MTaxTypeModel} from '../../../../../models/m-tax-type.model';
import {DocumentRequestModel} from '../../../../../models/document-request.model';
import {catchError, delay} from 'rxjs/operators';
import {PpsMasterService} from './../../../../../services/pps-master.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../../../../environments/environment';
import {NameValuePairModel} from '../../../../../../../shared/models/NameValuePair.model';
import {PageRequest} from '../../../../../../../shared/models/page-request';
import {ToastrService} from 'ngx-toastr';
import {ResponseT} from '../../../../../../../shared/models/response.model';

@Injectable({
  providedIn: 'root'
})
export class TaxRequestService {

  private taxRequestApi = environment.ppsApiUrl + 'tax-request/';

  constructor(private httpClient: HttpClient, private ppsMasterService: PpsMasterService, private toastrService: ToastrService) {
  }

  public getAllApproveType(): Observable<Array<NameValuePairModel>> {
    return this.httpClient.get<Array<NameValuePairModel>>(this.taxRequestApi + 'approve-type');
  }

  public approveDocument(taxRequestId: number) {
    return this.httpClient.put(this.taxRequestApi + 'approve-document/' + taxRequestId, null);
  }

  public addTransferBranch(taxRequestModel: TaxRequestModel) {
    return this.httpClient.post(this.taxRequestApi + 'add-transfer-branch', taxRequestModel);
  }

  public approveRequestK10(taxRequestModel: TaxRequestModel) {
    return this.httpClient.put(this.taxRequestApi + 'approve-request-k10', taxRequestModel);
  }

  public disapproveRequestK10(taxRequestModel: TaxRequestModel) {
    return this.httpClient.put(this.taxRequestApi + 'disapprove-request-k10', taxRequestModel);
  }

  public taxRequestPagination(taxRequestPageRequestModel: TaxRequestPageRequestModel): Observable<PageResponse<TaxRequestPageResponseModel>> {
    const copyPageRequest: PageRequest = new PageRequest();
    Object.assign(copyPageRequest, taxRequestPageRequestModel);
    copyPageRequest.page -= 1;
    return this.httpClient.post<PageResponse<TaxRequestPageResponseModel>>(this.taxRequestApi + 'pagination', copyPageRequest)
      .pipe(
        catchError(err => {
          this.toastrService.error('ไม่สามารถดึงข้อมูลคำร้องขอคืนภาษีอากรได้เนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
          return observableThrowError(err);
        })
      );
    ;
  }

  public getAllStatus(): Observable<Array<NameValuePairModel>> {
    return this.httpClient.get<Array<NameValuePairModel>>(this.taxRequestApi + 'status');
  }

  public getAllMTaxType(): Observable<Array<MTaxTypeModel>> {
    return this.httpClient.get<Array<MTaxTypeModel>>(this.taxRequestApi + 'm-tax-type');
  }

  public getTaxRequestById(taxRequestId): Observable<ResponseT<TaxRequestModel>> {
    return this.httpClient.post<ResponseT<TaxRequestModel>>(this.taxRequestApi + 'getTaxRequestById', taxRequestId);
  }

  public save(taxRequestModel): Observable<TaxRequestModel> {
    // taxRequestModel.docNoticeNumber = 'K1007110001';
    // taxRequestModel.createdDate = new Date();
    // return of(taxRequestModel);
    return <Observable<TaxRequestModel>>this.httpClient.post(this.taxRequestApi + 'saveTaxRequest', taxRequestModel);
  }

  public getTaxRequest(taxRequestId: number): Observable<TaxRequestModel> {
    return of(new TaxRequestModel()).pipe(delay(2000));
  }

  public getDocumentRequestByTaxType(taxTypeCode: string): Observable<Array<DocumentRequestModel>> {
    // return of(this.mockDocumentRequest()).pipe(delay(1000));
    return this.ppsMasterService.getDocumentRequestList();
  }

  public uploadTaxRequestAttachments(fileForm) {
    return this.httpClient.post(this.taxRequestApi + 'uploadTaxRequestAttachments', fileForm);
  }


  mockDocumentRequest(): Array<DocumentRequestModel> {
    return [
      <DocumentRequestModel>{
        docRequestName: 'ภาพถ่ายหนังสือรับรองของนายทะเบียนหุ้นส่วนบริษัท',
        docRequestCode: 'TEST-001'
      },
      <DocumentRequestModel>{
        docRequestName: 'ภาพถ่ายบัตรประจำตัวประชาชน',
        docRequestCode: 'TEST-002'
      }
    ];
  }
}
