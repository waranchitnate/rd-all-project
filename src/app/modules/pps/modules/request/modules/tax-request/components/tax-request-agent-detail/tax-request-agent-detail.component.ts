import {Component, Input, OnInit} from '@angular/core';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';

@Component({
  selector: 'app-tax-request-agent-detail',
  templateUrl: './tax-request-agent-detail.component.html',
  styleUrls: ['./tax-request-agent-detail.component.css']
})
export class TaxRequestAgentDetailComponent implements OnInit {

  @Input() taxRequestModel: TaxRequestModel;
  constructor() { }

  ngOnInit() {
  }

}
