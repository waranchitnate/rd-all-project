import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAmountRequestDetailComponent } from './tax-amount-request-detail.component';

describe('TaxAmountRequestDetailComponent', () => {
  let component: TaxAmountRequestDetailComponent;
  let fixture: ComponentFixture<TaxAmountRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxAmountRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAmountRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
