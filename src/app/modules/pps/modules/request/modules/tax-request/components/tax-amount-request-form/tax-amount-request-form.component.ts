import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormGroup} from '@angular/forms';
import {DocumentRequestModel} from '../../../../../../models/document-request.model';
import { MTaxTypeModel } from 'src/app/modules/pps/models/m-tax-type.model';
import { ReasonRequestModel } from 'src/app/modules/pps/models/reason-request.model';
import { PpsMasterService } from 'src/app/modules/pps/services/pps-master.service';

@Component({
  selector: 'app-tax-amount-request-form',
  templateUrl: './tax-amount-request-form.component.html',
  styleUrls: ['./tax-amount-request-form.component.css']
})
export class TaxAmountRequestFormComponent implements OnInit {

  @Input() taxRequestForm: FormGroup;
  @Input() documentRequestModels: Array<DocumentRequestModel>;
  taxTypeList: Array<MTaxTypeModel> = [];
  reasonRequestList: Array<ReasonRequestModel> = [];
  constructor(private ppsMasterService: PpsMasterService) { }

  ngOnInit() {
    this.ppsMasterService.getTaxTypeList().subscribe((res: any) => {
      this.taxTypeList = res.data;
    });

    this.ppsMasterService.getReasonRequestList().subscribe((res: any) => {
      this.reasonRequestList = res.data;
    });

    this.taxTypeCodeControl.valueChanges.subscribe(taxTypecode => {
      console.log('taxTypecode: ', taxTypecode);
      const taxType = this.taxTypeList.filter(s => s.taxTypeCode === taxTypecode);
      this.taxRequestForm.get('taxType').get('taxTypeName').setValue(taxType.length > 0 ? taxType[0].taxTypeName : null);
    });

    this.reasonCodeControl.valueChanges.subscribe(reasonCode => {
      const reasonRequest = this.reasonRequestList.filter(s => s.reasonCode === reasonCode);
      this.taxRequestForm.get('reasonRequest').get('reasonName').setValue(reasonRequest.length > 0 ? reasonRequest[0].reasonName : null);
    });
  }

  get taxRequestTransferBranchDtoFormArray(): FormArray {
    return this.taxRequestForm.get('taxRequestTransferBranchDto') as FormArray;
  }

  get accountPeriodStartControl(): AbstractControl {
    return this.taxRequestForm.get('accountPeriodStart');
  }

  get accountPeriodEndControl(): AbstractControl {
    return this.taxRequestForm.get('accountPeriodEnd');
  }

  get reasonRequestControl(): AbstractControl {
    return this.taxRequestForm.get('reasonRequest');
  }

  get reasonCodeControl(): AbstractControl {
    return this.taxRequestForm.get('reasonRequest').get('reasonCode');
  }

  get taxTypeCodeControl(): AbstractControl {
    return this.taxRequestForm.get('taxType').get('taxTypeCode');
  }
}
