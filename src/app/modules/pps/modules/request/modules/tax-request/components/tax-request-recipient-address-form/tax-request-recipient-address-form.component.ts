import {Component, Input, OnInit} from '@angular/core';
import {FormGroup, ControlContainer, AbstractControl} from '@angular/forms';
import { ProvinceModel } from 'src/app/core/models/province.model';
import { DistrictModel } from 'src/app/core/models/district.model';
import { SubDistrictModel } from 'src/app/core/models/sub-district.model';
import { RdCommonApiService } from 'src/app/core/services/rd-common-api.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-tax-request-recipient-address-form',
  templateUrl: './tax-request-recipient-address-form.component.html',
  styleUrls: ['./tax-request-recipient-address-form.component.css']
})
export class TaxRequestRecipientAddressFormComponent implements OnInit {

  @Input() taxRequestForm: FormGroup;
  provinceRecipientList: Array<ProvinceModel> = [];
  districtRecipientList: Array<DistrictModel> = [];
  subDistrictRecipientList: Array<SubDistrictModel> = [];

  constructor(public controlContainer: ControlContainer, private commonService: RdCommonApiService) { }

  ngOnInit() {
    this.commonService.getAllProvince().subscribe(res => {
      this.provinceRecipientList = res;
      this.chackValueIn('recipientProvinceCode', res);
    });

    this.provinceCodeControl.valueChanges
      .pipe(finalize(() => console.log('fin')))
      .subscribe(provinceCode => {
        this.commonService.getAllDistrictByProvinceCode(provinceCode).subscribe(res => {
          this.districtRecipientList = res;
          this.chackValueIn('recipientDistrictCode', res);
        });
        const province = this.provinceRecipientList.filter(s => s.provinceCode === provinceCode);
        this.taxRequestForm.get('recipientProvinceName').setValue(province.length > 0 ? province[0].provinceNameTh : null);
      });

    this.districtCodeControl.valueChanges.subscribe(districtCode => {
      this.commonService.getAllSubDistrictByDistrictCode(districtCode).subscribe(res => {
        this.subDistrictRecipientList = res;
        this.chackValueIn('recipientSubDistrictCode', res);
      });
      const district = this.districtRecipientList.filter(s => s.districtCode === districtCode);
      this.taxRequestForm.get('recipientDistrictName').setValue(district.length > 0 ? district[0].districtNameTh : null);
    });

    this.subDistrictCodeControl.valueChanges.subscribe(subDistrictCode => {
      const subDistrict = this.subDistrictRecipientList.filter(s => s.subDistrictCode === subDistrictCode);
      this.taxRequestForm.get('recipientSubDistrictName').setValue(subDistrict.length > 0 ? subDistrict[0].subDistrictNameTh : null);
    });
  }

  chackValueIn(formCtrlName, list: any[]) {
    const controlValue = this.controlContainer.control.get(formCtrlName).value;
    const index = list.findIndex(i => {
      return i.provinceCode == controlValue
        || i.districtCode == controlValue
        || i.subDistrictCode == controlValue;
    });
    if (index === -1) {
      this.controlContainer.control.get(formCtrlName).setValue(null);
    }

    if (this.controlContainer.control.get(formCtrlName).untouched) {
      this.controlContainer.control.get(formCtrlName).setValue(this.controlContainer.control.get(formCtrlName).value);
    }
  }

  get subDistrictCodeControl(): AbstractControl {
    return this.taxRequestForm.get('recipientSubDistrictCode');
  }

  get districtCodeControl(): AbstractControl {
    return this.taxRequestForm.get('recipientDistrictCode');
  }

  get provinceCodeControl(): AbstractControl {
    return this.taxRequestForm.get('recipientProvinceCode');
  }
}
