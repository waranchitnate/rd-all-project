import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TaxRequestTransferBranchModel} from '../../../../../../models/tax-request-transfer-branch.model';
import {FormGroup, AbstractControl} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap';
import {TaxTransferBranchFormModalComponent} from '../tax-transfer-branch-form-modal/tax-transfer-branch-form-modal.component';
import {Organization} from 'src/app/modules/admin/setting-organization/models/organization.model';
import {RdCommonApiService} from 'src/app/core/services/rd-common-api.service';
import {finalize} from 'rxjs/operators';
import {PpsMasterService} from 'src/app/modules/pps/services/pps-master.service';

@Component({
  selector: 'app-authority-department-form',
  templateUrl: './authority-department-form.component.html',
  styleUrls: ['../../pages/tax-request-form-parent/tax-request-form-parent.component.css']
})
export class AuthorityDepartmentFormComponent implements OnInit {

  @Input() setAuthorityDepartment = false;
  @Input() setTransferBranch = false;
  @Input() taxRequestForm: FormGroup;
  @Input() taxRequestTransferBranchModelList: Array<TaxRequestTransferBranchModel>;
  @Output() transferBranchChangeEvt = new EventEmitter<boolean>();
  // @Input() currentUserOfficeCode: string;
  orgList: Array<Organization> = [];

  constructor(private bsModalService: BsModalService, private masterService: PpsMasterService) {
  }

  ngOnInit() {
    if (this.setAuthorityDepartment) {
      this.masterService.getOrganizationList().subscribe(res => {
        this.orgList = res;
        console.log('this.orgList', this.orgList);
      });

      this.taxRequestForm.get('officeCode').valueChanges
        .pipe(finalize(() => console.log('fin')))
        .subscribe(officeCode => {
          const org = this.orgList.filter(s => s.orgCode === officeCode);
          this.taxRequestForm.get('officeName').setValue(org.length > 0 ? org[0].orgName : null);
          console.log('officeName: ', this.taxRequestForm.get('officeName'));
        });
    }
  }

  transferBranch() {
    let latestBranch: TaxRequestTransferBranchModel;
    if(this.taxRequestTransferBranchModelList.length === 0) {
      latestBranch = new TaxRequestTransferBranchModel();
    } else {
      latestBranch = this.taxRequestTransferBranchModelList[this.taxRequestTransferBranchModelList.length - 1];
    }
    const modalRef = this.bsModalService.show(TaxTransferBranchFormModalComponent, {initialState: {latestBranch: latestBranch}});
    (modalRef.content as TaxTransferBranchFormModalComponent).newBranchSubject.subscribe(newBranch => {
      if (newBranch != null) {
        latestBranch.prevBranch = latestBranch.currBranch;
        latestBranch.currBranch = newBranch.currBranch;
        this.taxRequestTransferBranchModelList.push(newBranch);
        this.transferBranchChangeEvt.emit(true);
      }
    });
  }

}
