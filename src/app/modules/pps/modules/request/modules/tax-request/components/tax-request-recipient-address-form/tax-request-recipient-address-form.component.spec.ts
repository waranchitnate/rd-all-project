import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestRecipientAddressFormComponent } from './tax-request-recipient-address-form.component';

describe('TaxRequestRecipientAddressFormComponent', () => {
  let component: TaxRequestRecipientAddressFormComponent;
  let fixture: ComponentFixture<TaxRequestRecipientAddressFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestRecipientAddressFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestRecipientAddressFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
