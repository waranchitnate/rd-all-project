import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestAddressDetailComponent } from './tax-request-address-detail.component';

describe('TaxRequestAddressDetailComponent', () => {
  let component: TaxRequestAddressDetailComponent;
  let fixture: ComponentFixture<TaxRequestAddressDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestAddressDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestAddressDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
