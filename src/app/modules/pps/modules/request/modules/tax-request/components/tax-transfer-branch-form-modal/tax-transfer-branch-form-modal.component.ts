import {Component, OnDestroy, OnInit} from '@angular/core';
import {TaxRequestTransferBranchModel} from '../../../../../../models/tax-request-transfer-branch.model';
import {BsModalRef} from 'ngx-bootstrap';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-tax-transfer-branch-form-modal',
  templateUrl: './tax-transfer-branch-form-modal.component.html',
  styleUrls: ['../../pages/tax-request-form-parent/tax-request-form-parent.component.css']
})
export class TaxTransferBranchFormModalComponent implements OnInit, OnDestroy {

  latestBranch: TaxRequestTransferBranchModel;
  newBranch: TaxRequestTransferBranchModel;
  newBranchSubject: Subject<TaxRequestTransferBranchModel> = new Subject();

  constructor(private bsModalRef: BsModalRef, private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.newBranch = new TaxRequestTransferBranchModel();
    this.newBranch.createdDate = new Date();
  }

  ngOnDestroy(): void {
    this.newBranchSubject.complete();
  }

  confirm() {
    if (this.newBranch.currBranch == null) {
        this.toastrService.error('กรุณาระบุชื่อหน่วยงานที่รับผิดชอบ', 'ข้อผิดพลาด');
        return;
    }
    this.newBranchSubject.next(this.newBranch);
    this.bsModalRef.hide();
  }

  dismiss() {
    this.newBranchSubject.next(null);
    this.bsModalRef.hide();
  }

}
