import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxTransferBranchFormModalComponent } from './tax-transfer-branch-form-modal.component';

describe('TaxTransferBranchFormModalComponent', () => {
  let component: TaxTransferBranchFormModalComponent;
  let fixture: ComponentFixture<TaxTransferBranchFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxTransferBranchFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxTransferBranchFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
