import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {tap} from 'rxjs/operators';
import {DocumentRequestModel} from '../../../../../../models/document-request.model';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';
import {TaxRequestService} from '../../services/tax-request.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-transfer-approval-detail',
  templateUrl: './transfer-approval-detail.component.html',
  styleUrls: ['./transfer-approval-detail.component.css']
})
export class TransferApprovalDetailComponent implements OnInit {

  modalRef: BsModalRef;
  documentRequestList: Array<DocumentRequestModel> = [];
  taxRequestModel: TaxRequestModel;

  constructor(  private modalService: BsModalService
              , private taxRequestService: TaxRequestService
              , private activatedRoute: ActivatedRoute
              , private toastrService: ToastrService
              , private router: Router) {}

  ngOnInit() {

    const taxReqId = this.activatedRoute.snapshot.params.id;
    if (taxReqId === null) {
      this.toastrService.error('ไม่พบหมายเลขยื่นขอภาษี');
      this.router.navigateByUrl('/pps/request/tax-request/tax-refund-request-list');
    }
    this.taxRequestService.getTaxRequest(taxReqId)
      .pipe(
        tap(res => {
          if (res.taxType.taxTypeCode) {
            this.setDocumentRequest(res.taxTypeCode);
          }
        })
      )
      .subscribe(res => {
        this.taxRequestModel = res;
      }, err => {
        this.toastrService.error('ไม่สามารถดึงข้อมูลได้', 'ข้อผิดพลาด');
      });

  }

  setDocumentRequest(taxTypeCode: string) {
    this.taxRequestService.getDocumentRequestByTaxType(taxTypeCode).subscribe(res => {
      this.documentRequestList = res;
    }, err => {
      this.toastrService.error('ไม่สามารถดึงข้อมูลเอกสารได้', 'ข้อผิดพลาด');
    });
  }


  openRejectModal(id: number, template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

}
