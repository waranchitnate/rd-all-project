import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-rpt-k10',
  templateUrl: './rpt-k10.component.html',
  styleUrls: ['./rpt-k10.component.css']
})
export class RptK10Component implements OnInit {

  formGroup: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.formInit();
  }

  private formInit() {
    this.formGroup = this.fb.group({
      startDate: this.fb.control(''),
      endDate: this.fb.control(''),
      reportType: this.fb.control('')
    });
  }

  get startDateControl(): AbstractControl {
    return this.formGroup.get('startDate');
  }

  get endDateControl(): AbstractControl {
    return this.formGroup.get('endDate');
  }

  dateChange() {
    this.formGroup.get('endDate').setValue(null);
  }

}
