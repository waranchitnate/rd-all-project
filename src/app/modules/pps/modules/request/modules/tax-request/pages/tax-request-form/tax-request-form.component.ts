import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TaxRequestService} from '../../services/tax-request.service';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';
import {ToastrService} from 'ngx-toastr';
import {TaxRequestTransferBranchModel} from '../../../../../../models/tax-request-transfer-branch.model';
import {TaxRequestFormParentComponent} from '../tax-request-form-parent/tax-request-form-parent.component';
import {concatMap, filter, map, take} from 'rxjs/operators';
import {TaxReqAttachmentModel} from '../../../../../../models/tax-req-attachment.model';
import {DocumentRequestModel} from '../../../../../../models/document-request.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { SsoUserModel } from 'src/app/core/models/sso-user.model';

@Component({
  selector: 'app-tax-request-form',
  templateUrl: './tax-request-form.component.html',
  styleUrls: ['../tax-request-form-parent/tax-request-form-parent.component.css']
})
export class TaxRequestFormComponent implements OnInit {

  documentRequestModels: Array<DocumentRequestModel>;
  taxRequestForm: FormGroup;
  currentUser: SsoUserModel;

  constructor(private activatedRoute: ActivatedRoute, private taxRequestService: TaxRequestService, private toastrService: ToastrService,
              private router: Router, private taxRequestFormParentComponent: TaxRequestFormParentComponent
              , public authService: AuthenticationService) {
  }

  ngOnInit() {
    this.authService.getSSOUserDetail().subscribe(result => {
      this.currentUser = result;
    });

    this.taxRequestFormParentComponent.taxRequestModelObs
      .pipe(
        filter(s => s != null),
        take(1)
      )
      .pipe(
        concatMap(taxRequest =>
          this.taxRequestService
            .getDocumentRequestByTaxType(taxRequest.taxType.taxTypeCode)
            .pipe(map(documentRequestModel => [taxRequest, documentRequestModel])))
      ).subscribe((val: Array<any>) => {
        console.log('documentRequestModels : ', val);
      this.documentRequestModels = val[1];
      this.taxRequestForm = this.initialTaxRequestForm(val[0]);
      console.log('this.taxRequestForm:', this.taxRequestForm);
    });
  }

  markFormGroupTouched(formGroup: FormGroup) {
    const values = Object.keys(formGroup.controls).map(e => formGroup.controls[e]);
    values.forEach((control: any) => {
      if (control.controls) { // control is a FormGroup
        this.markFormGroupTouched(control);
      } else { // control is a FormControl
        control.markAsTouched();
      }
    });
  }

  goToConfirmPage() {
    if (this.taxRequestForm.invalid) {
      this.markFormGroupTouched(this.taxRequestForm);
      this.toastrService.error('กรุณาระบุข้อมูลให้ถูกต้อง', 'ข้อผิดพลาด');
    } else {
      this.taxRequestFormParentComponent.taxRequestModelObs.next(this.taxRequestForm.value);
      this.router.navigate(['confirm'], {relativeTo: this.activatedRoute, skipLocationChange: true});
    }
  }

  initialTaxRequestForm(model: TaxRequestModel): FormGroup {
    return new FormGroup({
      taxRequestId: new FormControl(model.taxRequestId),
      taxAgentPin: new FormControl(model.taxAgentPin,  [Validators.required, Validators.minLength(13)]),
      taxAgentName: new FormControl(model.taxAgentName, Validators.required),
      companyName: new FormControl(model.companyName, Validators.required),
      receiptDocNo: new FormControl(model.receiptDocNo),
      officeCode: new FormControl(model.officeCode),
      officeName: new FormControl(model.officeName),
      taxType: new FormGroup({
        taxTypeCode: new FormControl(model.taxType.taxTypeCode, Validators.required),
        taxTypeName: new FormControl(model.taxType.taxTypeName, Validators.required),
      }),
      taxRequestTransferBranchDto: this.initialTaxRequestTransferBranchFormArray(model.taxRequestTransferBranchDto),
      accountPeriodStart: new FormControl(model.accountPeriodStart, Validators.required),
      accountPeriodEnd: new FormControl(model.accountPeriodEnd, Validators.required),
      requestAmount: new FormControl(model.requestAmount),
      reasonRequest: new FormGroup({
        reasonCode: new FormControl(model.reasonRequest.reasonCode, Validators.required),
        reasonName: new FormControl(model.reasonRequest.reasonName, Validators.required)
      }),
      detailOfReason: new FormControl(model.detailOfReason),
      isHeadOffice: new FormControl(model.headOffice),
      branchNo: new FormControl(model.branchNo),
      nameBuilding: new FormControl(model.nameBuilding),
      roomNo: new FormControl(model.roomNo),
      floorNo: new FormControl(model.floorNo),
      village: new FormControl(model.village),
      addressNo: new FormControl(model.addressNo),
      moo: new FormControl(model.moo),
      soi: new FormControl(model.soi),
      lane: new FormControl(model.soi),
      road: new FormControl(model.road),
      subDistrictCode: new FormControl(model.subDistrictCode, Validators.required),
      subDistrictName: new FormControl(model.subDistrictName),
      districtCode: new FormControl(model.districtCode, Validators.required),
      districtName: new FormControl(model.districtName, Validators.required),
      provinceCode: new FormControl(model.provinceCode, Validators.required),
      provinceName: new FormControl(model.provinceName, Validators.required),
      postcode: new FormControl(model.postcode),
      telephone: new FormControl(model.telephone, Validators.required),
      recipientName: new FormControl(model.recipientName),
      recipientNameBuilding: new FormControl(model.recipientNameBuilding),
      recipientRoomNo: new FormControl(model.recipientRoomNo),
      recipientFloorNo: new FormControl(model.recipientFloorNo),
      recipientVillage: new FormControl(model.recipientVillage),
      recipientAddressNo: new FormControl(model.recipientAddressNo),
      recipientMoo: new FormControl(model.recipientMoo),
      recipientSoi: new FormControl(model.recipientSoi),
      recipientLane: new FormControl(model.recipientLane),
      recipientRoad: new FormControl(model.recipientRoad),
      recipientSubDistrictCode: new FormControl(model.recipientSubDistrictCode),
      recipientSubDistrictName: new FormControl(model.recipientSubDistrictName),
      recipientDistrictCode: new FormControl(model.recipientDistrictCode),
      recipientDistrictName: new FormControl(model.recipientDistrictName),
      recipientProvinceCode: new FormControl(model.recipientProvinceCode),
      recipientProvinceName: new FormControl(model.recipientProvinceName),
      recipientPostcode: new FormControl(model.recipientPostcode),
      recipientTelephone: new FormControl(model.recipientTelephone),
      documentApprove: new FormControl(model.documentApprove),
      remarkDocument: new FormControl(model.remarkDocument),
      email: new FormControl(model.email),
      taxReqAttachmentList: this.initialTaxReqAttachment(model.taxReqAttachmentList)
    });
  }

  initialTaxReqAttachment(taxReqAttachmentList: Array<TaxReqAttachmentModel>): FormArray {
    console.log('initialTaxReqAttachment: ', taxReqAttachmentList);
    const frmArr = new FormArray([]);
    this.documentRequestModels.forEach(docReq => {
      const taxReqAttachment = taxReqAttachmentList.find(taxReqAttchment => taxReqAttchment.docRequestCode === docReq.docRequestCode);
      frmArr.push(new FormGroup({
          hashTaxReqAttachmentId: new FormControl(taxReqAttachment !== undefined ? taxReqAttachment.hashTaxReqAttachmentId : null),
          fileName: new FormControl(taxReqAttachment !== undefined ? taxReqAttachment.fileName : null),
          docRequestCode: new FormControl(docReq.docRequestCode),
          path: new FormControl(taxReqAttachment !== undefined ? taxReqAttachment.path : null),
          remark: new FormControl(taxReqAttachment !== undefined ? taxReqAttachment.remark : null),
          file: new FormControl(taxReqAttachment !== undefined ? taxReqAttachment.file : null),
          checked: new FormControl(taxReqAttachment && taxReqAttachment.fileName),
          dataFlag:  new FormControl(taxReqAttachment !== undefined ? 'EDIT' : 'ADD'),
          fileBase64: new FormControl(taxReqAttachment !== undefined ? taxReqAttachment.fileBase64 : null),
          fileDelete: new FormControl(null)
        })
      );
    });
    return frmArr;
  }

  initialTaxRequestTransferBranchFormArray(taxRequestTransferBranchDto: Array<TaxRequestTransferBranchModel>): FormArray {
    const frmArray = new FormArray([]);
    if (taxRequestTransferBranchDto == null ||
         (taxRequestTransferBranchDto != null && taxRequestTransferBranchDto.length === 0)) {
        taxRequestTransferBranchDto = new Array<TaxRequestTransferBranchModel>();
        taxRequestTransferBranchDto.push(new TaxRequestTransferBranchModel());
    }
    taxRequestTransferBranchDto.forEach(s => {
      frmArray
        .push(
          new FormGroup({
            taxTransferBranchId: new FormControl(s.taxTransferBranchId),
            prevBranch: new FormControl(s.prevBranch),
            currBranch: new FormControl(s.currBranch),
            remark: new FormControl(s.remark)
          })
        );
    });
    return frmArray;
  }

}
