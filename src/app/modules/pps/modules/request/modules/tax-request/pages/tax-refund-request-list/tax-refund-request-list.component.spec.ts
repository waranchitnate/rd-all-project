import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRefundRequestListComponent } from './tax-refund-request-list.component';

describe('TaxRefundRequestListComponent', () => {
  let component: TaxRefundRequestListComponent;
  let fixture: ComponentFixture<TaxRefundRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRefundRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRefundRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
