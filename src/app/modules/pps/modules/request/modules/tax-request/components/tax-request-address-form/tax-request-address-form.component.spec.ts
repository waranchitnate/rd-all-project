import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestAddressFormComponent } from './tax-request-address-form.component';

describe('TaxRequestAddressFormComponent', () => {
  let component: TaxRequestAddressFormComponent;
  let fixture: ComponentFixture<TaxRequestAddressFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestAddressFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestAddressFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
