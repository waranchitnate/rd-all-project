import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TaxRequestPageRequestModel} from '../../models/tax-request-page-request.model';
import {PageResponse} from '../../../../../../../../shared/models/page-response';
import {TaxRequestPageResponseModel} from '../../models/tax-request-page-response.model';
import {TaxRequestService} from '../../services/tax-request.service';
import {TransferApprovalPageRequestModel} from '../../models/transfer-approval-page-request.model';
import {TransferApprovalPageResponseMode} from '../../models/transfer-approval-page-response.mode.';

@Component({
  selector: 'app-transfer-approval',
  templateUrl: './transfer-approval.component.html',
  styleUrls: ['./transfer-approval.component.css']
})
export class TransferApprovalComponent implements OnInit {

  searchFormGroup: FormGroup;
  pageRequest: TransferApprovalPageRequestModel;
  pageResponse: PageResponse<TransferApprovalPageResponseMode> = new PageResponse<TransferApprovalPageResponseMode>();

  constructor(private fb: FormBuilder, private taxRequestService: TaxRequestService) {}

  ngOnInit() {
    this.pageResponse.content = [];
    this.createNewPageRequest();
  }

  createNewPageRequest() {
    this.pageRequest = new TransferApprovalPageRequestModel();
    this.pageRequest.sortFieldName = 'accountPeriodStart';
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.pagination();
  }

  paginationWhenPressEnter(evt: KeyboardEvent) {
    if (evt.key === 'Enter') {
      this.pagination();
    }
  }

  pagination() {
    // this.taxRequestService.taxRequestPagination(this.pageRequest).subscribe(pageResponse => {
    //   this.pageResponse = pageResponse;
    // });
  }

  private formInit() {
    this.searchFormGroup = this.fb.group({
      requestAmount: this.fb.control(''),
      isRefund: this.fb.control(''),
      currApproveType: this.fb.control('1'),
      refNo: this.fb.control('8273746389'),
      refNoNonRefund: this.fb.control(''),
    });
  }

}
