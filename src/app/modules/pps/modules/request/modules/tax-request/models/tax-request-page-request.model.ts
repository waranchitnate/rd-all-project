import {PageRequest} from '../../../../../../../shared/models/page-request';

export class TaxRequestPageRequestModel extends PageRequest {

  taxTypeCode: string
  createdDateFrom: Date;
  createdDateTo: Date;
  requestStatus: string;
  receiptDocNo: string;
  taxAgentPin: string;
  taxAgentName: string;
}
