import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestFormParentComponent } from './tax-request-form-parent.component';

describe('TaxRequestFormParentComponent', () => {
  let component: TaxRequestFormParentComponent;
  let fixture: ComponentFixture<TaxRequestFormParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestFormParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestFormParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
