import {Component, Input, OnInit} from '@angular/core';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';

@Component({
  selector: 'app-tax-request-address-detail',
  templateUrl: './tax-request-address-detail.component.html',
  styleUrls: ['./tax-request-address-detail.component.css']
})
export class TaxRequestAddressDetailComponent implements OnInit {

  @Input() taxRequestModel: TaxRequestModel;
  constructor() { }

  ngOnInit() {
  }

}
