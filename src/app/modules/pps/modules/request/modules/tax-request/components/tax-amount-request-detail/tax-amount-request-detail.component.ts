import {Component, Input, OnInit} from '@angular/core';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';

@Component({
  selector: 'app-tax-amount-request-detail',
  templateUrl: './tax-amount-request-detail.component.html',
  styleUrls: ['./tax-amount-request-detail.component.css']
})
export class TaxAmountRequestDetailComponent implements OnInit {

  @Input() taxRequestModel: TaxRequestModel;
  constructor() { }

  ngOnInit() {
  }

}
