export interface TransferApprovalPageResponseMode {
  docNoticeNumber: string;
  accountPeriodStart: Date;
  taxFilingNo: string;

  taxRequestId: number;
  receiptDocNo: string;
  createdDate: Date;
  taxAgentPin: string;
  taxAgentName: string;
  taxTypeName: string;
  refRequestNo: string;
  requestAmount: number;
  requestStatusName: string;
  requestStatus: string;
  aesTaxRequestId: string;
}
