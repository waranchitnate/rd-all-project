import {Component, Input, OnInit} from '@angular/core';
import {DocumentRequestModel} from '../../../../../../models/document-request.model';
import {AbstractControl, FormArray, FormGroup, Validators} from '@angular/forms';
import { TaxReqAttachmentModel } from 'src/app/modules/pps/models/tax-req-attachment.model';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { BsModalRef } from 'ngx-bootstrap';
import { ModalService } from 'src/app/shared/services/modal.service';

@Component({
  selector: 'app-tax-request-attachment-form',
  templateUrl: './tax-request-attachment-form.component.html',
  styleUrls: ['./tax-request-attachment-form.component.css']
})
export class TaxRequestAttachmentFormComponent implements OnInit {

  @Input() documentRequestModels: Array<DocumentRequestModel>;
  @Input() taxRequestForm: FormGroup;
  removeList: Array<TaxReqAttachmentModel>;
  constructor(private modalService: ModalService, public bsModalRef: BsModalRef) { }

  ngOnInit() {

  }

  setFileName(evt, index) {
    this.fileControl(index).setValue(evt.target.files[0]);
    this.fileNameControl(index).setValue(evt.target.files[0].name);
  }

  fileNameControl(index: number): AbstractControl {
    const taxReqAttachmentList: FormArray = this.taxRequestForm.get('taxReqAttachmentList') as FormArray;
    return taxReqAttachmentList.at(index).get('fileName');
  }

  fileControl(index: number): AbstractControl {
    const taxReqAttachmentList: FormArray = this.taxRequestForm.get('taxReqAttachmentList') as FormArray;
    return taxReqAttachmentList.at(index).get('file');
  }

  checkedControl(index: number): AbstractControl {
    const taxReqAttachmentList: FormArray = this.taxRequestForm.get('taxReqAttachmentList') as FormArray;
    return taxReqAttachmentList.at(index).get('checked');
  }

  checkDocument(event: any, index: number) {
    console.log(event.target.checked + index);
    console.log('checkDocument', index);
    const taxReqAttachmentList: FormArray = this.taxRequestForm.get('taxReqAttachmentList') as FormArray;

    if ( event.target.checked) {
      taxReqAttachmentList.at(index).get('fileName').setValidators(Validators.required);
    } else {
      taxReqAttachmentList.at(index).get('fileName').setValidators(null);
    }

    taxReqAttachmentList.at(index).get('fileName').updateValueAndValidity();
  }

  removeFile(i){
    const taxRequestAttachmentForm = this.taxRequestForm.get('taxReqAttachmentList') as FormArray;

    if (taxRequestAttachmentForm.at(i).get('taxReqAttachmentId').value > 0) {
      taxRequestAttachmentForm.at(i).get('dataFlag').setValue('DELETE');
      taxRequestAttachmentForm.at(i).get('fileDelete').setValue(taxRequestAttachmentForm.at(i).get('fileName').value);
    }

    taxRequestAttachmentForm.at(i).get('checked').setValue(false);
    taxRequestAttachmentForm.at(i).get('file').setValue(null);
    taxRequestAttachmentForm.at(i).get('fileName').setValue(null);
    taxRequestAttachmentForm.at(i).get('fileBase64').setValue(null);

    taxRequestAttachmentForm.at(i).get('file').setValidators(null);
    taxRequestAttachmentForm.at(i).get('fileName').setValidators(null);

    taxRequestAttachmentForm.at(i).get('file').updateValueAndValidity();
    taxRequestAttachmentForm.at(i).get('fileName').updateValueAndValidity();
  }

  confirmRemoveFile(row, i){
    const taxRequestAttachmentForm = this.taxRequestForm.get('taxReqAttachmentList') as FormArray;
    const removeFileConfirmModalRef = this.modalService.openConfirmModal('ยืนยันลบเอกสารแนบ', 'คุณต้องการลบเอกสาร ' + row.docRequestName + '(' + taxRequestAttachmentForm.at(i).get('fileName').value + ')');
    const taxRequestAttachConfirmModal = (removeFileConfirmModalRef.content as AnswerModalComponent);

    taxRequestAttachConfirmModal.answerEvent.subscribe(cfAnwser => {
      if (cfAnwser) {
        this.removeFile(i);
        this.bsModalRef.hide();
      } else {
        this.bsModalRef.hide();
      }
    });
  }

}
