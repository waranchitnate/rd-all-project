import { TestBed } from '@angular/core/testing';

import { TaxRequestService } from './tax-request.service';

describe('TaxRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaxRequestService = TestBed.get(TaxRequestService);
    expect(service).toBeTruthy();
  });
});
