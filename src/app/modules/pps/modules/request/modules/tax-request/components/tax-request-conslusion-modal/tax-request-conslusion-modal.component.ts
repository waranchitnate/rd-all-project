import { Component, OnInit } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';

@Component({
  selector: 'app-tax-request-conslusion-modal',
  templateUrl: './tax-request-conslusion-modal.component.html',
  styleUrls: ['../../pages/tax-request-form-parent/tax-request-form-parent.component.css']
})
export class TaxRequestConslusionModalComponent implements OnInit {

  taxRequestModel: TaxRequestModel;
  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

}
