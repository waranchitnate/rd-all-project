import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestAttachmentListTableComponent } from './tax-request-attachment-list-table.component';

describe('TaxRequestAttachmentListTableComponent', () => {
  let component: TaxRequestAttachmentListTableComponent;
  let fixture: ComponentFixture<TaxRequestAttachmentListTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestAttachmentListTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestAttachmentListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
