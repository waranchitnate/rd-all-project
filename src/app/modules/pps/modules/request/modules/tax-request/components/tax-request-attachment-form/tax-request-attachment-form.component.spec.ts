import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestAttachmentFormComponent } from './tax-request-attachment-form.component';

describe('TaxRequestAttachmentFormComponent', () => {
  let component: TaxRequestAttachmentFormComponent;
  let fixture: ComponentFixture<TaxRequestAttachmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestAttachmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestAttachmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
