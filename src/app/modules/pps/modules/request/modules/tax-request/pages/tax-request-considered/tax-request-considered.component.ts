import {Component, OnInit} from '@angular/core';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';
import {TaxRequestService} from '../../services/tax-request.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../../../../../../environments/environment';

@Component({
  selector: 'app-tax-request-considered',
  templateUrl: './tax-request-considered.component.html',
  styleUrls: ['../tax-request-form-parent/tax-request-form-parent.component.css']
})
export class TaxRequestConsideredComponent implements OnInit {

  taxRequestModel: TaxRequestModel;
  approveResult = false;
  taxReqId: number;
  taxReqAttachmentApi: string;
  public transferBranchChange = false;

  constructor(private taxRequestService: TaxRequestService, private activatedRoute: ActivatedRoute, private toastrService: ToastrService,
              private router: Router) {
  }

  ngOnInit() {
    this.taxReqAttachmentApi = environment.ppsApiUrl + 'file/';
    const taxReqId = this.activatedRoute.snapshot.params.id;
    if (taxReqId === null) {
      this.toastrService.error('ไม่พบหมายเลขยื่นขอภาษี');
      this.router.navigateByUrl('/pps/request/tax-request/tax-refund-request-list');
    }
    this.taxReqId = (+taxReqId);
    this.taxRequestService.getTaxRequestById(+taxReqId)
      .subscribe(res => {
        this.taxRequestModel = res.data;
      }, err => {
        this.toastrService.error('ไม่สามารถดึงข้อมูลได้', 'ข้อผิดพลาด');
      });
  }

  save() {
    if (this.approveResult) {
      if (this.transferBranchChange) {
        this.taxRequestService.addTransferBranch(this.taxRequestModel).subscribe(() => {
          this.approveDocument();
        }, error => {
          this.toastrService.error('ไม่สามารถโอนย้ายหน่วยงานได้เนื่องจาก ' + error.error.message, 'ข้อผิดพลาด');
        });
      } else {
        this.approveDocument();
      }
    } else {
      // TO DO CREATE DISAPPROVE DOC FUNCTIONAL
    }
  }

  approveDocument() {
    this.taxRequestService.approveDocument(this.taxReqId).subscribe(() => {
      this.toastrService.success('อนุมัติเอกสารสำเร็จ');
      this.router.navigate(['../../tax-refund-request-list'], {relativeTo: this.activatedRoute});
    }, err => {
      this.toastrService.error('ไม่สามารถอนุมัติเอกสารได้เนื่องจาก' + err.error.message, 'ข้อผิดพลาด');
    });
  }

}
