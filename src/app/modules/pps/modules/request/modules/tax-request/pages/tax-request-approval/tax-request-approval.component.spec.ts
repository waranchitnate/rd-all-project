import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestApprovalComponent } from './tax-request-approval.component';

describe('TaxRequestApprovalComponent', () => {
  let component: TaxRequestApprovalComponent;
  let fixture: ComponentFixture<TaxRequestApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
