import {Component, Input, OnInit} from '@angular/core';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';

@Component({
  selector: 'app-tax-request-recipient-address-detail',
  templateUrl: './tax-request-recipient-address-detail.component.html',
  styleUrls: ['./tax-request-recipient-address-detail.component.css']
})
export class TaxRequestRecipientAddressDetailComponent implements OnInit {

  @Input() taxRequestModel: TaxRequestModel;
  constructor() { }

  ngOnInit() {
  }

}
