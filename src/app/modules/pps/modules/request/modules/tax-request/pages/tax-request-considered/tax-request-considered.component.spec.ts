import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestConsideredComponent } from './tax-request-considered.component';

describe('TaxRequestConsideredComponent', () => {
  let component: TaxRequestConsideredComponent;
  let fixture: ComponentFixture<TaxRequestConsideredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestConsideredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestConsideredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
