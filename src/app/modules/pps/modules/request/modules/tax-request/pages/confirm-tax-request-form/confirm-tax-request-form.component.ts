import {Component, OnInit} from '@angular/core';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';
import {Router} from '@angular/router';
import {TaxRequestService} from '../../services/tax-request.service';
import {TaxRequestFormParentComponent} from '../tax-request-form-parent/tax-request-form-parent.component';
import {ToastrService} from 'ngx-toastr';
import {BsModalService} from 'ngx-bootstrap';
import {TaxRequestConslusionModalComponent} from '../../components/tax-request-conslusion-modal/tax-request-conslusion-modal.component';

@Component({
  selector: 'app-confirm-tax-request-form',
  templateUrl: './confirm-tax-request-form.component.html',
  styleUrls: ['../tax-request-form-parent/tax-request-form-parent.component.css']
})
export class ConfirmTaxRequestFormComponent implements OnInit {

  currBranch: string;
  taxRequestModel: TaxRequestModel;
  officeName: string;

  constructor(private router: Router, private taxRequestService: TaxRequestService, private taxRequestFormParentComponent: TaxRequestFormParentComponent,
              private toastrService: ToastrService, private bsModalService: BsModalService) {
  }

  ngOnInit() {
    this.taxRequestModel = this.taxRequestFormParentComponent.taxRequestModelObs.value;
    this.currBranch = this.taxRequestModel.taxRequestTransferBranchDto[this.taxRequestModel.taxRequestTransferBranchDto.length - 1].prevBranch;
    this.officeName = this.taxRequestModel.officeName;
  }

  save() {
    const attachments = this.taxRequestModel.taxReqAttachmentList;
    const formData = new FormData();
    for (const item of attachments) {
      debugger;
      if (item.file != null && item.fileName != null
          && (item.fileBase64 == null || item.fileBase64 == undefined)) {
        formData.append('file', item.file);
      }
    }
    // let attachList = this.taxRequestModel.taxReqAttachmentList;
    // console.log('this.taxRequestModel.taxReqAttachmentRemovedList: ', this.taxRequestModel.taxReqAttachmentRemovedList);
    // if ((this.taxRequestModel.taxReqAttachmentRemovedList != null)){
    //   attachList = this.taxRequestModel.taxReqAttachmentRemovedList.concat(attachList);
    // }

    // this.taxRequestModel.taxReqAttachmentList = attachList;

    const taxRequestDto = JSON.stringify(this.taxRequestModel);
    formData.append('taxRequestDto', new Blob([taxRequestDto], {type: 'application/json'}));
    console.log('formData: ', formData);
    this.taxRequestService.save(formData).subscribe((taxRequest: any) => {
      if (this.taxRequestModel.taxRequestId === null) {
        this.openTaxRequestConclusionModal(taxRequest.data);
        // const attachments = this.taxRequestModel.taxReqAttachmentList;
        // const formData = new FormData();
        // for (const item of attachments) {
        //   debugger;
        //   if (item.file != null && item.fileName != null) {
        //     formData.append('file', item.file);
        //   }
        // }
        // formData.append('taxRequestId', taxRequest.data.taxRequestId);
        // this.taxRequestService.uploadTaxRequestAttachments(formData).subscribe(res => {
        //   console.log('taxRequest.data: ' , taxRequest.data);
        //   this.openTaxRequestConclusionModal(taxRequest.data);
        // }, uploadFileError => {
        //   this.toastrService.error('ไม่สามารถบันทึกข้อมูลได้เนื่องจาก ' + uploadFileError.error.messsage, 'ข้อผิดพลาด');
        // });
      }
      this.toastrService.success('บันทึกสำเร็จ');
      this.router.navigateByUrl('/pps/request/tax-request/tax-refund-request-list');
    }, err => {
      this.toastrService.error('ไม่สามารถบันทึกข้อมูลได้เนื่องจาก ' + err.error.messsage, 'ข้อผิดพลาด');
    });
  }

  openTaxRequestConclusionModal(taxRequest: TaxRequestModel) {
    this.bsModalService.show(TaxRequestConslusionModalComponent, {initialState: {taxRequestModel: taxRequest}});
  }
}
