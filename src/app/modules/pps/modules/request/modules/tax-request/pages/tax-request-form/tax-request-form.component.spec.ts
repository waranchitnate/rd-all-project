import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestFormComponent } from './tax-request-form.component';

describe('TaxRequestFormComponent', () => {
  let component: TaxRequestFormComponent;
  let fixture: ComponentFixture<TaxRequestFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
