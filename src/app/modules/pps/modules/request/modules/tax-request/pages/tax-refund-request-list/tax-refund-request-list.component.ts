import {Component, OnInit, ViewChild} from '@angular/core';
import {TaxRequestService} from '../../services/tax-request.service';
import {TaxRequestPageRequestModel} from '../../models/tax-request-page-request.model';
import {PageResponse} from '../../../../../../../../shared/models/page-response';
import {TaxRequestPageResponseModel} from '../../models/tax-request-page-response.model';
import {FloatButtonComponent} from '../../../../../../../../shared/components/float-button/float-button.component';
import {Observable} from 'rxjs';
import {NameValuePairModel} from '../../../../../../../../shared/models/NameValuePair.model';
import {MTaxTypeModel} from '../../../../../../models/m-tax-type.model';

@Component({
  selector: 'app-tax-refund-request-list',
  templateUrl: './tax-refund-request-list.component.html',
  styleUrls: ['./tax-refund-request-list.component.css']
})
export class TaxRefundRequestListComponent implements OnInit {

  @ViewChild(FloatButtonComponent) floatComp;
  pageRequest: TaxRequestPageRequestModel;
  pageResponse: PageResponse<TaxRequestPageResponseModel> = new PageResponse<TaxRequestPageResponseModel>();
  taxRequestStatusObs: Observable<Array<NameValuePairModel>>;
  mTaxTypeStatusObs: Observable<Array<MTaxTypeModel>>;
  constructor(private taxRequestService: TaxRequestService) { }

  ngOnInit() {
    this.taxRequestStatusObs = this.taxRequestService.getAllStatus();
    this.mTaxTypeStatusObs = this.taxRequestService.getAllMTaxType();
    this.pageResponse.content = [];
    this.createNewPageRequest();
    this.floatComp.urlAdd = '/pps/request/tax-request/create-request-k10';
    this.floatComp.tooltipMsg = 'เพิ่มประเภทภาษี';
    this.pagination();
  }

  paginationWhenPressEnter(evt: KeyboardEvent) {
    if (evt.key === 'Enter') {
      this.pagination();
    }
  }

  pagination() {
    this.taxRequestService.taxRequestPagination(this.pageRequest).subscribe(pageResponse => {
      this.pageResponse = pageResponse;
    });
  }

  sort(fieldName) {
    if (this.pageRequest.sortFieldName === fieldName) {
      this.pageRequest.sortDirection = (this.pageRequest.sortDirection === 'ASC' ? 'DESC' : 'ASC');
    } else {
      this.pageRequest.sortFieldName = fieldName;
      this.pageRequest.sortDirection = 'ASC';
    }
    this.pagination();
  }

  createNewPageRequest() {
    this.pageRequest = new TaxRequestPageRequestModel();
    this.pageRequest.sortFieldName = 'createdDate';
  }

}
