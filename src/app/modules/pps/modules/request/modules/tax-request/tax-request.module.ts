import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TaxRequestRoutingModule} from './tax-request-routing.module';
import {RptK10Component} from './pages/rpt-k10/rpt-k10.component';
import {TransferApprovalComponent} from './pages/transfer-approval/transfer-approval.component';
import {AnalysisResultsComponent} from './pages/analysis-results/analysis-results.component';
import {SharedModule} from '../../../../../../shared/shared.module';
import {TaxRefundRequestListComponent} from './pages/tax-refund-request-list/tax-refund-request-list.component';
import {TaxRequestFormComponent} from './pages/tax-request-form/tax-request-form.component';
import {TaxTransferBranchFormModalComponent} from './components/tax-transfer-branch-form-modal/tax-transfer-branch-form-modal.component';
import {TaxRequestConslusionModalComponent} from './components/tax-request-conslusion-modal/tax-request-conslusion-modal.component';
import {TaxRequestConfirmComponent} from './pages/tax-request-confirm/tax-request-confirm.component';
import {TaxRequestApprovalComponent} from './pages/tax-request-approval/tax-request-approval.component';
import {TaxAgentFormComponent} from './components/tax-agent-form/tax-agent-form.component';
import {AuthorityDepartmentFormComponent} from './components/authority-department-form/authority-department-form.component';
import {TaxAmountRequestFormComponent} from './components/tax-amount-request-form/tax-amount-request-form.component';
import { TaxRequestAddressFormComponent } from './components/tax-request-address-form/tax-request-address-form.component';
import { TaxRequestAttachmentFormComponent } from './components/tax-request-attachment-form/tax-request-attachment-form.component';
import { TaxRequestConsideredComponent } from './pages/tax-request-considered/tax-request-considered.component';
import { TaxRequestAgentDetailComponent } from './components/tax-request-agent-detail/tax-request-agent-detail.component';
import { TaxRequestAddressDetailComponent } from './components/tax-request-address-detail/tax-request-address-detail.component';
import { TaxRequestRecipientAddressDetailComponent } from './components/tax-request-recipient-address-detail/tax-request-recipient-address-detail.component';
import { TaxAmountRequestDetailComponent } from './components/tax-amount-request-detail/tax-amount-request-detail.component';
import { TransferApprovalDetailComponent } from './pages/transfer-approval-detail/transfer-approval-detail.component';
import { TaxRequestRecipientAddressFormComponent } from './components/tax-request-recipient-address-form/tax-request-recipient-address-form.component';
import { ConfirmTaxRequestFormComponent } from './pages/confirm-tax-request-form/confirm-tax-request-form.component';
import { TaxRequestFormParentComponent } from './pages/tax-request-form-parent/tax-request-form-parent.component';
import { TaxRequestAttachmentListTableComponent } from './components/tax-request-attachment-list-table/tax-request-attachment-list-table.component';
import { TaxRequestDetailComponent } from './pages/tax-request-detail/tax-request-detail.component';

@NgModule({
  declarations: [
    RptK10Component
    , TransferApprovalComponent
    , TransferApprovalDetailComponent
    , AnalysisResultsComponent
    , TaxRefundRequestListComponent
    , TaxRequestFormComponent
    , TaxTransferBranchFormModalComponent
    , TaxRequestConslusionModalComponent
    , TaxRequestConfirmComponent
    , TaxRequestApprovalComponent
    , TaxAgentFormComponent
    , AuthorityDepartmentFormComponent
    , TaxAmountRequestFormComponent
    , TaxRequestAddressFormComponent
    , TaxRequestAttachmentFormComponent
    , TaxRequestConsideredComponent
    , TaxRequestAgentDetailComponent
    , TaxRequestAddressDetailComponent
    , TaxRequestRecipientAddressDetailComponent
    , TaxAmountRequestDetailComponent
    , TransferApprovalDetailComponent
    , TaxRequestRecipientAddressFormComponent, ConfirmTaxRequestFormComponent, TaxRequestFormParentComponent, TaxRequestAttachmentListTableComponent, TaxRequestDetailComponent
  ],
  imports: [
    CommonModule,
    TaxRequestRoutingModule,
    SharedModule
  ],
  entryComponents: [
    TaxRequestConslusionModalComponent,
    TaxTransferBranchFormModalComponent
  ]
})
export class TaxRequestModule {
}
