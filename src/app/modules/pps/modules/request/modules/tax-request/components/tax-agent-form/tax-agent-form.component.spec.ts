import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAgentFormComponent } from './tax-agent-form.component';

describe('TaxAgentFormComponent', () => {
  let component: TaxAgentFormComponent;
  let fixture: ComponentFixture<TaxAgentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxAgentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAgentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
