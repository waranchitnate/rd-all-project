import {PageRequest} from '../../../../../../../shared/models/page-request';

export class TransferApprovalPageRequestModel extends PageRequest {

  docNoticeNumber: string;
  taxAgentPin: string;
  taxAgentName: string;
  taxTypeCode: string;
  requestStatus: string;
  accountPeriodStart: Date;
  accountPeriodEnd: Date;
}
