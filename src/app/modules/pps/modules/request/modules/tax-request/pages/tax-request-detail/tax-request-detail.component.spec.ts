import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestDetailComponent } from './tax-request-detail.component';

describe('TaxRequestDetailComponent', () => {
  let component: TaxRequestDetailComponent;
  let fixture: ComponentFixture<TaxRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
