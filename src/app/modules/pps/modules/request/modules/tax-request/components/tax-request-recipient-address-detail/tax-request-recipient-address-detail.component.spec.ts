import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestRecipientAddressDetailComponent } from './tax-request-recipient-address-detail.component';

describe('TaxRequestRecipientAddressDetailComponent', () => {
  let component: TaxRequestRecipientAddressDetailComponent;
  let fixture: ComponentFixture<TaxRequestRecipientAddressDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestRecipientAddressDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestRecipientAddressDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
