import {Component, Input, OnInit} from '@angular/core';
import {DocumentRequestModel} from '../../../../../../models/document-request.model';
import {TaxRequestService} from '../../services/tax-request.service';
import {TaxReqAttachmentModel} from '../../../../../../models/tax-req-attachment.model';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-tax-request-attachment-list-table',
  templateUrl: './tax-request-attachment-list-table.component.html',
  styleUrls: ['./tax-request-attachment-list-table.component.css']
})
export class TaxRequestAttachmentListTableComponent implements OnInit {

  @Input() taxReqAttachmentList: Array<TaxReqAttachmentModel>;
  documentRequestModels: Array<DocumentRequestModel>;
  constructor(private taxRequestService: TaxRequestService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.taxRequestService.getDocumentRequestByTaxType('test').subscribe(res => {
      res.forEach(docReq => {
        const taxReqAttachment = this.taxReqAttachmentList.find(s => s.docRequestCode === docReq.docRequestCode && s.checked);
        if (taxReqAttachment !== undefined) {
          docReq.file = taxReqAttachment.file;
          docReq.fileName = taxReqAttachment.fileName;
          const blob = docReq.file != null ? new Blob([docReq.file.slice(0, docReq.file.size)], { type: 'application/octet-stream' }) : this.dataURItoBlob(taxReqAttachment.fileBase64, 'application/octet-stream');
          docReq.safeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
          docReq.fileBase64 = taxReqAttachment.fileBase64;
        }
      });
      this.documentRequestModels = res;
    });
  }

  dataURItoBlob(dataURI, contentType) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: contentType });
    return blob;
 }

}
