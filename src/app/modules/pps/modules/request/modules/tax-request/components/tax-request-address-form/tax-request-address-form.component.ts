import {Component, Input, OnInit, ChangeDetectorRef} from '@angular/core';
import {AbstractControl, FormGroup, ControlContainer} from '@angular/forms';
import {RdCommonApiService} from '../../../../../../../../core/services/rd-common-api.service';
import {ProvinceModel} from '../../../../../../../../core/models/province.model';
import {DistrictModel} from '../../../../../../../../core/models/district.model';
import {SubDistrictModel} from '../../../../../../../../core/models/sub-district.model';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-tax-request-address-form',
  templateUrl: './tax-request-address-form.component.html',
  styleUrls: ['./tax-request-address-form.component.css']
})
export class TaxRequestAddressFormComponent implements OnInit {

  @Input() taxRequestForm: FormGroup;
  provinceList: Array<ProvinceModel> = [];
  districtList: Array<DistrictModel> = [];
  subDistrictList: Array<SubDistrictModel> = [];

  constructor(public controlContainer: ControlContainer, private commonService: RdCommonApiService
    , public cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.commonService.getAllProvince().subscribe(res => {
      this.provinceList = res;
      this.chackValueIn('provinceCode', res);
    });

    this.provinceCodeControl.valueChanges
      .pipe(finalize(() => console.log('fin')))
      .subscribe(provinceCode => {
        this.commonService.getAllDistrictByProvinceCode(provinceCode).subscribe(res => {
          this.districtList = res;
          this.chackValueIn('districtCode', res);
        });
        const province = this.provinceList.filter(s => s.provinceCode === provinceCode);
        this.taxRequestForm.get('provinceName').setValue(province.length > 0 ? province[0].provinceNameTh : null);
      });

    this.districtCodeControl.valueChanges.subscribe(districtCode => {
      this.commonService.getAllSubDistrictByDistrictCode(districtCode).subscribe(res => {
        this.subDistrictList = res;
        this.chackValueIn('subDistrictCode', res);
      });
      const district = this.districtList.filter(s => s.districtCode === districtCode);
      this.taxRequestForm.get('districtName').setValue(district.length > 0 ? district[0].districtNameTh : null);
    });

    this.subDistrictCodeControl.valueChanges.subscribe(subDistrictCode => {
      console.log('subDistrictCode: ', subDistrictCode);
      const subDistrict = this.subDistrictList.filter(s => s.subDistrictCode === subDistrictCode);
      this.taxRequestForm.get('subDistrictName').setValue(subDistrict.length > 0 ? subDistrict[0].subDistrictNameTh : null);
    });
  }

  get subDistrictCodeControl(): AbstractControl {
    return this.taxRequestForm.get('subDistrictCode');
  }

  get districtCodeControl(): AbstractControl {
    return this.taxRequestForm.get('districtCode');
  }

  get provinceCodeControl(): AbstractControl {
    return this.taxRequestForm.get('provinceCode');
  }

  chackValueIn(formCtrlName, list: any[]) {
    const controlValue = this.controlContainer.control.get(formCtrlName).value;
    const index = list.findIndex(i => {
      return i.provinceCode == controlValue
        || i.districtCode == controlValue
        || i.subDistrictCode == controlValue;
    });
    if (index === -1) {
      this.controlContainer.control.get(formCtrlName).setValue(null);
    }

    if (this.controlContainer.control.get(formCtrlName).untouched) {
      this.controlContainer.control.get(formCtrlName).setValue(this.controlContainer.control.get(formCtrlName).value);
    }
  }
}
