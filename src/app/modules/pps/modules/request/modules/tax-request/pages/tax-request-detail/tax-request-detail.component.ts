import { Component, OnInit } from '@angular/core';
import {DocumentRequestModel} from '../../../../../../models/document-request.model';
import {TaxRequestModel} from '../../../../../../models/tax-request.model';
import {TaxRequestService} from '../../services/tax-request.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import {environment} from '../../../../../../../../../environments/environment';

@Component({
  selector: 'app-tax-request-detail',
  templateUrl: './tax-request-detail.component.html',
  styleUrls: ['../tax-request-form-parent/tax-request-form-parent.component.css']
})
export class TaxRequestDetailComponent implements OnInit {

  documentRequestList: Array<DocumentRequestModel> = [];
  documentRequestModels: Array<DocumentRequestModel>;
  taxRequestModel: TaxRequestModel;
  fileApi: string;

  constructor(private taxRequestService: TaxRequestService, private activatedRoute: ActivatedRoute, private toastrService: ToastrService,
              private router: Router, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.fileApi = environment.ppsApiUrl + 'file/';
    const taxReqId = this.activatedRoute.snapshot.params.id;
    if (taxReqId === null) {
      this.toastrService.error('ไม่พบหมายเลขยื่นขอภาษี');
      this.router.navigateByUrl('/pps/request/tax-request/tax-refund-request-list');
    }
    this.taxRequestService.getTaxRequestById(+taxReqId)
      .subscribe((res: any) => {
        this.taxRequestModel = res.data;
      }, err => {
        this.toastrService.error('ไม่สามารถดึงข้อมูลได้', 'ข้อผิดพลาด');
      });
  }

  dataURItoBlob(dataURI, contentType) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: contentType });
    return blob;
 }

}
