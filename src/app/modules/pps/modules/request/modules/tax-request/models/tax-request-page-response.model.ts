export class TaxRequestPageResponseModel {
  taxRequestId: number;
  receiptDocNo: string;
  createdDate: Date;
  taxAgentPin: string;
  taxAgentName: string;
  taxTypeName: string;
  refRequestNo: string;
  requestAmount: number;
  requestStatusName: string;
  requestStatus: string;
}
