import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRequestConslusionModalComponent } from './tax-request-conslusion-modal.component';

describe('TaxRequestConslusionModalComponent', () => {
  let component: TaxRequestConslusionModalComponent;
  let fixture: ComponentFixture<TaxRequestConslusionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxRequestConslusionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRequestConslusionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
