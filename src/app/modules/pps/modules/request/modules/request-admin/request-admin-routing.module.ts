import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageAttachmentsComponent } from './pages/manage-attachments/manage-attachments/manage-attachments.component';
import { ManageAttachmentsFormComponent } from './pages/manage-attachments-form/manage-attachments-form/manage-attachments-form.component';
import { TaxTypeManagementComponent } from './pages/tax-type-management/tax-type-management/tax-type-management.component';
import { TaxTypeManagementFormComponent } from './pages/tax-type-management-form/tax-type-management-form/tax-type-management-form.component';

const routes: Routes = [
  { path: "manage-attachments", component: ManageAttachmentsComponent},
  { path: "manage-attachments/form", component: ManageAttachmentsFormComponent},
  { path: "tax-type-management", component: TaxTypeManagementComponent},
  { path: "tax-type-management/form", component: TaxTypeManagementFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestAdminRoutingModule { }
