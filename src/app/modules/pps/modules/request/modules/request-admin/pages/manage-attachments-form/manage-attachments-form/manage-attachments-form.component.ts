import { Component, OnInit } from '@angular/core';
import { DocumentRequest } from '../../../models/document-request.model';
import { ManageAttachmentsService } from '../../../services/manage-attachments.service';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-attachments-form',
  templateUrl: './manage-attachments-form.component.html',
  styleUrls: ['./manage-attachments-form.component.css']
})
export class ManageAttachmentsFormComponent implements OnInit {
  attachmentSubmit = false;
  documentRequest: Array<DocumentRequest>;
  formAttachment:  FormGroup;
  

  constructor(private manageAttachmentsService: ManageAttachmentsService,
    private router: Router,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute) { }

  initialFormAttachment(): FormGroup {
    return new FormGroup({
      docRequestCode: new FormControl(null),
      docRequestName: new FormControl('', Validators.required)
    });
  } 

  ngOnInit() {
    this.formAttachment = this.initialFormAttachment();
    this.getAttachmentById(this.actRoute.snapshot.queryParamMap.get('docRequestCode'));
  }

  getAttachmentById(docRequestCode){
    if (docRequestCode != null) {
      this.manageAttachmentsService.findById(docRequestCode).subscribe((result: any) => {
      // console.log(result)
        this.formAttachment.setValue({
          docRequestCode: result.docRequestCode,
          docRequestName: result.docRequestName
        });
      });
    }
  }

  get field() {
    return this.formAttachment.controls;
  }

  saveAttachment() {

    this.attachmentSubmit = true;
    if (this.formAttachment.invalid) {
      return;
    }
      /* this.manageAttachmentsService.saveAttachment(this.formAttachment.value).subscribe((result: any) => {
        this.showSuccess();
        this.router.navigate(['/pps/request/admin/manage-attachments']);
      },
      err => {this.showError();}); */
      this.showSuccess();
      this.router.navigate(['/pps/request/admin/manage-attachments']);
  }

  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล ไม่สามารถบันทึกข้อมูลซ้ำได้', 'ไม่สำเร็จ',{
      timeOut: 3000
    });
  }

  black(){
    this.router.navigate(['/pps/request/admin/manage-attachments']);
  }

  get docRequestNameControl(): AbstractControl{ return this.formAttachment.get('docRequestName'); }
}
