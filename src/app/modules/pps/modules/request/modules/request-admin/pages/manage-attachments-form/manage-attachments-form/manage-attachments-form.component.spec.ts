import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAttachmentsFormComponent } from './manage-attachments-form.component';

describe('ManageAttachmentsFormComponent', () => {
  let component: ManageAttachmentsFormComponent;
  let fixture: ComponentFixture<ManageAttachmentsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageAttachmentsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAttachmentsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
