import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxTypeManagementComponent } from './tax-type-management.component';

describe('TaxTypeManagementComponent', () => {
  let component: TaxTypeManagementComponent;
  let fixture: ComponentFixture<TaxTypeManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxTypeManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxTypeManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
