import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/shared/services/modal.service';
import { ToastrService } from 'ngx-toastr';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { DropdownTaxTypeCode, MTaxType } from '../../../models/m-tax-type.model';
import { TaxTypeManageService } from '../../../services/tax-type-manage.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';

@Component({
  selector: 'app-tax-type-management',
  templateUrl: './tax-type-management.component.html',
  styleUrls: ['./tax-type-management.component.css']
})
export class TaxTypeManagementComponent implements OnInit {
  @ViewChild(FloatButtonComponent) floatComp;
  searchCondition:  FormGroup;
  dropdownTaxTypeCode: Array<DropdownTaxTypeCode>;
  mTaxType: Array<MTaxType>;

  constructor(private taxTypeManageService: TaxTypeManageService,
    private fb: FormBuilder,
    private router: Router,
    private modalAlertService: ModalService,
    private toastr: ToastrService) { }

  initialSearchCondition(): FormGroup {
    return new FormGroup({
      taxTypeCode: new FormControl('')
    });
  } 

  ngOnInit() {
    this.floatComp.urlAdd = '/pps/request/admin/tax-type-management/form';
    this.floatComp.tooltipMsg = 'เพิ่มประเภทภาษี';
    this.searchCondition = this.initialSearchCondition();
    this.getAllTaxTypeCode();
    this.getData();
  }

  getData() {
    const condition = this.searchCondition.getRawValue();
  // console.log(condition)
  //mockup data.
    this.taxTypeManageService.findAllMaster().subscribe((res) => {
      this.mTaxType = res;
    });
  }
 clear() {
    this.searchCondition.reset();
    this.searchCondition.get('taxTypeCode').setValue('');
  }

  getAllTaxTypeCode(){
    this.taxTypeManageService.findAll().subscribe(res => {
      this.dropdownTaxTypeCode = res;
    });
  }

  viewTaxType(taxTypeCode) {
    this.router.navigate(['/pps/request/admin/tax-type-management/form'], { queryParams: { taxTypeCode ,status: 'view'} });
  }
  editTaxType(taxTypeCode){
    this.router.navigate(['/pps/request/admin/tax-type-management/form'], { queryParams: { taxTypeCode} });
  }

  deleteTaxType(taxTypeCode){
  const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ','คุณต้องการลบข้อมูล?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
       if (result) {
        this.showDeleteSuccess();
       /*  this.taxTypeManageService.deleteTaxType(taxTypeCode)
      .subscribe(
        res => {
          this.showDeleteSuccess();
        },
        error => this.showDeleteError(),
        () => {
          // this.modalService.dismissAll();
          this.ngOnInit();

        }
      ); */

      } else {
        console.log('false');
      } 
    });
  }

  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }
  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ',{
      timeOut: 3000
    });
  }
}
