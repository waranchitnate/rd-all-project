import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TaxTypeManageService } from '../../../services/tax-type-manage.service';
import { MTaxType } from '../../../models/m-tax-type.model';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-tax-type-management-form',
  templateUrl: './tax-type-management-form.component.html',
  styleUrls: ['./tax-type-management-form.component.css']
})
export class TaxTypeManagementFormComponent implements OnInit {

  taxTypeSubmit = false;
  mTaxType: Array<MTaxType>;
  formTaxType:  FormGroup;
  status = this.actRoute.snapshot.queryParamMap.get('status');
  constructor(private taxTypeManageService: TaxTypeManageService,
    private router: Router,
    private toastr: ToastrService,
    private actRoute: ActivatedRoute) { }


  initialFormTaxType(): FormGroup {
    return new FormGroup({
      taxTypeCode: new FormControl(null),
      taxTypeName: new FormControl(null, Validators.required),
      taxTypeCheckbox: new FormControl(0)
    });
  } 

  ngOnInit() {
    this.formTaxType = this.initialFormTaxType();
    this.getTaxTypeById(this.actRoute.snapshot.queryParamMap.get('taxTypeCode'));
    if(this.actRoute.snapshot.queryParamMap.get('status') =="view"){
      this.formTaxType.get('taxTypeName').disable();
      this.formTaxType.get('taxTypeCheckbox').disable();
    }
  }

  get field() {
    return this.formTaxType.controls;
  }

  getTaxTypeById(taxTypeCode){
    if (taxTypeCode != null) {
      this.taxTypeManageService.findById(taxTypeCode).subscribe((result: any) => {
      // console.log(result)
        this.formTaxType.setValue({
          taxTypeCode: result.taxTypeCode,
          taxTypeName: result.taxTypeName,
          taxTypeCheckbox:1
        });
      });
    }
  }
  saveTaxType() {

    this.taxTypeSubmit = true;
    if (this.formTaxType.invalid) {
      return;
    }
      /* this.taxTypeManageService.saveTaxType(this.formTaxType.value).subscribe((result: any) => {
        this.showSuccess();
        this.router.navigate(['/pps/request/admin/tax-type-management']);
      },
      err => {this.showError();}); */
      this.showSuccess();
      this.router.navigate(['/pps/request/admin/tax-type-management']);
  }

  showSuccess() {
    this.toastr.success('บันทึกข้อมูลสำเร็จ', 'สำเร็จ');
  }

  showError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างบันทึกข้อมูล ไม่สามารถบันทึกข้อมูลซ้ำได้', 'ไม่สำเร็จ',{
      timeOut: 3000
    });
  }

  black(){
    this.router.navigate(['/pps/request/admin/tax-type-management']);
  }

  checkAll(ele) {
    var checkboxes = document.getElementsByTagName('input');
    if (ele.target.checked) {
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = true;
            }
        }
    } else {
        for (var i = 0; i < checkboxes.length; i++) {
            console.log(i)
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = false;
            }
        }
    }
}

get taxTypeCodeControl(): AbstractControl{ return this.formTaxType.get('taxTypeCode'); }
get taxTypeCheckboxControl(): AbstractControl{ return this.formTaxType.get('taxTypeCheckbox'); }
}
