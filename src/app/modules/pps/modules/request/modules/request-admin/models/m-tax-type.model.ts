export interface MTaxType {
    taxTypeCode: string;
    taxTypeName: string;
    dropdownTaxTypeCode: DropdownTaxTypeCode;
    status: number;
  }

  export interface DropdownTaxTypeCode {
    taxTypeCode: string;
    taxTypeName: string;
  }