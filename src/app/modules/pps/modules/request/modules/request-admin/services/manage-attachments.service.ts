import { Injectable } from '@angular/core';
import { DropdownTaxTypeCode } from '../models/m-tax-type.model';
import { Observable, of } from 'rxjs';
import { DocumentRequest } from '../models/document-request.model';
import { ResponseT } from 'src/app/shared/models/response.model';

@Injectable({
  providedIn: 'root'
})
export class ManageAttachmentsService {

    masterData = [
      {docRequestCode: '01', docRequestName: 'ภาพถ่ายหนังสือรับรองของนายทะเบียนหุ้นส่วนของบริษัท'},
      {docRequestCode: '03', docRequestName: 'สำเนาใบเสร็จรับเงิน'}
    ];
  
  private master: Array<any> = this.masterData;
 
  constructor() { }
 
  public findAllMaster(): Observable<Array<DocumentRequest>> {
    return of(this.master);
  }

  public findById(docRequestCode: string): Observable<Array<DocumentRequest>> {
    return of( this.master.find(doc => {
      return doc.docRequestCode == docRequestCode;
    }));
  } 
}
