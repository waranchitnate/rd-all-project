import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxTypeManagementFormComponent } from './tax-type-management-form.component';

describe('TaxTypeManagementFormComponent', () => {
  let component: TaxTypeManagementFormComponent;
  let fixture: ComponentFixture<TaxTypeManagementFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxTypeManagementFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxTypeManagementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
