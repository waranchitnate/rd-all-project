import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DropdownTaxTypeCode, MTaxType } from '../models/m-tax-type.model';

@Injectable({
  providedIn: 'root'
})
export class TaxTypeManageService {

  mockupDropDown = [
    {taxTypeCode: '001', taxTypeName: 'ภาษีเงินได้บุคคลธรรมดา'},
    {taxTypeCode: '002', taxTypeName: 'ภาษีเงินได้นิติบุคคล'},
    {taxTypeCode: '003', taxTypeName: 'ภาษีเงินได้ปิโตเลียม'},
    {taxTypeCode: '004', taxTypeName: 'ภาษีเงินได้หัก ณ ที่จ่าย(ภาษีการค้า)'},
    {taxTypeCode: '005', taxTypeName: 'ภาษีเงินได้มูลค่าเพิ่ม'},
    {taxTypeCode: '006', taxTypeName: 'ภาษีธุรกิจเฉพาะ'},
    {taxTypeCode: '007', taxTypeName: 'อากรแสตมป์'},
    {taxTypeCode: '008', taxTypeName: 'ภาษีการรับมรดก'},
    {taxTypeCode: '011', taxTypeName: 'ค่าปรับภาษีอากร'},
    {taxTypeCode: '161', taxTypeName: 'อากรมหรสพ'}
  ];


  private master: Array<any> = this.mockupDropDown;
  constructor() { }

  public findAll(): Observable<Array<DropdownTaxTypeCode>> {
    return of(this.master);
  }

  public findAllMaster(): Observable<Array<MTaxType>> {
    return of(this.master);
  }

  public findById(taxTypeCode: string): Observable<Array<MTaxType>> {
    return of( this.master.find(tax => {
      return tax.taxTypeCode == taxTypeCode;
    }));
  } 
}
