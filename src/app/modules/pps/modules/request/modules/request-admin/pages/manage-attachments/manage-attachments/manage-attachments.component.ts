import { Component, OnInit, ViewChild } from '@angular/core';
import { DropdownTaxTypeCode } from '../../../models/m-tax-type.model';
import { ManageAttachmentsService } from '../../../services/manage-attachments.service';

import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder } from '@angular/forms';
import { DocumentRequest } from '../../../models/document-request.model';
import { Router } from '@angular/router';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { ToastrService } from 'ngx-toastr';
import { TaxTypeManageService } from '../../../services/tax-type-manage.service';

@Component({
  selector: 'app-manage-attachments',
  templateUrl: './manage-attachments.component.html',
  styleUrls: ['./manage-attachments.component.css']
})
export class ManageAttachmentsComponent implements OnInit {
  @ViewChild(FloatButtonComponent) floatComp;
  searchCondition:  FormGroup;
  dropdownTaxTypeCode : Array<DropdownTaxTypeCode>;
  documentRequest: Array<DocumentRequest>;
  
  constructor(private manageAttachmentsService: ManageAttachmentsService,
    private taxTypeManageService: TaxTypeManageService,
    private fb: FormBuilder,
    private router: Router,
    private modalAlertService: ModalService,
    private toastr: ToastrService) { }

  initialSearchCondition(): FormGroup {
      return new FormGroup({
        taxTypeCode: new FormControl(''),
        docRequestName: new FormControl('')
      });
    } 

  ngOnInit() {
    this.floatComp.urlAdd = '/pps/request/admin/manage-attachments/form';
    this.floatComp.tooltipMsg = 'เพิ่มเอกสารแนบ';
    this.searchCondition = this.initialSearchCondition();
    this.getAllTaxTypeCode();
    this.getData();
  }
  
 
  getData() {
    const condition = this.searchCondition.getRawValue();
  // console.log(condition)
  //mockup data.
    this.manageAttachmentsService.findAllMaster().subscribe((res) => {
      this.documentRequest = res;
    });
  }
  
  clear() {
    this.searchCondition.reset();
    this.searchCondition.get('taxTypeCode').setValue('');
  }

  getAllTaxTypeCode(){
    this.taxTypeManageService.findAll().subscribe(res => {
      this.dropdownTaxTypeCode = res;
    });
  }

  editDocReq(docRequestCode) {
    this.router.navigate(['/pps/request/admin/manage-attachments/form'], { queryParams: { docRequestCode } });
  }
  
  deleteDocReq(docRequestCode) {
    const modalRef = this.modalAlertService.openConfirmModal('ยืนยันการลบ','คุณต้องการลบข้อมูล?');
    (modalRef.content as AnswerModalComponent).answerEvent.subscribe(result => {
       if (result) {
        this.showDeleteSuccess();
       /*  this.manageAttachmentsService.deleteDocReq(docRequestCode)
      .subscribe(
        res => {
          this.showDeleteSuccess();
        },
        error => this.showDeleteError(),
        () => {
          // this.modalService.dismissAll();
          this.ngOnInit();

        }
      ); */

      } else {
        console.log('false');
      } 
    });
  }

  showDeleteSuccess() {
    this.toastr.success('ลบข้อมูลสำเร็จ', 'สำเร็จ');
  }
  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ',{
      timeOut: 3000
    });
  }
  get taxTypeCodeControl(): AbstractControl{ return this.searchCondition.get('taxTypeCode'); }
  get docRequestNameControl(): AbstractControl{ return this.searchCondition.get('docRequestName'); }
}
