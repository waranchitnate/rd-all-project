import { TestBed } from '@angular/core/testing';

import { TaxTypeManageService } from './tax-type-manage.service';

describe('TaxTypeManageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaxTypeManageService = TestBed.get(TaxTypeManageService);
    expect(service).toBeTruthy();
  });
});
