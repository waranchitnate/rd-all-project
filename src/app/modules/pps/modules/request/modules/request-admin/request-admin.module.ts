import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestAdminRoutingModule } from './request-admin-routing.module';
import { ManageAttachmentsComponent } from './pages/manage-attachments/manage-attachments/manage-attachments.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { TooltipModule, ModalModule } from 'ngx-bootstrap-th';
import { ManageAttachmentsFormComponent } from './pages/manage-attachments-form/manage-attachments-form/manage-attachments-form.component';
import { TaxTypeManagementComponent } from './pages/tax-type-management/tax-type-management/tax-type-management.component';
import { TaxTypeManagementFormComponent } from './pages/tax-type-management-form/tax-type-management-form/tax-type-management-form.component';



@NgModule({
  declarations: [ManageAttachmentsComponent, ManageAttachmentsFormComponent, TaxTypeManagementComponent, TaxTypeManagementFormComponent],
  imports: [
    CommonModule,
    RequestAdminRoutingModule,
    SharedModule,
    FormsModule,
    TooltipModule,
    ReactiveFormsModule,
    ModalModule
  ]
})
export class RequestAdminModule { }
