import { TestBed } from '@angular/core/testing';
import { ManageAttachmentsService } from './manage-attachments.service';



describe('ManageAttachmentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageAttachmentsService = TestBed.get(ManageAttachmentsService);
    expect(service).toBeTruthy();
  });
});
