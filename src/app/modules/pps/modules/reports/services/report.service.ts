import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  url = environment.ppsApiUrl + "report";

  constructor(private http: HttpClient, 
              private datepipe: DatePipe,
              private sanitizer: DomSanitizer) { }

  downloadFile(blob: Blob, filename: string){
    let a = document.createElement("a");
    a.href = URL.createObjectURL(blob);
    a.download = decodeURIComponent(filename);
    a.click();
  }
  
  genReport5(reportType: string, startDate: string, endDate: string): void {
    const url = this.url + '/report5?'
              +'reportType='+reportType
              +'&startDate='+this.datepipe.transform(startDate, 'dd/MM/yyyy')
              +'&endDate='+this.datepipe.transform(endDate, 'dd/MM/yyyy');
    this.http.get<Blob>(url, {
      observe: 'response',
      responseType: 'blob' as 'json'
    }).subscribe(data => {
      this.downloadFile(data.body, data.headers.get('filename'));
    });
  }

  genReport8(reportType: string, startDate: string, endDate: string): void {
    const url = this.url + '/report8?'
              +'reportType='+reportType
              +'&startDate='+this.datepipe.transform(startDate, 'dd/MM/yyyy')
              +'&endDate='+this.datepipe.transform(endDate, 'dd/MM/yyyy');
    this.http.get<Blob>(url, {
      observe: 'response',
      responseType: 'blob' as 'json'
    }).subscribe(data => {
      this.downloadFile(data.body, data.headers.get('filename'));
    });
  }
}
