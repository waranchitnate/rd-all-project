import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Report5Component } from './pages/reports5/report5.component';
import { Report8Component } from './pages/reports8/report8.component';
import { ReportTranferAmountComponent } from './pages/reports-tranfer-amount/reports-tranfer-amount.component';
import { ReportReconcileComponent } from './pages/reports-reconcile/report-reconcile.component';
import { ReportRepeatPaymentComponent } from './pages/reports-repeate-payment/report-repeate-payment.component';
import { ReportPaymentComponent } from './pages/reports-payment/report-payment.component';

const routes: Routes = [
  { path: "report5", component: Report5Component},
  { path: "report8", component: Report8Component},
  { path: "report-tranfer-amount", component: ReportTranferAmountComponent},
  { path: "report-reconcile", component: ReportReconcileComponent},
  { path: "report-payment", component: ReportPaymentComponent},
  { path: "report-repeate-payment", component: ReportRepeatPaymentComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
