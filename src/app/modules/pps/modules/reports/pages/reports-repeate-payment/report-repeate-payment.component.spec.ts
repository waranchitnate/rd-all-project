import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportRepeatPaymentComponent } from './report-repeate-payment.component';

describe('ReportRepeatPaymentComponent', () => {
  let component: ReportRepeatPaymentComponent;
  let fixture: ComponentFixture<ReportRepeatPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportRepeatPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportRepeatPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
