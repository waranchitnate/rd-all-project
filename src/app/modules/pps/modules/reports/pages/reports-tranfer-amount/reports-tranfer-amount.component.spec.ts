import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportTranferAmountComponent } from './reports-tranfer-amount.component';

describe('ReportTranferAmountComponent', () => {
  let component: ReportTranferAmountComponent;
  let fixture: ComponentFixture<ReportTranferAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportTranferAmountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportTranferAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
