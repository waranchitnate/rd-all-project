import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { FloatButtonComponent } from 'src/app/shared/components/float-button/float-button.component';
import { ModalService } from 'src/app/shared/services/modal.service';
import { AnswerModalComponent } from 'src/app/shared/modals/answer-modal/answer-modal.component';
import { ToastrService } from 'ngx-toastr';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ReportService } from "src/app/modules/pps/modules/reports/services/report.service";

@Component({
  selector: 'app-report5',
  templateUrl: './report5.component.html',
  styleUrls: ['./report5.component.css']
})
export class Report5Component implements OnInit {
  @ViewChild(FloatButtonComponent) floatComp;
  searchCondition:  FormGroup;
  minDate = new Date();
  maxDate = new Date();
  bsConfig: Partial<BsDatepickerConfig>;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private modalAlertService: ModalService,
    private toastr: ToastrService,
    private reportService: ReportService) {this.minDate = new Date();
      this.maxDate = new Date();
      this.minDate.setDate(this.minDate.getDate());
      this.maxDate.setDate(this.maxDate.getDate());
      this.bsConfig = Object.assign({}, { containerClass: 'theme-orange' }); }

  ngOnInit() {
    this.searchCondition = this.initialSearchCondition();
    let paymentDate = this.searchCondition.get('paymentDate');
    let paymentDateTo = this.searchCondition.get('paymentDateTo');
    this.searchCondition.get('reportType').setValue('PDF');


  }
  
  genReport() {
    const condition = this.searchCondition.getRawValue();
    this.reportService.genReport5(condition.reportType, condition.paymentDate, condition.paymentDateTo);
  }

  initialSearchCondition(): FormGroup {
    return new FormGroup({
      paymentDate: new FormControl(null, Validators.required),
      paymentDateTo: new FormControl(null, Validators.required),
      reportType: new FormControl(null, Validators.required)
    });
  }

  clear() {
    this.searchCondition.reset();
    this.searchCondition.get('paymentDate').setValue('');
    this.searchCondition.get('paymentDateTo').setValue('');
    this.searchCondition.get('reportType').setValue('PDF');

    this.minDate = new Date();
    this.maxDate = new Date();
    this.minDate.setDate(this.minDate.getDate());
    this.maxDate.setDate(this.maxDate.getDate());
    this.bsConfig = Object.assign({}, { containerClass: 'theme-orange' });
  }

  onChangeDate(): void {
    this.searchCondition.get('paymentDate').valueChanges.subscribe(val => {
      this.minDate = val;
      if(this.searchCondition.get('paymentDateTo').value == null || this.searchCondition.get('paymentDateTo').value == ''){
        this.searchCondition.get('paymentDateTo').setValue(val);
      }     
    });

    this.searchCondition.get('paymentDateTo').valueChanges.subscribe(val => {
      this.maxDate = val;
    });
  }

  get startDateControl(): AbstractControl {
    return this.searchCondition.get('paymentDate');
  }

  get endDateControl(): AbstractControl {
    return this.searchCondition.get('paymentDateTo');
  }
  minMaxDate(createdDateStart){
    if(createdDateStart != null && createdDateStart != "" ){
      this.minDate = createdDateStart;
      this.minDate.setDate(this.minDate.getDate());
    }
  }

  showDeleteError() {
    this.toastr.error('พบข้อผิดพลาดระหว่างลบข้อมูล', 'ไม่สำเร็จ',{
      timeOut: 3000
    });
  }
  get taxTypeCodeControl(): AbstractControl{ return this.searchCondition.get('taxTypeCode'); }
  get docRequestNameControl(): AbstractControl{ return this.searchCondition.get('docRequestName'); }
}
