import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportReconcileComponent } from './report-reconcile.component';

describe('ReportReconcileComponent', () => {
  let component: ReportReconcileComponent;
  let fixture: ComponentFixture<ReportReconcileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportReconcileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportReconcileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
