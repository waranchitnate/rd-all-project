import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsRoutingModule } from './reports-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { TooltipModule, ModalModule } from 'ngx-bootstrap-th';
import { Report5Component } from './pages/reports5/report5.component';
import { Report8Component } from './pages/reports8/report8.component';
import { ReportTranferAmountComponent } from './pages/reports-tranfer-amount/reports-tranfer-amount.component';
import { ReportReconcileComponent } from './pages/reports-reconcile/report-reconcile.component';
import { ReportRepeatPaymentComponent } from './pages/reports-repeate-payment/report-repeate-payment.component';
import { ReportPaymentComponent } from './pages/reports-payment/report-payment.component';

@NgModule({
  declarations: [Report5Component,Report8Component,ReportTranferAmountComponent,ReportReconcileComponent,ReportRepeatPaymentComponent,ReportPaymentComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule,
    FormsModule,
    TooltipModule,
    ReactiveFormsModule,
    ModalModule
  ]
})
export class ReportsModule { }
