import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PpsRoutingModule } from './pps-routing.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PpsRoutingModule,
    SharedModule
  ]
})
export class PpsModule { }
