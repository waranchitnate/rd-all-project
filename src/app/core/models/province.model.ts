export interface ProvinceModel {
  provinceCode: string;
  provinceNameEn: string;
  provinceNameTh: string;
}
