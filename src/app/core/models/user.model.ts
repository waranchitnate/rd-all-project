export interface User {
  createdDate?: string;
  updatedDate?: Date;
  userId?: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  email: string;
}
