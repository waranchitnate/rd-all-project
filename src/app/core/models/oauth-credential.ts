import {ProgramModel} from './program.model';

export class OauthCredential {
  access_token?: string|null;
  expires_in?: number|null;
  refresh_token?: string|null;
  scope?: string|null;
  token_type?: string|null;
  programs: Array<ProgramModel>;
  jti?: string;
}
