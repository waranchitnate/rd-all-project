export interface SubDistrictModel {
  subDistrictCode: string;
  subDistrictNameTh: string;
  subDistrictNameEn: string;
}
