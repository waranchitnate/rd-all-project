export interface Status {
    statusCode: string;
    statusEngDesc: string;
    statusThDesc: string;
  }
