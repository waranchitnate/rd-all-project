export class TitleNameResponseModel {
  titleId: number;
  titleName: string;
  titleNameEn: string;
  titleCode: string;
  status: string;
}
