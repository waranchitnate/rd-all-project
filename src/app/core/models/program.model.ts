export interface ProgramModel {
  programCode: number;
  programName: string;
  path: string;
  parentId: number;
  positionId: number;
  appId: number;
  iconClass: string;
  children: Array<ProgramModel>;
}
