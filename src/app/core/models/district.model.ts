export interface DistrictModel {
  districtCode: string;
  districtNameTh: string;
  districtNameEn: string;
}
