export interface ResponseDataModel<T> {
  status: number;
  message: string;
  responseData: T;
}
