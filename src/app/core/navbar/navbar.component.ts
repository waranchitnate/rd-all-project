import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication/authentication.service';
import {SsoUserModel} from '../models/sso-user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userDetial: SsoUserModel;

  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.ssoUserDetail.subscribe(u => {
      if (u) {
        this.userDetial = u;
      }
    });
  }

  logout() {
    this.authService.removeUserLogin();
    window.close();
  }

}
