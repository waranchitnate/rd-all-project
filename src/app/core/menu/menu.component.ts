import {Component, OnInit} from '@angular/core';
import {ProgramModel} from '../models/program.model';
import {AuthenticationService} from '../authentication/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  programList: Array<ProgramModel> = [];

  constructor(private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.authenticationService.ssoUserDetail.subscribe(loginDetail => {
      this.programList = loginDetail.programs;
    });
    // this.programList = new Array<ProgramModel>();
    // this.programList.push(<ProgramModel>{
    //   programName: 'จัดการระบบนำเข้าไฟล์', children: [
    //     {programName: 'ประเภทข้อมูล', path: '/mft/data-type'},
    //     {programName: 'ตารางเวลาการทำงาน', path: '/mft/schedule'},
    //     {programName: 'รายการผู้นำส่งข้อมูลแบบนำไฟล์มาวาง', path: '/mft/sender-transfer-push'},
    //     {programName: 'รายการหน่วยงานที่นำส่งข้อมูลแบบดึงไฟล์', path: '/mft/sender-transfer-pull'},
    //     {programName: 'การจัดการรหัสของระบบ', path: '/mft/config-code'},
    //     {programName: 'รูปแบบการแจ้งเตือน', path: '/mft/notification-template'},
    //     // {programName: 'ตั้งค่าการแจ้งเตือน', path: '/mft/notification-event'},
    //     // {programName: 'ตั้งค่าช่องทางการส่งผลการตรวจสอบ<br>กลับผู้นำส่งข้อมูล', path: '/mft/notification-sender'}
    //   ]
    // });
    // this.programList.push(<ProgramModel>{
    //         programName: 'จัดการระบบคัดแยกไฟล์', children: [
    //           {programName: 'กรณีการนำส่งภาษีมูลค่าเพิ่ม', path: '/etl/vat-type'},
    //           {programName: 'อัตราภาษีมูลค่าเพิ่ม', path: '/etl/vat-rate'},
    //           {programName: 'มาตรากฏหมาย', path: '/etl/section'},
    //           {programName: 'อนุสัญญาภาษีซ้อน', path: '/etl/crossborder-dta'},
    //           {programName: 'ประเภทรายได้ของผู้รับเงินอยู่ต่างประเทศ', path: '/etl/cross-border-income-type'},
    //           {programName: 'ประเภทรายได้ของผู้รับเงินอยู่ในประเทศ', path: '/etl/domestic-income-type'},
    //           {programName: 'ค่าความคาดเคลื่อน', path: '/etl/discrepancy'},
    //           {programName: 'สกุลเงิน', path: '/etl/currency'},
    //           {programName: 'สถาบันการเงิน', path: '/etl/financial-institution'},
    //           {programName: 'คำนำหน้าชื่อ', path: '/etl/title-code'},
    //           {programName: 'ประเทศ', path: '/etl/country'},
    //           {programName: 'จังหวัด', path: '/etl/province'},
    //           {programName: 'อำเภอ', path: '/etl/amphur'},
    //           {programName: 'ตำบล', path: '/etl/thambol'},
    //           {programName: 'รหัสไปรษณีย์', path: '/etl/postcode'},
    //           {programName: 'หน่วยงานกรมสรรพากร', path: '/etl/officecode'},
    //           {programName: 'ประเภทผู้เสียภาษีอากร (0993,0994)', path: '/etl/nid-gov-asso'}
    //         ]
    //     });
    // this.programList.push(<ProgramModel>{
    //   programName: 'รายงาน', children: [
    //     {programName: 'รายงานการรับ-ส่งข้อมูล', path: '/mft/report'},
    //     {programName: 'รายงานการตรวจสอบความถูกต้อง', path: '/etl/report-validate'},
    //     {programName: 'รายงานความผิดพลาดแต่ละรายการ', path: '/etl/report-data-error'},
    //     {programName: 'รายงานสรุปผลการคัดแยกข้อมูล', path: '/etl/report-select-data'},
    //     {programName: 'รายงานผลการปรับปรุงข้อมูล<br>จากระบบงานอื่นๆ', path: '/etl/report-improve'},
    //     {programName: 'รายงานข้อมูลอ้างอิงที่ไม่พบ<br>ในระบบงานอื่นๆ', path: '/etl/report-not-found'},
    //   ]
    // });

  }

}
