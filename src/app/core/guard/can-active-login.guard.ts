import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CanActiveLoginGuard implements CanActivate {

  constructor(private authService: AuthenticationService, private router: Router) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return !this.checkLogin();
  }

  checkLogin() {

    if (this.authService.getOauthCredentialFromStorage().access_token !== null) {
      return true;
    } else {
      return false;
    }
  }
}
