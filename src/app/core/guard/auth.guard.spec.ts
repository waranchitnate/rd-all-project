import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { AuthInterceptor } from '../interceptor/auth-interceptor';
import { HttpRequest, HttpClient, HttpHeaders } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Data, Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpProgressService } from '../../shared/services/http-progress.service';
import { Injector } from '@angular/core';
import { CoreModule } from '../core.module';
import { AuthenticationService } from '../authentication/authentication.service';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from '../../modules/home/home/home.component';
import { UserHomeComponent } from '../../modules/user/user-home/user-home.component';
import { SharedModule } from '../../shared/shared.module';
import { OauthCredential, UserDetail } from '../models/oauth-credential';

describe('#authentication guard testing', () => {
    let injector: Injector;
    let authGuard: AuthGuard;
    let authenticationService: AuthenticationService;
    let activatedRouteSnapshot: ActivatedRouteSnapshot;
    let router: Router;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [
          HomeComponent,
          UserHomeComponent
        ],
        imports: [
          SharedModule,
          CoreModule,
          RouterTestingModule.withRoutes([
            { path: 'guest', component: HomeComponent},
            { path: 'member', component: UserHomeComponent, canActivate: [AuthGuard]},
            { path: 'cert-admin', component: UserHomeComponent, canActivate: [AuthGuard], data: {requestPermission: ['ADMIN']}}
          ])
        ],
      });
      authenticationService = TestBed.get(AuthenticationService);
      authenticationService.loginDetailSnapShot.access_token = null;
      router = TestBed.get(Router);
    });

    it('guest can access no requried authentication page', (done) => {
      router.navigateByUrl('/guest').then(res => {
        expect(res).toEqual(true);
        done();
      });
    });

    it('guest can not access required authentication page', (done) => {
      router.navigateByUrl('/member').then(res => {
        expect(res).toEqual(false);
        done();
      });
    });

    it('member can acess no requried authentication page', (done) => {
      authenticationService.loginDetailSnapShot.access_token = 'token';
      router.navigateByUrl('/guest').then(res => {
        expect(res).toEqual(true);
        done();
      });
    });

    it('member can access required authentication page', (done) => {
      authenticationService.loginDetailSnapShot.access_token = 'token';
      router.navigateByUrl('/member').then(res => {
        expect(res).toEqual(true);
        done();
      });
    });

    it('member with cert-user role can not access cert-admin', (done) => {
      authenticationService.loginDetailSnapShot.access_token = 'token';
      authenticationService.loginDetailSnapShot.user_detail = new UserDetail();
      authenticationService.loginDetailSnapShot.user_detail.authorities = ['USER'];
      router.navigateByUrl('/cert-admin').then(res => {
        expect(res).toEqual(false);
        done();
      });
    });

    it('member with cert-admin role can not access cert-admin', (done) => {
      authenticationService.loginDetailSnapShot.access_token = 'token';
      authenticationService.loginDetailSnapShot.user_detail = new UserDetail();
      authenticationService.loginDetailSnapShot.user_detail.authorities = ['ADMIN'];
      router.navigateByUrl('/cert-admin').then(res => {
        expect(res).toEqual(true);
        done();
      });
    });
});
