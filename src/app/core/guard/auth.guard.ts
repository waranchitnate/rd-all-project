import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../authentication/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthenticationService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLogin() && this.checkProgramPermission(next);
  }

  checkLogin() {
    const oauthCredentail = this.authService.getOauthCredentialFromStorage();
    if (oauthCredentail) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

  checkProgramPermission(activateRoute: ActivatedRouteSnapshot) {
    if (!activateRoute.data || !activateRoute.data.requestPermission) {
      return true;
    } else {
      const requestPermission = activateRoute.data.requestPermission as Array<string>;
      const authorities = this.authService.ssoUserDetailSnapshot.authorities;
      if (requestPermission.find(s => authorities.indexOf(s) !== -1)) {
        return true;
      } else {
        this.router.navigate(['/un-authorize']);
        return false;
      }
    }
  }
}
