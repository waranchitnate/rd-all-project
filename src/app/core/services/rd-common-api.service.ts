import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TitleNameResponseModel} from '../models/TitleNameResponse.model';
import {ProvinceModel} from '../models/province.model';
import {DistrictModel} from '../models/district.model';
import {SubDistrictModel} from '../models/sub-district.model';

@Injectable({
  providedIn: 'root'
})
export class RdCommonApiService {

  private rdCommonApiEndpoint = environment.commonApiUrl;
  private titleNameApiEndpoint = 'title-name/';

  constructor(private http: HttpClient) {
  }

  public getAllTitleName(): Observable<Array<TitleNameResponseModel>> {
    return this.http.get<Array<TitleNameResponseModel>>(this.rdCommonApiEndpoint + this.titleNameApiEndpoint + 'find-all');
  }

  public getTitleNameById(titleNameId: number | string): Observable<TitleNameResponseModel> {
    return this.http.get<TitleNameResponseModel>(this.rdCommonApiEndpoint + this.titleNameApiEndpoint + 'find-by-id?id=' + titleNameId.toString());
  }

  public getAllProvince(): Observable<Array<ProvinceModel>> {
    return this.http.get<Array<ProvinceModel>>(this.rdCommonApiEndpoint + 'province/find-all');
  }

  public getProvinceByProvinceCode(provinceCode: string): Observable<ProvinceModel> {
    return this.http.get<ProvinceModel>(this.rdCommonApiEndpoint + 'province/find-by-code?code=' + provinceCode);
  }

  public getAllDistrict(): Observable<Array<DistrictModel>> {
    return this.http.get<Array<DistrictModel>>(this.rdCommonApiEndpoint + 'district/find-all');
  }

  public getDistrictByDistrictCode(districtCode: string): Observable<DistrictModel> {
    return this.http.get<DistrictModel>(this.rdCommonApiEndpoint + 'district/find-by-code?code=' + districtCode);
  }

  public getAllDistrictByProvinceCode(provinceCode: string): Observable<Array<DistrictModel>> {
    return this.http.get<Array<DistrictModel>>(this.rdCommonApiEndpoint + 'district/find-all-by-province-code?code=' + provinceCode);
  }

  public getAllSubDistrict(): Observable<Array<SubDistrictModel>> {
    return this.http.get<Array<SubDistrictModel>>(this.rdCommonApiEndpoint + 'sub-district/find-all');
  }

  public getSubDistrictBySubDistrictCode(subDistrictCode: string): Observable<SubDistrictModel> {
    return this.http.get<SubDistrictModel>(this.rdCommonApiEndpoint + 'sub-district/find-by-code?code=' + subDistrictCode);
  }

  public getAllSubDistrictByDistrictCode(districtCode: string): Observable<Array<SubDistrictModel>> {
    return this.http.get<Array<SubDistrictModel>>(this.rdCommonApiEndpoint + 'sub-district/find-all-by-district-code?code=' + districtCode);
  }

}
