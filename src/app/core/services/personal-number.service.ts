import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersonalNumberService {

  constructor() { }

  checkSum(pId: string): boolean {
    let multiNum = 13;
    let strDigit = 0;
    let result = 0;
    while (multiNum != 1) {
      let digit = Number(pId.charAt(strDigit));
      result = result + digit * multiNum;
      multiNum = multiNum - 1;
      strDigit = strDigit + 1;
    }
    result = result % 11;
    if (result == 0) {
      result = 10;
    }

    result = 11 - result;

    if(result == 10) {
      result = 0;
    }

    if(result == Number(pId.charAt(12))) {
      return true;
    } else {
      return false;
    }
  }
}
