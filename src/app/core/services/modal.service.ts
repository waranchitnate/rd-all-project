import { AnswerModalComponent } from '../../shared/modals/answer-modal/answer-modal.component';
import {Injectable} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import { AlertModalComponent } from '../../shared/modals/alert-modal/alert-modal.component';


@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(public modalService: BsModalService) { }

  openModal(title, content): BsModalRef {
    const modalRef = this.modalService.show(AlertModalComponent);
    modalRef.content.title = title;
    modalRef.content.content = content;
    return modalRef;
  }
}
