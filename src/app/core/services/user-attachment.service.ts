import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserAttachmentModel} from '../../modules/admin/public-user-management/models/user-attachment.model';
import {ScannedUserAttachmentModel} from '../../modules/admin/public-user-management/models/scanned-user-attachment.model';

@Injectable({
  providedIn: 'root'
})
export class UserAttachmentService {

  private userAttachmentApi = environment.internalUserManagementApiUrl + 'user-attachment/';
  constructor(private http: HttpClient) { }

  public findAllByUserId(hashUserId: string): Observable<Array<UserAttachmentModel>> {
    return this.http.get<Array<UserAttachmentModel>>(this.userAttachmentApi + 'find-all-by-user-id/' + hashUserId);
  }

  public addScannedAttachment(scannedAttachment: ScannedUserAttachmentModel, username: string) {
    return this.http.post(this.userAttachmentApi + 'add-scanned-user-attachment?username=' + username, scannedAttachment);
  }

  public findScanAttachAllByUserId(hashUserId: string): Observable<Array<ScannedUserAttachmentModel>> {
    return this.http.get<Array<ScannedUserAttachmentModel>>(this.userAttachmentApi + 'find-attach-scan-all-by-user-id/' + hashUserId);
  }
}
