import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';

import {AuthGuard} from './guard/auth.guard';
import {AuthenticationService} from './authentication/authentication.service';
import {MenuComponent} from './menu/menu.component';
import {HttpProgressService} from '../shared/services/http-progress.service';
import {BsDropdownModule} from 'ngx-bootstrap';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { AuthenticationComponent } from './pages/authentication/authentication.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot()
  ],
  declarations: [MenuComponent, NavbarComponent, FooterComponent, AuthenticationComponent],
  providers: [
    AuthenticationService,
    AuthGuard,
    HttpProgressService,
   ],
  exports: [MenuComponent, NavbarComponent, FooterComponent, HttpClientModule]
})
export class CoreModule {
}
