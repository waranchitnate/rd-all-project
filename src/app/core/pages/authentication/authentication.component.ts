import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../authentication/authentication.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private authenService: AuthenticationService) {
  }

  ngOnInit() {
    const paramMap = this.activatedRoute.snapshot.queryParamMap;
    if (paramMap.has('token')) {
      const token = paramMap.get('token');
      this.authenService.loginToken(token).subscribe(res => {
        this.router.navigateByUrl('/');
      }, err => {
        console.error(err);
      });
    } else {
      const oauthCredential = this.authenService.getOauthCredentialFromStorage();
      if (oauthCredential) {
        this.authenService.refreshToken(oauthCredential.refresh_token).subscribe(() => {
          const redirectUri = paramMap.get('redirectTo');
          if (redirectUri !== undefined && redirectUri !== '/login') {
            this.router.navigateByUrl(redirectUri);
          } else {
            this.router.navigate(['/']);
          }
        });
      } else {
        this.router.navigate(['login']);
      }
    }
  }

}
