import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthenticationService } from './authentication.service';
import { OauthCredential, UserDetail } from '../models/oauth-credential';

function createUserDetailToken() {
    return btoa(JSON.stringify( <UserDetail>{
        username: 'test',
        authorities: ['ROLE_USER', 'ROLE_ADMIN', 'PERMISSION_TEST'],
        email: null,
    }));
}

describe('Authentication service', () => {
    let authenticationService: AuthenticationService;
    let httpTestingController: HttpTestingController;
    const testLoginDetail = <OauthCredential>{
        access_token: 'aaa.' + createUserDetailToken(),
        email: 'test@test.com',
        expires_in: null,
        refresh_token: null,
        scope: null,
        token_type: null,
        jti: null,
        user_detail:  <UserDetail>{
            username: 'test',
            authorities: ['ROLE_USER', 'ROLE_ADMIN'],
            email: null,
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule
            ],
            providers: [
                AuthenticationService
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
        authenticationService = TestBed.get(AuthenticationService);
    });

    it('can login', (done) => {
        // send username password to back-end
        authenticationService.login('username', 'password').subscribe();

        // catch login req
        const req = httpTestingController.expectOne(authenticationService.url);
        expect(req.request.method).toEqual('POST');
        // response data to login req
        req.flush(testLoginDetail);

        // check login detail in authen service
        authenticationService.loginDetail.subscribe(res => {
            expect(res).toEqual(testLoginDetail);
            done();
        });

        httpTestingController.verify();
    });

    it('can login by sso token', (done) => {
        authenticationService.loginToken('ss0-token').subscribe();
        const req = httpTestingController.expectOne(authenticationService.url);
        expect(req.request.method).toEqual('POST');
        req.flush(testLoginDetail);

        authenticationService.loginDetail.subscribe(res => {
            expect(res).toEqual(testLoginDetail);
            done();
        });

        httpTestingController.verify();
    });

    it('has role work and permission', () => {
        authenticationService.loginDetailSnapShot.user_detail = null;
        expect(authenticationService.hasRole('ADMIN')).toEqual(false);
        expect(authenticationService.hasPermission('TEST')).toEqual(false);

        authenticationService.saveUserLogin(testLoginDetail);

        expect(authenticationService.loginDetailSnapShot.user_detail.authorities).toContain('ROLE_ADMIN');
        expect(authenticationService.loginDetailSnapShot.user_detail.authorities).toContain('PERMISSION_TEST');

        expect(authenticationService.hasRole('ADMIN')).toEqual(true);
        expect(authenticationService.hasPermission('TEST')).toEqual(true);

        expect(authenticationService.hasRole('AD@E@!E!@')).toEqual(false);
        expect(authenticationService.hasPermission('ASD@@#$')).toEqual(false);
    });

    afterEach(() => {
        authenticationService.deleteStorage();

        // After every test, assert that there are no more pending requests.
        httpTestingController.verify();
    });
});
