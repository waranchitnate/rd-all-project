import {combineLatest} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {BehaviorSubject, Observable, pipe} from 'rxjs';
import {Router} from '@angular/router';
import {OauthCredential} from '../models/oauth-credential';
import {SsoUserModel} from '../models/sso-user.model';
import {log} from 'util';
import {ProgramModel} from '../models/program.model';
import {map, tap} from 'rxjs/operators';
import { element } from '@angular/core/src/render3';


@Injectable()
export class AuthenticationService {
  // Response: Response;

  defaultLoginDetail = null;
  ssoUserDetail = new BehaviorSubject<SsoUserModel>(this.defaultLoginDetail);
  ssoUserDetailSnapshot: SsoUserModel = this.defaultLoginDetail;

  tokenUrl = environment.oauthUrl + 'oauth/token';
  private rdOauthUrl = environment.oauthUrl;

  constructor(private http: HttpClient
    , private router: Router) {
    this.ssoUserDetail.subscribe(detail => {
      this.ssoUserDetailSnapshot = detail;
    });
  }

  loginToken(token: string) {
    const body = new HttpParams({
      fromObject: {
        grant_type: 'sso',
        token: token
      }
    });

    const encoded = btoa('adminapp' + ':' + 'password');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        'Authorization': 'Basic ' + encoded,
        'SkipAuth': '1'
      })
    };

    return this.http.post(this.tokenUrl, body, httpOptions).pipe(tap(ret => {
      this.saveOauthToStorage(ret);
      combineLatest(this.getSSOUserDetail(), this.getPrograms())
        .pipe(
          map(([ssoUser, programs]) => {
            ssoUser.programs = programs;
            return ssoUser;
          }))
        .subscribe(ssoUser => this.ssoUserDetail.next(ssoUser));
    }));
  }

  refreshToken(refreshToken) {
    const body = new HttpParams({
      fromObject: {
        grant_type: 'refresh_token',
        refresh_token: refreshToken
      }
    });

    const encoded = btoa('adminapp' + ':' + 'password');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        'Authorization': 'Basic ' + encoded,
        'SkipAuth': '1'
      })
    };

    return this.http.post(this.tokenUrl, body, httpOptions).pipe(tap(ret => {
      this.saveOauthToStorage(ret);
      combineLatest(this.getSSOUserDetail(), this.getPrograms())
        .pipe(
          map(([ssoUser, programs]) => {
            ssoUser.programs = programs;
            return ssoUser;
          }))
        .subscribe(ssoUser => this.ssoUserDetail.next(ssoUser));
    }, () => {
      this.removeUserLogin();
    }));
  }

  getSSOUserDetail(): Observable<SsoUserModel> {
    return this.http.get<SsoUserModel>(this.rdOauthUrl + 'user/me');
  }

  getPrograms(): Observable<Array<ProgramModel>> {
    return this.http.get<Array<ProgramModel>>(this.rdOauthUrl + 'menu/find-all');
  }

  removeUserLogin() {
    localStorage.removeItem('rd-internal-oauth');
    this.ssoUserDetail.next(this.defaultLoginDetail);
  }

  saveOauthToStorage(ret: object) {
    localStorage.setItem('rd-internal-oauth', JSON.stringify(ret));
  }

  getOauthCredentialFromStorage(): OauthCredential {
    const login = localStorage.getItem('rd-internal-oauth');
    const detail = JSON.parse(login);
    if (detail) {
      return detail;
    }
    return null;
  }

  deleteStorage() {
    //  test only
    localStorage.removeItem('rd-internal-oauth');
  }

  hasAnyPermission(permissionList: Array<string>) {
    if (!permissionList || !this.ssoUserDetailSnapshot) {
      return false;
    }
    const userDetail = this.ssoUserDetailSnapshot;

    if (userDetail != null && userDetail.authorities != null) {
      const result = userDetail.authorities.find((a: any) => {
        return permissionList.indexOf(a.authority.replace('PERMISSION_', '')) !== -1;
      });
      return result !== undefined && result != null;
    }
  }

  hasPermission(permissionName: string): boolean {
    if (!permissionName || !this.ssoUserDetailSnapshot) {
      return false;
    }
    const userDetail = this.ssoUserDetailSnapshot;
    if (userDetail != null && permissionName != null) {
      const permissionFullName = 'PERMISSION_' + permissionName.toUpperCase();

      const index = userDetail.authorities.findIndex(element => element === permissionFullName);
      return (index !== -1);

    }
    return false;
  }

  hasRole(roleName: string): boolean {
    if (!roleName || !this.ssoUserDetailSnapshot) {
      return false;
    }
    const userDetail = this.ssoUserDetailSnapshot;
    if (userDetail != null && roleName != null) {
      const roleFullName = 'ROLE_' + roleName.toUpperCase();
      const index = userDetail.authorities.findIndex(element => element === roleFullName);
      return (index !== -1);
    }

    return false;
  }

}
