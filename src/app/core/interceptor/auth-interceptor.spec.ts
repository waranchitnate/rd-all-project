import { AuthenticationService } from '../authentication/authentication.service';
import { of, throwError } from 'rxjs';
import { AuthInterceptor } from './auth-interceptor';
import { TestBed } from '@angular/core/testing';
import { HomeComponent } from '../../modules/home/home/home.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpProgressService } from '../../shared/services/http-progress.service';


class MockAuthenService extends AuthenticationService {
  token = {'access_token': 'first token', 'refresh_token': 'refresh token'};

  getStorage(key) {
    return this.token[key];
  }

  refreshToken(refreshToken) {
    this.token['access_token'] = refreshToken;
    return of('refresh success');
  }

  deleteStorage() {

  }
}

describe('authentication interceptor', () => {
  let httpInterceptor: AuthInterceptor;
  let authenticationService: MockAuthenService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [
          // for authentication interceptor when nav to login
          HomeComponent,
        ],
        imports: [
          HttpClientTestingModule,
          RouterTestingModule.withRoutes([
            {path: 'login', component: HomeComponent}
          ])
        ]
      });

      authenticationService = new MockAuthenService(TestBed.get(HttpClient), TestBed.get(Router));
      httpInterceptor = new AuthInterceptor(authenticationService, TestBed.get(Router), new HttpProgressService());
    });

    it('header must have authorization', (done) => {

    // create handler request
    const next: any = {
        handle: (request: HttpRequest<any>) => {
        expect(request.headers.has('Authorization')).toBeTruthy();
        expect(request.headers.get('Authorization')).toEqual('Bearer first token');
          return of(new HttpResponse({body: 'success'}));
        }
    };

    // create req
    const req = new HttpRequest<any>('GET', '/data');

    httpInterceptor.intercept(req, next).subscribe(obj => {
      expect(obj['body']).toEqual('success');
      done();
    });

    });

    it('header with skip authentication must not have authorization header ', (done) => {
    // create handler request
    const next: any = {
        handle: (request: HttpRequest<any>) => {
        expect(request.headers.has('Authorization')).toBeFalsy();
          return of({ hello: 'world' });
        }
    };
    // create req
    const skipAuthReq = new HttpRequest<any>('GET', '/data', {headers: new HttpHeaders({'SkipAuth': '1'}) });
    httpInterceptor.intercept(skipAuthReq, next).subscribe(obj => done());
    });

    it('expired token must refresh and re send request ', (done) => {
      expect(authenticationService.getStorage('access_token')).toEqual('first token');
      const next: any = {
          handle: (request: HttpRequest<any>) => {
          if (request.headers.get('Authorization') == 'Bearer first token') {
            return throwError(new HttpErrorResponse({status: 401}));
          } else if (request.headers.get('Authorization') == 'Bearer refresh token') {
            return of(new HttpResponse({body: 'success'}));
          }
        }
      };
      const req = new HttpRequest<any>('GET', '/data');

      httpInterceptor.intercept(req, next).subscribe(obj => {
        expect(obj['body']).toEqual('success');
        done();
      });
    });

    it('handle refresh token fail request', (done) => {
      expect(authenticationService.getStorage('access_token')).toEqual('first token');
      const next: any = {
          handle: (request: HttpRequest<any>) => {
          if (request.headers.get('Authorization') == 'Bearer first token') {
            return throwError(new HttpErrorResponse({status: 401}));
          } else if (request.headers.get('Authorization') == 'Bearer refresh token') {
            return throwError('throw error');
          }
        }
      };
      const req = new HttpRequest<any>('GET', '/data');

      httpInterceptor.intercept(req, next).subscribe(obj => {
        done();
      }, err => {
        expect(err).toEqual('throw error');
        done();
      });
    });
});
