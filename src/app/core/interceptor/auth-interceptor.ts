import {throwError as observableThrowError, Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import {catchError, switchMap, finalize, filter, take} from 'rxjs/operators';
import {Router} from '@angular/router';

import {AuthenticationService} from '../authentication/authentication.service';
import {HttpProgressService} from '../../shared/services/http-progress.service';
import {ToastrService} from 'ngx-toastr';
import {OauthCredential} from '../models/oauth-credential';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService, private router: Router, private httpProgress: HttpProgressService
    , private toastrService: ToastrService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler)
    : Observable<HttpEvent<any>> {
    let headers = req.headers;
    let oauthCredential = this.authenticationService.getOauthCredentialFromStorage();
    const skip = headers.get('SkipAuth');
    headers = headers.delete('SkipAuth');
    // push req-url to progress service so app component will show progress bar
    this.httpProgress.pushRequestToHttpProgress(req.url);
    if (oauthCredential && skip !== '1') {
      headers = headers.set('Authorization', 'Bearer ' + oauthCredential.access_token);
      const authReq = req.clone({headers});
      return next.handle(authReq).pipe(
        // pop req-url out when success
        finalize(() => {
          this.httpProgress.popRequestFromHttpProgress(req.url);
        }),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch ((<HttpErrorResponse>err).status) {
              case 401:
                oauthCredential = this.authenticationService.getOauthCredentialFromStorage();
                this.authenticationService.deleteStorage();
                if (oauthCredential !== null) {
                  return this.refreshTokenAndRequest(req, next, oauthCredential);
                } else {
                  return this.requestWhenRefreshTokenSuccessful(req, next);
                }
              case 403:
                this.toastrService.error('ขออภัยคุณไม่มีสิทธิ์เข้าใช้งานฟังก์ชั่น ' + req.url);
            }
          }
          return observableThrowError(err);
        }),
      );
    } else {
      const newAuthReq = req.clone({headers});
      return next.handle(newAuthReq).pipe(
        finalize(() => {
          this.httpProgress.popRequestFromHttpProgress(req.url);
        }),
      );
    }
  }

  refreshTokenAndRequest(req: HttpRequest<any>, next: HttpHandler, oauthCredential: OauthCredential): Observable<HttpEvent<any>> {
    let headers = req.headers;
    const refreshToken = oauthCredential.refresh_token;
    return this.authenticationService.refreshToken(refreshToken).pipe(
      switchMap(() => {
        const accessToken = this.authenticationService.getOauthCredentialFromStorage().access_token;
        headers = headers.set('Authorization', 'Bearer ' + accessToken);
        const newAuthReq = req.clone({headers});
        return next.handle(newAuthReq);
      }),
      catchError(err2 => {
        this.router.navigate(['/un-authorize']);
        this.toastrService.error('Token ของท่านหมดอายุการใช้งาน กรุณาล็อคอินเพื่อขอ Token ใหม่อีกครั้ง');
        return observableThrowError(err2);
      })
    );
  }

  requestWhenRefreshTokenSuccessful(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers = req.headers;
    return this.httpProgress.getHttpProgressObservable()
      .pipe(
        filter(reqList => reqList.filter(s => s.indexOf(this.authenticationService.tokenUrl) !== -1).length === 0),
        take(1),
        switchMap(() => {
          if (this.authenticationService.getOauthCredentialFromStorage() === null) {
            const httpEvent = new HttpResponse({
              status: 401,
              statusText: 'Refresh Token is not successful so request will not be made again'
            });
            this.router.navigate(['/un-authorize']);
            return of(httpEvent);
          } else {
            const accessToken = this.authenticationService.getOauthCredentialFromStorage().access_token;
            headers = headers.set('Authorization', 'Bearer ' + accessToken);
            const newAuthReq = req.clone({headers});
            return next.handle(newAuthReq);
          }
        })
      );
  }

}
