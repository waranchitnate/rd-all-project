import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {HomeModule} from './modules/home/home.module';
import {EdcModule} from './modules/edc/edc.module';
import {NotFoundComponent} from './core/pages/not-found/not-found.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './core/interceptor/auth-interceptor';
import { UnAuthorizeComponent } from './core/pages/un-authorize/un-authorize.component';
import { registerLocaleData, DatePipe } from '@angular/common';
import th from '@angular/common/locales/th';
registerLocaleData(th);

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    UnAuthorizeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    EdcModule,
    HomeModule,
    AppRoutingModule,
  ],
  providers: [ {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {
}
