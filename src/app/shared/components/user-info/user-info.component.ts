import {Component, OnInit, Input} from '@angular/core';
import {AuthenticationService} from 'src/app/core/authentication/authentication.service';
import {SsoUserModel} from '../../../core/models/sso-user.model';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  @Input('headerText') headerText: string;
  customClass = 'customClass';
  currentUserInfo: SsoUserModel;

  constructor(public authenService: AuthenticationService) {
  }

  ngOnInit() {
    this.getCurrentUserInfo();
  }

  getCurrentUserInfo() {
    this.currentUserInfo = this.authenService.ssoUserDetailSnapshot
    return this.currentUserInfo;
  }

  get userInfo() {
    return this.currentUserInfo;
  }
}
