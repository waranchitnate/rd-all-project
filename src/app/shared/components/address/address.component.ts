import {Tambon} from '../../models/address.model';
import {AddressService} from '../../services/address.service';
import {Component, OnInit} from '@angular/core';
import {ControlContainer} from '@angular/forms';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  provinceList = [];
  amphoeList = [];
  tambonList = [];

  constructor(public controlContainer: ControlContainer, private addressService: AddressService) { }

  ngOnInit() {
    this.initChange();
  }

  initChange() {

    this.addressService.getProvince().subscribe(res => {
      this.provinceList = res;
      this.chackValueIn('province', res);
    });

    this.controlContainer.control.get('province').valueChanges.subscribe(provinceChange => {
      if (provinceChange !== null) {
        this.addressService.getAmphoe(provinceChange).subscribe(resAmp => {
          this.amphoeList = resAmp;
          this.chackValueIn('amphoe', resAmp);
        });
      } else {
        this.amphoeList = [];
        this.controlContainer.control.get('amphoe').setValue(null);
      }

    });

    this.controlContainer.control.get('amphoe').valueChanges.subscribe(amphoeChange => {
      if (amphoeChange !== null) {
        this.addressService.getTambon(amphoeChange).subscribe(resTam => {
          this.tambonList = resTam;
          this.chackValueIn('tambon', resTam);
        });
      } else {
        this.tambonList = [];
        this.controlContainer.control.get('tambon').setValue(null);
      }
    });

    this.controlContainer.control.get('tambon').valueChanges.subscribe((tambonChange: string) => {
      if (tambonChange !== null) {
        const tambon = this.tambonList.find((t: Tambon) => t.tambonId === tambonChange);
        if (tambon) {
          this.controlContainer.control.get('postcode').setValue(tambon.zipCode);
        }
      } else {
        this.controlContainer.control.get('postcode').setValue(null);
      }

    });
  }

  chackValueIn(formCtrlName, list: any[]) {
    const controlValue = this.controlContainer.control.get(formCtrlName).value;
    const index = list.findIndex(i => {
      return i.provinceId == controlValue
        || i.amphoeId == controlValue
        || i.tambonId == controlValue;
    });
    if (index === -1) {
      this.controlContainer.control.get(formCtrlName).setValue(null);
    }

    if (this.controlContainer.control.get(formCtrlName).untouched) {
      this.controlContainer.control.get(formCtrlName).setValue(this.controlContainer.control.get(formCtrlName).value);
    }
  }


  get province() { return this.controlContainer.control.get('province'); }
  get amphoe() { return this.controlContainer.control.get('amphoe'); }
  get tambon() { return this.controlContainer.control.get('tambon'); }
  get no() { return this.controlContainer.control.get('no'); }
  get moo() { return this.controlContainer.control.get('moo'); }
  get build() { return this.controlContainer.control.get('build'); }
  get soi() { return this.controlContainer.control.get('soi'); }
  get trok() { return this.controlContainer.control.get('trok'); }
  get road() { return this.controlContainer.control.get('road'); }
}
