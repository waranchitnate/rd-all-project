import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ControlContainer, FormBuilder, FormGroupDirective, ReactiveFormsModule, Validators } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { AddressComponent } from './address.component';
import { AddressService } from '../../services/address.service';


describe('AddressComponent', () => {
  let component: AddressComponent;
  let fixture: ComponentFixture<AddressComponent>;
  const fb = new FormBuilder();
  const fg = fb.group({
    no: ['', Validators.required],
    moo: [''],
    build: [''],
    soi: ['', Validators.required],
    trok: ['', Validators.required],
    road: ['', Validators.required],
    province: [null, Validators.required],
    amphoe: [ null, Validators.required],
    tambon: [null, Validators.required],
    postcode: ['', Validators.required],
  });

  const fgd: FormGroupDirective = new FormGroupDirective([], []);
  fgd.form = fg;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressComponent ],
      imports: [ ReactiveFormsModule, NgSelectModule, HttpClientTestingModule],
      providers: [ AddressService,
        { provide: ControlContainer, useValue: fgd }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
