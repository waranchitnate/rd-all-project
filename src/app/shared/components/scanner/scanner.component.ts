import { Component, OnInit, Output, EventEmitter, resolveForwardRef, Input } from '@angular/core';
import { FileService } from '../../services/file.service';
import { SafeResourceUrl } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie-service';
import { MRegisteredAttachmentModel } from 'src/app/modules/admin/public-user-management/models/m-registered-attachment.model';
import { UserAttachmentModel } from 'src/app/modules/admin/public-user-management/models/user-attachment.model';
import { FileModel } from 'src/app/modules/admin/public-user-management/models/file.model';
import { ScannedUserAttachmentModel } from 'src/app/modules/admin/public-user-management/models/scanned-user-attachment.model';
import { UserAttachmentService } from 'src/app/core/services/user-attachment.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.css']
})
export class ScannerComponent implements OnInit {

  @Output() close = new EventEmitter();
  @Output() save = new EventEmitter();
  @Input() scRegisteredAttachment: MRegisteredAttachmentModel;
  @Input() scUserAttachmentModel: UserAttachmentModel;
  @Input() scUserId: number;
  @Input() scUsername: string;

  scannerList = [];
  scanner = '';
  pageSide = '';
  base64Pdf = '';
  pdf: SafeResourceUrl = null;
  fileName = '';
  textBase64: any;
  userAttachmentModel: UserAttachmentModel;
  userId;
  mRegisteredAttachmentId;
  username;
  scannedUserAttachmentList: Array<ScannedUserAttachmentModel> = [];

  constructor(
    private fileService: FileService,
    private cookieService: CookieService,
    private userAttachmentService: UserAttachmentService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.userId = this.scUserId;
    this.mRegisteredAttachmentId = this.scRegisteredAttachment.id;
    this.username = this.scUsername;
    setTimeout(() => {
      this.fileService.getScannerList().subscribe(res => {
        // HP Deskjet 1050 J410 series
        // let p = {'name': 'HP Deskjet 1050 J410 series'};
        this.scannerList = res.responseMsg;
        this.scanner = this.cookieService.get('rd-internal-scanner-name');
        this.pageSide = this.cookieService.get('rd-internal-page-side');
      });
    });
  }

  onScan() {
    this.fileService.scan(this.scanner, this.pageSide).subscribe(res => {
      if (res.errorCode === '0') {
        this.base64Pdf = res.base64Pdf;
        this.pdf = res.pdf;
        this.fileName = res.fileName;
        this.cookieService.set('rd-internal-scanner-name', this.scanner);
        this.cookieService.set('rd-internal-page-side', this.pageSide);
      } else {
        this.toastrService.info('Check Printer Connector Or Power On.... ');
        // alert('Check Printer Connector Or Power On.... ');
      }
    }, err => {
      this.toastrService.info('Error in getting document.');
      // alert('Error in getting document.');
    });
  }

  downloadPDF() {
    const byteArray = new Uint8Array(atob(this.base64Pdf.replace(/\s/g, '')).split('').map(char => char.charCodeAt(0)));
    const pdfBlob = new Blob([byteArray], {type: 'application/pdf'});
    window.navigator.msSaveOrOpenBlob(pdfBlob, this.fileName);
  }

  onClose() {
    this.getScanAttachFile();
    this.close.emit();
  }

  getScanAttachFile() {
    this.userAttachmentService.findScanAttachAllByUserId(this.userId).subscribe(res => {
      this.scannedUserAttachmentList = res;
    });
  }

  onSave() {
   const fileExtension = 'pdf';
   const fileName = 'SC' + this.mRegisteredAttachmentId + this.username;
   const dataBase64 = this.base64Pdf;

    this.fileService.uploadScannedAttachment(this.username, fileName, fileExtension, dataBase64).subscribe(() => {
        const fileModel: FileModel = <FileModel>{
          fileBaseName: fileName,
          fileExtension: fileExtension,
          dataBase64: dataBase64
        };
        const scannedUserAttachment: ScannedUserAttachmentModel = new ScannedUserAttachmentModel();
        scannedUserAttachment.userId = this.userId;
        scannedUserAttachment.mRegisteredAttachmentId = this.mRegisteredAttachmentId;
        scannedUserAttachment.fileDto = fileModel;
        this.userAttachmentService.addScannedAttachment(scannedUserAttachment, this.username).subscribe(() => {
          this.toastrService.success('เพิ่มเอกสารสำเร็จ');
          this.onClose();
        }, err => {
          this.toastrService.error('เพิ่มเอกสารไม้สำรเร็จเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
        });

      }, err => {
           this.toastrService.error('อัพโหลดเอกสารไม่สำเร็จเนื่องจาก ' + err.error.message, 'ข้อผิดพลาด');
         });
  }



}
