import { Component, OnInit, Input } from '@angular/core';
import { Step } from '../../models/step.model';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css']
})
export class StepperComponent implements OnInit {

  @Input() listStep: Step[] = [];

  // active position start at 0
  @Input() activePosition: number = 0;

  constructor() { }

  ngOnInit() {
    if (this.listStep.length < (this.activePosition + 1) ) {
      this.activePosition = this.listStep.length - 1;
    }
  }



}
