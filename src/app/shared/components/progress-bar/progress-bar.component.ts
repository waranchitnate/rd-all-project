import { Component, OnInit, Input, ViewChild, AfterViewChecked, AfterViewInit } from '@angular/core';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { HttpProgressService } from '../../services/http-progress.service';
import { timeInterval } from 'rxjs/operators';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit, AfterViewInit {

  @Input() name;
  @ViewChild('content') content: any;

  isLoading = true;

  constructor(private httpProgress: HttpProgressService, private modalService: NgbModal, config: NgbModalConfig) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {


  }

  ngAfterViewInit() {
    // fixed detect change bug in angular
    setTimeout(() => {
      this.httpProgress.getHttpProgressObservable().subscribe(res => {
        if (res.length != 0) {
          this.isLoading = true;
        } else {
          this.isLoading = false;
        }
      });
    });
  }

}
