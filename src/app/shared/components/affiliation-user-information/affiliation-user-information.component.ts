import {Component, Input, OnInit} from '@angular/core';
import {PublicUserModel} from '../../models/public-user.model';

@Component({
  selector: 'app-affiliation-user-information',
  templateUrl: './affiliation-user-information.component.html',
  styleUrls: ['./affiliation-user-information.component.css']
})
export class AffiliationUserInformationComponent implements OnInit {

  @Input() publicUser: PublicUserModel;
  constructor() { }

  ngOnInit() {
  }

}
