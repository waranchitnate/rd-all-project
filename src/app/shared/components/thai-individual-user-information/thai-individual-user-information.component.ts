import {Component, Input, OnInit} from '@angular/core';
import {PublicUserModel} from '../../models/public-user.model';

@Component({
  selector: 'app-thai-individual-user-information',
  templateUrl: './thai-individual-user-information.component.html',
  styleUrls: ['./thai-individual-user-information.component.css']
})
export class ThaiIndividualUserInformationComponent implements OnInit {

  @Input() publicUser: PublicUserModel;
  constructor() { }

  ngOnInit() {
  }

}
