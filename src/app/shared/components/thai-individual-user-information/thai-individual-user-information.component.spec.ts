import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThaiIndividualUserInformationComponent } from './thai-individual-user-information.component';

describe('ThaiIndividualUserInformationComponent', () => {
  let component: ThaiIndividualUserInformationComponent;
  let fixture: ComponentFixture<ThaiIndividualUserInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThaiIndividualUserInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThaiIndividualUserInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
