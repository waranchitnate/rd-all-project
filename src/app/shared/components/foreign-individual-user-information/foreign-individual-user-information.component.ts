import {Component, Input, OnInit} from '@angular/core';
import {PublicUserModel} from '../../models/public-user.model';

@Component({
  selector: 'app-foreign-individual-user-information',
  templateUrl: './foreign-individual-user-information.component.html',
  styleUrls: ['./foreign-individual-user-information.component.css']
})
export class ForeignIndividualUserInformationComponent implements OnInit {

  @Input() publicUser: PublicUserModel;
  constructor() { }

  ngOnInit() {
  }

}
