import {Component, Input, OnInit} from '@angular/core';
import {PublicUserModel} from '../../models/public-user.model';

@Component({
  selector: 'app-partnership-individual-user-information',
  templateUrl: './partnership-individual-user-information.component.html',
  styleUrls: ['./partnership-individual-user-information.component.css']
})
export class PartnershipIndividualUserInformationComponent implements OnInit {

  @Input() publicUser: PublicUserModel;
  constructor() { }

  ngOnInit() {
  }

}
