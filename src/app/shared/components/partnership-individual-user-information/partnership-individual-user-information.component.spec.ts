import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnershipIndividualUserInformationComponent } from './partnership-individual-user-information.component';

describe('PartnershipIndividualUserInformationComponent', () => {
  let component: PartnershipIndividualUserInformationComponent;
  let fixture: ComponentFixture<PartnershipIndividualUserInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnershipIndividualUserInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnershipIndividualUserInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
