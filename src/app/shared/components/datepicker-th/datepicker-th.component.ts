import {
  Component,
  forwardRef,
  Input,
  ViewChild,
  ElementRef,
  Renderer2,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap-th';
import * as moment from 'moment';

@Component({
  selector: 'app-datepicker-th',
  templateUrl: './datepicker-th.component.html',
  styleUrls: ['./datepicker-th.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatepickerThComponent),
      multi: true
    }
  ]
})
export class DatepickerThComponent implements ControlValueAccessor {

  model;
  isDisabled = false;
  @Input() id: string;
  @Input() name: string;
  @Input() placeholder = 'วัน/เดือน/ปี';
  @Input() borderDanger: boolean;
  @Input() minDate: Date;
  @Input() maxDate: Date;
  @Input() readonly: boolean;

  @ViewChild('inputElement') _inputElement: ElementRef;
  @Output() valueOnchange = new EventEmitter();

  bsConfig: Partial<BsDatepickerConfig>;

  get inputElement(): ElementRef {
    return this._inputElement;
  }

  private _onChange = (_: any) => {
  };
  private _onTouched = () => {
  };

  constructor(
    private _renderer: Renderer2
  ) {
    this.bsConfig = Object.assign({}, {
      containerClass: 'theme-orange'
      , dateInputFormat: 'DD/MM/YYYY'
      , showWeekNumbers: false
    });
  }

  writeValue(obj: moment.Moment | Date | string): void {
    if (obj != null) {
      let objMoment: moment.Moment;
      if (obj instanceof Date) {
        objMoment = moment(obj);
      } else if (obj instanceof moment) {
        objMoment = obj as moment.Moment;
      } else {
        if (this.model === moment(obj, 'DD/MM/YYYY').isValid()) {
          objMoment = moment(obj, 'DD/MM/YYYY');
        } else {
          objMoment = moment(obj);
        }
      }
      // DATE PICKER RECEIVE UTC + 0
      this.model = objMoment.add(objMoment.utcOffset() / 60, 'h').toDate();
    }
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    this._renderer.setProperty(this._inputElement.nativeElement, 'disabled', isDisabled);
  }

  onChange(data: any) {
    console.log('onChange : ' + data);
    this._onChange(data);
    this.valueOnchange.emit(data);
  }

  onTouch() {
    this._onTouched();
  }

  validateInput(ngModel: NgModel) {

    if (ngModel.invalid) {
      this.model = null;
      this._onChange(this.model);
    }
  }
}
