import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerThComponent } from './datepicker-th.component';
import { NG_VALUE_ACCESSOR, FormsModule } from '@angular/forms';
import { forwardRef } from '@angular/core';
import { BsDatepickerModule, BsDatepickerConfig, BsDatepickerDirective,
  ComponentLoaderFactory, PositioningService, BsLocaleService } from 'ngx-bootstrap-th';

describe('DatepickerThComponent', () => {
  let component: DatepickerThComponent;
  let fixture: ComponentFixture<DatepickerThComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatepickerThComponent ],
      imports: [FormsModule, BsDatepickerModule],
      providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => DatepickerThComponent),
          multi: true
        },
        BsDatepickerConfig,
        BsDatepickerDirective,
        ComponentLoaderFactory,
        PositioningService,
        BsLocaleService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerThComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
