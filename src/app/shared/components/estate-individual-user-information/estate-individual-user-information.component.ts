import {Component, Input, OnInit} from '@angular/core';
import {PublicUserModel} from '../../models/public-user.model';

@Component({
  selector: 'app-estate-individual-user-information',
  templateUrl: './estate-individual-user-information.component.html',
  styleUrls: ['./estate-individual-user-information.component.css']
})
export class EstateIndividualUserInformationComponent implements OnInit {

  @Input() publicUser: PublicUserModel;
  constructor() { }

  ngOnInit() {
  }

}
