import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstateIndividualUserInformationComponent } from './estate-individual-user-information.component';

describe('EstateIndividualUserInformationComponent', () => {
  let component: EstateIndividualUserInformationComponent;
  let fixture: ComponentFixture<EstateIndividualUserInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstateIndividualUserInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstateIndividualUserInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
