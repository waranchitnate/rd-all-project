import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderUserInformationComponent } from './sender-user-information.component';

describe('SenderUserInformationComponent', () => {
  let component: SenderUserInformationComponent;
  let fixture: ComponentFixture<SenderUserInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderUserInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderUserInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
