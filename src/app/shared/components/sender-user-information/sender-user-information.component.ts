import {Component, Input, OnInit} from '@angular/core';
import {PublicUserModel} from '../../models/public-user.model';

@Component({
  selector: 'app-sender-user-information',
  templateUrl: './sender-user-information.component.html',
  styleUrls: ['./sender-user-information.component.css']
})
export class SenderUserInformationComponent implements OnInit {

  @Input() publicUser: PublicUserModel;
  constructor() { }

  ngOnInit() {
  }

}
