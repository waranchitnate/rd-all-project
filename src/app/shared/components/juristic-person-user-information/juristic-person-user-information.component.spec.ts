import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JuristicPersonUserInformationComponent } from './juristic-person-user-information.component';

describe('JuristicPersonUserInformationComponent', () => {
  let component: JuristicPersonUserInformationComponent;
  let fixture: ComponentFixture<JuristicPersonUserInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JuristicPersonUserInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JuristicPersonUserInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
