import {Component, Input, OnInit} from '@angular/core';
import {PublicUserModel} from '../../models/public-user.model';

@Component({
  selector: 'app-juristic-person-user-information',
  templateUrl: './juristic-person-user-information.component.html',
  styleUrls: ['./juristic-person-user-information.component.css']
})
export class JuristicPersonUserInformationComponent implements OnInit {

  @Input() publicUser: PublicUserModel;
  constructor() { }

  ngOnInit() {
  }

}
