import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-ellipse-step',
  templateUrl: './ellipse-step.component.html',
  styleUrls: ['./ellipse-step.component.css']
})
export class EllipseStepComponent implements OnInit {

  constructor() { }

  @Input("step") step: number = 1;
  @Input("success") success: boolean = true;
  @Input("firstNode") firstNode: boolean = false;
  @Input("lastNode") lastNode: boolean = false;
  @Input("processName") processName: string = "";
  @Input("currentStep") currentStep: number = 1;
  ngOnInit() {
  }

}
