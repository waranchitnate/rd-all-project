import {Component, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {SsoUserModel} from '../../../core/models/sso-user.model';
import {AuthenticationService} from '../../../core/authentication/authentication.service';
import {SignatureService} from '../../../modules/certificate/services/signature.service';
import { EventEmitter } from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {AlertModalComponent} from '../../../shared/modals/alert-modal/alert-modal.component';
import {BsModalService, ModalOptions} from 'ngx-bootstrap';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-verify-usb',
  templateUrl: './verify-usb.component.html',
  styleUrls: ['./verify-usb.component.css']
})
export class VerifyUsbComponent implements OnInit {

  @Output() isValid: EventEmitter<any> = new EventEmitter<any>();

  usbDrive: any;
  userKey: string;
  time: string;
  selectedDevice: any;
  selectedDrive: any;
  invalidDrive = false;
  warningMsg: any;
  manualDrive: string;
  manualDriveControl: any;
  verifyDriveFormGroup: FormGroup;
  currentUserInfo: SsoUserModel = this.authenService.ssoUserDetailSnapshot;

  constructor( private fb: FormBuilder,
               private router: Router,
               private authenService: AuthenticationService,
               private bsModalService: BsModalService,
               private toastrService: ToastrService,
               private signatureService: SignatureService) {
                 this.initFormGroup();
               }

  ngOnInit() {
    this.invalidDrive = false;
    this.selectedDrive = undefined;
    this.selectedDevice = undefined;
    this.manualDrive = '';
    this.getDrivesList();
    this.authenService.ssoUserDetail.subscribe(res => {
      if (res) {
        this.currentUserInfo = res;
        this.getUserKeyByUsername(this.currentUserInfo.username);
        this.verifyDriveFormGroup.patchValue({userId:this.currentUserInfo.username});
        this.verifyDriveFormGroup.updateValueAndValidity();
      }
    });
  }

  initFormGroup() {
    this.verifyDriveFormGroup = this.fb.group({
      userId: '',
      drive: '',
      key: '',
      time: '',
      pin: ['', Validators.required]
    });
  }

  get pin() {
    return this.verifyDriveFormGroup.get('pin');
  }

  getDrivesList() {
    this.usbDrive = [];
    this.signatureService.getLocalDrives().subscribe(result => {
      this.usbDrive = result.data;
    },
      error => {
      this.usbDrive = [{drive: 'Kingston://'}, {drive: 'Sandisk://'}];
      });
  }

  getUserKeyByUsername(username: string) {
    this.signatureService.getUserKeyByUsername(username).subscribe(result => {
      this.userKey = result.data;
    }) ;
  }

  manualDriveTyping(event) {
    this.manualDrive = event.target.value;
    this.manualDriveControl = event.target;
    this.selectedDrive = undefined;
    this.invalidDrive = false;
  }

  selectDrive(driveName) {
    this.selectedDrive = driveName;
    this.manualDrive = undefined;
    this.manualDriveControl = null;
    this.invalidDrive = false;
  }

  verifyCertificate() {
     // ถ้าไม่ fix ให้ใช้ fuction ด้านล้างที่คอมเม้นอยู่
    this.verifyDriveFormGroup.get('key').setValue(this.userKey);
    this.verifyDriveFormGroup.get('userId').setValue('user1');
    this.verifyDriveFormGroup.get('key').setValue('hsR43Tap2ymgo+2W0LIj5g==');
    this.verifyDriveFormGroup.get('drive').setValue(this.selectedDrive);
    this.time = '24/01/2020 15:13';
    this.verifyDriveFormGroup.get('time').setValue(this.time);
    this.signatureService.verifyUSB({requestData: [this.verifyDriveFormGroup.getRawValue()]}).subscribe(result => {
      console.log(result);
      result.request = this.verifyDriveFormGroup.getRawValue();
      this.isValid.emit(result);
    }, error => {
       console.log(error.name);
       if ( error.name === 'HttpErrorResponse' ) {
        this.toastrService.error('ไม่พบการติดตั้งโปรแกรมสำหรับยืนยันตัวบุคคล กรุณาติดตั้งหรือติดต่อผู้ดูแลระบบ', 'ข้อผิดพลาด');
       }
    });

  }

  // verifyCertificate() {
  //   this.signatureService.preparedInfoByUserName(this.currentUserInfo.username)
  //     .subscribe((preparedResult: any) => {
  //       this.verifyDriveFormGroup.get('userId').setValue(this.currentUserInfo.username);
  //       this.verifyDriveFormGroup.get('key').setValue(preparedResult.data.userKey);
  //       this.verifyDriveFormGroup.get('drive').setValue(this.selectedDrive);
  //       this.verifyDriveFormGroup.get('time').setValue(preparedResult.data.time);
  //     this.signatureService.verifyUSB({requestData: [this.verifyDriveFormGroup.getRawValue()]}).subscribe(result => {
  //       console.log(result);
  //       if(preparedResult.data.claimTrue = result.data){
  //       this.isValid.emit(true);
  //       }else if(preparedResult.data.claimFalse = result.data){
  //         this.isValid.emit(false);
  //       }else{
  //         console.log("String hash ถูกแก้ไข หรือไม่ได้มาจาก ระบบ");
  //         this.isValid.emit(false);
  //       }
  //     }, error => {
  //        console.log(error.name);
  //     });
  //   });
  //   }
}
