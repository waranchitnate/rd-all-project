import {Component, Input, OnInit, ViewChild, TemplateRef} from '@angular/core';
import {UserAttachmentModel} from '../../../modules/admin/public-user-management/models/user-attachment.model';
import {MRegisteredAttachmentModel} from '../../../modules/admin/public-user-management/models/m-registered-attachment.model';
import {MRegisteredAttachmentService} from '../../../modules/admin/public-user-management/services/m-registered-attachment.service';
import {FileService} from '../../services/file.service';
import {forkJoin} from 'rxjs';
import {UserAttachmentService} from '../../../core/services/user-attachment.service';
import {environment} from '../../../../environments/environment';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ScannedUserAttachmentModel } from 'src/app/modules/admin/public-user-management/models/scanned-user-attachment.model';

@Component({
  selector: 'app-user-attachment-table',
  templateUrl: './user-attachment-table.component.html',
  styleUrls: ['./user-attachment-table.component.css']
})
export class UserAttachmentTableComponent implements OnInit {

  @Input() hashUserId: string;
  @Input() userTypeKey: string;
  @Input() username: string;
  fileApi = environment.internalUserManagementApiUrl + 'file/download-user-attachment/';
  fileScanApi = environment.internalUserManagementApiUrl + 'file/download-scan-attachment/';
  scannedUserAttachmentList: Array<ScannedUserAttachmentModel> = [];
  userAttachmentDtoList: Array<UserAttachmentModel>;
  registeredAttachmentList: Array<MRegisteredAttachmentModel> = [];
  scanFileList = [];
  refScanModal: NgbModalRef;
  registeredAttachment: MRegisteredAttachmentModel;
  userAttachmentModel: UserAttachmentModel;

  constructor(
    private mRegisteredAttachmentService: MRegisteredAttachmentService,
    private fileService: FileService,
    private userAttachmentService: UserAttachmentService,
    private modalService: NgbModal) {
  }

  ngOnInit() {
    forkJoin({userAttachmentList: this.userAttachmentService.findAllByUserId(this.hashUserId),
        registeredAttachmentList: this.mRegisteredAttachmentService.getRequiredAttachmentsByUserTypeKey(this.userTypeKey)})
      .subscribe(res => {
        console.log(res);
        this.userAttachmentDtoList = res.userAttachmentList;
        this.registeredAttachmentList = res.registeredAttachmentList;
        this.setUserAttachmentIdToMRegisteredAttachment();
      });
      this.getScanAttachFile();
  }

  getScanAttachFile() {
    this.userAttachmentService.findScanAttachAllByUserId(this.hashUserId).subscribe(res => {
      this.scannedUserAttachmentList = res;
    });
  }

  setUserAttachmentIdToMRegisteredAttachment() {
    this.registeredAttachmentList.forEach(registeredAttachment => {
      const userAttachment = this.userAttachmentDtoList.find(item => item.mRegisteredAttachmentId === registeredAttachment.id);
      if (userAttachment !== undefined) {
        this.fileService.getUserAttachment(userAttachment.userAttachmentId).subscribe(res => {
          registeredAttachment.fileModel = res;
        });
      }
    });
  }

  openScanModal(parentRegisteredAttachment, scanModal, parentUserAttachmentModel) {
    this.refScanModal = this.modalService.open(scanModal, { size: 'lg', backdrop: 'static' });
    this.registeredAttachment = parentRegisteredAttachment;
    this.userAttachmentModel = parentUserAttachmentModel;
  }

  closeScanModal() {
    this.getScanAttachFile();
    this.refScanModal.close();
  }

  upload(base64Pdf) {
    console.log(base64Pdf);
    this.refScanModal.close();
  }
}
