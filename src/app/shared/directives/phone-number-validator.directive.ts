import {Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';

@Directive({
  selector: '[appPhoneNumberValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: PhoneNumberValidatorDirective, multi: true}]
})
export class PhoneNumberValidatorDirective implements Validator {

  constructor() {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    let validationResult = null;
    if (control.value != null && control.value.length !== 0) {
      if (control.value.length < 2 && control.value[0] !== '0') {
        validationResult = {phoneNumberFormatError: 'หมายเลขโทรศัพท์ไม่ถูกต้อง'};
      } else if (control.value.length > 1 && (control.value[0] !== '0' || ['6', '8', '9'].indexOf(control.value[1]) === -1)) {
        validationResult = {phoneNumberFormatError: 'หมายเลขโทรศัพท์ไม่ถูกต้อง'};
      }
    }
    return validationResult;
  }
}
