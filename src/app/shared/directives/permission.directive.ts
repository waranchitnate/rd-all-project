import {Directive, Input, ViewContainerRef, TemplateRef} from '@angular/core';
import {AuthenticationService} from '../../core/authentication/authentication.service';

@Directive({
  selector: '[wsPermission]'
})
export class PermissionDirective {

  private userAuthorities = new Array<string>();

  constructor(private viewContainer: ViewContainerRef, private templateRef: TemplateRef<any>, private authenService: AuthenticationService) {
    if (this.authenService.ssoUserDetail !== undefined) {
      this.userAuthorities = this.authenService.ssoUserDetailSnapshot.authorities;
    }
  }

  @Input() set wsPermission(permissionList: Array<string>) {
    if (permissionList === undefined) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      const foundPermission = permissionList.find(s => this.userAuthorities.indexOf(s) != -1);
      if (foundPermission !== undefined) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else if (foundPermission === undefined) {
        this.viewContainer.clear();
      }
    }

  }

}
