import {Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';
import {PersonalNumberService} from '../../core/services/personal-number.service';

@Directive({
  selector: '[appPersonalNumberFormatValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: PersonalNumberFormatValidatorDirective, multi: true}]
})
export class PersonalNumberFormatValidatorDirective implements Validator {

  constructor(private personalNumberService: PersonalNumberService) {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    let validation: null | ValidationErrors = null;
    if (control.value !== null && control.value.length === 13) {
      if (!this.checkThreeDigitNumber(control.value) || !this.personalNumberService.checkSum(control.value)) {
        validation = {personalNumberFormatError: 'เลขประจำตัวผู้เสียภาษีอากรไม่ถูกต้อง'};
      }
    }
    return validation;
  }

  checkThreeDigitNumber(pId: string): boolean {
    const firstThreeDigit = pId.substr(0, 3);
    const firstThreeDigitNum = Number(firstThreeDigit);
    if (firstThreeDigitNum < Number(100) || firstThreeDigitNum == Number(601)) {
      return false;
    } else {
      return true;
    }
  }
}
