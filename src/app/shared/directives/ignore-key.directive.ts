import {Directive, ElementRef, HostListener, Input, isDevMode, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appIgnoreKey]'
})
export class IgnoreKeyDirective implements OnInit {

  @Input() allowKey: string;
  private englishAlphabet = 'abcdefghijklmnopqrstuvwxyz ';
  private thaiAlphabet = 'ๅภถุึคตจขชๆไำพะัีรนยบลฃฟหกดเ้่าสวงผปแอิืทมใฝูฎฑธํ๊ณฯญฐฅฤฆฏโฌ็๋ษศซฉฮฺ์ฒฬฦ ';
  private numberSymbol = '0123456789';
  private emailSymbol = '@-.';
  private datePickerCharSet = '0123456789/';

  constructor(private el: ElementRef, private renderer2: Renderer2) {
  }

  ngOnInit(): void {
    this.renderer2.setAttribute(this.el.nativeElement, 'autocomplete', 'off');
  }

  @HostListener('keydown', ['$event']) onkeyDown(event: KeyboardEvent): boolean {

    const k = event.key;
    if (this.allowKey === 'eng&space') {
      if ((!this.englishAlphabet.includes(k.toLocaleLowerCase()) && k.length === 1)) {
        event.preventDefault();
        return false;
      }
    } else if (this.allowKey === 'thai&space') {
      if ((!this.thaiAlphabet.includes(k) && k.length === 1)) {
        event.preventDefault();
        return false;
      }
    } else if (this.allowKey === 'eng&thai&space') {
      if ((!this.thaiAlphabet.includes(k) && k.length === 1) && (!this.englishAlphabet.includes(k.toLocaleLowerCase()) && k.length === 1)) {
        event.preventDefault();
        return false;
      }
    } else if (this.allowKey === 'eng') {
      if ((!this.englishAlphabet.includes(k.toLocaleLowerCase()) && k.length === 1)) {
        event.preventDefault();
        return false;
      }
    } else if (this.allowKey === 'thai') {
      if ((!this.thaiAlphabet.includes(k) && k.length === 1)) {
        event.preventDefault();
        return false;
      }
    } else if (this.allowKey === 'eng&num') {
      if ((!this.englishAlphabet.includes(k.toLocaleLowerCase()) && !this.numberSymbol.includes(k) && k.length === 1)) {
        event.preventDefault();
        return false;
      }
    } else if (this.allowKey === 'email') {
      if ((!this.englishAlphabet.includes(k.toLocaleLowerCase()) && !this.numberSymbol.includes(k) &&
        !this.emailSymbol.includes(k) && k.length === 1)) {
        event.preventDefault();
        return false;
      }
    } else if (this.allowKey === 'date-picker') {
      if (!this.datePickerCharSet.includes(k.toLocaleLowerCase()) && k.length === 1) {
        event.preventDefault();
        return false;
      }
    }
    return true;
  }

  @HostListener('paste', ['$event']) onPaste(event) {
    if (!isDevMode()) {
      event.preventDefault();
    }
  }

  @HostListener('drop', ['$event']) onDrop(event: DragEvent) {
    if (!isDevMode()) {
      event.preventDefault();
    }
  }
}
