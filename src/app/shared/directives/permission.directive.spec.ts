import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { UserHomeComponent } from '../../modules/user/user-home/user-home.component';
import { SharedModule } from '../shared.module';
import { PermissionDirective } from './permission.directive';
import { AuthenticationService } from '../../core/authentication/authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OauthCredential, UserDetail } from '../../core/models/oauth-credential';

// function setUpComponent(){
//     return TestBed.configureTestingModule({
//         declarations: [ UserHomeComponent],
//         providers: [AuthenticationService],
//         imports: [HttpClientTestingModule, SharedModule, RouterTestingModule]
//     })
//     .createComponent(UserHomeComponent);
// }

describe('PermissionDirective', () => {
  let component: UserHomeComponent;
  let fixture: ComponentFixture<UserHomeComponent>;
  let authenticationService: AuthenticationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ UserHomeComponent],
      providers: [AuthenticationService],
      imports: [HttpClientTestingModule, SharedModule, RouterTestingModule]
    });
    fixture = TestBed.createComponent(UserHomeComponent);
    component = fixture.componentInstance;
    component.users = [{name: 'Jonathan Wick', position: 'Developer', age: 23}];
    authenticationService = TestBed.get(AuthenticationService);
  });

  it('should not display add cert-user btn to non cert-admin role', () => {
    expect(authenticationService.loginDetailSnapShot.user_detail).toBeNull();
    fixture.detectChanges();
    const btn: HTMLElement = fixture.nativeElement.querySelector('#editUserBtn');
    expect(btn).toBeNull();
  });

  it('should display add cert-user btn to cert-admin role', () => {
    authenticationService = TestBed.get(AuthenticationService);
    authenticationService.loginDetailSnapShot = new OauthCredential();
    authenticationService.loginDetailSnapShot.user_detail = new UserDetail();
    authenticationService.loginDetailSnapShot.user_detail.authorities = ['ROLE_ADMIN'];
    fixture.detectChanges();
    const btn: HTMLElement = fixture.nativeElement.querySelector('#editUserBtn');
    expect(btn).toBeTruthy();
  });

});
