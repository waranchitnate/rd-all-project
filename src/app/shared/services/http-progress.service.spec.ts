import { TestBed } from '@angular/core/testing';

import { HttpProgressService } from './http-progress.service';

describe('HttpProgressService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpProgressService = TestBed.get(HttpProgressService);
    expect(service).toBeTruthy();
  });
});
