import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpProgressService {

  private httpProgress = new BehaviorSubject(new Array<string>());

  constructor() { }

  getHttpProgressObservable() {
    return this.httpProgress;
  }

  pushRequestToHttpProgress(req: string) {
    let httpProgress = this.httpProgress.value;
    const index = httpProgress.push(req);
    this.httpProgress.next(httpProgress);
    return index;
  }

  popRequestFromHttpProgress(req: string) {
    let httpProgress = this.httpProgress.value;
    const item = httpProgress.splice(httpProgress.indexOf(req), 1);
    this.httpProgress.next(httpProgress);
    return item;
  }
}
