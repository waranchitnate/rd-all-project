import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FileModel} from '../../modules/admin/public-user-management/models/file.model';
import {map} from 'rxjs/operators';
import {DomSanitizer} from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private fileApiUrl = environment.internalUserManagementApiUrl + 'file/';
  private scannerApiUrl = environment.scannerApiUrl;

  headers = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
    'SkipAuth': '1'
  })

  constructor(private httpClient: HttpClient, private sanitizer: DomSanitizer) {
  }

  public getUserAttachment(userAttachmentId: number): Observable<FileModel> {
    return this.httpClient.get<FileModel>(this.fileApiUrl + 'user-attachment?userAttachmentId=' + userAttachmentId)
      .pipe(
        map(s => this.getSafeResourceUrl(s, this.sanitizer))
      );
  }

  public uploadScannedAttachment(username: string, fileName: string, fileExtension: string, dataBase64: string) {
    const fileModel: FileModel = <FileModel>{
      fileBaseName: fileName,
      fileExtension: fileExtension,
      dataBase64: dataBase64
    };
    return this.httpClient.post(this.fileApiUrl + 'upload-scanned-attachment?username=' + username, fileModel);
  }

  private getSafeResourceUrl(fileModel: FileModel, sanitizer: DomSanitizer): FileModel {
    if (fileModel != null) {
      fileModel.safeResourceUrl = sanitizer
        .bypassSecurityTrustResourceUrl('data:' + fileModel.contentType + ';base64, ' + fileModel.dataBase64);
    }
    return fileModel;
  }

  public getScannerList(): Observable<any> {
    return this.httpClient.get<any>(this.scannerApiUrl+'scannerList&cacheIE='+new Date().getMilliseconds(), { headers: this.headers });
  }

  public scan(scannerName: string, duplexEnable: string): Observable<any> {
    return this.httpClient.get<any>(this.scannerApiUrl+'scan?scannerName='+scannerName+'&duplexEnable='+duplexEnable+'&pdfType=true&cacheIE='+new Date().getMilliseconds(), { headers: this.headers })
      .pipe(
        map(s => this.getScanSafeResourceUrl(s, this.sanitizer))
      );
  }

  private getScanSafeResourceUrl(res: any, sanitizer: DomSanitizer): any {
    if (res != null) {
      //image png
      for(let file of res.responseMsg){
        if(file.image){
          file.image = sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64, ' + file.base64Image);
        }
      }

      //pdf
      if(res.base64Pdf){
        const isIEOrEdge = /msie\s|trident\//i.test(window.navigator.userAgent);
        if(isIEOrEdge){
          res.pdf = this.sanitizer.bypassSecurityTrustResourceUrl(this.scannerApiUrl+'download?fileName='+res.fileName);
        }else{
            const byteArray = new Uint8Array(atob(res.base64Pdf).split('').map(char => char.charCodeAt(0)));
            let pdfBlob = new Blob([byteArray], {type: 'application/pdf'});
            let pdfUrl = window.URL.createObjectURL(pdfBlob);
            res.pdf = this.sanitizer.bypassSecurityTrustResourceUrl(pdfUrl);
        }
      }
    }

    return res;
  }
}
