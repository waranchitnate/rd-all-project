import { Injectable } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertModalComponent } from '../modals/alert-modal/alert-modal.component';
import { AnswerModalComponent } from '../modals/answer-modal/answer-modal.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(public modalService: BsModalService) { }

  openModal(title, content) {
    const modalRef = this.modalService.show(AlertModalComponent);
    modalRef.content.title = title;
    modalRef.content.content = content;
    return modalRef;
  }

  openConfirmModal(title, content): BsModalRef {
    const modalRef = this.modalService.show(AnswerModalComponent, {backdrop: true, ignoreBackdropClick: true});
    modalRef.content.title = title;
    modalRef.content.content = content;
    return modalRef;
  }

}
