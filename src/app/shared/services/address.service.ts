import {Amphoe, Province, Tambon} from '../models/address.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class AddressService {

  constructor(private httpClient: HttpClient) {
  }

  getProvince(): Observable<Province[]> {
    return <Observable<Province[]>>this.httpClient.get(environment.commonApiUrl + 'province/find-all');
  }

  getAmphoe(provinceId: string): Observable<Amphoe[]> {
    return <Observable<Amphoe[]>>this.httpClient.get(environment.commonApiUrl + 'amphoe/find-by-province-id',
      {params: new HttpParams({fromObject: {provinceId: provinceId}})});
  }

  getTambon(amphoeId: string): Observable<Tambon[]> {
    return <Observable<Tambon[]>>this.httpClient.get(environment.commonApiUrl + 'tambon/find-by-amphoe-id',
      {params: new HttpParams({fromObject: {amphoeId: amphoeId}})});
  }


}
