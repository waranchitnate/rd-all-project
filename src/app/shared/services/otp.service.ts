import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OtpResponse} from '../models/OtpResponse.model';

@Injectable({
  providedIn: 'root'
})
export class OtpService {

  private otpApi = environment.internalUserManagementApiUrl + 'otp/';

  constructor(private http: HttpClient) {
  }

  public requestOtpForActiveUser(phoneNumber: string, username: string): Observable<OtpResponse> {
    let httpParam = new HttpParams();
    httpParam = httpParam.append('phoneNumber', phoneNumber);
    httpParam = httpParam.append('username', username);
    return this.http.get<OtpResponse>(this.otpApi + 'request-otp-for-active-user', {params: httpParam});
  }

  public verifyOtp(refCode: string, otpCode: string, phoneNumber: string, username: string): Observable<OtpResponse> {
    let httpParam = new HttpParams();
    httpParam = httpParam.append('phoneNumber', phoneNumber);
    httpParam = httpParam.append('username', username);
    httpParam = httpParam.append('otpCode', otpCode);
    httpParam = httpParam.append('refCode', refCode);
    return this.http.get<any>(this.otpApi + 'verify-otp', {params: httpParam});
  }
}
