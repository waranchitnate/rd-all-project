export interface AddressModel {
  amphoeCode: number;
  amphoeName: string;
  buildingName: string;
  moo: string;
  no: string;
  postcode: string;
  provinceCode: number;
  provinceName: string;
  road: string;
  soi: string;
  tambonCode: number;
  tambonName: string;
  trok: string;
  roomNo: string;
  floor: string;
}

export interface Province {
  provinceId: string;
  provinceCode: string|null;
  provinceNameEn: string;
  provinceNameTh: string;
}

export interface Amphoe {
  amphoeId:  string;
  amphoeCode:  string|null;
  amphoeNameEn: string;
  amphoeNameTh:  string;
}


export interface Tambon {
  tambonId: string;
  tambonCode: string|null;
  tambonNameEn: string;
  tambonNameTh: string;
  zipCode: string;
}
