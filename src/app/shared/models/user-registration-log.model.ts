export interface UserRegistrationLogModel {
  actionName: string;
  createdDate: Date;
  createdBy: string;
}
