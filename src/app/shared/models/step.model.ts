export class Step {
  title: string;
  circle: string;
  path?: string;
}
