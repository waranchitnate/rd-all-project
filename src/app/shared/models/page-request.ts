export class PageRequest {
    pageSize = 10;
    page = 1;
    sortDirection = 'ASC';
    sortFieldName: string;
}
