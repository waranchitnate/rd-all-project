
export class ResponseT<T> {
  [x: string]: any;
  status: number;
  message: string;
  data: T;
}
