export interface NameValuePairModel {
  name: string;
  value: string;
}
