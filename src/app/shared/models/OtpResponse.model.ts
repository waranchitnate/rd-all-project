export interface OtpResponse {
  code?: string;

  errorCode?: string;

  message?: string;

  data?: OtpResponseData;
}

export interface OtpResponseData {
  refCode?: string;
  otpCode?: string;
}
