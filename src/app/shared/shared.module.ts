import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AlertModalComponent} from './modals/alert-modal/alert-modal.component';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {ModalModule, BsModalRef} from 'ngx-bootstrap/modal';
import {PermissionDirective} from './directives/permission.directive';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {StepperComponent} from './components/stepper/stepper.component';
import {AddressComponent} from './components/address/address.component';
import {NgSelectConfig, NgSelectModule} from '@ng-select/ng-select';
import {AddressService} from './services/address.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsDatepickerModule, BsLocaleService} from 'ngx-bootstrap-th/datepicker';
import {defineLocale} from 'ngx-bootstrap-th/chronos';
import {thLocale} from 'ngx-bootstrap-th/locale';
import {DatepickerThComponent} from './components/datepicker-th/datepicker-th.component';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import {TypeaheadModule} from 'ngx-bootstrap/typeahead';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {UiSwitchModule} from 'ngx-ui-switch';
import {AnswerModalComponent} from './modals/answer-modal/answer-modal.component';
import {VerifyOtpModalComponent} from './modals/verify-otp-modal/verify-otp-modal.component';
import { NumberOnlyDirective } from './directives/number-only.directive';
import { EllipseStepComponent } from './components/ellipse-step/ellipse-step.component';
import {MonthYearDatePipe, ThaiDatePipe, ThaiMonthYearDatePipe,ThaiCustomFortmatPipe} from './pipe/thai-date.pipe';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { AccordionModule} from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ToastrModule } from 'ngx-toastr';
import { TooltipModule } from 'ngx-bootstrap-th';
import {IgnoreKeyDirective} from './directives/ignore-key.directive';
import { FloatButtonComponent } from './components/float-button/float-button.component';
import { ForeignIndividualUserInformationComponent } from './components/foreign-individual-user-information/foreign-individual-user-information.component';
import { EstateIndividualUserInformationComponent } from './components/estate-individual-user-information/estate-individual-user-information.component';
import { PartnershipIndividualUserInformationComponent } from './components/partnership-individual-user-information/partnership-individual-user-information.component';
import { JuristicPersonUserInformationComponent } from './components/juristic-person-user-information/juristic-person-user-information.component';
import { ThaiIndividualUserInformationComponent } from './components/thai-individual-user-information/thai-individual-user-information.component';
import { SenderUserInformationComponent } from './components/sender-user-information/sender-user-information.component';
import {UserAttachmentTableComponent} from './components/user-attachment-table/user-attachment-table.component';
import {ReplaceNullValuePipe} from './pipe/replace-null-value.pipe';
import {SumArrayFieldValuePipe} from './pipe/sum-array-field-value.pipe';
import { MoneyNumberToWordThPipe } from './pipe/money-number-to-word-th.pipe';
import {PersonalNumberFormatValidatorDirective} from './directives/personal-number-format-validator.directive';
import {PhoneNumberValidatorDirective} from './directives/phone-number-validator.directive';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { ScannerComponent } from './components/scanner/scanner.component';
import { CookieService } from 'ngx-cookie-service';
import { VerifyUsbComponent } from './components/verify-usb/verify-usb.component';
defineLocale('th', thLocale);

import { NgZorroAntdModule, NZ_I18N, th_TH } from 'ng-zorro-antd';
import {AffiliationUserInformationComponent} from './components/affiliation-user-information/affiliation-user-information.component';
import { ThaiPndTypePipe } from './pipe/thai-pnd-type.pipe';
import { ThaiMonthPipe } from './pipe/thai-month.pipe';
import { ThaiYearPipe } from './pipe/thai-year.pipe';
import { ThaiPndGroupPipe } from './pipe/thai-pnd-group.pipe';
import { AngularDualListBoxModule } from 'angular-dual-listbox';

@NgModule({
  imports: [
    CommonModule,
    NgbModalModule,
    NgSelectModule,
    ReactiveFormsModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    PaginationModule.forRoot(),
    CollapseModule.forRoot(),
    TypeaheadModule.forRoot(),
    BsDropdownModule.forRoot(),
    UiSwitchModule,
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    ToastrModule.forRoot({timeOut: 60000, closeButton: true}),
    TooltipModule.forRoot(),
    TabsModule.forRoot(),
    TimepickerModule.forRoot(),
    NgZorroAntdModule,
    AngularDualListBoxModule
  ],
  declarations: [
    AlertModalComponent
    , PermissionDirective
    , ProgressBarComponent
    , StepperComponent
    , AddressComponent
    , DatepickerThComponent
    , AnswerModalComponent
    , VerifyOtpModalComponent
    , NumberOnlyDirective
    , EllipseStepComponent
    , ThaiDatePipe
    , ThaiCustomFortmatPipe
    , UserInfoComponent
    , ThaiMonthYearDatePipe
    , IgnoreKeyDirective
    , FloatButtonComponent
    , ForeignIndividualUserInformationComponent
    , EstateIndividualUserInformationComponent
    , PartnershipIndividualUserInformationComponent
    , JuristicPersonUserInformationComponent
    , ThaiIndividualUserInformationComponent
    , SenderUserInformationComponent
    , UserAttachmentTableComponent
    , ReplaceNullValuePipe
    , SumArrayFieldValuePipe
    , MoneyNumberToWordThPipe
    , MonthYearDatePipe
    , PersonalNumberFormatValidatorDirective
    , PhoneNumberValidatorDirective
    , ScannerComponent
    , VerifyUsbComponent
    , AffiliationUserInformationComponent
    , ThaiPndTypePipe
    , ThaiMonthPipe
    , ThaiYearPipe
    , ThaiPndGroupPipe
  ],
  exports: [
    AlertModalComponent
    , PermissionDirective
    , ProgressBarComponent
    , StepperComponent
    , NgSelectModule
    , AddressComponent
    , AnswerModalComponent
    , VerifyOtpModalComponent
    , BsDatepickerModule
    , DatepickerThComponent
    , PaginationModule
    , CollapseModule
    , TypeaheadModule
    , BsDropdownModule
    , ReactiveFormsModule
    , FormsModule
    , UiSwitchModule
    , ModalModule
    , EllipseStepComponent
    , ThaiDatePipe
    , ThaiCustomFortmatPipe
    , AccordionModule
    , UserInfoComponent
    , NumberOnlyDirective
    , ThaiMonthYearDatePipe
    , ToastrModule
    , TooltipModule
    , IgnoreKeyDirective
    , TabsModule
    , FloatButtonComponent
    , ForeignIndividualUserInformationComponent
    , EstateIndividualUserInformationComponent
    , PartnershipIndividualUserInformationComponent
    , JuristicPersonUserInformationComponent
    , ThaiIndividualUserInformationComponent
    , SenderUserInformationComponent
    , UserAttachmentTableComponent
    , ReplaceNullValuePipe
    , SumArrayFieldValuePipe
    , MoneyNumberToWordThPipe
    , MonthYearDatePipe
    , PersonalNumberFormatValidatorDirective
    , PhoneNumberValidatorDirective
    , TimepickerModule
    , ScannerComponent
    , NgZorroAntdModule
    , VerifyUsbComponent
    , AffiliationUserInformationComponent
    , ThaiPndTypePipe
    , ThaiMonthPipe
    , ThaiYearPipe
    , ThaiPndGroupPipe
    , AngularDualListBoxModule
  ],
  entryComponents: [AlertModalComponent, AnswerModalComponent, VerifyOtpModalComponent],
  providers: [AddressService, BsModalRef, CookieService, { provide: NZ_I18N, useValue: th_TH }]
})
export class SharedModule {
  constructor(private ngSelectConfig: NgSelectConfig, private bsLocaleService: BsLocaleService) {
    this.ngSelectConfig.notFoundText = 'ไม่พบข้อมูล';
    this.ngSelectConfig.loadingText = 'กำลังค้นหา';
    this.bsLocaleService.use('th');
  }
}
