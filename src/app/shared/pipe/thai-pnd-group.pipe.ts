import { Pipe, PipeTransform } from '@angular/core';
import { DssConstant } from '../../modules/dss/class/dss-constants';

@Pipe({
  name: 'thaiPndGroup'
})
export class ThaiPndGroupPipe implements PipeTransform {

  groupPndList: any[] = DssConstant.grouppndList;
  transform(value: any): any {
    return (this.groupPndList.find((obj) => obj.value == value) || {}).name;
  }

}
