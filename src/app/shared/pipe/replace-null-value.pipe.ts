import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'replaceNullValue'
})
export class ReplaceNullValuePipe implements PipeTransform {

  transform(value: any, replaceWord?: string): any {
    return value == null || value == "" ? replaceWord : value;
  }

}
