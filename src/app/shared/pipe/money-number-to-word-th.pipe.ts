import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'moneyNumberToWordTh'
})
export class MoneyNumberToWordThPipe implements PipeTransform {

  moneyUnit = ['', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน'];
  numberWord = [
    {name: '0', value: 'ศูนย์'},
    {name: '1', value: 'หนึ่ง'},
    {name: '2', value: 'สอง'},
    {name: '3', value: 'สาม'},
    {name: '4', value: 'สี่'},
    {name: '5', value: 'ห้า'},
    {name: '6', value: 'หก'},
    {name: '7', value: 'เจ็ด'},
    {name: '8', value: 'แปด'},
    {name: '9', value: 'เก้า'}
    ];

  transform(value: number, args?: any): any {
    let moneyWord = '';
    const moneySplit = value.toString().split('.');
    const bath = moneySplit[0];
    const satang = moneySplit.length > 1 ? moneySplit[1] : '';

    for (let i = bath.length - 1; i !== 0; i--) {
      moneyWord = moneyWord.concat(this.numberWord.find(s => s.name === bath[i]).value);
      if (i !== 0) {
        moneyWord = moneyWord.concat(this.moneyUnit[i % 7]);
      } else {
        moneyWord = moneyWord.concat('บาท');
      }
    }
    if (satang.length > 0) {
      for(let i = 0; i < satang.length; i++) {
        moneyWord = moneyWord.concat(this.numberWord.find(s => s.name === satang[i]).value);
      }
      moneyWord = moneyWord.concat('สตางค์');
    }
    return moneyWord;
  }

}
