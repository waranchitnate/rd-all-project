import { PipeTransform, Pipe } from '@angular/core';
import * as moment from 'moment';
@Pipe({ name: 'thaidate' })
export class ThaiDatePipe implements PipeTransform {
    constructor() { }
    transform(value, format ?: string) {
        moment.locale('th');
        const date = ((value === null || value === undefined) ? '-' : moment(value).add(543, 'years').format(format ? format : 'DD/MM/YYYY'));
        return date;
    }
}

@Pipe({ name: 'thaiCustomFormatdate' })
export class ThaiCustomFortmatPipe implements PipeTransform {
    constructor() { }
    transform(value, format ?: string) {
        moment.locale('th');
        const date = ((value === null || value === undefined) ? '-' : moment(value,format ? format : 'DD-MM-YYYY').add(543, 'years').format(format ? format : 'DD/MM/YYYY'));
        return date;
    }
}


@Pipe({ name: 'thaimonthyeardate' })
export class ThaiMonthYearDatePipe implements PipeTransform {
    constructor() { }
    transform(value, format) {
        moment.locale('th');
        const date = ((value === null || value === undefined) ? '-' : moment(value).add(543, 'years').format(format ? format : 'MMMM YYYY'));
        return date;
    }
}

@Pipe({ name: 'monthyeardate' })
export class MonthYearDatePipe implements PipeTransform {
    constructor() { }
    transform(value, format) {
        moment.locale('th');
        const date = ((value === null || value === undefined) ? '-' : moment(value).format(format ? format : 'MMMM YYYY'));
        return date;
    }
}
