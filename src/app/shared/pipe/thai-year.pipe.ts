import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'thaiYear'
})
export class ThaiYearPipe implements PipeTransform {

  transform(value: any): any {
    return Number(value) + 543;
  }

}
