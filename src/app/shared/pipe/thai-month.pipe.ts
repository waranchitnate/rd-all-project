import { Pipe, PipeTransform } from '@angular/core';
import { DssConstant } from '../../modules/dss/class/dss-constants';

@Pipe({
  name: 'thaiMonth'
})
export class ThaiMonthPipe implements PipeTransform {

  monthList: any[] = DssConstant.monthList;
  transform(value: any): any {
    return this.monthList.find((obj) => obj.value == Number(value) - 1).name;
  }

}
