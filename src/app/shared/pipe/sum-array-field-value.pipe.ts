import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sumArrayFieldValue'
})
export class SumArrayFieldValuePipe implements PipeTransform {

  transform(value: Array<any>, fileName: string): any {
    let sum = 0;
    if(value !== undefined && value !== null && value.length > 0) {
      value.forEach(s => sum += s[fileName]);
    }
    return sum;
  }

}
