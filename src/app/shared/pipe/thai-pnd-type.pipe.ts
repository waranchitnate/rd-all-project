import { Pipe, PipeTransform } from '@angular/core';
import { DssConstant } from '../../modules/dss/class/dss-constants';

@Pipe({
  name: 'thaiPndType'
})
export class ThaiPndTypePipe implements PipeTransform {
  
  pndTypeList: any[] = DssConstant.pndTypeList;
  transform(value: any): any {
    return this.pndTypeList.find((obj) => obj.value == value).name;
  }

}
