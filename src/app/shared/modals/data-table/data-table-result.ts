export interface DataTableResult<T> {
    data: T,
    recordsTotal: number,
    totalPages: number
}