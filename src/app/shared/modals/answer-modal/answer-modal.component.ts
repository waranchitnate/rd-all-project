import {Component, OnInit, EventEmitter, Input, Output, OnDestroy} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-answer-modal',
  templateUrl: './answer-modal.component.html',
  styleUrls: ['./answer-modal.component.css']
})
export class AnswerModalComponent implements OnInit, OnDestroy {

  @Input() content;
  @Input() title;
  @Input() idButtonTrue;
  @Input() idButtonFalse;

  @Output() answerEvent = new EventEmitter();

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.answerEvent.complete();
  }

  answer(ans: boolean) {
    this.answerEvent.next(ans);
    this.bsModalRef.hide();
  }

}
