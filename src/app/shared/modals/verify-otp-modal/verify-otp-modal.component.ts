import {Component, EventEmitter, isDevMode, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {OtpResponse} from '../../models/OtpResponse.model';
import {OtpService} from '../../services/otp.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-verify-otp-modal',
  templateUrl: './verify-otp-modal.component.html',
  styleUrls: ['./verify-otp-modal.component.css']
})
export class VerifyOtpModalComponent implements OnInit, OnDestroy {

  @Output('closeModalEvent') closeModalEvent = new EventEmitter();
  @Output('requestNewOtpEvent') requestNewOtpEvent = new EventEmitter();
  username: string;
  phoneNumber: string;
  otpResponse: OtpResponse;
  error;
  otpForm: FormGroup;

  constructor(public bsModalRef: BsModalRef, private otpService: OtpService, private bsModalService: BsModalService, private toastrService: ToastrService) {

  }

  ngOnInit() {
    this.otpForm = new FormGroup({
        refCode: new FormControl(this.otpResponse.data.refCode),
        otpCode: new FormControl(null, Validators.required)
      }
    );

    if (isDevMode()) {
      this.otpForm.get('otpCode').setValue(this.otpResponse.data.otpCode);
    }
  }

  ngOnDestroy(): void {
    this.closeModalEvent.complete();
    this.requestNewOtpEvent.complete();
  }

  submit() {
    this.otpService.verifyOtp(this.refCode.value, this.otpCode.value, this.phoneNumber, this.username).subscribe(res => {
      if (res.code === '200') {
        this.toastrService.success('ยืนยันตัวตนสำเร็จ');
        this.closeModalEvent.next(true);
        this.bsModalRef.hide();
      } else {
        this.toastrService.error('ยืนยันตัวตนไม่สำเร็จเนื่องจาก ' + res.message, 'ข้อผิดพลาด');
      }
    }, ee => {
      this.error = ee.error;
      this.toastrService.success('ยืนยันตัวตนไม่สำเร็จเนื่องจาก ' + ee.error().message, 'ข้อผิดพลาด');
    });
  }

  closeModal() {
    this.closeModalEvent.next(false);
    this.bsModalRef.hide();
  }

  get refCode() {
    return this.otpForm.get('refCode');
  }

  get otpCode() {
    return this.otpForm.get('otpCode');
  }


}
