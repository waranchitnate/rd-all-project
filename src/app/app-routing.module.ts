import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {NotFoundComponent} from './core/pages/not-found/not-found.component';
import {AuthGuard} from './core/guard/auth.guard';
import {UnAuthorizeComponent} from './core/pages/un-authorize/un-authorize.component';
import {AuthenticationComponent} from './core/pages/authentication/authentication.component';
import {HomeComponent} from './modules/home/home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'pps', loadChildren: './modules/pps/pps.module#PpsModule'},
  { path: 'organization', loadChildren: './modules/admin/setting-organization/organization.module#OrganizationModule', canActivate: [AuthGuard]},
  { path: 'cert', loadChildren: './modules/certificate/certificate.module#CertificateModule', canActivate: [AuthGuard]},
  { path: 'public-user-management', loadChildren: './modules/admin/public-user-management/public-user-management.module#PublicUserManagementModule', canActivate: [AuthGuard]},
  { path: 'external-user-management', loadChildren: './modules/admin/external-user-type-management/external-user-type-management.module#ExternalUserTypeManagementModule', canActivate: [AuthGuard]},
  { path: 'group-management', loadChildren: './modules/admin/group-management/group-management.module#GroupManagementModule', canActivate: [AuthGuard]},
  { path: 'un-authorize', component: UnAuthorizeComponent},
  { path: 'authentication', component: AuthenticationComponent},
  { path: 'gws', loadChildren: './modules/welfare/welfare.module#WelfareModule', canActivate: [AuthGuard]},
  { path: 'pdf', loadChildren: './modules/pdf/pdf.module#PdfModule', canActivate: [AuthGuard]},
  { path: 'example', loadChildren: './modules/example/example.module#ExampleModule', canActivate: [AuthGuard]},
  { path: 'mns', loadChildren: './modules/mns/report.module#ReportModule', canActivate: [AuthGuard]},
  { path: 'trs', loadChildren: './modules/trs/report2.module#ReportModule', canActivate: [AuthGuard]},
  { path: 'dss', loadChildren: './modules/dss/dss.module#DssModule'},
  // { path: 'edc', loadChildren: './modules/edc/edc.module#EdcModule', canActivate: [AuthGuard]},
  { path: 'print', loadChildren: './modules/print/print.module#PrintModule' },
  { path: 'edc', loadChildren: './modules/edc/edc.module#EdcModule', canActivate: [AuthGuard]},
  { path: 'ars', loadChildren: './modules/ars/ars.module#ArsModule', canActivate: [AuthGuard]},
  { path: '**', component: NotFoundComponent }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
