import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from './core/authentication/authentication.service';
import {ActivatedRoute, NavigationStart, Router} from '@angular/router';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isAuthenticated = false;
  title = 'RD Internal';

  constructor(private authenticationService: AuthenticationService, private router: Router, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.authenticationService.ssoUserDetail.subscribe(ssoUser => {
      if (ssoUser !== null) {
        this.isAuthenticated = true;
      } else {
        this.isAuthenticated = false;
      }
    });

    this.router.events.pipe(take(1)).subscribe(evt => {
      if (evt instanceof NavigationStart) {
        const navigationStart: NavigationStart = evt as NavigationStart;
        if (navigationStart.url.indexOf('authentication') === -1) {
           this.router.navigateByUrl('authentication?redirectTo=' + navigationStart.url, {skipLocationChange: false});
        }
      }
    });
  }

}
