export const environment = {
  production: true,
  pkiFlag: false,
  webApiUrl: 'https://epayintra.rd.go.th/frmweb/',
  oauthUrl: 'https://epayintra.rd.go.th/pkioauth/',
  commonApiUrl: 'https://epayintra.rd.go.th/rd-common-api/',
  certApiUrl: 'https://epayintra.rd.go.th/rd-cert-management/',
  internalUserManagementApiUrl: 'https://epayintra.rd.go.th/rd-internal-user-management/',
  internalWelfareManagementApiUrl: 'https://epayintra.rd.go.th/rd-internal-welfare-management/',
  pdfUtilityUrl: 'https://epayintra.rd.go.th/rd-pdf-generator/',
  streamApiUrl: 'https://epayintra.rd.go.th/edc-internal/',
  caApiUrl: 'http://localhost:8888/signature/',
  vatApiUrl: 'https://epayintra.rd.go.th/rd-internal-dss/',
  ppsApiUrl: 'https://epayintra.rd.go.th/rd-internal-pps/',
  scannerApiUrl: 'http://localhost:9173/',
  arsApiUrl: 'https://epayintra.rd.go.th/rd-internal-ars/',
  dssApiUrl: 'https://epayintra.rd.go.th/rd-internal-dss/',
  mnsApiUrl:'https://epayintra.rd.go.th/rd-internal-mns/',
  trsApiUrl: 'https://epayintra.rd.go.th/rd-internal-trs/',
};
