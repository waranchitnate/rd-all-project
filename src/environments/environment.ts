// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  pkiFlag: false,
  production: false,
  // office
  // webApiUrl: 'http://localhost:8081/frmweb/',
  // oauthUrl: 'http://localhost:8090/pkioauth/',
  certApiUrl: 'http://localhost:8091/rd-cert-management/',
  // commonApiUrl: 'http://10.12.254.42:9082/rd-common-api/',
  // internalWelfareManagementApiUrl: 'http://10.12.254.42:9082/rd-internal-welfare-management/'

  // local
  webApiUrl: 'http://10.12.254.42:9082/frmweb/',
  oauthUrl: 'http://10.12.254.42:9082/pkioauth/',
  // certApiUrl: 'http://10.12.254.42:9080/rd-cert-management/',
  commonApiUrl: 'http://10.12.254.42:9082/rd-common-api/',
  // commonApiUrl: 'http://10.12.254.42:9080/rd-api-gateway/rd-common-api/',
  internalUserManagementApiUrl: 'http://10.12.254.42:9082/rd-internal-user-management/',
  internalWelfareManagementApiUrl: 'http://localhost:8093/rd-internal-welfare-management/',
  pdfUtilityUrl: 'http://localhost:8083/rd-pdf-generator/',
  //caApiUrl: 'http://localhost:8888/signature/',
  caApiUrl: 'http://localhost:8090/signature/',
  vatApiUrl: 'http://10.12.254.42:9082/rd-internal-dss/',
  // streamApiUrl : 'http://localhost:8090/edc-internal/'
  streamApiUrl: 'http://10.12.254.42:9082/edc-internal/',
  ppsApiUrl: 'http://localhost:8083/rd-internal-pps/',
  scannerApiUrl: 'http://localhost:9173/',
  arsApiUrl: 'http://119.160.213.17:9082/rd-internal-ars/',
  // arsApiUrl: 'http://localhost:8088/rd-internal-ars/',
  // arsApiUrl: 'http://192.168.2.39:8088/rd-internal-ars/',
  dssApiUrl: 'http://localhost:8083/rd-internal-dss/',
  mnsApiUrl: 'http://localhost:8083/rd-internal-mns/',
  trsApiUrl: 'http://localhost:8083/rd-internal-trs/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
