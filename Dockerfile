FROM nginx:1.15.5-alpine
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/general.conf /etc/nginx/nginxconfig.io/general.conf

RUN rm -rf /etc/nginx/conf.d/default.conf

COPY dist/ng6 /var/www/html/dist

